//
//  SWMyReportsCollectionViewCellForType2.h
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyReportsCollectionViewCellForType2;

@protocol SWMyReportsCollectionViewCellForType2Delegate <NSObject>
- (void)myReportsCollectionViewCellForType2TargetAction1:(SWMyReportsCollectionViewCellForType2 *)sender;
@end

@interface SWMyReportsCollectionViewCellForType2 : UICollectionViewCell
@property (nonatomic, weak) id<SWMyReportsCollectionViewCellForType2Delegate> delegate;
- (void)myReportsCollectionViewCellForType2SetMarginLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
- (void)myReportsCollectionViewCellForType2SetButtonAttributedString:(NSAttributedString *)attributedString;
- (void)myReportsCollectionViewCellForType2SetImage:(UIImage *)image;
- (void)myReportsCollectionViewCellForType2SetLabelAttributedString:(NSAttributedString *)attributedString;
- (void)myReportsCollectionViewCellForType2SetTextViewAttributedString:(NSAttributedString *)attributedString;
@end
