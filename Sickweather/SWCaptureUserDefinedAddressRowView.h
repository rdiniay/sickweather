//
//  SWCaptureUserDefinedAddressRowView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWCaptureUserDefinedAddressRowView;

@protocol SWCaptureUserDefinedAddressRowViewDelegate
- (NSString *)captureUserDefinedAddressRowViewTitleString:(SWCaptureUserDefinedAddressRowView *)sender;
@end

@interface SWCaptureUserDefinedAddressRowView : UIView
@property (nonatomic, weak) id<SWCaptureUserDefinedAddressRowViewDelegate> delegate;
- (void)showBottomBorder;
- (void)setUserText:(NSString *)userText;
- (NSString *)userText;
@end
