//
//  SWCaptureUserDefinedAddressViewController.h
//  Sickweather
//
//  Created by John Erck on 10/7/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWCaptureUserDefinedAddressViewController;

@protocol SWCaptureUserDefinedAddressViewControllerDelegate <NSObject>
- (void)captureUserDefinedAddressViewControllerWantsToDismiss:(SWCaptureUserDefinedAddressViewController *)sender;
- (void)captureUserDefinedAddressViewController:(SWCaptureUserDefinedAddressViewController *)sender userInputCityString:(NSString *)cityString stateRegionString:(NSString *)stateRegionString countryString:(NSString *)countryString;
- (NSString *)captureUserDefinedAddressViewControllerCity;
- (NSString *)captureUserDefinedAddressViewControllerState;
- (NSString *)captureUserDefinedAddressViewControllerCountry;
@end

@interface SWCaptureUserDefinedAddressViewController : UIViewController
@property (nonatomic, weak) id<SWCaptureUserDefinedAddressViewControllerDelegate> delegate;
@end
