//
//  SWConnectBluetoothDeviceTableViewController.h
//  Sickweather
//
//  Created by Muhammad Javed on 05/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@protocol SWConnectBluetoothDeviceViewControllerDelegate
- (void)connectBluetoothDeviceViewControllerWantsToDismiss:(id)sender;
- (void)connectBluetoothDeviceViewControllerWantsToCancel:(id)sender;
@end

@interface SWConnectBluetoothDeviceTableViewController : UITableViewController
@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) NSArray *imageArray;
@property (nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@property (nonatomic, weak) id<SWConnectBluetoothDeviceViewControllerDelegate> delegate;
@end

