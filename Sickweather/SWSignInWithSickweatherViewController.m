//
//  SWSignInWithSickweatherViewController.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//
@import SWAnalytics;
#import "Flurry.h"
#import "SWSignInWithSickweatherViewController.h"
#import "SWCreateSickweatherAccountViewController.h"
#import "SWAppDelegate.h"
#import "SWUserCDM+SWAdditions.h"

@interface SWSignInWithSickweatherViewController () <UITextFieldDelegate, UIAlertViewDelegate, SWCreateSickweatherAccountViewControllerDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) UITextField *activeField;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *topContainer;
@property (strong, nonatomic) UIView *formContainer;
@property (strong, nonatomic) UITextField *emailTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UIButton *signInWithSickweatherButton;
@property (strong, nonatomic) UIButton *forgotPasswordButton;
@property (strong, nonatomic) UIButton *createSickweatherAccountButton;
@property (strong, nonatomic) UIView *backgroundImageView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIButton *dismissButton;
@property (strong, nonatomic) UIAlertView *alertViewInvalidLoginCredentials;
@property (assign) BOOL keyboardIsComingUp;
@property (strong, nonatomic) NSLayoutConstraint *topContainerHeightConstraint;
@property (strong, nonatomic) UIAlertView *forgotPasswordAlert;
@property (strong, nonatomic) UITextField *validationTargetTextField;
@property (strong, nonatomic) UIAlertView *validationAlert;
@end

@implementation SWSignInWithSickweatherViewController

#pragma mark - SWCreateSickweatherAccountViewControllerDelegate

- (void)createSickweatherAccountViewControllerDidDismissWithSuccessfulLogin:(SWCreateSickweatherAccountViewController *)vc
{
    [self.delegate signInWithSickweatherViewControllerDidDismissWithSuccessfulLogin:self]; // Forward on
}

- (void)createSickweatherAccountViewControllerDidDismissWithoutLoggingIn:(SWCreateSickweatherAccountViewController *)vc
{
    // Does not get forwarded. Client of this class should never concern itself with this. We just disimss.
    [vc dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - UIScrollViewDelegate

//static CGFloat lastContentY = 0;
/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //NSLog(@"fabsf(scrollView.contentOffset.y = %f) - fabsf(lastContentY = %f) = %f", scrollView.contentOffset.y, lastContentY, fabs(scrollView.contentOffset.y) - fabs(lastContentY));
    //NSLog(@"%f - %f = %f", fabs(scrollView.contentOffset.y), fabs(lastContentY), fabs(scrollView.contentOffset.y) - fabs(lastContentY));
    if (fabs(scrollView.contentOffset.y) - fabs(lastContentY) > 40 || fabs(scrollView.contentOffset.y) - fabs(lastContentY) < -40)
    {
        if (!self.keyboardIsComingUp)
        {
            [self.activeField resignFirstResponder];
            lastContentY = scrollView.contentOffset.y;
        }
    }
}
*/

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:self.alertViewInvalidLoginCredentials])
    {
        if (buttonIndex == 0)
        {
            // Try Again
        }
        else if (buttonIndex == 1)
        {
            // Issue call to reset password
            [self issuePasswordResetPostRequestUsingCurrentEmail];
            
            /*
            // Reset Password
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.sickweather.com/signin/forgot.php?email=%@", [SWHelper helperURLEncode:self.emailTextField.text]]]];
             */
        }
    }
    else if ([alertView isEqual:self.forgotPasswordAlert])
    {
        if (buttonIndex == 0)
        {
            // Cancel
        }
        else if (buttonIndex == 1)
        {
            // Get user input
            NSString *email = [[alertView textFieldAtIndex:0] text];
            self.emailTextField.text = email;
            
            // Issue call to reset password
            [self issuePasswordResetPostRequestUsingCurrentEmail];
            
            /*
             // Reset Password
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.sickweather.com/signin/forgot.php?email=%@", [SWHelper helperURLEncode:self.emailTextField.text]]]];
             */
        }
    }
    else if ([alertView isEqual:self.validationAlert])
    {
        if (self.validationTargetTextField)
        {
            [self.validationTargetTextField becomeFirstResponder];
        }
    }
}

#pragma mark - Target/Action

- (void)signInWithSickweatherButtonTapped:(id)sender
{
    if ([self.emailTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.emailTextField;
        self.validationAlert = [[UIAlertView alloc] initWithTitle:@"Missing Email" message:@"Please enter your email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        self.validationAlert.delegate = self;
        [self.validationAlert show];
    }
    else if ([self.passwordTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.passwordTextField;
        self.validationAlert = [[UIAlertView alloc] initWithTitle:@"Missing Password" message:@"Please enter your password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        self.validationAlert.delegate = self;
        [self.validationAlert show];
    }
    else
    {
        [self.activityIndicator startAnimating];
        [[SWHelper helperAppBackend] signIntoSickweatherUsingEmail:self.emailTextField.text password:self.passwordTextField.text usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicator stopAnimating];
                if ([[jsonResponseBody objectForKey:@"valid"] isEqualToString:@"true"])
                {
                    SWUserCDM *loggedInUser = [SWUserCDM createOrGetAsUpdatedLoggedInUserUsingUserId:[jsonResponseBody objectForKey:@"user_id"] emailHash:[jsonResponseBody objectForKey:@"email"] passwordHash:[jsonResponseBody objectForKey:@"password"] firstName:[jsonResponseBody objectForKey:@"name_first"]];
                    if (loggedInUser)
                    {
                        // Success!
						NSString *currentUserSWId = [SWHelper helperGetCurrentUserSWIdFromDefaults];
						if ([currentUserSWId length] > 0){
							[SWAnalytics setUserWithUserId:currentUserSWId];
						}
                        [Flurry logEvent:@"Sign In With Sickweather Account"];
						[self getFamilyMemberIdFromBackend];
                        [self.delegate signInWithSickweatherViewControllerDidDismissWithSuccessfulLogin:self];
                        /*
                        double delayInSeconds = 2.0;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            
            
                        });*/
					
                    }
                    else
                    {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"An Error Occurred" message:[NSString stringWithFormat:@"Please try again"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alertView show];
                        [SWHelper helperLogErrorToAppBackendForJSONResponseBody:jsonResponseBody responseBodyAsString:responseBodyAsString responseHeaders:responseHeaders requestError:requestError jsonParseError:jsonParseError requestURLAbsoluteString:requestURLAbsoluteString file:__FILE__ prettyFunction:__PRETTY_FUNCTION__ line:__LINE__];
                    }
                }
                else
                {
                    self.alertViewInvalidLoginCredentials = [[UIAlertView alloc] initWithTitle:@"Invalid Password" message:[NSString stringWithFormat:@"Please try again."] delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:@"Reset My Password", nil];
                    self.alertViewInvalidLoginCredentials.delegate = self;
                    [self.alertViewInvalidLoginCredentials show];
                    [SWHelper helperLogErrorToAppBackendForJSONResponseBody:jsonResponseBody responseBodyAsString:responseBodyAsString responseHeaders:responseHeaders requestError:requestError jsonParseError:jsonParseError requestURLAbsoluteString:requestURLAbsoluteString file:__FILE__ prettyFunction:__PRETTY_FUNCTION__ line:__LINE__];
                }
            });
        }];
    }
}

-(void)getFamilyMemberIdFromBackend{
	if ([SWUserCDM currentlyLoggedInUser]) {
		[[SWHelper helperAppBackend]appBackendGetFamilyIdUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
			if (jsonResponseBody){
				if ([jsonResponseBody valueForKey:@"family_id"] != nil && ![[jsonResponseBody valueForKey:@"family_id"] isEqual:[NSNull null]] && ![[jsonResponseBody valueForKey:@"family_id"] isEqualToString:@""]){
                    NSLog(@"family_id: %@", [jsonResponseBody valueForKey:@"family_id"]);
					[SWHelper setFamilyId:[jsonResponseBody valueForKey:@"family_id"]];
				}
			}
		}];
	}
}

- (void)forgotPasswordButtonTapped:(id)sender
{
    self.forgotPasswordAlert = [[UIAlertView alloc] initWithTitle:@"Reset Password" message:@"Please enter your email address" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reset", nil];
    self.forgotPasswordAlert.delegate = self;
    self.forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[self.forgotPasswordAlert textFieldAtIndex:0] setText:self.emailTextField.text];
    [self.forgotPasswordAlert show];
    /*
    // Reset Password
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.sickweather.com/signin/forgot.php?email=%@", [SWHelper helperURLEncode:self.emailTextField.text]]]];
     */
}

- (void)createSickweatherAccountButtonTapped:(id)sender
{
    // Log tap
    [Flurry logEvent:@"Create Account 1"];
    
    // Present modal
    SWCreateSickweatherAccountViewController *createSickweatherAccountViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWCreateSickweatherAccountViewController"];
    createSickweatherAccountViewController.delegate = self;
    [self presentViewController:createSickweatherAccountViewController animated:YES completion:nil];
}

- (void)signInWithFacebookButtonTapped:(id)sender
{
    [Flurry logEvent:@"Sign In With Facebook"];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)dismissSelf:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Helpers

- (void)issuePasswordResetPostRequestUsingCurrentEmail
{
    // Create request
    NSURL *url = [NSURL URLWithString:@"http://www.sickweather.com/signin/forgot.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"text/html" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSData *postData = [[NSString stringWithFormat:@"mySubmit=true&email=%@", [SWHelper helperURLEncode:self.emailTextField.text]] dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postData];
    
    // Execute request
    [self.activityIndicator startAnimating];
    NSURLSessionDataTask *postDataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            NSString *serverResponseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if ([serverResponseString rangeOfString:@"Sorry, we could not find that e-mail address in our system.  Please try again."].location != NSNotFound)
            {
                [SWHelper helperShowAlertWithTitle:@"Sorry, we could not find that e-mail address in our system.  Please try again."];
            }
            else
            {
                [SWHelper helperShowAlertWithTitle:@"Check your email for your new password."];
            }
        });
    }];
    [postDataTask resume];
}

#pragma mark - Keyboard Management Methods

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    self.keyboardIsComingUp = YES;
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    // Calculate the frame of the scrollview, in self.view's coordinate system
    UIScrollView *scrollView    = self.scrollView;
    CGRect scrollViewRect       = [self.view convertRect:scrollView.frame fromView:scrollView.superview];
    
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    
    // Figure out where the two frames overlap, and set the content offset of the scrollview appropriately
    CGRect hiddenScrollViewRect = CGRectIntersection(scrollViewRect, kbRect);
    if (!CGRectIsNull(hiddenScrollViewRect))
    {
        UIEdgeInsets contentInsets       = UIEdgeInsetsMake(0.0,  0.0, hiddenScrollViewRect.size.height,  0.0);
        scrollView.contentInset          = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }
    [self performSelector:@selector(scrollToActiveTextField) withObject:nil afterDelay:0.1]; // Delay is required for iOS to handle things as expected
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.scrollView.contentInset          = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)scrollToActiveTextField
{
    if (self.activeField)
    {
        CGRect visibleRect = self.activeField.frame;
        visibleRect        = [self.scrollView convertRect:visibleRect fromView:self.activeField.superview];
        visibleRect        = CGRectInset(visibleRect, 0.0f, -60.0f);
        [self.scrollView scrollRectToVisible:visibleRect animated:YES];
    }
}

// Register for notifications helper
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if ([textField isEqual:self.emailTextField])
    {
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.passwordTextField])
    {
        [self.passwordTextField resignFirstResponder];
        [self signInWithSickweatherButtonTapped:self];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
}

// Support only portrait
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // This code enables us to design our purple formContainer to be any size we need it to be, and the view above gets
    // its height based on device screen height and how much space we ate up using formContainer (which "pushes out" on
    // its edges)...
    
    //NSLog(@"self.formContainer.frame = %@", NSStringFromCGRect(self.formContainer.frame));
    if (self.formContainer.frame.size.height > 0)
    {
        self.topContainerHeightConstraint.constant = [UIScreen mainScreen].bounds.size.height - self.formContainer.frame.size.height;
        [self.view setNeedsDisplay];
    }
    self.scrollView.contentSize = self.contentView.frame.size;
}

#pragma mark - Lifecycle Methods

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
	
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.topContainer = [[UIView alloc] init];
    self.formContainer = [[UIView alloc] init];
    self.dismissButton = [[UIButton alloc] init];
    self.emailTextField = [[UITextField alloc] init];
    self.passwordTextField = [[UITextField alloc] init];
    self.signInWithSickweatherButton = [[UIButton alloc] init];
    self.forgotPasswordButton = [[UIButton alloc] init];
    self.createSickweatherAccountButton = [[UIButton alloc] init];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cloud-sickweather-going-around"]];
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_scrollView, _contentView, _topContainer, _formContainer, _dismissButton, _emailTextField, _passwordTextField, _signInWithSickweatherButton, _forgotPasswordButton, _createSickweatherAccountButton, _backgroundImageView, _activityIndicator);
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.topContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.formContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.passwordTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.signInWithSickweatherButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.createSickweatherAccountButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView]; // View adds scroll view
    [self.scrollView addSubview:self.contentView]; // Scroll view adds content view
    [self.contentView addSubview:self.topContainer]; // Content view adds top container
    [self.contentView addSubview:self.formContainer]; // Content view adds form container
    [self.topContainer addSubview:self.backgroundImageView]; // Top container adds image view
    [self.topContainer addSubview:self.dismissButton];
    [self.topContainer addSubview:self.activityIndicator];
    [self.formContainer addSubview:self.emailTextField]; // Form container holds everything else
    [self.formContainer addSubview:self.passwordTextField];
    [self.formContainer addSubview:self.signInWithSickweatherButton];
    [self.formContainer addSubview:self.forgotPasswordButton];
    [self.formContainer addSubview:self.createSickweatherAccountButton];
    
    // COLOR VIEWS
    /*
    self.view.backgroundColor = [UIColor redColor];
    self.scrollView.backgroundColor = [UIColor yellowColor];
    self.contentView.backgroundColor = [UIColor greenColor];
    self.topContainer.backgroundColor = [UIColor grayColor];
    self.formContainer.backgroundColor = [UIColor purpleColor];
    self.dismissButton.backgroundColor = [UIColor orangeColor];
    */
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_contentView]" options:0 metrics:0 views:viewsDictionary]];
    // Still laying out CONTENT VIEW (pin to self.view left and right, let height be based on what we put inside)
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:0
                                                                         toItem:self.view
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1.0
                                                                       constant:0];
    [self.view addConstraint:leftConstraint];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:0
                                                                          toItem:self.view
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1.0
                                                                        constant:0];
    [self.view addConstraint:rightConstraint];
    
    // Layout TOP CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_topContainer]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_topContainer]" options:0 metrics:0 views:viewsDictionary]];
    self.topContainerHeightConstraint = [NSLayoutConstraint constraintWithItem:self.topContainer
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:0
                                                                       attribute:0
                                                                      multiplier:1
                                                                        constant:0]; // We won't know this value until viewDidLayoutSubviews
    [self.view addConstraint:self.topContainerHeightConstraint];
    
    // Layout FORM CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_formContainer]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_topContainer][_formContainer]|" options:0 metrics:0 views:viewsDictionary]]; // <-- Anchor PUSH OUT constraint happens here
    
    // Layout TOP CONTAINER ITEMS
    
    // Layout IMAGE VIEW
    NSLayoutConstraint *backgroundImageViewCenterX = [NSLayoutConstraint constraintWithItem:self.backgroundImageView
                                                                                  attribute:NSLayoutAttributeCenterX
                                                                                  relatedBy:0
                                                                                     toItem:self.topContainer
                                                                                  attribute:NSLayoutAttributeCenterX
                                                                                 multiplier:1.0
                                                                                   constant:0];
    [self.view addConstraint:backgroundImageViewCenterX];
    NSLayoutConstraint *backgroundImageViewCenterY = [NSLayoutConstraint constraintWithItem:self.backgroundImageView
                                                                                  attribute:NSLayoutAttributeCenterY
                                                                                  relatedBy:0
                                                                                     toItem:self.topContainer
                                                                                  attribute:NSLayoutAttributeCenterY
                                                                                 multiplier:1.0
                                                                                   constant:0];
    [self.view addConstraint:backgroundImageViewCenterY];
    
    // Layout ACTIVITY INDICATOR
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.activityIndicator
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.topContainer
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1.0
                                                                constant:0];
    [self.view addConstraint:centerX];
    NSLayoutConstraint *topY = [NSLayoutConstraint constraintWithItem:self.activityIndicator
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.backgroundImageView
                                                               attribute:NSLayoutAttributeBottom
                                                              multiplier:1.0
                                                                constant:20];
    [self.view addConstraint:topY];
    
    // Layout DISMISS BUTTON
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:15]];
    
    // Layout FORM CONTAINER ITEMS
    
    // Layout EMAIL TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_emailTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(15)-[_emailTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout PASSWORD TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_passwordTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_emailTextField]-(10)-[_passwordTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout SIGN IN WITH SICKWEATHER BUTTON
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_signInWithSickweatherButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_passwordTextField]-(10)-[_signInWithSickweatherButton(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout FORGOT PASSWORD BUTTON
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_forgotPasswordButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_signInWithSickweatherButton]-(5)-[_forgotPasswordButton]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout CREATE SICKWEATHER ACCOUNT BUTTON
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_createSickweatherAccountButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_forgotPasswordButton]-(20)-[_createSickweatherAccountButton(40)]-(60)-|" options:0 metrics:0 views:viewsDictionary]]; // <-- Anchor PUSH OUT constraint happens here
    
    // CONFIG ALL VIEWS
    
    // Config VIEW
    
    // Add tap (for dismissing keyboard)
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = NO;
    
    // Config IMAGE VIEW
    self.backgroundImageView.contentMode = UIViewContentModeCenter;
    
    // Config EMAIL TEXT FIELD
    self.emailTextField.delegate = self;
    self.emailTextField.placeholder = @"Email";
    self.emailTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.emailTextField.enablesReturnKeyAutomatically = YES;
    self.emailTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.returnKeyType = UIReturnKeyNext;
    self.emailTextField.secureTextEntry = NO;
    self.emailTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.emailTextField.layer.borderWidth = 0.5;
    self.emailTextField.layer.cornerRadius = 5.0;
    self.emailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config PASSWORD TEXT FIELD
    self.passwordTextField.delegate = self;
    self.passwordTextField.placeholder = @"Password";
    self.passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.passwordTextField.enablesReturnKeyAutomatically = YES;
    self.passwordTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.passwordTextField.keyboardType = UIKeyboardTypeDefault;
    self.passwordTextField.returnKeyType = UIReturnKeyGo;
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.passwordTextField.layer.borderWidth = 0.5;
    self.passwordTextField.layer.cornerRadius = 5.0;
    self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config SIGN IN WITH SICKWEATHER BUTTON
    [self.signInWithSickweatherButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"SIGN IN" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}] forState:UIControlStateNormal];
    [self.signInWithSickweatherButton setBackgroundColor:[SWColor colorSickweatherBlue41x171x226]];
    [self.signInWithSickweatherButton.layer setBorderColor:[SWColor colorSickweatherBlue41x171x226].CGColor];
    [self.signInWithSickweatherButton.layer setBorderWidth:0.5];
    [self.signInWithSickweatherButton.layer setCornerRadius:2];
    [self.signInWithSickweatherButton addTarget:self action:@selector(signInWithSickweatherButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config FORGOT PASSWORD BUTTON
    [self.forgotPasswordButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Forgot password?" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226], NSFontAttributeName:[SWFont fontStandardFontOfSize:11]}] forState:UIControlStateNormal];
    self.forgotPasswordButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.forgotPasswordButton addTarget:self action:@selector(forgotPasswordButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config CREATE SICKWEATHER ACCOUNT BUTTON
    [self.createSickweatherAccountButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"CREATE SICKWEATHER ACCOUNT" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}] forState:UIControlStateNormal];
    [self.createSickweatherAccountButton setBackgroundColor:[SWColor colorSickweatherWhite255x255x255]];
    [self.createSickweatherAccountButton.layer setBorderColor:[SWColor colorSickweatherDarkGrayText104x104x104].CGColor];
    [self.createSickweatherAccountButton.layer setBorderWidth:0.5];
    [self.createSickweatherAccountButton.layer setCornerRadius:2];
    [self.createSickweatherAccountButton addTarget:self action:@selector(createSickweatherAccountButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config DISMISS BUTTON
    [self.dismissButton setImage:[UIImage imageNamed:@"close-icon"] forState:UIControlStateNormal];
    [self.dismissButton addTarget:self action:@selector(dismissSelf:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config KEYBOARD NOTIFICATIONS
    // https://developer.apple.com/library/ios/documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html
    [self registerForKeyboardNotifications];
    
    // Config ACTIVITY INDICATOR
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    //[self.activityIndicator startAnimating]; // For design time debugging...
}

@end
