//
//  SWConnectBluetoothDeviceTableViewController.m
//  Sickweather
//
//  Created by Muhammad Javed on 05/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWConnectBluetoothDeviceTableViewController.h"
#import "SWConnectBluetoothDeviceTableViewCell.h"
#import "SWConnectDeviceViewController.h"
#import "SWBuyDeviceViewController.h"

#define rowHeight 200
#define CELL_IDENTIFIER @"SWConnectBluetoothDeviceTableViewCell"

@interface SWConnectBluetoothDeviceTableViewController () <SWConnectBluetoothDeviceTableViewCellDelegate>

@end

@implementation SWConnectBluetoothDeviceTableViewController

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.delegate connectBluetoothDeviceViewControllerWantsToCancel:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWConnectBluetoothDeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    [cell setDeviceTitle:self.titleArray[indexPath.row]];
    [cell setDeviceImage:self.imageArray[indexPath.row]];
    if (indexPath.row == 0) {
        [cell setRecommendedBanner:YES];
    } else {
        [cell setRecommendedBanner:NO];
    }
    cell.delegate = self;
    cell.indexPath = indexPath;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKER SUPER
    [super viewDidLoad];
    // Config LEFT BAR BUTTON ITEM
    self.title = @"Select a Device";
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    self.view.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
    self.view.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    self.titleArray = @[ @"Swaive", @"Kinsa", @"Philips"];
    self.imageArray = @[ [UIImage imageNamed:@"thermometer-swaive"], [UIImage imageNamed:@"thermometer-kinsa"], [UIImage imageNamed:@"thermometer-philips"]];
    self.tableView.allowsSelection = NO;
}


- (void)connectBluetoothDeviceTableViewCellConnectButtonTapped:(NSIndexPath *)indexPath
{
    SWConnectDeviceViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWConnectDeviceViewController"];
    DeviceType type;
    switch (indexPath.row) {
        case 0:
			//Add Flurry Event
			[SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device > Swaive Connect Device button tapped"];
            type = swaive;
            break;
        case 1:
			//Add Flurry Event
			[SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device > Kinsa Connect Device button tapped"];
            type = kinsa;
            break;
        case 2:
			//Add Flurry Event
			[SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device > Phillips Connect Device button tapped"];
            type = philips;
            break;
        default:
            return;
            break;
    }
    vc.deviceType = type;
    vc.isFamilyMember = self.isFamilyMember;
    vc.familyMember = self.familyMember;
    [self.navigationController pushViewController:vc animated:YES ];
}
- (void)connectBluetoothDeviceTableViewCellBuyItButtonTapped:(NSIndexPath *)indexPath
{
    SWBuyDeviceViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWBuyDeviceViewController"];
    switch (indexPath.row) {
        case 0:
			//Add Flurry Event
			[SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device > Swaive Buy It button tapped"];
            vc.urlString = @"http://sick.io/swaive";
            vc.viewTitle = @"Buy your Swaive Device";
            break;
        case 1:
			//Add Flurry Event
			[SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device > Kinsa Buy It button tapped"];
            vc.urlString = @"http://sick.io/kinsa";
            vc.viewTitle = @"Buy your Kinsa Device";
            break;
        case 2:
			//Add Flurry Event
			[SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device > Phillips Buy It button tapped"];
            vc.urlString = @"http://sick.io/philips";
            vc.viewTitle = @"Buy your Philips Device";
            break;
        default:
            return;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
