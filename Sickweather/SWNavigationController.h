//
//  SWNavigationController.h
//  Sickweather
//
//  Created by John Erck on 11/13/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWNavigationController : UINavigationController

@end
