//
//  SWAddEventSuccessView.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAddEventSuccessView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@end
