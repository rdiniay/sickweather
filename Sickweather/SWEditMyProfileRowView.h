//
//  SWEditMyProfileRowView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWEditMyProfileRowView;

@protocol SWEditMyProfileRowViewDelegate
- (NSString *)editMyProfileRowViewTitleString:(SWEditMyProfileRowView *)sender;
- (NSString *)editMyProfileRowViewPlaceholderString:(SWEditMyProfileRowView *)sender;
- (UIImage *)editMyProfileRowViewIconImage:(SWEditMyProfileRowView *)sender;
- (void)editMyProfileRowWasTapped:(SWEditMyProfileRowView *)sender;
@end

@interface SWEditMyProfileRowView : UIView
@property (nonatomic, weak) id<SWEditMyProfileRowViewDelegate> delegate;
- (void)setRowTextFieldText:(NSString *)text;
- (void)showBottomBorder;
- (NSString *)currentText;
- (UITextField *)currentTextField;
- (void)addDisclosureIconAndTapFeature;
- (void)highlightNewlySelectedValue;
@end
