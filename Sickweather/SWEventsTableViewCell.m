//
//  SWEventsTableViewCell.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/6/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWEventsTableViewCell.h"
#import "SWColor.h"

@interface SWEventsTableViewCell ()
@property (nonatomic)BOOL isCelsius;
@end

@implementation SWEventsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)populateCellWith:(SWEventModal*)event {
	//green
	if ([event.trackType isKindOfClass:[NSString class]] && ([[event.trackType lowercaseString] containsString:@"symptom"] ||  [event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeSymptoms]])) {
		//black
		self.textViewType.text = @"Symptoms Reported";
		self.lblValue.text = event.trackerValue;
		self.typeCircleImgView.image = [UIImage imageNamed:@"event-circle-black"];
		self.typeCircleSubImgView.image = [UIImage imageNamed:@"event-icon-symptoms"];
	}else if ([event.trackType isKindOfClass:[NSString class]] && ([[event.trackType lowercaseString] containsString:@"temp"] || [event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeBluetoothTemperature]] || [event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeManualTemperature]])) {
		self.isCelsius = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants isCelsiusTemperatureTypeDefaultKey]];
		//orange
		if (event.trackType != nil && ![event isEqual:[NSNull null]]) {
			if ([[event.trackType lowercaseString] containsString:@"temp"]) {
				self.lblValue.text = [self getTempValue:event.trackerValue];
				self.textViewType.text = [event.trackType capitalizedString];
			}else {
				self.lblValue.text = event.trackerValue;
				if ([event.trackerValue isKindOfClass:[NSString class]] && ![[event.trackerValue lowercaseString] containsString:@"fever"] ){
					if ([event.trackerValue isKindOfClass:[NSString class]] && event.trackerValue != nil && ![event.trackerValue isEqual:[NSNull null]] && ![event.trackerValue isEqualToString:@""]) {
						self.lblValue.text = [NSString stringWithFormat:@"%@%@ (%@)",[self convertFarenhietToCelsiusString:[event.trackerValue floatValue]], self.isCelsius ? @"°C": @"°F", self.isCelsius ? [SWHelper updateFeverSeverityWithCelcius: [SWHelper convertFarenhietToCelsius:[event.trackerValue floatValue]]] : [SWHelper updateFeverSeverityWithFarenheit:[event.trackerValue floatValue]]];
					}
				}else{
					self.lblValue.text = [self getTempValue:event.trackerValue];
				}
				self.textViewType.text = @"Manual Temperature";
				if ([event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeBluetoothTemperature]]) {
					self.textViewType.text = @"Bluetooth Temperature";
				}
			}
		}
		self.typeCircleImgView.image = [UIImage imageNamed:@"event-circle-orange"];
		self.typeCircleSubImgView.image = [UIImage imageNamed:@"event-icon-temperature"];
	}else if ([event.trackType isKindOfClass:[NSString class]] && ([[event.trackType lowercaseString] containsString:@"(medications)"] || [event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeMdeication]])){
		if (event.trackType != nil && ![event isEqual:[NSNull null]]) {
			if([[event.trackType lowercaseString] containsString:@"(medications)"]){
				NSRange replaceRange = [[event.trackType lowercaseString] rangeOfString:@"(medications)"];
				if (replaceRange.location != NSNotFound){
					self.textViewType.text = event.trackerValue;
					self.lblValue.text = [event.trackType stringByReplacingCharactersInRange:replaceRange withString:@""];
				}
			}else {
				if (event.medication != nil) {
					self.lblValue.text = [event.medication.name capitalizedString];
					self.textViewType.text = [NSString stringWithFormat:@"%@ %@",event.medication.dose , event.medication.dose_unit];
					if (event.medication.notes != nil && ![event.medication.notes isEqual:[NSNull null]] && ![event.medication.notes isEqualToString:@""]) {
						self.textViewType.text = [NSString stringWithFormat:@"%@\n%@",self.textViewType.text,event.medication.notes];
					}
				}
				self.typeCircleSubImgView.image = [UIImage imageNamed:@"event-icon-medication"];
			}
		}
		self.typeCircleImgView.image = [UIImage imageNamed:@"event-circle-green"];
		
	}else if ([event.trackType isKindOfClass:[NSString class]] && ([[event.trackType lowercaseString] containsString:@"notes"] || [event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeNotes]])){
		self.textViewType.text = event.trackerValue;
		self.lblValue.text = @"Notes";
		self.typeCircleSubImgView.image = [UIImage imageNamed:@"event-icon-note"];
		self.typeCircleImgView.image = [UIImage imageNamed:@"event-circle-green"];
	}else if ([event.trackType isKindOfClass:[NSString class]] && ([[event.trackType lowercaseString] containsString:@"illness"] || [event.trackType isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeIllness]])){
		self.textViewType.text = event.locale;
		self.lblValue.text = event.illnessDisplayWord;
		self.typeCircleSubImgView.image = [UIImage imageNamed:@"event-icon-illness-report"];
		self.typeCircleImgView.image = [UIImage imageNamed:@"event-circle-orange"];
	}else{
		self.typeCircleImgView.image = [UIImage imageNamed:@"event-circle-orange"];
		self.textViewType.text = event.trackType;
		self.lblValue.text = event.trackerValue;
		self.typeCircleSubImgView.image = [UIImage imageNamed:@"event-icon-illness-report"];
	}
	[self changeCirlceImageColorOf:self.typeCircleSubImgView];
	self.lblCreatedAt.attributedText = [self parseDate:event.dateCreated];
}

#pragma mark - Change Image Color
-(void)changeCirlceImageColorOf:(UIImageView*)imageView{
	imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	[imageView setTintColor:[SWColor colorSickweatherBlue41x171x226]];
}

#pragma mark - Helper
-(NSMutableAttributedString*)parseDate:(NSString*)dateString{
	 NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	[formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	NSDate *date = [formatter dateFromString:dateString];
	BOOL today = [[NSCalendar currentCalendar] isDateInToday:date];
	if (today) {
		[formatter setDateFormat:@"h:mm a"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
		NSString * dateString = [formatter stringFromDate:date];
		NSArray * split = [dateString componentsSeparatedByString:@" "];
		NSMutableAttributedString *attributedConditionsString = [[NSMutableAttributedString alloc] initWithString:dateString];
		[attributedConditionsString addAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[SWFont fontStandardFontOfSize:15]} range:[dateString rangeOfString:[split objectAtIndex:0]]];
		if (split.count > 1) {
			[attributedConditionsString addAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],NSFontAttributeName:[SWFont fontStandardFontOfSize:15]} range:[dateString rangeOfString:[split objectAtIndex:1]]];
		}
		return attributedConditionsString;
	}else {
		[formatter setDateFormat:@"MM/dd"];
		return [[NSMutableAttributedString alloc] initWithString:[formatter stringFromDate:date]];
	}
}

-(NSString*)convertFarenhietToCelsiusString:(float)temp{
	NSNumberFormatter *twoDecimalPlacesFormatter = [[NSNumberFormatter alloc]init];
	[twoDecimalPlacesFormatter setMaximumFractionDigits:1];
	[twoDecimalPlacesFormatter setMinimumFractionDigits:0];
	return [NSString stringWithFormat:@"%@",[twoDecimalPlacesFormatter stringFromNumber:[NSNumber numberWithFloat: self.isCelsius ? [SWHelper convertFarenhietToCelsius:temp] : temp]]];
}

-(NSString*)getTempValue:(NSString*)temp{
	NSArray *array = [temp componentsSeparatedByString:@"°F"];
	if (array.count > 0) {
		NSMutableArray * tempArray = [NSMutableArray arrayWithArray:array];
		if (self.isCelsius) {
			NSString *convertedTemp = [self convertFarenhietToCelsiusString:[[array objectAtIndex:0] floatValue]];
			[tempArray replaceObjectAtIndex:0 withObject:convertedTemp];
			return [tempArray componentsJoinedByString:@""];
		}
		return temp;
	}
	return @"";
}

@end
