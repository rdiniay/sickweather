//
//  SWBlockTabWithLoginView.h
//  Sickweather
//
//  Created by John Erck on 12/20/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWBlockTabWithLoginViewDelegate
- (void)primaryButtonTapped:(id)sender;
@end

@interface SWBlockTabWithLoginView : UIView
@property (nonatomic, weak) id<SWBlockTabWithLoginViewDelegate> delegate;
- (void)updateBackgroundColor:(UIColor *)color;
- (void)updateImage:(UIImage *)image;
- (void)updateTitle:(NSString *)title;
- (void)updateDescription:(NSString *)description;
- (void)updateButtonTitle:(NSString *)title;
@end
