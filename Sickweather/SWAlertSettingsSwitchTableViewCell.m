//
//  SWAlertSettingsSwitchTableViewCell.m
//  Sickweather
//
//  Created by John Erck on 12/19/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWAlertSettingsSwitchTableViewCell.h"

@interface SWAlertSettingsSwitchTableViewCell ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UISwitch *mySwitch;
@property (strong, nonatomic) UILabel *myLabel;
@end

@implementation SWAlertSettingsSwitchTableViewCell

#pragma mark - Public Methods

- (void)updateForDict:(NSDictionary *)dict
{
    NSDictionary *attributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]};
    if ([[dict objectForKey:@"name"] isKindOfClass:[NSString class]])
    {
        self.myLabel.attributedText = [[NSAttributedString alloc] initWithString:[dict objectForKey:@"name"] attributes:attributes];
    }
}

- (void)styleAsActive
{
    self.mySwitch.enabled = YES;
}

- (void)styleAsInactive
{
    self.mySwitch.enabled = NO;
}

- (void)styleAsOn
{
    self.mySwitch.on = YES;
}

- (void)styleAsOff
{
    self.mySwitch.on = NO;
}

#pragma mark - Target/Action

-(void)cellTapped:(UITapGestureRecognizer*) tapGes
{
    if (!self.mySwitch.enabled) {
        return;
    }
    if (!self.mySwitch.on)
    {
        [self.mySwitch setOn:YES animated:YES];
        [self.delegate mySwitchFlippedOn:self];
    }
    else
    {
        [self.mySwitch setOn:NO animated:YES];
        [self.delegate mySwitchFlippedOff:self];
    }
}

- (void)switchStateChanged:(id)sender
{
    if ([sender isKindOfClass:[UISwitch class]])
    {
        UISwitch *mySwitch = sender;
        //NSLog(@"mySwitch = %@", mySwitch);
        if (mySwitch.on)
        {
            [self.delegate mySwitchFlippedOn:self];
        }
        else
        {
            [self.delegate mySwitchFlippedOff:self];
        }
    }
}

#pragma mark - Life Cycle Methods

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.mySwitch = [[UISwitch alloc] init];
        self.myLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.mySwitch forKey:@"mySwitch"];
        [self.viewsDictionary setObject:self.myLabel forKey:@"myLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.mySwitch.translatesAutoresizingMaskIntoConstraints = NO;
        self.myLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.mySwitch];
        [self.contentView addSubview:self.myLabel];
        
        // LAYOUT
        
        // contentView
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[mySwitch]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.mySwitch attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // myLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[mySwitch]-(10)-[myLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.myLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // CONFIG
        
        // contentView
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapped:)];
        [self.contentView setUserInteractionEnabled:YES];
        [self.contentView addGestureRecognizer:tapGes];
        
        // mySwitch
        [self.mySwitch addTarget:self action:@selector(switchStateChanged:) forControlEvents:UIControlEventValueChanged];
        
        // myLabel
//        [self.myLabel setUserInteractionEnabled:YES];
//        [self.myLabel addGestureRecognizer:tapGes];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

