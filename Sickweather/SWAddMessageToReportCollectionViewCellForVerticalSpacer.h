//
//  SWAddMessageToReportCollectionViewCellForVerticalSpacer.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWAddMessageToReportCollectionViewCellForVerticalSpacer;

@protocol SWAddMessageToReportCollectionViewCellForVerticalSpacerDelegate
@end

@interface SWAddMessageToReportCollectionViewCellForVerticalSpacer : UICollectionViewCell
@property (nonatomic, weak) id<SWAddMessageToReportCollectionViewCellForVerticalSpacerDelegate> delegate;
@end
