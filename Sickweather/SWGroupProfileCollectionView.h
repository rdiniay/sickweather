//
//  SWGroupProfileCollectionView.h
//  Sickweather
//
//  Created by John Erck on 1/21/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWGroupProfileCollectionView : UICollectionView

@end
