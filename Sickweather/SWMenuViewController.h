//
//  SWMenuViewController.h
//  Sickweather
//
//  Created by John Erck on 8/21/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ACCOUNT_SETTINGS_SECTION_HEADER_INDEX 0

@interface SWMenuViewController : UIViewController
- (void)loadGroupAndPresentProfilePageForSWFoursquareGroupId:(NSString *)swFoursquareGroupId;
@end
