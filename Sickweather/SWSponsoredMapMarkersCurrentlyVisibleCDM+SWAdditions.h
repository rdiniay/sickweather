//
//  SWSponsoredMapMarkersCurrentlyVisibleCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 12/27/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMapMarkersCurrentlyVisibleCDM.h"

@interface SWSponsoredMapMarkersCurrentlyVisibleCDM (SWAdditions)
+ (NSArray *)sponsoredMapMarkersCurrentlyVisibleGetAll;
+ (void)sponsoredMapMarkersCurrentlyVisibleResetAs:(NSArray *)annotationsInVisibleMapRect;
@end
