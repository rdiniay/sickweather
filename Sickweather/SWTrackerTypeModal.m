//
//  SWTrackerTypeModal.m
//  Sickweather
//
//  Created by Shan Shafiq on 2/12/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWTrackerTypeModal.h"

@implementation SWTrackerTypeModal
- (instancetype)init
{
	self = [super init];
	return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if (self) {
		if ([NSNull null] != [dictionary valueForKey:@"id"]) self.id = [dictionary valueForKey:@"id"]; else self.id =  @"";
		if ([NSNull null] != [dictionary valueForKey:@"name"]) self.name = [dictionary valueForKey:@"description"]; else self.name = @"";
	}
	return self;
}
@end
