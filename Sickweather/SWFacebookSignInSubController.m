//
//  SWFacebookSignInSubController.m
//  Sickweather
//
//  Created by John Erck on 3/31/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//
@import SWAnalytics;
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SWFacebookSignInSubController.h"

@interface SWFacebookSignInSubController () <UIAlertViewDelegate>
@property (strong, nonatomic) UIAlertView *alertViewTakeMeToSettings;
@property (strong, nonatomic) UIAlertView *alertViewOK;
@end

@implementation SWFacebookSignInSubController

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:self.alertViewTakeMeToSettings])
    {
        if ([alertView isEqual:self.alertViewTakeMeToSettings])
        {
            if (buttonIndex == 0) // OK
            {
                // Do nothing
            }
            else if (buttonIndex == 1) // Take Me to Settings
            {
                if (UIApplicationOpenSettingsURLString)
                {
                    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    [[UIApplication sharedApplication] openURL:appSettings];
                }
                else
                {
                    // Do nothing
                }
            }
            else
            {
                // Do nothing
            }
        }
    }
    else // OK
    {
        // Do nothing
    }
}

#pragma mark - Public Interface

- (void)loginUsingCompletionHandler:(SWFacebookSignInSubControllerLoginCompletionHandler)completionHandler;
{
    // If your app asks for more than public_profile, email and user_friends, Facebook must review it before you release it. See Review Guidelines.
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email",@"user_friends"] fromViewController:self.presentingViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            //NSString *failureReason = [error.userInfo objectForKey:@"com.facebook.sdk:ErrorLoginFailedReason"];
            if ([SWHelper helperIsIOS8OrHigher])
            {
                // Link them to settings app
                self.alertViewTakeMeToSettings = [[UIAlertView alloc] initWithTitle:@"Facebook Login Error" message:@"Please try again. If this issue persists, make sure Sickweather has access to your Facebook account via the Settings App." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go To Settings", nil];
                self.alertViewTakeMeToSettings.delegate = self;
                [self.alertViewTakeMeToSettings show];
            }
            else
            {
                self.alertViewOK = [[UIAlertView alloc] initWithTitle:@"Facebook Login Error" message:@"Please try again. If this issue persists, make sure Sickweather has access to your Facebook account via the Settings App." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [self.alertViewOK show];
            }
            completionHandler(nil);
        }
        else if (result.isCancelled)
        {
            // Handle cancellations
            self.alertViewOK = [[UIAlertView alloc] initWithTitle:@"Facebook Login Cancelled" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [self.alertViewOK show];
            completionHandler(nil);
        }
        else
        {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions are missing
            if ([result.grantedPermissions containsObject:@"email"])
            {
                // Sign into sickweather
                [[SWHelper helperAppBackend] signIntoSickweatherUsingFacebookAccessToken:[FBSDKAccessToken currentAccessToken].tokenString usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                        // Check for success
                        if ([[jsonResponseBody objectForKey:@"connect_status"] isEqualToString:@"success"])
                        {
                            // Core data access must happen on main thread as we are doing here in this main thread block
                            SWUserCDM *loggedInUser = [SWUserCDM createOrGetAsUpdatedLoggedInUserUsingUserId:[jsonResponseBody objectForKey:@"user_id"] emailHash:[jsonResponseBody objectForKey:@"email"] passwordHash:[jsonResponseBody objectForKey:@"password"] firstName:[jsonResponseBody objectForKey:@"name_first"]];
                            if (loggedInUser)
                            {
								NSString *currentUserSWId = [SWHelper helperGetCurrentUserSWIdFromDefaults];
								if ([currentUserSWId length] > 0){
									[SWAnalytics setUserWithUserId:currentUserSWId];
								}
                                // Success!
                                completionHandler(loggedInUser);
                            }
                            else
                            {
                                self.alertViewOK = [[UIAlertView alloc] initWithTitle:@"Core Data Error" message:@"Please try again. If this problem persists, please contact us or try reinstalling the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                [self.alertViewOK show];
                                completionHandler(nil);
                            }
                        }
                        else
                        {
                            self.alertViewOK = [[UIAlertView alloc] initWithTitle:@"Web Service Error" message:@"Please try again. If this problem persists, please contact us." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [self.alertViewOK show];
                            completionHandler(nil);
                        }
                    });
                }];
            }
            else
            {
                self.alertViewOK = [[UIAlertView alloc] initWithTitle:@"Missing Email Error" message:@"To sign in using Facebook we need access to your email address. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [self.alertViewOK show];
                completionHandler(nil);
            }
        }
    }];
}

@end
