//
//  SWProfilePicFlowCollectionViewCell.h
//  Sickweather
//
//  Created by John Erck on 1/20/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWGroupProfileCollectionViewCellForProfilePic : UICollectionViewCell
- (void)groupProfileCollectionViewCellForProfilePicSetImage:(UIImage *)image;
@end
