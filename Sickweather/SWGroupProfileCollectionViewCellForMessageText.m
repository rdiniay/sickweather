//
//  SWGroupProfileCollectionViewCellForMessageText.m
//  Sickweather
//
//  Created by John Erck on 1/22/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForMessageText.h"

@interface SWGroupProfileCollectionViewCellForMessageText ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UITextView *textView;
@end

@implementation SWGroupProfileCollectionViewCellForMessageText

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForMessageTextSetMessageTextAttributedString:(NSAttributedString *)text;
{
    self.textView.attributedText = text;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.textView = [[UITextView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.textView forKey:@"textView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.textView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.textView];
        
        // LAYOUT
        
        // Layout TEXT VIEW
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[textView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[textView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config TEXT VIEW
        self.textView.scrollEnabled = NO;
        self.textView.scrollsToTop = NO;
        self.textView.editable = NO;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.textView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
