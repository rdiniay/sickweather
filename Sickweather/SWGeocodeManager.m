//
//  SWGeocodeManager.m
//  Sickweather
//
//  Created by John Erck on 5/24/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWGeocodeManager.h"

@interface SWGeocodeManager ()
@property (strong, nonatomic) CLGeocoder *geocoder;
@property (assign) NSTimeInterval lastRequestTime;
@property (assign) NSTimeInterval nowRequestTime;
@end

@implementation SWGeocodeManager

- (void)reverseGeocodeLocation:(CLLocation *)location completionHandler:(CLGeocodeCompletionHandler)completionHandler
{
    self.nowRequestTime = [[NSDate date] timeIntervalSince1970];
    if (self.nowRequestTime - self.lastRequestTime > 2)
    {
        SWDLog(@"timeDiff = %f", self.nowRequestTime - self.lastRequestTime);
        self.lastRequestTime = self.nowRequestTime;
        [self.geocoder reverseGeocodeLocation:location completionHandler:completionHandler];
    }
    else
    {
        //NSLog(@"Skipped call to reverseGeocodeLocation");
        [self.geocoder reverseGeocodeLocation:location completionHandler:completionHandler];
    }
}

- (void)geocodeAddressString:(NSString *)addressString completionHandler:(CLGeocodeCompletionHandler)completionHandler
{
    self.nowRequestTime = [[NSDate date] timeIntervalSince1970];
    if (self.nowRequestTime - self.lastRequestTime > 2)
    {
        SWDLog(@"timeDiff = %f", self.nowRequestTime - self.lastRequestTime);
        self.lastRequestTime = self.nowRequestTime;
        [self.geocoder geocodeAddressString:addressString completionHandler:completionHandler];
    }
}

- (id)initWithGeocoder:(CLGeocoder *)geocoder
{
    self = [super init];
    if (self)
    {
        self.lastRequestTime = 0;
        self.geocoder = geocoder;
    }
    return self;
}

@end
