//
//  SWGroupsViewController.m
//  Sickweather
//
//  Created by John Erck on 8/21/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "Flurry.h"
#import "SWGroupsViewController.h"
#import "SWGroupsViewControllerCollectionViewLayout.h"
#import "SWGroupsViewControllerStandardItemCollectionViewCell.h"
#import "SWGroupsViewControllerStandardSectionHeaderCollectionReusableView.h"
#import "SWNavigationController.h"
#import "SWAboutViewController.h"
#import "SWGroupProfileViewController.h"
#import "SWMyProfileViewController.h"
#import "SWShareAppWithFriendsTableViewController.h"
#import "SWCreateGroupViewController.h"
#import "SWBlockTabWithLoginView.h"

@interface SWGroupsViewController () <SWGroupsViewControllerStandardItemCollectionViewCellDelegate, SWGroupsViewControllerCollectionViewLayoutDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SWSignInWithFacebookViewControllerDelegate, SWAboutViewControllerDelegate, SWGroupProfileViewControllerDelegate, SWMyProfileViewControllerDelegate, SWShareAppWithFriendsTableViewControllerDelegate, SWGroupsViewControllerStandardSectionHeaderCollectionReusableViewDelegate, SWCreateGroupViewControllerDelegate, SWBlockTabWithLoginViewDelegate, UITabBarControllerDelegate>
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) SWBlockTabWithLoginView *blockTabWithLoginView;
@property (strong, nonatomic) SWGroupsViewControllerCollectionViewLayout *customCollectionViewLayout;
@property (strong, nonatomic) NSMutableDictionary *groupsData; // groups.json
@property (assign) BOOL isPresentingFullScreenModalAndWantsStatusBarHidden;
@property (strong, nonatomic) NSString *currentSearchStringText;
@property (strong, nonatomic) SWGroupsViewControllerStandardSectionHeaderCollectionReusableView *currentSearchHeaderView;
@property (strong, nonatomic) NSString *currentMyGroupsSectionDefaultRecordTitle;
@property (strong, nonatomic) NSDictionary *latestServerResponse;
@end

@implementation SWGroupsViewController

#pragma mark - Public Methods

- (void)loadGroupAndPresentProfilePageForSWFoursquareGroupId:(NSString *)swFoursquareGroupId
{
    //NSLog(@"swFoursquareGroupId = %@", swFoursquareGroupId);
    SWGroupProfileViewController *groupProfileController = [[SWGroupProfileViewController alloc] init];
    groupProfileController.delegate = self;
    groupProfileController.serverGroupDict = @{};
    groupProfileController.latestMenuServerResponse = @{};
    SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:groupProfileController];
    [self presentViewController:navVC animated:YES completion:^{}];
    [[SWHelper helperAppBackend] appBackendGetGroupProfileUsingArgs:@{@"foursquare_id":swFoursquareGroupId,@"start_index":@"0",@"per_page":@"10"} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"responseBodyAsString = %@, requestURLAbsoluteString = %@", responseBodyAsString, requestURLAbsoluteString);
            [groupProfileController setThenLoadGetGroupProfileJSONObject:(NSMutableDictionary *)jsonResponseBody];
        });
    }];
}

- (void)loadGroupAndPushProfilePageForSWFoursquareGroupId:(NSString *)swFoursquareGroupId{
	SWGroupProfileViewController *groupProfile = [[SWGroupProfileViewController alloc] init];
	groupProfile.delegate = self;
	[groupProfile loadForFoursquareId:swFoursquareGroupId];
	[self.navigationController pushViewController:groupProfile animated:YES];
}

#pragma mark - SWBlockTabWithLoginViewDelegate

- (void)primaryButtonTapped:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWSignInWithFacebookViewController *signInWithFacebookViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
    signInWithFacebookViewController.delegate = self;
    UINavigationController *signInWithFacebookNavigationController = [[UINavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
    [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
}

#pragma mark - Custom Getters/Setters

- (NSString *)currentMyGroupsSectionDefaultRecordTitle
{
    // INIT DEFAULTS
    _currentMyGroupsSectionDefaultRecordTitle = @"Log in to start following groups...";
    SWUserCDM *loggedInUser = [SWUserCDM currentlyLoggedInUser];
    if ([loggedInUser isKindOfClass:[SWUserCDM class]])
    {
        if (self.currentSearchStringText.length > 0)
        {
            _currentMyGroupsSectionDefaultRecordTitle = [NSString stringWithFormat:@"None of your groups match \"%@\"...", self.currentSearchStringText];
        }
        else
        {
            if (self.latestServerResponse)
            {
                _currentMyGroupsSectionDefaultRecordTitle = @"You aren't following any groups yet...";
            }
            else
            {
                _currentMyGroupsSectionDefaultRecordTitle = @"Loading...";
            }
        }
    }
    return _currentMyGroupsSectionDefaultRecordTitle;
}

#pragma mark - Static Constants

static NSString * const groupsViewControllerStandardItemCollectionViewCellReuseIdentifier = @"groupsViewControllerStandardItemCollectionViewCellReuseIdentifier";
static NSString * const groupsViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier = @"groupsViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier";
//static NSString * const footerViewReuseIdentifier = @"FooterView";

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
    {
        if ([SWUserCDM currentlyLoggedInUser])
        {
            [self.currentSearchHeaderView setFocusOnSearch];
        }
        else
        {
            [self helperLaunchLoginFlow];
        }
    }
    else if ([self helperIsEitherTypeOfGroupSection:indexPath])
    {
        if ([SWUserCDM currentlyLoggedInUser])
        {
            NSDictionary *serverGroupDict = [[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.row];
            //[SWHelper helperShowAlertWithTitle:[cellDataDict objectForKey:@"title"] message:[NSString stringWithFormat:@"serverGroupDict = %@", serverGroupDict]];
            SWGroupProfileViewController *groupProfileController = [[SWGroupProfileViewController alloc] init];
            groupProfileController.delegate = self;
            groupProfileController.serverGroupDict = serverGroupDict;
            groupProfileController.latestMenuServerResponse = self.latestServerResponse;
            if ([self helperIsMyGroupsSection:indexPath])
            {
                groupProfileController.setAsFollowing = YES;
            }
            [self.navigationController pushViewController:groupProfileController animated:YES];
            /*
            SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:groupProfileController];
            [self presentViewController:navVC animated:YES completion:^{}];
             */
        }
        else
        {
            [self helperLaunchLoginFlow];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSUInteger itemsCount = 0;
    NSInteger index = 0;
    for (NSDictionary *sectionDict in [[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"])
    {
        if (index == section)
        {
            itemsCount = [[sectionDict objectForKey:@"rows"] count];
            break;
        }
        index++;
    }
    return itemsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:groupsViewControllerStandardItemCollectionViewCellReuseIdentifier forIndexPath:indexPath];
    if ([cell isKindOfClass:[SWGroupsViewControllerStandardItemCollectionViewCell class]])
    {
        SWGroupsViewControllerStandardItemCollectionViewCell *standardItemCell = (SWGroupsViewControllerStandardItemCollectionViewCell *)cell;
        standardItemCell.delegate = self;
        [standardItemCell setTitleAttributedString:[self titleAttributedStringForIndexPath:indexPath]];
        if ([self helperIsEitherTypeOfGroupSection:indexPath])
        {
            [standardItemCell hideIconImageView];
            [standardItemCell setAddressAttributedString:[self addressAttributedStringForIndexPath:indexPath]];
            if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
            {
                [standardItemCell styleTitleForInfoMessage];
            }
        }
        else
        {
            [standardItemCell hideIconImageView];
        }
    }
    //cell.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        SWGroupsViewControllerStandardSectionHeaderCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:groupsViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier forIndexPath:indexPath];
        headerView.delegate = self;
        [headerView prepareForReuse];
        if ([self helperIsSearchSection:indexPath])
        {
            self.currentSearchHeaderView = headerView;
            headerView.styleStringKey = @"searchSectionStyle";
            [headerView setCurrentSearchStringText:self.currentSearchStringText];
        }
        else if ([self helperIsEitherTypeOfGroupSection:indexPath])
        {
            headerView.styleStringKey = @"subSectionStyle";
            if ([self helperIsMyGroupsSection:indexPath])
            {
                [headerView showCreateNewGroupButton];
            }
        }
        else
        {
            headerView.styleStringKey = @"regularSectionStyle";
        }
        [headerView setTitle:[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"title"]];
        reusableview = headerView;
    }
    if (kind == UICollectionElementKindSectionFooter)
    {
        /*
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerViewReuseIdentifier forIndexPath:indexPath];
        reusableview = footerview;
         */
    }
    return reusableview;
}

#pragma mark - SWMyProfileViewControllerDelegate

- (void)myProfileViewControllerWantsToPopOffTheStack:(SWMyProfileViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateLoginRelatedRecords]; // The user could have logged out
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - SWSignInWithFacebookViewControllerDelegate

- (void)didDismissWithSuccessfulLogin:(SWSignInWithFacebookViewController *)vc
{
    self.blockTabWithLoginView.hidden = YES;
    self.isPresentingFullScreenModalAndWantsStatusBarHidden = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"Back to groups");
        [self helperUpdateLoginRelatedRecords];
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

- (void)didDismissWithoutLoggingIn:(SWSignInWithFacebookViewController *)vc
{
    self.isPresentingFullScreenModalAndWantsStatusBarHidden = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"Back to groups");
        [self helperUpdateLoginRelatedRecords];
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - SWGroupsViewControllerCollectionViewLayoutDelegate

- (NSAttributedString *)groupsViewControllerCollectionViewLayoutDelegateAttributedTitleStringForStandardItemCollectionViewCell:(NSIndexPath *)indexPath
{
    return [self titleAttributedStringForIndexPath:indexPath];
}

- (CGFloat)sectionHeaderPreferredVerticalHeightForIndexPath:(NSIndexPath *)indexPath
{
    if ([self helperIsEitherTypeOfGroupSection:indexPath])
    {
        return 36;
    }
    else
    {
        return 50;
    }
}

- (CGFloat)totalExtraTitleMarginNeededForIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *titleLeftMargin = @(50);
    NSNumber *titleRightMargin = @(16); /* THESE VALUES NEED TO BE IN SYNC WITH PARTNER LAYOUT FILE'S SETTINGS */
    titleLeftMargin = @(10);
    if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
    {
        titleRightMargin = @(0);
    }
    return titleLeftMargin.floatValue + titleRightMargin.floatValue;
}

#pragma mark - SWGroupsViewControllerStandardItemCollectionViewCellDelegate

- (void)groupsViewControllerStandardItemCollectionViewCellDelegateExampleAction1:(SWGroupsViewControllerStandardItemCollectionViewCell *)cell
{
    //NSLog(@"groupsViewControllerStandardItemCollectionViewCellDelegateExampleAction1");
}

#pragma mark - SWShareAppWithFriendsTableViewControllerDelegate

- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulMessageSend:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismissBySkipping:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismiss:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}


#pragma mark - SWGroupsViewControllerStandardSectionHeaderCollectionReusableViewDelegate

- (void)searchBarTextDidChange:(NSString *)searchText
{
    NSLog(@"searchText = %@", searchText);
    [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;
{
    // Log to Flurry
   [SWHelper logFlurryEventsWithTag:@"Search All Goups, Search Bar Activated"];
}

- (void)createNewGroupButtonTapped:(SWGroupsViewControllerStandardSectionHeaderCollectionReusableView *)sender
{
    // Log to Flurry
	[SWHelper logFlurryEventsWithTag:@"Create Group button tapped"];
    if ([SWUserCDM currentlyLoggedInUser])
    {
        SWCreateGroupViewController *vc = [[SWCreateGroupViewController alloc] init];
        vc.delegate = self;
        SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navVC animated:YES completion:^{}];
    }
    else
    {
        [self helperLaunchLoginFlow];
    }
}

#pragma mark - SWCreateGroupViewControllerDelegate

- (void)createGroupViewControllerWantsToDismissDueToUserCancel:(SWCreateGroupViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)createGroupViewControllerWantsToDismissAfterSuccessfullyCreatingNewGroup:(SWCreateGroupViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - SWAboutViewControllerDelegate

- (void)aboutViewControllerWantsToDismiss
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWGroupProfileViewControllerDelegate

- (void)groupProfileViewControllerWantsToDismiss
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - Helpers

- (void)loadJSONData
{
    self.groupsData = (NSMutableDictionary *)[SWHelper helperLoadJSONObjectFromBundleDocsUsingName:@"groups" forceFeedFromBundle:YES];
    [self helperUpdateLoginRelatedRecords];
}

- (void)helperUpdateLoginRelatedRecords
{
    
}

- (NSMutableArray *)helperRowsInSection:(NSIndexPath *)indexPath
{
    NSMutableArray *rowsInSection = [[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"];
    return rowsInSection;
}

- (void)helperLaunchLoginFlow
{
    [Flurry logEvent:@"Groups - Log In/Create Account Button Tapped"];
    self.isPresentingFullScreenModalAndWantsStatusBarHidden = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    SWSignInWithFacebookViewController *signInWithFacebookViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
    signInWithFacebookViewController.delegate = self;
    SWNavigationController *signInWithFacebookNavigationController = [[SWNavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
    [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
}

- (NSAttributedString *)titleAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    NSAttributedString *answer;
    if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
    {
        NSDictionary *dict = [[self helperRowsInSection:indexPath] objectAtIndex:indexPath.item];
        [dict setValue:self.currentMyGroupsSectionDefaultRecordTitle forKey:@"title"];
        answer = [[NSAttributedString alloc] initWithString:[[[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"title"] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
    }
    else
    {
        NSString *title = [[[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"title"];
        if ([title isEqualToString:@"Loading..."])
        {
            answer = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
        }
        else
        {
            answer = [[NSAttributedString alloc] initWithString:[[[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"title"] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226]}];
        }
    }
    return answer;
}

- (NSAttributedString *)addressAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"[[[[[[self.groupsData objectForKey:@\"groups\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] objectForKey:@\"rows\"] objectAtIndex:indexPath.item] objectForKey:@\"address\"] = %@", [[[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"address"]);
    //NSLog(@"[[[[[self.groupsData objectForKey:@\"groups\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] objectForKey:@\"rows\"] objectAtIndex:indexPath.item] = %@", [[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item]);
    //NSLog(@"[[[[self.groupsData objectForKey:@\"groups\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] objectForKey:@\"rows\"] = %@", [[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"]);
    //NSLog(@"[[[self.groupsData objectForKey:@\"groups\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] = %@", [[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section]);
    //NSLog(@"[[self.groupsData objectForKey:@\"groups\"] objectForKey:@\"sections\"] = %@", [[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"]);
    //NSLog(@"[self.groupsData objectForKey:@\"groups\"] = %@", [self.groupsData objectForKey:@"groups"]);
    //NSLog(@"self.groupsData = %@", self.groupsData);
    NSDictionary *object = [[[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item];
    NSString *address = [object objectForKey:@"address"];
    if (!address || [address isKindOfClass:[NSNull class]])
    {
        address = @"";
    }
    else
    {
        /*
         // If we had lat/lon per group we could do this...
        CLLocation *currentLocation = [SWHelper helperLocationManager].location;
        CLLocation *markerLocation = [[CLLocation alloc] initWithLatitude:[[annotation.serverDict objectForKey:@"lat"] doubleValue] longitude:[[annotation.serverDict objectForKey:@"lon"] doubleValue]];
        CLLocationDistance distance = [currentLocation distanceFromLocation:markerLocation];
        NSString *distanceFromCurrentLocation = [NSString stringWithFormat:@"%.1f mi",(distance/1609.344)];
        //NSLog(@"Calculated Miles %@", distanceFromCurrentLocation);
        [popupView setDistanceFromCurrentLocation:distanceFromCurrentLocation];
        */
    }
    return [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:10],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
}

- (BOOL)helperIsSearchSection:(NSIndexPath *)indexPath
{
    return indexPath.section == SEARCH_SECTION_HEADER_INDEX;
}

- (BOOL)helperIsEitherTypeOfGroupSection:(NSIndexPath *)indexPath
{
    return indexPath.section == MY_GROUPS_SECTION_HEADER_INDEX || indexPath.section == GROUPS_AROUND_YOU_SECTION_HEADER_INDEX;
}

- (BOOL)helperIsMyGroupOrNearMeGroupRecord:(NSIndexPath *)indexPath
{
    if ([self helperIsEitherTypeOfGroupSection:indexPath])
    {
        if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return NO;
    }
}

- (BOOL)helperIsMyGroupsSection:(NSIndexPath *)indexPath
{
    return indexPath.section == MY_GROUPS_SECTION_HEADER_INDEX;
}

- (BOOL)helperIsDefaultEmptyMyGroupsRecord:(NSIndexPath *)indexPath
{
    BOOL answer = NO;
    NSArray *rowsInSection = [self helperRowsInSection:indexPath];
    if ([self helperIsMyGroupsSection:indexPath] && rowsInSection.count == 1)
    {
        NSDictionary *dict = [rowsInSection objectAtIndex:indexPath.item];
        NSString *title = [dict objectForKey:@"title"];
        if (title && ([title isEqualToString:@""] || [title isEqualToString:self.currentMyGroupsSectionDefaultRecordTitle]))
        {
            answer = YES;
        }
    }
    return answer;
}

- (void)helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:(NSString *)searchText
{
    // Get groups data...
    if (![searchText isKindOfClass:[NSString class]])
    {
        searchText = self.currentSearchStringText;
    }
    self.currentSearchStringText = searchText;
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    if (currentUser)
    {
        //NSString *city = [currentUser userLastKnownCurrentLocationPlacemarkObject].locality; // e.g. Huntington Beach
        //NSString *state = [currentUser userLastKnownCurrentLocationPlacemarkObject].administrativeArea; // e.g. CA
        NSString *lat = [currentUser userLastKnownCurrentLocationLatitude].stringValue;
        NSString *lon = [currentUser userLastKnownCurrentLocationLongitude].stringValue;
        if (/*(city && state) || (*/lat && lon/*)*/)
        {
            /*
             api_key - Your API key
             location - city, state location for search (optional when my_groups = 1)
             lat - latitude location parameter (can be used instead of or in addition to location)
             lon - longitude location parameter (can be used instead of or in addition to location)
             radius - search radius in km (optional when my_groups = 1)
             per_page - number of results to return per page (defaults to 10)
             start_index - pagination start index (defaults to 0)
             
             Optional Inputs
             search - venue or group keyword(s), searches group name
             my_groups = 1 - only includes groups in which user is a member
             ehash - user’s email MD5 hash (user authentication required for my_groups=1)
             phash - user’s password MD5 hash
             user_id - user’s unique ID
             */
            [[SWHelper helperAppBackend] appBackendGetGroupsUsingArgs:@{/*@"location":[NSString stringWithFormat:@"%@, %@", city, state], */@"lat":lat, @"lon":lon, @"search":searchText, @"radius":@"50", @"per_page":@"50", @"offset":@"0"} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    self.latestServerResponse = jsonResponseBody;
                    NSArray *groupsMatchingSearch = [jsonResponseBody objectForKey:@"groups"];
                    if (groupsMatchingSearch)
                    {
                        NSMutableArray *modifiedGroups = [NSMutableArray new];
                        for (NSDictionary *group in groupsMatchingSearch)
                        {
                            NSMutableDictionary *modifiedGroup = [group mutableCopy];
                            [modifiedGroup setObject:[group objectForKey:@"name"] forKey:@"title"];
                            [modifiedGroups addObject:modifiedGroup];
                            if ([searchText isEqualToString:@""] && modifiedGroups.count >= 3)
                            {
                                break;
                            }
                        }
                        [[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:GROUPS_AROUND_YOU_SECTION_HEADER_INDEX] setObject:modifiedGroups forKey:@"rows"];
                    }
                    NSArray *myGroupsMatchingSearch = [[jsonResponseBody objectForKey:@"my_groups"] objectForKey:@"groups"];
                    NSMutableArray *modifiedGroups = [NSMutableArray new];
                    if (myGroupsMatchingSearch)
                    {
                        for (NSDictionary *group in myGroupsMatchingSearch)
                        {
                            NSMutableDictionary *modifiedGroup = [group mutableCopy];
                            [modifiedGroup setObject:[group objectForKey:@"name"] forKey:@"title"];
                            [modifiedGroups addObject:modifiedGroup];
                        }
                        if (modifiedGroups.count == 0)
                        {
                            [modifiedGroups addObject:[@{@"title": @"", @"address": @""} mutableCopy]]; // Default which will show special message to user then
                        }
                        [[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:MY_GROUPS_SECTION_HEADER_INDEX] setObject:modifiedGroups forKey:@"rows"];
                    }
                    else
                    {
                        [modifiedGroups addObject:[@{@"title": @"", @"address": @""} mutableCopy]]; // Default which will show special message to user then
                        [[[[self.groupsData objectForKey:@"groups"] objectForKey:@"sections"] objectAtIndex:MY_GROUPS_SECTION_HEADER_INDEX] setObject:modifiedGroups forKey:@"rows"];
                    }
                    [self.collectionView reloadSections:[[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(1, 2)]];
                });
            }];
        }
        else
        {
            [[SWHelper helperAppDelegate] appDelegateUpdateUserLaskKnownCurrentLocationPlacemarkUsingBestMethodAvailableFinishingWithDidUpdateCurrentLocationPlacemarkNotfification];
        }
    }
}

#pragma mark - Public Methods

#pragma mark - UITabBarControllerDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if ([viewController isEqual:[tabBarController.viewControllers objectAtIndex:4]])
    {
        if ([self.navigationController.topViewController isEqual:self])
        {
            // JUST SCROLL UP (WITH ANIMATION)
            [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        }
        else
        {
            // POP (WITH ANIMATION) BACK TO ALREADY SCROLLED UP COLLECTION VIEW
            [self.navigationController popToRootViewControllerAnimated:YES];
            [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        }
    }
}

#pragma mark - Life Cycle Methods

- (BOOL)prefersStatusBarHidden
{
    if (self.isPresentingFullScreenModalAndWantsStatusBarHidden == YES)
    {
        return YES;
    }
    if (self.view.bounds.size.width > self.view.bounds.size.height)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([SWHelper helperUserIsLoggedIn])
    {
        self.blockTabWithLoginView.hidden = YES;
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:@""];
    }
    else
    {
        self.blockTabWithLoginView.hidden = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view endEditing:YES];
    });
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSWDidUpdateCurrentLocationPlacemarkNotification object:nil];
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // Tell us when the user's current location has been translated to a placemark object (if ever, permissions, etc)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:)
                                                 name:kSWDidUpdateCurrentLocationPlacemarkNotification
                                               object:nil];
    
    // LOAD JSON DATA
    [self loadJSONData];
    
    // PRE LOAD GROUPS DATA
    [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:@""];
    
    // PRE INIT
    self.customCollectionViewLayout = [[SWGroupsViewControllerCollectionViewLayout alloc] init];
    self.customCollectionViewLayout.delegate = self;
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.customCollectionViewLayout];
    self.blockTabWithLoginView = [[SWBlockTabWithLoginView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.collectionView forKey:@"collectionView"];
    [self.viewsDictionary setObject:self.blockTabWithLoginView forKey:@"blockTabWithLoginView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.blockTabWithLoginView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.blockTabWithLoginView];
    
    // LAYOUT
    
    // Layout COLLECTION VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout blockTabWithLoginView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[blockTabWithLoginView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[blockTabWithLoginView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config NAVIGATION BAR
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.title = @"Groups";
    
    // Config TAB BAR
    self.tabBarController.delegate = self;
    
    // Config collectionView
    self.collectionView.delegate = self;
    [self.collectionView registerClass:[SWGroupsViewControllerStandardItemCollectionViewCell class] forCellWithReuseIdentifier:groupsViewControllerStandardItemCollectionViewCellReuseIdentifier];
    [self.collectionView registerClass:[SWGroupsViewControllerStandardSectionHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:groupsViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier];
    //    [self.collectionView registerClass:[SWChecklistCollectionReusableViewFooter class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:footerViewReuseIdentifier];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    // Config blockTabWithLoginView
    [self.blockTabWithLoginView updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.blockTabWithLoginView updateImage:[UIImage imageNamed:@"onboarding-icon-groups"]];
    [self.blockTabWithLoginView updateTitle:@"Sickweather Groups"];
    [self.blockTabWithLoginView updateDescription:@"Log in to stay in the know about what's going on at your local schools, public spaces, and other areas nearby."];
    [self.blockTabWithLoginView updateButtonTitle:@"Login"];
    self.blockTabWithLoginView.delegate = self;
    
    // COLOR VIEWS
}

@end
