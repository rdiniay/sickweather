//
//  SWEventModal.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/6/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWEventModal.h"

@implementation SWEventModal : NSObject
- (instancetype)init
{
	self = [super init];
	return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if (self) {
		if ([NSNull null] != [dictionary valueForKey:@"id"]) self.id = [dictionary valueForKey:@"id"]; else self.id = @"";
		if ([NSNull null] != [dictionary valueForKey:@"user_id"]) self.userId = [dictionary valueForKey:@"user_id"]; else self.userId = @"";
		if ([NSNull null] != [dictionary valueForKey:@"member_id"]) self.memberId = [dictionary valueForKey:@"member_id"]; else self.memberId = @"";
		if ([NSNull null] != [dictionary valueForKey:@"member_name"]) self.memberName = [dictionary valueForKey:@"member_name"]; else self.memberName = @"";
		if ([NSNull null] != [dictionary valueForKey:@"tracker_value"]) self.trackerValue = [dictionary valueForKey:@"tracker_value"]; else self.trackerValue = @"";
		if ([NSNull null] != [dictionary valueForKey:@"tracker_type"]) self.trackType = [dictionary valueForKey:@"tracker_type"]; else self.trackType = @"";
		if ([NSNull null] != [dictionary valueForKey:@"created_by"]) self.createdBy = [dictionary valueForKey:@"created_by"]; else self.createdBy = @"";
		if ([NSNull null] != [dictionary valueForKey:@"date_created"]) self.dateCreated = [dictionary valueForKey:@"date_created"]; else self.dateCreated = @"";
		if ([NSNull null] != [dictionary valueForKey:@"lastmod_by"]) self.lastModifiedBy = [dictionary valueForKey:@"lastmod_by"]; else self.lastModifiedBy = @"";
		if ([NSNull null] != [dictionary valueForKey:@"locale"]) self.locale = [dictionary valueForKey:@"locale"]; else self.locale = @"";
		if ([NSNull null] != [dictionary valueForKey:@"display_word"]) self.illnessDisplayWord = [dictionary valueForKey:@"display_word"]; else self.illnessDisplayWord = @"";
		if ([NSNull null] != [dictionary valueForKey:@"date_lastmod"]) self.dateLastModified = [dictionary valueForKey:@"date_lastmod"]; else self.dateLastModified = @"";
		NSDictionary *medication = [dictionary objectForKey:@"medication"];
		if (medication && [dictionary isKindOfClass:[NSDictionary class]]) {
			self.medication = [[SWMedicationModal alloc]initWithDictionary:medication];
		}else{
			self.medication = nil;
		}
	}
	return self;
}

@end
