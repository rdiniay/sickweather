//
//  SWMenuViewControllerStandardItemCollectionViewCell.h
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMenuViewControllerStandardItemCollectionViewCell;

@protocol SWMenuViewControllerStandardItemCollectionViewCellDelegate
- (void)menuViewControllerStandardItemCollectionViewCellDelegateExampleAction1:(SWMenuViewControllerStandardItemCollectionViewCell *)cell;
@end

@interface SWMenuViewControllerStandardItemCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) id<SWMenuViewControllerStandardItemCollectionViewCellDelegate> delegate;
- (void)setIconImageName:(NSString *)imageName;
- (void)setTitleAttributedString:(NSAttributedString *)titleAttributedString;
- (void)setAddressAttributedString:(NSAttributedString *)addressAttributedString;
- (void)showTopBorder;
- (void)hideIconImageView;
- (void)styleTitleForInfoMessage;
@end
