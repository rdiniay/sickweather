//
//  SWAboutViewController.h
//  Sickweather
//
//  Created by John Erck on 11/16/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWAboutViewControllerDelegate <NSObject>
- (void)aboutViewControllerWantsToDismiss;
@end

@interface SWAboutViewController : UIViewController
@property (nonatomic, assign) id <SWAboutViewControllerDelegate> delegate;
@end
