//
//  SWHomePageAddView.h
//  Sickweather
//
//  Created by John Erck on 7/26/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWHomePageAddViewDelegate
- (void)homePageAddViewAddedLocation:(id)sender;
- (void)homePageAddViewDeletedLocation:(id)sender;
- (void)homePageAddViewReorderedLocations:(id)sender;
- (void)duplicationPlaceError:(NSString*)message;
@end

@interface SWHomePageAddView : UIView
@property (nonatomic, weak) id<SWHomePageAddViewDelegate> delegate;
- (void)activateKeyboard;
@end
