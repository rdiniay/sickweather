//
//  SWReportCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWReportCDM.h"
#import "SWUserCDM.h"
#import "SWIllnessCDM.h"
#import <CoreLocation/CoreLocation.h>

@interface SWReportCDM (SWAdditions)
+ (SWReportCDM *)logReportForUser:(SWUserCDM *)user forIllness:(SWIllnessCDM *)illness atLocation:(CLLocationCoordinate2D)location inManagedObjectContext:(NSManagedObjectContext *)context;
+ (SWReportCDM *)getUpdatedReportForServerReportDict:(NSDictionary *)serverReportDict;
+ (NSArray *)getAllReports; // of SWReportCDM
+ (void)reportDeleteRecordWithId:(NSNumber *)reportId;
@end
