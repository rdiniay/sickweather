//
//  SWWevViewViewController.h
//  Sickweather
//
//  Created by John Erck on 4/18/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWWebViewViewControllerDelegate <NSObject>
- (void)webViewViewControllerScreenWantsToDismiss:(id)sender;
@end

@interface SWWebViewViewController : UIViewController
@property (nonatomic, assign) id <SWWebViewViewControllerDelegate> delegate;
- (void)webViewViewControllerLoadURL:(NSURL *)url;
@end
