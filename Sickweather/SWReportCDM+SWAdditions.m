//
//  SWReportCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWReportCDM+SWAdditions.h"
#import "SWUserCDM+SWAdditions.h"
#import "SWIllnessCDM+SWAdditions.h"

@implementation SWReportCDM (SWAdditions)

+ (SWReportCDM *)logReportForUser:(SWUserCDM *)user forIllness:(SWIllnessCDM *)illness atLocation:(CLLocationCoordinate2D)location inManagedObjectContext:(NSManagedObjectContext *)context
{
    // Create new
    SWReportCDM *report = [NSEntityDescription insertNewObjectForEntityForName:@"SWReportCDM" inManagedObjectContext:context];
    report.owner = user;
    report.illness = illness;
    report.latitude = [NSNumber numberWithDouble:location.latitude];
    report.longitude = [NSNumber numberWithDouble:location.longitude];
    report.reportDate = [NSDate new];
    if (report.illness == nil)
    {
        //NSLog(@"report.illness == nil WHY?!");
    }
    return report;
}

+ (SWReportCDM *)getUpdatedReportForServerReportDict:(NSDictionary *)serverReportDict
{
    // Define return var
    SWReportCDM *report = nil;
    
    // Create and config database request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWReportCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"postedTimestampUTC" ascending:NO]];
    request.predicate = [NSPredicate predicateWithFormat:@"reportId = %@", [serverReportDict objectForKey:@"id"]];
    
    // Execute fetch request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    if (!matches || [matches count] > 1)
    {
        // Error
    }
    else if ([matches count] == 1)
    {
        // Use existing, after updating
        report = [matches lastObject];
    }
    else
    {
        // Create new
        report = [NSEntityDescription insertNewObjectForEntityForName:@"SWReportCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
    }
    
    // Update values
    if (report)
    {
        // Cast to specific types
        NSString *reportId = [serverReportDict objectForKey:@"id"];
        NSString *illnessId = [serverReportDict objectForKey:@"illness_id"];
        //NSString *userId = [serverReportDict objectForKey:@"user_id"]; // Available but not needed here
        NSString *feedText = [serverReportDict objectForKey:@"feed_text"];
        NSString *lat = [serverReportDict objectForKey:@"lat"];
        NSString *lon = [serverReportDict objectForKey:@"lon"];
        NSString *postedTime = [serverReportDict objectForKey:@"posted_time"];
        
        NSString *serverDateFormat = @"yyyy-MM-dd H:mm:ss"; // e.g. 2016-02-16 19:04:47
        //NSLog(@"postedTime = %@", postedTime);
        postedTime = [[serverReportDict objectForKey:@"posted_time"] stringByAppendingString:@" EST"]; // I add this
        //NSLog(@"postedTime = %@", postedTime);
        //NSLog(@"serverDateFormat = %@", serverDateFormat);
        serverDateFormat = [serverDateFormat stringByAppendingString:@" zzz"]; // I add this
        //NSLog(@"serverDateFormat = %@", serverDateFormat);
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = serverDateFormat;
        fmt.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
        NSDate *postedDate = [fmt dateFromString:postedTime];
        
        // Assign as updated props
        report.reportId = reportId;
        report.latitude = [NSNumber numberWithDouble:[lat doubleValue]];
        report.longitude = [NSNumber numberWithDouble:[lon doubleValue]];
        report.reportDate = postedDate;
        report.feedText = feedText;
        report.postedTimestampUTC = [NSString stringWithFormat:@"%f", [postedDate timeIntervalSince1970]];
        report.illness = [SWIllnessCDM illnessGetRecordUsingId:illnessId];
        report.owner = [SWUserCDM currentUser];
    }
    
    // Check for nil here due to sickweather backend response dict illness_id = <null> on occassion...
    if (report.illness == nil)
    {
        // Well then delete it so we don't corrupt the DB and prevent future saves!!
        [[SWHelper helperManagedObjectContext] deleteObject:report];
        NSError *saveError = nil;
        [[SWHelper helperManagedObjectContext] save:&saveError];
        report = nil;
        if (saveError)
        {
            //NSLog(@"saveError = %@", saveError);
        }
    }
    
    // Return object
    return report;
}

+ (NSArray *)getAllReports
{
    // Prevent crash
    if ([SWHelper helperManagedObjectContext])
    {
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"SWReportCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique != %@", @"myReports"];
        //[request setPredicate:predicate];
        
        //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
        //[request setSortDescriptors:@[sortDescriptor]];
        
        NSError *error;
        return [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    }
    return @[];
}

+ (void)reportDeleteRecordWithId:(NSNumber *)reportId
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"SWReportCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]]];
    request.predicate = [NSPredicate predicateWithFormat:@"reportId = %@", reportId];
    [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError *error = nil;
    NSArray *records = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    for (NSManagedObject *reportRecord in records)
    {
        [[SWHelper helperManagedObjectContext] deleteObject:reportRecord];
    }
    if (records.count == 0)
    {
        //NSLog(@"Huh? records = %@ reportId = %@", records, reportId);
        //NSArray *allReports = [SWReportCDM getAllReports];
        //NSLog(@"allReports = %@", allReports);
    }
    NSError *saveError = nil;
    [[SWHelper helperManagedObjectContext] save:&saveError];
    if (saveError)
    {
        //NSLog(@"saveError = %@", saveError);
    }
}

@end
