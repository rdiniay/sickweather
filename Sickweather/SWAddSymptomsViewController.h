//
//  SWAddSymptomsViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"
#import "Sickweather-Swift.h"

@interface SWAddSymptomsViewController : UIViewController <UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL isFromBlueToothTempScreen;
@property (nonatomic) BOOL isFromFamilyScreen;
@property (nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@property (strong, nonatomic) NSString *temp;
@property (strong , nonatomic) NSString* severity;
@end
