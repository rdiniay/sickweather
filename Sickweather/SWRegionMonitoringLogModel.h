//
//  SWRegionMonitoringLogModel.h
//  Sickweather
//
//  Created by John Erck on 11/3/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SWRegionMonitoringLogModel : NSObject

+ (SWRegionMonitoringLogModel *)sharedInstance;

@property (strong, nonatomic) NSDate *lastLocationRefreshTime;
@property (strong, nonatomic) CLLocation *lastLocationThatTriggeredUpdate;
@property (strong, nonatomic) NSString *lastLoadedCommaSeparatedGeofenceIllnessIDs;
@property (strong, nonatomic) UILocalNotification *localNotificationOpenedFromTerminatedState;

@end
