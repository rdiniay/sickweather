//
//  THERMConnection.h
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/13/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol THERMConnectionDelegate <NSObject>

- (void)deviceConnection:(CBPeripheral *)peripheral;
- (void)deviceDisconnection:(CBPeripheral *)peripheral;
- (void)deviceDiscovered:(CBPeripheral *)peripheral;
- (void)getBatteryLevel:(int)value;

- (void)getDeviceTemperatureScale:(NSString *)scale;
- (void)getTemperature:(float)temperatureValue scale:(NSString *)temperatureScale date:(NSDate *)temperatureTimestamp deviceDiscovered:(CBPeripheral *)peripheral;

@end

@interface THERMConnection : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate> {
    id <THERMConnectionDelegate> _delegate;
}

@property (nonatomic, strong)   id <THERMConnectionDelegate> delegate;
@property (readonly, nonatomic) CBCentralManager *bluetoothManager;
@property (readonly, nonatomic) NSMutableArray *peripherals;

+ (THERMConnection *)sharedManager;
- (void)initialize;
- (void)connectTHERM:(CBPeripheral *)peripheral;
- (void)disconnectTHERM:(CBPeripheral *)peripheral;
- (NSString *)setTemperatureScale:(int)scale;


@end
