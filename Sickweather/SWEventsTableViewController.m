//
//  SWEventsTableViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/5/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWEventsTableViewController.h"
#import "SWEventsTableViewCell.h"
#import "SWHelper.h"
#import "SWEventModal.h"
#import "SWAddNewEventTableViewController.h"
#import "SWAddEventSuccessView.h"
#import "SWFamilyProfileViewController.h"
#import "SWEmptyTableViewCell.h"

// 
@interface SWEventsTableViewController ()
@property (strong, nonatomic) NSMutableArray *eventsData;
@property (nonatomic) BOOL isloaded;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation SWEventsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Register nib
	[self.tableView registerNib:[UINib nibWithNibName:@"SWEmptyTableViewCell" bundle:nil] forCellReuseIdentifier:@"SWEmptyTableViewCell"];
	//Init
	self.eventsData = [[NSMutableArray alloc] init];
	// Spinner shown during load the TableView
	self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	// Ovveride refresh control
	UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
	[refreshControl addTarget:self action:@selector(refreshEventsFeed) forControlEvents:UIControlEventValueChanged];
	self.refreshControl = refreshControl;
	// Config LEFT BAR BUTTON ITEM
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	[self setLoading];
	//Refresh List
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(refreshEvents:)
												 name:kSWRefreshEventsNotification
											   object:nil];
    
    if(self.isFamilyMember){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Profile" style:UIBarButtonItemStylePlain target:self action:@selector(familyMemberProfileTapped:)];
    }
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 90.0;
	[self getEventsUsingWebService];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	self.title = self.familyMember != nil ? self.familyMember.name_first : [SWUserCDM currentUser].firstName;
    if (@available(iOS 11.0, *)) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSWRefreshEventsNotification object:nil];
}

#pragma mark - MISC

-(void)setLoading{
	[self.activityIndicator startAnimating];
	self.activityIndicator.hidesWhenStopped = true;
	CGPoint point = self.view.center;
	point.y -= 44;
	self.activityIndicator.center = point;
	[self.view addSubview:self.activityIndicator];
	[self.view bringSubviewToFront:self.activityIndicator];
}

-(void)removeLoading{
	if ([self.activityIndicator isAnimating]) {
		[self.activityIndicator stopAnimating];
	}
}

-(void)reloadData{
	NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:self.tableView]);
	NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
	[self.tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Refresh Events Feed

-(void)refreshEvents:(id)sender{
	NSNotification *notification = (NSNotification*)sender;
	NSDictionary *userInfo = notification.userInfo;
	self.showSuccessHeader = true;
	self.successMessage = [userInfo objectForKey:@"message"];
	[self.tableView reloadData];
	[self setLoading];
	[self getEventsUsingWebService];
}

- (void) refreshEventsFeed {
	self.showSuccessHeader = false;
	[self getEventsUsingWebService];
}

#pragma mark - ApiCall

-(void)getEventsUsingWebService{
	[[SWHelper helperAppBackend] appBackendGetEventsUsingArgs:(self.familyMember != nil)? @{@"member_id":self.familyMember.member_id} : @{@"member_id":@""} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
			self.isloaded = YES;
            NSArray *events = [jsonResponseBody objectForKey:@"results"];
            [self.eventsData removeAllObjects];
            for (NSDictionary *event in events){
                SWEventModal * eventObj = [[SWEventModal alloc] initWithDictionary:event];
				if ([eventObj.trackType isKindOfClass:[NSString class]] && ![eventObj.trackType isEqual:[NSNull null]] && ![eventObj.trackType isEqualToString:@""]) {
//					if (self.familyMember == nil) {
//						if (eventObj.memberId == nil  || [eventObj.memberId isEqual:[NSNull null]] || [eventObj.memberId isEqualToString:@""] || [eventObj.memberId isEqualToString:@"0"]) {
//							[self.eventsData addObject:eventObj];
//						}
//					}else{
						[self.eventsData addObject:eventObj];
					//}
				}
            }
            if (self.refreshControl.isRefreshing) {
                [self.refreshControl endRefreshing];
                [self.tableView reloadData];
            }else {
                [self reloadData];
            }
            [self removeLoading];
            
        });
    }];
}

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	[self.navigationController popViewControllerAnimated:true];
}

-(void)addNewButtonPressed:(UIButton*)sender{
	SWAddNewEventTableViewController *addNewEventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddNewEventTableViewController"];
	addNewEventsTableViewController.isFromEventsScreen = true;
    if(self.isFamilyMember){
        addNewEventsTableViewController.isFamilyMember = YES;
        addNewEventsTableViewController.familyMember = self.familyMember;
    }
    [self.navigationController pushViewController:addNewEventsTableViewController animated:YES];
}

- (void)familyMemberProfileTapped:(id)sender
{
    if(self.isFamilyMember){
        // Family Member profile
        SWFamilyProfileViewController *vc = [[SWFamilyProfileViewController alloc] init];
        vc.familyMember = self.familyMember;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return section == 0  ? 1  : self.eventsData.count == 0 ? 1:  self.eventsData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (indexPath.section == 0) {
		static NSString *CellIdentifier = @"SWAddNewEventCell";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		UIButton *addNewEventBtn = (UIButton*) [cell viewWithTag:100];
		[addNewEventBtn addTarget:self action:@selector(addNewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
		return cell;
	}
	if (indexPath.section == 1 && self.eventsData.count == 0){
		static NSString *CellIdentifier = @"SWEmptyTableViewCell";
		SWEmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		[cell.lblText setHidden:!self.isloaded];
		return cell;
	}
	
	static NSString *CellIdentifier = @"SWEventsTableViewCell";
	SWEventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	SWEventModal * eventobj = (SWEventModal*)[self.eventsData objectAtIndex:indexPath.row];
	[cell populateCellWith:eventobj];
	return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	if (section == 0 && self.showSuccessHeader) {
		NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SWAddEventSuccessView" owner:self options:nil];
		SWAddEventSuccessView *headerView = [nibArray objectAtIndex:0];
		headerView.lblText.text = [NSString stringWithFormat:@"We recorded your new %@ for %@",self.successMessage, self.familyMember != nil ? self.familyMember.name_first : [SWUserCDM currentUser].firstName];
		return headerView;
	}
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (indexPath.section == 0) {
		return 100.0;
	}else if (indexPath.section == 1 && self.eventsData.count == 0){
		return [[UIScreen mainScreen]bounds].size.height - 250;
	}
	return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return section == 0 && self.showSuccessHeader? 120.0 : 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	// remove bottom extra 20px space.
	return CGFLOAT_MIN;
}

@end
