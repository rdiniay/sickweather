//
//  SWMapKitAnnotation.h
//  Sickweather
//
//  Created by John Erck on 5/1/17.
//  Copyright (c) 2017 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SWPrivateReportMapKitAnnotation : NSObject <MKAnnotation>
@property (nonatomic, readonly, copy) NSString *identifier;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, readonly, copy) NSString *typeId;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSDate *reportDate;
@property (nonatomic, strong) NSDictionary *report;
@property (nonatomic, assign) BOOL isACurrentGeofenceToo;
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate identifier:(NSString *)identifier title:(NSString *)title subtitle:(NSString *)subtitle typeId:(NSString *)typeId;
@end
