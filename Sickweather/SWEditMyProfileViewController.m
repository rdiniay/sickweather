//
//  SWEditMyProfileViewController.m
//  Sickweather
//
//  Created by John Erck on 9/28/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

//#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "SWEditMyProfileViewController.h"
#import "SWEditMyProfileHeaderView.h"
#import "SWEditMyProfileSectionHeaderView.h"
#import "SWEditMyProfileRowView.h"
#import "SWSelectItemFromListViewController.h"
#import "SWCaptureUserDefinedAddressViewController.h"
#import "SWDatePicker.h"
#import "NSString+FontAwesome.h"
#import "Flurry.h"

#define rowHeight 50
#define DATE_FORMAT_FOR_UI @"MMMM d, yyyy"
#define DATE_FORMAT_FROM_SERVER @"MM/dd/yyyy"

@interface SWEditMyProfileViewController () <SWEditMyProfileRowViewDelegate, SWSelectItemFromListViewControllerDelegate, SWEditMyProfileHeaderViewDelegate, SWEditMyProfileSectionHeaderViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, SWDatePickerDelegate, SWCaptureUserDefinedAddressViewControllerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) UITextField *activeField;
@property (weak, nonatomic) SWEditMyProfileRowView *activeRow;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *youSectionHeader;
@property (strong, nonatomic) SWEditMyProfileHeaderView *headerView;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *usernameSectionHeader;
@property (strong, nonatomic) SWEditMyProfileRowView *emailRow;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *contactSectionHeader;
@property (strong, nonatomic) SWEditMyProfileRowView *usernameRow;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *aboutSectionHeader;
@property (strong, nonatomic) SWEditMyProfileRowView *genderRow;
@property (strong, nonatomic) SWEditMyProfileRowView *raceEthnicityRow;
@property (strong, nonatomic) SWEditMyProfileRowView *birthdayRow;
@property (strong, nonatomic) SWEditMyProfileRowView *locationRow;
@property (strong, nonatomic) SWEditMyProfileRowView *healthCarePreferenceRow;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSMutableDictionary *imageForStringURL;
@property (strong, nonatomic) NSMutableArray *stringURLCurrentlyLoading;
@property (strong, nonatomic) SWDatePicker *datePicker;
@property (strong, nonatomic) NSLayoutConstraint *datePickerVerticalHeightConstraint;
@property (strong, nonatomic) NSString *firstNameValueToSubmitToServer;
@property (strong, nonatomic) NSString *lastNameValueToSubmitToServer;
@property (strong, nonatomic) NSString *usernameValueToSubmitToServer;
@property (strong, nonatomic) NSString *emailValueToSubmitToServer;
@property (strong, nonatomic) NSString *genderValueToSubmitToServer;
@property (strong, nonatomic) NSString *raceEthnicityValueToSubmitToServer;
@property (strong, nonatomic) NSDate *birthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *dayBirthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *monthBirthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *yearBirthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *cityLocationValueToSubmitToServer;
@property (strong, nonatomic) NSString *stateLocationValueToSubmitToServer;
@property (strong, nonatomic) NSString *countryLocationValueToSubmitToServer;
@property (strong, nonatomic) NSString *healthCarePrefValueToSubmitToServer;
@property (strong, nonatomic) UIImage *usersNewProfilePicImage;
@property (strong, nonatomic) UIPopoverController *popOver;
@end

@implementation SWEditMyProfileViewController

#pragma mark Keyboard Management Methods (call register from viewDidLoad)

// Register for notifications helper
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //NSLog(@"keyboardWillShow");
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    // Calculate the frame of the scrollview, in self.view's coordinate system
    UIScrollView *scrollView    = self.scrollView;
    CGRect scrollViewRect       = [self.view convertRect:scrollView.frame fromView:scrollView.superview];
    
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    
    // Figure out where the two frames overlap, and set the content offset of the scrollview appropriately
    CGRect hiddenScrollViewRect = CGRectIntersection(scrollViewRect, kbRect);
    if (!CGRectIsNull(hiddenScrollViewRect))
    {
        UIEdgeInsets contentInsets       = UIEdgeInsetsMake(0.0,  0.0, hiddenScrollViewRect.size.height,  0.0);
        scrollView.contentInset          = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }
    [self performSelector:@selector(scrollToActiveTextField) withObject:nil afterDelay:0.1]; // Delay is required for iOS to handle things as expected
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.scrollView.contentInset          = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)scrollToActiveTextField
{
    if (self.activeField)
    {
        CGRect visibleRect = self.activeField.frame;
        visibleRect        = [self.scrollView convertRect:visibleRect fromView:self.activeField.superview];
        visibleRect        = CGRectInset(visibleRect, 0.0f, -60.0f);
        [self.scrollView scrollRectToVisible:visibleRect animated:YES];
    }
}

#pragma mark - UITextFieldDelegate (part of Keyboard Management Methods)

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if ([textField isEqual:self.headerView.firstNameTextField])
    {
        [self.headerView.firstNameTextField resignFirstResponder];
        [self.headerView.lastNameTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.headerView.lastNameTextField])
    {
        [self.headerView.lastNameTextField resignFirstResponder];
        [[self.usernameRow currentTextField] becomeFirstResponder];
    }
    if ([textField isEqual:[self.usernameRow currentTextField]])
    {
        [[self.usernameRow currentTextField] resignFirstResponder];
        [[self.emailRow currentTextField] becomeFirstResponder];
    }
    if ([textField isEqual:[self.emailRow currentTextField]])
    {
        [[self.emailRow currentTextField] resignFirstResponder];
        [self editMyProfileRowWasTapped:self.genderRow];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self hideDatePicker];
}

#pragma mark - SWDatePickerDelegate

- (void)datePickerWantsToCancel:(SWDatePicker *)sender
{
    [self hideDatePicker];
}

- (void)datePickerWantsToDone:(SWDatePicker *)sender
{
    [self hideDatePicker];
    self.birthdayValueToSubmitToServer = sender.datePicker.date;
    NSDateFormatter *dayFormat = [[NSDateFormatter alloc] init];
    [dayFormat setDateFormat:@"dd"];
    self.dayBirthdayValueToSubmitToServer = [dayFormat stringFromDate:sender.datePicker.date];
    NSDateFormatter *monthFormat = [[NSDateFormatter alloc] init];
    [monthFormat setDateFormat:@"MM"];
    self.monthBirthdayValueToSubmitToServer = [monthFormat stringFromDate:sender.datePicker.date];
    NSDateFormatter *yearFormat = [[NSDateFormatter alloc] init];
    [yearFormat setDateFormat:@"yyyy"];
    self.yearBirthdayValueToSubmitToServer = [yearFormat stringFromDate:sender.datePicker.date];
    NSDate *myDate = sender.datePicker.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMAT_FOR_UI];
    NSString *prettyVersion = [dateFormat stringFromDate:myDate];
    [self.activeRow setRowTextFieldText:prettyVersion];
    [self.activeRow highlightNewlySelectedValue];
}

#pragma mark - UIScrollViewDelegate

#pragma mark - SWEditMyProfileRowViewDelegate

- (NSString *)editMyProfileRowViewTitleString:(SWEditMyProfileRowView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.usernameRow])
    {
        title = [self userDataUsernameString];
    }
    else if ([sender isEqual:self.emailRow])
    {
        title = [self userDataEmailString];
    }
    else if ([sender isEqual:self.genderRow])
    {
        title = [self userDataGenderFormattedString];
    }
    else if ([sender isEqual:self.raceEthnicityRow])
    {
        title = [self userDataRaceEthnicityFormattedString];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        title = [self userDataDateOfBirthFormattedString];
    }
    else if ([sender isEqual:self.locationRow])
    {
        title = [self userDataLocationString];
    }
    else if ([sender isEqual:self.healthCarePreferenceRow])
    {
        title = [self userDataHealthCarePreferenceString];
    }
    return title;
}

- (NSString *)editMyProfileRowViewPlaceholderString:(SWEditMyProfileRowView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.usernameRow])
    {
        title = @"username";
    }
    else if ([sender isEqual:self.emailRow])
    {
        title = @"email";
    }
    else if ([sender isEqual:self.genderRow])
    {
        title = @"gender";
    }
    else if ([sender isEqual:self.raceEthnicityRow])
    {
        title = @"race/ethnicity";
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        title = @"birthday";
    }
    else if ([sender isEqual:self.locationRow])
    {
        title = @"Location";
    }
    else if ([sender isEqual:self.healthCarePreferenceRow])
    {
        title = @"Health care preference";
    }
    return title;
}

- (UIImage *)editMyProfileRowViewIconImage:(SWEditMyProfileRowView *)sender
{
    UIImage *image;
    if ([sender isEqual:self.usernameRow])
    {
        image = [UIImage imageNamed:@"profile-icon-username"];
    }
    else if ([sender isEqual:self.emailRow])
    {
        image = [UIImage imageNamed:@"profile-icon-email"];
    }
    else if ([sender isEqual:self.genderRow])
    {
        image = [UIImage imageNamed:@"profile-icon-gender"];
    }
    else if ([sender isEqual:self.raceEthnicityRow])
    {
        image = [UIImage imageNamed:@"profile-icon-ethnicity"];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        image = [UIImage imageNamed:@"profile-icon-birthdate"];
    }
    else if ([sender isEqual:self.locationRow])
    {
        image = [UIImage imageNamed:@"profile-icon-location"];
    }
    else if ([sender isEqual:self.healthCarePreferenceRow])
    {
        image = [UIImage imageNamed:@"profile-icon-medicine"];
    }
    return image;
}

- (void)editMyProfileRowWasTapped:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.emailRow])
    {
       // Do nothing
    }
    else if ([sender isEqual:self.usernameRow])
    {
        // Do nothing
    }
    else if ([sender isEqual:self.genderRow])
    {
        //[SWHelper helperShowAlertWithTitle:@"Gender Tapped"];
        self.activeRow = self.genderRow;
        SWSelectItemFromListViewController *selectItemFromListVC = [[SWSelectItemFromListViewController alloc] init];
        selectItemFromListVC.delegate = self;
        [self.navigationController pushViewController:selectItemFromListVC animated:YES];
    }
    else if ([sender isEqual:self.raceEthnicityRow])
    {
        //[SWHelper helperShowAlertWithTitle:@"Race/Ethnicity Tapped"];
        self.activeRow = self.raceEthnicityRow;
        SWSelectItemFromListViewController *selectItemFromListVC = [[SWSelectItemFromListViewController alloc] init];
        selectItemFromListVC.delegate = self;
        [self.navigationController pushViewController:selectItemFromListVC animated:YES];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        //[SWHelper helperShowAlertWithTitle:@"Birthday Tapped"];
        [self.view endEditing:YES];
        self.activeRow = self.birthdayRow;
        [self showDatePickerUsingTitle:@"Birthday"];
    }
    else if ([sender isEqual:self.locationRow])
    {
        //[SWHelper helperShowAlertWithTitle:@"Location Tapped"];
        self.activeRow = self.locationRow;
        SWCaptureUserDefinedAddressViewController *captureUserDefinedAddressVC = [[SWCaptureUserDefinedAddressViewController alloc] init];
        captureUserDefinedAddressVC.delegate = self;
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:captureUserDefinedAddressVC];
        [self presentViewController:navVC animated:YES completion:^{}];
        //[self.navigationController pushViewController:captureUserDefinedAddressVC animated:YES];
    }
    else if ([sender isEqual:self.healthCarePreferenceRow])
    {
        //[SWHelper helperShowAlertWithTitle:@"Health Care Pref Tapped"];
        self.activeRow = self.healthCarePreferenceRow;
        SWSelectItemFromListViewController *selectItemFromListVC = [[SWSelectItemFromListViewController alloc] init];
        selectItemFromListVC.delegate = self;
        [self.navigationController pushViewController:selectItemFromListVC animated:YES];
    }
}

#pragma mark - SWSelectItemFromListViewControllerDelegate

- (NSString *)selectItemFromListViewControllerJSONFileToUseBaseNameWithoutExtension
{
    NSString *name;
    if ([self.activeRow isEqual:self.genderRow])
    {
        name = @"selectGender"; // .json in app's bundle
    }
    else if ([self.activeRow isEqual:self.raceEthnicityRow])
    {
        name = @"selectRaceEthnicity"; // .json in app's bundle
    }
    else if ([self.activeRow isEqual:self.healthCarePreferenceRow])
    {
        name = @"selectPreferredMedicine"; // .json in app's bundle
    }
    return name;
}

- (NSString *)selectItemFromListViewControllerSelectedSystemValue:(SWSelectItemFromListViewController *)sender
{
    NSString *systemValue;
    if ([self.activeRow isEqual:self.genderRow])
    {
        systemValue = self.genderValueToSubmitToServer;
    }
    else if ([self.activeRow isEqual:self.raceEthnicityRow])
    {
        systemValue = self.raceEthnicityValueToSubmitToServer;
    }
    else if ([self.activeRow isEqual:self.healthCarePreferenceRow])
    {
        systemValue = self.healthCarePrefValueToSubmitToServer;
    }
    return systemValue;
}

- (void)selectItemFromListViewControllerWantsToDismiss:(SWSelectItemFromListViewController *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selectItemFromListViewController:(SWSelectItemFromListViewController *)sender userSelectedRowDict:(NSDictionary *)rowDict
{
    if ([self.activeRow isEqual:self.genderRow])
    {
        if ([rowDict objectForKey:@"system_value"])
        {
            self.genderValueToSubmitToServer = [rowDict objectForKey:@"system_value"];
        }
    }
    else if ([self.activeRow isEqual:self.raceEthnicityRow])
    {
        if ([rowDict objectForKey:@"system_value"])
        {
            self.raceEthnicityValueToSubmitToServer = [rowDict objectForKey:@"system_value"];
        }
    }
    else if ([self.activeRow isEqual:self.healthCarePreferenceRow])
    {
        if ([rowDict objectForKey:@"system_value"])
        {
            self.healthCarePrefValueToSubmitToServer = [rowDict objectForKey:@"system_value"];
        }
    }
    [self.activeRow setRowTextFieldText:[rowDict objectForKey:@"name"]];
    [self.activeRow highlightNewlySelectedValue];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SWCaptureUserDefinedAddressViewControllerDelegate

- (void)captureUserDefinedAddressViewControllerWantsToDismiss:(SWCaptureUserDefinedAddressViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)captureUserDefinedAddressViewController:(SWCaptureUserDefinedAddressViewController *)sender userInputCityString:(NSString *)cityString stateRegionString:(NSString *)stateRegionString countryString:(NSString *)countryString
{
    self.cityLocationValueToSubmitToServer = cityString;
    self.stateLocationValueToSubmitToServer = stateRegionString;
    self.countryLocationValueToSubmitToServer = countryString;
    NSString *newLocation = [SWHelper helperUserLocationCityStateCountryFormattedStringForUICity:cityString state:stateRegionString country:countryString];
    [self.activeRow setRowTextFieldText:newLocation];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.activeRow highlightNewlySelectedValue];
    }];
}

- (NSString *)captureUserDefinedAddressViewControllerCity
{
    return self.cityLocationValueToSubmitToServer;
}

- (NSString *)captureUserDefinedAddressViewControllerState
{
    return self.stateLocationValueToSubmitToServer;
}

- (NSString *)captureUserDefinedAddressViewControllerCountry
{
    return self.countryLocationValueToSubmitToServer;
}

#pragma mark - SWEditMyProfileSectionHeaderViewDelegate

- (NSString *)editMyProfileSectionHeaderViewTitleString:(SWEditMyProfileSectionHeaderView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.youSectionHeader])
    {
        title = @"YOU";
    }
    else if ([sender isEqual:self.usernameSectionHeader])
    {
        title = @"USERNAME";
    }
    else if ([sender isEqual:self.contactSectionHeader])
    {
        title = @"CONTACT";
    }
    else if ([sender isEqual:self.aboutSectionHeader])
    {
        title = @"ABOUT";
    }
    return title;
}

#pragma mark - SWEditMyProfileHeaderViewDelegate

- (NSString *)editMyProfileHeaderViewFirstNameString:(SWEditMyProfileHeaderView *)sender
{
    return [self userDataFirstNameString];
}

- (NSString *)editMyProfileHeaderViewLastNameString:(SWEditMyProfileHeaderView *)sender
{
    return [self userDataLastNameString];
}

- (UIImage *)editMyProfileHeaderViewProfilePicImage:(SWEditMyProfileHeaderView *)sender
{
    NSString *stringURL = [self userDataProfilePicStringURL];
    UIImage *imageToReturn = [self.imageForStringURL objectForKey:stringURL];
    if (!imageToReturn)
    {
        if ([self.stringURLCurrentlyLoading containsObject:stringURL])
        {
            //NSLog(@"Skip, work in progress for stringURL %@", stringURL);
        }
        else
        {
            //NSLog(@"Start, fetch for stringURL %@", stringURL);
            [self.stringURLCurrentlyLoading addObject:stringURL]; // Log as currently loading
            NSURLSession *session = [NSURLSession sharedSession];
            [[session dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                //NSLog(@"Background Thread...");
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"Main Thread...");
                    [self.stringURLCurrentlyLoading removeObject:stringURL]; // Remove from currently loading
                    UIImage *image = [UIImage imageWithData:data];
                    if (image)
                    {
                        [self.imageForStringURL setObject:image forKey:stringURL]; // Load to cache
                        //NSLog(@"Loaded image to cache for stringURL %@", stringURL);
                        [sender setProfilePicImage:image];
                    }
                    else
                    {
                        [sender setProfilePicImage:[UIImage imageNamed:@"avatar-empty"]];
                    }
                });
            }] resume];
        }
    }
    return imageToReturn;
}

- (void)editMyProfileHeaderViewProfilePicTapped:(SWEditMyProfileHeaderView *)sender
{
    [self.view endEditing:YES];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select Profile Pic" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Cancel button tappped.
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Camera button tapped.
        [self showPhotoPickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Photos library button tapped.
        [self showPhotoPickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - Target Action

- (void)leftBarButtonItemTapped:(id)sender
{
    [Flurry logEvent:@"Edit My Profile - Cancel Button Tapped"];
    return [self.delegate editMyProfileViewControllerWantsToDismiss:self];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    // END EDITING
    [self.view endEditing:YES];
    [self hideDatePicker];
    
    // VALIDATE
    if ([[self.headerView.firstNameTextField text] isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing First Name" message:@"Please enter your first name." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.headerView.firstNameTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([[self.headerView.lastNameTextField text] isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Last Name" message:@"Please enter your last name." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.headerView.lastNameTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([[self.usernameRow currentText] isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Username" message:@"Please enter your username." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[self.usernameRow currentTextField] becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([[self.emailRow currentText] isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Email" message:@"Please enter your email." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[self.emailRow currentTextField] becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (!self.genderValueToSubmitToServer)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Gender" message:@"Please enter your gender." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[self.genderRow currentTextField] becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (!self.raceEthnicityValueToSubmitToServer)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Race/Ethnicity" message:@"Please enter your race/ethnicity." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[self.raceEthnicityRow currentTextField] becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        /*
         name_first - user’s first name
         name_last - user’s last name
         
         Optional Inputs
         gender - user’s gender (1=female, 2=male)
         race - user's race/ethnicity (
         0 - Not Specified
         1 - American Indian / Alaskan Native
         2 - Asian
         3 - Black or African American
         4 - Hispanic or Latino
         5 - Native Hawaiian or Other Pacific Islander
         6 - White
         )
         month - two digit birth month
         day - two digit birth day
         year - four digit birth year
         city - user’s city
         state - user’s two letter state abbreviation
         country - user’s three letter country abbreviation
         med_pref - user’s preferred medicine (Western/Conventional=1, Holistic/Alternative=2, Both=3, No Preference=0)
         passwd = user’s password
         passwd_c = password again for confirmation
         */
        NSMutableDictionary *serverArgs = [NSMutableDictionary new];
        self.firstNameValueToSubmitToServer = self.headerView.firstNameTextField.text;
        if (self.firstNameValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.firstNameValueToSubmitToServer forKey:@"name_first"];
        }
        self.lastNameValueToSubmitToServer = self.headerView.lastNameTextField.text;
        if (self.lastNameValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.lastNameValueToSubmitToServer forKey:@"name_last"];
        }
        self.usernameValueToSubmitToServer = [[[self usernameRow] currentTextField] text];
        if (self.usernameValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.usernameValueToSubmitToServer forKey:@"username"];
        }
        self.emailValueToSubmitToServer = [[[self emailRow] currentTextField] text];
        if (self.emailValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.emailValueToSubmitToServer forKey:@"email"];
        }
        if (self.genderValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.genderValueToSubmitToServer forKey:@"gender"];
        }
        if (self.raceEthnicityValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.raceEthnicityValueToSubmitToServer forKey:@"race"];
        }
        if (self.dayBirthdayValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.dayBirthdayValueToSubmitToServer forKey:@"day"];
        }
        if (self.monthBirthdayValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.monthBirthdayValueToSubmitToServer forKey:@"month"];
        }
        if (self.yearBirthdayValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.yearBirthdayValueToSubmitToServer forKey:@"year"];
        }
        if (self.cityLocationValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.cityLocationValueToSubmitToServer forKey:@"city"];
        }
        if (self.stateLocationValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.stateLocationValueToSubmitToServer forKey:@"state"];
        }
        if (self.countryLocationValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.countryLocationValueToSubmitToServer forKey:@"country"];
        }
        if (self.healthCarePrefValueToSubmitToServer.length > 0)
        {
            [serverArgs setObject:self.healthCarePrefValueToSubmitToServer forKey:@"med_pref"];
        }
        [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"Saving..."];
        [[SWHelper helperAppBackend] appBackendUpdateUserProfileUsingArgs:serverArgs usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"jsonResponseBody = %@", jsonResponseBody);
                [Flurry logEvent:@"Edit My Profile - Save Button Tapped"];
                if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"])
                {
                    // Yay! It worked. Update core data now that we know the server has the values...
                    SWUserCDM *user = [SWUserCDM currentlyLoggedInUser];
                    if ([serverArgs objectForKey:@"name_first"]) {
                        user.firstName = [serverArgs objectForKey:@"name_first"];
                    }
                    if ([serverArgs objectForKey:@"name_last"]) {
                        user.lastName = [serverArgs objectForKey:@"name_last"];
                    }
                    if ([serverArgs objectForKey:@"username"]) {
                        user.username = [serverArgs objectForKey:@"username"];
                    }
                    if ([serverArgs objectForKey:@"email"]) {
                        user.email = [serverArgs objectForKey:@"email"];
                    }
                    if ([serverArgs objectForKey:@"gender"]) {
                        user.gender = [serverArgs objectForKey:@"gender"];
                    }
                    if ([serverArgs objectForKey:@"race"]) {
                        user.raceEthnicity = [serverArgs objectForKey:@"race"];
                    }
                    // The server serves up dates like so: 1985-09-02
                    if ([serverArgs objectForKey:@"day"] && [serverArgs objectForKey:@"month"] && [serverArgs objectForKey:@"year"]) {
                        user.birthDate = [[NSString alloc] initWithFormat:@"%@-%@-%@", [serverArgs objectForKey:@"year"], [serverArgs objectForKey:@"month"], [serverArgs objectForKey:@"day"]];
                    }
                    if ([serverArgs objectForKey:@"city"]) {
                        user.city = [serverArgs objectForKey:@"city"];
                    }
                    if ([serverArgs objectForKey:@"state"]) {
                        user.state = [serverArgs objectForKey:@"state"];
                    }
                    if ([serverArgs objectForKey:@"country"]) {
                        user.country = [serverArgs objectForKey:@"country"];
                    }
                    if ([serverArgs objectForKey:@"med_pref"]) {
                        user.medPref = [serverArgs objectForKey:@"med_pref"];
                    }
                    
                    // Now that we know we've successfully saved their text data, let's check and see if they have a new profile pic on file
                    if (self.usersNewProfilePicImage)
                    {
                        [[SWHelper helperAppBackend] appBackendUpdateUserProfilePicImage:self.usersNewProfilePicImage usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                                if ([[jsonResponseBody objectForKey:@"photo_original"] isKindOfClass:[NSString class]])
                                {
                                    user.photoOriginal = [jsonResponseBody objectForKey:@"photo_original"];
                                }
                                if ([[jsonResponseBody objectForKey:@"photo_thumb"] isKindOfClass:[NSString class]])
                                {
                                    user.photoThumb = [jsonResponseBody objectForKey:@"photo_thumb"];
                                }
                                NSURLSession *ephemeralSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
                                NSURL *url = [NSURL URLWithString:user.photoThumb];
                                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url]; //[request setHTTPMethod:@"GET"];
                                NSURLSessionDataTask *dataTask = [ephemeralSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [user setProfilePicThumbnailImageData:data];
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification
                                                                                            object:self
                                                                                          userInfo:@{}];
                                        [SWHelper helperDismissFullScreenActivityIndicator];
                                        [self.delegate editMyProfileViewControllerWantsToDismissWithUpdateProfilePic:self.usersNewProfilePicImage sender:self];
                                    });
                                }];
                                [dataTask resume];
                                [ephemeralSession finishTasksAndInvalidate];
                            });
                        }];
                    }
                    else
                    {
                        // No profile pic updates, just finish up...
                        [SWHelper helperDismissFullScreenActivityIndicator];
                        [self.delegate editMyProfileViewControllerWantsToDismiss:self];
                    }
                }
                else if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"fail"])
                {
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    if ([jsonResponseBody objectForKey:@"message"])
                    {
                        [SWHelper helperShowAlertWithTitle:@"Error" message:[jsonResponseBody objectForKey:@"message"]];
                    }
                    else
                    {
                        [SWHelper helperShowAlertWithTitle:@"Error" message:@"Unable to save updates (status = fail, no message). Please try again."];
                    }
                }
                else
                {
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    [SWHelper helperShowAlertWithTitle:@"Error" message:@"Unable to save updates. Please try again."];
                }
            });
        }];
    }
}

#pragma mark - UIImagePickerControllerDelegate

// Tells the delegate that the user picked a still image or movie.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //NSLog(@"info = %@", info);
    [self.headerView setProfilePicImage:[info objectForKey:UIImagePickerControllerEditedImage]];
    self.usersNewProfilePicImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [self dismissViewControllerAnimated:YES completion:^{}];
    /*
     info = {
     UIImagePickerControllerCropRect = "NSRect: {{0, 0}, {955, 960}}";
     UIImagePickerControllerEditedImage = "<UIImage: 0x154e46e60> size {744, 750} orientation 0 scale 1.000000";
     UIImagePickerControllerMediaType = "public.image";
     UIImagePickerControllerOriginalImage = "<UIImage: 0x154c9b0e0> size {960, 1280} orientation 3 scale 1.000000";
     UIImagePickerControllerReferenceURL = "assets-library://asset/asset.JPG?id=FCF0A8FB-376C-4DF5-94DB-18C8102A5335&ext=JPG";
     }
     */
}

// Tells the delegate that the user cancelled the pick operation.
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //NSLog(@"Cancelled the photo picker process...");
    [self dismissViewControllerAnimated:YES completion:^{}];
}

// Tells the delegate that the user picked an image. (Deprecated in iOS 3.0. Use imagePickerController:didFinishPickingMediaWithInfo: instead.)
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    //NSLog(@"Did this get called?");
}

#pragma mark - Helpers

- (void)hideDatePicker
{
    // HIDE THE DATE PICKER
    self.datePickerVerticalHeightConstraint.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

- (void)showPhotoPickerUsingSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        NSString *mediaType = AVMediaTypeVideo;
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
        if (authStatus == AVAuthorizationStatusAuthorized)
        {
            // do your logic
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.sourceType = sourceType;
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                self.popOver = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.popOver presentPopoverFromRect:CGRectMake(120, 110, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
                });
            }
            else
            {
                [self presentViewController:imagePicker animated:YES completion:^{}];
            }
        }
        else if (authStatus == AVAuthorizationStatusDenied)
        {
            // denied
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera Access Off" message:@"To turn camera access on, go to settings and turn camera access on." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
            [alertController addAction:ok];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (authStatus == AVAuthorizationStatusRestricted)
        {
            // restricted, normally won't happen
            [SWHelper helperShowAlertWithTitle:@"Camera Access Restricted"];
        }
        else if (authStatus == AVAuthorizationStatusNotDetermined)
        {
            // not determined?!
            [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
                if(granted)
                {
                    //NSLog(@"Granted access to %@", mediaType);
                }
                else
                {
                    //NSLog(@"Not granted access to %@", mediaType);
                }
            }];
        }
        else
        {
            // impossible, unknown authorization status
        }
    }
    else
    {
        [SWHelper helperShowAlertWithTitle:@"Selected Source Type Not Available"];
    }
}

- (void)showDatePickerUsingTitle:(NSString *)title
{
    // SHOW THE DATE PICKER
    [self.datePicker setTitle:title];
    if (self.birthdayValueToSubmitToServer != nil)
    {
        self.datePicker.datePicker.date = self.birthdayValueToSubmitToServer;
    }
    self.datePickerVerticalHeightConstraint.constant = 300;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

- (void)addDatePicker
{
    if (![self.viewsDictionary objectForKey:@"datePicker"])
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.datePicker = [[SWDatePicker alloc] init];
        if ([[SWUserCDM currentlyLoggedInUser] birthDateAsDate] != nil)
        {
            self.datePicker.datePicker.date = [[SWUserCDM currentlyLoggedInUser] birthDateAsDate];
        }
        
        // INIT AUTO LAYOUT VIEWS DICT
        [self.viewsDictionary setObject:self.datePicker forKey:@"datePicker"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.datePicker.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.view addSubview:self.datePicker];
        
        // LAYOUT
        
        // Layout DATE PICKER
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[datePicker]|" options:0 metrics:nil views:self.viewsDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[datePicker]|" options:0 metrics:nil views:self.viewsDictionary]];
        self.datePickerVerticalHeightConstraint = [NSLayoutConstraint constraintWithItem:self.datePicker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0];
        [self.view addConstraint:self.datePickerVerticalHeightConstraint];
        
        // CONFIG
        
        // Config DATE PICKER
        self.datePicker.delegate = self;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.datePicker.backgroundColor = [UIColor redColor];
    }
}

- (NSString *)userDataProfilePicStringURL
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].photoThumb;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataFirstNameString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].firstName;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataLastNameString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].lastName;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataUsernameString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].username;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataEmailString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].email;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataGenderString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].gender;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataRaceEthnicityString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].raceEthnicity;
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataGenderFormattedString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] genderForUI];
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataRaceEthnicityFormattedString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] raceEthnicityForUI];
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataDateOfBirthString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] birthDate];
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataDateOfBirthFormattedString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] birthDateForUI];
    if (!answer)
    {
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)userDataLocationString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] userLocationForUI];
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

- (NSString *)userDataHealthCarePreferenceString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] healthCarePrefForUI];
    if (!answer)
    {
        answer = @"";
    }
    return answer;
}

#pragma mark - Life Cycle Methods


- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT SELF
    self.imageForStringURL = [NSMutableDictionary new];
    self.stringURLCurrentlyLoading = [NSMutableArray new];
    self.firstNameValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].firstName;
    self.lastNameValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].lastName;
    self.usernameValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].username;
    self.emailValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].email;
    self.genderValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].gender;
    self.raceEthnicityValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].raceEthnicity;
    self.birthdayValueToSubmitToServer = [[SWUserCDM currentlyLoggedInUser] birthDateAsDate];
    self.dayBirthdayValueToSubmitToServer = [[SWUserCDM currentlyLoggedInUser] birthDateDayForServer];
    self.monthBirthdayValueToSubmitToServer = [[SWUserCDM currentlyLoggedInUser] birthDateMonthForServer];
    self.yearBirthdayValueToSubmitToServer = [[SWUserCDM currentlyLoggedInUser] birthDateYearForServer];
    self.cityLocationValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].city;
    self.stateLocationValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].state;
    self.countryLocationValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].country;
    self.healthCarePrefValueToSubmitToServer = [SWUserCDM currentlyLoggedInUser].medPref;
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.youSectionHeader = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.headerView = [[SWEditMyProfileHeaderView alloc] init];
    self.usernameSectionHeader = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.usernameRow = [[SWEditMyProfileRowView alloc] init];
    self.contactSectionHeader = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.emailRow = [[SWEditMyProfileRowView alloc] init];
    self.aboutSectionHeader = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.genderRow = [[SWEditMyProfileRowView alloc] init];
    self.raceEthnicityRow = [[SWEditMyProfileRowView alloc] init];
    self.birthdayRow = [[SWEditMyProfileRowView alloc] init];
    self.locationRow = [[SWEditMyProfileRowView alloc] init];
    self.healthCarePreferenceRow = [[SWEditMyProfileRowView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.youSectionHeader forKey:@"youSectionHeader"];
    [self.viewsDictionary setObject:self.headerView forKey:@"headerView"];
    [self.viewsDictionary setObject:self.usernameSectionHeader forKey:@"usernameSectionHeader"];
    [self.viewsDictionary setObject:self.usernameRow forKey:@"usernameRow"];
    [self.viewsDictionary setObject:self.contactSectionHeader forKey:@"contactSectionHeader"];
    [self.viewsDictionary setObject:self.emailRow forKey:@"emailRow"];
    [self.viewsDictionary setObject:self.aboutSectionHeader forKey:@"aboutSectionHeader"];
    [self.viewsDictionary setObject:self.genderRow forKey:@"genderRow"];
    [self.viewsDictionary setObject:self.raceEthnicityRow forKey:@"raceEthnicityRow"];
    [self.viewsDictionary setObject:self.birthdayRow forKey:@"birthdayRow"];
    [self.viewsDictionary setObject:self.locationRow forKey:@"locationRow"];
    [self.viewsDictionary setObject:self.healthCarePreferenceRow forKey:@"healthCarePreferenceRow"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.youSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.usernameSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.usernameRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.contactSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.aboutSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.genderRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.raceEthnicityRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.birthdayRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.locationRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.healthCarePreferenceRow.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.youSectionHeader];
    [self.contentView addSubview:self.headerView];
    [self.contentView addSubview:self.usernameSectionHeader];
    [self.contentView addSubview:self.usernameRow];
    [self.contentView addSubview:self.contactSectionHeader];
    [self.contentView addSubview:self.emailRow];
    [self.contentView addSubview:self.aboutSectionHeader];
    [self.contentView addSubview:self.genderRow];
    [self.contentView addSubview:self.raceEthnicityRow];
    [self.contentView addSubview:self.birthdayRow];
    [self.contentView addSubview:self.locationRow];
    [self.contentView addSubview:self.healthCarePreferenceRow];
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout YOU SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[youSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[youSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout HEADER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[headerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[youSectionHeader][headerView(130)]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.headerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
    
    // Layout USERNAME SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[usernameSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[headerView][usernameSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout USERNAME ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[usernameRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[usernameSectionHeader]-(0)-[usernameRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout CONTACT SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contactSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[usernameRow]-(0)-[contactSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout EMAIL ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[emailRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[contactSectionHeader]-(0)-[emailRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout ABOUT SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[aboutSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[emailRow]-(0)-[aboutSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout GENDER ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[genderRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[aboutSectionHeader]-(0)-[genderRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout RACE/ETHNICITY ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[raceEthnicityRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[genderRow]-(0)-[raceEthnicityRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout BIRTHDAY ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[birthdayRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[raceEthnicityRow]-(0)-[birthdayRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout LOCATION ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[locationRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[birthdayRow]-(0)-[locationRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout HEALTH CARE PREFERENCE ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[healthCarePreferenceRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[locationRow]-(0)-[healthCarePreferenceRow(rowHeight)]-|"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR HEIGHT !!!
    
    // CONFIG
    
    // Config KEYBOARD NOTIFICATIONS
    // https://developer.apple.com/library/ios/documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html
    [self registerForKeyboardNotifications];
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
    
    // Config TITLE
    self.title = @"Edit My Profile";
    self.navigationItem.titleView = nil;
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Config LEFT BAR BUTTON ITEM
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // Config YOU SECTION HEADER
    self.youSectionHeader.delegate = self;
    
    // Config HEADER VIEW
    self.headerView.delegate = self;
    
    // Config USERNAME SECTION HEADER
    self.usernameSectionHeader.delegate = self;
    
    // Config FIRST NAME TEXT FIELD
    self.headerView.firstNameTextField.delegate = self;
    self.headerView.firstNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.headerView.firstNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.headerView.firstNameTextField.spellCheckingType = UITextAutocapitalizationTypeNone;
    self.headerView.firstNameTextField.enablesReturnKeyAutomatically = YES;
    self.headerView.firstNameTextField.keyboardType = UIKeyboardTypeDefault;
    self.headerView.firstNameTextField.returnKeyType = UIReturnKeyNext;
    self.headerView.firstNameTextField.secureTextEntry = NO;
    
    // Config LAST NAME TEXT FIELD
    self.headerView.lastNameTextField.delegate = self;
    self.headerView.lastNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.headerView.lastNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.headerView.lastNameTextField.spellCheckingType = UITextAutocapitalizationTypeNone;
    self.headerView.lastNameTextField.enablesReturnKeyAutomatically = YES;
    self.headerView.lastNameTextField.keyboardType = UIKeyboardTypeDefault;
    self.headerView.lastNameTextField.returnKeyType = UIReturnKeyNext;
    self.headerView.lastNameTextField.secureTextEntry = NO;
    
    // Config USERNAME ROW
    self.usernameRow.delegate = self;
    [self.usernameRow currentTextField].delegate = self;
    [self.usernameRow currentTextField].autocorrectionType = UITextAutocorrectionTypeNo;
    [self.usernameRow currentTextField].autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.usernameRow currentTextField].spellCheckingType = UITextAutocapitalizationTypeNone;
    [self.usernameRow currentTextField].enablesReturnKeyAutomatically = YES;
    [self.usernameRow currentTextField].keyboardType = UIKeyboardTypeDefault;
    [self.usernameRow currentTextField].returnKeyType = UIReturnKeyNext;
    [self.usernameRow currentTextField].secureTextEntry = NO;
    
    // Config CONTACT SECTION HEADER
    self.contactSectionHeader.delegate = self;
    
    // Config EMAIL ROW
    self.emailRow.delegate = self;
    [self.emailRow currentTextField].delegate = self;
    [self.emailRow currentTextField].autocorrectionType = UITextAutocorrectionTypeNo;
    [self.emailRow currentTextField].autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.emailRow currentTextField].spellCheckingType = UITextAutocapitalizationTypeNone;
    [self.emailRow currentTextField].enablesReturnKeyAutomatically = YES;
    [self.emailRow currentTextField].keyboardType = UIKeyboardTypeEmailAddress;
    [self.emailRow currentTextField].returnKeyType = UIReturnKeyNext;
    [self.emailRow currentTextField].secureTextEntry = NO;
    
    // Config ABOUT SECTION HEADER
    self.aboutSectionHeader.delegate = self;
    
    // Config GENDER ROW
    self.genderRow.delegate = self;
    [self.genderRow addDisclosureIconAndTapFeature];
    
    // Config RACE/ETHNICITY ROW
    self.raceEthnicityRow.delegate = self;
    [self.raceEthnicityRow addDisclosureIconAndTapFeature];
    
    // Config BIRTHDAY ROW
    self.birthdayRow.delegate = self;
    [self.birthdayRow addDisclosureIconAndTapFeature];
    
    // Config LOCATION ROW
    self.locationRow.delegate = self;
    [self.locationRow addDisclosureIconAndTapFeature];
    
    // Config HEALTH CARE PREFERENCE ROW
    self.healthCarePreferenceRow.delegate = self;
    [self.healthCarePreferenceRow addDisclosureIconAndTapFeature];
    [self.healthCarePreferenceRow showBottomBorder];
    
    // Config DATE PICKER
    [self addDatePicker];
    
    /*
    // RUN NETWORK TEST...
    [[SWHelper helperAppBackend] appBackendGetUserProfileUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        //NSLog(@"requestURLAbsoluteString = %@, responseBodyAsString = %@", requestURLAbsoluteString, responseBodyAsString);
    }];
    */
    
    // COLOR FOR DEVELOPMENT PURPOSES
//    self.staticHeaderImageView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
//    self.staticHeaderImageOverlayView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
//    self.scrollView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
//    self.contentView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
//    self.headerView.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
//    self.contactSectionHeader.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
//    self.emailRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
//    self.aboutSectionHeader.backgroundColor = [SWColor color:[UIColor orangeColor] usingOpacity:0.5];
//    self.genderRow.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
//    self.raceEthnicityRow.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
//    self.birthdayRow.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
//    self.locationRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
//    self.healthCarePreferenceRow.backgroundColor = [SWColor color:[UIColor cyanColor] usingOpacity:0.5];
}

@end
