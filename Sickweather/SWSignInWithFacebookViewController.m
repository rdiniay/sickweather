//
//  SWSignInWithFacebookViewController.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SWFacebookSignInSubController.h"
#import "Flurry.h"
#import "SWSignInWithFacebookViewController.h"
#import "SWSignInWithSickweatherViewController.h"
#import "SWAppDelegate.h"
#import "SWUserCDM+SWAdditions.h"
#import "SWNavigationController.h"
#import "SWCreateSickweatherAccountViewController.h"
#import "NSString+FontAwesome.h"

typedef void (^SWAPIFetcherJSONCompletionHandler)(NSDictionary *jsonResponseBody, NSHTTPURLResponse *responseHeaders, NSError *error);

@interface SWSignInWithFacebookViewController () <SWSignInWithSickweatherViewControllerDelegate, SWCreateSickweatherAccountViewControllerDelegate>

// UI VIEWS
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIButton *dismissButton;
@property (strong, nonatomic) UIView *topContainer;
@property (strong, nonatomic) UIView *formContainerLayoutShell;
@property (strong, nonatomic) UIView *formContainer;
@property (strong, nonatomic) NSLayoutConstraint *formContainerVerticalHeightConstraint;
@property (strong, nonatomic) UILabel *contextTitleLabel;
@property (strong, nonatomic) UILabel *contextDescriptionLabel;
@property (strong, nonatomic) UIButton *signInWithFacebookButton;
@property (strong, nonatomic) UIView *dashBeforeView;
@property (strong, nonatomic) UILabel *orLabel;
@property (strong, nonatomic) UIView *dashAfterView;
@property (strong, nonatomic) UIButton *signInWithEmailButton;
@property (strong, nonatomic) UIButton *createAccountButton;
@property (strong, nonatomic) UITextView *termsOfUseTextView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) SWFacebookSignInSubController *facebookSignInSubController;

@end

@implementation SWSignInWithFacebookViewController

#pragma mark - Custom Getters/Setters

- (NSString *)context
{
    if (!_context) {
        _context = kSWSignInViewControllerContextDefault;
    }
    return _context;
}

- (SWFacebookSignInSubController *)facebookSignInSubController
{
    if (!_facebookSignInSubController) {
        _facebookSignInSubController = [[SWFacebookSignInSubController alloc] init];
        _facebookSignInSubController.presentingViewController = self;
    }
    return _facebookSignInSubController;
}

#pragma mark - SWSignInWithSickweatherViewControllerDelegate

- (void)signInWithSickweatherViewControllerDidDismissWithSuccessfulLogin:(SWSignInWithSickweatherViewController *)vc
{
    [[SWHelper helperAppDelegate] appDelegateUpdateUserBasedOnNetwork];
    [self.delegate didDismissWithSuccessfulLogin:self]; // Forward message
}

- (void)signInWithSickweatherViewControllerDidDismissWithoutLoggingIn:(SWSignInWithSickweatherViewController *)vc
{
    [self.delegate didDismissWithoutLoggingIn:self]; // Forward message
}

#pragma mark - SWCreateSickweatherAccountViewControllerDelegate

- (void)createSickweatherAccountViewControllerDidDismissWithSuccessfulLogin:(SWCreateSickweatherAccountViewController *)vc
{
    [[SWHelper helperAppDelegate] appDelegateUpdateUserBasedOnNetwork];
    [self.delegate didDismissWithSuccessfulLogin:self]; // Forward message
}

- (void)createSickweatherAccountViewControllerDidDismissWithoutLoggingIn:(SWCreateSickweatherAccountViewController *)vc
{
    // Does not get forwarded. Client of this class should never concern itself with this. We just disimss.
    [vc dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Helpers

#pragma mark - Facebook Integration Helper Methods

- (NSString *)getErrorMessageFor:(NSString *)responseBody statusCode:(NSString *)statusCode line:(NSString *)line
{
    return [NSString stringWithFormat:@"Please Try Again.\n\nResponse Body: %@\n\nResponse Header Status Code: %@", responseBody, statusCode];
    return [NSString stringWithFormat:@"Please Try Again.\n\nResponse Body: %@\n\nResponse Header Status Code: %@\n\nLine: %@", responseBody, statusCode, line];
}

- (void)facebookAccessTokenDidChangeNotification:(NSNotification *)notification
{
    //NSLog(@"notification = %@", notification);
    //FBSDKAccessToken *newAccessToken = [notification.userInfo objectForKey:FBSDKAccessTokenChangeNewKey];
    //NSLog(@"new = %@", newAccessToken);
    //FBSDKAccessToken *oldAccessToken = [notification.userInfo objectForKey:FBSDKAccessTokenChangeOldKey];
    //NSLog(@"old = %@", oldAccessToken);
    //NSLog(@"current = %@", [FBSDKAccessToken currentAccessToken]);
}

#pragma mark - Target Action

- (void)upperRightBarButtonItemTapped:(id)sender
{
    [self.delegate didDismissWithoutLoggingIn:self];
}

- (void)termsTextViewTapped:(id)sender
{
    // Get layout manager
    NSLayoutManager *layoutManager = self.termsOfUseTextView.layoutManager;
    
    // Get tap location
    CGPoint location = [sender locationInView:self.termsOfUseTextView];
    
    // Adjust location by container insets
    location.x -= self.termsOfUseTextView.textContainerInset.left;
    location.y -= self.termsOfUseTextView.textContainerInset.top;
    
    // Get character for tap location
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:location inTextContainer:self.termsOfUseTextView.textContainer fractionOfDistanceBetweenInsertionPoints:NULL];
    
    // Check fot match
    if (characterIndex < self.termsOfUseTextView.textStorage.length)
    {
        NSRange range;
        id value = [self.termsOfUseTextView.attributedText attribute:@"termsOfService" atIndex:characterIndex effectiveRange:&range];
        if (value)
        {
            // Show terms of service
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", @"http://www.sickweather.com/terms/"]]];
        }
        value = [self.termsOfUseTextView.attributedText attribute:@"privacyPolicy" atIndex:characterIndex effectiveRange:&range];
        if (value)
        {
            // Show privacy policy
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", @"http://www.sickweather.com/privacy/"]]];
        }
    }
}

- (void)signInUsingFacebook:(id)sender
{
    [self.activityIndicator startAnimating];
    [self.facebookSignInSubController loginUsingCompletionHandler:^(SWUserCDM *loggedInUser) {
        if (loggedInUser)
        {
            // Dismiss on success!
            [Flurry logEvent:@"Facebook Signup"];
            [self.activityIndicator stopAnimating];
            [[SWHelper helperAppDelegate] appDelegateUpdateUserBasedOnNetwork];
			[self getFamilyMemberIdFromBackend];
            [self.delegate didDismissWithSuccessfulLogin:self]; // Forward message
        }
        else
        {
            // Do nothing, stay on page, sub controller has already issued alert for user
        }
        [self.activityIndicator stopAnimating];
    }];
}

- (void)signInUsingSickweather:(id)sender
{
    // Log tap
    [Flurry logEvent:@"Skip FB Sign In"];
    
    // Present modal
    SWSignInWithSickweatherViewController *vc = (SWSignInWithSickweatherViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SWSignInWithSickweatherViewController"];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)getFamilyMemberIdFromBackend{
	if ([SWUserCDM currentlyLoggedInUser]) {
		[[SWHelper helperAppBackend]appBackendGetFamilyIdUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
			if (jsonResponseBody){
				if ([jsonResponseBody valueForKey:@"family_id"] != nil && ![[jsonResponseBody valueForKey:@"family_id"] isEqual:[NSNull null]] && ![[jsonResponseBody valueForKey:@"family_id"] isEqualToString:@""]){
					[SWHelper setFamilyId:[jsonResponseBody valueForKey:@"family_id"]];
				}
			}
		}];
	}
}


- (void)createSickweatherAccount:(id)sender
{
    // Log tap
    [Flurry logEvent:@"Create Account 1"];
    
    // Present modal
    SWCreateSickweatherAccountViewController *createSickweatherAccountViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWCreateSickweatherAccountViewController"];
    createSickweatherAccountViewController.delegate = self;
    [self presentViewController:createSickweatherAccountViewController animated:YES completion:nil];
}

#pragma mark - Lifecycle Methods

// Support only portrait
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    if (self.showStatusBar)
    {
        return NO;
    }
    return YES;
}

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // Set title
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        self.title = @"Report an Illness";
    }
    else
    {
        self.title = @"";
    }
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.imageView = [[UIImageView alloc] init];
    self.dismissButton = [[UIButton alloc] init];
    self.topContainer = [[UIView alloc] init];
    self.formContainerLayoutShell = [[UIView alloc] init];
    self.formContainer = [[UIView alloc] init];
    self.contextTitleLabel = [[UILabel alloc] init];
    self.contextDescriptionLabel = [[UILabel alloc] init];
    self.signInWithFacebookButton = [[UIButton alloc] init];
    self.dashBeforeView = [[UIView alloc] init];
    self.orLabel = [[UILabel alloc] init];
    self.dashAfterView = [[UIView alloc] init];
    self.signInWithEmailButton = [[UIButton alloc] init];
    self.createAccountButton = [[UIButton alloc] init];
    self.termsOfUseTextView = [[UITextView alloc] init];
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_scrollView, _imageView, _dismissButton, _topContainer, _formContainerLayoutShell, _formContainer, _contextTitleLabel, _contextDescriptionLabel, _signInWithFacebookButton, _dashBeforeView, _orLabel, _dashAfterView, _signInWithEmailButton, _createAccountButton, _termsOfUseTextView, _activityIndicator);
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.formContainerLayoutShell.translatesAutoresizingMaskIntoConstraints = NO;
    self.topContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.formContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.contextTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.contextDescriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.signInWithFacebookButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.dashBeforeView.translatesAutoresizingMaskIntoConstraints = NO;
    self.orLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.dashAfterView.translatesAutoresizingMaskIntoConstraints = NO;
    self.signInWithEmailButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.createAccountButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.termsOfUseTextView.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.formContainerLayoutShell];
    [self.scrollView addSubview:self.formContainer];
    [self.scrollView addSubview:self.topContainer];
    [self.view addSubview:self.dismissButton];
    [self.topContainer addSubview:self.imageView];
    [self.formContainer addSubview:self.signInWithFacebookButton];
    [self.formContainer addSubview:self.dashBeforeView];
    [self.formContainer addSubview:self.orLabel];
    [self.formContainer addSubview:self.dashAfterView];
    [self.formContainer addSubview:self.signInWithEmailButton];
    [self.formContainer addSubview:self.createAccountButton];
    [self.formContainer addSubview:self.termsOfUseTextView];
    [self.formContainer addSubview:self.activityIndicator];
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        [self.formContainer addSubview:self.contextTitleLabel];
        [self.formContainer addSubview:self.contextDescriptionLabel];
    }
    
    // COLOR VIEWS (for debugging)
    /*
    self.scrollView.backgroundColor = [UIColor redColor];
    self.formContainer.backgroundColor = [UIColor purpleColor];
    self.formContainerLayoutShell.backgroundColor = [UIColor orangeColor];
    self.topContainer.backgroundColor = [UIColor greenColor];
    */
    
    // LAYOUT
    
    CGFloat layoutShellHeight = 300; // The 220 found here is for all device types and syncs with Launch Screen.xib
    /*
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        layoutShellHeight = 325;
    }
    */
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout FORM CONTAINER LAYOUT SHELL
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_formContainerLayoutShell(layoutShellHeight)]" options:0 metrics:@{@"layoutShellHeight":@(layoutShellHeight)} views:viewsDictionary]];
    NSLayoutConstraint *formContainerLayoutShellLeftConstraint = [NSLayoutConstraint constraintWithItem:self.formContainerLayoutShell
                                                                                              attribute:NSLayoutAttributeLeading
                                                                                              relatedBy:0
                                                                                                 toItem:self.view
                                                                                              attribute:NSLayoutAttributeLeading
                                                                                             multiplier:1.0
                                                                                               constant:0];
    [self.view addConstraint:formContainerLayoutShellLeftConstraint];
    NSLayoutConstraint *formContainerLayoutShellRightConstraint = [NSLayoutConstraint constraintWithItem:self.formContainerLayoutShell
                                                                                               attribute:NSLayoutAttributeTrailing
                                                                                               relatedBy:0
                                                                                                  toItem:self.view
                                                                                               attribute:NSLayoutAttributeTrailing
                                                                                              multiplier:1.0
                                                                                                constant:0];
    [self.view addConstraint:formContainerLayoutShellRightConstraint];
    NSLayoutConstraint *formContainerLayoutShellBottomConstraint = [NSLayoutConstraint constraintWithItem:self.formContainerLayoutShell
                                                                                                attribute:NSLayoutAttributeBottom
                                                                                                relatedBy:0
                                                                                                   toItem:self.view
                                                                                                attribute:NSLayoutAttributeBottom
                                                                                               multiplier:1.0
                                                                                                 constant:0];
    [self.view addConstraint:formContainerLayoutShellBottomConstraint];
    
    // Layout TOP CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_topContainer]-0-[_formContainerLayoutShell]" options:0 metrics:0 views:viewsDictionary]];
    NSLayoutConstraint *topContainerLeftConstraint = [NSLayoutConstraint constraintWithItem:self.topContainer
                                                                                  attribute:NSLayoutAttributeLeading
                                                                                  relatedBy:0
                                                                                     toItem:self.view
                                                                                  attribute:NSLayoutAttributeLeading
                                                                                 multiplier:1.0
                                                                                   constant:0];
    [self.view addConstraint:topContainerLeftConstraint];
    NSLayoutConstraint *topContainerRightConstraint = [NSLayoutConstraint constraintWithItem:self.topContainer
                                                                                   attribute:NSLayoutAttributeTrailing
                                                                                   relatedBy:0
                                                                                      toItem:self.view
                                                                                   attribute:NSLayoutAttributeTrailing
                                                                                  multiplier:1.0
                                                                                    constant:0];
    [self.view addConstraint:topContainerRightConstraint];
    
    // Config HEADER IMAGE
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imageView]|" options:0 metrics:0 views:viewsDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageView]|" options:0 metrics:0 views:viewsDictionary]];
    }
    else
    {
        NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                             attribute:NSLayoutAttributeCenterX
                                                                             relatedBy:0
                                                                                toItem:self.topContainer
                                                                             attribute:NSLayoutAttributeCenterX
                                                                            multiplier:1.0
                                                                              constant:0];
        [self.view addConstraint:centerXConstraint];
        NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                             attribute:NSLayoutAttributeCenterY
                                                                             relatedBy:0
                                                                                toItem:self.topContainer
                                                                             attribute:NSLayoutAttributeCenterY
                                                                            multiplier:1.0
                                                                              constant:0];
        [self.view addConstraint:centerYConstraint];
    }
    
    // Config HEADER IMAGE
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_contextTitleLabel]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(15)-[_contextTitleLabel]" options:0 metrics:0 views:viewsDictionary]];
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_contextDescriptionLabel]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_contextTitleLabel]-(12)-[_contextDescriptionLabel]" options:0 metrics:0 views:viewsDictionary]];
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_signInWithFacebookButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_contextDescriptionLabel]-(25)-[_signInWithFacebookButton(40)]" options:0 metrics:0 views:viewsDictionary]];
    }
    else
    {
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_signInWithFacebookButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
        [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(15)-[_signInWithFacebookButton(40)]" options:0 metrics:0 views:viewsDictionary]];
    }
    
    // Layout FORM CONTAINER
    self.formContainerVerticalHeightConstraint = [NSLayoutConstraint constraintWithItem:self.formContainer attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:0 attribute:0 multiplier:1 constant:0]; // Start with zero height (for animation purposes)
    [self.view addConstraint:self.formContainerVerticalHeightConstraint];
    NSLayoutConstraint *formContainerLeftConstraint = [NSLayoutConstraint constraintWithItem:self.formContainer
                                                                                   attribute:NSLayoutAttributeLeading
                                                                                   relatedBy:0
                                                                                      toItem:self.view
                                                                                   attribute:NSLayoutAttributeLeading
                                                                                  multiplier:1.0
                                                                                    constant:0];
    [self.view addConstraint:formContainerLeftConstraint];
    NSLayoutConstraint *formContainerRightConstraint = [NSLayoutConstraint constraintWithItem:self.formContainer
                                                                                    attribute:NSLayoutAttributeTrailing
                                                                                    relatedBy:0
                                                                                       toItem:self.view
                                                                                    attribute:NSLayoutAttributeTrailing
                                                                                   multiplier:1.0
                                                                                     constant:0];
    [self.view addConstraint:formContainerRightConstraint];
    NSLayoutConstraint *formContainerBottomConstraint = [NSLayoutConstraint constraintWithItem:self.formContainer
                                                                                     attribute:NSLayoutAttributeBottom
                                                                                     relatedBy:0
                                                                                        toItem:self.view
                                                                                     attribute:NSLayoutAttributeBottom
                                                                                    multiplier:1.0
                                                                                      constant:0];
    [self.view addConstraint:formContainerBottomConstraint];
    
    // Layout DISMISS BUTTON
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:15]];
    
    // Layout DASH BEFORE
    CGFloat dashWidthSize = [UIScreen mainScreen].bounds.size.width/2 - 40;
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_dashBeforeView(width)]" options:0 metrics:@{@"width":@(dashWidthSize)} views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_signInWithFacebookButton]-(25)-[_dashBeforeView(height)]" options:0 metrics:@{@"height":@(0.5)} views:viewsDictionary]];
    
    // Layout DASH AFTER
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_orLabel]-[_dashAfterView(width)]-(15)-|" options:0 metrics:@{@"width":@(dashWidthSize)} views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_dashAfterView(height)]" options:0 metrics:@{@"height":@(0.5)} views:viewsDictionary]];
    
    // Layout OR LABEL
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_dashBeforeView]-[_orLabel]" options:0 metrics:0 views:viewsDictionary]];
    NSLayoutConstraint *orLabelConstraint = [NSLayoutConstraint constraintWithItem:self.orLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.dashBeforeView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    [self.formContainer addConstraint:orLabelConstraint];
    
    NSLayoutConstraint *dashAfterViewConstraint = [NSLayoutConstraint constraintWithItem:self.dashAfterView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.orLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    [self.formContainer addConstraint:dashAfterViewConstraint];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_signInWithEmailButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_dashAfterView]-(25)-[_signInWithEmailButton(40)]" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_createAccountButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_signInWithEmailButton]-(10)-[_createAccountButton(40)]" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_termsOfUseTextView]|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_createAccountButton]-(0)-[_termsOfUseTextView]" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.signInWithFacebookButton attribute:NSLayoutAttributeRight multiplier:1 constant:-25]];
    [self.formContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.signInWithFacebookButton attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    
    // CONFIG
    
    // Config SCROLL VIEW
    self.scrollView.alwaysBounceVertical = NO;
    
    // Config TERMS OF USE
    self.termsOfUseTextView.backgroundColor = [UIColor clearColor];
    
    // Config HEADER IMAGE
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        self.imageView.image = [UIImage imageNamed:@"login-banner-report-illness"];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
    }
    else
    {
        self.imageView.image = [UIImage imageNamed:@"cloud-sickweather-going-around"];
        [self.imageView sizeToFit];
        self.imageView.contentMode = UIViewContentModeCenter;
    }
    
    // Config DISMISS BUTTON
    [self.dismissButton setImage:[UIImage imageNamed:@"close-icon"] forState:UIControlStateNormal];
    [self.dismissButton addTarget:self action:@selector(upperRightBarButtonItemTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.dismissButton.layer.opacity = 0;
    [UIView animateWithDuration:0.65 delay:0 options:0 animations:^{
        self.dismissButton.layer.opacity = 1;
    } completion:^(BOOL finished) {}];
    
    // Config [CONTEXT TITLE LABEL, CONTEXT DESCRIPTION LABEL] SIGN IN WITH FACEBOOK BUTTON
    if ([kSWSignInViewControllerContextReport isEqualToString:self.context])
    {
        // Add context title label
        self.contextTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Please Sign In to Report an Illness" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}];
        self.contextTitleLabel.hidden = NO;
        self.contextTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        
        // Add context title label
        self.contextDescriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Help keep your community informed with anonymous illness reports and track your own illness history over time." attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
        self.contextDescriptionLabel.hidden = YES;
        self.contextDescriptionLabel.hidden = NO;
        self.contextDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.contextDescriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.contextDescriptionLabel.numberOfLines = 0;
        
        
        // Add sign in with facebook button
        NSMutableAttributedString *fb = [[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaFacebookSquare] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontAwesomeOfSize:13]}];
        [fb appendAttributedString:[[NSAttributedString alloc] initWithString:@"  SIGN IN WITH FACEBOOK" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}]];
        [self.signInWithFacebookButton setAttributedTitle:fb forState:UIControlStateNormal];
        [self.signInWithFacebookButton setBackgroundColor:[SWColor colorSickweatherFacebookBlue59x89x152]];
        [self.signInWithFacebookButton.layer setBorderColor:[SWColor colorSickweatherFacebookBlue59x89x152].CGColor];
        [self.signInWithFacebookButton.layer setBorderWidth:0.5];
        [self.signInWithFacebookButton.layer setCornerRadius:2];
        [self.signInWithFacebookButton addTarget:self action:@selector(signInUsingFacebook:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        // Add sign in with facebook button
        NSMutableAttributedString *fb = [[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaFacebookSquare] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontAwesomeOfSize:13]}];
        [fb appendAttributedString:[[NSAttributedString alloc] initWithString:@"  SIGN IN WITH FACEBOOK" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}]];
        [self.signInWithFacebookButton setAttributedTitle:fb forState:UIControlStateNormal];
        [self.signInWithFacebookButton setBackgroundColor:[SWColor colorSickweatherFacebookBlue59x89x152]];
        [self.signInWithFacebookButton.layer setBorderColor:[SWColor colorSickweatherFacebookBlue59x89x152].CGColor];
        [self.signInWithFacebookButton.layer setBorderWidth:0.5];
        [self.signInWithFacebookButton.layer setCornerRadius:2];
        [self.signInWithFacebookButton addTarget:self action:@selector(signInUsingFacebook:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Config DASH BEFORE
    self.dashBeforeView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
    
    // Config OR LABEL
    self.orLabel.attributedText = [[NSAttributedString alloc] initWithString:@"OR" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12], NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104]}];
    self.orLabel.textAlignment = NSTextAlignmentCenter;
    
    // Config DASH AFTER
    self.dashAfterView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
    
    // Config SIGN IN WITH EMAIL
    [self.signInWithEmailButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"SIGN IN WITH EMAIL" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}] forState:UIControlStateNormal];
    [self.signInWithEmailButton setBackgroundColor:[SWColor colorSickweatherWhite255x255x255]];
    [self.signInWithEmailButton.layer setBorderColor:[SWColor colorSickweatherDarkGrayText104x104x104].CGColor];
    [self.signInWithEmailButton.layer setBorderWidth:0.5];
    [self.signInWithEmailButton.layer setCornerRadius:2];
    [self.signInWithEmailButton addTarget:self action:@selector(signInUsingSickweather:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config CREATE ACCOUNT BUTTON
    [self.createAccountButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"CREATE SICKWEATHER ACCOUNT" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}] forState:UIControlStateNormal];
    [self.createAccountButton setBackgroundColor:[SWColor colorSickweatherWhite255x255x255]];
    [self.createAccountButton.layer setBorderColor:[SWColor colorSickweatherDarkGrayText104x104x104].CGColor];
    [self.createAccountButton.layer setBorderWidth:0.5];
    [self.createAccountButton.layer setCornerRadius:2];
    [self.createAccountButton addTarget:self action:@selector(createSickweatherAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config TERMS OF USE TEXT VIEW
    CGFloat fontSize = 8;
    NSMutableAttributedString *termsAttributedString = [[NSMutableAttributedString alloc] init];
    [termsAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"By using Sickweather you agree to the " attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:fontSize], NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104]}]];
    [termsAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Terms of Use" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:fontSize], NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226], @"termsOfService": @"yes"}]];
    [termsAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" and " attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:fontSize], NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104]}]];
    [termsAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Privacy Policy" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:fontSize], NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226], @"privacyPolicy": @"yes"}]];
    [termsAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"." attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:fontSize], NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104]}]];
    self.termsOfUseTextView.scrollEnabled = NO; // Now has intrinsic size
    self.termsOfUseTextView.editable = NO;
    self.termsOfUseTextView.attributedText = termsAttributedString;
    self.termsOfUseTextView.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *termsOfUseTextViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsTextViewTapped:)];
    [self.termsOfUseTextView addGestureRecognizer:termsOfUseTextViewTap];
    
    // Config ACTIVITY INDICATOR
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    
    // Tell us when the facebook token changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(facebookAccessTokenDidChangeNotification:)
                                                 name:FBSDKAccessTokenDidChangeNotification
                                               object:nil];
    
    // Add bar buttons
    UIBarButtonItem *upperRightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStylePlain target:self action:@selector(upperRightBarButtonItemTapped:)];
    self.navigationItem.rightBarButtonItem = upperRightBarButtonItem;
    [self.navigationController setNavigationBarHidden:YES];
    
    // Update styling
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[SWColor colorSickweatherBlue41x171x226] forKey:NSForegroundColorAttributeName];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // Layout, then animate layout change
    [self.view layoutIfNeeded];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.formContainerVerticalHeightConstraint.constant = layoutShellHeight;
        [UIView animateWithDuration:0.65 animations:^{
            [self.view layoutIfNeeded];
        }];
    });
}

@end
