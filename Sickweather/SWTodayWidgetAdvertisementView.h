//
//  SWTodayWidgetAdvertisementView.h
//  Sickweather
//
//  Created by John Erck on 8/12/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWTodayWidgetAdvertisementViewDelegate <NSObject>
- (void)todayWidgetAdvertisementViewDidTouchUpInsideDismissButton:(id)sender;
- (void)todayWidgetAdvertisementViewDidTouchUpInsideRemindMeLaterButton:(id)sender;
@end

@interface SWTodayWidgetAdvertisementView : UIView
@property (weak, nonatomic) id<SWTodayWidgetAdvertisementViewDelegate> delegate;
@end
