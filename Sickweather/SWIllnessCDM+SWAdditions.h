//
//  SWIllnessCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 3/19/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWIllnessCDM.h"

@interface SWIllnessCDM (SWAdditions)

#pragma mark - Class Methods

+ (SWIllnessCDM *)illnessGetUpdatedRecordUsingUnique:(NSString *)unique
                                      isGroupIllness:(NSString *)isGroupIllness
                                                name:(NSString *)name
                                  illnessDescription:(NSString *)illnessDescription
                                      selfReportText:(NSString *)selfReportText
                                           sortOrder:(NSNumber *)sortOrder;
+ (SWIllnessCDM *)illnessGetRecordUsingId:(NSString *)illnessId;
+ (NSArray *)illnessGetAll;
+ (NSArray *)illnessGetAllIndividuals;
+ (NSArray *)illnessGetAllGrouped;
+ (NSArray *)illnessGetMyReports;
+ (void)illnessDeleteRecordUsingUnique:(NSString *)unique;

#pragma mark - Instance Methods

- (NSString *)nameWithWordsCapitalized;

@end
