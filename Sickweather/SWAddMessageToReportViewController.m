//
//  SWAddMessageToReportViewController.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportViewController.h"
#import "SWAddMessageToReportCollectionView.h"
#import "SWAddMessageToReportCollectionViewFlowLayout.h"
#import "SWAddMessageToReportHeaderView.h"
#import "SWAddMessageToReportCollectionViewCellForGroup.h"
#import "SWAddMessageToReportCollectionViewCellForVerticalSpacer.h"
#import "SWAddMessageToReportCollectionViewCellForShareButton.h"
#import "SWAddMessageToReportCollectionViewCellForLabel2.h"
#import "SWAddMessageToReportCollectionViewCellForLabel3.h"
#import "SWAddMessageToReportCollectionViewCellForImageView1.h"
#import "SWAddMessageToReportCollectionViewCellForUserMessageTextView.h"
#import "SWAddMessageToReportCollectionViewCellForCustomView.h"
#import "SWColor.h"
#import "SWFont.h"
#import "NSAttributedString+Height.h"
#import "SWMessageBannerView.h"
#import "SWActivityIndicatorView.h"
#import "SWReportCDM+SWAdditions.h"
#import "Flurry.h"

@interface SWAddMessageToReportViewController () <UIActionSheetDelegate, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SWAddMessageToReportMessageTableViewHeaderDelegate, SWAddMessageToReportCollectionViewCellForVerticalSpacerDelegate, UIGestureRecognizerDelegate, SWMessageBannerViewDelegate, SWAddMessageToReportCollectionViewCellForShareButtonDelegate>
@property (strong, nonatomic) NSArray *serverResponse;
@property (strong, nonatomic) NSArray *cvDataObjects;
@property (strong, nonatomic) SWAddMessageToReportCollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) SWAddMessageToReportCollectionView *collectionView;
@property (strong, nonatomic) UIView *chatBarContainerView;
@property (strong, nonatomic) UIView *chatBarContainerViewInnerBackgroundView;
@property (strong, nonatomic) UITextView *chatTextView;
@property (strong, nonatomic) UIButton *chatButtonSend;
@property (strong, nonatomic) SWAddMessageToReportHeaderView *header;
@property (strong, nonatomic) UIActionSheet *level1ActionSheet;
@property (strong, nonatomic) UIActionSheet *level2ActionSheet;
@property (strong, nonatomic) NSLayoutConstraint *chatBarContainerViewHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *spaceFromBottomChatBarContainerViewConstraint;
@property (strong, nonatomic) NSString *currentUserMessage;
@property (strong, nonatomic) NSIndexPath *userMessageTextIndexPath;
@property (assign) BOOL keyboardIsComingUp;
@property (assign) CGFloat keyboardOffset;
@end

#define DEFAULT_CHAT_CONTAINER_HEIGHT 60
#define DEFAULT_LEFT_RIGHT_PADDING_INSET 15
#define CELL_TYPE_KEY @"CELL_TYPE_KEY"
#define IS_CELL_TYPE_HEADER @"IS_CELL_TYPE_HEADER"
#define IS_CELL_TYPE_GROUP @"IS_CELL_TYPE_GROUP"
#define IS_CELL_TYPE_VERTICAL_SPACER @"IS_CELL_TYPE_VERTICAL_SPACER"
#define IS_CELL_TYPE_SHARE_BUTTON @"IS_CELL_TYPE_SHARE_BUTTON"
#define IS_CELL_TYPE_LABEL2 @"IS_CELL_TYPE_LABEL2"
#define IS_CELL_TYPE_LABEL3 @"IS_CELL_TYPE_LABEL3"
#define IS_CELL_TYPE_IMAGEVIEW1 @"IS_CELL_TYPE_IMAGEVIEW1"
#define IS_CELL_TYPE_USER_MESSAGE_TEXT_VIEW @"IS_CELL_TYPE_USER_MESSAGE_TEXT_VIEW"

@implementation SWAddMessageToReportViewController

#pragma mark - Phone Rotated Code

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.flowLayout invalidateLayout];
    [self.header setNeedsLayout];
}

#pragma mark - Keyboard Management Methods

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    SWAddMessageToReportCollectionViewCellForUserMessageTextView *cell = (SWAddMessageToReportCollectionViewCellForUserMessageTextView *)[self.collectionView cellForItemAtIndexPath:self.userMessageTextIndexPath];
    if (cell)
    {
        cell.defaultLabel.hidden = YES;
    }
    self.keyboardIsComingUp = YES;
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    self.spaceFromBottomChatBarContainerViewConstraint.constant = -kbRect.size.height;
    self.keyboardOffset = kbRect.size.height;
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
		@try
		{
		  [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.cvDataObjects.count-3 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
		}
		@catch (NSException * e) {
			[SWHelper addNonFatalExceptionWith:e];
		}
		
    }];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    /*
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    self.spaceFromBottomChatBarContainerViewConstraint.constant = -kbRect.size.height;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
     */
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    SWAddMessageToReportCollectionViewCellForUserMessageTextView *cell = (SWAddMessageToReportCollectionViewCellForUserMessageTextView *)[self.collectionView cellForItemAtIndexPath:self.userMessageTextIndexPath];
    if (cell)
    {
        if (cell.textView.attributedText.string.length == 0)
        {
            cell.defaultLabel.hidden = NO;
        }
    }
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    self.collectionView.scrollIndicatorInsets = [self helperGetDefaultCollectionViewScrollIndicatorInsets];
    self.spaceFromBottomChatBarContainerViewConstraint.constant = 0;
    self.keyboardOffset = 0;
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

// Register for notifications helper
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.collectionView])
    {
        [self.view endEditing:YES];
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.currentUserMessage = textView.text;
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([actionSheet isEqual:self.level1ActionSheet])
    {
        if (buttonIndex == 0)
        {
            // Do something...
        }
        if (buttonIndex == 1)
        {
            // Cancel
        }
    }
    if ([actionSheet isEqual:self.level2ActionSheet])
    {
        if (buttonIndex == 0)
        {
            // Do something...
        }
        if (buttonIndex == 1)
        {
            // Cancel
        }
    }
}

#pragma mark - SWAddMessageToReportMessageTableViewHeaderDelegate

#pragma mark - Custom Getters/Setters

- (void)setReports:(NSArray *)reports
{
    _reports = reports;
    [SWHelper helperDismissFullScreenActivityIndicator];
    if ([self.reports count] > 1)
    {
        // Plural
        [self addBannerWithMessage:@"Success!"];
    }
    else
    {
        // Singular
        [self addBannerWithMessage:@"Success!"];
    }
}

#pragma mark - Target/Action

- (void)chatButtonSendTapped:(id)sender
{
    [self.view endEditing:YES];
}

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.delegate addMessageToReportViewControllerWantsToDismissWithoutAddingMessageOrTaggingToAnyGroups];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    // Log to Flurry
    NSString *flurryMessage = self.currentUserMessage;
    if (!flurryMessage)
    {
        flurryMessage = @"None";
    }
    [Flurry logEvent:@"Share button in Share Your Report screen tapped" withParameters:@{@"User Message":flurryMessage}];
    
    // Talk to the network
    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
    
    /*
     group_id - Group ID // Can accept multiple group IDs as an array or single group ID as integer.
     keyword_id - Array of keyword IDs associated with illness.
     message - Message text to include in group message feed.
     report_id - Corresponds to unique ID of illness report submitted and stored in markers table.
     */
    NSString *commaSeparatedGroupIdString = [self helperGetCommaSeparatedGroupIdString];
    NSArray *commaSeparatedKeywordIdArray = @[];
    NSString *commaSeparatedKeywordIdString = @"";
    NSString *message = self.currentUserMessage;
    NSArray *commaSeparatedReportIdArray = @[];
    NSString *commaSeparatedReportIdString = @"";
    for (SWReportCDM *report in self.reports)
    {
        commaSeparatedKeywordIdArray = [commaSeparatedKeywordIdArray arrayByAddingObject:report.illness.unique];
        commaSeparatedReportIdArray = [commaSeparatedReportIdArray arrayByAddingObject:report.reportId];
    }
    commaSeparatedKeywordIdString = [commaSeparatedKeywordIdArray componentsJoinedByString:@","];
    commaSeparatedReportIdString = [commaSeparatedReportIdArray componentsJoinedByString:@","];
    [[SWHelper helperAppBackend] appBackendSubmitReportsToGroupWithMessageUsingArgs:@{@"group_id":commaSeparatedGroupIdString,@"keyword_id":commaSeparatedKeywordIdString,@"message":message,@"report_id":commaSeparatedReportIdString} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
            [SWHelper helperDismissFullScreenActivityIndicator];
            [self.delegate addMessageToReportViewControllerWantsToDismissAfterTaggingToGroupOrAddingMessage];
        });
    }];
}

#pragma mark - Helpers

- (NSString *)helperGetCommaSeparatedGroupIdString
{
    NSArray *commaArray = @[];
    for (NSDictionary *cellData in self.cvDataObjects)
    {
        if ([cellData isKindOfClass:[NSDictionary class]])
        {
            if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_GROUP])
            {
                NSMutableDictionary *serverDictWithAddOns = [cellData objectForKey:@"data"];
                NSString *yesOrNo = [serverDictWithAddOns objectForKey:@"is_selected_yes_no_string"];
                if ([yesOrNo isEqualToString:@"yes"])
                {
                    //NSLog(@"serverDictWithAddOns = %@", serverDictWithAddOns);
                    commaArray = [commaArray arrayByAddingObject:[serverDictWithAddOns objectForKey:@"id"]];
                }
            }
        }
    }
    return [commaArray componentsJoinedByString:@","];
}

- (UIEdgeInsets)helperGetDefaultCollectionViewContentInset
{
    // top, left, bottom, right
    return UIEdgeInsetsMake(0, 0, DEFAULT_LEFT_RIGHT_PADDING_INSET + self.keyboardOffset, 0);
}

- (UIEdgeInsets)helperGetDefaultCollectionViewScrollIndicatorInsets
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)helperCollectionViewWidth
{
    return self.view.bounds.size.width-(DEFAULT_LEFT_RIGHT_PADDING_INSET*2);
}

- (void)addBannerWithMessage:(NSString *)message
{
    // INIT ALL REQUIRED UI ELEMENTS
    SWMessageBannerView *messageBannerView = [[SWMessageBannerView alloc] init];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    messageBannerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:messageBannerView];
    
    // CONFIG
    
    // Config MESSAGE BANNER VIEW
    messageBannerView.delegate = self;
    [messageBannerView setMessage:message];
    
    // INIT AUTO LAYOUT VIEWS DICT
    NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
    [viewsDictionary setObject:messageBannerView forKey:@"messageBannerView"];
    
    // LAYOUT
    
    // Layout MESSAGE BANNER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[messageBannerView]|"] options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[messageBannerView(44)]"] options:0 metrics:0 views:viewsDictionary]];
}

#pragma mark - SWMessageBannerViewDelegate

- (void)messageBannerViewWantsToDismiss:(id)sender
{
    // We intentionally just leave the banner up
    
    /*
    //NSLog(@"messageBannerViewWantsToDismiss");
    if ([sender isKindOfClass:[SWMessageBannerView class]])
    {
        SWMessageBannerView *messageBannerView = (SWMessageBannerView *)sender;
        [UIView animateWithDuration:1.0 animations:^{
            messageBannerView.alpha = 0;
        } completion:^(BOOL finished) {
            [messageBannerView removeFromSuperview];
        }];
    }
    */
}

#pragma mark - SWAddMessageToReportCollectionViewCellForVerticalSpacerDelegate

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_GROUP])
        {
            NSMutableDictionary *serverDictWithAddOns = [cellData objectForKey:@"data"];
            NSString *yesOrNo = [serverDictWithAddOns objectForKey:@"is_selected_yes_no_string"];
            if ([yesOrNo isEqualToString:@"yes"])
            {
                [serverDictWithAddOns setObject:@"no" forKey:@"is_selected_yes_no_string"];
            }
            else
            {
                [serverDictWithAddOns setObject:@"yes" forKey:@"is_selected_yes_no_string"];
            }
            [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cvDataObjects.count;
}

#pragma mark - SWAddMessageToReportCollectionViewCellForShareButtonDelegate

- (void)addMessageToReportCollectionViewCellForShareButtonDelegateButtonTapped:(SWAddMessageToReportCollectionViewCellForShareButton *)sender
{
    [self rightBarButtonItemTapped:self];
}

#pragma mark - Life Cycle Methods

- (void)helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse
{
    NSArray *myGroupsFromServer = [[self.latestMenuServerResponse objectForKey:@"my_groups"] objectForKey:@"groups"];
    if (myGroupsFromServer)
    {
        // This is where we order things for the collection view's UI!!!!
        self.cvDataObjects = @[];
        
        // Always start with header
        self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:IS_CELL_TYPE_HEADER,@"data":@""}];
        
        //self.serverGroupDict <-- This is the one that the user clicked through on to get here...
        
        // Then add in message info...
        for (NSDictionary *myGroup in myGroupsFromServer)
        {
            NSUInteger index = [myGroupsFromServer indexOfObject:myGroup];
            NSMutableDictionary *mutableDict = [myGroup mutableCopy];
            if (myGroupsFromServer.count == 1)
            {
                [mutableDict setObject:@"cap_type_single_record" forKey:@"cap_type"];
            }
            else if (index == 0)
            {
                [mutableDict setObject:@"cap_type_top" forKey:@"cap_type"];
            }
            else if (index == myGroupsFromServer.count - 1)
            {
                [mutableDict setObject:@"cap_type_bottom" forKey:@"cap_type"];
            }
            else
            {
                [mutableDict setObject:@"cap_type_middle" forKey:@"cap_type"];
            }
            if ([[myGroup objectForKey:@"id"] isEqualToString:[self.serverGroupDict objectForKey:@"id"]])
            {
                [mutableDict setObject:@"yes" forKey:@"is_selected_yes_no_string"];
            }
            self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:IS_CELL_TYPE_GROUP,@"data":mutableDict}];
        }
        
        // Add vertical spacer cell
        self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:IS_CELL_TYPE_VERTICAL_SPACER,@"data":@""}];
        
        // Add text area for user message
        self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:IS_CELL_TYPE_USER_MESSAGE_TEXT_VIEW,@"data":@""}];
        self.userMessageTextIndexPath = [NSIndexPath indexPathForItem:[self.cvDataObjects count]-1 inSection:0];
        
        // Add vertical spacer cell
        self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:IS_CELL_TYPE_VERTICAL_SPACER,@"data":@""}];
        
        // Add text area for user message
        self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:IS_CELL_TYPE_SHARE_BUTTON,@"data":@""}];
    }
    else
    {
        //NSLog(@"What to do, they have no my_groups ???");
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_HEADER])
        {
            if (!self.header)
            {
                self.header = [[SWAddMessageToReportHeaderView alloc] init];
                self.header.delegate = self;
            }
            SWAddMessageToReportCollectionViewCellForCustomView *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForCustomView" forIndexPath:indexPath];
            [myCell addMessageToReportCollectionViewCellForCustomViewSetView:self.header];
            //myCell.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_IMAGEVIEW1])
        {
            SWAddMessageToReportCollectionViewCellForImageView1 *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForImageView1" forIndexPath:indexPath];
            [myCell addMessageToReportCollectionViewCellForImageView1SetImage:[UIImage imageNamed:@"avatar-empty"]];
            //myCell.backgroundColor = [SWColor color:[UIColor orangeColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_SHARE_BUTTON])
        {
            //NSDictionary *myGroup = [cellData objectForKey:@"data"];
            //NSLog(@"myGroup = %@", myGroup);
            SWAddMessageToReportCollectionViewCellForShareButton *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForShareButton" forIndexPath:indexPath];
            myCell.delegate = self;
            //myCell.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_LABEL2])
        {
            SWAddMessageToReportCollectionViewCellForLabel2 *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForLabel2" forIndexPath:indexPath];
            [myCell addMessageToReportCollectionViewCellForLabel2SetLabel2AttributedString:[cellData objectForKey:@"data"]];
            //myCell.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_LABEL3])
        {
            SWAddMessageToReportCollectionViewCellForLabel3 *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForLabel3" forIndexPath:indexPath];
            //[myCell addMessageToReportCollectionViewCellForLabel3SetLabel3AttributedString:];
            //myCell.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_USER_MESSAGE_TEXT_VIEW])
        {
            SWAddMessageToReportCollectionViewCellForUserMessageTextView *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForUserMessageTextView" forIndexPath:indexPath];
            myCell.textView.delegate = self;
            //[myCell addMessageToReportCollectionViewCellForUserMessageTextViewSetUserMessageTextViewAttributedString:[cellData objectForKey:@"data"]];
            //myCell.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_GROUP])
        {
            NSMutableDictionary *serverDictWithAddOns = [cellData objectForKey:@"data"];
            SWAddMessageToReportCollectionViewCellForGroup *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForGroup" forIndexPath:indexPath];
            [myCell addMessageToReportCollectionViewCellForGroupUpdateDefaultInsetsLeft:@0 right:@0 top:@0 bottom:@0];
            [myCell setServerDict:serverDictWithAddOns];
            if ([[serverDictWithAddOns objectForKey:@"cap_type"] isEqualToString:@"cap_type_single_record"])
            {
                [myCell setStyleAsSingleRecordCap];
            }
            if ([[serverDictWithAddOns objectForKey:@"cap_type"] isEqualToString:@"cap_type_top"])
            {
                [myCell setStyleAsTopCap];
            }
            if ([[serverDictWithAddOns objectForKey:@"cap_type"] isEqualToString:@"cap_type_bottom"])
            {
                [myCell setStyleAsBottomCap];
            }
            if ([[serverDictWithAddOns objectForKey:@"cap_type"] isEqualToString:@"cap_type_middle"])
            {
                [myCell setStyleAsCenterCap];
            }
            if ([[serverDictWithAddOns objectForKey:@"is_selected_yes_no_string"] isEqualToString:@"yes"])
            {
                [myCell setStyleAsSelected];
            }
            else
            {
                [myCell setStyleAsUnselected];
            }
            //myCell.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_VERTICAL_SPACER])
        {
            SWAddMessageToReportCollectionViewCellForVerticalSpacer *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForVerticalSpacer" forIndexPath:indexPath];
            myCell.delegate = self;
            //[myCell addMessageToReportCollectionViewCellForVerticalSpacerSetMarginLeft:@(-DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT) right:@(-DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT)];
            //myCell.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
            cell = myCell;
        }
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_HEADER])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 130);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_IMAGEVIEW1])
        {
            size = CGSizeMake(20, 20);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_SHARE_BUTTON])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 58);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_LABEL2])
        {
            NSAttributedString *created = [cellData objectForKey:@"data"];
            size = CGSizeMake([created size].width + 04, 20);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_LABEL3])
        {
            NSAttributedString *keyword = [cellData objectForKey:@"data"];
            size = CGSizeMake([keyword size].width + 20, 20);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_USER_MESSAGE_TEXT_VIEW])
        {
            //NSAttributedString *message = [cellData objectForKey:@"data"];
            size = CGSizeMake([self helperCollectionViewWidth], 100);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_GROUP])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 58);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_CELL_TYPE_VERTICAL_SPACER])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 40);
        }
    }
    return size;
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT DEFAULTS
    self.flowLayout = [[SWAddMessageToReportCollectionViewFlowLayout alloc] init];
    self.keyboardOffset = 0;
    self.currentUserMessage = @"";
    
    // Cover with spinner by default (until we get reports)
    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
    
    // Add tap (for dismissing keyboard)
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)];
    [tapGesture setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapGesture];
    [self registerForKeyboardNotifications];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.collectionView = [[SWAddMessageToReportCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    self.chatBarContainerView = [[UIView alloc] init];
    self.chatBarContainerViewInnerBackgroundView = [[UIView alloc] init];
    self.chatTextView = [[UITextView alloc] init];
    self.chatButtonSend = [[UIButton alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.collectionView forKey:@"collectionView"];
    [self.viewsDictionary setObject:self.chatBarContainerView forKey:@"chatBarContainerView"];
    [self.viewsDictionary setObject:self.chatBarContainerViewInnerBackgroundView forKey:@"chatBarContainerViewInnerBackgroundView"];
    [self.viewsDictionary setObject:self.chatTextView forKey:@"chatTextView"];
    [self.viewsDictionary setObject:self.chatButtonSend forKey:@"chatButtonSend"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatBarContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatBarContainerViewInnerBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatTextView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatButtonSend.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.chatBarContainerView];
    [self.chatBarContainerView addSubview:self.chatBarContainerViewInnerBackgroundView];
    [self.chatBarContainerView addSubview:self.chatTextView];
    [self.chatBarContainerView addSubview:self.chatButtonSend];
    
    // LAYOUT
    
    // Layout COLLECTION VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CHAT BAR CONTAINER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[chatBarContainerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[chatBarContainerView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.spaceFromBottomChatBarContainerViewConstraint = [NSLayoutConstraint constraintWithItem:self.chatBarContainerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    [self.view addConstraint:self.spaceFromBottomChatBarContainerViewConstraint];
    self.chatBarContainerViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.chatBarContainerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:DEFAULT_CHAT_CONTAINER_HEIGHT];
    [self.view addConstraint:self.chatBarContainerViewHeightConstraint];
    
    // Layout CHAT BAR CONTAINER VIEW INNER BACKGROUND VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[chatBarContainerViewInnerBackgroundView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(1)-[chatBarContainerViewInnerBackgroundView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CHAT TEXT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[chatTextView]-[chatButtonSend]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[chatTextView]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CHAT BUTTON SEND
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[chatButtonSend]-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[chatButtonSend]-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorChatBoxGray];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // CREATE AND SET INTO NAVIGATION ITEM
    self.navigationItem.title = @"Share Your Report";
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemTapped:)];
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(rightBarButtonItemTapped:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config COLLECTION VIEW
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    self.collectionView.scrollIndicatorInsets = [self helperGetDefaultCollectionViewScrollIndicatorInsets];
    self.collectionView.scrollsToTop = YES;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForCustomView class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForCustomView"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForShareButton class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForShareButton"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForLabel2 class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForLabel2"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForLabel3 class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForLabel3"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForGroup class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForGroup"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForVerticalSpacer class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForVerticalSpacer"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForUserMessageTextView class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForUserMessageTextView"];
    [self.collectionView registerClass:[SWAddMessageToReportCollectionViewCellForImageView1 class] forCellWithReuseIdentifier:@"SWAddMessageToReportCollectionViewCellForImageView1"];
    
    // Config CHAT BAR CONTAINER VIEW
    self.chatBarContainerView.backgroundColor = [SWColor colorChatBoxGrayAccent];
    self.chatBarContainerView.hidden = YES; // Drop from this view controller, came over from copy/paste
    
    // Config CHAT BAR CONTAINER VIEW INNER BACKGROUND VIEW
    self.chatBarContainerViewInnerBackgroundView.backgroundColor = [SWColor colorChatBoxGray];
    
    // Config CHAT TEXT VIEW
    self.chatTextView.delegate = self;
    self.chatTextView.layer.cornerRadius = 5;
    self.chatTextView.layer.borderColor = [SWColor colorChatBoxGrayAccent].CGColor;
    self.chatTextView.layer.borderWidth = 1;
    self.chatTextView.scrollsToTop = NO;
    
    // Config CHAT BUTTON SEND
    [self.chatButtonSend setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Send" attributes:@{NSForegroundColorAttributeName:[SWColor colorChatBoxGray],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}] forState:UIControlStateNormal];
    [self.chatButtonSend addTarget:self action:@selector(chatButtonSendTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Start with empty green stripe...
    [self addBannerWithMessage:@"Adding to map..."];
    
    // GET DATA FROM SERVER, UPDATE UI
    [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse];
    
    // COLOR FOR DEVELOPMENT PURPOSES
    //self.collectionView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
}

@end
