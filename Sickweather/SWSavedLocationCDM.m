//
//  SWSavedLocationCDM.m
//  Sickweather
//
//  Created by John Erck on 5/15/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSavedLocationCDM.h"


@implementation SWSavedLocationCDM

@dynamic placemarkObjectData;
@dynamic savedLocationId;

@end
