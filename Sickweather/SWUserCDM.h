//
//  SWUserCDM.h
//  Sickweather
//
//  Created by John Erck on 10/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SWReportCDM;

NS_ASSUME_NONNULL_BEGIN

@interface SWUserCDM : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SWUserCDM+CoreDataProperties.h"
