//
//  SWFonts.m
//  Sickweather
//
//  Created by John Erck on 1/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWFont.h"

@implementation SWFont

+ (void)fontLogAllOptions
{
    for (NSString* family in [UIFont familyNames])
    {
        //NSLog(@"%@", family);
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
}

+ (UIFont *)fontDuepuntozeroRegularFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Duepuntozero" size:size];
}

+ (UIFont *)fontDuepuntozeroBlackFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"DuepuntozeroBlack" size:size];
}

+ (UIFont *)fontDuepuntozeroBoldFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Duepuntozerobold" size:size];
}

+ (UIFont *)fontStandardFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Raleway" size:size];
}

+ (UIFont *)fontStandardBoldFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Raleway-SemiBold" size:size];
}

+ (UIFont *)fontAwesomeOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"FontAwesome" size:size];
}

@end
