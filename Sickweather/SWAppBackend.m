//
//  SWAPIFetcher.m
//  Sickweather
//
//  Created by John Erck on 3/3/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWAppBackend.h"
#import "SWSettings.h"
#import "SWUserCDM+SWAdditions.h"
#import "NSString+SWAdditions.h"
#import "Flurry.h"
#import <OneSignal/OneSignal.h>

@interface SWAppBackend () <NSURLSessionDelegate>
@property (strong, nonatomic) NSURLSession *sharedBackgroundSessionForFileUploadProp;
@property (strong, nonatomic) NSMutableArray *outstandingTasks; // of NSURLSessionTask
@property (strong, nonatomic) NSMutableArray *outstandingTaskCompletionHandlers; // of SWAppBackendJSONCompletionHandler
@property (strong, nonatomic) NSMutableArray *outstandingTaskResponses; // of NSMutableArray of NSData
@end

@implementation SWAppBackend

#pragma mark - Customer Getters and Setters

- (NSMutableArray *)outstandingTasks
{
    if (!_outstandingTasks) {
        _outstandingTasks = [[NSMutableArray alloc] init];
    }
    return _outstandingTasks;
}

- (NSMutableArray *)outstandingTaskCompletionHandlers
{
    if (!_outstandingTaskCompletionHandlers) {
        _outstandingTaskCompletionHandlers = [[NSMutableArray alloc] init];
    }
    return _outstandingTaskCompletionHandlers;
}

- (NSMutableArray *)outstandingTaskResponses
{
    if (!_outstandingTaskResponses) {
        _outstandingTaskResponses = [[NSMutableArray alloc] init];
    }
    return _outstandingTaskResponses;
}

#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if ([session isEqual:self.sharedBackgroundSessionForFileUploadProp])
    {
        for (NSURLSessionTask *outstandingTask in self.outstandingTasks)
        {
            if ([outstandingTask isEqual:task])
            {
                NSUInteger indexkey = [self.outstandingTasks indexOfObject:outstandingTask];
                NSMutableData *responseData = [NSMutableData data];
                NSMutableArray *responseDatas = [self.outstandingTaskResponses objectAtIndex:indexkey];
                for (NSData *responseDataItem in responseDatas) {
                    [responseData appendData:responseDataItem];
                }
                NSDictionary *jsonResponseBody = nil;
                NSString *responseBodyAsString = nil;
                NSHTTPURLResponse *responseHeaders = nil;
                NSError *requestError = error;
                NSError *jsonParseError = nil;
                NSString *requestURLAbsoluteString = nil;
                jsonResponseBody = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonParseError];
                if (!jsonResponseBody)
                {
                    NSLog(@"Error parsing JSON: %@", jsonParseError);
                }
                responseBodyAsString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                SWAppBackendJSONCompletionHandler completionHandler = [self.outstandingTaskCompletionHandlers objectAtIndex:indexkey];
                completionHandler(jsonResponseBody, responseBodyAsString, responseHeaders, requestError, jsonParseError, requestURLAbsoluteString);
                [self.outstandingTaskCompletionHandlers removeObjectAtIndex:indexkey];
                [self.outstandingTaskResponses removeObjectAtIndex:indexkey];
                [self.outstandingTasks removeObjectAtIndex:indexkey];
                if (self.outstandingTasks.count == 0)
                {
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                }
            }
        }
    }
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
 needNewBodyStream:(void (^)(NSInputStream *bodyStream))completionHandler
{
    
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
willPerformHTTPRedirection:(NSHTTPURLResponse *)response
        newRequest:(NSURLRequest *)request
 completionHandler:(void (^)(NSURLRequest *))completionHandler
{
    
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
    
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    NSString *dataAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSLog(@"dataAsString = %@", dataAsString);
    if ([session isEqual:self.sharedBackgroundSessionForFileUploadProp])
    {
        for (NSURLSessionTask *outstandingTask in self.outstandingTasks)
        {
            if ([outstandingTask isEqual:dataTask])
            {
                NSUInteger indexkey = [self.outstandingTasks indexOfObject:outstandingTask];
                NSMutableArray *responseDataObjects = [[NSMutableArray alloc] init];
                if ([self.outstandingTaskResponses count] > indexkey)
                {
                    responseDataObjects = [self.outstandingTaskResponses objectAtIndex:indexkey];
                }
                else
                {
                    [self.outstandingTaskResponses addObject:responseDataObjects];
                }
                [responseDataObjects addObject:data];
            }
        }
    }
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
 willCacheResponse:(NSCachedURLResponse *)proposedResponse
 completionHandler:(void (^)(NSCachedURLResponse *cachedResponse))completionHandler
{
    
}

#pragma mark - Helpers

- (NSURLSession *)sharedDefaultSessionForAppBackendImageDownload
{
    static NSURLSession *sharedDefaultSessionForAppBackendImageDownload = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.URLCache = [NSURLCache sharedURLCache];
        configuration.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
        sharedDefaultSessionForAppBackendImageDownload = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    });
    return sharedDefaultSessionForAppBackendImageDownload;
}

- (NSURLSession *)sharedBackgroundSessionForFileUpload
{
    if (self.sharedBackgroundSessionForFileUploadProp)
    {
        return self.sharedBackgroundSessionForFileUploadProp;
    }
    NSString *backgroundKey = [NSString stringWithFormat:@"%@.fileupload-background-session-identifier", [[NSBundle mainBundle] bundleIdentifier]];
    NSURLSessionConfiguration *configuration;
    configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:backgroundKey];
    configuration.HTTPAdditionalHeaders = @{@"Content-Type": [NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self formBoundary]]};
    self.sharedBackgroundSessionForFileUploadProp = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    return self.sharedBackgroundSessionForFileUploadProp;
}

- (void)postBodyAddFileUploadDataUsingPostArgName:(NSString *)postArgName fileName:(NSString *)fileName contentType:(NSString *)contentType data:(NSData *)data postRequestBody:(NSMutableData *)postRequestBody
{
    /*
     ------WebKitFormBoundaryIKFDqrAnm5Uw6LFS
     Content-Disposition: form-data; name="file"; filename="DSC_0057.jpg"
     Content-Type: image/jpeg
     
     
     */
    [postRequestBody appendData:[[NSString stringWithFormat:@"--%@\r\n", [self formBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postRequestBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", postArgName, fileName] dataUsingEncoding:NSUTF8StringEncoding]]; // file, DSC_0057.jpg
    [postRequestBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", contentType] dataUsingEncoding:NSUTF8StringEncoding]]; // image/jpeg
    [postRequestBody appendData:data];
    [postRequestBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void)appendStringArgWithName:(NSString *)name value:(NSString *)value postRequestBody:(NSMutableData *)postRequestBody
{
    /*
     ------WebKitFormBoundaryIKFDqrAnm5Uw6LFS
     Content-Disposition: form-data; name="title"
     
     The Great China
     */
    [postRequestBody appendData:[[NSString stringWithFormat:@"--%@\r\n", [self formBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postRequestBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", name] dataUsingEncoding:NSUTF8StringEncoding]];
    [postRequestBody appendData:[[NSString stringWithFormat:@"%@\r\n", [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void)postBodyAddImageArgWithName:(NSString *)name image:(UIImage *)image postRequestBody:(NSMutableData *)postRequestBody
{
    // Convert UIImage to NSData
    NSData *imageData = UIImageJPEGRepresentation(image, 0.75);
    
    // If successful
    if (imageData)
    {
        /*
         ------WebKitFormBoundaryIKFDqrAnm5Uw6LFS
         Content-Disposition: form-data; name="file"; filename="file.JPG"
         Content-Type: image/jpeg
         
         
         */
        [postRequestBody appendData:[[NSString stringWithFormat:@"--%@\r\n", [self formBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
        [postRequestBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpg\"\r\n", name, name] dataUsingEncoding:NSUTF8StringEncoding]];
        [postRequestBody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postRequestBody appendData:imageData];
        [postRequestBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (NSString *)formBoundary
{
    return @"SickweatherFormBoundaryABCDEFGHIJKLMNOP";
}

#pragma mark - Public Methods

- (void)appBackendWriteDataFileNamed:(NSString *)fileName fileContentType:(NSString *)fileContentType fileContent:(NSData *)fileContent usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://logs.abovemarket.com/sickweather/writefileupload"];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create url request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self formBoundary]] forHTTPHeaderField:@"Content-Type"];
    
    // Create POST body data object (start fresh each call)
    NSMutableData *postRequestBody = [NSMutableData data];
    
    // Set request args
    [self appendStringArgWithName:@"api_key" value:@"C2x932866b974APN" postRequestBody:postRequestBody];
    
    // Add data
    [self postBodyAddFileUploadDataUsingPostArgName:@"file" fileName:fileName contentType:fileContentType data:fileContent postRequestBody:postRequestBody];
    
    // End body
    // ------WebKitFormBoundaryIKFDqrAnm5Uw6LFS--
    [postRequestBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", [self formBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Flip activity indicator on
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    // Save post request data as file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; // Get the docs directory
    NSString *postRequestBodyDataFileName = [NSString stringWithFormat:@"swfile.data"];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:postRequestBodyDataFileName]; // Add the file name.
    [postRequestBody writeToFile:filePath atomically:YES]; // Write the file
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath]; // Get as URL
    
    // Create the upload task
    NSURLSessionUploadTask *uploadTask = [[self sharedBackgroundSessionForFileUpload] uploadTaskWithRequest:request fromFile:fileURL];
    
    // Go
    [uploadTask resume];
    [self.outstandingTasks addObject:uploadTask];
    [self.outstandingTaskCompletionHandlers addObject:completionHandler];
}

- (void)appBackendWriteFileNamed:(NSString *)fileName fileContent:(NSString *)fileContent usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    NSString *post = [NSString stringWithFormat:@"api_key=%@&file_name=%@&file_content=%@", [SWHelper helperURLEncode:[SWHelper helperSickweatherAPIKey]], [SWHelper helperURLEncode:fileName], [SWHelper helperURLEncode:fileContent]];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://logs.abovemarket.com/sickweather/write"];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendLogSelfReportToSickweatherServerForLat:(NSString *)lat lon:(NSString *)lon reportText:(NSString *)reportText identifier:(NSString *)identifier visible:(NSString *)visibleZeroOrOne usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // e.g. reportText = "cough"
    // e.g. id = 6
    
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/selfReport.php?ehash=%@&phash=%@&api_key=%@&lat=%@&lon=%@&report=%@&id=%@&visible=%@", [SWHelper helperGetCurrentUser].emailHash, [SWHelper helperGetCurrentUser].passwordHash, [SWHelper helperAppBackend].appBackendSickweatherAPIKey, lat, lon, [reportText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]], identifier, visibleZeroOrOne];
    //urlString = [NSString stringWithFormat:@"http://54.157.247.68/ws/v1.1/mobile/selfReport.php?ehash=%@&phash=%@&api_key=%@&lat=%@&lon=%@&report=%@&id=%@&visible=%@", [SWHelper helperGetCurrentUser].emailHash, [SWHelper helperGetCurrentUser].passwordHash, [SWHelper helperAppBackend].appBackendSickweatherAPIKey, lat, lon, [reportText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]], identifier, visibleZeroOrOne];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendSearchForMedicationUsingArgs:(NSString *)name usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler{
	NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
	
		// Add headers
	config.HTTPAdditionalHeaders = @{};
	
		// Create session using config
	NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
		// INIT ARGS DICT BASED ON USER DICT
	NSMutableDictionary *args = [[NSMutableDictionary alloc] init];
	
		// ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
	[args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
	[args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
	[args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
	[args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
	
		// ADD OTHER REQUIRED INPUTS
	[args setObject:name forKey:@"search"];
	
	NSString *queryStringArgValues = @"";
	for (NSString *key in args)
		{
			//NSLog(@"key = %@", key);
		NSString *value = [args objectForKey:key];
			//NSLog(@"value = %@", value);
		queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
		}
	if (queryStringArgValues.length > 0)
		{
		queryStringArgValues = [queryStringArgValues substringFromIndex:1];
		}
	NSLog(@"queryStringArgValues = %@", queryStringArgValues);
	
		// Define url
	NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/trackers/searchMedications.php?%@", queryStringArgValues];
	NSURL *url = [NSURL URLWithString:urlString];
	
		// Create URL request
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setHTTPMethod:@"GET"];
	
		// Create and execute data task
	NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
			// Declare baseline
		NSDictionary *jsonResponseBody;
		NSString *responseBodyAsString;
		NSHTTPURLResponse *httpResponse;
		NSError *requestError;
		NSError *jsonParseError;
		NSString *requestURLAbsoluteString;
		// Assign accordingly
		if (data != nil) {
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
		httpResponse = (NSHTTPURLResponse *)response;
		requestError = error;
		requestURLAbsoluteString = [request URL].absoluteString;
		
		// Call completion handler
		completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
	}];
	[dataTask resume];
	
		// Manage memory
	[session finishTasksAndInvalidate];
}


- (void)appBackendSubmitReportToSickweatherServerForLat:(NSString *)lat lon:(NSString *)lon reportText:(NSString *)reportText identifier:(NSString *)identifier visible:(NSString *)visibleZeroOrOne familyArgs:(NSMutableDictionary*)familyArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // e.g. reportText = "cough"
    // e.g. id = 6
    
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [[NSMutableDictionary alloc] init];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    
    // ADD OTHER REQUIRED INPUTS
    [args setObject:lat forKey:@"lat"];
    [args setObject:lon forKey:@"lon"];
    [args setObject:[reportText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] forKey:@"report"];
    [args setObject:identifier forKey:@"id"];
    [args setObject:visibleZeroOrOne forKey:@"visible"];
    
    // ADD OPTIONAL INPUTS
    [args addEntriesFromDictionary:familyArgs];
    
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/selfReport.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
		if (data != nil) {
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetForecastForLat:(NSString *)lat lon:(NSString *)lon loc:(NSString *)loc zip:(NSString *)zip usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/getForecast.php?lat=%@&lon=%@&loc=%@&zip=%@&api_key=%@", lat, lon, [loc stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], zip, [SWHelper helperURLEncode:[SWHelper helperSickweatherAPIKey]]];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetLocallyCachedImageForStringURL:(NSString *)stringURL alwaysUsingCompletionHandler:(SWAppBackendImageCompletionHandler)completionHandler
{
    NSURLSessionDataTask *dataTask = [[self sharedDefaultSessionForAppBackendImageDownload] dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        UIImage *image = [UIImage imageWithData:data];
        completionHandler(image, (NSHTTPURLResponse *)response, error);
    }];
    [dataTask resume];
}

- (void)appBackendGetSickScoreInRadiusForLat:(CGFloat)lat lon:(CGFloat)lon usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/sickscore/getSickScoreInRadius.php?api_key=%@&lat=%f&lon=%f", [SWHelper helperSickweatherAPIKey], lat, lon];
    NSLog(@"urlString = %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil) // <-- Before adding this, we had 1,936 app crashes for App Store version v3.6.2 alone
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Flurry logEvent:@"App Crash Averted - getSickScoreInRadius.php returned nil" withParameters:@{@"URL": urlString}];
            });
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetCovidScoreForLat:(CGFloat)lat lon:(CGFloat)lon usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://covid-api.sickweather.com/api/cvscore.php?api_key=%@&lat=%f&lon=%f",
        [SWHelper helperSickweatherAPIKey], lat, lon];
    NSLog(@"urlString = %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil) // <-- Before adding this, we had 1,936 app crashes for App Store version v3.6.2 alone
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"covidScore: %@", responseBodyAsString);
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Flurry logEvent:@"App Crash Averted - getCovidScore.php returned nil" withParameters:@{@"URL": urlString}];
            });
        }
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonResponseBody
//                                                      options:(NSJSONWritingOptions)    (true ? NSJSONWritingPrettyPrinted : 0)
//                                                        error:&error];
//
//        if (! jsonData) {
//           NSLog(@"%s: error: %@", __func__, error.localizedDescription);
//        } else {
//           NSLog(@"covidScore: %@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
//        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetCovidContentByKeyword:(NSString *)illnesId usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    //https://covid-api.sickweather.com/api/get-content-by-keyword.php.php?keyword_id=6&api_key=7HjdT29MsuZ5&locale=en
    //NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/library/getContentByKeyword.php?api_key=%@&keyword_id=%@", [SWHelper helperSickweatherAPIKey], illnesId];
    NSString *urlString = [NSString stringWithFormat:@"https://covid-api.sickweather.com/api/get-content-by-keyword.php?api_key=%@&keyword_id=%@&locale=en", [SWHelper helperSickweatherAPIKey], illnesId];
    //NSString *urlString = [NSString", [SWHelper helperSickweatherAPIKey], illnesId];
    NSLog(@"urlString = %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil) // <-- Before adding this, we had 1,936 app crashes for App Store version v3.6.2 alone
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"randolf-contentByKeyword: %@", jsonResponseBody);
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Flurry logEvent:@"App Crash Averted - getSickScoreInRadius.php returned nil" withParameters:@{@"URL": urlString}];
            });
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetContentByKeyword:(NSString *)illnesId usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/library/getContentByKeyword.php?api_key=%@&keyword_id=%@", [SWHelper helperSickweatherAPIKey], illnesId];
    //NSLog(@"urlString = %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil) // <-- Before adding this, we had 1,936 app crashes for App Store version v3.6.2 alone
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Flurry logEvent:@"App Crash Averted - getSickScoreInRadius.php returned nil" withParameters:@{@"URL": urlString}];
            });
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendUpdateLocationForUserUsingLat:(CGFloat)lat lon:(CGFloat)lon extraArgs:(NSDictionary *)extraArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        //NSLog(@"userId = %@ pushToken = %@", userId, pushToken);
        
        // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        
        // Add headers
        config.HTTPAdditionalHeaders = @{};
        
        // Create session using config
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        // INIT ARGS DICT BASED ON USER DICT
        NSMutableDictionary *args = [extraArgs mutableCopy];
        
        // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
        [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
        [args setObject:[@(lat) stringValue] forKey:@"lat"];
        [args setObject:[@(lon) stringValue] forKey:@"lon"];
        
        // os_push_id
        if ([userId length] > 0)
        {
            [args setObject:userId forKey:@"os_push_id"];
        }
        
        // SIGN IF LOGGED IN
        if ([SWUserCDM currentlyLoggedInUser])
        {
            [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
            [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
            [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
        }
        NSString *queryStringArgValues = @"";
        for (NSString *key in args)
        {
            //NSLog(@"key = %@", key);
            NSString *value = [args objectForKey:key];
            //NSLog(@"value = %@", value);
            queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
        }
        if (queryStringArgValues.length > 0)
        {
            queryStringArgValues = [queryStringArgValues substringFromIndex:1];
        }
        //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
        
        // Define url
        NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/updateLocation.php?%@", queryStringArgValues];
//        NSLog(@"urlString = %@", urlString);
        NSURL *url = [NSURL URLWithString:urlString];
        
        // Create URL request
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        
        // Create and execute data task
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // Declare baseline
            NSDictionary *jsonResponseBody;
            NSString *responseBodyAsString;
            NSHTTPURLResponse *httpResponse;
            NSError *requestError;
            NSError *jsonParseError;
            NSString *requestURLAbsoluteString;
            // Assign accordingly
            if (data != nil) // <-- Before adding this, we had 1,936 app crashes for App Store version v3.6.2 alone
            {
                jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
                responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            }
            httpResponse = (NSHTTPURLResponse *)response;
            requestError = error;
            requestURLAbsoluteString = [request URL].absoluteString;
            // Call completion handler
            completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
        }];
        [dataTask resume];
        
        // Manage memory
        [session finishTasksAndInvalidate];
    }];
}

- (void)appBackendGetCovidMarkersInRadiusForIllnessTypeID:illnessTypeId lat:(CGFloat)lat lon:(CGFloat)lon limit:(NSNumber *)limit usingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *baseEndpointURL = @"https://covid-api.sickweather.com/api/get-covid-markers-in-radius.php";
    NSString *urlString = [NSString stringWithFormat:@"%@?ids=%@&lat=%f&lon=%f&limit=%@&api_key=%@&ehash=%@&phash=%@&locale=en", baseEndpointURL, illnessTypeId, lat, lon, limit, [SWHelper helperSickweatherAPIKey], [SWHelper helperGetCurrentUser].emailHash, [SWHelper helperGetCurrentUser].passwordHash];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
      //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
      //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetMarkersInRadiusForIllnessTypeID:illnessTypeId lat:(CGFloat)lat lon:(CGFloat)lon limit:(NSNumber *)limit usingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *baseEndpointURL = @"https://mobilesvc.sickweather.com/ws/v1.1/getMarkersInRadius.php";
    //baseEndpointURL = @"http://54.157.247.68/ws/v1.1/getMarkersInRadius.php";
    NSString *urlString = [NSString stringWithFormat:@"%@?ids=%@&lat=%f&lon=%f&limit=%@&api_key=%@&ehash=%@&phash=%@", baseEndpointURL, illnessTypeId, lat, lon, limit, [SWHelper helperSickweatherAPIKey], [SWHelper helperGetCurrentUser].emailHash, [SWHelper helperGetCurrentUser].passwordHash];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
      //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
      //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetSponsoredMarkersInRadiusForIllnessIds:(NSString *)illnessIds lat:(NSString *)lat lon:(NSString *)lon limit:(NSString *)limit usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *baseEndpointURL = @"https://mobilesvc.sickweather.com/ws/v1.1/getSponsoredMarkersInRadius.php";
    //baseEndpointURL = @"http://megan.sickweather.com/ws/v1.1/getSponsoredMarkersInRadius.php";
    NSString *urlString = [NSString stringWithFormat:@"%@?ids=%@&lat=%@&lon=%@&limit=%@&api_key=%@&scale=%@&v=%@&client=ios", baseEndpointURL, [illnessIds urlencode], [lat urlencode], [lon urlencode], [limit urlencode], self.appBackendSickweatherAPIKey, @([[UIScreen mainScreen] scale]), [[SWHelper helperAppVersionString] urlencode]];
    //urlString = @"http://megan.sickweather.com/ws/v1.1/getSponsoredMarkersInRadius.php?ids=2,4,1,15,33,7&lat=32.87906646729&lon=-111.68012237549&limit=250&api_key=oWCYcO9KTEdnMduhBE6NHcDdVxHAzGBN&scale=2&v=v3.3.1%20%232&client=ios";
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        //data = [[NSString stringWithFormat:@"[{\"id\":\"9646\",\"advertiser_id\":\"2\",\"title\":\"Walgreens\",\"display_text\":\"19501 Beach Blvd\\nHuntington Beach, CA 92648\",\"marker_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-marker_walgreens@2x.png\",\"popup_logo\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-popup-logo_walgreens@2x.png\",\"url\":\"Walgreens.com\",\"url_link\":\"http://www.walgreens.com/locator/walgreens-19501+beach+blvd-huntington+beach-ca-92648/id=5881\",\"phone\":\"(714) 969-1368\",\"lat\":\"33.67893981934\",\"lon\":\"-117.98922729492\",\"special_offers_link\":\"https://www.walgreens.com/offers/offers.jsp\",\"special_offers_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-offer-btn_walgreens@2x.png\",\"status\":\"active\"},{\"id\":\"15703\",\"advertiser_id\":\"2\",\"title\":\"Walgreens\",\"display_text\":\"19001 Brookhurst St\\nHuntington Beach, CA 92646\",\"marker_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-marker_walgreens@2x.png\",\"popup_logo\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-popup-logo_walgreens@2x.png\",\"url\":\"Walgreens.com\",\"url_link\":\"http://www.walgreens.com/locator/walgreens-19001+brookhurst+st-huntington+beach-ca-92646/id=9089\",\"phone\":\"1-714-593-1352\",\"lat\":\"33.68638610840\",\"lon\":\"-117.95468139648\",\"special_offers_link\":\"https://www.walgreens.com/offers/offers.jsp\",\"special_offers_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-offer-btn_walgreens@2x.png\",\"status\":\"active\"},{\"id\":\"10001\",\"advertiser_id\":\"2\",\"title\":\"Walgreens\",\"display_text\":\"17522 Beach Blvd\\nHuntington Beach, CA 92647\",\"marker_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-marker_walgreens@2x.png\",\"popup_logo\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-popup-logo_walgreens@2x.png\",\"url\":\"Walgreens.com\",\"url_link\":\"http://www.walgreens.com/locator/walgreens-17522+beach+blvd-huntington+beach-ca-92647/id=5771\",\"phone\":\"1-714-596-5272\",\"lat\":\"33.70771026611\",\"lon\":\"-117.98833465576\",\"special_offers_link\":\"https://www.walgreens.com/offers/offers.jsp\",\"special_offers_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-offer-btn_walgreens@2x.png\",\"status\":\"active\"},{\"id\":\"11596\",\"advertiser_id\":\"2\",\"title\":\"Walgreens\",\"display_text\":\"6012 Warner Ave\\nHuntington Beach, CA 92647\",\"marker_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-marker_walgreens@2x.png\",\"popup_logo\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-popup-logo_walgreens@2x.png\",\"url\":\"Walgreens.com\",\"url_link\":\"http://www.walgreens.com/locator/walgreens-6012+warner+ave-huntington+beach-ca-92647/id=4354\",\"phone\":\"1-714-375-5443\",\"lat\":\"33.71509552002\",\"lon\":\"-118.02350616455\",\"special_offers_link\":\"https://www.walgreens.com/offers/offers.jsp\",\"special_offers_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-offer-btn_walgreens@2x.png\",\"status\":\"active\"},{\"id\":\"11181\",\"advertiser_id\":\"2\",\"title\":\"Walgreens\",\"display_text\":\"4935 Warner Ave\\nHuntington Beach, CA 92649\",\"marker_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-marker_walgreens@2x.png\",\"popup_logo\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-popup-logo_walgreens@2x.png\",\"url\":\"Walgreens.com\",\"url_link\":\"http://www.walgreens.com/locator/walgreens-4935+warner+ave-huntington+beach-ca-92649/id=12140\",\"phone\":\"1-714-377-3756\",\"lat\":\"33.71563720703\",\"lon\":\"-118.04181671143\",\"special_offers_link\":\"https://www.walgreens.com/offers/offers.jsp\",\"special_offers_img\":\"https://mobilesvc.sickweather.com/images/markers/new_sponsor-offer-btn_walgreens@2x.png\",\"status\":\"active\"}]"] dataUsingEncoding:NSUTF8StringEncoding];
        // Assign accordingly
        if (data)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetCovidIllnessesUsingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *baseEndpointURL = [NSString stringWithFormat:@"https://covid-api.sickweather.com/api/get-illnesses.php?api_key=%@", [SWHelper helperURLEncode:[SWHelper helperSickweatherAPIKey]]];
    NSString *urlString = [NSString stringWithFormat:@"%@", baseEndpointURL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSArray *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSArray *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        NSLog(@"randolfTesting1-covidIllnesses: %@", jsonResponseBody);
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetIllnessesUsingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *baseEndpointURL = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/getIllnesses.php?api_key=%@", [SWHelper helperURLEncode:[SWHelper helperSickweatherAPIKey]]];
    NSString *urlString = [NSString stringWithFormat:@"%@", baseEndpointURL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSArray *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSArray *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetFamilyIdUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler{
        // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
        // Add headers
    config.HTTPAdditionalHeaders = @{};
    
        // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
        // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/family/getFamilyId.php?api_key=%@&user_id=%@&phash=%@&ehash=%@", [SWHelper helperSickweatherAPIKey], [SWUserCDM currentUser].unique, [SWUserCDM currentUser].passwordHash, [SWUserCDM currentUser].emailHash];
    NSURL *url = [NSURL URLWithString:urlString];
    
        // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
        // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
            // Assign accordingly
        if (data != nil)
            {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
            //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
            // Call completion handler
        NSLog(@"jsonResponseBody: %@", jsonResponseBody);
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
        // Manage memory
    [session finishTasksAndInvalidate];
	
	
	
}


- (void)appBackendGetSymptomsUsingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler{
        // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
        // Add headers
    config.HTTPAdditionalHeaders = @{};
    
        // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
        // Define url
    NSString *baseEndpointURL = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/trackers/getSymptoms.php?api_key=%@", [SWHelper helperURLEncode:[SWHelper helperSickweatherAPIKey]]];
    NSString *urlString = [NSString stringWithFormat:@"%@", baseEndpointURL];
    NSURL *url = [NSURL URLWithString:urlString];
    
        // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
        // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // Declare baseline
        NSArray *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
            // Assign accordingly
        if (data != nil)
			{
			jsonResponseBody = (NSArray *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        NSLog(@"randolfTesting2: %@", jsonResponseBody);
            // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
        // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetTrackerTypersUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler{
	// Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
	NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
	
		// Add headers
	config.HTTPAdditionalHeaders = @{};
	
		// Create session using config
	NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
	
		// Define url
	NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/trackers/getTrackerTypes.php?api_key=%@", [SWHelper helperSickweatherAPIKey]];
	NSURL *url = [NSURL URLWithString:urlString];
	
		// Create URL request
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setHTTPMethod:@"GET"];
	
		// Create and execute data task
	NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
			// Declare baseline
		NSDictionary *jsonResponseBody;
		NSString *responseBodyAsString;
		NSHTTPURLResponse *httpResponse;
		NSError *requestError;
		NSError *jsonParseError;
		NSString *requestURLAbsoluteString;
			// Assign accordingly
		if (data != nil){
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
		httpResponse = (NSHTTPURLResponse *)response;
		requestError = error;
		requestURLAbsoluteString = [request URL].absoluteString;
		
			//NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
			//NSLog(@"responseBodyAsString = %@", responseBodyAsString);
		
			// Call completion handler
		completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
	}];
	[dataTask resume];
	
		// Manage memory
	[session finishTasksAndInvalidate];
}

- (void)appBackendGetSelfReportsUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/getUserReports.php?per_page=1000&api_key=%@&user_id=%@&phash=%@&ehash=%@", [SWHelper helperSickweatherAPIKey], [SWUserCDM currentUser].unique, [SWUserCDM currentUser].passwordHash, [SWUserCDM currentUser].emailHash];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetUserProfileUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/getProfile.php?api_key=%@&user_id=%@&phash=%@&ehash=%@", [SWHelper helperSickweatherAPIKey], [SWUserCDM currentUser].unique, [SWUserCDM currentUser].passwordHash, [SWUserCDM currentUser].emailHash];
    NSURL *url = [NSURL URLWithString:urlString];
	NSLog(@"%@",urlString);
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetUserLocationViaIPAddressUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"http://ipof.in/json"];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
            /*
             responseBodyAsString = {
             "blacklisted": true,
                "geoip": [
                    0,
                    33.6603,
                    -117.9992,
                    "California",
                    "Huntington Beach",
                    "United States"
                ],
             "ip": "104.34.205.30", 
             "time": 1449527858
             }
            */
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendDeleteFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler{
        // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
        // Add headers
    config.HTTPAdditionalHeaders = @{};
    
        // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
        // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
        // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
        {
            //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
            //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
        }
    if (queryStringArgValues.length > 0)
        {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
        }
        //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
        // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/family/deleteFamilyMember.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
        // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
        // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
            // Assign accordingly
		if(data != nil){
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
            // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
        // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendAddFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/family/saveFamilyMember.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/family/getFamilyMembers.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
     NSLog(@"Url String = %@", urlString);
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
		if(data != nil){
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendRequestToAddFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/family/requestLink.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
	NSLog(@"%@",urlString);
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
		if(data != nil){
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendUploadFamilyMemberProfilePicUsingArgs:(NSDictionary*)serverArgs Image:(UIImage *)image usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/family/uploadProfileImage.php"];
    //urlString = @"http://megan.sickweather.com/ws/v1.1/uploadProfileImage.php";
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create url request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self formBoundary]] forHTTPHeaderField:@"Content-Type"];
    
    // Create POST body data object (start fresh each call)
    NSMutableData *postRequestBody = [NSMutableData data];
    
    // Set request args
    /*
     api_key - Your API key
     ehash - user’s email MD5 hash
     phash - user’s password MD5 hash
     user_id - user’s unique ID
     */
    [self appendStringArgWithName:@"api_key" value:[SWHelper helperSickweatherAPIKey] postRequestBody:postRequestBody];
    [self appendStringArgWithName:@"ehash" value:[SWUserCDM currentlyLoggedInUser].emailHash postRequestBody:postRequestBody];
    [self appendStringArgWithName:@"phash" value:[SWUserCDM currentlyLoggedInUser].passwordHash postRequestBody:postRequestBody];
    [self appendStringArgWithName:@"user_id" value:[SWUserCDM currentlyLoggedInUser].unique postRequestBody:postRequestBody];
    
    // Adding member_id and family_id(if available)
    for (NSString *key in serverArgs)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [serverArgs objectForKey:key];
        //NSLog(@"value = %@", value);
        [self appendStringArgWithName:[SWHelper helperURLEncode:key] value:[SWHelper helperURLEncode:value] postRequestBody:postRequestBody];
    }
    
    // Add image
    NSString *imageName = [NSString stringWithFormat:@"file"];
    [self postBodyAddImageArgWithName:imageName image:image postRequestBody:postRequestBody];
    
    // End body
    // ------WebKitFormBoundaryIKFDqrAnm5Uw6LFS--
    [postRequestBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", [self formBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create/resume
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request fromData:postRequestBody completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        //NSLog(@"data = %@", data);
        //NSLog(@"response = %@", response);
        //NSLog(@"error = %@", error);
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [uploadTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetEventsUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/trackers/getTrackerFeed.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
	NSLog(@"%@",urlString);
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
		if (data != nil) {
			jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
			responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetAlertsUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        
        // Add headers
        config.HTTPAdditionalHeaders = @{};
        
        // Create session using config
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        // INIT ARGS DICT BASED ON USER DICT
        NSMutableDictionary *args = [serverArgs mutableCopy];
        
        // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
        [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
        
        // os_push_id
        if ([userId length] > 0)
        {
            [args setObject:userId forKey:@"os_push_id"];
        }
        
        // SIGN IF LOGGED IN
        if ([SWHelper helperUserIsLoggedIn])
        {
            if ([SWUserCDM currentlyLoggedInUser].emailHash != nil && ![[SWUserCDM currentlyLoggedInUser].emailHash isEqual:[NSNull null]]){
                [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
            }
            if ([SWUserCDM currentlyLoggedInUser].passwordHash != nil && ![[SWUserCDM currentlyLoggedInUser].passwordHash isEqual:[NSNull null]]){
                [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
            }
            if ([SWUserCDM currentlyLoggedInUser].unique != nil && ![[SWUserCDM currentlyLoggedInUser].unique isEqual:[NSNull null]]){
                 [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
            }
        }
        NSString *queryStringArgValues = @"";
        for (NSString *key in args)
        {
            //NSLog(@"key = %@", key);
            NSString *value = [args objectForKey:key];
            //NSLog(@"value = %@", value);
            queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
        }
        if (queryStringArgValues.length > 0)
        {
            queryStringArgValues = [queryStringArgValues substringFromIndex:1];
        }
//        NSLog(@"queryStringArgValues = %@", queryStringArgValues);
        
        // Define url
        NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/alerts/getAlertsList.php?%@", queryStringArgValues];
        NSURL *url = [NSURL URLWithString:urlString];
        NSLog(@"urlString = %@", urlString);
        
        // Create URL request
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        
        // Create and execute data task
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // Declare baseline
            NSDictionary *jsonResponseBody;
            NSString *responseBodyAsString;
            NSHTTPURLResponse *httpResponse;
            NSError *requestError;
            NSError *jsonParseError;
            NSString *requestURLAbsoluteString;
            // Assign accordingly
            if (data != nil){
                jsonResponseBody = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
                responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            }
            httpResponse = (NSHTTPURLResponse *)response;
            requestError = error;
            requestURLAbsoluteString = [request URL].absoluteString;
            // Call completion handler
            completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
        }];
        [dataTask resume];
        
        // Manage memory
        [session finishTasksAndInvalidate];
    }];
}

- (void)appBackendUpdateUserProfileUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    /*
    api_key - Your API key
    ehash - user’s email MD5 hash
    phash - user’s password MD5 hash
    user_id - user’s unique ID
    name_first - user’s first name
    name_last - user’s last name
    
    Optional Inputs
    gender - user’s gender (1=female, 2=male)
    race - user’s race/ethnicity (
     0 - Not Specified
     1 - American Indian / Alaskan Native
     2 - Asian
     3 - Black or African American
     4 - Hispanic or Latino
     5 - Native Hawaiian or Other Pacific Islander
     6 - White
    )
    month - two digit birth month
    day - two digit birth day
    year - four digit birth year
    city - user’s city
    state - user’s two letter state abbreviation
    country - user’s three letter country abbreviation
    med_pref - user’s preferred medicine (Western/Conventional=1, Holistic/Alternative=2, Both=3, No Preference=0)
    passwd = user’s password
    passwd_c = password again for confirmation
    */
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
    [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/updateProfile.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendUpdateUserProfilePicImage:(UIImage *)image usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/uploadProfileImage.php"];
    //urlString = @"http://megan.sickweather.com/ws/v1.1/uploadProfileImage.php";
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create url request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self formBoundary]] forHTTPHeaderField:@"Content-Type"];
    
    // Create POST body data object (start fresh each call)
    NSMutableData *postRequestBody = [NSMutableData data];
    
    // Set request args
    /*
    api_key - Your API key
    ehash - user’s email MD5 hash
    phash - user’s password MD5 hash
    user_id - user’s unique ID
    */
    [self appendStringArgWithName:@"api_key" value:[SWHelper helperSickweatherAPIKey] postRequestBody:postRequestBody];
    [self appendStringArgWithName:@"ehash" value:[SWUserCDM currentlyLoggedInUser].emailHash postRequestBody:postRequestBody];
    [self appendStringArgWithName:@"phash" value:[SWUserCDM currentlyLoggedInUser].passwordHash postRequestBody:postRequestBody];
    [self appendStringArgWithName:@"user_id" value:[SWUserCDM currentlyLoggedInUser].unique postRequestBody:postRequestBody];
    
    // Add image
    NSString *imageName = [NSString stringWithFormat:@"file"];
    [self postBodyAddImageArgWithName:imageName image:image postRequestBody:postRequestBody];
    
    // End body
    // ------WebKitFormBoundaryIKFDqrAnm5Uw6LFS--
    [postRequestBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", [self formBoundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create/resume
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request fromData:postRequestBody completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        //NSLog(@"data = %@", data);
        //NSLog(@"response = %@", response);
        //NSLog(@"error = %@", error);
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [uploadTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetGroupsUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
        [args setObject:@"1" forKey:@"my_groups"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/getGroups.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        
        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendGetGroupProfileUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/getGroupProfile.php?%@", queryStringArgValues];
    //urlString = [NSString stringWithFormat:@"http://megan.sickweather.com/ws/v1.1/groups/getGroupProfile.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendSetPushIdUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/setPushId.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendClaimGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/claimGroup.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendReportGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/reportGroup.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendReportUserOfGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/reportUser.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendRemoveGroupAdminUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/removeAdmin.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendAddTrackerUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler{
        // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
        // Add headers
    config.HTTPAdditionalHeaders = @{};
    
        // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
        // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
        // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
        {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
        }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
        {
            //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
            //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
        }
    if (queryStringArgValues.length > 0)
        {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
        }
        NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
        // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/trackers/addTracker.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
        // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
        // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
            // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
            // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
        // Manage memory
    [session finishTasksAndInvalidate];
    
}



- (void)appBackendTurnGroupNotificationsOnOffUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/updateNotifications.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendSubmitMessageToGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/submitMessage.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendCreateGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/addGroup.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendDeleteGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/deleteGroup.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendAddSympathyToGroupMessageUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/addSympathy.php?%@", queryStringArgValues];
    //urlString = [NSString stringWithFormat:@"http://megan.sickweather.com/ws/v1.1/groups/addSympathy.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendFollowGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/addFollow.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendUnfollowGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/removeFollow.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendDeleteGroupMessageUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/deleteMessage.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendDeleteReportUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
        [args setObject:@"1" forKey:@"groups"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/deleteReport.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendBlockGroupFollowerUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/groups/blockFollower.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendAddGroupAdminUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/addAdmin.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)appBackendSubmitReportsToGroupWithMessageUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // Set NSURLSessionConfig to be an ephemeral session (do not store any data to disk, all in RAM)
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Add headers
    config.HTTPAdditionalHeaders = @{};
    
    // Create session using config
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // INIT ARGS DICT BASED ON USER DICT
    NSMutableDictionary *args = [serverArgs mutableCopy];
    
    // ADD WHAT WE ARE RESPONSIBLE FOR (SIGNING REQUEST)
    [args setObject:[SWHelper helperSickweatherAPIKey] forKey:@"api_key"];
    SWUserCDM *u = [SWUserCDM currentlyLoggedInUser];
    if (u && u.emailHash && u.passwordHash && u.unique)
    {
        [args setObject:[SWUserCDM currentlyLoggedInUser].emailHash forKey:@"ehash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].passwordHash forKey:@"phash"];
        [args setObject:[SWUserCDM currentlyLoggedInUser].unique forKey:@"user_id"];
    }
    NSString *queryStringArgValues = @"";
    for (NSString *key in args)
    {
        //NSLog(@"key = %@", key);
        NSString *value = [args objectForKey:key];
        //NSLog(@"value = %@", value);
        queryStringArgValues = [queryStringArgValues stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", [SWHelper helperURLEncode:key], [SWHelper helperURLEncode:value]]];
    }
    if (queryStringArgValues.length > 0)
    {
        queryStringArgValues = [queryStringArgValues substringFromIndex:1];
    }
    //NSLog(@"queryStringArgValues = %@", queryStringArgValues);
    
    // Define url
    NSString *urlString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/groups/submitGroupReport.php?%@", queryStringArgValues];
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Create URL request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    // Create and execute data task
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil)
        {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
    [dataTask resume];
    
    // Manage memory
    [session finishTasksAndInvalidate];
}

- (void)fetchIllnessDefinitionForIllnessTypeId:(NSString *)illnessTypeId usingCompletionHandler:(SWAPIFetcherJSONCompletionHandler)completionHandler
{
    NSString *formattedURLString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/mobile/getKeywordDescription.php?id=%@", illnessTypeId];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:formattedURLString]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    /*RANDOLF TESTING
    NSURLSession * session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
    }];*/
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if ([data length] > 0 && error == nil)
        {
            NSDictionary *jsonDict = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            completionHandler(jsonDict, httpResponse, error);
        }
        else
        {
            completionHandler(@{}, httpResponse, error);
        }
    }];
}

- (void)signIntoSickweatherUsingFacebookAccessToken:(NSString *)facebookAccessToken usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // https://mobilesvc.sickweather.com/ws/v1.1/mobile/facebookLogin.php?facebook_token=CAAUHoOdm6JABAEOK89uhQ4tEipqiZBjn26yrGsK74XOQc1YVv8024ZA5EhB5u5M8MQbJvx7C92mNdMAnhvQ0yBVMtX5SaknJvwZAooDnznu62abMG6nEk6nweu2OaqxTmKVZAfpvl9BTLSF390SthFAMVgxSQMC4p6Rl0PWZCOOJ03KBOOu5tkBlaPZB6AbNr0hCYmHZBC6Ps0Kh2NQndsLxkQGsTE3rXTSGetusfc9gQZDZD&someword=orangebanana
    NSString *formattedURLString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v1.1/mobile/facebookLogin.php?facebook_token=%@&someword=orangebanana", [SWHelper helperURLEncode:facebookAccessToken]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:formattedURLString]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    /*RANDOLF TESTING
    NSURLSession * session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
    }];*/
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil) {
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
}

- (void)signIntoSickweatherUsingEmail:(NSString *)email password:(NSString *)password usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // https://mobilesvc.sickweather.com/ws/v1.1/mobile/checkLogin.php?email=erck0006%40gmail.com&password=john%3Ferck%2F
    NSString *formattedURLString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/checkLogin.php?email=%@&password=%@&api_key=%@", [SWHelper helperURLEncode:email], [SWHelper helperURLEncode:password], [SWHelper helperSickweatherAPIKey]];
	NSLog(@"%@",formattedURLString);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:formattedURLString]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    /*
    NSURLSession * session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
    }];*/
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
}

- (void)verifySickweatherEmailHash:(NSString *)emailHash andPasswordHash:(NSString *)passwordHash usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    // https://mobilesvc.sickweather.com/ws/v1.1/mobile/verifyLogin.php?email=1f974ce77c2349b0eba3ab19faa060eb&password=9294a21bb94f0dd03168dcdb7b492eb7
    NSString *formattedURLString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/verifyLogin.php?email=%@&password=%@", [SWHelper helperURLEncode:emailHash], [SWHelper helperURLEncode:passwordHash]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:formattedURLString]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    /*RANDOLF TESTING
    NSURLSession * session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
    }];
     */
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
}

- (void)createSickweatherAccountUsingFirstName:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email password:(NSString *)password usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler
{
    /*
     name_first
     name_last
     email
     email_c
     passwd
     passwd_c
     */
    NSString *formattedURLString = [NSString stringWithFormat:@"https://mobilesvc.sickweather.com/ws/v2.0/mobile/createAccount.php?name_first=%@&name_last=%@&email=%@&email_c=%@&passwd=%@&passwd_c=%@&mySubmit=true", [SWHelper helperURLEncode:firstName], [SWHelper helperURLEncode:lastName], [SWHelper helperURLEncode:email], [SWHelper helperURLEncode:email], [SWHelper helperURLEncode:password], [SWHelper helperURLEncode:password]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:formattedURLString]];
    request.HTTPMethod = @"POST";
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    /*RANDOLF TESTING
    NSURLSession * session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
    }];
     */
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        // Declare baseline
        NSDictionary *jsonResponseBody;
        NSString *responseBodyAsString;
        NSHTTPURLResponse *httpResponse;
        NSError *requestError;
        NSError *jsonParseError;
        NSString *requestURLAbsoluteString;
        // Assign accordingly
        if (data != nil){
            jsonResponseBody = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
            responseBodyAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        httpResponse = (NSHTTPURLResponse *)response;
        requestError = error;
        requestURLAbsoluteString = [request URL].absoluteString;
        // Call completion handler
        completionHandler(jsonResponseBody, responseBodyAsString, httpResponse, requestError, jsonParseError, requestURLAbsoluteString);
    }];
}

- (UIImage *)fetchImage:(NSString *)link usingCompletionHandler:(SWAPIFetcherImageCompletionHandler)completionHandler
{
    UIImage *image = nil;
    NSURL *url = [NSURL URLWithString:link];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:1*60];
    NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:request];
    if (cachedResponse.data)
    {
        //NSLog(@"Do something with final product RIGHT HERE! You already have it... %@", cachedResponse);
        image = [UIImage imageWithData:cachedResponse.data scale:[UIScreen mainScreen].scale];
    }
    else
    {
        NSString *formattedURLString = link;
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:formattedURLString]];
        request.HTTPMethod = @"GET";
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        /*RANDOLF TESTING
        NSURLSession * session = [NSURLSession sharedSession];
        [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
            
        }];
         */
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if ([data length] > 0 && error == nil)
            {
                if (httpResponse.statusCode == 200)
                {
                    NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:response data:data];
                    [[NSURLCache sharedURLCache] storeCachedResponse:cachedResponse forRequest:request];
                }
                else
                {
                    SWDLog(@"httpResponse.statusCode != 200, SKIP CACHE for URL: %@", link);
                }
                completionHandler(data, response, error);
            }
            else
            {
                completionHandler(nil, response, error);
            }
        }];
    }
    return image;
}

@end
