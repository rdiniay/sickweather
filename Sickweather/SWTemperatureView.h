//
//  SWTemperatureAlertView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 08/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWTemperatureViewDelegate
-(void)performViewCancel;
-(void)saveTemperatureWith:(float)temp severity:(NSString*)status;
@end

@interface SWTemperatureView : UIView
- (void)initializeWithTemperature:(float)temperature withTemperatureScale:(NSString*)tempScale;
@property (weak, nonatomic) id<SWTemperatureViewDelegate> delegate;
@end
