//
//  SWWatchKitAppExtensionDelegate.h
//  Sickweather
//
//  Created by John Erck on 9/29/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface SWWatchKitAppExtensionDelegate : NSObject
@property (strong, nonatomic) WCSession *session;
@end
