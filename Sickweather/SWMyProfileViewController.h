//
//  SWMyProfileViewController.h
//  Sickweather
//
//  Created by John Erck on 9/28/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyProfileViewController;

@protocol SWMyProfileViewControllerDelegate <NSObject>
- (void)myProfileViewControllerWantsToPopOffTheStack:(SWMyProfileViewController *)sender;
@end

@interface SWMyProfileViewController : UIViewController
@property (nonatomic, weak) id<SWMyProfileViewControllerDelegate> delegate;
@end
