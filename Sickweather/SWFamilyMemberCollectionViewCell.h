//
//  SWFamilyMemberCollectionViewCell.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 01/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@interface SWFamilyMemberCollectionViewCell : UICollectionViewCell
-(void)initWithData:(SWFamilyMemberModal*)memberData isCurrentUser:(BOOL)isCurrentUser;
@end
