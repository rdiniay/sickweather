//
//  SWAddTemperatureViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWAddTemperatureViewController.h"
#import "SWConnectBluetoothDeviceTableViewController.h"
#import "SWAddSymptomsViewController.h"
#import "SWConnectDeviceViewController.h"
@interface SWAddTemperatureViewController () <SWConnectBluetoothDeviceViewControllerDelegate>
@property int originalknownDeviceListTopConstraint;
@property (nonatomic) BOOL isCelsius;
@property int originalknownDeviceListHeight;
@property int originalswaiveDeviceHeight;
@property int originalswaiveDeviceBottomConstraint;
@property int originalkinsaDeviceHeight;
@property int originalkinsaDeviceBottomConstraint;
@property int originalphilipsDeviceHeight;
@property int originalphilipsDeviceBottomConstraint;
@property int originalconnectBluetoothDeviceButtonHeight;
@property int originalconnectBluetoothDeviceTopConstraint;
@end
float savedTemperature;
@implementation SWAddTemperatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // saving original constraints
    self.originalknownDeviceListTopConstraint = self.knownDeviceListTopConstraint.constant;
    self.originalknownDeviceListHeight = self.knownDeviceListHeight.constant;
    self.originalswaiveDeviceHeight = self.swaiveDeviceHeight.constant;
    self.originalswaiveDeviceBottomConstraint = self.swaiveDeviceBottomConstraint.constant;
    self.originalkinsaDeviceHeight = self.kinsaDeviceHeight.constant;
    self.originalkinsaDeviceBottomConstraint = self.kinsaDeviceBottomConstraint.constant;
    self.originalphilipsDeviceHeight = self.philipsDeviceHeight.constant;
    self.originalphilipsDeviceBottomConstraint = self.philipsDeviceBottomConstraint.constant;
    self.originalconnectBluetoothDeviceButtonHeight = self.connectBluetoothDeviceButtonHeight.constant;
    self.originalconnectBluetoothDeviceTopConstraint = self.connectBluetoothDeviceTopConstraint.constant;
    
    // Config Navigation Bar Items
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemTapped:)];
    
    // connectBluetoothDeviceButton
	[self.connectBluetoothDeviceButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Connect a BluetoothDevice" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
	self.connectBluetoothDeviceButton.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
	self.connectBluetoothDeviceButton.layer.cornerRadius = 6;
	self.connectBluetoothDeviceButton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
	self.connectBluetoothDeviceButton.layer.borderWidth = 2.0;
	
	//Temperature Scale Prefrences
	self.isCelsius = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants isCelsiusTemperatureTypeDefaultKey]];
	[self.switchTemperatureScale setRightSelected:self.isCelsius];
	
    // Adding round border to device list items
    self.swaiveDevice.layer.borderWidth = 2;
    self.swaiveDevice.layer.cornerRadius = 6;
    self.swaiveDevice.layer.borderColor = [SWColor colorSickweatherLightGray204x204x204].CGColor;
    self.philipsDevice.layer.borderWidth = 2;
    self.philipsDevice.layer.cornerRadius = 6;
    self.philipsDevice.layer.borderColor = [SWColor colorSickweatherLightGray204x204x204].CGColor;
    self.kinsaDevice.layer.borderWidth = 2;
    self.kinsaDevice.layer.cornerRadius = 6;
    self.kinsaDevice.layer.borderColor = [SWColor colorSickweatherLightGray204x204x204].CGColor;
    
    BOOL swaiveExists = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothSwaiveDeviceKey]];
    BOOL kinsaExists = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothKinsaDeviceKey]];
    BOOL philipsExists = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothPhilipsDeviceKey]];
    
    // hiding data
    if (!swaiveExists && !kinsaExists && !philipsExists){
        // no devices
        self.knownDeviceListView.hidden = YES;
        self.knownDeviceListTopConstraint.constant = 0;
        self.knownDeviceListHeight.constant = 0;
    } else if (swaiveExists && kinsaExists && philipsExists){
        // All devices and no button
        self.connectBluetoothDeviceButton.hidden = YES;
        self.connectBluetoothDeviceButtonHeight.constant = 0;
        self.connectBluetoothDeviceTopConstraint.constant = 0;
    } else {
        if (!swaiveExists){
            self.knownDeviceListHeight.constant -= self.swaiveDeviceHeight.constant + self.swaiveDeviceBottomConstraint.constant;
            self.swaiveDevice.hidden = YES;
            self.swaiveDeviceHeight.constant = 0;
            self.swaiveDeviceBottomConstraint.constant = 0;
        }
        if (!kinsaExists){
            self.knownDeviceListHeight.constant -= self.kinsaDeviceHeight.constant + self.kinsaDeviceBottomConstraint.constant;
            self.kinsaDevice.hidden = YES;
            self.kinsaDeviceHeight.constant = 0;
            self.kinsaDeviceBottomConstraint.constant = 0;
        }
        if (!philipsExists){
            self.knownDeviceListHeight.constant -= self.philipsDeviceHeight.constant + self.philipsDeviceBottomConstraint.constant;
            self.philipsDevice.hidden = YES;
            self.philipsDeviceHeight.constant = 0;
            self.philipsDeviceBottomConstraint.constant = 0;
        }
    }
    [self.view layoutIfNeeded];

	// Tap Gesture
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
	tapGesture.cancelsTouchesInView = NO;
	[self.view addGestureRecognizer:tapGesture];
    UITapGestureRecognizer *swaiveTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swaiveDevicePressed:)];
    [self.swaiveDevice addGestureRecognizer:swaiveTapGes];
    UITapGestureRecognizer *kinsaTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(kinsaDevicePressed:)];
    [self.kinsaDevice addGestureRecognizer:kinsaTapGes];
    UITapGestureRecognizer *philipstapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(philipsDevicePressed:)];
    [self.philipsDevice addGestureRecognizer:philipstapGes];
	
	//Add Selector
	[self.tempTxtField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	
    // Adding notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceListingReload:)
                                                 name:@"deviceListingReload"
                                               object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Add Tracker

-(void)saveTemperatureWith:(float)temp severity:(NSString*)status{
	
	NSString *tempValue = [NSString stringWithFormat:@"%@",[self getStringFromNumber: self.isCelsius ? [SWHelper convertCelsiusToFarenheit:temp] : temp]];
	[self addTrackerWith:@{@"type":[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeManualTemperature], @"member_id":self.familyMember != nil ? self.familyMember.member_id : @"", @"value": tempValue}];
	UIStoryboard *familyStoryboard = [UIStoryboard storyboardWithName:@"Family" bundle:nil];
	SWAddSymptomsViewController *addSymptomsViewController = [familyStoryboard instantiateViewControllerWithIdentifier:@"SWAddSymptomsViewController"];
	addSymptomsViewController.title = @"Symptoms";
    addSymptomsViewController.isFromBlueToothTempScreen = true;
    addSymptomsViewController.isFamilyMember = self.isFamilyMember;
    addSymptomsViewController.familyMember = self.familyMember;
    addSymptomsViewController.temp = [NSString stringWithFormat:@"%@%@",[self getStringFromNumber:temp],self.isCelsius ? @"°C":@"°F"];
	addSymptomsViewController.severity = status;
	[self.navigationController pushViewController:addSymptomsViewController animated:YES];
}

-(void)addTrackerWith:(NSDictionary*)data{
	[[SWHelper helperAppBackend]appBackendAddTrackerUsingArgs:data completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		dispatch_async(dispatch_get_main_queue(), ^{
		});
	}];
}

#pragma mark - Target/Action

-(NSString*)getStringFromNumber:(float)value{
	NSNumberFormatter *twoDecimalPlacesFormatter = [[NSNumberFormatter alloc]init];
	[twoDecimalPlacesFormatter setMaximumFractionDigits:1];
	[twoDecimalPlacesFormatter setMinimumFractionDigits:0];
	return [twoDecimalPlacesFormatter stringFromNumber:[NSNumber numberWithFloat:value]];
}


- (void)hideKeyboard:(id)sender{
	[self.view endEditing:true];
	
}
- (void)leftBarButtonItemTapped:(id)sender
{
	[self.navigationController popViewControllerAnimated:true];
}

- (void)rightBarButtonItemTapped:(id)sender
{
	if ((!self.isCelsius && savedTemperature > 80 && savedTemperature < 110) || (self.isCelsius && savedTemperature > 26.7 && savedTemperature < 43.3)) {
		[self saveTemperatureWith:savedTemperature severity:self.lblSeverity.text];
	}else {
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"Invalid temperature, Kindly enter temperature in %@",self.isCelsius ? @"Celsius":@"Farenheit"] preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
		[alert addAction:ok];
		[self presentViewController:alert animated:YES completion:nil];
	}
}

- (IBAction)temperatureScaleValueChanged:(Switch *)sender {
	self.isCelsius = sender.rightSelected;
	if (![self.tempTxtField.text isEqualToString:@""]) {
		NSString *tempValue = self.tempTxtField.text;
		float convertedTemp = (self.isCelsius) ? convertedTemp = [SWHelper convertFarenhietToCelsius:[tempValue floatValue]] : [SWHelper convertCelsiusToFarenheit:[tempValue floatValue]];
		self.tempTxtField.text = [self getStringFromNumber:convertedTemp];
		savedTemperature = [self.tempTxtField.text floatValue];
	}else {
		savedTemperature = 0.0;
		[self updateGuageView];
	}
	[[NSUserDefaults standardUserDefaults] setBool:self.isCelsius forKey:[SWConstants isCelsiusTemperatureTypeDefaultKey]];
	//[self updateGuageView];
}

-(void)textFieldDidChange :(UITextField *)theTextField{
	savedTemperature = [theTextField.text floatValue];
	[self updateGuageView];
}

-(void)updateGuageView{
	self.lblSeverity.text = self.isCelsius ? [SWHelper updateFeverSeverityWithCelcius:savedTemperature] : [SWHelper updateFeverSeverityWithFarenheit:savedTemperature];
	self.guageView.temperature = savedTemperature;
	self.guageView.isCelsiusTemperatureScaleType = self.isCelsius;
	[self.guageView setNeedsDisplay];
}

-(void)swaiveDevicePressed:(UITapGestureRecognizer *)recognizer
{
    SWConnectDeviceViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWConnectDeviceViewController"];
    vc.deviceType = swaive;
	vc.isFamilyMember = self.isFamilyMember;
	vc.familyMember = self.familyMember;
    [self.navigationController pushViewController:vc animated:YES ];
}

-(void)kinsaDevicePressed:(UITapGestureRecognizer *)recognizer
{
    SWConnectDeviceViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWConnectDeviceViewController"];
    vc.deviceType = kinsa;
    vc.isFamilyMember = self.isFamilyMember;
    vc.familyMember = self.familyMember;
    [self.navigationController pushViewController:vc animated:YES ];
}

-(void)philipsDevicePressed:(UITapGestureRecognizer *)recognizer
{
    SWConnectDeviceViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWConnectDeviceViewController"];
    vc.deviceType = philips;
    vc.isFamilyMember = self.isFamilyMember;
    vc.familyMember = self.familyMember;
    [self.navigationController pushViewController:vc animated:YES ];
}

-(IBAction)blueToothButtonPressed:(id)sender
{
	SWConnectBluetoothDeviceTableViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWConnectBluetoothDeviceTableViewController"];
	vc.delegate = self;
    vc.isFamilyMember = self.isFamilyMember;
    vc.familyMember = self.familyMember;
	[self.navigationController pushViewController:vc animated:YES ];
	
}

-(void)deviceListingReload:(NSNotification *)notification
{
    // resetting to original
    self.knownDeviceListTopConstraint.constant = self.originalknownDeviceListTopConstraint;
    self.knownDeviceListHeight.constant = self.originalknownDeviceListHeight;
    self.swaiveDeviceHeight.constant = self.originalswaiveDeviceHeight;
    self.swaiveDeviceBottomConstraint.constant = self.originalswaiveDeviceBottomConstraint;
    self.kinsaDeviceHeight.constant = self.originalkinsaDeviceHeight;
    self.kinsaDeviceBottomConstraint.constant = self.originalkinsaDeviceBottomConstraint;
    self.philipsDeviceHeight.constant = self.originalphilipsDeviceHeight;
    self.philipsDeviceBottomConstraint.constant = self.originalphilipsDeviceBottomConstraint;
    self.connectBluetoothDeviceButtonHeight.constant = self.originalconnectBluetoothDeviceButtonHeight;
    self.connectBluetoothDeviceTopConstraint.constant = self.originalconnectBluetoothDeviceTopConstraint;
    self.knownDeviceListView.hidden = NO;
    self.connectBluetoothDeviceButton.hidden = NO;
    self.swaiveDevice.hidden = NO;
    self.kinsaDevice.hidden = NO;
    self.philipsDevice.hidden = NO;
    
    
    // re-evaluating constraints
    BOOL swaiveExists = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothSwaiveDeviceKey]];
    BOOL kinsaExists = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothKinsaDeviceKey]];
    BOOL philipsExists = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothPhilipsDeviceKey]];
    // hiding data
    if (!swaiveExists && !kinsaExists && !philipsExists){
        // no devices
        self.knownDeviceListView.hidden = YES;
        self.knownDeviceListTopConstraint.constant = 0;
        self.knownDeviceListHeight.constant = 0;
    } else if (swaiveExists && kinsaExists && philipsExists){
        // All devices and no button
        self.connectBluetoothDeviceButton.hidden = YES;
        self.connectBluetoothDeviceButtonHeight.constant = 0;
        self.connectBluetoothDeviceTopConstraint.constant = 0;
    } else {
        if (!swaiveExists){
            self.knownDeviceListHeight.constant -= self.swaiveDeviceHeight.constant + self.swaiveDeviceBottomConstraint.constant;
            self.swaiveDevice.hidden = YES;
            self.swaiveDeviceHeight.constant = 0;
            self.swaiveDeviceBottomConstraint.constant = 0;
        }
        if (!kinsaExists){
            self.knownDeviceListHeight.constant -= self.kinsaDeviceHeight.constant + self.kinsaDeviceBottomConstraint.constant;
            self.kinsaDevice.hidden = YES;
            self.kinsaDeviceHeight.constant = 0;
            self.kinsaDeviceBottomConstraint.constant = 0;
        }
        if (!philipsExists){
            self.knownDeviceListHeight.constant -= self.philipsDeviceHeight.constant + self.philipsDeviceBottomConstraint.constant;
            self.philipsDevice.hidden = YES;
            self.philipsDeviceHeight.constant = 0;
            self.philipsDeviceBottomConstraint.constant = 0;
        }
    }
    [self.view layoutIfNeeded];
}

#pragma mark - SWConnectBluetoothDeviceTableViewControllerDelegate

- (void)connectBluetoothDeviceViewControllerWantsToDismiss:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)connectBluetoothDeviceViewControllerWantsToCancel:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	int limit = 5;
	return !([textField.text length]>=limit && [string length] >= range.length);
}

@end
