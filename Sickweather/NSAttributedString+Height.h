//
//  NSAttributedString+Height.h
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (Height)

- (CGFloat)heightForWidth:(CGFloat)width;

@end
