//
//  BTUtility.h
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/13/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BTUtility : NSObject


+ (NSString *)charToASCII:(char)charValue;
+ (NSString *)hexToASCII:(NSData *)hexValue;
+ (NSString *)hexToString:(NSData *)hexValue;
+ (int)hexToDecimal:(NSData *)hexValue;
+ (NSMutableData *)intToData:(int)value;

+ (void)writeCharacteristic:(CBPeripheral *)peripheral service:(CBUUID *)currentService characteristic:(CBUUID *)currentCharacteristic data:(NSData *)data;


@end
