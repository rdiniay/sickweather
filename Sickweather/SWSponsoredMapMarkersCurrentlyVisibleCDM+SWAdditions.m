//
//  SWSponsoredMapMarkersCurrentlyVisibleCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 12/27/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMapMarkersCurrentlyVisibleCDM+SWAdditions.h"
#import "SWSponsoredMarkerMapKitAnnotation.h"

@implementation SWSponsoredMapMarkersCurrentlyVisibleCDM (SWAdditions)

+ (NSArray *)sponsoredMapMarkersCurrentlyVisibleGetAll
{
    // Create and config request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWSponsoredMapMarkersCurrentlyVisibleCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"sponsoredMapMarkerId" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"sponsoredMapMarkerId != 0"]; // All
    
    // Execute request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    return matches;
}

+ (void)sponsoredMapMarkersCurrentlyVisibleResetAs:(NSArray *)annotationsInVisibleMapRect
{
    if ([SWHelper helperManagedObjectContext])
    {
        // First, delete everything
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"SWSponsoredMapMarkersCurrentlyVisibleCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]]];
        [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        NSError *error = nil;
        NSArray *records = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
        for (NSManagedObject *record in records)
        {
            [[SWHelper helperManagedObjectContext] deleteObject:record];
        }
        
        // Then, insert incoming
        for (id<MKAnnotation> annotation in annotationsInVisibleMapRect)
        {
            if ([annotation isKindOfClass:[SWSponsoredMarkerMapKitAnnotation class]])
            {
                // Create new
                SWSponsoredMarkerMapKitAnnotation *sponsoredMapMarkerAnnotation = (SWSponsoredMarkerMapKitAnnotation *)annotation;
                SWSponsoredMapMarkersCurrentlyVisibleCDM *sponsoredMapMarkerThatIsVisible = [NSEntityDescription insertNewObjectForEntityForName:@"SWSponsoredMapMarkersCurrentlyVisibleCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
                sponsoredMapMarkerThatIsVisible.sponsoredMapMarkerId = sponsoredMapMarkerAnnotation.identifier;
            }
        }
        
        // Save
        NSError *saveError = nil;
        [[SWHelper helperManagedObjectContext] save:&saveError];
    }
}

@end
