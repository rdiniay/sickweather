//
//  SWMedicationModal.h
//  Sickweather
//
//  Created by Shan Shafiq on 2/14/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWMedicationModal : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *dose;
@property (strong, nonatomic) NSString *dose_unit;
@property (strong, nonatomic) NSString *notes;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
