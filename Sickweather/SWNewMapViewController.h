//
//  SWNewMapViewController.h
//  Sickweather
//
//  Created by John Erck on 7/25/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol SWNewMapViewControllerDelegate
// TBD
@end

@interface SWNewMapViewController : UIViewController

// DELEGATE
@property (nonatomic, weak) id<SWNewMapViewControllerDelegate> delegate;
@property (nonatomic) BOOL isFromIllnessReport;
@property (nonatomic) BOOL isCovidScore;
@property (nonatomic) NSString * illnessName;
// USE THIS METHOD TO UPDATE THE MAP/INIT HOW YOU WANT
- (void)updateForIllnessIDs:(NSArray *)illnessIDs andLocation:(CLLocation *)location;
- (void)updateForMyReports:(CLLocation *)location;
// USE THIS METHOD TO FORMAT getMarkersInRadius.php OR getUserReports.php OBJ TO CUSTOM ANNOTATION OBJ
+ (NSObject <MKAnnotation>*)classHelperGetAnnotationForReportDict:(NSDictionary *)report objectFromDocsUsingName: (NSString *) objectFromDocsUsingName;

@end
