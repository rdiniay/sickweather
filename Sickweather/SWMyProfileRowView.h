//
//  SWMyProfileRowView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyProfileRowView;

@protocol SWMyProfileRowViewDelegate
- (NSString *)myProfileRowViewTitleString:(SWMyProfileRowView *)sender;
- (UIImage *)myProfileRowViewIconImage:(SWMyProfileRowView *)sender;
- (void)myProfileRowViewWasTapped:(SWMyProfileRowView *)sender;
@end

@interface SWMyProfileRowView : UIView
@property (nonatomic, weak) id<SWMyProfileRowViewDelegate> delegate;
- (void)showBottomBorder;
- (void)setShowDisclosureIndicatorIcon;
@end
