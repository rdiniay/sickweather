//
//  SWHelper.swift
//  Sickweather
//
//  Created by John Erck on 3/6/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit

class SWHelper: NSObject
{
    
    class func helperSickScoreGetColorRiskGlanceBackgroundImageNameForScore(_ sickScore: Int) -> (color: UIColor, risk: String, glanceBackgroundImageName: String)
    {
        var currentColor = SWColor.blueColor()
        var currentRisk = "VERY LOW RISK"
        var glanceBackgroundImageName = "sickscore-glance-background-blue"
        switch sickScore {
        case 0...25:
            currentColor = SWColor.blueColor()
            currentRisk = "VERY LOW RISK"
            glanceBackgroundImageName = "sickscore-glance-background-blue"
        case 25...50:
            currentColor = SWColor.yellowColor()
            currentRisk = "LOW RISK"
            glanceBackgroundImageName = "sickscore-glance-background-yellow"
        case 50...75:
            currentColor = SWColor.orangeColor()
            currentRisk = "MEDIUM RISK"
            glanceBackgroundImageName = "sickscore-glance-background-orange"
        case 75...100:
            currentColor = SWColor.redColor()
            currentRisk = "HIGH RISK"
            glanceBackgroundImageName = "sickscore-glance-background-red"
        default:
            currentColor = SWColor.blueColor()
            currentRisk = "VERY LOW RISK"
            glanceBackgroundImageName = "sickscore-glance-background-blue"
        }
        return (currentColor, currentRisk, glanceBackgroundImageName)
    }
    
    class func helperHandWashTimerDuration() -> Int
    {
        return 20
    }
    
    class func helperHandWashTimerFramesPerSecond() -> Int
    {
        return 4
    }
    
    class func helperHandWashTimerTotalFrameCount() -> Int
    {
        return helperHandWashTimerDuration() * helperHandWashTimerFramesPerSecond() + 1 // "for frameindex in 0...totalFrameCount" <-- We get one more frame than our count, we like this because it is how our timer images are created and this way gets them to end perfect.
    }
    
}
