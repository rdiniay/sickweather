//
//  SWAnalytics.swift
//  SWAnalytics
//
//  Created by Shan Shafiq on 2/22/17.
//  Copyright © 2017 Technology Rivers. All rights reserved.
//

import Foundation
import CoreLocation
import AdSupport
@objc public class SWAnalytics: NSObject {
	//Constants
	private static let APP_VERSION_CODE = "appVersionCode"
	private static let USER_AGENT = "User-Agent"
	private static let OS_USER_ID = "osUserId"
	private static let USER_ID = "userId"
	private static let LATITUDE = "latitude"
	private static let LONGITUDE = "longitude"
	private static let SICKSCORE = "sickscore"
	private static let ALLERGY_SICKSCORE = "allergy_sickscore"
	private static let COLDFLU_SICKSCORE = "coldflu_sickscore"
    private static let COVIDSCORE = "cvscore"
    private static let ALLERGY_COVIDSCORE = "allergy_cvscore"
    private static let COLDFLU_COVIDSCORE = "coldflu_cvscore"
	private static let ADVERTISEMENT_ID = "advertisementId"
	private static let CREATED_AT = "created_time"
	private static let DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX"

	// top varaibles
	private static var apiKey: String!
	private static var userCurrentLatitude : Double = 0.0
	private static var userCurrentLongitude : Double = 0.0
	private static var reachability: Reachability!
	private static var serverUrl = "https://snowapi.sickweather.com/location"

	//MARK: Init Method
	@objc open class func initWith(apiKey: String, osUserId: String, userId: String?) {
		SWAnalytics.apiKey = apiKey
		self.saveUserId(userId: userId)
		self.saveOsUserId(osUserId: osUserId)
		SWAnalytics.setUp()
	}

	@objc open class func initWith(apiKey: String) {
		SWAnalytics.apiKey = apiKey
		SWAnalytics.setUp()
	}

	@objc open class func setUser(userId: String) {
		self.saveUserId(userId: userId)
	}

	@objc open class func setOSUser(osUserId: String) {
		self.saveOsUserId(osUserId: osUserId)
	}

	private static func setUp() {
		SWAnalytics.reachability = Reachability()
		NotificationCenter.default.addObserver(self, selector: #selector(SWAnalytics.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: SWAnalytics.reachability)
		do {
			try SWAnalytics.reachability.startNotifier()
		} catch {
			print("Unable to start notifier")
		}
	}

	//MARK: Location Update
	@objc open class func updateLocation(_ latitude: Double, longitude: Double, params: Dictionary<String, Any>?) {
		var paramsData: [String: Any] = [:]
		if params != nil {
			paramsData = params!
		}
		paramsData.updateValue(String(latitude), forKey: "lat")
		paramsData.updateValue(String(longitude), forKey: "lng")
		
		if let createdAt = getISO() {
			paramsData.updateValue(createdAt, forKey:CREATED_AT)
		}
		
		if params != nil {
			if let extraParams = params!["params"] as? [String : Any] {
				if extraParams["illness_id"] == nil && extraParams["tracker_id"] == nil {
					if getUserCurrentLat() == latitude && getUserCurrentLng() == longitude {
						setUserCurrentLatLng(lat: latitude, lng: longitude)
						return
					}
				}
			}else {
				if getUserCurrentLat() == latitude && getUserCurrentLng() == longitude {
					setUserCurrentLatLng(lat: latitude, lng: longitude)
					return
				}
			}
		}
		
		setUserCurrentLatLng(lat: latitude, lng: longitude)
		if let lat = getUserLatitude(), let lon = getUserLongitude() {
			let coordinate₀ = CLLocation(latitude: lat, longitude:lon)
			let coordinate₁ = CLLocation(latitude: latitude, longitude:longitude)
			let distanceInMeters = coordinate₀.distance(from: coordinate₁)
			if distanceInMeters > 0  && (distanceInMeters / 1609) >= 20 {
				self.getSickScore(latitude, longitude:longitude, params : paramsData) { (sickscore) in}
			}else{
				var params : [String:Any] = [:]
				if let param = paramsData["params"] as? [String:Any]{
					params = param
				}
				if let sickscore = getSickScore() {
					params.updateValue(String(sickscore), forKey:"sickscore")
				}
				if let allergySickscore = getAllergySickScore() {
					params.updateValue(String(allergySickscore), forKey:"allergy_sickscore")
				}
				if let fluSickscore = getColdFluSickScore() {
					params.updateValue(String(fluSickscore), forKey:"coldflu_sickscore")
				}
				setUserLatitudeLongitude(lat:latitude, lon:longitude)
				paramsData.updateValue(params, forKey:"params")
				self.post(params: paramsData)
			}
		}else{
				self.getSickScore(latitude, longitude:longitude, params: paramsData) { (sickscore) in
			}
		}
	}

	private static func postOrSaveData(params : Dictionary<String, Any>){
		if SWAnalytics.reachability.isReachable {
			SWAnalytics.post(params: params)
		} else {
			SWAnalytics.saveData(params: params)
		}
	}
	
	//MARK: Api Call
	private static func post(params: Dictionary<String, Any>) {
		let request = NSMutableURLRequest(url:URL(string: serverUrl)!)
		let session = URLSession.shared
		let authValue = "Bearer " + self.apiKey
		request.httpMethod = "POST"
		do {
			request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
		} catch {
			print("Error \(error)")
		}
		let info: NSDictionary = Bundle.main.infoDictionary! as NSDictionary
		let version: String = info.object(forKey: "CFBundleShortVersionString") as! String
		request.addValue(authValue, forHTTPHeaderField: "Authorization")
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		request.addValue(version, forHTTPHeaderField: APP_VERSION_CODE)
		request.addValue(self.makeUserAgent(), forHTTPHeaderField: USER_AGENT)
		if let osUserId = self.getOsUserId() {
			request.addValue(osUserId, forHTTPHeaderField: OS_USER_ID)
		}
		if let userId = self.getUserId() {
			request.addValue(userId, forHTTPHeaderField: USER_ID)
		}

		if let identifier = IDFA.shared.identifier {
			request.addValue(identifier, forHTTPHeaderField: ADVERTISEMENT_ID)
		}

		let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error -> Void in
			if error == nil {
				do {
					let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
					if json?["error"] as? String == "Unauthorized" {
						print("Invalid SWAnalytics Key")
					}
				} catch {
					self.saveData(params: params)
					print("Error \(error)")
				}
			} else {
				self.saveData(params: params)
			}
		})
		task.resume()
	}
    
    private static func getCovidScore(_ latitude: Double, longitude: Double,  params: Dictionary<String, Any>, completion : @escaping (_ response : Int?)-> ()) {
        var params = params
        let sickScoreUrl = "https://covid-api.sickweather.com/api/cvscore.php?api_key=oWCYcO9KTEdnMduhBE6NHcDdVxHAzGBN&lat=\(latitude)&lon=\(longitude)"
        let request = NSMutableURLRequest(url:URL(string: sickScoreUrl)!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error -> Void in
            if error == nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    var tempParams : [String:Any] = [:]
                    if let covidscore = json?.object(forKey: "cvscore") as? Int{
                        if let param = params["params"] as? [String:Any]{
                            tempParams = param
                        }
                        self.setCovidScore(covidscore)
                        tempParams.updateValue(String(covidscore), forKey:"cvscore")
                    }
                    if let coldflu_covidscore = json?.object(forKey: "coldflu") as? Int{
                        self.setColdFluCovidScore(coldflu_covidscore)
                        tempParams.updateValue(String(coldflu_covidscore), forKey:"coldflu_cvscore")
                    }
                    if let allergy_covidscore = json?.object(forKey: "allergy") as? Int{
                        self.setAllergyCovidScore(allergy_covidscore)
                        tempParams.updateValue(String(allergy_covidscore), forKey:"allergy_cvscore")
                    }
                    params.updateValue(tempParams, forKey:"params")
                    SWAnalytics.post(params: params)
                    setUserLatitudeLongitude(lat:latitude, lon:longitude)
                    completion(0)
                } catch {
                    SWAnalytics.post(params: params)
                    print("Error \(error)")
                }
            }else{
                SWAnalytics.post(params: params)
            }
        })
        task.resume()
    }

	private static func getSickScore(_ latitude: Double, longitude: Double,  params: Dictionary<String, Any>, completion : @escaping (_ response : Int?)-> ()) {
		var params = params
		let sickScoreUrl = "https://mobilesvc.sickweather.com/ws/v2.0/sickscore/getMultipleSickScoreInRadius.php?api_key=oWCYcO9KTEdnMduhBE6NHcDdVxHAzGBN&lat=\(latitude)&lon=\(longitude)"
        print("randolfTesting: lat=\(latitude) || lon=\(longitude)")
		let request = NSMutableURLRequest(url:URL(string: sickScoreUrl)!)
		request.httpMethod = "GET"
		let session = URLSession.shared
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error -> Void in
			if error == nil {
				do {
					let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
					var tempParams : [String:Any] = [:]
					if let sickscore = json?.object(forKey: "sickscore") as? Int{
						if let param = params["params"] as? [String:Any]{
							tempParams = param
						}
						self.setSickScore(sickscore)
						tempParams.updateValue(String(sickscore), forKey:"sickscore")
					}
					if let coldflu_sickscore = json?.object(forKey: "coldflu") as? Int{
						self.setColdFluSickScore(coldflu_sickscore)
						tempParams.updateValue(String(coldflu_sickscore), forKey:"coldflu_sickscore")
					}
					if let allergy_sickscore = json?.object(forKey: "allergy") as? Int{
						self.setAllergySickScore(allergy_sickscore)
						tempParams.updateValue(String(allergy_sickscore), forKey:"allergy_sickscore")
					}
					params.updateValue(tempParams, forKey:"params")
					SWAnalytics.post(params: params)
					setUserLatitudeLongitude(lat:latitude, lon:longitude)
					completion(0)
				} catch {
					SWAnalytics.post(params: params)
					print("Error \(error)")
				}
			}else{
				SWAnalytics.post(params: params)
			}
		})
		task.resume()
	}
	
	//MARK: Get UserAgent
	private static func makeUserAgent() -> String {
		let osVersion = UIDevice.current.systemVersion
		return "manufacturer:" + UIDevice.current.model + ";model:" + self.platform() + ";osVersion:" + osVersion
	}

	//MARK: Get Device Modal
	private static func platform() -> String {
		return UIDevice.current.modelName
	}

	//MARK: Save Data
	private static func saveData(params: Dictionary<String, Any>) {
		var dataObj: [[String: Any]] = []
		if let data = UserDefaults.standard.array(forKey: "params") as? [[String: Any]] {
			dataObj = data
		}
		dataObj.append(params)
		UserDefaults.standard.set(dataObj, forKey: "params")
	}

	private static func saveUserId (userId: String?) {
		if let userId = userId {
			UserDefaults.standard.set(userId, forKey: USER_ID)
		}
	}

	private static func saveOsUserId (osUserId: String) {
		UserDefaults.standard.set(osUserId, forKey: OS_USER_ID)
	}
	
	//Mark: User Lat Long
	private static func setUserCurrentLatLng (lat: Double, lng: Double) {
		self.userCurrentLatitude = lat
		self.userCurrentLongitude = lng
	}
	
	private static func getUserCurrentLat () -> Double {
		return self.userCurrentLatitude
	}
	
	private static func getUserCurrentLng () -> Double {
		return self.userCurrentLongitude
	}
	
	private static func setUserLatitudeLongitude(lat : Double?, lon : Double?) {
		if let lat = lat , let lon = lon {
			UserDefaults.standard.set(lat, forKey: LATITUDE)
			UserDefaults.standard.set(lon, forKey: LONGITUDE)
		}
	}
	
	private static func getUserLatitude() -> Double? {
		if let lat = UserDefaults.standard.value(forKey: LATITUDE) as? Double {
			return lat
		}
		return nil
	}
	
	private static func getUserLongitude() -> Double? {
		if let lon = UserDefaults.standard.value(forKey: LONGITUDE) as? Double {
			return lon
		}
		return nil
	}
    
    //MARK: Covid Score
    private static func setCovidScore(_ covidscore : Int?) {
        if let covidscore = covidscore {
            UserDefaults.standard.set(covidscore, forKey:COVIDSCORE)
        }
    }
    
    private static func getCovidScore() -> Int? {
        if let covidscore = UserDefaults.standard.value(forKey:COVIDSCORE) as? Int {
            return covidscore
        }
        return nil
    }
    
    private static func setAllergyCovidScore(_ covidscore : Int?) {
        if let covidscore = covidscore {
            UserDefaults.standard.set(covidscore, forKey:ALLERGY_COVIDSCORE)
        }
    }
    
    private static func getAllergyCovidScore() -> Int? {
        if let covidscore = UserDefaults.standard.value(forKey:ALLERGY_COVIDSCORE) as? Int {
            return covidscore
        }
        return nil
    }
    
    private static func setColdFluCovidScore(_ covidscore : Int?) {
        if let covidscore = covidscore {
            UserDefaults.standard.set(covidscore, forKey:COLDFLU_COVIDSCORE)
        }
    }
    
    private static func getColdFluCovidScore() -> Int? {
        if let covidscore = UserDefaults.standard.value(forKey:COLDFLU_COVIDSCORE) as? Int {
            return covidscore
        }
        return nil
    }
	
	//MARK: Sick Score
	private static func setSickScore(_ sickscore : Int?) {
		if let sickscore = sickscore {
			UserDefaults.standard.set(sickscore, forKey:SICKSCORE)
		}
	}
	
	private static func getSickScore() -> Int? {
		if let sickscore = UserDefaults.standard.value(forKey:SICKSCORE) as? Int {
			return sickscore
		}
		return nil
	}
	
	private static func setAllergySickScore(_ sickscore : Int?) {
		if let sickscore = sickscore {
			UserDefaults.standard.set(sickscore, forKey:ALLERGY_SICKSCORE)
		}
	}
	
	private static func getAllergySickScore() -> Int? {
		if let sickscore = UserDefaults.standard.value(forKey:ALLERGY_SICKSCORE) as? Int {
			return sickscore
		}
		return nil
	}
	
	private static func setColdFluSickScore(_ sickscore : Int?) {
		if let sickscore = sickscore {
			UserDefaults.standard.set(sickscore, forKey:COLDFLU_SICKSCORE)
		}
	}
	
	private static func getColdFluSickScore() -> Int? {
		if let sickscore = UserDefaults.standard.value(forKey:COLDFLU_SICKSCORE) as? Int {
			return sickscore
		}
		return nil
	}
	
	//MARK: Get Data
	private static func getData() {
		if let dataObj = UserDefaults.standard.array(forKey: "params") as? [[String: Any]] {
			for item in dataObj {
				var paramsData = ["lat": item["lat"]!, "lng": item["lng"]!,"created_time": item[CREATED_AT]!]
				if let extraParams = item["params"] {
					paramsData.updateValue(extraParams, forKey: "params")
				}
				SWAnalytics.post(params: paramsData)
			}
		}
		UserDefaults.standard.set([:], forKey: "params")
	}

	private static func getUserId () -> String? {
		return UserDefaults.standard.string(forKey: USER_ID)
	}

	private static func getOsUserId () -> String? {
		return UserDefaults.standard.string(forKey: OS_USER_ID)
	}

	//MARK: convert to ios format
	
	private static func getISO() -> String? {
		let formatter = DateFormatter()
		formatter.dateFormat = DATE_FORMAT
		return formatter.string(from: Date())
	}

	//MARK: Notifier

	@objc private static func reachabilityChanged(_ note: Foundation.Notification) {
		let reachability = note.object as! Reachability
		if reachability.isReachable {
			self.getData()
		}
	}
}

fileprivate class IDFA {
	//Stored Type Properties
	static let shared = IDFA()
	//Computed Instance Properties
	/// Returns `true` if the user has turned off advertisement tracking, else `false`.
	var limited: Bool {
		return !ASIdentifierManager.shared().isAdvertisingTrackingEnabled
	}

	/// Returns the identifier if the user has turned advertisement tracking on, else `nil`.
	var identifier: String? {
		guard !limited else { return nil }
		return ASIdentifierManager.shared().advertisingIdentifier.uuidString
	}
}

private extension UIDevice {
	var modelName: String {
		var systemInfo = utsname()
		uname(&systemInfo)
		let machineMirror = Mirror(reflecting: systemInfo.machine)
		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else { return identifier }
			return identifier + String(UnicodeScalar(UInt8(value)))
		}

		switch identifier {
		case "iPod5,1": return "iPod Touch 5"
		case "iPod7,1": return "iPod Touch 6"
		case "iPhone3,1", "iPhone3,2", "iPhone3,3": return "iPhone 4"
		case "iPhone4,1": return "iPhone 4s"
		case "iPhone5,1", "iPhone5,2": return "iPhone 5"
		case "iPhone5,3", "iPhone5,4": return "iPhone 5c"
		case "iPhone6,1", "iPhone6,2": return "iPhone 5s"
		case "iPhone7,2": return "iPhone 6"
		case "iPhone7,1": return "iPhone 6 Plus"
		case "iPhone8,1": return "iPhone 6s"
		case "iPhone8,2": return "iPhone 6s Plus"
		case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return "iPad 2"
		case "iPad3,1", "iPad3,2", "iPad3,3": return "iPad 3"
		case "iPad3,4", "iPad3,5", "iPad3,6": return "iPad 4"
		case "iPad4,1", "iPad4,2", "iPad4,3": return "iPad Air"
		case "iPad5,3", "iPad5,4": return "iPad Air 2"
		case "iPad2,5", "iPad2,6", "iPad2,7": return "iPad Mini"
		case "iPad4,4", "iPad4,5", "iPad4,6": return "iPad Mini 2"
		case "iPad4,7", "iPad4,8", "iPad4,9": return "iPad Mini 3"
		case "iPad5,1", "iPad5,2": return "iPad Mini 4"
		case "iPad6,7", "iPad6,8": return "iPad Pro"
		case "AppleTV5,3": return "Apple TV"
		case "i386", "x86_64": return "Simulator"
		default: return identifier
		}
	}
}
