//
//  SWMessageBannerView.m
//  Sickweather
//
//  Created by John Erck on 9/25/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

// OVERVIEW: THIS VIEW, LIKE MOST VIEWS, EXPECTS TO GET ITS SIZE SET FOR IT BY THE CONTROLLER
// THE INTERNAL DESIGN IS WRT TO AN ARBITRARY CONTAINER SIZE. USERS OF THIS VIEW DEFINE THE
// CONTAINER SIZE, AND THIS DOES THE REST INTERNALLY.

#import "SWMessageBannerView.h"

@interface SWMessageBannerView ()
@property (assign) BOOL setDismissModeAsAuto;
@property (assign) BOOL setDismissModeAsButton;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UILabel *messageLabel;
@property (strong, nonatomic) UIButton *dismissButton;
@end

@implementation SWMessageBannerView

#pragma mark - Public Methods

- (void)setMessage:(NSString *)message
{
    self.messageLabel.attributedText = [[NSAttributedString alloc] initWithString:message attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}];
}

- (void)setDismissMethodAsAuto
{
    self.setDismissModeAsAuto = YES;
    self.setDismissModeAsButton = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.delegate messageBannerViewWantsToDismiss:self];
    });
    self.dismissButton.hidden = YES;
}

- (void)setDismissMethodAsButton
{
    self.setDismissModeAsAuto = NO;
    self.setDismissModeAsButton = YES;
    self.dismissButton.hidden = NO;
}

#pragma mark - Target/Action

- (void)dismissButtonTapped:(id)sender
{
    [self.delegate messageBannerViewWantsToDismiss:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.contentView = [[UIView alloc] init];
        self.messageLabel = [[UILabel alloc] init];
        self.dismissButton = [[UIButton alloc] init];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.contentView];
        [self.contentView addSubview:self.messageLabel];
        [self.contentView addSubview:self.dismissButton];
        
        // CONFIG
        
        // Config CONTENT VIEW
        self.backgroundColor = [SWColor colorSickweatherStyleGuideGreen68x157x68];
        
        // Config MESSAGE LABEL
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config DISMISS BUTTON
        [self.dismissButton setTitle:@"Dismiss" forState:UIControlStateNormal];
        [self.dismissButton addTarget:self action:@selector(dismissButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
        [self.viewsDictionary setObject:self.messageLabel forKey:@"messageLabel"];
        [self.viewsDictionary setObject:self.dismissButton forKey:@"dismissButton"];
        
        // LAYOUT
        
        // Layout CONTENT VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout MESSAGE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[messageLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[messageLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // ESTABLISH DEFAULTS
        [self setDismissMethodAsAuto];
    }
    return self;
}

@end
