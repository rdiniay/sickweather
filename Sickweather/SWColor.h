//
//  SWColors.h
//  Sickweather
//
//  Created by John Erck on 1/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWColor : NSObject

// OLD
+ (UIColor *)colorSickweatherBlue41x171x226;
+ (UIColor *)colorSickweatherBlueSelected2x146x207;
+ (UIColor *)sickweatherSuperLightGray247x247x247;
+ (UIColor *)colorSickweatherFacebookBlue59x89x152;
+ (UIColor *)colorSickweatherGreen76x217x100;
+ (UIColor *)colorSickweatherGrayText135x135x135;
+ (UIColor *)colorSickweatherLightGray204x204x204;
+ (UIColor *)colorSickweatherDarkGrayText104x104x104;
+ (UIColor *)colorSickweatherWhite255x255x255;
+ (UIColor *)colorSickweatherBlack0x0x0;

// NEW
+ (UIColor *)colorSickweatherStyleGuideBlue37x169x224;
+ (UIColor *)colorSickweatherStyleGuideRed255x1x0;
+ (UIColor *)colorSickweatherStyleGuideOrange252x111x6;
+ (UIColor *)colorSickweatherStyleGuideYellow249x237x11;
+ (UIColor *)colorSickweatherStyleGuideGrayDark35x31x32;
+ (UIColor *)colorSickweatherStyleGuideGray79x76x77;
+ (UIColor *)colorSickweatherStyleGuideGrayLight133x133x134;
+ (UIColor *)colorSickweatherStyleGuideGreen68x157x68;
+ (UIColor *)colorSickweatherStyleGuideOffwhite246x246x246;
+ (UIColor *)colorSickweatherStyleGuideWalgreensRed227x24x55;
+ (UIColor *)colorSickweatherStyleGuideSearchBackground230x230x230;
+ (UIColor *)color:(UIColor *)color usingOpacity:(CGFloat)opacity;
+ (UIColor *)colorSickweatherGreen68x157x68;
// OTHER
+ (UIColor *)colorChatBoxGray;
+ (UIColor *)colorChatBoxGrayAccent;

@end
