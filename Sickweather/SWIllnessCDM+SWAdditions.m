//
//  SWIllnessCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 3/19/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWIllnessCDM+SWAdditions.h"

@implementation SWIllnessCDM (SWAdditions)

+ (SWIllnessCDM *)illnessGetUpdatedRecordUsingUnique:(NSString *)unique
                                      isGroupIllness:(NSString *)isGroupIllness
                                                name:(NSString *)name
                                  illnessDescription:(NSString *)illnessDescription
                                      selfReportText:(NSString *)selfReportText
                                           sortOrder:(NSNumber *)sortOrder
{
    // Define return var
    SWIllnessCDM *illness = nil;
    
    // Create and config database request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWIllnessCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"unique" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", unique];
    
    // Execute fetch request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    if (!matches || [matches count] > 1)
    {
        // Error
    }
    else if ([matches count] == 1)
    {
        // Use existing, after updating
        illness = [matches lastObject];
    }
    else
    {
        // Create new
        illness = [NSEntityDescription insertNewObjectForEntityForName:@"SWIllnessCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
    }
    
    // Update values
    if (illness)
    {
        illness.unique = unique;
        illness.isGroupIllness = isGroupIllness;
        illness.name = name;
        illness.illnessDescription = illnessDescription;
        illness.selfReportText = selfReportText;
        illness.sortOrder = sortOrder;
    }
    
    // Return object
    return illness;
}

+ (SWIllnessCDM *)illnessGetRecordUsingId:(NSString *)illnessId
{
    // Define return var
    SWIllnessCDM *illness = nil;
    
    // Create and config database request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWIllnessCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"unique" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", illnessId];
    
    // Execute fetch request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    if ([matches count] == 1)
    {
        // Found it
        illness = [matches lastObject];
    }
    
    // Return object
    return illness;
}

+ (NSArray *)illnessGetAll
{
    // Prevent crash
    if ([SWHelper helperManagedObjectContext])
    {
        NSManagedObjectContext *moc = [SWHelper helperManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"SWIllnessCDM" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique != %@", @"myReports"];
        [request setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
        [request setSortDescriptors:@[sortDescriptor]];
        
        NSError *error;
        return [moc executeFetchRequest:request error:&error];
    }
    return @[];
}

+ (NSArray *)illnessGetAllIndividuals
{
    // Prevent crash
    if ([SWHelper helperManagedObjectContext])
    {
        NSManagedObjectContext *moc = [SWHelper helperManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"SWIllnessCDM" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique != %@ AND isGroupIllness = %@", @"myReports", @"no"];
        [request setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        [request setSortDescriptors:@[sortDescriptor]];
        
        NSError *error;
        return [moc executeFetchRequest:request error:&error];
    }
    return @[];
}

+ (NSArray *)illnessGetAllGrouped
{
    // Prevent crash
    if ([SWHelper helperManagedObjectContext])
    {
        NSManagedObjectContext *moc = [SWHelper helperManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"SWIllnessCDM" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique != %@ AND isGroupIllness = %@", @"myReports", @"yes"];
        [request setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
        [request setSortDescriptors:@[sortDescriptor]];
        
        NSError *error;
        return [moc executeFetchRequest:request error:&error];
    }
    return @[];
}

+ (NSArray *)illnessGetMyReports
{
    // Prevent crash
    if ([SWHelper helperManagedObjectContext])
    {
        NSManagedObjectContext *moc = [SWHelper helperManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"SWIllnessCDM" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique = %@", @"myReports"];
        [request setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"unique" ascending:YES];
        [request setSortDescriptors:@[sortDescriptor]];
        
        NSError *error;
        return [moc executeFetchRequest:request error:&error];
    }
    return @[];
}

+ (void)illnessDeleteRecordUsingUnique:(NSString *)unique
{
    // Get illness type record itself
    NSFetchRequest *illnessRequest = [[NSFetchRequest alloc] init];
    [illnessRequest setEntity:[NSEntityDescription entityForName:@"SWIllnessCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]]];
    illnessRequest.predicate = [NSPredicate predicateWithFormat:@"unique = %@", unique];
    [illnessRequest setIncludesPropertyValues:NO]; // Only fetch the managedObjectID
    NSError *illnessRequestError = nil;
    NSArray *illnessRequestRecords = [[SWHelper helperManagedObjectContext] executeFetchRequest:illnessRequest error:&illnessRequestError];
    SWIllnessCDM *illnessTypeRecord = nil;
    for (SWIllnessCDM *illnessRecord in illnessRequestRecords)
    {
        illnessTypeRecord = illnessRecord;
    }
    
    if (illnessTypeRecord)
    {
        // Delete associated reports first, if any
        NSFetchRequest *reportsRequest = [[NSFetchRequest alloc] init];
        [reportsRequest setEntity:[NSEntityDescription entityForName:@"SWReportCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]]];
        reportsRequest.predicate = [NSPredicate predicateWithFormat:@"illness = %@", illnessTypeRecord];
        [reportsRequest setIncludesPropertyValues:NO]; // Only fetch the managedObjectID
        NSError *reportsRequestError = nil;
        NSArray *reportsRequestRecords = [[SWHelper helperManagedObjectContext] executeFetchRequest:reportsRequest error:&reportsRequestError];
        for (NSManagedObject *reportRecord in reportsRequestRecords)
        {
            [[SWHelper helperManagedObjectContext] deleteObject:reportRecord];
        }
        NSError *reportsRequestSaveError = nil;
        [[SWHelper helperManagedObjectContext] save:&reportsRequestSaveError];
        if (reportsRequestSaveError)
        {
            //NSLog(@"saveError = %@", reportsRequestSaveError);
        }
        
        // Now you can delete the illness type record itself
        for (NSManagedObject *illnessRecord in illnessRequestRecords)
        {
            [[SWHelper helperManagedObjectContext] deleteObject:illnessRecord];
        }
        NSError *illnessRequestSaveError = nil;
        [[SWHelper helperManagedObjectContext] save:&illnessRequestSaveError];
        if (illnessRequestSaveError)
        {
            //NSLog(@"saveError = %@", illnessRequestSaveError);
        }
    }
}

- (NSString *)nameWithWordsCapitalized
{
    NSString *name = [self.name capitalizedString];
    if ([[name lowercaseString] isEqualToString:@"rsv"])
    {
        name = @"RSV";
    }
    return name;
}

@end
