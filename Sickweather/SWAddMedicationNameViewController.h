//
//  SWAddMedicationNameViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 1/31/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SWAddMedicationNameViewController;

@protocol SWAddMedicationNameViewControllerDelegate <NSObject>
- (void)didDismissWithSuccessfulMedication:(NSString *)name;
@end


@interface SWAddMedicationNameViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id <SWAddMedicationNameViewControllerDelegate> delegate;
@end
