//
//  SWMyReportsCollectionViewCellForIllnessReportInstance.m
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWMyReportsCollectionViewCellForIllnessReportInstance.h"
#import "NSString+FontAwesome.h"

@interface SWMyReportsCollectionViewCellForIllnessReportInstance ()
@property (strong, nonatomic) NSArray *containerInsetConstraints;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *myContainerView;
@property (strong, nonatomic) UIButton *deleteReportButton;
@property (strong, nonatomic) UIButton *privatePublicIconButton;
@property (strong, nonatomic) UILabel *myDisplayWordLabel;
@property (strong, nonatomic) NSLayoutConstraint *myDisplayWordLabelDistanceFromTopConstraint;
@property (strong, nonatomic) UILabel *myTimeLabel;
@property (strong, nonatomic) UIView *topBorderView;
@end

@implementation SWMyReportsCollectionViewCellForIllnessReportInstance

#pragma mark - Custom Getters/Setters

- (void)setServerDict:(NSDictionary *)serverDict
{
    _serverDict = serverDict;
    if ([self.serverDict objectForKey:@"display_word"])
    {
        self.myDisplayWordLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.serverDict objectForKey:@"display_word"] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
    }
    if ([self.serverDict objectForKey:@"posted_time"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSString *serverDateFormat = @"yyyy-MM-dd H:mm:ss"; // e.g. 2016-02-16 19:04:47
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setDateFormat:serverDateFormat];
        NSDate *estDate = [dateFormatter dateFromString:[self.serverDict objectForKey:@"posted_time"]]; // EST date
        [dateFormatter setDateFormat:@"h:mm a"];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        self.myTimeLabel.attributedText = [[NSAttributedString alloc] initWithString:[dateFormatter stringFromDate:estDate] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:14],NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
    }
    
    // Manage public/private icon
    //NSLog(@"serverDict = %@", serverDict);
    if ([[serverDict objectForKey:@"visible"] isEqualToString:@"0"])
    {
        //NSLog(@"VISIBLE 0");
        self.myDisplayWordLabel.backgroundColor = [UIColor whiteColor];
        self.myDisplayWordLabelDistanceFromTopConstraint.constant = 8;
        NSMutableAttributedString *myFaLock = [[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaLock] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontAwesomeOfSize:10]}];
        [myFaLock appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@" Reported Privately" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontStandardFontOfSize:10]}]];
        [self.privatePublicIconButton addTarget:self action:@selector(privatePublicIconButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.privatePublicIconButton setAttributedTitle:myFaLock forState:UIControlStateNormal];
    }
    else if ([[serverDict objectForKey:@"visible"] isEqualToString:@"1"])
    {
        //NSLog(@"VISIBLE 1");
        self.myDisplayWordLabel.backgroundColor = [UIColor whiteColor];
        self.myDisplayWordLabelDistanceFromTopConstraint.constant = 16;
        NSMutableAttributedString *empty = [[NSMutableAttributedString alloc] initWithString:@"" attributes:@{}];
        [self.privatePublicIconButton setAttributedTitle:empty forState:UIControlStateNormal];
    }
    else
    {
        // Doesn't get executed
        //NSLog(@"Huh?");
    }
}

#pragma mark - Public Methods

- (void)myReportsCollectionViewCellForIllnessReportInstanceSetMarginLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom
{
	[self.contentView removeConstraints:self.containerInsetConstraints];
	self.containerInsetConstraints = @[];
	self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[myContainerView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary]];
	self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[myContainerView]-(bottom)-|" options:0 metrics:@{@"top":top,@"bottom":bottom} views:self.viewsDictionary]];
	[self.contentView addConstraints:self.containerInsetConstraints];
}

- (void)myReportsCollectionViewCellForIllnessReportInstanceSetButtonAttributedString:(NSAttributedString *)attributedString
{
    [self.deleteReportButton setAttributedTitle:attributedString forState:UIControlStateNormal];
}

- (void)myReportsCollectionViewCellForIllnessReportInstanceSetImage:(UIImage *)image
{
    
}

- (void)myReportsCollectionViewCellForIllnessReportInstanceSetLabelAttributedString:(NSAttributedString *)attributedString
{
    self.myDisplayWordLabel.attributedText = attributedString;
}

- (void)myReportsCollectionViewCellForIllnessReportInstanceSetTextViewAttributedString:(NSAttributedString *)attributedString
{
    
}

#pragma mark - Target/Action

- (void)deleteReportButtonTapped:(id)sender
{
    [self.delegate myReportsCollectionViewCellForIllnessReportInstanceDeleteButtonTapped:self];
}

- (void)privatePublicIconButtonTapped:(id)sender
{
    [self.delegate myReportsCollectionViewCellForIllnessReportInstancePrivatePublicIconButtonTapped:self];
}

#pragma mark - Life Cycle Methods

// http://stackoverflow.com/a/24626651/394969 <-- pointInside & hitTest (for buttons with negative offsets)
/*
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    // Check for YES via super
    if ([super pointInside:point withEvent:event])
    {
        return YES;
    }
    
    // Before returning NO, check to see if it is within deleteReportButton
    return !self.deleteReportButton.hidden && [self.deleteReportButton pointInside:[self.deleteReportButton convertPoint:point fromView:self] withEvent:event];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    // Check for hit within deleteReportButton
    UIView *view = [self.deleteReportButton hitTest:[self.deleteReportButton convertPoint:point fromView:self] withEvent:event];
    if (view == nil)
    {
        view = [super hitTest:point withEvent:event];
    }
    return view;
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.containerInsetConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.myContainerView = [[UIView alloc] init];
        self.deleteReportButton = [[UIButton alloc] init];
        self.privatePublicIconButton = [[UIButton alloc] init];
        self.myDisplayWordLabel = [[UILabel alloc] init];
        self.myTimeLabel = [[UILabel alloc] init];
        self.topBorderView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.myContainerView forKey:@"myContainerView"];
        [self.viewsDictionary setObject:self.deleteReportButton forKey:@"deleteReportButton"];
        [self.viewsDictionary setObject:self.privatePublicIconButton forKey:@"privatePublicIconButton"];
        [self.viewsDictionary setObject:self.myDisplayWordLabel forKey:@"myDisplayWordLabel"];
        [self.viewsDictionary setObject:self.myTimeLabel forKey:@"myTimeLabel"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.deleteReportButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.privatePublicIconButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.myDisplayWordLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.myTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.myContainerView];
        [self.myContainerView addSubview:self.deleteReportButton];
        [self.myContainerView addSubview:self.privatePublicIconButton];
        [self.myContainerView addSubview:self.myDisplayWordLabel];
        [self.myContainerView addSubview:self.myTimeLabel];
        [self.myContainerView addSubview:self.topBorderView];
        
        // LAYOUT
        
        // Layout myContainerView
        self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[myContainerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[myContainerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:self.containerInsetConstraints];
        
        // Layout deleteReportButton
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[deleteReportButton]-(10)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[deleteReportButton]-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout privatePublicIconButton
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[privatePublicIconButton]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[myDisplayWordLabel]-(-3)-[privatePublicIconButton]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout myDisplayWordLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[myDisplayWordLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        self.myDisplayWordLabelDistanceFromTopConstraint = [NSLayoutConstraint constraintWithItem:self.myDisplayWordLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.myContainerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
        [self.contentView addConstraint:self.myDisplayWordLabelDistanceFromTopConstraint];
        
        // Layout myTimeLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[myTimeLabel]-(40)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[myTimeLabel]-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout topBorderView
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[topBorderView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[topBorderView(1)]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config myContainerView
        //self.myContainerView.backgroundColor = [SWColor colorChatBoxGray210x213x219];
        
        // Config topBorderView
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config deleteReportButton
        NSMutableAttributedString *myFaTrashO = [[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaTrashO] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104], NSFontAttributeName:[SWFont fontAwesomeOfSize:16]}];
        [self.deleteReportButton addTarget:self action:@selector(deleteReportButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.deleteReportButton setAttributedTitle:myFaTrashO forState:UIControlStateNormal];
        
        // Config privatePublicIconButton
        [self.privatePublicIconButton addTarget:self action:@selector(privatePublicIconButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        //self.deleteReportButton.backgroundColor = [UIColor orangeColor];
        //self.deleteReportButton.layer.cornerRadius = 10;
        //self.deleteReportButton.layer.borderColor = [UIColor redColor].CGColor;
        //self.deleteReportButton.layer.borderWidth = 1;
        
        //self.privatePublicIconButton.backgroundColor = [UIColor orangeColor];
        //self.privatePublicIconButton.layer.cornerRadius = 10;
        //self.privatePublicIconButton.layer.borderColor = [UIColor redColor].CGColor;
        //self.privatePublicIconButton.layer.borderWidth = 1;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.myContainerView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
