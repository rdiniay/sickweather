//
//  SWAddMessageToReportCollectionViewCellForUserMessageTextView.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAddMessageToReportCollectionViewCellForUserMessageTextView : UICollectionViewCell
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UILabel *defaultLabel;
- (void)addMessageToReportCollectionViewCellForUserMessageTextViewSetUserMessageTextViewAttributedString:(NSAttributedString *)text;
@end
