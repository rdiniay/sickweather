//
//  SWAddMessageToReportCollectionViewCellForCustomView.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAddMessageToReportCollectionViewCellForCustomView : UICollectionViewCell
- (void)addMessageToReportCollectionViewCellForCustomViewSetView:(UIView *)view;
- (void)addMessageToReportCollectionViewCellForCustomViewSetView:(UIView *)view left:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
@end
