//
//  SWConstants.h
//  Sickweather
//
//  Created by John Erck on 10/10/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSWLoadMapForLocalNotificationOpenFromTerminatedState (@"kSWLoadMapForLocalNotificationOpenFromTerminatedState")
#define kSWHasBeenPromptedForUserLocationKey (@"kSWHasBeenPromptedForUserLocationKey")
#define kSWAppHasReceiveFirstDidAuthorizeNotificationForUserLocationAccess (@"kSWAppHasReceiveFirstDidAuthorizeNotificationForUserLocationAccess")
#define kSWAppHasReceiveFirstDidDenyNotificationForUserLocationAccess (@"kSWAppHasReceiveFirstDidDenyNotificationForUserLocationAccess")
#define kSWHasBeenAlertedAboutSkippingNotifications (@"kSWHasBeenAlertedAboutSkippingNotifications")
#define kSWStoryboardIDIllnessPickerTableViewCell (@"IllnessPickerTableViewCell")
#define kSWSegueIDPresentIllnessPicker1 (@"showMapSicknessPicker1")
#define kSWSegueIDPresentIllnessPicker2 (@"showMapSicknessPicker2")
#define kSWSignInViewControllerContextDefault (@"kSWSignInViewControllerContextDefault")
#define kSWSignInViewControllerContextReport (@"kSWSignInViewControllerContextReport")
#define kIsDevelopment YES;

@interface SWConstants : NSObject

// Defaults Keys
+ (NSString *)masterSwitchForSickweatherProximityBasedIllnessAlertsIsOnDefaultsKey;
+ (NSString *)hasPassedWelcomeToSickweatherSlideThroughScreensDefaultsKey;
+ (NSString *)hasPassedInitialAlertsSetupScreenDefaultsKey;
+ (NSString *)hasDismissedLoginScreenUserDefaultsKey;
+ (NSString *)hasBeenTippedAboutGeofenceOnButNoIllnessesSelectedDefaultsKey;
+ (NSString *)currentlyLoggedInUserSickweatherIdDefaultsKey; // Single interface into core data database which is where everything else is stored, unset to log user out
+ (NSString *)hasDismissedTodayWidgetAdViewDefaultsKey;
+ (NSString *)symptomsDefaultKey;
+ (NSString *)trackerTypesDefaultKey;
+ (NSString *)familyIdDefaultKey;
+ (NSString *)cityDefaultKey;
+ (NSString *)isCelsiusTemperatureTypeDefaultKey;

// Device keys
+ (NSString *)bluetoothSwaiveDeviceKey;
+ (NSString *)bluetoothKinsaDeviceKey;
+ (NSString *)bluetoothPhilipsDeviceKey;

// Formatting settings
+ (CGFloat)fontSizeForSignInFlowGrayLinkButtons;

// Tags
+ (NSInteger)drivingDirectionsLeftAccessoryControlTag;
+ (NSInteger)launchSafariRightAccessoryControlTag;
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewTag;
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewPhoneViewTag;
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewGetDirectionsViewTag;
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewSpecialOfferButtonTag;
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewProfilePicImageViewTag;

// NSNotificationCenter notification name constants (http://stackoverflow.com/a/539191/394969)
FOUNDATION_EXPORT NSString *const kSWLocationManagerDidEnterRegionNotification;
FOUNDATION_EXPORT NSString *const kSWLocationManagerDidDetermineStateNotification;
FOUNDATION_EXPORT NSString *const kSWLocationManagerMonitoringDidFailForRegionWithErrorNotification;
FOUNDATION_EXPORT NSString *const kSWLocationManagerDidStartMonitoringForRegionNotification;
FOUNDATION_EXPORT NSString *const kSWLocationManagerDidUpdateLocationsNotification;
FOUNDATION_EXPORT NSString *const kSWLocationManagerDidChangeAuthorizationStatusNotification;
FOUNDATION_EXPORT NSString *const kSWCoreDataManagedDocumentDidLoadNotificationKey;
FOUNDATION_EXPORT NSString *const kSWDidUpdateCurrentLocationPlacemarkNotification;
FOUNDATION_EXPORT NSString *const kSWECSlidingViewControllerDidSlideTopViewControllerBackOnStackNotification;
FOUNDATION_EXPORT NSString *const kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification;
FOUNDATION_EXPORT NSString *const kSWUserDidJustLogOutNotification;
FOUNDATION_EXPORT NSString *const kSWRefreshEventsNotification;
FOUNDATION_EXPORT NSString *const kSWRefreshFamilyMembersNotification;
FOUNDATION_EXPORT NSString *const kSWShowUserFeedBackFlowNotificationKey;

// Current user key, for coredata fetch of current user object
FOUNDATION_EXPORT NSString *const kSWCoreDataIsCurrentUserKeyYes;
FOUNDATION_EXPORT NSString *const kSWCoreDataIsCurrentUserKeyNo;


@end
