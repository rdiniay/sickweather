//
//  SWChangeLocationViewController.m
//  Sickweather
//
//  Created by John Erck on 5/7/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWChangeLocationViewController.h"
#import "SWSavedLocationCDM+SWAdditions.h"

@interface SWChangeLocationViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *changeLocationTextField;
@property (strong, nonatomic) UIScrollView *scrollView;
@end

@implementation SWChangeLocationViewController

#pragma mark - Helpers

- (void)reloadSavedLocations
{
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, [[SWSavedLocationCDM savedLocations] count] * 40);
    self.scrollView.contentOffset = CGPointZero;
    for (UIView *subview in self.scrollView.subviews)
    {
        [subview removeFromSuperview];
    }
    NSInteger count = 0;
    for (SWSavedLocationCDM *savedLocation in [SWSavedLocationCDM savedLocations])
    {
        CLPlacemark *placemark = [savedLocation placemark];
        SWDLog(@"savedLocations = %@", placemark);
        UIImageView *blueMagGlassIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue-mag-glass"]];
        blueMagGlassIcon.frame = CGRectMake(10, 10, blueMagGlassIcon.bounds.size.width, blueMagGlassIcon.bounds.size.height);
        UILabel *placemarkNameLabel = [[UILabel alloc] init];
        if (placemark.locality && placemark.administrativeArea)
        {
            placemarkNameLabel.text = [NSString stringWithFormat:@"%@, %@", placemark.locality, placemark.administrativeArea];
        }
        else if (!placemark.locality && placemark.administrativeArea)
        {
            placemarkNameLabel.text = [NSString stringWithFormat:@"%@", [SWHelper helperLongStateNameGivenTwoCharCode:placemark.administrativeArea]];
        }
        placemarkNameLabel.textColor = [SWColor colorSickweatherDarkGrayText104x104x104];
        placemarkNameLabel.font = [SWFont fontStandardFontOfSize:16];
        [placemarkNameLabel sizeToFit];
        placemarkNameLabel.frame = CGRectMake(40, 10, placemarkNameLabel.bounds.size.width, placemarkNameLabel.bounds.size.height);
        //placemarkNameLabel.backgroundColor = [UIColor redColor];
        UITapGestureRecognizer *xIconTappedGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(xIconTapped:)];
        UIImageView *xIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"grey-x"]];
        xIcon.userInteractionEnabled = YES;
        [xIcon addGestureRecognizer:xIconTappedGesture];
        xIcon.contentMode = UIViewContentModeCenter;
        xIcon.frame = CGRectMake(self.view.bounds.size.width-40, 0, 40, 40);
        //xIcon.backgroundColor = [UIColor purpleColor];
        UITapGestureRecognizer *savedLocationTappedGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(savedLocationTapped:)];
        UIView *view = [[UIView alloc] init];
        //view.backgroundColor = [UIColor orangeColor];
        [view addGestureRecognizer:savedLocationTappedGesture];
        view.tag = [[savedLocation savedLocationId] integerValue];
        view.frame = CGRectMake(0, 40*count, self.view.bounds.size.width, 40);
        //view.backgroundColor = [UIColor redColor];
        [view addSubview:blueMagGlassIcon];
        [view addSubview:placemarkNameLabel];
        [view addSubview:xIcon];
        [self.scrollView addSubview:view];
        count++;
    }
}

#pragma mark - Target Action

- (void)savedLocationTapped:(UITapGestureRecognizer *)tap
{
    //UIView *tappedView = tap.view;
    //SWDLog(@"tappedView = %ld", (long)tappedView.tag);
    SWSavedLocationCDM *savedLocation = [SWSavedLocationCDM savedLocationForId:[NSNumber numberWithInteger:tap.view.tag]];
    CLPlacemark *placemark = [savedLocation placemark];
    SWDLog(@"placemark = %@", placemark);
    [self.delegate didSuccessfullyCompleteSearchWithResultPlacemark:placemark];
}

- (void)xIconTapped:(UITapGestureRecognizer *)tap
{
    SWDLog(@"xIconTapped");
    UIView *tappedView = [tap.view superview];
    SWDLog(@"tappedView = %ld", (long)tappedView.tag);
    [SWSavedLocationCDM savedLocationDeleteRecordWithId:[NSNumber numberWithInteger:tappedView.tag]];
    [self reloadSavedLocations];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    SWDLog(@"textFieldDidBeginEditing");
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    SWDLog(@"textFieldDidEndEditing");
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(0, 170, spinner.frame.size.width, spinner.frame.size.height);
    spinner.center = CGPointMake(self.view.bounds.size.width/2, spinner.center.y);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    [[SWHelper helperGeocodeManager] geocodeAddressString:textField.text completionHandler:^(NSArray* placemarks, NSError* error)
    {
        SWDLog(@"error = %@", error);
        if ([placemarks count] > 0)
        {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            [SWSavedLocationCDM createSavedLocationForPlacemark:placemark];
            [self.delegate didSuccessfullyCompleteSearchWithResultPlacemark:placemark];
        }
        else
        {
            // No results found
        }
        [spinner stopAnimating];
        [spinner removeFromSuperview];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    /*
    if (textField == self.changeLocationTextField)
    {
        [self callUserNameMethod];
    }
    */
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Target Action

- (IBAction)upperRightBarButtonItemTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Lifecycle Methods

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // Update styling
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[SWColor colorSickweatherBlue41x171x226] forKey:NSForegroundColorAttributeName];
    
    // Setup text input and keyboard
    self.changeLocationTextField.delegate = self;
    self.changeLocationTextField.keyboardType = UIKeyboardTypeDefault;
    self.changeLocationTextField.returnKeyType = UIReturnKeyGo;
    self.changeLocationTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    if ([[SWSavedLocationCDM savedLocations] count] == 0)
    {
        [self.changeLocationTextField becomeFirstResponder];
    }
    
    UILabel *savedLocationsTitle = [[UILabel alloc] init];
    savedLocationsTitle.text = @"Saved Locations";
    savedLocationsTitle.textColor = [SWColor colorSickweatherDarkGrayText104x104x104];
    savedLocationsTitle.font = [SWFont fontStandardFontOfSize:12];
    [savedLocationsTitle sizeToFit];
    savedLocationsTitle.center = CGPointMake(self.view.bounds.size.width/2, 150);
    [self.view addSubview:savedLocationsTitle];
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = CGRectMake(0, 165, self.view.bounds.size.width, self.view.bounds.size.height-165);
    self.scrollView.alwaysBounceVertical = YES;
    [self.view addSubview:self.scrollView];
    
    [self reloadSavedLocations];
}

@end
