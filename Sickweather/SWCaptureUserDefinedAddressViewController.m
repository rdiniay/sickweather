//
//  SWCaptureUserDefinedAddressViewController.m
//  Sickweather
//
//  Created by John Erck on 10/7/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SWCaptureUserDefinedAddressViewController.h"
#import "SWCaptureUserDefinedAddressSectionHeaderView.h"
#import "SWCaptureUserDefinedAddressRowView.h"
#import "SWActivityIndicatorView.h"
#import "SWCaptureUserDefinedAddressViewController.h"
#import "NSString+FontAwesome.h"

#define rowHeight 50

@interface SWCaptureUserDefinedAddressViewController () <SWCaptureUserDefinedAddressRowViewDelegate, SWCaptureUserDefinedAddressSectionHeaderViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource , MKLocalSearchCompleterDelegate>
@property (strong, nonatomic) NSMutableDictionary *captureUserDefinedAddressData; // captureUserDefinedAddress.json
@property (weak, nonatomic) UITextField *activeField;
@property (strong, nonatomic) SWActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SWCaptureUserDefinedAddressSectionHeaderView *headerView;
@property (strong, nonatomic) SWCaptureUserDefinedAddressRowView *cityRowView;
@property (strong, nonatomic) SWCaptureUserDefinedAddressRowView *stateRegionRowView;
@property (strong, nonatomic) SWCaptureUserDefinedAddressRowView *countryRowView;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UITextField *validationTargetTextField;
@property (strong, nonatomic) UIAlertView *validationAlert;
@property (strong, nonatomic) NSMutableArray *placemarks;
@end

@implementation SWCaptureUserDefinedAddressViewController

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWCaptureUserDefinedAddressViewControllerDelegate>)delegate
{
    _delegate = delegate;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearchUsingText:searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        self.tableView.hidden = YES;
    }
    else
    {
        [self performSearchUsingText:searchText];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MKPlacemark *placemark = [self.placemarks objectAtIndex:indexPath.row];
    NSString *address = ABCreateStringWithAddressDictionary(placemark.addressDictionary, NO);
    address = [address stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    /*
    //NSLog(@"Address of placemark: %@", address);
    NSString *country = [placemark.country stringByReplacingOccurrencesOfString: @" " withString:@"-"]; // United States
    NSString *administrativeArea = [placemark.administrativeArea stringByReplacingOccurrencesOfString: @" " withString:@"-"]; // CA
    NSString *locality = [placemark.locality stringByReplacingOccurrencesOfString: @" " withString:@"-"]; // Huntington-Beach
    NSString *postalCode = [placemark.postalCode stringByReplacingOccurrencesOfString: @" " withString:@"-"]; // 92648
     */
    [self.cityRowView setUserText:placemark.locality];
    [self.stateRegionRowView setUserText:placemark.administrativeArea];
    [self.countryRowView setUserText:placemark.country];
    self.tableView.hidden = YES;
    [self rightBarButtonItemTapped:self];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    
    /*
     *   If the cell is nil it means no cell was available for reuse and that we should
     *   create a new one.
     */
    if (cell == nil)
    {
        
        /*
         *   Actually create a new cell (with an identifier so that it can be dequeued).
         */
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    /*
     *   Now that we have a cell we can configure it to display the data corresponding to
     *   this row/section
     */
    MKPlacemark *item = [self.placemarks objectAtIndex:indexPath.row];
    NSString *address = ABCreateStringWithAddressDictionary(item.addressDictionary, NO);
    address = [address stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    //NSLog(@"Address of placemark: %@", address);
    cell.textLabel.text = address;
    cell.detailTextLabel.text = address;
    //cell.imageView.image = theImage;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.placemarks.count == 0)
    {
        self.tableView.hidden = YES;
    }
    else
    {
        self.tableView.hidden = NO;
    }
    return self.placemarks.count;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:self.validationAlert])
    {
        if (self.validationTargetTextField)
        {
            [self.validationTargetTextField becomeFirstResponder];
        }
    }
}

#pragma mark - UIScrollViewDelegate

#pragma mark - SWCaptureUserDefinedAddressRowViewDelegate

- (NSString *)captureUserDefinedAddressRowViewTitleString:(SWCaptureUserDefinedAddressRowView *)sender
{
    NSString *answer;
    if ([sender isEqual:self.cityRowView])
    {
        answer = @"City";
    }
    else if ([sender isEqual:self.stateRegionRowView])
    {
        answer = @"State/Region";
    }
    else if ([sender isEqual:self.countryRowView])
    {
        answer = @"Country";
    }
    return answer;
}

#pragma mark - SWCaptureUserDefinedAddressSectionHeaderViewDelegate

- (NSString *)captureUserDefinedAddressSectionHeaderViewTitleString:(SWCaptureUserDefinedAddressSectionHeaderView *)sender
{
    return @"LOCATION";
}

#pragma mark - Target Action

- (void)leftBarButtonItemTapped:(id)sender
{
    return [self.delegate captureUserDefinedAddressViewControllerWantsToDismiss:self];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    return [self.delegate captureUserDefinedAddressViewController:self userInputCityString:[self.cityRowView userText] stateRegionString:[self.stateRegionRowView userText] countryString:[self.countryRowView userText]];
}

- (void)updateUIForSearchResults
{
    [self.tableView reloadData];
}

- (void)performSearchUsingText:(NSString *)searchText
{
    //NSLog(@"searchText = %@", searchText);
	MKLocalSearchCompleter *search = [[MKLocalSearchCompleter alloc] init];
    MKLocalSearchRequest *localSerachRequest = [[MKLocalSearchRequest alloc] init];
    localSerachRequest.naturalLanguageQuery = searchText;
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:localSerachRequest];
    [self.activityIndicator startAnimating];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse * _Nullable response, NSError * _Nullable error) {
        self.placemarks = [NSMutableArray array];
        for (MKMapItem *item in response.mapItems)
        {
            MKPlacemark *placemark = item.placemark;
            [self.placemarks addObject:placemark];
        }
        [self updateUIForSearchResults];
        [self.activityIndicator stopAnimating];
    }];
}

#pragma mark - Helpers

- (NSString *)listDataHeaderNameForSectionIndex:(NSUInteger)sectionIndex;
{
    return [[[[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"name"];
}

- (NSString *)listDataRowNameForSectionIndex:(NSUInteger)sectionIndex rowIndex:(NSUInteger)rowIndex;
{
    return [[[[[[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"rows"] objectAtIndex:rowIndex] objectForKey:@"name"];
}

- (NSDictionary *)listDataRowForSectionIndex:(NSUInteger)sectionIndex rowIndex:(NSUInteger)rowIndex
{
    return [[[[[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"rows"] objectAtIndex:rowIndex];
}

- (NSString *)listDataRowSystemNameForSectionIndex:(NSUInteger)sectionIndex rowIndex:(NSUInteger)rowIndex
{
    return [[[[[[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"rows"] objectAtIndex:rowIndex] objectForKey:@"system_value"];
}

- (NSString *)listDataGetTitle
{
    return [[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"title"];
}

- (NSNumber *)listDataGetSelectedRowIndex
{
    return [[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"selected_row_index"];
}

- (NSNumber *)listDataGetSelectedSectionIndex
{
    return [[self.captureUserDefinedAddressData objectForKey:@"list"] objectForKey:@"selected_section_index"];
}

- (void)loadJSONDataUsingFileBaseNameStringWithoutExtension:(NSString *)fileBaseNameStringWithoutExtension // e.g. captureUserDefinedAddress
{
    // GET BUNDLE PATH
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:fileBaseNameStringWithoutExtension ofType:@"json"];
    NSString *bundleJSONString = [NSString stringWithContentsOfFile:bundlePath encoding:NSUTF8StringEncoding error:nil];
    //NSLog(@"bundleJSONString = %@", bundleJSONString);
    
    // GET DOCUMENTS PATH
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", fileBaseNameStringWithoutExtension]];
    //NSLog(@"documentsPath %@", documentsPath);
    
    // IF THE DOCUMENTS PATH FILE DOES NOT EXIST, CREATE IT BASED ON THE BUNDLE PATH FILE
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsPath])
    {
        NSError *errorBundleJSONString;
        [bundleJSONString writeToFile:documentsPath atomically:YES encoding:NSUTF8StringEncoding error:&errorBundleJSONString];
    }
    
    // SET self.captureUserDefinedAddressData BASED ON DOCUMENTS PATH
    NSString *documentsPathJSONString = [NSString stringWithContentsOfFile:documentsPath encoding:NSUTF8StringEncoding error:nil];
    NSError *jsonParseError =  nil;
    self.captureUserDefinedAddressData = [NSJSONSerialization JSONObjectWithData:[documentsPathJSONString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonParseError];
}

- (void)saveJSONDataUsingFileBaseNameStringWithoutExtension:(NSString *)fileBaseNameStringWithoutExtension // e.g. captureUserDefinedAddress
{
    // GET DOCUMENTS PATH
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", fileBaseNameStringWithoutExtension]];
    //NSLog(@"documentsPath %@", documentsPath);
    
    // GET JSON DATA BASED ON self.captureUserDefinedAddressData
    NSError *jsonSerializationError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.captureUserDefinedAddressData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&jsonSerializationError];
    
    // GET JSON STRING BASED ON JSON DATA
    NSString *jsonString;
    if (!jsonData)
    {
        //NSLog(@"Got an error: %@", jsonSerializationError);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    // SAVE JSON STRING (self.captureUserDefinedAddressData) TO DOCUMENTS PATH
    NSError *errorDocumentsPathJSONString;
    [jsonString writeToFile:documentsPath atomically:YES encoding:NSUTF8StringEncoding error:&errorDocumentsPathJSONString];
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT HELPER PROPS
    self.viewsDictionary = [NSMutableDictionary new];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.activityIndicator = [[SWActivityIndicatorView alloc] init];
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.searchBar = [[UISearchBar alloc] init];
    self.tableView = [[UITableView alloc] init];
    self.headerView = [[SWCaptureUserDefinedAddressSectionHeaderView alloc] init];
    self.cityRowView = [[SWCaptureUserDefinedAddressRowView alloc] init];
    self.stateRegionRowView = [[SWCaptureUserDefinedAddressRowView alloc] init];
    self.countryRowView = [[SWCaptureUserDefinedAddressRowView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    [self.viewsDictionary setObject:self.activityIndicator forKey:@"activityIndicator"];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.searchBar forKey:@"searchBar"];
    [self.viewsDictionary setObject:self.tableView forKey:@"tableView"];
    [self.viewsDictionary setObject:self.headerView forKey:@"headerView"];
    [self.viewsDictionary setObject:self.cityRowView forKey:@"cityRowView"];
    [self.viewsDictionary setObject:self.stateRegionRowView forKey:@"stateRegionRowView"];
    [self.viewsDictionary setObject:self.countryRowView forKey:@"countryRowView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cityRowView.translatesAutoresizingMaskIntoConstraints = NO;
    self.stateRegionRowView.translatesAutoresizingMaskIntoConstraints = NO;
    self.countryRowView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.activityIndicator];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.searchBar];
    [self.contentView addSubview:self.tableView];
    [self.contentView addSubview:self.headerView];
    [self.contentView addSubview:self.cityRowView];
    [self.contentView addSubview:self.stateRegionRowView];
    [self.contentView addSubview:self.countryRowView];
    
    // LAYOUT
    
    // Layout ACTIVITY INDICATOR
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout SEARCH BAR
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[searchBar]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[searchBar(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout TABLE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[tableView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[searchBar][tableView(300)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout HEADER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[headerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[searchBar][headerView(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout CITY ROW VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[cityRowView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[headerView][cityRowView(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout STATE REGION ROW VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[stateRegionRowView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[cityRowView][stateRegionRowView(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout COUNTRY ROW VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[countryRowView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[stateRegionRowView][countryRowView(rowHeight)]|"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config TITLE
    self.title = [self listDataGetTitle];
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config ACTIVITY INDICATOR
    [self.activityIndicator setMessageText:@"Searching..."];
    [self.activityIndicator styleForInlineCenterScreenUse];
    [self.view bringSubviewToFront:self.activityIndicator];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // Config SEARCH BAR
    self.searchBar.delegate = self;
    
    // Config TABLE VIEW
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.hidden = YES;
    [self.contentView bringSubviewToFront:self.tableView];
    
    // Config HEADER VIEW
    self.headerView.delegate = self;
    
    // Config CITY ROW VIEW
    self.cityRowView.delegate = self;
    [self.cityRowView setUserText:[self.delegate captureUserDefinedAddressViewControllerCity]];
    
    // Config STATE REGION ROW VIEW
    self.stateRegionRowView.delegate = self;
    [self.stateRegionRowView setUserText:[self.delegate captureUserDefinedAddressViewControllerState]];
    
    // Layout COUNTRY ROW VIEW
    self.countryRowView.delegate = self;
    [self.countryRowView setUserText:[self.delegate captureUserDefinedAddressViewControllerCountry]];
    
    // COLOR FOR DEVELOPMENT PURPOSES
}

@end
