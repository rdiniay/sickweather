//
//  SWMyProfileSectionHeaderView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyProfileSectionHeaderView;

@protocol SWMyProfileSectionHeaderViewDelegate
- (NSString *)myProfileSectionHeaderViewTitleString:(SWMyProfileSectionHeaderView *)sender;
@end

@interface SWMyProfileSectionHeaderView : UIView
@property (nonatomic, weak) id<SWMyProfileSectionHeaderViewDelegate> delegate;
@end
