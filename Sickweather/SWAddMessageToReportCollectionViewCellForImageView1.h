//
//  SWImageView1FlowCollectionViewCell.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAddMessageToReportCollectionViewCellForImageView1 : UICollectionViewCell
- (void)addMessageToReportCollectionViewCellForImageView1SetImage:(UIImage *)image;
@end
