//
//  SWGroupProfileCollectionViewCellForEmptyMessageArea.h
//  Sickweather
//
//  Created by John Erck on 1/23/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWGroupProfileCollectionViewCellForEmptyMessageAreaDelegate
- (void)groupProfileCollectionViewCellForEmptyMessageAreaShareGroupButtonTapped:(id)sender;
@end

@interface SWGroupProfileCollectionViewCellForEmptyMessageArea : UICollectionViewCell
- (void)groupProfileCollectionViewCellForEmptyMessageAreaUpdateDefaultInsetsLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
@property (nonatomic, weak) id<SWGroupProfileCollectionViewCellForEmptyMessageAreaDelegate> delegate;
@end
