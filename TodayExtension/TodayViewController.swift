//
//  TodayViewController.swift
//  TodayExtension
//
//  Created by John Erck on 4/28/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreLocation

class TodayViewController: UIViewController, NCWidgetProviding, SWHighlightViewDelegate
{
    // MARK: Constants
    
    let kTodayWidgetHeight = 328 // Master value here, sync storyboard to this (pre-iOS 10)
    
    // MARK: Variables
    var cancelledOutCount = 0
    
    // MARK: Outlets
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    @IBOutlet weak var sizingView: UIView!
    @IBOutlet weak var styleRootView: UIView!
    @IBOutlet weak var styleRootViewLeftMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSectionContainerView: SWHighlightView!
    @IBOutlet weak var topSectionContainerViewButton: UIButton!
    @IBOutlet weak var topSectionContainerViewButtonLabel: UILabel!
    @IBOutlet weak var topSectionContainerViewButtonSickScoreLabel: UILabel!
    @IBOutlet weak var topSectionContainerViewLocationLabel: UILabel!
    @IBOutlet weak var topSectionContainerViewProgressView: UIProgressView!
    @IBOutlet weak var topSectionContainerViewProgressViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSectionContainerViewRiskLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var topContributingIllnessesLabel: UILabel!
    @IBOutlet weak var topIllnessRowContainerView0: SWHighlightView!
    @IBOutlet weak var topIllnessRowContainerView0Button: UIButton!
    @IBOutlet weak var topIllnessRowContainerView0Label: UILabel!
    @IBOutlet weak var topIllnessRowContainerView1: SWHighlightView!
    @IBOutlet weak var topIllnessRowContainerView1Button: UIButton!
    @IBOutlet weak var topIllnessRowContainerView1Label: UILabel!
    @IBOutlet weak var topIllnessRowContainerView2: SWHighlightView!
    @IBOutlet weak var topIllnessRowContainerView2Button: UIButton!
    @IBOutlet weak var topIllnessRowContainerView2Label: UILabel!
    
    // MARK: SWHighlightViewDelegate
    
    func highlightViewTouchUpInside(_ view: UIView) {
        let defaults = UserDefaults.standard
        var url = URL(string: "sickweather://")
        if view == topSectionContainerView {
            url = URL(string: "sickweather://")
        } else if view == topIllnessRowContainerView0 {
            let stringId:String = defaults.object(forKey: "topIllnessRowContainerView0IllnessId") as! String
            url = URL(string: "sickweather://maps/\(stringId)")
        } else if view == topIllnessRowContainerView1 {
            let stringId:String = defaults.object(forKey: "topIllnessRowContainerView1IllnessId") as! String
            url = URL(string: "sickweather://maps/\(stringId)")
        } else if view == topIllnessRowContainerView2 {
            let stringId:String = defaults.object(forKey: "topIllnessRowContainerView2IllnessId") as! String
            url = URL(string: "sickweather://maps/\(stringId)")
        }
        self.extensionContext?.open(url!, completionHandler: { (success) -> Void in
            print("Done...")
        });
    }
    
    func highlightViewTouchCancelled(_ view: UIView) {
        
        // Increment, then test for debug trigger...
        cancelledOutCount += 1
        if cancelledOutCount > 2 {
            latLabel.isHidden = false
            lonLabel.isHidden = false
        }
    }
    
    func highlightViewBaseColor(_ view: UIView) -> UIColor {
        return UIColor(red: CGFloat(255/255.0), green: CGFloat(255/255.0), blue: CGFloat(255/255.0), alpha: 0.0)
    }
    
    func highlightViewHighlightColor(_ view: UIView) -> UIColor {
        return UIColor(red: CGFloat(255/255.0), green: CGFloat(255/255.0), blue: CGFloat(255/255.0), alpha: 0.2)
    }
    
    // MARK: Helper Methods
    
    func helperKLookupList() -> [[String:String]]
    {
        // http://stackoverflow.com/a/27531394/394969
        var test = [["id":"18","name":"Allergies","iconLetters":"Al"]]
        test.append(["id":"30","name":"Asthma","iconLetters":"As"])
        test.append(["id":"2","name":"Bronchitis","iconLetters":"Br"])
        test.append(["id":"10","name":"Chicken Pox","iconLetters":"Ch"])
        test.append(["id":"4","name":"Common Cold","iconLetters":"Co"])
        test.append(["id":"6","name":"Cough","iconLetters":"Cg"])
        test.append(["id":"14","name":"Croup","iconLetters":"Cr"])
        test.append(["id":"23","name":"Ear Infection","iconLetters":"Ea"])
        test.append(["id":"11","name":"Fever","iconLetters":"Fe"])
        test.append(["id":"31","name":"Fifth Disease","iconLetters":"Fi"])
        test.append(["id":"1","name":"Flu","iconLetters":"Fl"])
        test.append(["id":"16","name":"Hand Foot And Mouth","iconLetters":"Ha"])
        test.append(["id":"25","name":"Man Flu","iconLetters":"Ma"])
        test.append(["id":"5","name":"Nasal Congestion","iconLetters":"Na"])
        test.append(["id":"32","name":"Norovirus","iconLetters":"No"])
        test.append(["id":"22","name":"Pink Eye","iconLetters":"Pi"])
        test.append(["id":"15","name":"Pneumonia","iconLetters":"Pn"])
        test.append(["id":"33","name":"RSV","iconLetters":"Rs"])
        test.append(["id":"20","name":"Sinus Infection","iconLetters":"Si"])
        test.append(["id":"21","name":"Sore Throat","iconLetters":"So"])
        test.append(["id":"24","name":"Stomach Virus","iconLetters":"Sv"])
        test.append(["id":"7","name":"Strep Throat","iconLetters":"St"])
        test.append(["id":"3","name":"Whooping Cough","iconLetters":"Wh"])
        return test
    }
    
    func helperUpdateUIGivenCurrentNSUserDefaultsSickScoreValueAnimated(_ animated: Bool)
    {
        // Defaults
        let defaults = UserDefaults.standard
        
        // Get color risk
        let currentColorRisk = helperSickScoreGetColorRiskGlanceBackgroundImageNameForScore(defaults.integer(forKey: "sickScore"))
        
        let locationSource = defaults.string(forKey: "locationSource")
        let sourceLat = defaults.string(forKey: "sourceLat")
        let sourceLon = defaults.string(forKey: "sourceLon")
        latLabel.text = "Lat: \(sourceLat!)"
        lonLabel.text = "Lon: \(sourceLon!) \(locationSource!)"
        
        // Set SickScore value
        topSectionContainerViewButtonLabel.text = defaults.string(forKey: "sickScore")
        topSectionContainerViewButton.layer.borderColor = UIColor.white.cgColor
        
        // Set CityLabelText value
        topSectionContainerViewLocationLabel.text = defaults.string(forKey: "cityLabelText")
        
        // Style risk statement
        topSectionContainerViewRiskLabel.textColor = currentColorRisk.color
        topSectionContainerViewRiskLabel.text = currentColorRisk.risk
        
        // Style progress bar
        topSectionContainerViewProgressView.progressTintColor = currentColorRisk.color
        if let myProgress = defaults.string(forKey: "progress") {
            topSectionContainerViewProgressView.setProgress((myProgress as NSString).floatValue, animated: animated)
        }
        
        // Update rows
        self.topIllnessRowContainerView0Button.setTitle(defaults.string(forKey: "topIllnessRowContainerView0ButtonTitle"), for: UIControl.State())
        self.topIllnessRowContainerView0Label.text = defaults.string(forKey: "topIllnessRowContainerView0LabelText")
        
        self.topIllnessRowContainerView1Button.setTitle(defaults.string(forKey: "topIllnessRowContainerView1ButtonTitle"), for: UIControl.State())
        self.topIllnessRowContainerView1Label.text = defaults.string(forKey: "topIllnessRowContainerView1LabelText")
        
        self.topIllnessRowContainerView2Button.setTitle(defaults.string(forKey: "topIllnessRowContainerView2ButtonTitle"), for: UIControl.State())
        self.topIllnessRowContainerView2Label.text = defaults.string(forKey: "topIllnessRowContainerView2LabelText")
    }
    
    func helperUpdateSickScoreScreenUsingCompletionHandler(_ completionHandler: ((_ updateResult: NCUpdateResult) -> Void)?)
    {
        // Run chain of events to get updated sick score info
        
        // Show that we're Scanning...
        topSectionContainerViewLocationLabel.text = "Scanning..."
        topSectionContainerViewProgressView.progress = 0
        
        // Load...
        self.helperGetSickScoreForLocation(completionHandler: { (sickScore, cityLabelText, topContributors, sourceLat, sourceLon, locationSource) -> Void in
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 1.0, animations: { () -> Void in
                    let defaults = UserDefaults.standard
                    var count = 0
                    for illness in topContributors
                    {
                        print("illness.id = \(illness.id)")
                        if count == 0
                        {
                            defaults.set(illness.id, forKey: "topIllnessRowContainerView0IllnessId")
                            defaults.set(illness.iconLetters, forKey: "topIllnessRowContainerView0ButtonTitle")
                            defaults.set(illness.illnessName, forKey: "topIllnessRowContainerView0LabelText")
                        }
                        else if count == 1
                        {
                            defaults.set(illness.id, forKey: "topIllnessRowContainerView1IllnessId")
                            defaults.set(illness.iconLetters, forKey: "topIllnessRowContainerView1ButtonTitle")
                            defaults.set(illness.illnessName, forKey: "topIllnessRowContainerView1LabelText")
                        }
                        else
                        {
                            defaults.set(illness.id, forKey: "topIllnessRowContainerView2IllnessId")
                            defaults.set(illness.iconLetters, forKey: "topIllnessRowContainerView2ButtonTitle")
                            defaults.set(illness.illnessName, forKey: "topIllnessRowContainerView2LabelText")
                        }
                        count += 1
                    }
                    let progress = (sickScore as NSString).floatValue/100.0
                    defaults.set(locationSource, forKey: "locationSource")
                    defaults.set(sourceLat, forKey: "sourceLat")
                    defaults.set(sourceLon, forKey: "sourceLon")
                    defaults.set(cityLabelText, forKey: "cityLabelText")
                    defaults.set(sickScore, forKey: "sickScore")
                    defaults.set("\(progress)", forKey: "progress")
                    self.helperUpdateUIGivenCurrentNSUserDefaultsSickScoreValueAnimated(true)
                    }, completion: { (finished) -> Void in
                        completionHandler?(NCUpdateResult.newData)
                })
            })
        })
    }
    
    func helperGetLocationViaIPAddressUsingCompletionHandler(_ completionHandler: @escaping (_ location: CLLocation, _ locationSource: String) -> Void)
    {
        let urlPath: String = "http://www.telize.com/geoip?"
        let url: URL = URL(string: urlPath)!
        let request: URLRequest = URLRequest(url: url)
        let queue:OperationQueue = OperationQueue()
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: URLResponse?, data: Data?, error: NSError?) -> Void in
            if (error == nil && data != nil)
            {
                // Success!
                let jsonResult = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! Dictionary<String, AnyObject>
                //println("jsonResult = \(jsonResult)")
                /*
                jsonResult = {
                "area_code" = 0;
                asn = AS20001;
                city = "Huntington Beach";
                "continent_code" = NA;
                country = "United States";
                "country_code" = US;
                "country_code3" = USA;
                "dma_code" = 0;
                ip = "104.34.204.100";
                isp = "Time Warner Cable Internet LLC";
                latitude = "33.6746";
                longitude = "-118.0086";
                offset = "-7";
                "postal_code" = 92648;
                region = California;
                "region_code" = CA;
                timezone = "America/Los_Angeles";
                }
                */
                if let latitude: AnyObject = jsonResult["latitude"] {
                    if let longitude: AnyObject = jsonResult["longitude"] {
                        completionHandler(CLLocation(latitude: latitude as! Double, longitude: longitude as! Double), "www.telize.com")
                    } else {
                        completionHandler(CLLocation(latitude: 0.0, longitude: 0.0), "Failed telize 1")
                    }
                } else {
                    completionHandler(CLLocation(latitude: 0.0, longitude: 0.0), "Failed telize 2")
                }
            }
            else
            {
                completionHandler(CLLocation(latitude: 0.0, longitude: 0.0), "Failed telize 3")
            }
        } as! (URLResponse?, Data?, Error?) -> Void)
    }
    
    func helperGetSickScoreForLocation(completionHandler: @escaping (_ sickScore: String, _ cityLabelText: String, _ topContributors: [(id: String, iconLetters: String, illnessName: String)], _ sourceLat: String, _ sourceLon: String, _ locationSource: String) -> Void)
    {
        // NO NEED TO GO TO NETWORK FROM HERE, JUST READ FROM DISK, CONTAINING APP IS RESPONSIBLE FOR KEEPING THE UNDERLYING
        // FILE SYSTEM FILES/NSUSERDEFAULTS UP TO DATE...
        
        /*
        // READ DATA FROM DISK (HERE JUST AS AN EXAMPLE, FOR IF NEEDED LATER)
        let groupURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.sickweather.Sickweather");
        let getSickScoreInRadiusResponseLocalFileSystemURL = groupURL?.appendingPathComponent("getSickScoreInRadius");
        var getSickScoreInRadiusResponseString: String?;
        do
        {
            getSickScoreInRadiusResponseString = try NSString(contentsOf: getSickScoreInRadiusResponseLocalFileSystemURL!, encoding: String.Encoding.utf8.rawValue) as String;
        }
        catch
        {
            print(error);
        }
        if (getSickScoreInRadiusResponseString != nil)
        {
            print("getSickScoreInRadiusResponseString = \(getSickScoreInRadiusResponseString)");
        }
        */
        
        // READ DATA FROM USER DEFAULTS
        let defaults = UserDefaults(suiteName: "group.com.sickweather.Sickweather");
        let jsonResult: [String:Any]? = defaults?.dictionary(forKey: "getSickScoreInRadius");
        let placemark: [String:Any]? = defaults?.dictionary(forKey: "placemark");
        //print(placemark);
        
        // SET LAT/LON STRING VARS
        //let sourceLatString = "\(location.coordinate.latitude)"
        //let sourceLonString = "\(location.coordinate.longitude)"
        
        // GET CITY NAME
        var finalPlacemarkLocality = "Unknown";
        if let myPlacemarkLocality: String = placemark?["locality"] as? String {
            finalPlacemarkLocality = myPlacemarkLocality;
        }
        
        // SET VARIABLES USING THE PROCESS BELOW
        var sickScoreToReturn = "0";
        var topIllnesses:[(id: String, iconLetters: String, illnessName: String)] = [];
        if let mySickScore = jsonResult?["sickscore"] as? NSNumber
        {
            // We do have a sickscore to work with
            sickScoreToReturn = "\(mySickScore.intValue)"
            
            if let illnessArr: [NSDictionary] = jsonResult?["illnesses"] as? [NSDictionary]
            {
                for illnessObj: AnyObject in illnessArr
                {
                    if let illnessObj = illnessObj as? NSDictionary
                    {
						if let id = illnessObj.object(forKey: "illness_id") as? String , let iconLetters = illnessObj.object(forKey: "illness_short_code") as? String  , let illnessName = illnessObj.object(forKey: "illness_name") as? String  {
							let capitalizedString = illnessName == "RSV" ? illnessName : illnessName.capitalized
                            topIllnesses.append((id:id, iconLetters:iconLetters, illnessName:capitalizedString))
						}
                    }
                }
            }
            completionHandler(sickScoreToReturn, finalPlacemarkLocality, topIllnesses, "", "", "");
        }
        else
        {
            // We do not have a sickscore to work with
            completionHandler(sickScoreToReturn, finalPlacemarkLocality, topIllnesses, "", "", "");
        }
        
        // TRIGGER BACKGROUND NETWORK REQUEST TO FETCH THE LATEST SICKSCORE INFO <- LEAVING IN, BUT I DON'T THINK IS NEEDED...
        /*
        let config: URLSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "com.sickweather.Sickweather.backgroundsession");
        config.sharedContainerIdentifier = "group.com.sickweather.Sickweather";
        config.sessionSendsLaunchEvents = true;
        let session: URLSession = URLSession.init(configuration: config);
        let sickScoreURL: URL = URL(string: "https://mobilesvc.sickweather.com/ws/v1.1/getSickScoreInRadius.php?api_key=oWCYcO9KTEdnMduhBE6NHcDdVxHAzGBN&lat=\(sourceLatString)&lon=\(sourceLonString)&ids=1&limit=10000&pretty=0")!;
        let backgroundDownloadTask = session.downloadTask(with: sickScoreURL);
        backgroundDownloadTask.resume();
         */
    }
    
    func helperUpdateForLandscape()
    {
        /*
        if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Unspecified
        {
        println("Unspecified")
        }
        if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact
        {
        println("Compact")
        }
        if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular
        {
        println("Regular")
        }
        
        println("view.bounds.size.height = \(view.bounds.size.height)")
        println("view.bounds.size.width = \(view.bounds.size.width)")
        
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.Unknown
        {
        println("Unknown")
        }
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.Portrait
        {
        println("Portrait")
        }
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.PortraitUpsideDown
        {
        println("PortraitUpsideDown")
        }
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft
        {
        println("LandscapeLeft")
        }
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeRight
        {
        println("LandscapeRight")
        }
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.FaceUp
        {
        println("FaceUp")
        }
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.FaceDown
        {
        println("FaceDown")
        }
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad
        {
        println("Pad")
        }
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
        println("Phone")
        }
        if UIDevice.currentDevice().userInterfaceIdiom == .Unspecified
        {
        println("Unspecified")
        }
        */
        preferredContentSize = CGSize(width: 0, height: 130)
        styleRootViewLeftMarginConstraint.constant = 47
        separatorView.backgroundColor = UIColor.clear
    }
    
    func helperSickScoreGetColorRiskGlanceBackgroundImageNameForScore(_ sickScore: Int) -> (color: UIColor, risk: String, glanceBackgroundImageName: String)
    {
        var currentColor = SWColor.blueColor()
        var currentRisk = "VERY LOW RISK"
        var glanceBackgroundImageName = "sickscore-glance-background-blue"
        switch UserDefaults.standard.integer(forKey: "sickScore") {
        case 0...25:
            currentColor = SWColor.blueColor()
            currentRisk = "VERY LOW RISK"
            glanceBackgroundImageName = "sickscore-glance-background-blue"
        case 25...50:
            currentColor = SWColor.yellowColor()
            currentRisk = "LOW RISK"
            glanceBackgroundImageName = "sickscore-glance-background-yellow"
        case 50...75:
            currentColor = SWColor.orangeColor()
            currentRisk = "MEDIUM RISK"
            glanceBackgroundImageName = "sickscore-glance-background-orange"
        case 75...100:
            currentColor = SWColor.redColor()
            currentRisk = "HIGH RISK"
            glanceBackgroundImageName = "sickscore-glance-background-red"
        default:
            currentColor = SWColor.blueColor()
            currentRisk = "VERY LOW RISK"
            glanceBackgroundImageName = "sickscore-glance-background-blue"
        }
        return (currentColor, currentRisk, glanceBackgroundImageName)
    }
    
    // MARK: NCWidgetProviding Protocol Methods
    
    func widgetMarginInsets(forProposedMarginInsets defaultMarginInsets: UIEdgeInsets) -> (UIEdgeInsets)
    {
        return UIEdgeInsets.zero
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void))
    {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        // Update
        helperUpdateSickScoreScreenUsingCompletionHandler(completionHandler)
        completionHandler(NCUpdateResult.newData)
    }
    
    // MARK: Life Cycle Methods
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews()
    {
        // Check for special iOS 10+ methods
        if #available(iOSApplicationExtension 10.0, *)
        {
            // Nothing special to do
        }
        // Fallback to pre-iOS 10 methods
        else
        {
            //println("sizingView = \(sizingView.frame)")
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                //println("Pad")
            }
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                //println("Phone")
                if sizingView.frame.width > 414.0 /*Style for landscape if width is greater than that of 6 plus*/
                {
                    helperUpdateForLandscape()
                }
            }
            if UIDevice.current.userInterfaceIdiom == .unspecified
            {
                //println("Unspecified")
            }
        }
        super.viewDidLayoutSubviews()
    }
    
    @available(iOS 10, *)
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize)
    {
        //print(maxSize);
        self.topConstraint.constant = 9;
        if (activeDisplayMode == NCWidgetDisplayMode.compact) // fixed height (110)
        {
            self.preferredContentSize = maxSize;
        }
        else if (activeDisplayMode == NCWidgetDisplayMode.expanded) // variable height
        {
            if (maxSize.height > CGFloat(308))
            {
                self.preferredContentSize = CGSize(width: maxSize.width, height: CGFloat(308));
            }
            else
            {
                self.preferredContentSize = CGSize(width: maxSize.width, height: CGFloat(110));
            }
        }
    }
    
    override func viewDidLoad()
    {
        // Invoke super
        super.viewDidLoad()
        
        // Do any additional setup after loading the view from its nib.
        
        // For debugging...
        latLabel.isHidden = true
        lonLabel.isHidden = true
        
        // Register default values
        UserDefaults.standard.register(defaults: [
            "locationSource": "Unknown",
            "sourceLat": "0",
            "sourceLon": "0",
            "sickScore": "0",
            "cityLabelText": "Scanning...",
            "progress": "0.0",
            "topIllnessRowContainerView0IllnessId": "1",
            "topIllnessRowContainerView1IllnessId": "1",
            "topIllnessRowContainerView2IllnessId": "1",
        ])
        
        // Check for special iOS 10+ methods
        if #available(iOSApplicationExtension 10.0, *)
        {
            self.extensionContext?.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded;
        }
        // Fallback to pre-iOS 10 methods
        else
        {
            // Define our size internally by defining our "preferred content size"
            self.preferredContentSize = CGSize(width: 0, height: CGFloat(kTodayWidgetHeight))
        }
        
        // Make main view's background color fully transparent
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        // Style SickScore circle
        topSectionContainerViewButton.setBackgroundImage(nil, for: UIControl.State())
        topSectionContainerViewButton.layer.borderColor = UIColor.white.cgColor
        topSectionContainerViewButton.layer.borderWidth = 4
        topSectionContainerViewButton.layer.cornerRadius = topSectionContainerViewButton.bounds.width/2
        
        // Style progress bar
        topSectionContainerViewProgressView.trackTintColor = SWColor.grayColor()
        topSectionContainerViewProgressView.layer.cornerRadius = 5
        topSectionContainerViewProgressView.clipsToBounds = true
        topSectionContainerViewProgressViewHeightConstraint.constant = 10
        
        // Style rows
        topIllnessRowContainerView0Button.layer.borderColor = SWColor.whiteColor().cgColor
        topIllnessRowContainerView0Button.layer.borderWidth = 1
        topIllnessRowContainerView0Button.layer.cornerRadius = topIllnessRowContainerView0Button.bounds.width/2
        
        topIllnessRowContainerView1Button.layer.borderColor = SWColor.whiteColor().cgColor
        topIllnessRowContainerView1Button.layer.borderWidth = 1
        topIllnessRowContainerView1Button.layer.cornerRadius = topIllnessRowContainerView0Button.bounds.width/2
        
        topIllnessRowContainerView2Button.layer.borderColor = SWColor.whiteColor().cgColor
        topIllnessRowContainerView2Button.layer.borderWidth = 1
        topIllnessRowContainerView2Button.layer.cornerRadius = topIllnessRowContainerView0Button.bounds.width/2
        
        let debugColors = false
        if debugColors
        {
            // Do nothing
        }
        else
        {
            sizingView.backgroundColor = UIColor.clear
            styleRootView.backgroundColor = UIColor.clear
            topSectionContainerView.backgroundColor = UIColor.clear
            topIllnessRowContainerView0.backgroundColor = UIColor.clear
            topIllnessRowContainerView1.backgroundColor = UIColor.clear
            topIllnessRowContainerView2.backgroundColor = UIColor.clear
        }
        
        topSectionContainerView.delegate = self
        topIllnessRowContainerView0.delegate = self
        topIllnessRowContainerView1.delegate = self
        topIllnessRowContainerView2.delegate = self
        topSectionContainerViewButton.isUserInteractionEnabled = CBool(false)
        
        // Update
        helperUpdateUIGivenCurrentNSUserDefaultsSickScoreValueAnimated(false)
    }
}
