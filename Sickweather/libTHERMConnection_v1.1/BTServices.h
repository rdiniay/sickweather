//
//  BTServices.h
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/13/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#ifndef BTServices_h
#define BTServices_h


// Current Time Service
#define CurrentTimeServiceID        @"1805"

// Health Thermometer Service
#define HealthThermometerServiceID  @"1809"

// Device Information Service
#define DeviceInformationServiceID  @"180A"

// Battery Service
#define BatteryServiceID            @"180F"

// Custom Service (Temperature)
#define CustomServiceID             @"2893B28B-C868-423A-9DC2-E9C2FCB4EBB5"



#endif /* BTServices_h */
