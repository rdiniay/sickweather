//
//  SWAnalytics.h
//  SWAnalytics
//
//  Created by Shan Shafiq on 3/28/17.
//  Copyright © 2017 TechnlogyRivers. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SWAnalytics.
FOUNDATION_EXPORT double SWAnalyticsVersionNumber;

//! Project version string for SWAnalytics.
FOUNDATION_EXPORT const unsigned char SWAnalyticsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SWAnalytics/PublicHeader.h>


