//
//  SWMyReportsViewController.m
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWMyReportsViewController.h"
#import "SWMyReportsCollectionView.h"
#import "SWMyReportsCollectionViewFlowLayout.h"
#import "SWMyReportsHeaderView.h"
#import "SWMyReportsCollectionViewCellForIllnessReportInstance.h"
#import "SWMyReportsCollectionViewCellForType2.h"
#import "NSAttributedString+Height.h"
#import "SWReportCDM+SWAdditions.h"
#import "Flurry.h"
#import "SWEditMyProfileViewController.h"

@interface SWMyReportsViewController () <UIActionSheetDelegate, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SWMyReportsCollectionViewCellForIllnessReportInstance, SWMyReportsCollectionViewCellForType2Delegate, UIGestureRecognizerDelegate, SWEditMyProfileViewControllerDelegate>
@property (strong, nonatomic) NSDictionary *serverResponse; // objectForKey:@"data" typically holds "the array" you're looking for
@property (strong, nonatomic) NSArray *cvDataObjects;
@property (strong, nonatomic) SWMyReportsCollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) SWMyReportsCollectionView *collectionView;
@property (strong, nonatomic) SWMyReportsHeaderView *header;
@property (strong, nonatomic) UIActionSheet *groupLevelAdminActionSheet;
@property (strong, nonatomic) NSDictionary *currentCellDataParentObject;
@property (strong, nonatomic) UILabel *fullViewLabel;
@end

#define DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT 0
#define DEFAULT_LEFT_RIGHT_PADDING_INSET 0
#define CELL_TYPE_KEY @"CELL_TYPE_KEY"
#define kSWMyReportsCollectionViewCellForIllnessReportInstance @"1"
#define kSWMyReportsCollectionViewCellForType2 @"2"

@implementation SWMyReportsViewController

#pragma mark - Phone Rotated Code

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.flowLayout invalidateLayout];
    [self.header setNeedsLayout];
}

#pragma mark - SWMyReportsCollectionViewCellForIllnessReportInstance

- (void)myReportsCollectionViewCellForIllnessReportInstanceDeleteButtonTapped:(SWMyReportsCollectionViewCellForIllnessReportInstance *)sender
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        NSString *cellDataType = [cellData objectForKey:CELL_TYPE_KEY];
        if ([cellDataType isEqualToString:kSWMyReportsCollectionViewCellForIllnessReportInstance])
        {
            NSDictionary *cellDataParentObject = [cellData objectForKey:@"cell_data_parent_object"];
            if ([cellDataParentObject isKindOfClass:[NSDictionary class]])
            {
                self.currentCellDataParentObject = cellDataParentObject;
                self.groupLevelAdminActionSheet = [[UIActionSheet alloc] initWithTitle:[cellDataParentObject objectForKey:@"display_word"] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Report" otherButtonTitles:nil];
                [self.groupLevelAdminActionSheet showInView:self.view];
                NSString *displayWord = [cellDataParentObject objectForKey:@"display_word"];
                if ([displayWord isKindOfClass:[NSString class]])
                {
                    //[SWHelper helperShowAlertWithTitle:displayWord];
                }
                else
                {
                    //NSLog(@"cellDataParentObject = %@", cellDataParentObject);
                    //[SWHelper helperShowAlertWithTitle:cellDataParentObject];
                }
            }
        }
    }
}

- (void)myReportsCollectionViewCellForIllnessReportInstancePrivatePublicIconButtonTapped:(SWMyReportsCollectionViewCellForIllnessReportInstance *)sender
{
    //NSLog(@"LOCK GLOBE ICON TAPPED");
    // DO NOTHING FOR NOW
}

#pragma mark - SWMyReportsCollectionViewCellForType2Delegate

- (void)myReportsCollectionViewCellForType2TargetAction1:(SWMyReportsCollectionViewCellForType2 *)sender
{
    // DO NOTHING, THIS IS EXAMPLE CODE, THE BUTTON IS NOT EVEN VISIBLE
    return;
    
    
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        NSString *cellDataType = [cellData objectForKey:CELL_TYPE_KEY];
        if ([cellDataType isEqualToString:kSWMyReportsCollectionViewCellForType2])
        {
            NSDictionary *cellDataParentObject = [cellData objectForKey:@"cell_data_parent_object"];
            if ([cellDataParentObject isKindOfClass:[NSDictionary class]])
            {
                NSString *displayWord = [cellDataParentObject objectForKey:@"display_word"];
                if ([displayWord isKindOfClass:[NSString class]])
                {
                    [SWHelper helperShowAlertWithTitle:displayWord];
                }
                else
                {
                    //NSLog(@"cellDataParentObject = %@", cellDataParentObject);
                    //[SWHelper helperShowAlertWithTitle:cellDataParentObject];
                }
            }
        }
    }
}

#pragma mark - Custom Getters/Setters

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.delegate myReportsViewControllerWantsToDismiss];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    SWEditMyProfileViewController *vc = [[SWEditMyProfileViewController alloc] init];
    vc.delegate = self;
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:^{}];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([actionSheet isEqual:self.groupLevelAdminActionSheet])
    {
        if (buttonIndex == 0)
        {
            // Delete
            //NSLog(@"Delete... %@", self.currentCellDataParentObject);
            if ([self.currentCellDataParentObject objectForKey:@"id"])
            {
                // Log to Flurry
                [Flurry logEvent:@"Report deleted from Report History"];
                
                [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                [[SWHelper helperAppBackend] appBackendDeleteReportUsingArgs:@{@"feed_id":[self.currentCellDataParentObject objectForKey:@"id"]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                        if (!jsonResponseBody)
                        {
                            [SWHelper helperDismissFullScreenActivityIndicator];
                            [SWHelper helperShowAlertWithTitle:@"Error Deleting Report" message:[NSString stringWithFormat:@"%@...", [responseBodyAsString substringToIndex:80]]];
                        }
                        else
                        {
                            // Delete from local database
                            [SWReportCDM reportDeleteRecordWithId:@([[self.currentCellDataParentObject objectForKey:@"id"] integerValue])];
                            
                            // Get fresh data off the network
                            [[SWHelper helperAppBackend] appBackendGetSelfReportsUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                                    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                                    [SWHelper helperSaveJSONString:responseBodyAsString toDocsUsingName:@"getUserReports"];
                                    self.serverResponse = jsonResponseBody;
                                    [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse];
                                    [self.collectionView reloadData];
                                    [SWHelper helperDismissFullScreenActivityIndicator];
                                     NSString *message = [jsonResponseBody objectForKey:@"message"];
                                     if ([message isKindOfClass:[NSString class]] && message.length > 0)
                                     {
                                         if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                                         {
                                             if (![[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"])
                                             {
                                                 [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                                             }
                                         }
                                     }
                                });
                            }];
                        }
                    });
                }];
            }
        }
        else
        {
            // Cancel
            //NSLog(@"Cancel...");
        }
    }
}

#pragma mark - Helpers

- (UIEdgeInsets)helperGetDefaultCollectionViewContentInset
{
    // top, left, bottom, right
    return UIEdgeInsetsMake(0, DEFAULT_LEFT_RIGHT_PADDING_INSET, DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT, DEFAULT_LEFT_RIGHT_PADDING_INSET);
}

- (UIEdgeInsets)helperGetDefaultCollectionViewScrollIndicatorInsets
{
    return UIEdgeInsetsMake(0, 0, DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT, 0);
}

- (CGFloat)helperCollectionViewWidth
{
    return self.view.bounds.size.width-(DEFAULT_LEFT_RIGHT_PADDING_INSET*2);
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - UICollectionViewDelegate

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cvDataObjects.count;
}

#pragma mark - SWEditMyProfileViewControllerDelegate

- (void)editMyProfileViewControllerWantsToDismiss:(SWEditMyProfileViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)editMyProfileViewControllerWantsToDismissWithUpdateProfilePic:(UIImage *)image sender:(SWEditMyProfileViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Life Cycle Methods

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        NSString *cellDataType = [cellData objectForKey:CELL_TYPE_KEY];
        if ([cellDataType isEqualToString:kSWMyReportsCollectionViewCellForIllnessReportInstance])
        {
            NSDictionary *serverDict = [cellData objectForKey:@"cell_data_parent_object"];
            SWMyReportsCollectionViewCellForIllnessReportInstance *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWMyReportsCollectionViewCellForIllnessReportInstance" forIndexPath:indexPath];
            myCell.delegate = self;
            myCell.serverDict = serverDict;
            //NSLog(@"serverDict = %@", serverDict);
            //myCell.backgroundColor = [SWColor color:[UIColor orangeColor] usingOpacity:0.5];
            cell = myCell;
        }
        else if ([cellDataType isEqualToString:kSWMyReportsCollectionViewCellForType2])
        {
            //NSDictionary *serverDict = [cellData objectForKey:@"cell_data_parent_object"];
            NSAttributedString *headerString = [cellData objectForKey:@"cell_data_root_object"];
            SWMyReportsCollectionViewCellForType2 *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWMyReportsCollectionViewCellForType2" forIndexPath:indexPath];
            [myCell myReportsCollectionViewCellForType2SetLabelAttributedString:headerString];
            myCell.delegate = self;
            //NSLog(@"serverDict = %@", serverDict);
            //myCell.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
            cell = myCell;
        }
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        NSString *cellDataType = [cellData objectForKey:CELL_TYPE_KEY];
        if ([cellDataType isEqualToString:kSWMyReportsCollectionViewCellForIllnessReportInstance])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 50);
            /*
            NSAttributedString *cellDataRootObject = [cellData objectForKey:@"cell_data_root_object"];
            if ([cellDataRootObject isKindOfClass:[NSAttributedString class]])
            {
                size = CGSizeMake(cellDataRootObject.size.width + 20, [cellDataRootObject heightForWidth:[self helperCollectionViewWidth]] + 20);
            }
             */
        }
        else if ([cellDataType isEqualToString:kSWMyReportsCollectionViewCellForType2])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 50);
            /*
             NSAttributedString *cellDataRootObject = [cellData objectForKey:@"cell_data_root_object"];
             if ([cellDataRootObject isKindOfClass:[NSAttributedString class]])
             {
             size = CGSizeMake(cellDataRootObject.size.width + 20, [cellDataRootObject heightForWidth:[self helperCollectionViewWidth]] + 20);
             }
             */
        }
    }
    return size;
}

- (void)helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse
{
    if ([self.serverResponse isKindOfClass:[NSDictionary class]])
    {
        NSArray *reports = [self.serverResponse objectForKey:@"reports"];
        if (!reports)
        {
            reports = @[];
        }
        if ([reports isKindOfClass:[NSArray class]])
        {
            self.cvDataObjects = @[];
            NSString *lastReportDay = @"";
            for (NSDictionary *reportDict in reports)
            {
                //NSInteger index = [reports indexOfObject:reportDict];
                //NSLog(@"reportDict = %@", reportDict);
                /*
                 "display_word" = Allergies;
                 "feed_text" = "allergies | allergies";
                 id = 86212;
                 "illness_id" = 18;
                 lat = "33.66292190552";
                 lon = "-118.00818634033";
                 "posted_time" = "2016-02-16 19:04:47";
                 "user_id" = 138128;
                 */
                NSString *postedTime = [reportDict objectForKey:@"posted_time"];
                NSString *subtitle = @"";
                NSString *localTimeStringDownToDayForCompareAddHeader = @"";
                NSString *localTimeForUI = @"";
                if (postedTime)
                {
                    // Create date formattter
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    
                    // Define server's date format
                    NSString *serverDateFormat = @"yyyy-MM-dd H:mm:ss"; // e.g. 2016-02-16 19:04:47";
                    
                    // Convert to NSDate object
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"EST"]];
                    [dateFormatter setDateFormat:serverDateFormat];
                    NSDate *estDate = [dateFormatter dateFromString:postedTime]; // EST date
                    
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd H:mm:ss"];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                    localTimeStringDownToDayForCompareAddHeader = [dateFormatter stringFromDate:estDate];
                    //NSLog(@"localTimeStringDownToDayForCompareAddHeader = %@", localTimeStringDownToDayForCompareAddHeader);
                    //[dateFormatter setDateFormat:@"MMMM d YYYY"];
                    [dateFormatter setDateFormat:@"EEEE, MMMM d, YYYY"];
                    localTimeForUI = [dateFormatter stringFromDate:estDate];
                    //NSLog(@"localTimeForUI = %@", localTimeForUI);
                    //NSLog(@"estDate = %f", [estDate timeIntervalSince1970]);
                    //NSLog(@"NSDate new = %f", [[NSDate new] timeIntervalSince1970]);
//                    NSDate *dateSystemTime = [dateFormatter dateFromString:formattedDate];
//                    //NSLog(@"dateSystemTime = %@", dateSystemTime);
                    subtitle = localTimeStringDownToDayForCompareAddHeader;
                    subtitle = [subtitle stringByAppendingString:[reportDict objectForKey:@"display_word"]];
                }
                
                if (lastReportDay.length == 0 || ![localTimeStringDownToDayForCompareAddHeader isEqualToString:lastReportDay])
                {
                    NSDictionary *cvDataObject = @{CELL_TYPE_KEY:kSWMyReportsCollectionViewCellForType2,
                                                   @"cell_data_root_object":[[NSAttributedString alloc] initWithString:localTimeForUI attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12]}],
                                                   @"cell_data_parent_object":reportDict};
                    self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:cvDataObject];
                }
                
                NSDictionary *cvDataObject = @{CELL_TYPE_KEY:kSWMyReportsCollectionViewCellForIllnessReportInstance,
                                               @"cell_data_root_object":[[NSAttributedString alloc] initWithString:subtitle attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:22],NSForegroundColorAttributeName:[UIColor orangeColor]}],
                                               @"cell_data_parent_object":reportDict};
                self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:cvDataObject];
                
                /*;
                NSString *fontAwesomeName = [reportDict objectForKey:@"font-awesome-name"];
                if ([fontAwesomeName isKindOfClass:[NSString class]])
                {
                    NSDictionary *cvDataObject = @{CELL_TYPE_KEY:kSWMyReportsCollectionViewCellForIllnessReportInstance,
                                                   @"cell_data_root_object":[[NSAttributedString alloc] initWithString:@"MY STRING" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:22],NSForegroundColorAttributeName:[UIColor orangeColor]}],
                                                   @"cell_data_parent_object":reportDict};
                    self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:cvDataObject];
                }
                */
                lastReportDay = localTimeStringDownToDayForCompareAddHeader; // Update for next round
            }
            if (reports.count == 0)
            {
                //self.cvDataObjects = [self.cvDataObjects arrayByAddingObject:@{CELL_TYPE_KEY:kSWMyReportsCollectionViewCellForIllnessReportInstance,@"cell_data_root_object":letterDict}];
            }
        }
        if (reports.count == 0)
        {
            if (self.fullViewLabel)
            {
                [SWHelper helperDismissFullViewLabel:self.fullViewLabel];
            }
            self.fullViewLabel = [[UILabel alloc] init];
            self.fullViewLabel.attributedText = [[NSAttributedString alloc] initWithString:@"You haven't made any reports yet." attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:18]}];
            [SWHelper helperShowFullViewLabel:self.fullViewLabel usingView:self.view];
        }
        else
        {
            [SWHelper helperDismissFullViewLabel:self.fullViewLabel];
            self.fullViewLabel = nil;
        }
    }
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT DEFAULTS
    self.flowLayout = [[SWMyReportsCollectionViewFlowLayout alloc] init];
    self.flowLayout.minimumInteritemSpacing = 0;
    self.flowLayout.minimumLineSpacing = 0;
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.collectionView = [[SWMyReportsCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.collectionView forKey:@"collectionView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.collectionView];
    
    // LAYOUT
    
    // Layout COLLECTION VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config SELF
    self.title = @"Report History";
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherDarkGrayText104x104x104];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Config COLLECTION VIEW
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.allowsSelection = NO;
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    self.collectionView.scrollIndicatorInsets = [self helperGetDefaultCollectionViewScrollIndicatorInsets];
    self.collectionView.scrollsToTop = YES;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[SWMyReportsCollectionViewCellForIllnessReportInstance class] forCellWithReuseIdentifier:@"SWMyReportsCollectionViewCellForIllnessReportInstance"];
    [self.collectionView registerClass:[SWMyReportsCollectionViewCellForType2 class] forCellWithReuseIdentifier:@"SWMyReportsCollectionViewCellForType2"];
    
    // GET DATA FROM SERVER, UPDATE UI (0.0 DELAY GETS SCREEN TO LOAD EVEN FASTER, MORE RESPONSIVE UX)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.serverResponse = (NSDictionary *)[SWHelper helperLoadJSONObjectFromBundleDocsUsingName:@"getUserReports" forceFeedFromBundle:NO];
        [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse];
        [self.collectionView reloadData];
        //[SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
        [[SWHelper helperAppBackend] appBackendGetSelfReportsUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                [SWHelper helperSaveJSONString:responseBodyAsString toDocsUsingName:@"getUserReports"];
                self.serverResponse = jsonResponseBody;
                [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse];
                [self.collectionView reloadData];
                //[SWHelper helperDismissFullScreenActivityIndicator];
            });
        }];
    });
    
    // COLOR FOR DEVELOPMENT PURPOSES
    //self.collectionView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
}

@end
