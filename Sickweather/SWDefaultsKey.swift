//
//  SWDefaultsKey.swift
//  Sickweather
//
//  Created by John Erck on 5/18/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import Foundation

@objc class SWDefaultsKey: NSObject {
    
    @objc class func keyForUserLastKnownCurrentLocationPlacemarkLatitude() -> String {
        return "keyForUserLastKnownCurrentLocationPlacemarkLatitude"
    }
    
   @objc  class func keyForUserLastKnownCurrentLocationPlacemarkLongitude() -> String {
        return "keyForUserLastKnownCurrentLocationPlacemarkLongitude"
    }
    
    @objc class func keyForTimeOfLastPresentedLocalNotificationForIllnessTypeId(_ illnessTypeId: String) -> String {
        return "keyForTimeOfLastPresentedLocalNotificationForIllnessTypeId\(illnessTypeId)"
    }
}
