//
//  SWUserCDM+CoreDataProperties.h
//  Sickweather
//
//  Created by John Erck on 4/13/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SWUserCDM.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWUserCDM (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *birthDate;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *country;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *emailHash;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *gender;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *medPref;
@property (nullable, nonatomic, retain) NSString *passwordHash;
@property (nullable, nonatomic, retain) NSString *photoOriginal;
@property (nullable, nonatomic, retain) NSString *photoThumb;
@property (nullable, nonatomic, retain) NSString *raceEthnicity;
@property (nullable, nonatomic, retain) NSString *state;
@property (nullable, nonatomic, retain) NSString *status;
@property (nullable, nonatomic, retain) NSString *unique;
@property (nullable, nonatomic, retain) NSString *userCurrentUserKey;
@property (nullable, nonatomic, retain) NSNumber *userLastKnownCurrentLocationLatitude;
@property (nullable, nonatomic, retain) NSNumber *userLastKnownCurrentLocationLongitude;
@property (nullable, nonatomic, retain) NSData *userLastKnownCurrentLocationPlacemark;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSData *profilePicThumbnailImageData;
@property (nullable, nonatomic, retain) NSSet<SWReportCDM *> *reports;

@end

@interface SWUserCDM (CoreDataGeneratedAccessors)

- (void)addReportsObject:(SWReportCDM *)value;
- (void)removeReportsObject:(SWReportCDM *)value;
- (void)addReports:(NSSet<SWReportCDM *> *)values;
- (void)removeReports:(NSSet<SWReportCDM *> *)values;

@end

NS_ASSUME_NONNULL_END
