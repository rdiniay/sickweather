//
//  SWBannerView.h
//  Sickweather
//
//  Created by John Erck on 10/24/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWBannerView;

@protocol SWBannerViewDelegate <NSObject>
- (void)bannerViewWantsToDismiss:(SWBannerView *)sender;
@end

@interface SWBannerView : UIView
@property (weak, nonatomic) id <SWBannerViewDelegate> delegate;
- (void)updateMessage:(NSString *)message;
@end
