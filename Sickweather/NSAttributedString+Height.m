//
//  NSAttributedString+Height.m
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "NSAttributedString+Height.h"

@implementation NSAttributedString (Height)

- (CGFloat)heightForWidth:(CGFloat)width
{
    return ceilf(CGRectGetHeight([self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                    context:nil])) + 1;
}

@end
