//
//  SWAppDelegate.m
//  Sickweather
//
//  Created by John Erck on 9/20/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//
@import SWAnalytics;
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Bolts/Bolts.h>
#import <OneSignal/OneSignal.h>
#import <WatchConnectivity/WatchConnectivity.h>
#import "SWAppDelegate.h"
#import "SWSettings.h"
#import "SWSlidingViewController.h"
#import "SWNavigationController.h"
#import "SWRegionMonitoringLogModel.h"
#import "SWIllnessModel.h"
#import "SWUserModel.h"
#import "SWSignInWithFacebookViewController.h"
#import "SWDemoMyReportsCDTVC.h"
#import "SWUserCDM+SWAdditions.h"
#import "SWIllnessCDM+SWAdditions.h"
#import "Flurry.h"
#import "SWSavedLocationCDM+SWAdditions.h"
#import "SWAlertLogCDM+SWAdditions.h"
#import "Sickweather-Swift.h"
#import "SWReportCDM+SWAdditions.h"
#import "SWHomeViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#include <GJPG/GJPG.h>
@import AdSupport;


#define SHOW_ALERT_LOGIC_LOG NO

typedef void(^HandleWatchKitExtensionRequestReplyBlock)(NSDictionary *);

@interface SWAppDelegate () <CLLocationManagerDelegate, WCSessionDelegate>
@property (strong, nonatomic) NSString *flurryUniqueApplicationKey;
@property (assign) BOOL uaInProduction;
@property (strong, nonatomic) NSString *uaDevelopmentAppKey;
@property (strong, nonatomic) NSString *uaDevelopmentAppSecret;
@property (strong, nonatomic) NSString *uaProductionAppKey;
@property (strong, nonatomic) NSString *uaProductionAppSecret;
@property (strong, nonatomic) NSData *saveOnLoadUserLastKnownCurrentLocationPlacemarkData;
@property (strong, nonatomic) NSURL *customDeepLinkURL;
@property (assign) BOOL logLocationToFlurry;
@property (assign) BOOL didFinishLaunchingWitOptions;
@property (assign) BOOL hasSentSWCoreDataManagedDocumentDidLoadNotificationKey;
@property (assign) BOOL syncRegionsCallIsProcessing;
@property (nonatomic, copy) HandleWatchKitExtensionRequestReplyBlock handleWatchKitExtensionRequestReplyBlock;
@property (strong, nonatomic) NSDictionary *handleWatchKitExtensionRequestUserInfo;
@property (assign) NSUInteger handleWatchKitExtensionRequestBackgroundId;
@property (strong, nonatomic) NSMutableArray *startMonitoringForRegionsQueue;
@property (strong, nonatomic) NSMutableArray *startMonitoringForRegionsQueueReports;
@property (strong, nonatomic) WCSession *watchConnectivitySession;
@property (assign) BOOL showConsentDialog;
@end

@implementation SWAppDelegate


#pragma mark - Custom Getters/Setters

- (NSMutableArray *)startMonitoringForRegionsQueue
{
    if (!_startMonitoringForRegionsQueue) {
        _startMonitoringForRegionsQueue = [NSMutableArray new];
    }
    return _startMonitoringForRegionsQueue;
}

- (NSMutableArray *)startMonitoringForRegionsQueueReports
{
    if (!_startMonitoringForRegionsQueueReports) {
        _startMonitoringForRegionsQueueReports = [NSMutableArray new];
    }
    return _startMonitoringForRegionsQueueReports;
}

#pragma mark - Public

- (void)helperInterpretCustomDeepLinkURL:(BOOL)fromBackground
{
    if (self.customDeepLinkURL)
    {
        NSURL *url = [self.customDeepLinkURL copy]; // Get strong local copy
        self.customDeepLinkURL = nil; // Interpret one time
        
        // Log to flurry
        [Flurry logEvent:@"Custom URL Opened" withParameters:@{@"url": url.absoluteString}];
        
        // Trim protocol
        NSString *protocolString = @"sickweather://"; // PROTOCOL SETTING HERE
        NSString *protocol = [url.absoluteString substringWithRange:NSMakeRange(0, protocolString.length)];
        //NSLog(@"protocol = %@", protocol);
        if ([protocol isEqualToString:protocolString])
        {
            // Carry on then...
            NSString *everythingPastProtocol = [url.absoluteString substringWithRange:NSMakeRange(protocolString.length, url.absoluteString.length - protocolString.length)];
            //NSLog(@"everythingPastProtocol = %@", everythingPastProtocol);
            NSString *everythingPastProtocolMapsPrefix = @"maps/"; // PATH PREFIX SETTING HERE
            if (everythingPastProtocol.length >= everythingPastProtocolMapsPrefix.length && [[everythingPastProtocol substringWithRange:NSMakeRange(0, everythingPastProtocolMapsPrefix.length)] isEqualToString:everythingPastProtocolMapsPrefix])
            {
               // NSString *illnessIdToLoadMapFor = [everythingPastProtocol substringWithRange:NSMakeRange(everythingPastProtocolMapsPrefix.length, everythingPastProtocol.length - everythingPastProtocolMapsPrefix.length)];
                //NSLog(@"illnessIdToLoadMapFor = %@", illnessIdToLoadMapFor);
                
                // Now preset the map to load the illness type that matches the alert
               // SWSlidingViewController *swSlidingVC = [SWHelper helperGetTab1SlidingViewController];
               // UINavigationController *navigationController = (UINavigationController *)swSlidingVC.topViewController;
//                if ([navigationController.topViewController isKindOfClass:[SWMapViewController class]])
//                {
//                    // Get map page
//                   // SWMapViewController *mapVC = (SWMapViewController *)navigationController.topViewController;
//
//                    // Proceed
//                    SWIllnessCDM *illness = [SWIllnessCDM illnessGetRecordUsingId:illnessIdToLoadMapFor];
//                    if (illness)
//                    {
//                        // Select map
//                       // mapVC.selectedSicknessTypeId = illnessIdToLoadMapFor;
//                        //mapVC.selectedSicknessTypeName = [illness nameWithWordsCapitalized];
//                    }
//                    else
//                    {
//                        // THIS CODE EXPECTS TO BE CALLED AFTER CORE DATA IS AVAILABLE TO BE USED...
//                        // THIS BLOCK HERE TO JUST REMIND YOU OF THAT INTENTIONAL DESIGN.
//                    }
//                }
            }
            everythingPastProtocolMapsPrefix = @"menu/"; // PATH PREFIX SETTING HERE
            if (everythingPastProtocol.length >= everythingPastProtocolMapsPrefix.length && [[everythingPastProtocol substringWithRange:NSMakeRange(0, everythingPastProtocolMapsPrefix.length)] isEqualToString:everythingPastProtocolMapsPrefix])
            {
                //[SWHelper helperShowAlertWithTitle:@"Menu Deep Link"];
                
                // GET MAP VC
                SWSlidingViewController *swSlidingVC = [SWHelper helperGetTab1SlidingViewController];
                if ([swSlidingVC isKindOfClass:[SWSlidingViewController class]])
                {
                    //UINavigationController *navigationController = (UINavigationController *)swSlidingVC.topViewController;
//                    if ([navigationController isKindOfClass:[UINavigationController class]])
//                    {
//                        if ([navigationController.topViewController isKindOfClass:[SWMapViewController class]])
//                        {
//                            SWMapViewController *mapVC = (SWMapViewController *)navigationController.topViewController;
//                            [mapVC showMenu];
//                        }
//                    }
                }
            }
        }
        else
        {
            // LIST GROUPS
            NSString *endsWithMeansShowGroupListWithoutEndingSlash = @"sickweather.com/groups";
            NSString *endsWithMeansShowGroupList = [NSString stringWithFormat:@"%@/", endsWithMeansShowGroupListWithoutEndingSlash];
            
            // SHOW SPECIFIC GROUP PROFILE
            NSString *containsMeansShowGroupProfilePage = @"sickweather.com/groups/profile/";
            
            // CHECK FOR LIST
            if ([url.absoluteString hasSuffix:endsWithMeansShowGroupList] || [url.absoluteString hasSuffix:endsWithMeansShowGroupListWithoutEndingSlash])
            {
                // GET MAP VC
//                SWSlidingViewController *swSlidingVC = [SWHelper helperGetTab1SlidingViewController];
//                if ([swSlidingVC isKindOfClass:[SWSlidingViewController class]])
//                {
//                    UINavigationController *navigationController = (UINavigationController *)swSlidingVC.topViewController;
//                    if ([navigationController isKindOfClass:[UINavigationController class]])
//                    {
//                        if ([navigationController.topViewController isKindOfClass:[SWMapViewController class]])
//                        {
//                            SWMapViewController *mapVC = (SWMapViewController *)navigationController.topViewController;
//                            [mapVC showMenu];
//                        }
//                    }
//                }
            }
            else if ([url.absoluteString containsString:containsMeansShowGroupProfilePage])
            {
                NSRange matchRange = [url.absoluteString rangeOfString:containsMeansShowGroupProfilePage];
                if (matchRange.location == NSNotFound)
                {
                    // No match, this is unexpected here...
                }
                else
                {
                    // We DID find a match as expected here...
                    NSString *foursquareId = [url.absoluteString substringFromIndex:matchRange.location + matchRange.length];
                    NSLog(@"foursquareId = %@", foursquareId);
                    NSRange myRange = [foursquareId rangeOfString:@"/"];
                    NSLog(@"myRange = %@", NSStringFromRange(myRange));
                    if (myRange.location != NSNotFound)
                    {
                        foursquareId = [foursquareId substringToIndex:myRange.location];
                        //NSLog(@"foursquareId = %@", foursquareId);
                    }
                if (fromBackground){
                        // GET MAP VC
                    SWSlidingViewController *swSlidingVC = [SWHelper helperGetTab1SlidingViewController];
                    if ([swSlidingVC isKindOfClass:[SWSlidingViewController class]])
                        {
                        UINavigationController *navigationController = (UINavigationController *)swSlidingVC.topViewController;
                        if ([navigationController isKindOfClass:[UINavigationController class]])
                            {
                            if ([navigationController.topViewController isKindOfClass:[SWHomeViewController class]]){
                                SWHomeViewController *mapVC = (SWHomeViewController *)navigationController.topViewController;
                                [mapVC showGroupUsingSWFoursquareId:foursquareId];
                            }
                        }
                    }
                }else {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:foursquareId forKey:@"foursquareId"];
                }
            }
         }
      }
   }
}

- (UIView *)launchScreenView
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"Launch Screen" owner:self options:nil];
    UIView *myView = [objects objectAtIndex:0];
    myView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    return myView;
}

-(void)getSymptomsFromBackend{
    [[SWHelper helperAppBackend] appBackendGetSymptomsUsingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (jsonResponseBody) {
                [[NSUserDefaults standardUserDefaults] setObject:jsonResponseBody forKey:[SWConstants symptomsDefaultKey]];
            }
        });
    }];
}

-(void)getTrackerTypes{
    [[SWHelper helperAppBackend] appBackendGetTrackerTypersUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([jsonResponseBody isKindOfClass:[NSDictionary class]] && [[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) {
                [[NSUserDefaults standardUserDefaults] setObject:jsonResponseBody forKey:[SWConstants trackerTypesDefaultKey]];
            }
        });
    }];
}

-(void)getFamilyMemberIdFromBackend{
    if ([SWUserCDM currentlyLoggedInUser]) {
        [[SWHelper helperAppBackend]appBackendGetFamilyIdUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            if (jsonResponseBody){
                if ([jsonResponseBody valueForKey:@"family_id"] != nil && ![[jsonResponseBody valueForKey:@"family_id"] isEqual:[NSNull null]] && ![[jsonResponseBody valueForKey:@"family_id"] isEqualToString:@""]){
                    [SWHelper setFamilyId:[jsonResponseBody valueForKey:@"family_id"]];
                }
            }
        }];
    }
}

- (void)getIllnessesSynchronouslyFollowedByAsynchronousNetworkUpdate:(SWAppDelegateArrayResponse)update
{
    // Load code blocked off so we can also call this from the web request's completion handler
    NSArray *(^updateCoreDataIllnessesInAdditiveOnlyFashionBasedOnSourceFileUpdatedToMirrorGetIllnessesPHPEndpointBlock)(NSArray *illnesses) = ^NSArray *(NSArray *illnesses) {
        // Object key mapping for "self report text" on a per id basis
        NSDictionary *grahamMap = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"allergies", @"18",
                                   @"asthma", @"30",
                                   @"bronchitis", @"2",
                                   @"chicken pox", @"10",
                                   @"sick", @"4",
                                   @"cough", @"6",
                                   @"croup", @"14",
                                   @"ear infection", @"23",
                                   @"fever", @"11",
                                   @"fifth disease", @"31",
                                   @"flu", @"1",
                                   @"hand foot and mouth", @"16",
                                   @"man flu", @"25",
                                   @"runny nose", @"5",
                                   @"norovirus", @"32",
                                   @"pink eye", @"22",
                                   @"pneumonia", @"15",
                                   @"rsv", @"33",
                                   @"sinus infection", @"20",
                                   @"sore throat", @"21",
                                   @"stomach virus", @"24",
                                   @"strep", @"7",
                                   @"whooping cough", @"3",
                                   nil];
        // Parse data as JSON
        if (illnesses != nil && [illnesses isKindOfClass:[NSArray class]]){
            
            // Iterate through each illness in the local file and update its corresponding core data record
            for (NSDictionary *illnessDict in illnesses)
            {
                // Check for self report text (weird to do it like this... but we do...)
                @try{
                    if ([illnessDict isKindOfClass:[NSDictionary class]] && [illnessDict objectForKey:@"id"] != nil &&  ![[illnessDict objectForKey:@"id"] isEqual:[NSNull null]] && ![[illnessDict objectForKey:@"id"] isEqualToString:@""] && [[illnessDict objectForKey:@"id"] isKindOfClass:[NSString class]])
                    {
                        NSString *selfReportText;
                        if ([illnessDict objectForKey:@"id"] != nil && ![[illnessDict objectForKey:@"id"] isEqual:[NSNull null]]) {
                            selfReportText = [grahamMap objectForKey:[illnessDict objectForKey:@"id"]];
                        }
                        if (!selfReportText) selfReportText = @"";
                        // Determine if this is a grouped illness or not and codify this as yes/no
                        NSString *isGroupIllness = @"yes";
                        if ([illnessDict objectForKey:@"description"] == nil ||  [[illnessDict objectForKey:@"description"] isEqual:[NSNull null]] || [[illnessDict objectForKey:@"description"] isEqualToString:@""]) isGroupIllness = @"no";
                    
                        // Define custom sortOrder
                        double sortOrder = 1;
                        if ([illnessDict objectForKey:@"name"] != nil && ![[illnessDict objectForKey:@"name"] isEqual:[NSNull null]] && ![[illnessDict objectForKey:@"name"] isEqualToString:@""]  && [[illnessDict objectForKey:@"name"] isEqualToString:@"Respiratory"]){
                            sortOrder = 0.1;
                        }
                        if ([illnessDict objectForKey:@"name"] != nil && ![[illnessDict objectForKey:@"name"] isEqual:[NSNull null]] && ![[illnessDict objectForKey:@"name"] isEqualToString:@""] && [[illnessDict objectForKey:@"name"] isEqualToString:@"Gastrointestinal"]){
                            sortOrder = 0.2;
                        }
                        if ([illnessDict objectForKey:@"name"] != nil && ![[illnessDict objectForKey:@"name"] isEqual:[NSNull null]] && ![[illnessDict objectForKey:@"name"] isEqualToString:@""]  && [[illnessDict objectForKey:@"name"] isEqualToString:@"Environmental"]){
                            sortOrder = 0.3;
                        }
                        if ([illnessDict objectForKey:@"name"] != nil && ![[illnessDict objectForKey:@"name"] isEqual:[NSNull null]] && ![[illnessDict objectForKey:@"name"] isEqualToString:@""]  && [[illnessDict objectForKey:@"name"] isEqualToString:@"Childhood"]){
                            sortOrder = 0.4;
                        }
                    
                        // Update core data being sure NOT TO EVER DELETE A CORE DATA ILLNESS RECORD TIED TO A USER'S REPORT BECAUSE THAT WOULD OF COURSE CORUPT THE DATAMODEL!!!!
                        [SWIllnessCDM illnessGetUpdatedRecordUsingUnique:[illnessDict objectForKey:@"id"] isGroupIllness:isGroupIllness name:[illnessDict objectForKey:@"name"] illnessDescription:[illnessDict objectForKey:@"description"] selfReportText:selfReportText sortOrder:[NSNumber numberWithDouble:sortOrder]];
                    }
              }
                @catch (NSException * e) {
                    [SWHelper addNonFatalErrorWithDictionary:illnessDict];
                }
            }
            
            // Iterate through each illness in core data and remove it if the id does not show up in local file (network response)
        for (SWIllnessCDM *illnessCDM in [SWIllnessCDM illnessGetAll]){
            if (illnessCDM.unique != nil && ![illnessCDM.unique isEqual:[NSNull null]] && ![illnessCDM.unique isEqualToString:@""]){
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", illnessCDM.unique];
                if (predicate != nil) {
                    if (illnesses.count > 0){
                        NSArray *matches = [illnesses filteredArrayUsingPredicate:predicate];
                        if (matches){
                            if (matches.count == 0){
                                // No matches, time for this type to go bye bye
                                [SWIllnessCDM illnessDeleteRecordUsingUnique:illnessCDM.unique];
                            }
                        }
                    }
                }
            }
        }
    }
        
    // Internally add back in our one special record
    [SWIllnessCDM illnessGetUpdatedRecordUsingUnique:@"myReports" isGroupIllness:@"yes" name:@"My Reports" illnessDescription:@"Map of your reports" selfReportText:@"" sortOrder:@(0)];
        // Return all illnesses
        return [SWIllnessCDM illnessGetAll];
    };
    
    // Go to network and if we get a successful response then let's replace illnesses-data.json with the new data for our next offline session...
    [[SWHelper helperAppBackend] appBackendGetIllnessesUsingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (jsonResponseBody && [jsonResponseBody isKindOfClass:[NSArray class]]){
                // CALL CORE DATA METHODS
                NSArray *networkIllnessesNowLocal = updateCoreDataIllnessesInAdditiveOnlyFashionBasedOnSourceFileUpdatedToMirrorGetIllnessesPHPEndpointBlock(jsonResponseBody);
                update(networkIllnessesNowLocal);
            }
        });
    }];
}

- (NSArray *)getReportsSynchronouslyFollowedByAsynchronousNetworkUpdate:(SWAppDelegateArrayResponse)update
{
    // Load code blocked off so we can also call this from the web request's completion handler
    NSArray *(^updateCoreDataSelfReportsUsingReports)(NSData *reports) = ^NSArray *(NSData *reports) {
        
        if (reports != nil)
        {
            // Parse data as JSON
            NSError *error;
            NSDictionary *serverResponse = [NSJSONSerialization JSONObjectWithData:reports options:kNilOptions error:&error];
            NSArray *reportRecords = [serverResponse objectForKey:@"reports"];
            if (reportRecords)
            {
                // Iterate through each illness in the local file
                for (NSDictionary *reportDict in reportRecords)
                {
                    //NSLog(@"reportDict = %@", reportDict);
                    [SWReportCDM getUpdatedReportForServerReportDict:reportDict];
                }
            }
        }
        
        return [SWReportCDM getAllReports];
    };
    
    // Call block here for instant synchronous default setup...
    NSArray *localReports = updateCoreDataSelfReportsUsingReports([SWHelper helperLoadJSONDataFromBundleDocsUsingName:@"getUserReports" forceFeedFromBundle:NO]);
    
    // Go to network and if we get a successful response then let's replace getUserReports.json with the new data...
    [[SWHelper helperAppBackend] appBackendGetSelfReportsUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
            NSData *responseBodyAsData = [responseBodyAsString dataUsingEncoding:NSUTF8StringEncoding];
            [SWHelper helperSaveJSONData:responseBodyAsData toDocsUsingName:@"getUserReports"];
            NSArray *networkReports = updateCoreDataSelfReportsUsingReports(responseBodyAsData);
            update(networkReports);
        });
    }];
    return localReports;
}

- (void)appDelegateUpdateUserBasedOnNetwork
{
    // Load the user's reports now that we have a user
    [[SWHelper helperAppDelegate] getReportsSynchronouslyFollowedByAsynchronousNetworkUpdate:^(NSArray *array) {}];
    
    // Load the user's profile info
    [[SWHelper helperAppBackend] appBackendGetUserProfileUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
            
            // NEW SYSTEM STARTS HERE (USES NSUserDefaults:currentUser)
            if ([jsonResponseBody isKindOfClass:[NSDictionary class]])
            {
                NSMutableDictionary *user = [NSMutableDictionary new];
                for (NSString *key in jsonResponseBody)
                {
                    id value = [jsonResponseBody objectForKey:key];
                    if ([value isKindOfClass:[NSNull class]])
                    {
                        [user setObject:@"" forKey:key];
                    }
                    else
                    {
                        [user setObject:value forKey:key];
                    }
                }
                [SWHelper helperGetCurrentUserFromDefaults:user];
            }
            
            // OLD SYSTEM FOLLOWS...
            
            // Update user based on network values, being sure to never crash
            SWUserCDM *user = [SWUserCDM currentlyLoggedInUser];
            
            // SERVER FIRST NAME VALUE
            if ([[jsonResponseBody objectForKey:@"name_first"] isKindOfClass:[NSString class]])
            {
                user.firstName = [jsonResponseBody objectForKey:@"name_first"];
            }
            
            // SERVER LAST NAME VALUE
            if ([[jsonResponseBody objectForKey:@"name_last"] isKindOfClass:[NSString class]])
            {
                user.lastName = [jsonResponseBody objectForKey:@"name_last"];
            }
            
            // SERVER EMAIL VALUE
            if ([[jsonResponseBody objectForKey:@"email"] isKindOfClass:[NSString class]])
            {
                user.email = [jsonResponseBody objectForKey:@"email"];
            }
            
            // SERVER USERNAME VALUE
            if ([[jsonResponseBody objectForKey:@"username"] isKindOfClass:[NSString class]])
            {
                user.username = [jsonResponseBody objectForKey:@"username"];
            }
            
            // SERVER GENDER VALUE
            if ([[jsonResponseBody objectForKey:@"gender"] isKindOfClass:[NSString class]])
            {
                user.gender = [jsonResponseBody objectForKey:@"gender"];
            }
            
            // SERVER RACE VALUE
            if ([[jsonResponseBody objectForKey:@"race"] isKindOfClass:[NSString class]])
            {
                user.raceEthnicity = [jsonResponseBody objectForKey:@"race"];
            }
            
            // SERVER BIRTH DATE VALUE
            if ([[jsonResponseBody objectForKey:@"birth_date"] isKindOfClass:[NSString class]])
            {
                user.birthDate = [jsonResponseBody objectForKey:@"birth_date"];
            }
            
            // SERVER CITY VALUE
            if ([[jsonResponseBody objectForKey:@"city"] isKindOfClass:[NSString class]])
            {
                user.city = [jsonResponseBody objectForKey:@"city"];
            }
            
            // SERVER STATE VALUE
            if ([[jsonResponseBody objectForKey:@"state"] isKindOfClass:[NSString class]])
            {
                user.state = [jsonResponseBody objectForKey:@"state"];
            }
            
            // SERVER COUNTRY VALUE
            if ([[jsonResponseBody objectForKey:@"country"] isKindOfClass:[NSString class]])
            {
                user.country = [jsonResponseBody objectForKey:@"country"];
            }
            
            // SERVER MED PREF VALUE
            if ([[jsonResponseBody objectForKey:@"med_pref"] isKindOfClass:[NSString class]])
            {
                user.medPref = [jsonResponseBody objectForKey:@"med_pref"];
            }
            
            // SERVER PHOTO ORIGINAL VALUE
            if ([[jsonResponseBody objectForKey:@"photo_original"] isKindOfClass:[NSString class]])
            {
                user.photoOriginal = [jsonResponseBody objectForKey:@"photo_original"];
            }
            
            // SERVER PHOTO THUMB
            if ([[jsonResponseBody objectForKey:@"photo_thumb"] isKindOfClass:[NSString class]])
            {
                user.photoThumb = [jsonResponseBody objectForKey:@"photo_thumb"];
            }
            if (user.photoThumb)
            {
                NSURLSession *ephemeralSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
                NSURL *url = [NSURL URLWithString:user.photoThumb];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url]; //[request setHTTPMethod:@"GET"];
                NSURLSessionDataTask *dataTask = [ephemeralSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [user setProfilePicThumbnailImageData:data];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification
                                                                            object:self
                                                                          userInfo:@{}];
                        
                    });
                }];
                [dataTask resume];
                [ephemeralSession finishTasksAndInvalidate];
            }
            
            // SERVER STATUS
            if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
            {
                user.status = [jsonResponseBody objectForKey:@"status"];
            }
        });
    }];
}

- (void)appDelegateUpdateUserLaskKnownCurrentLocationPlacemarkUsingBestMethodAvailableFinishingWithDidUpdateCurrentLocationPlacemarkNotfification
{
    // Update current user object
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    if (currentUser)
    {
        // If we have access to location then we want to go direct and get the latest values
        if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [SWHelper helperLocationManager].location)
        {
            currentUser.userLastKnownCurrentLocationLatitude = [NSNumber numberWithDouble:[SWHelper helperLocationManager].location.coordinate.latitude];
            currentUser.userLastKnownCurrentLocationLongitude = [NSNumber numberWithDouble:[SWHelper helperLocationManager].location.coordinate.longitude];
            //NSLog(@"currentUser.userLastKnownCurrentLocationLatitude = %@", currentUser.userLastKnownCurrentLocationLatitude);
            //NSLog(@"currentUser.userLastKnownCurrentLocationLongitude = %@", currentUser.userLastKnownCurrentLocationLongitude);
            [self.geocodeManager reverseGeocodeLocation:[SWHelper helperLocationManager].location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *placemark = [placemarks lastObject];
                if (placemark)
                {
                    [self updateUserCurrentLocationPlacemarkUsingPlacemark:placemark];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                        object:self
                                                                      userInfo:@{@"placemark":placemark}];
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                        object:self
                                                                      userInfo:@{@"error":@"Failed to create placemark object."}];
                }
            }];
        }
        else
        {
            /*
            // We don't currently have access to location so let's see if we can get via ip address
            [[SWHelper helperAppBackend] appBackendGetUserLocationViaIPAddressUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"jsonResponseBody = %@", jsonResponseBody);
             
                     jsonResponseBody = {
                        blacklisted = 1;
                        geoip =     (
                            0,
                            "33.6603",
                            "-117.9992",
                            California,
                            "Huntington Beach",
                            "United States"
                        );
                        ip = "104.34.205.30";
                        time = 1449527858;
                     }
             
                    NSArray *geoip = [jsonResponseBody objectForKey:@"geoip"];
                    //NSLog(@"geoip = %@", geoip);
                    NSString *lat = [geoip objectAtIndex:1];
                    NSString *lon = [geoip objectAtIndex:2];
                    for (NSObject *obj in geoip)
                    {
                        NSInteger index = [geoip indexOfObject:obj];
                        if ([obj isKindOfClass:[NSNumber class]])
                        {
                            NSNumber *number = (NSNumber *)obj;
                            //NSLog(@"index(%ld) => value(%@)", (long)index, number);
                            if (index == 1)
                            {
                                lat = [number stringValue];
                            }
                            if (index == 2)
                            {
                                lon = [number stringValue];
                            }
                        }
                        if ([obj isKindOfClass:[NSString class]])
                        {
                            NSString *string = (NSString *)obj;
                            //NSLog(@"index(%ld) => value(%@)", (long)index, string);
                        }
                    }
                    if ([lat length] > 0 && [lon length] > 0)
                    {
                        currentUser.userLastKnownCurrentLocationLatitude = [NSNumber numberWithDouble:[lat doubleValue]];
                        currentUser.userLastKnownCurrentLocationLongitude = [NSNumber numberWithDouble:[lon doubleValue]];
                        CLLocation *location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
                        [self.geocodeManager reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                            CLPlacemark *placemark = [placemarks lastObject];
                            [self updateUserCurrentLocationPlacemarkUsingPlacemark:placemark];
                            [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                                object:self
                                                                              userInfo:@{@"placemark":placemark}];
                        }];
                    }
                    else
                    {
                        // Lookup via ip address failed, so let's see if we can use the user's last known current location
                        CLPlacemark *placemark = [currentUser userLastKnownCurrentLocationPlacemarkObject];
                        if (placemark)
                        {
                            [self updateUserCurrentLocationPlacemarkUsingPlacemark:placemark];
                            [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                                object:self
                                                                              userInfo:@{@"placemark":placemark}];
                        }
                        else
                        {
                            // Lookup via userLastKnownCurrentLocationPlacemarkObject failed, maybe they have a manual address on file
                            if ([[currentUser formattedAddress] length] > 0)
                            {
                                // Try to parse it into a placemark object
                                [self.geocodeManager geocodeAddressString:[currentUser formattedAddress] completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                                    CLPlacemark *placemark = [placemarks lastObject];
                                    if (placemark)
                                    {
                                        [self updateUserCurrentLocationPlacemarkUsingPlacemark:placemark];
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                                            object:self
                                                                                          userInfo:@{@"placemark":placemark}];
                                    }
                                    else
                                    {
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                                            object:self
                                                                                          userInfo:@{@"error":@"The address they have on file failed to geocode."}];
                                    }
                                }];
                            }
                            else
                            {
                                [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                                    object:self
                                                                                  userInfo:@{@"error":@"Well dang! We just don't know where this person is!"}];
                            }
                        }
                    }
                });
            }];
             */
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                            object:self
                                                          userInfo:@{@"error":@"Failed due to nil user."}];
    }
}

- (NSDictionary *)getLatestCurrentLocationSickScoreSynchronouslyFromDisk
{
    // IF WE DON'T HAVE A LOCAL currentLocationSickScore.json FILE YET THEN CREATE IT USING THE BUNDLE TEMPALTE THAT SHIPS WITH THE APP
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationSickScore.json"].path])
    {
        // THEN WE CREATE IT USING THE FOLLOWING AS OUR OFFLINE STARTING POINT
        NSString *path = [[NSBundle mainBundle] pathForResource:@"currentLocationSickScore" ofType:@"json"];
        NSURL *url = [NSURL fileURLWithPath:path];
        NSString *firstTimeAppOpenedDefaults = [NSString stringWithContentsOfFile:url.path encoding:NSUTF8StringEncoding error:nil];
        
        // CREATE FILE USING THE ABOVE STRING AS THE CONTENTS
        [[NSFileManager defaultManager] createFileAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationSickScore.json"].path contents:[firstTimeAppOpenedDefaults dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    }
    
    // UNCONDITIONALLY READ FROM THE LOCAL FILE SYSTEM FOR THE "LATEST" LOCAL FILE
    NSDictionary *localFileAsDict = nil;
    NSData *content = [NSData dataWithContentsOfURL:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationSickScore.json"]];
    if (content)
    {
        // PARSE FILE CONTENT AS JSON TO DICT
        NSError *jsonParseError;
        localFileAsDict = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingMutableContainers error:&jsonParseError];
    }
    
    // SYNCHRONOUSLY RETURN DICT FROM LOCAL FILE
    return localFileAsDict;
}

- (NSDictionary *)updateLatestCurrentLocationSickScoreFromNetworkSavingToDiskFinishingWithAsynchronousNotificationUsingPlacemark:(CLPlacemark *)placemark
{
    if (placemark.location.coordinate.latitude && placemark.location.coordinate.longitude)
    {
        // GET THE LATEST AND GREATEST COPY ASYNCHRONOUSLY VIA THE SERVER AND WRITE THE RESULT TO THE LOCAL FILE SYSTEM THEN SEND AN APP WIDE NOTIFICATION
        [[SWHelper helperAppBackend] appBackendGetSickScoreInRadiusForLat:placemark.location.coordinate.latitude lon:placemark.location.coordinate.longitude usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([jsonResponseBody isKindOfClass:[NSDictionary class]])
                {
                    // CREATE MUTABLE COPY WE CAN DECORATE
                    NSMutableDictionary *sickScoreServerResponseWithOurAddOns = [jsonResponseBody mutableCopy];
                    
                    // INTEGRATE OUR PLACEMARK OBJECT WITH THE RESPONSE...
                    NSMutableDictionary *myPlacemark = [NSMutableDictionary new];
                    if ([placemark.locality isKindOfClass:[NSString class]])
                    {
                        [myPlacemark setObject:placemark.locality forKey:@"locality"];
                    }
                    if ([placemark.administrativeArea isKindOfClass:[NSString class]])
                    {
                        [myPlacemark setObject:placemark.administrativeArea forKey:@"administrativeArea"];
                        [myPlacemark setObject:placemark.administrativeArea forKey:@"administrative_area"];
                    }
                    if (placemark.location.coordinate.latitude != 0)
                    {
                        [myPlacemark setObject:@(placemark.location.coordinate.latitude) forKey:@"latitude"];
                    }
                    if (placemark.location.coordinate.longitude != 0)
                    {
                        [myPlacemark setObject:@(placemark.location.coordinate.longitude) forKey:@"longitude"];
                    }
                    [sickScoreServerResponseWithOurAddOns setObject:myPlacemark forKey:@"placemark"];
                    
                    // FLAG THIS RESPONSE AS A CURRENT LOCATION ONE
                    [sickScoreServerResponseWithOurAddOns setObject:@"yes" forKey:@"placemark_is_current_location"];
                    
                    // CONVERT COMPOSITE OBJECT TO JSON STRING
                    
                    // GET AS DATA
                    NSError *error;
                    NSData *newDictJSONData = [NSJSONSerialization dataWithJSONObject:sickScoreServerResponseWithOurAddOns options:NSJSONWritingPrettyPrinted error:&error];
                    
                    // CHECK FOR ERROR
                    if (!newDictJSONData)
                    {
                        NSLog(@"Got an error: %@", error);
                    }
                    else
                    {
                        // SUCCESS, WE HAVE IT AS DATA
                        
                        // NOW GET AS STRING
                        NSString *newDictJSONString = [[NSString alloc] initWithData:newDictJSONData encoding:NSUTF8StringEncoding];
                        
                        // SAVE STRING TO DISK FOR NEXT OFFLINE APP OPEN
                        [SWHelper helperWriteString:newDictJSONString toFilePath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationSickScore.json"].path];
                        
                        // SEND ASYNCHRONOUS NOTIFICATION
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"currentLocationSickScore.json" object:self userInfo:sickScoreServerResponseWithOurAddOns];
                    }
                }
            });
        }];
    }
    
    // SYNCHRONOUSLY RETURN DICT FROM LOCAL FILE
    return [self getLatestCurrentLocationSickScoreSynchronouslyFromDisk];
}

- (NSDictionary *)getLatestCurrentLocationCovidScoreSynchronouslyFromDisk
{
    // IF WE DON'T HAVE A LOCAL currentLocationSickScore.json FILE YET THEN CREATE IT USING THE BUNDLE TEMPALTE THAT SHIPS WITH THE APP
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationCovidScore.json"].path])
    {
        // THEN WE CREATE IT USING THE FOLLOWING AS OUR OFFLINE STARTING POINT
        NSString *path = [[NSBundle mainBundle] pathForResource:@"currentLocationCovidScore" ofType:@"json"];
        NSURL *url = [NSURL fileURLWithPath:path];
        NSString *firstTimeAppOpenedDefaults = [NSString stringWithContentsOfFile:url.path encoding:NSUTF8StringEncoding error:nil];
        
        // CREATE FILE USING THE ABOVE STRING AS THE CONTENTS
        [[NSFileManager defaultManager] createFileAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationCovidScore.json"].path contents:[firstTimeAppOpenedDefaults dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    }
    
    // UNCONDITIONALLY READ FROM THE LOCAL FILE SYSTEM FOR THE "LATEST" LOCAL FILE
    NSDictionary *localFileAsDict = nil;
    NSData *content = [NSData dataWithContentsOfURL:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationCovidScore.json"]];
    if (content)
    {
        // PARSE FILE CONTENT AS JSON TO DICT
        NSError *jsonParseError;
        localFileAsDict = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingMutableContainers error:&jsonParseError];
    }
    
    // SYNCHRONOUSLY RETURN DICT FROM LOCAL FILE
    return localFileAsDict;
}

- (NSDictionary *)updateLatestCurrentLocationCovidScoreFromNetworkSavingToDiskFinishingWithAsynchronousNotificationUsingPlacemark:(CLPlacemark *)placemark
{
    if (placemark.location.coordinate.latitude && placemark.location.coordinate.longitude)
    {
        // GET THE LATEST AND GREATEST COPY ASYNCHRONOUSLY VIA THE SERVER AND WRITE THE RESULT TO THE LOCAL FILE SYSTEM THEN SEND AN APP WIDE NOTIFICATION
        [[SWHelper helperAppBackend] appBackendGetCovidScoreForLat:placemark.location.coordinate.latitude lon:placemark.location.coordinate.longitude usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([jsonResponseBody isKindOfClass:[NSDictionary class]])
                {
                    // CREATE MUTABLE COPY WE CAN DECORATE
                    NSMutableDictionary *covidScoreServerResponseWithOurAddOns = [jsonResponseBody mutableCopy];
                    
                    // INTEGRATE OUR PLACEMARK OBJECT WITH THE RESPONSE...
                    NSMutableDictionary *myPlacemark = [NSMutableDictionary new];
                    if ([placemark.locality isKindOfClass:[NSString class]])
                    {
                        [myPlacemark setObject:placemark.locality forKey:@"locality"];
                    }
                    if ([placemark.administrativeArea isKindOfClass:[NSString class]])
                    {
                        [myPlacemark setObject:placemark.administrativeArea forKey:@"administrativeArea"];
                        [myPlacemark setObject:placemark.administrativeArea forKey:@"administrative_area"];
                    }
                    if (placemark.location.coordinate.latitude != 0)
                    {
                        [myPlacemark setObject:@(placemark.location.coordinate.latitude) forKey:@"latitude"];
                    }
                    if (placemark.location.coordinate.longitude != 0)
                    {
                        [myPlacemark setObject:@(placemark.location.coordinate.longitude) forKey:@"longitude"];
                    }
                    [covidScoreServerResponseWithOurAddOns setObject:myPlacemark forKey:@"placemark"];
                    
                    // FLAG THIS RESPONSE AS A CURRENT LOCATION ONE
                    [covidScoreServerResponseWithOurAddOns setObject:@"yes" forKey:@"placemark_is_current_location"];
                    
                    // CONVERT COMPOSITE OBJECT TO JSON STRING
                    
                    // GET AS DATA
                    NSError *error;
                    NSData *newDictJSONData = [NSJSONSerialization dataWithJSONObject:covidScoreServerResponseWithOurAddOns options:NSJSONWritingPrettyPrinted error:&error];
                    
                    // CHECK FOR ERROR
                    if (!newDictJSONData)
                    {
                        NSLog(@"Got an error: %@", error);
                    }
                    else
                    {
                        // SUCCESS, WE HAVE IT AS DATA
                        
                        // NOW GET AS STRING
                        
                        NSLog(@"parseIllnessesWithJsonResponseBody: %@", jsonResponseBody);
                        [self parseIllnessesWithJsonResponseBody: jsonResponseBody placemark:placemark];
                        
                        NSString *newDictJSONString = [[NSString alloc] initWithData:newDictJSONData encoding:NSUTF8StringEncoding];
                        
                        // SAVE STRING TO DISK FOR NEXT OFFLINE APP OPEN
                        [SWHelper helperWriteString:newDictJSONString toFilePath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"currentLocationCovidScore.json"].path];
                        
                        // SEND ASYNCHRONOUS NOTIFICATION
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"currentLocationCovidScore.json" object:self userInfo:covidScoreServerResponseWithOurAddOns];
                    }
                }
            });
        }];
    }
    
    // SYNCHRONOUSLY RETURN DICT FROM LOCAL FILE
    return [self getLatestCurrentLocationCovidScoreSynchronouslyFromDisk];
}

- (void)addGooglePlacemarkToPlacesFile:(GMSPlace *)place
{
        // IF WE DON'T HAVE A LOCAL myPlacemarks.json FILE YET THEN CREATE IT USING THE BUNDLE TEMPALTE THAT SHIPS WITH THE APP
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path])
        {
            // THEN WE CREATE IT USING THE FOLLOWING AS OUR OFFLINE STARTING POINT
        NSString *path = [[NSBundle mainBundle] pathForResource:@"myPlacemarks" ofType:@"json"];
        NSURL *url = [NSURL fileURLWithPath:path];
        NSString *firstTimeAppOpenedDefaults = [NSString stringWithContentsOfFile:url.path encoding:NSUTF8StringEncoding error:nil];
        
            // CREATE FILE USING THE ABOVE STRING AS THE CONTENTS
        [[NSFileManager defaultManager] createFileAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path contents:[firstTimeAppOpenedDefaults dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
        }
    
        // UNCONDITIONALLY READ FROM THE LOCAL FILE SYSTEM FOR THE "LATEST" LOCAL FILE
    NSMutableDictionary *localFileAsDict = nil;
    NSData *content = [NSData dataWithContentsOfURL:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"]];
    if (content)
        {
            // PARSE FILE CONTENT AS JSON TO DICT
        NSError *jsonParseError;
        localFileAsDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingMutableContainers error:&jsonParseError];
    }
        //NSLog(@"localFileAsDict = %@", localFileAsDict);
    
    // THIS IS WHERE WE "ADD"...
    NSMutableArray *newList = [[localFileAsDict objectForKey:@"placemarks"] mutableCopy];
    NSMutableDictionary *newDict = [NSMutableDictionary new];
    if ([place.name isKindOfClass:[NSString class]]) {[newDict setObject:place.name forKey:@"name"];}
    if (CLLocationCoordinate2DIsValid(place.coordinate)) {[newDict setObject:[NSString stringWithFormat:@"%@", @(place.coordinate.latitude)] forKey:@"latitude"];}
    if (CLLocationCoordinate2DIsValid(place.coordinate)) {[newDict setObject:[NSString stringWithFormat:@"%@", @(place.coordinate.longitude)] forKey:@"longitude"];}
    if ([place.name isKindOfClass:[NSString class]]) {[newDict setObject:place.name forKey:@"locality"];}
    
    for (GMSAddressComponent *component in place.addressComponents) {
        if ([component.type isKindOfClass:[NSString class]] && [component.type isEqualToString:@"locality"] && [component.name isKindOfClass:[NSString class]]) {
            [newDict setObject:component.name forKey:@"locality"];
        }
        if ([component.type isKindOfClass:[NSString class]] && [component.type isEqualToString:@"administrative_area_level_2"]  && [component.name isKindOfClass:[NSString class]]) {
            [newDict setObject:component.name forKey:@"administrativeArea"];
        }
        if ([component.type isKindOfClass:[NSString class]] && [component.type isEqualToString:@"administrative_area_level_1"]  && [component.name isKindOfClass:[NSString class]]) {
            [newDict setObject:component.name forKey:@"administrativeArea"];
        }
        if ([component.type isKindOfClass:[NSString class]] && [component.type isEqualToString:@"country"]  && [component.name isKindOfClass:[NSString class]]) {
            [newDict setObject:component.name forKey:@"country"];
        }
    }
    
    [newList addObject:newDict];
    [localFileAsDict setObject:newList forKey:@"placemarks"];
    
        // THIS IS WHERE WE SAVE
    
        // CONVERT NEW OBJECT TO JSON STRING
    NSError *error;
    NSData *newDictJSONData = [NSJSONSerialization dataWithJSONObject:localFileAsDict options:NSJSONWritingPrettyPrinted error:&error];
    if (!newDictJSONData)
        {
        NSLog(@"Got an error: %@", error);
        }
    else
        {
            // GET AS STRING
        NSString *newDictJSONString = [[NSString alloc] initWithData:newDictJSONData encoding:NSUTF8StringEncoding];
        
            // SAVE TO DISK FOR NEXT OFFLINE APP OPEN
        [SWHelper helperWriteString:newDictJSONString toFilePath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path];
    }
}

- (void)addPlacemarkToPlacesFile:(CLPlacemark *)placemark
{
    // IF WE DON'T HAVE A LOCAL myPlacemarks.json FILE YET THEN CREATE IT USING THE BUNDLE TEMPALTE THAT SHIPS WITH THE APP
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path])
    {
        // THEN WE CREATE IT USING THE FOLLOWING AS OUR OFFLINE STARTING POINT
        NSString *path = [[NSBundle mainBundle] pathForResource:@"myPlacemarks" ofType:@"json"];
        NSURL *url = [NSURL fileURLWithPath:path];
        NSString *firstTimeAppOpenedDefaults = [NSString stringWithContentsOfFile:url.path encoding:NSUTF8StringEncoding error:nil];
        
        // CREATE FILE USING THE ABOVE STRING AS THE CONTENTS
        [[NSFileManager defaultManager] createFileAtPath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path contents:[firstTimeAppOpenedDefaults dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    }
    
    // UNCONDITIONALLY READ FROM THE LOCAL FILE SYSTEM FOR THE "LATEST" LOCAL FILE
    NSMutableDictionary *localFileAsDict = nil;
    NSData *content = [NSData dataWithContentsOfURL:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"]];
    if (content)
    {
        // PARSE FILE CONTENT AS JSON TO DICT
        NSError *jsonParseError;
        localFileAsDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingMutableContainers error:&jsonParseError];
    }
    //NSLog(@"localFileAsDict = %@", localFileAsDict);
    
    // THIS IS WHERE WE "ADD"...
    NSMutableArray *newList = [[localFileAsDict objectForKey:@"placemarks"] mutableCopy];
    NSMutableDictionary *newDict = [NSMutableDictionary new];
    /*
     @property (nonatomic, readonly, copy, nullable) NSString *name; // eg. Apple Inc.
     @property (nonatomic, readonly, copy, nullable) NSString *thoroughfare; // street name, eg. Infinite Loop
     @property (nonatomic, readonly, copy, nullable) NSString *subThoroughfare; // eg. 1
     @property (nonatomic, readonly, copy, nullable) NSString *locality; // city, eg. Cupertino
     @property (nonatomic, readonly, copy, nullable) NSString *subLocality; // neighborhood, common name, eg. Mission District
     @property (nonatomic, readonly, copy, nullable) NSString *administrativeArea; // state, eg. CA
     @property (nonatomic, readonly, copy, nullable) NSString *subAdministrativeArea; // county, eg. Santa Clara
     @property (nonatomic, readonly, copy, nullable) NSString *postalCode; // zip code, eg. 95014
     @property (nonatomic, readonly, copy, nullable) NSString *ISOcountryCode; // eg. US
     @property (nonatomic, readonly, copy, nullable) NSString *country; // eg. United States
     @property (nonatomic, readonly, copy, nullable) NSString *inlandWater; // eg. Lake Tahoe
     @property (nonatomic, readonly, copy, nullable) NSString *ocean; // eg. Pacific Ocean
     @property (nonatomic, readonly, copy, nullable) NSArray<NSString *> *areasOfInterest; // eg. Golden Gate Park
     */
    if ([placemark.name isKindOfClass:[NSString class]]) {[newDict setObject:placemark.name forKey:@"name"];}else{
        if([placemark.addressDictionary isKindOfClass:[NSDictionary class]]){
            NSString *name = [placemark.addressDictionary objectForKey:@"Name"];
            if([name isKindOfClass:[NSString class]] && ![name isEqual:[NSNull null]] && ![name isEqualToString:@""]){
                [newDict setObject:name forKey:@"name"];
            }
        }
    }
    if ([placemark.location isKindOfClass:[CLLocation class]]) {[newDict setObject:[NSString stringWithFormat:@"%@", @(placemark.location.coordinate.latitude)] forKey:@"latitude"];}
    if ([placemark.location isKindOfClass:[CLLocation class]]) {[newDict setObject:[NSString stringWithFormat:@"%@", @(placemark.location.coordinate.longitude)] forKey:@"longitude"];}
    if ([placemark.thoroughfare isKindOfClass:[NSString class]]) {[newDict setObject:placemark.thoroughfare forKey:@"thoroughfare"];}
    if ([placemark.subThoroughfare isKindOfClass:[NSString class]]) {[newDict setObject:placemark.subThoroughfare forKey:@"subThoroughfare"];}
    if ([placemark.locality isKindOfClass:[NSString class]]) {[newDict setObject:placemark.locality forKey:@"locality"];}else{
        if([placemark.addressDictionary isKindOfClass:[NSDictionary class]]){
            NSString *name = [placemark.addressDictionary objectForKey:@"Name"];
            if([name isKindOfClass:[NSString class]] && ![name isEqual:[NSNull null]] && ![name isEqualToString:@""]){
                [newDict setObject:name forKey:@"locality"];
            }
        }
    }
    if ([placemark.subLocality isKindOfClass:[NSString class]]) {[newDict setObject:placemark.subLocality forKey:@"subLocality"];}
    if ([placemark.administrativeArea isKindOfClass:[NSString class]]) {[newDict setObject:placemark.administrativeArea forKey:@"administrativeArea"];}
    if ([placemark.subAdministrativeArea isKindOfClass:[NSString class]]) {[newDict setObject:placemark.subAdministrativeArea forKey:@"subAdministrativeArea"];}
    if ([placemark.postalCode isKindOfClass:[NSString class]]) {[newDict setObject:placemark.postalCode forKey:@"postalCode"];}
    if ([placemark.ISOcountryCode isKindOfClass:[NSString class]]) {[newDict setObject:placemark.ISOcountryCode forKey:@"ISOcountryCode"];}
    if ([placemark.country isKindOfClass:[NSString class]]) {[newDict setObject:placemark.country forKey:@"country"];}
    if ([placemark.inlandWater isKindOfClass:[NSString class]]) {[newDict setObject:placemark.inlandWater forKey:@"inlandWater"];}
    if ([placemark.ocean isKindOfClass:[NSString class]]) {[newDict setObject:placemark.ocean forKey:@"ocean"];}
    [newList addObject:newDict];
    [localFileAsDict setObject:newList forKey:@"placemarks"];
    
    // THIS IS WHERE WE SAVE
    
    // CONVERT NEW OBJECT TO JSON STRING
    NSError *error;
    NSData *newDictJSONData = [NSJSONSerialization dataWithJSONObject:localFileAsDict options:NSJSONWritingPrettyPrinted error:&error];
    if (!newDictJSONData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        // GET AS STRING
        NSString *newDictJSONString = [[NSString alloc] initWithData:newDictJSONData encoding:NSUTF8StringEncoding];
        
        // SAVE TO DISK FOR NEXT OFFLINE APP OPEN
        [SWHelper helperWriteString:newDictJSONString toFilePath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path];
    }
}

- (NSDictionary *)getPlacemarksFromPlacesFile
{
    // UNCONDITIONALLY READ FROM THE LOCAL FILE SYSTEM FOR THE "LATEST" LOCAL FILE
    NSMutableDictionary *localFileAsDict = nil;
    NSData *content = [NSData dataWithContentsOfURL:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"]];
    if (content)
    {
        // PARSE FILE CONTENT AS JSON TO DICT
        NSError *jsonParseError;
        localFileAsDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:content options:NSJSONReadingMutableContainers error:&jsonParseError];
    }
    //NSLog(@"localFileAsDict = %@", localFileAsDict);
    return localFileAsDict;
}

- (void)savePlacemarksToPlacesFile:(NSDictionary *)placemarks
{
    NSError *error;
    NSData *newDictJSONData = [NSJSONSerialization dataWithJSONObject:placemarks options:NSJSONWritingPrettyPrinted error:&error];
    if (!newDictJSONData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        // GET AS STRING
        NSString *newDictJSONString = [[NSString alloc] initWithData:newDictJSONData encoding:NSUTF8StringEncoding];
        
        // SAVE TO DISK FOR NEXT OFFLINE APP OPEN
        [SWHelper helperWriteString:newDictJSONString toFilePath:[SWHelper helperGetDocumentDirectoryURLForFileName:@"myPlacemarks.json"].path];
    }
}

#pragma mark - WCSessionDelegate

- (void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *, id> *)message replyHandler:(void (^)(NSDictionary<NSString *, id> *replyMessage))replyHandler
{
    //NSLog(@"message = %@", message);
    //replyHandler(@{@"test_key":@"test_value"});
    NSDictionary *userInfo = message;
    UIApplication *application = [UIApplication sharedApplication];
    if ([[userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"] isEqualToString:@"presentLocalNotificationForHandWashTimerComplete"])
    {
        NSUInteger myBackgroundTaskID = [application beginBackgroundTaskWithName:[NSString stringWithFormat:@"BackgroundTask-%@", [userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"]] expirationHandler:^{}];
        NSDate *fireDate = [NSDate date]; // Now
        NSNumber *fireDateSecondsFromNow = [userInfo objectForKey:@"fireDateSecondsFromNow"];
        if (fireDateSecondsFromNow)
        {
            fireDate = [[NSDate alloc] initWithTimeIntervalSinceNow:[fireDateSecondsFromNow integerValue]];
        }
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = fireDate;
        localNotification.alertTitle = @"Done!";
        localNotification.alertBody = @"Hand wash timer complete.";
        localNotification.alertAction = @"";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.userInfo = userInfo;
        [application scheduleLocalNotification:localNotification];
        [application endBackgroundTask:myBackgroundTaskID];
        replyHandler(@{});
    }
    else if ([[userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"] isEqualToString:@"clearAllHandWashTimerCompleteNotifications"])
    {
        NSUInteger myBackgroundTaskID = [application beginBackgroundTaskWithName:[NSString stringWithFormat:@"BackgroundTask-%@", [userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"]] expirationHandler:^{}];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [application endBackgroundTask:myBackgroundTaskID];
        replyHandler(@{});
    }
    else if ([[userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"] isEqualToString:@"getSickScoreForCurrentLocation"])
    {
        self.handleWatchKitExtensionRequestBackgroundId = [[UIApplication sharedApplication] beginBackgroundTaskWithName:[NSString stringWithFormat:@"BackgroundTask-%@", [userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"]] expirationHandler:^{}];
        self.handleWatchKitExtensionRequestReplyBlock = replyHandler;
        self.handleWatchKitExtensionRequestUserInfo = userInfo;
        if (self.hasSentSWCoreDataManagedDocumentDidLoadNotificationKey == YES)
        {
            [self handleWatchKitExtensionRequestNowThatCoreDataIsAvailable:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleWatchKitExtensionRequestNowThatCoreDataIsAvailable:)
                                                         name:kSWCoreDataManagedDocumentDidLoadNotificationKey
                                                       object:nil];
        }
    }
}

#pragma mark - CLLocationManagerDelegate (App-Wide Notifications)

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWLocationManagerDidEnterRegionNotification
                                                        object:self
                                                      userInfo:@{@"didEnterRegion":region}];
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWLocationManagerDidDetermineStateNotification
                                                        object:self
                                                      userInfo:@{@"didDetermineState":[NSNumber numberWithInteger:state], @"forRegion":region}];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWLocationManagerMonitoringDidFailForRegionWithErrorNotification
                                                        object:self
                                                      userInfo:@{@"monitoringDidFailForRegion":region, @"withError":error}];
    //NSLog(@"monitoringDidFailForRegion:%@ withError:%@", region, error);
    
    // Remove the region since it failed and move on to the next one
    NSUInteger lookupIndex = [self.startMonitoringForRegionsQueue indexOfObject:region];
    if (lookupIndex != NSNotFound)
    {
        [self.startMonitoringForRegionsQueue removeObject:region];
        [self.startMonitoringForRegionsQueueReports removeObjectAtIndex:lookupIndex];
    }
    
    // Check and see if there are any more regions in the queue
    if ([self.startMonitoringForRegionsQueue count] == 0)
    {
        // Done
        
        // End of process, update flag
        self.syncRegionsCallIsProcessing = NO;
    }
    else
    {
        // Do some more munching
        [self startMonitoringNextRegionInQueue];
    }
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWLocationManagerDidStartMonitoringForRegionNotification
                                                        object:self
                                                      userInfo:@{@"didStartMonitoringForRegion":region}];
    
    // Log to database
    NSInteger regionIndex = [self.startMonitoringForRegionsQueue indexOfObject:region];
    if (regionIndex != NSNotFound)
    {
        NSDictionary *report = [self.startMonitoringForRegionsQueueReports objectAtIndex:regionIndex];
        if (report)
        {
            [SWAlertLogCDM createAlertLogObjectThatWeHaveStartedRegionMonitoringForForReport:report region:region]; // Our one app-wide call to log that we at some point DID startMonitoringForRegion for this newRegionToMonitor
            
            // Remove from queue now that we are successfully monitoring the region
            NSUInteger lookupIndex = [self.startMonitoringForRegionsQueue indexOfObject:region];
            if (lookupIndex != NSNotFound)
            {
                NSDictionary *report = [self.startMonitoringForRegionsQueueReports objectAtIndex:lookupIndex];
                if (SHOW_ALERT_LOGIC_LOG)
                {
                    //NSLog(@"ALERT LOGIC LOG: Did START MONITORING for nearness to this %@ report (id:%@,illness:%@)", [report objectForKey:@"illness_word"], [report objectForKey:@"id"], [report objectForKey:@"illness"]);
                }
                [self.startMonitoringForRegionsQueue removeObject:region];
                [self.startMonitoringForRegionsQueueReports removeObjectAtIndex:lookupIndex];
            }
            
            // Check and see if there are any more regions in the queue
            if ([self.startMonitoringForRegionsQueue count] == 0)
            {
                // Done
                
                // End of process, update flag
                self.syncRegionsCallIsProcessing = NO;
            }
            else
            {
                // Do some more munching
                [self startMonitoringNextRegionInQueue];
            }
            
            // Trigger async call to didDetermineState
            // It's key we delay this to avoid triggering monitoringDidFailForRegion
            // See: http://stackoverflow.com/a/24032673/394969
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[SWHelper helperLocationManager] requestStateForRegion:region];
            });
        }
        else
        {
            //NSLog(@"ISSUE!!! self.startMonitoringForRegionsQueueReports is missing report at regionIndex, %@", @(regionIndex));
            //NSLog(@"ISSUE!!! self.startMonitoringForRegionsQueueReports, %@", self.startMonitoringForRegionsQueueReports);
        }
    }
    else
    {
        //NSLog(@"ISSUE!!! self.startMonitoringForRegionsQueue is missing region, %@", region);
        //NSLog(@"ISSUE!!! self.startMonitoringForRegionsQueue, %@", self.startMonitoringForRegionsQueue);
    }
}

- (void)stopMonitoringAllCurrentMonitoredRegions
{
    // Clear current (using our own data source since monitoredRegions
    // gets populated over a period of time asynchronously)
    for (CLRegion *region in [SWAlertLogCDM regionsAlreadyFetchedFromWebServiceThatWeHaveStartedMonitoringFor])
    {
        // If you're monitoring this region, don't (remember, monitoredRegions
        // populates over time async, that's why we went to our own data source
        // with code like this).
        [[SWHelper helperLocationManager] stopMonitoringForRegion:region];
    }
    
    // In the odd case that you're for some reason still monitoring something, don't.
    for (CLRegion *region in [[[SWHelper helperLocationManager] monitoredRegions] allObjects])
    {
        // STOP MONITORING for all current regions
        [[SWHelper helperLocationManager] stopMonitoringForRegion:region];
    }
}

- (BOOL)weAreCurrentlyMonitoringOrAreInQueueToStartMonitoringReport:(NSDictionary *)report
{
    for (CLRegion *region in [SWHelper helperLocationManager].monitoredRegions)
    {
        if ([region.identifier isEqualToString:[report objectForKey:@"id"]])
        {
            return YES; // WeAreCurrentlyMonitoring
        }
    }
    for (CLRegion *region in self.startMonitoringForRegionsQueue)
    {
        if ([region.identifier isEqualToString:[report objectForKey:@"id"]])
        {
            return YES; // AreInQueueToStartMonitoring
        }
    }
    return NO;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // GET LOCATION
    CLLocation *location = [locations lastObject];
    
    // PICK DATA ATTRIBUTES TO PASS UP TO SERVER
    NSMutableDictionary *dataForServer = [NSMutableDictionary new];
    if ([location.timestamp isKindOfClass:[NSString class]])
    {
        //NSLog(@"location.timestamp = %@", location.timestamp);
        [dataForServer setObject:location.timestamp forKey:@"timestamp"];
    }
    
    //NSLog(@"location.speed = %@", [@(location.speed) stringValue]);
    [dataForServer setObject:[@(location.speed) stringValue] forKey:@"speed"];
    
    //NSLog(@"location.course = %@", [@(location.course) stringValue]);
    [dataForServer setObject:[@(location.course) stringValue] forKey:@"course"];
    
    //NSLog(@"location.altitude = %@", [@(location.altitude) stringValue]);
    [dataForServer setObject:[@(location.altitude) stringValue] forKey:@"altitude"];
    
    // ADD USER ILLNESS TYPE PREFS
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *networkIllnesses = [SWHelper helperLoadJSONObjectFromDocsUsingName:@"my-network-illnesses"];
    NSArray *individualIllnesses = @[];
//    NSLog(@"myNetworkIllnesses = %@", networkIllnesses);
    if (networkIllnesses && [networkIllnesses isKindOfClass:[NSArray class]]){
        for (NSDictionary *illness in networkIllnesses)
            {
            @try
                {
                if ([illness isKindOfClass:[NSDictionary class]] && [illness objectForKey:@"id"] != nil && ![[illness objectForKey:@"id"] isEqual:[NSNull null]] && ![[illness objectForKey:@"id"] containsString:@","])
                    {
                        individualIllnesses = [individualIllnesses arrayByAddingObject:[illness objectForKey:@"id"]];
                    }
                }
            @catch (NSException * e) {
                [SWHelper addNonFatalExceptionWith:e];
            }
        }
    }
    
    //NSLog(@"individualIllnesses = %@", individualIllnesses);
    NSArray *illnessIdsOn = @[];
    for (NSString *individualIllnessId in individualIllnesses)
    {
        //NSLog(@"individualIllnessId = %@", individualIllnessId);
        NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_for_id_%@_yes_no", individualIllnessId];
        if ([[defaults stringForKey:defaultsKey] isEqualToString:@"yes"])
        {
            illnessIdsOn = [illnessIdsOn arrayByAddingObject:individualIllnessId];
        }
    }
    //NSLog(@"illnessIdsOn = %@", illnessIdsOn);
    [dataForServer setObject:[illnessIdsOn componentsJoinedByString:@","] forKey:@"alert_when_near"];
    if ([[defaults stringForKey:@"proximity_alerts_are_on_yes_no"] isEqualToString:@"yes"])
    {
        // PROXIMITY ALERTS ARE ON, LEAVE AS IS
    }
    else
    {
        // PROXIMITY ALERTS ARE OFF, DROP FROM REQUEST
        [dataForServer setObject:@"" forKey:@"alert_when_near"];
    }
	__block NSString *oneSignalUserID = @"";
	[OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
		oneSignalUserID = userId;
	}];
	
	[Flurry logEvent:@"Location Update" withParameters:@{@"latitude":[NSString stringWithFormat:@"%@", @(location.coordinate.latitude)], @"logitude" : [NSString stringWithFormat:@"%@", @(location.coordinate.longitude)], @"timestamp" : location.timestamp, @"one_signal_user_id":oneSignalUserID}];
	
    // PASS THIS INFO UP TO THE SICKWEATHER SERVER ONE TIME FOR EVERY TIME THIS METHOD IS CALLED
    [[SWHelper helperAppBackend] appBackendUpdateLocationForUserUsingLat:location.coordinate.latitude lon:location.coordinate.longitude extraArgs:dataForServer usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
        });
    }];
    
    // GET USER
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    
    // Get user's last known lat/lon
    CLLocation *userLastKnownCurrentLocationPlacemarkLocation = nil;
    NSString *userLastKnownCurrentLocationPlacemarkLatitude = [[SWHelper helperDefaults] objectForKey:[SWDefaultsKey keyForUserLastKnownCurrentLocationPlacemarkLatitude]];
    NSString *userLastKnownCurrentLocationPlacemarkLongitude = [[SWHelper helperDefaults] objectForKey:[SWDefaultsKey keyForUserLastKnownCurrentLocationPlacemarkLongitude]];
    if (userLastKnownCurrentLocationPlacemarkLatitude)
    {
        userLastKnownCurrentLocationPlacemarkLocation = [[CLLocation alloc] initWithLatitude:userLastKnownCurrentLocationPlacemarkLatitude.doubleValue longitude:userLastKnownCurrentLocationPlacemarkLongitude.doubleValue];
    }
    
    // Log user's current lat/lon as last known
    currentUser.userLastKnownCurrentLocationLatitude = [NSNumber numberWithDouble:location.coordinate.latitude];
    currentUser.userLastKnownCurrentLocationLongitude = [NSNumber numberWithDouble:location.coordinate.longitude];
    
    // Log user's last known lat/lon to OneSignal for push...
    //NSLog(@"latitude = %@", [currentUser.userLastKnownCurrentLocationLatitude stringValue]);
    //NSLog(@"longitude = %@", [currentUser.userLastKnownCurrentLocationLongitude stringValue]);
	NSMutableDictionary * additionalData = [[SWUserCDM currentlyLoggedInUser] userProfileDataForSnow];
	[SWAnalytics updateLocation:location.coordinate.latitude longitude:location.coordinate.longitude params:additionalData];
	
    [OneSignal sendTag:@"Latitude" value:[currentUser.userLastKnownCurrentLocationLatitude stringValue]];
    [OneSignal sendTag:@"Longitude" value:[currentUser.userLastKnownCurrentLocationLongitude stringValue]];
    
    // Define distance filters
    CLLocationDistance distanceFilterForReverseGeocodeLocation = 200;
    
    // Get distance diff between last known and current
    CLLocationDistance distanceDiff;
    if (userLastKnownCurrentLocationPlacemarkLocation)
    {
        distanceDiff = [userLastKnownCurrentLocationPlacemarkLocation distanceFromLocation:location];
    }
    else
    {
        distanceDiff = distanceFilterForReverseGeocodeLocation + 1; // Trigger refresh
    }
    
    // Check distance diff versus distance filters
    if (distanceDiff > distanceFilterForReverseGeocodeLocation)
    {
        // Update!
        
        // Get user's location, and tag 'em
        [[SWHelper helperDefaults] setObject:[NSString stringWithFormat:@"%f", location.coordinate.latitude] forKey:[SWDefaultsKey keyForUserLastKnownCurrentLocationPlacemarkLatitude]];
        [[SWHelper helperDefaults] setObject:[NSString stringWithFormat:@"%f", location.coordinate.longitude] forKey:[SWDefaultsKey keyForUserLastKnownCurrentLocationPlacemarkLongitude]];
        [self.geocodeManager reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            
            // Get placemark
            CLPlacemark *placemark = [placemarks lastObject];
            
            if (placemark != nil)
            {
                // Post notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                    object:self
                                                                  userInfo:@{@"placemark":placemark}];
            }
        }];
    }
    else
    {
        // Do not run a new reverseGeocodeLocation call
    }
    
    // Log to Flurry?
    if (self.logLocationToFlurry)
    {
        // Log to Flurry
        if (location)
        {
            [Flurry setLatitude:location.coordinate.latitude
                      longitude:location.coordinate.longitude
             horizontalAccuracy:location.horizontalAccuracy
               verticalAccuracy:location.verticalAccuracy];
            self.logLocationToFlurry = NO; // One-time complete
        }
    }
    
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWLocationManagerDidUpdateLocationsNotification
                                                        object:self
                                                      userInfo:@{@"didUpdateLocations":locations}];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    // CHECK AND SEE WHAT'S GOING ON!
    
    // CHECK FOR ALWAY AUTH FIRST
    if (status == kCLAuthorizationStatusAuthorizedAlways)
    {
        // START UPDATING BOTH IN-APP WHILE IN USE LOCATION UPDATES
        [manager startUpdatingLocation];
        
        // AND, SINCE WE HAVE ALWAYS ACCESS, ALSO START SIGNIFICANT LOCATION CHANGES!
        [manager startMonitoringSignificantLocationChanges];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults valueForKey:@"IS_CONSENT_ASKED"]) {
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showConsentNoticeAlertIfLocationAvailable" object:self userInfo:nil];
            });
        }
    }
    
    // CHECK FOR WHEN IN USE ACCESS NEXT (NEXT BEST)
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        // START UPDATING ONLY IN-APP WHILE IN USE LOCATION UPDATES
        // SIGNIFICANT CHANGE NOT AVAILABLE SINCE WE DO NOT HAVE ALWAYS ACCESS
        [manager startUpdatingLocation];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults valueForKey:@"IS_CONSENT_ASKED"]) {
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"showConsentNoticeAlertIfLocationAvailable" object:self userInfo:nil];
            });
        }
       
    }
    else
    {
		// WE NO LONGER HAVE LOCATION ACCESS!
        // STOP UPDATING LOCATION ALL TOGETHER
        [manager stopUpdatingLocation];
        [manager stopMonitoringSignificantLocationChanges];
        
        // TURN ALERTS OFF
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:[SWConstants masterSwitchForSickweatherProximityBasedIllnessAlertsIsOnDefaultsKey]];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
	
    [self initializeXmodeSdk];
	
    // OK, NOW TELL THE ENTIRE APP ABOUT THIS CHANGE IN LOCATION AUTHORIZATION!
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWLocationManagerDidChangeAuthorizationStatusNotification
                                                        object:self
                                                      userInfo:@{@"didChangeAuthorizationStatus":[NSNumber numberWithInt:status]}];
}

#pragma mark - NSNotificationCenter Callback Methods

- (void)didUpdateCurrentLocationPlacemarkNotification:(NSNotification *)notification
{
    NSString *error = [notification.userInfo objectForKey:@"error"];
    CLPlacemark *placemark = [notification.userInfo objectForKey:@"placemark"];
    NSLog(@"placemark = %@", placemark);
    NSLog(@"error = %@", error);
    if (error)
    {
        //NSLog(@"error = %@", error);
    }
    else if (placemark)
    {
        [self updateUserCurrentLocationPlacemarkUsingPlacemark:placemark];
    }
}

#pragma mark - Helpers

// This is our heavy hitter method for getting the right data written to disk...
- (void)_helperUpdateLocalDistributedFileSystemForLocation:(CLPlacemark *)placemark
{
    //NSLog(@"Time to update system location using the given placemark object...");
    [[SWHelper helperAppBackend] appBackendGetSickScoreInRadiusForLat:placemark.location.coordinate.latitude lon:placemark.location.coordinate.longitude usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        //NSLog(@"Background thread");
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //NSLog(@"So here's the gameplan... write this data to disk platform wide.");
            
            // VIA USER DEFAULTS
            if (![jsonResponseBody isKindOfClass:[NSDictionary class]])
            {
                // ^ Check for nil, avoid crash, don't log nil to defaults via setObject:nil, you'll crash!!
                // Avoid crash!! This should be logged by our famous "App Crash Averted - getSickScoreInRadius.php returned nil"
            }
            else
            {
                // jsonResponseBody IS NSDictionary...
                /* NOTE, THIS IS WHERE A NULL LIST USED TO CRASH THE APP INSTANTLY FOR SOME USERS, MIKE FIXED ON THE BACKEND */
                [self parseIllnessesWithJsonResponseBody: jsonResponseBody placemark:placemark];
            }
        });
    }];
}

- (void)parseIllnessesWithJsonResponseBody:jsonResponseBody placemark: (CLPlacemark *) placemark {
    NSUserDefaults *sickweatherAppGroupDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.sickweather.Sickweather"];
            NSMutableDictionary *mutableDictionary = [jsonResponseBody mutableCopy];
            NSMutableDictionary *placeMark = [[jsonResponseBody objectForKey:@"placemark"] mutableCopy];
            if ([placeMark isKindOfClass:[NSMutableDictionary class]] && ([placeMark objectForKey:@"locality"] == nil || [[placeMark objectForKey:@"locality"] isEqual:[NSNull null]])){
                [placeMark setObject:@"" forKey:@"locality"];
            }
            if ([placeMark isKindOfClass:[NSMutableDictionary class]] && ([placeMark objectForKey:@"administrative_area"] == nil || [[placeMark objectForKey:@"administrative_area"] isEqual:[NSNull null]])){
                [placeMark setObject:@"" forKey:@"administrative_area"];
            }
            if ([placeMark isKindOfClass:[NSMutableDictionary class]] && ([placeMark objectForKey:@"latitude"] == nil || [[placeMark objectForKey:@"latitude"] isEqual:[NSNull null]])){
                [placeMark setObject:@"" forKey:@"latitude"];
            }
            if ([placeMark isKindOfClass:[NSMutableDictionary class]] && ([placeMark objectForKey:@"longitude"] == nil || [[placeMark objectForKey:@"longitude"] isEqual:[NSNull null]])){
                [placeMark setObject:@"" forKey:@"longitude"];
            }
            if([placeMark isKindOfClass:[NSMutableDictionary class]]){
                [mutableDictionary setObject:placeMark forKey:@"placemark"];
            }
            NSArray * illnesses = [mutableDictionary objectForKey:@"illnesses"];
            NSArray * illness = illnesses ? illnesses : [mutableDictionary objectForKey:@"trending_keywords"];
            NSMutableArray *mutedArray = [[NSMutableArray alloc] init];
            for (NSDictionary *dic in illness) {
                NSMutableDictionary *mutableDictionary = [dic mutableCopy];
                NSString * trending = [mutableDictionary valueForKey:@"trending"];
                if ( trending == nil || [trending isEqual:[NSNull null]]){
                    [mutableDictionary setObject:@"" forKey:@"trending"];
                }
                NSString * trending_percent = [mutableDictionary valueForKey:@"trending_percent"];
                if ( trending_percent == nil || [trending_percent isEqual:[NSNull null]]){
                    [mutableDictionary setObject:@"0" forKey:@"trending_percent"];
                }
                NSString * illness_short_code = [mutableDictionary valueForKey:@"illness_short_code"];
                if ( illness_short_code == nil || [illness_short_code isEqual:[NSNull null]]){
                    [mutableDictionary setObject:@"" forKey:@"illness_short_code"];
                }
                NSString * illness_name = [mutableDictionary valueForKey:@"illness_name"];
                if ( illness_name == nil || [illness_name isEqual:[NSNull null]]){
                    [mutableDictionary setObject:@"" forKey:@"illness_name"];
                }
                NSString * illness_id = [mutableDictionary valueForKey:@"illness_id"];
                if ( illness_id == nil || [illness_id isEqual:[NSNull null]]){
                    [mutableDictionary setObject:@"11" forKey:@"illness_id"];
                }
                NSString * number_of_reports = [mutableDictionary valueForKey:@"number_of_reports"];
                if ( number_of_reports == nil || [number_of_reports isEqual:[NSNull null]]){
                    [mutableDictionary setObject:@"0" forKey:@"number_of_reports"];
                }
                [mutableDictionary setObject:@[] forKey:@"info"];
                [mutedArray addObject:mutableDictionary];
            }
            [mutableDictionary setObject:[mutedArray copy] forKey:@"illnesses"];
            @try
            {
                [sickweatherAppGroupDefaults setObject:[mutableDictionary copy] forKey:@"getSickScoreInRadius"];
            }
            @catch (NSException * e) {
                [SWHelper addNonFatalExceptionWith:e];
            }
    
            // ALSO NOW LOG OUR PLACEMARK INFO HERE TOO (AS DICTIONARY)
            NSMutableDictionary *placemarkAsDict = [[NSMutableDictionary alloc] init];
            if ([placemark.name isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.name forKey:@"name"];
            }
            if ([placemark.addressDictionary isKindOfClass:[NSDictionary class]])
            {
                [placemarkAsDict setObject:placemark.addressDictionary forKey:@"addressDictionary"];
            }
            if ([placemark.ISOcountryCode isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.ISOcountryCode forKey:@"ISOcountryCode"];
            }
            if ([placemark.country isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.country forKey:@"country"];
            }
            if ([placemark.postalCode isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.postalCode forKey:@"postalCode"];
            }
            if ([placemark.administrativeArea isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.administrativeArea forKey:@"administrativeArea"];
            }
            if ([placemark.subAdministrativeArea isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.subAdministrativeArea forKey:@"subAdministrativeArea"];
            }
            if ([placemark.locality isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.locality forKey:@"locality"];
            }
            if ([placemark.subLocality isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.subLocality forKey:@"subLocality"];
            }
            if ([placemark.thoroughfare isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.thoroughfare forKey:@"thoroughfare"];
            }
            if ([placemark.subThoroughfare isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.subThoroughfare forKey:@"subThoroughfare"];
            }
            if ([placemark.region isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.region forKey:@"region"];
            }
            if ([placemark respondsToSelector:@selector(timeZone)] && [placemark.timeZone isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.timeZone forKey:@"timeZone"];
            }
            if ([placemark.inlandWater isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.inlandWater forKey:@"inlandWater"];
            }
            if ([placemark.ocean isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.ocean forKey:@"ocean"];
            }
            if ([placemark.areasOfInterest isKindOfClass:[NSString class]])
            {
                [placemarkAsDict setObject:placemark.areasOfInterest forKey:@"areasOfInterest"];
            }
        
            @try
            {
                [sickweatherAppGroupDefaults setObject:placemarkAsDict forKey:@"placemark"];
            }
            @catch (NSException * e) {
                [SWHelper addNonFatalExceptionWith:e];
            }
        
            // VIA NATIVE FILE SYSTEM (HERE FOR EXAMPLE, NOT BEING USED)
            /*
             NSURL *groupURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:@"group.com.sickweather.Sickweather"];
             NSURL *localDistributedGetSickScoreInRadiusFileURL = [groupURL URLByAppendingPathComponent:@"getSickScoreInRadius"];
             NSError *error;
             [responseBodyAsString writeToURL:localDistributedGetSickScoreInRadiusFileURL atomically:YES encoding:NSUTF8StringEncoding error:&error];
             if (error) {NSLog(@"error = %@", error);}
             */
}

- (void)helperPreloadKeyboardToEliminateLagOnFirstTapLater
{
    // Preloads keyboard so there's no lag on initial keyboard appearance.
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.window addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
}

- (void)startMonitoringNextRegionInQueue
{
    CLCircularRegion *regionToMonitor = nil;
    if ([self.startMonitoringForRegionsQueue count] > 0)
    {
        regionToMonitor = [self.startMonitoringForRegionsQueue objectAtIndex:0]; // fifo
    }
    if (regionToMonitor)
    {
        // Then START MONITORING for it
        [[SWHelper helperLocationManager] startMonitoringForRegion:regionToMonitor]; // Our one app-wide call to startMonitoringForRegion
    }
    else
    {
        // End of process, update flag
        self.syncRegionsCallIsProcessing = NO;
    }
}

- (void)updateUserCurrentLocationPlacemarkUsingPlacemark:(CLPlacemark *)placemark
{
    // LOAD LATEST DATA FROM NETWORK, SAVE TO DISTRIBUTED APP GROUP FILE SYSTEM
    [self _helperUpdateLocalDistributedFileSystemForLocation:placemark];
    
    // Helper code for later extension...
    NSString *name = @"";
    NSString *value = @"";
    
    name = @"user-location-name";
    value = [[placemark.name stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // 101-main-st
    
    //NSLog(@"placemark.addressDictionary = %@", placemark.addressDictionary);
    
    name = @"user-location-iso-country-code";
    value = [[placemark.ISOcountryCode stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // us
    
    name = @"user-location-country";
    value = [[placemark.country stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // united-states
    
    name = @"user-location-postal-code";
    value = [[placemark.postalCode stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // 92648
    
    name = @"user-location-administrative-area";
    value = [[placemark.administrativeArea stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // ca
    
    name = @"user-location-sub-administrative-area";
    value = [[placemark.subAdministrativeArea stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // orange
    
    name = @"user-location-locality";
    value = [[placemark.locality stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // huntington-beach
    
    name = @"user-location-sub-locality";
    value = [[placemark.subLocality stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // downtown
    
    name = @"user-location-thoroughfare";
    value = [[placemark.thoroughfare stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // main-st
    
    name = @"user-location-sub-thoroughfare";
    value = [[placemark.subThoroughfare stringByReplacingOccurrencesOfString: @" " withString:@"-"] lowercaseString];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // 101
    
    //NSLog(@"placemark.region = %@", placemark.region);
    
    name = @"Latitude";
    value = [[NSNumber numberWithDouble:placemark.location.coordinate.latitude] stringValue];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // 33.6575879
    
    name = @"Longitude";
    value = [[NSNumber numberWithDouble:placemark.location.coordinate.longitude] stringValue];
    //NSLog(@"%@ = %@", name, value);
    [OneSignal sendTag:name value:value]; // -118.001893
    
    // Log to user if user available
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    if (currentUser)
    {
        // 1 of 2 spots where we log the user's last known location placemark
        currentUser.userLastKnownCurrentLocationPlacemark = [NSKeyedArchiver archivedDataWithRootObject:placemark];
        CLPlacemark *placemark = [currentUser userLastKnownCurrentLocationPlacemarkObject];
        if ([currentUser.country length] == 0 && [currentUser.state length] == 0 && [currentUser.city length] == 0)
        {
            currentUser.country = placemark.country;
            currentUser.state = placemark.administrativeArea;
            currentUser.city = placemark.locality;
        }
        self.saveOnLoadUserLastKnownCurrentLocationPlacemarkData = nil;
    }
    else
    {
        // Log as prop to save later (on db init)
        self.saveOnLoadUserLastKnownCurrentLocationPlacemarkData = [NSKeyedArchiver archivedDataWithRootObject:placemark];
    }
}

- (NSURL *)managedDocumentURL
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"Sickweather Managed Object Document For Managed Object Context"];
    return url;
}

/*!
 @abstract This method is designed to be called immediately after the core data database becomes available. The very last thing this method does is send out an NSNotification letting the rest of the app know core data is available. This method, by way of editing its contents, gives us a place to put code that depends on an available database, like starting location services (since we log location updates to core data as the user's most recent current location).
 */
- (void)initializeManagedObjectContextDataThenBroadcastNotification
{
    // Get illnesses
    [self getIllnessesSynchronouslyFollowedByAsynchronousNetworkUpdate:^(NSArray *array) {
        //NSLog(@"array = %@", array);
    }];
    [self getSymptomsFromBackend];
    [self getFamilyMemberIdFromBackend];
    //Get Tracker Types
    [self getTrackerTypes];
    // If we have a lask known user location, roll with that...
    CLPlacemark *laskKnownLocationPlacemark = [[SWUserCDM currentUser] userLastKnownCurrentLocationPlacemarkObject];
    if (laskKnownLocationPlacemark)
    {
        [self _helperUpdateLocalDistributedFileSystemForLocation:laskKnownLocationPlacemark];
    }
    
    // THIS IS 1 OF 2 PLACES, APP-WIDE, THAT WE PUT THIS CODE, THE OTHER IS ON SUCCESSFUL LOGIN (Get Reports)
    if ([SWUserCDM currentlyLoggedInUser])
    {
        // We do have a logged in user, issue the call that requires auth
        [self appDelegateUpdateUserBasedOnNetwork];
    }
    else
    {
        // We don't have a logged in user so we are going to skip this call that requires auth
        //NSLog(@"// We don't have a logged in user so we are going to skip this call that requires auth");
    }
    
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    if (self.saveOnLoadUserLastKnownCurrentLocationPlacemarkData)
    {
        // 2 of 2 spots where we log the user's last known location placemark
        currentUser.userLastKnownCurrentLocationPlacemark = self.saveOnLoadUserLastKnownCurrentLocationPlacemarkData;
        CLPlacemark *placemark = [currentUser userLastKnownCurrentLocationPlacemarkObject];
        if ([currentUser.country length] == 0 && [currentUser.state length] == 0 && [currentUser.city length] == 0)
        {
            currentUser.country = placemark.country;
            currentUser.state = placemark.administrativeArea;
            currentUser.city = placemark.locality;
        }
        self.saveOnLoadUserLastKnownCurrentLocationPlacemarkData = nil;
    }
    
    // Create shared geocoder manager object (rate limits requests)
    // So we can get user's city, state, etc on app open if location is authorized...
    self.geocodeManager = [[SWGeocodeManager alloc] initWithGeocoder:[[CLGeocoder alloc] init]];
    
    // Init and set our global location manager object
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = 800; // Meters
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.locationManager.activityType = CLActivityTypeOther;
    self.locationManager.pausesLocationUpdatesAutomatically = YES; // Set to YES as instructed to do so via docs: https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/LocationAwarenessPG/CoreLocation/CoreLocation.html
    self.locationManager.delegate = self; // Our single app-wide location manager object, broadcasts updates to entire app via NSNotifications
    
    // Something we can interface with later if we want
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusAvailable)
    {
        //NSLog(@"UIBackgroundRefreshStatusAvailable");
    }
    else if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied)
    {
        //NSLog(@"UIBackgroundRefreshStatusDenied");
    }
    else if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted)
    {
        //NSLog(@"UIBackgroundRefreshStatusRestricted");
    }
    
    // Check auth status
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // Enable the location service here, this value is persisted.
        //[UALocationService setAirshipLocationServiceEnabled:YES];
        //UALocationService *locationService = [[UAirship shared] locationService];
        
        // Run this once to get the current location after the pitch
        //[locationService reportCurrentLocation];
        
        // This will setup the app to get a location on every subsequent app foreground event.
        // The code above will now be active, and didFinishLaunchingWithOptions:launchOptions will
        // now generate a location event as well. This value is persisted.
        //locationService.automaticLocationOnForegroundEnabled = YES;
        //locationService.backgroundLocationServiceEnabled = YES;
    }
    
    // Send alias and tag info to UA
    //[[UAPush shared] updateRegistration];
    
    // We put our "on app open start location" code here so all location related methods never have to worry about whether or not the database is going to be available for use (prior to this sig change wakeup in the background was calling didUpdateLocations and that method was calling into core data but core data wasn't available yet and so we were crashing the app!! Starting location updates AFTER we have a database connection on app open solves all this. All location related method calls will be invoked after we have a db cnx making their code simpler and easier to write/understand.
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.locationManager startUpdatingLocation];
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
    
    // CHECK FOR ONE SIGNAL USER ID
    [OneSignal IdsAvailable:^(NSString* userId, NSString* pushToken) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"UserId:%@", userId);
            //[SWHelper helperShowAlertWithTitle:@"OS User Id" message:userId];
            NSString *pushTokenForServer = @"";
            if (pushToken != nil)
            {
                pushTokenForServer = pushToken;
                //NSLog(@"pushToken:%@", pushToken);
                //[SWHelper helperShowAlertWithTitle:@"OS Push Token" message:pushToken];
            }
			NSDictionary * dic = @{@"os_user_id":userId,@"os_push_token":pushTokenForServer};
			NSString *advertisingId = [self identifierForAdvertising];
			if ( ![advertisingId isEqual: [NSNull null]] && advertisingId != nil) {
				dic = @{@"os_user_id":userId,@"os_push_token":pushTokenForServer, @"maid":advertisingId};
			}
            [[SWHelper helperAppBackend] appBackendSetPushIdUsingArgs:dic completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[SWHelper helperShowAlertWithTitle:@"URL" message:requestURLAbsoluteString];
                    //[SWHelper helperShowAlertWithTitle:@"Response" message:responseBodyAsString];
                    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                });
            }];
        });
    }];
    
    // Broadcast notification to the rest of the app now that we've completed these important initialization steps...
    self.hasSentSWCoreDataManagedDocumentDidLoadNotificationKey = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWCoreDataManagedDocumentDidLoadNotificationKey object:self.managedDocument userInfo:nil];
}

- (NSString *)identifierForAdvertising {
		// Check whether advertising tracking is enabled
	if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
		NSUUID *identifier = [[ASIdentifierManager sharedManager] advertisingIdentifier];
		return [identifier UUIDString];
	}
	
		// Get and return IDFA
	return nil;
}

- (void)loadManagedDocumentWithNotificationPostedOnCompletion
{
    if (!self.managedDocument)
    {
        NSURL *url = [self managedDocumentURL];
        UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
        
        // Enable automatic migration
        NSDictionary *options = @{
                                  NSMigratePersistentStoresAutomaticallyOption : @YES,
                                  NSInferMappingModelAutomaticallyOption : @YES
                                  };
        document.persistentStoreOptions = options;
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]])
        {
            // Create it
            [document saveToURL:url forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
                if (success)
                {
                    self.managedDocument = document;
                    [self initializeManagedObjectContextDataThenBroadcastNotification];
                }
                else
                {
                    // Log error
                    [SWHelper helperLogErrorToAppBackendUsingMessage:@"Error" file:__FILE__ prettyFunction:__PRETTY_FUNCTION__ line:__LINE__];
                }
            }];
        }
        else if (document.documentState == UIDocumentStateClosed)
        {
            // Open it
            [document openWithCompletionHandler:^(BOOL success) {
                if (success)
                {
                    self.managedDocument = document;
                    [self initializeManagedObjectContextDataThenBroadcastNotification];
                }
                else
                {
                    // Log error
                    [SWHelper helperLogErrorToAppBackendUsingMessage:@"Error" file:__FILE__ prettyFunction:__PRETTY_FUNCTION__ line:__LINE__];
                }
            }];
        }
        else
        {
            // Use it
            self.managedDocument = document;
            [self initializeManagedObjectContextDataThenBroadcastNotification];
        }
    }
    else
    {
        // Use it
    }
}

- (void)handleWatchKitExtensionRequestNowThatCoreDataIsAvailable:(id)sender
{
    void (^blockNameIssueRequest)() = ^(double lat, double lon, NSString *locationName)
    {
        [[SWHelper helperAppBackend] appBackendGetSickScoreInRadiusForLat:lat lon:lon usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            
            // Define inline mapping data...
            NSArray *illnessIconMap = @[
                                        @{@"id":@"18",@"iconLetters":@"Al"},
                                        @{@"id":@"30",@"iconLetters":@"As"},
                                        @{@"id":@"2",@"iconLetters":@"Br"},
                                        @{@"id":@"10",@"iconLetters":@"Ch"},
                                        @{@"id":@"4",@"iconLetters":@"Co"},
                                        @{@"id":@"6",@"iconLetters":@"Cg"},
                                        @{@"id":@"14",@"iconLetters":@"Cr"},
                                        @{@"id":@"23",@"iconLetters":@"Ea"},
                                        @{@"id":@"11",@"iconLetters":@"Fe"},
                                        @{@"id":@"31",@"iconLetters":@"Fi"},
                                        @{@"id":@"1",@"iconLetters":@"Fl"},
                                        @{@"id":@"16",@"iconLetters":@"Ha"},
                                        @{@"id":@"25",@"iconLetters":@"Ma"},
                                        @{@"id":@"5",@"iconLetters":@"Na"},
                                        @{@"id":@"32",@"iconLetters":@"No"},
                                        @{@"id":@"22",@"iconLetters":@"Pi"},
                                        @{@"id":@"15",@"iconLetters":@"Pn"},
                                        @{@"id":@"33",@"iconLetters":@"Rs"},
                                        @{@"id":@"20",@"iconLetters":@"Si"},
                                        @{@"id":@"21",@"iconLetters":@"So"},
                                        @{@"id":@"24",@"iconLetters":@"Sv"},
                                        @{@"id":@"7",@"iconLetters":@"St"},
                                        @{@"id":@"3",@"iconLetters":@"Wh"}
                                        ];
            
            NSDictionary *responseDict = @{};
            if ([jsonResponseBody objectForKey:@"sickscore"])
            {
                NSNumber *sickScoreDouble = [jsonResponseBody objectForKey:@"sickscore"];
                NSNumber *sickScoreInt = [NSNumber numberWithInteger:[sickScoreDouble integerValue]];
                responseDict = @{
                                 @"sickscore": [NSString stringWithFormat:@"%@", sickScoreInt],
                                 @"location_name": locationName
                                 };
                if ([jsonResponseBody objectForKey:@"top_illnesses"])
                {
                    // Get illness lookup info...
                    NSString *path = [[NSBundle mainBundle] pathForResource:@"illnesses-data" ofType:@"json"];
                    NSData *illnessData = [NSData dataWithContentsOfFile:path];
                    
                    // Parse data as JSON
                    NSError *error;
                    NSArray *illnesses = [NSJSONSerialization JSONObjectWithData:illnessData options:kNilOptions error:&error];
                    
                    // Get log vars for loop
                    NSArray *topIllnessIDs = [NSArray new];
                    NSArray *topIllnessNames = [NSArray new];
                    NSArray *topIllnessIconLetters = [NSArray new];
                    for (NSNumber *topIllnessIdAsNumber in [jsonResponseBody objectForKey:@"top_illnesses"])
                    {
                        if ([topIllnessIdAsNumber isKindOfClass:[NSNumber class]])
                        {
                            topIllnessIDs = [topIllnessIDs arrayByAddingObject:[topIllnessIdAsNumber stringValue]];
                            for (NSDictionary *illnessDict in illnesses)
                            {
                                if ([[illnessDict objectForKey:@"id"] isEqualToString:[topIllnessIdAsNumber stringValue]])
                                {
                                    topIllnessNames = [topIllnessNames arrayByAddingObject:[illnessDict objectForKey:@"name"]];
                                    break;
                                }
                            }
                            for (NSDictionary *iconLettersToIdDict in illnessIconMap)
                            {
                                if ([[iconLettersToIdDict objectForKey:@"id"] isEqualToString:[topIllnessIdAsNumber stringValue]])
                                {
                                    topIllnessIconLetters = [topIllnessIconLetters arrayByAddingObject:[iconLettersToIdDict objectForKey:@"iconLetters"]];
                                    break;
                                }
                            }
                            
                        }
                    }
                    responseDict = @{
                                     @"sickscore": responseDict[@"sickscore"],
                                     @"location_name": responseDict[@"location_name"],
                                     @"top_illness_ids": topIllnessIDs,
                                     @"top_illness_names": topIllnessNames,
                                     @"top_illness_icon_letters": topIllnessIconLetters
                                     };
                }
            }
            self.handleWatchKitExtensionRequestReplyBlock(responseDict);
            [[UIApplication sharedApplication] endBackgroundTask:self.handleWatchKitExtensionRequestBackgroundId];
        }];
    };
    
    // Update current user object
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    if (currentUser)
    {
        // If we have access to location then we want to go direct and get the latest values
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
        {
            currentUser.userLastKnownCurrentLocationLatitude = [NSNumber numberWithDouble:[SWHelper helperLocationManager].location.coordinate.latitude];
            currentUser.userLastKnownCurrentLocationLongitude = [NSNumber numberWithDouble:[SWHelper helperLocationManager].location.coordinate.longitude];
            [self.geocodeManager reverseGeocodeLocation:[SWHelper helperLocationManager].location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *placemark = [placemarks lastObject];
                [self updateUserCurrentLocationPlacemarkUsingPlacemark:placemark];
                [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                                    object:self
                                                                  userInfo:@{@"placemark":placemark}];
                NSString *locationName = placemark.locality;
                blockNameIssueRequest([currentUser.userLastKnownCurrentLocationLatitude doubleValue], [currentUser.userLastKnownCurrentLocationLongitude doubleValue], locationName);
            }];
        }
        else
        {
            // We don't currently have access to location so let's see if we can rely on data previously logged to core data...
            if (currentUser.userLastKnownCurrentLocationPlacemark)
            {
                CLPlacemark *lastKnownCurrentLocationPlacemark = [currentUser userLastKnownCurrentLocationPlacemarkObject];
                NSString *locationName = lastKnownCurrentLocationPlacemark.locality;
                blockNameIssueRequest([currentUser.userLastKnownCurrentLocationLatitude doubleValue], [currentUser.userLastKnownCurrentLocationLongitude doubleValue], locationName);
            }
            else
            {
                // We don't have the user's location!!!
                NSDictionary *responseDict = @{
                                               @"sickscore": @"0",
                                               @"location_name": @"No Location",
                                               @"top_illness_ids": @[@"1",@"1",@"1"],
                                               @"top_illness_names": @[@"Flu",@"Flu",@"Flu"],
                                               @"top_illness_icon_letters": @[@"Fl",@"Fl",@"Fl"]
                                               };
                self.handleWatchKitExtensionRequestReplyBlock(responseDict);
                [[UIApplication sharedApplication] endBackgroundTask:self.handleWatchKitExtensionRequestBackgroundId];
            }
        }
    }
    else
    {
        // WHAT?!?!
        
        // We don't have a user!!
        NSDictionary *responseDict = @{
                                       @"sickscore": @"0",
                                       @"location_name": @"Error",
                                       @"top_illness_ids": @[],
                                       @"top_illness_names": @[],
                                       @"top_illness_icon_letters": @[]
                                       };
        self.handleWatchKitExtensionRequestReplyBlock(responseDict);
        [[UIApplication sharedApplication] endBackgroundTask:self.handleWatchKitExtensionRequestBackgroundId];
    }
}

#pragma mark - Lifecycle Methods

// Regular app launch or launching app from background via local notification (swipe to open)
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Define environment dependent variables
#if RELEASE
    self.flurryUniqueApplicationKey = @"7NWTZGSXWYMRFXD7XHDD";
    self.uaInProduction = YES;
    self.uaDevelopmentAppKey = @"";
    self.uaDevelopmentAppSecret = @"";
    self.uaProductionAppKey = @"C1qgwk9gTBuy6_uW3bE6MA";
    self.uaProductionAppSecret = @"-WHWklthRJmM4zhgfjJdKA";
    self.appBackendSickweatherAPIKey = @"oWCYcO9KTEdnMduhBE6NHcDdVxHAzGBN";
#elif DEBUG
    self.flurryUniqueApplicationKey = @"5R7J5F79ZZ3GBFDD3X75";
    self.uaInProduction = NO;
    self.uaDevelopmentAppKey = @"7EFB9LhpTnilGsZ1rTvaeg";
    self.uaDevelopmentAppSecret = @"J_cvvrUjTvepokYkrEYy9Q";
    self.uaProductionAppKey = @"9lp8_cxMTMKnBg21ANlRFA";
    self.uaProductionAppSecret = @"WoeYKUFaTrOSB8yqHhKLkw";
    self.appBackendSickweatherAPIKey = @"oWCYcO9KTEdnMduhBE6NHcDdVxHAzGBN";
#endif
    
    // UPDATE FLAG
    self.didFinishLaunchingWitOptions = YES;
    
    //SETUP CRASHLYTICS
    [Fabric with:@[[Crashlytics class]]];
    [self initializeXmodeSdkOnLaunch];
    
    // SETUP ONE SIGNAL OBJECT
    NSDictionary *settings = @{kOSSettingsKeyInAppAlerts:@NO,kOSSettingsKeyAutoPrompt:@NO};
    [OneSignal initWithLaunchOptions:launchOptions appId:@"bf015dd2-5718-477d-950e-87e1dbc0e015" handleNotificationReceived:^(OSNotification *notification) {
        //NSLog(@"In app push notification experience happening now");
        // This block gets called only if the user is in the app (foreground) and a push is sent to this device
        
        //NSLog(@"notification.stringify = %@", notification.stringify);
    } handleNotificationAction:^(OSNotificationOpenedResult *result) {
        // This block gets called on a hard app open when the user taps on the notification
        // Yes this does get executed on the main thread
        // No, the other side of this block doesn't get executed, only this one from a fresh app open
        //NSLog(@"Fresh app open due to tap on push notification experience is happening now");
        
        //NSLog(@"notification.stringify = %@", result.notification.stringify);
    } settings:settings];
    
    //SETUP GOOGLE PLACES
    [GMSPlacesClient provideAPIKey:@"AIzaSyBFYBPvy3H5hMh9ENYzocAvSNmTVcfKN5M"];
   
    // Printing Version Number
    //NSLog(@"Current Version %@", [SWHelper helperAppVersionString]);
    
    // Always setup app to receive incoming WatchConnectivity content
    // Check to make sure WatchConnectivity is supported (e.g. for iPad this will be false)
    if ([WCSession isSupported])
    {
        //NSLog(@"didFinishLaunchingWithOptions, [WCSession isSupported] == true");
        self.watchConnectivitySession = [WCSession defaultSession];
        self.watchConnectivitySession.delegate = self;
        [self.watchConnectivitySession activateSession];
        if (!self.watchConnectivitySession.paired)
        {
            // Nothing else for us to do! It's as if we're on an iPad at this point
            
            // If/when the user does pair an apple watch, we'll get the sessionWatchStateDidChange delegate callback notification with session.paired == true
            
            //NSLog(@"didFinishLaunchingWithOptions, !self.watchConnectivitySession.paired");
        }
        else
        {
            // There IS a paired watch for us to work with
            //NSLog(@"didFinishLaunchingWithOptions, self.watchConnectivitySession.paired, there IS a paired watch for us to work with");
            
            if (!self.watchConnectivitySession.watchAppInstalled)
            {
                // Our watch app isn't installed!
                
                //NSLog(@"didFinishLaunchingWithOptions, !self.watchConnectivitySession.watchAppInstalled");
            }
            else
            {
                // Our watch app is installed
                
                //NSLog(@"didFinishLaunchingWithOptions, self.watchConnectivitySession.watchAppInstalled == true");
            }
        }
    }
    else
    {
        //NSLog(@"didFinishLaunchingWithOptions, ![WCSession isSupported]");
    }
    
    // Call some basic helper functions on start up
    [self helperPreloadKeyboardToEliminateLagOnFirstTapLater];
    
    // Set certain "app open" flags...
    self.logLocationToFlurry = YES;
    
    // Configure Flurry
    [Flurry setCrashReportingEnabled:NO];
    [Flurry setAppVersion:[SWHelper helperAppVersionString]];
    [Flurry startSession:self.flurryUniqueApplicationKey];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]] length] > 0)
    {
        [Flurry setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]]];
    }
    
    // Register to receive notifications...
    
    // Tell us when the user's current location has been translated to a placemark object (if ever, permissions, etc)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUpdateCurrentLocationPlacemarkNotification:)
                                                 name:kSWDidUpdateCurrentLocationPlacemarkNotification
                                               object:nil];
    
    // SET UI BAR BUTTON ITEM APPEARANCE COLOR APP WIDE
    [[UIBarButtonItem appearance] setTintColor:[SWColor colorSickweatherBlue41x171x226]];
    
    // SET OUR CUSTOM FONT INTO ALL NAVIGATION CONTROLLER TITLES
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: [SWColor colorSickweatherStyleGuideGrayDark35x31x32],
                                                            NSFontAttributeName: [SWFont fontStandardBoldFontOfSize:16]
                                                            }];
    
    // BRAND TAB BAR USING SICKWEATHER BLUE
    [[UITabBar appearance] setTintColor:[SWColor colorSickweatherBlue41x171x226]];
    
    // Create in-memory, on disk cache
    NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:[[SWSettings sharedInstance] cacheMemoryCapacity] diskCapacity:[[SWSettings sharedInstance] cacheDiskCapacity] diskPath:[[SWSettings sharedInstance] cacheDiskPath]];
    [NSURLCache setSharedURLCache:cache]; // Set cache as app's shared cache
    [NSURLCache sharedURLCache]; // Read to scan disk
    sleep(1); // Let call to scan disk finish and properly init
    [[NSURLCache sharedURLCache] removeAllCachedResponses]; // Clear cache on every app open to clear out "clear 200OK png images" when the DB sync fails and there are no markers for nate's script to process
    
    // Register app defaults
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{
                                                              [SWConstants hasPassedWelcomeToSickweatherSlideThroughScreensDefaultsKey]: @NO,
                                                              [SWConstants hasPassedInitialAlertsSetupScreenDefaultsKey]: @NO,
                                                              [SWConstants hasDismissedLoginScreenUserDefaultsKey]: @NO,
                                                              [SWConstants masterSwitchForSickweatherProximityBasedIllnessAlertsIsOnDefaultsKey]: @NO,
                                                              [SWConstants hasBeenTippedAboutGeofenceOnButNoIllnessesSelectedDefaultsKey]: @NO
                                                              }];
    
    // Backwards compatibility w/ already released versions where user has already logged in...
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]] length] > 0)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[SWConstants hasDismissedLoginScreenUserDefaultsKey]];
    }
    
    // SETUP APP BACKEND
    self.sickweatherAppBackend = [[SWAppBackend alloc] init];
    self.sickweatherAppBackend.appBackendSickweatherAPIKey = self.appBackendSickweatherAPIKey;
    
    // FETCH ALL ILLNESSES FROM BACKEND, SAVE TO LOCAL FILE (EVERYBODY ELSE CAN JUST READ FROM THAT FILE, REFRESHED HERE ON APP OPEN)
    [[SWHelper helperAppBackend] appBackendGetIllnessesUsingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SWHelper helperSaveJSONString:responseBodyAsString toDocsUsingName:@"my-network-illnesses"];
            
            // Setting default values if none exist
            [self parseIllnessesWithJsonResponseBody:jsonResponseBody];
            
        });
    }];
    
    [[SWHelper helperAppBackend] appBackendGetCovidIllnessesUsingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SWHelper helperSaveJSONString:responseBodyAsString toDocsUsingName:@"my-network-trending_keywords"];
            
            // Setting default values if none exist
            [self parseIllnessesWithJsonResponseBody:jsonResponseBody];
            
        });
    }];
    
    // Check for open due to universal link
    NSDictionary *activityDic = [launchOptions objectForKey:UIApplicationLaunchOptionsUserActivityDictionaryKey];
    if (activityDic)
    {
        NSUserActivity *userActivity = [activityDic valueForKey:@"UIApplicationLaunchOptionsUserActivityKey"];
        if ([userActivity.activityType isEqualToString: NSUserActivityTypeBrowsingWeb])
        {
            NSURL *url = userActivity.webpageURL;
            self.customDeepLinkURL = url;
            [self helperInterpretCustomDeepLinkURL:NO];
        }
    }
    
    // Log customer URL as prop if one is found
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsURLKey])
    {
        self.customDeepLinkURL = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    }
    
    // Significant location change is waking us up
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey])
    {
        //NSLog(@"Nothing special we have to do...");
    }
    
    // INIT SW SNOW SDK
    [SWAnalytics initWithApiKey:@"b3N9QQS6C$RjkT..=e|jM\"pO21\"Yv1"];
    
    // Check if we have SW user ID on file, make use of it if so...
    NSString *currentUserSWId = [SWHelper helperGetCurrentUserSWIdFromDefaults];
    if ([currentUserSWId length] > 0)
    {
        //[UAPush shared].alias = currentUserSWId;
        [OneSignal sendTag:@"user-sickweather-id" value:currentUserSWId];
        
        // LOG USER IN
        [SWAnalytics setUserWithUserId:currentUserSWId];
    }
    
    // Set Snow OS User Id
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        [SWAnalytics setOSUserWithOsUserId:userId];
    }];
    
    // Load local database
    [self loadManagedDocumentWithNotificationPostedOnCompletion];
    
    // Create images (comment out to use in a one-off manner for development)
    //[SWHandWashTimerImageGenerator createImages];
    
    //NSLog( @"### Running FB SDK Version: %@", [FBSDKSettings sdkVersion]);
    
    // Finally, return by calling the FBSDK's didFinishLaunchingWithOptions method, this is required for the proper use of the SDK
    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}

- (void)parseIllnessesWithJsonResponseBody: (NSArray *) jsonResponseBody {
    NSArray *individualIllnesses = @[];
    if(jsonResponseBody && [jsonResponseBody isKindOfClass:[NSArray class]]){
        for (NSDictionary *illness in jsonResponseBody)
            {
            @try
                {
                if ([illness isKindOfClass:[NSDictionary class]] && [illness isKindOfClass:[NSDictionary class]] && [illness objectForKey:@"id"] != nil && ![[illness objectForKey:@"id"] isEqual:[NSNull null]] && ![[illness objectForKey:@"id"] containsString:@","]){
                        // IS NOT GROUP, IS INDIVIDUAL, ADD
                    individualIllnesses = [individualIllnesses arrayByAddingObject:[illness objectForKey:@"id"]];
                }
            }
            @catch (NSException * e) {
                [SWHelper addNonFatalExceptionWith:e];
            }
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // If master alert setting switch is not assigned, set it on for new app install
    NSString *alertMasterKey = @"proximity_alerts_are_on_yes_no";
    if ([defaults stringForKey:alertMasterKey] == nil)
    {
        [defaults setObject:@"yes" forKey:alertMasterKey];
    }
    
    
    // If individual alert settings are not assigned, set them on for new app install
    for (NSString *individualIllnessId in individualIllnesses)
    {
        NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_for_id_%@_yes_no", individualIllnessId];
        if ([defaults stringForKey:defaultsKey] == nil)
        {
            [defaults setObject:@"yes" forKey:defaultsKey];
        }
    }
    [defaults synchronize];
}

-(void)initializeXmodeSdk{
    // Initialize XDK (GJPG) if user give permission
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) &&
        ([defaults valueForKey:@"IS_CONSENT_ASKED"] && [defaults valueForKey:@"CONSENTAGREE"])) {
        NSLog(@"Xmode Start with api key called");
        [[GJPG sharedInstance] startWithApiKey:@"85f37285fdae4c74b0f4828ac87db402"];
    }
    
}

-(void)initializeXmodeSdkOnLaunch{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
        && (![defaults valueForKey:@"IS_CONSENT_ASKED"])) {
        NSLog(@"Xmode Initial Start with api key called");
        [[GJPG sharedInstance] startWithApiKey:@"85f37285fdae4c74b0f4828ac87db402"];
    }
}
    
- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    //NSLog(@"identifier = %@", identifier);
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
    if ([userActivity.activityType isEqualToString: NSUserActivityTypeBrowsingWeb])
    {
        NSURL *url = userActivity.webpageURL;
        self.customDeepLinkURL = url;
        if (self.hasSentSWCoreDataManagedDocumentDidLoadNotificationKey)
        {
        [self helperInterpretCustomDeepLinkURL:YES];
        }
        return YES;
    }
    return NO;
}

// If app is in foreground, the following method is called
// If app is in backbround, didFinishLaunchingWithOptions is called with UIApplicationLaunchOptionsLocalNotificationKey in options dictionary
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if ([[notification.userInfo objectForKey:@"handleWatchKitExtensionRequestMethod"] isEqualToString:@"presentLocalNotificationForHandWashTimerComplete"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Timer Done" message:@"Hand Wash Timer Complete" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

// During the Facebook login flow, your app passes control to the Facebook iOS app or Facebook in a mobile browser.
// After authentication, your app will be called back with the session information.
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BFURL *parsedUrl = [BFURL URLWithInboundURL:url sourceApplication:sourceApplication];
    if ([parsedUrl appLinkData])
    {
        /*
        // this is an applink url, handle it here
        NSURL *targetUrl = [parsedUrl targetURL];
        [[[UIAlertView alloc] initWithTitle:@"Received link:"
                                    message:[targetUrl absoluteString]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
         */
    }
    else
    {
        //NSLog(@"is NOT an app link url...");
        self.customDeepLinkURL = url;
    [self helperInterpretCustomDeepLinkURL:YES];
    }
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    // Save the database (and delete any objects causing our save to fail, up to 20)
    NSError *error = nil;
    NSUInteger numberOfTrys = 0;
    while (![[SWHelper helperManagedObjectContext] save:&error] && numberOfTrys < 20)
    {
        // An error was encountered when trying to save
        //NSLog(@"helperManagedObjectContext] save:&error] = %@", error);
        
        // Increment
        numberOfTrys++;
        
        // Look and see if you can find the problem object (and delete to fix db state issues)
        @try
        {
            NSManagedObject *problemObject = [[error userInfo] objectForKey:@"NSValidationErrorObject"];
            if (problemObject)
            {
                //NSLog(@"We WERE able to find a NSValidationErrorObject problemObject, and we deleted it.");
                [[SWHelper helperManagedObjectContext] deleteObject:problemObject];
            }
            else
            {
                //NSLog(@"We were unable to find a NSValidationErrorObject problemObject.");
            }
        }
        @catch (NSException *exception)
        {
            //NSLog(@"exception.reason = %@", exception.reason);
        }
        @finally
        {
            //NSLog(@"Finally block...");
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSUserDefaults standardUserDefaults] synchronize]; // Catch all
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    // Cancel any outstanding notifications (yes, on every launch)
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // Clear badge of count (yes, on every launch)
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        [[SWHelper helperLocationManager] startUpdatingLocation];
        [[SWHelper helperLocationManager] startMonitoringSignificantLocationChanges];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Cancel any outstanding notifications (yes, on every launch)
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // Clear badge of count (yes, on every launch)
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    // App Events let you measure installs on your mobile app ads, create high value audiences for targeting, and view analytics uncluding user demographics.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
