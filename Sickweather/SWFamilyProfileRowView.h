//
//  SWFamilyProfileRowView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWFamilyProfileRowView;

@protocol SWFamilyProfileRowViewDelegate
- (NSString *)familyProfileRowViewTitleString:(SWFamilyProfileRowView *)sender;
- (UIImage *)familyProfileRowViewIconImage:(SWFamilyProfileRowView *)sender;
- (void)familyProfileRowViewWasTapped:(SWFamilyProfileRowView *)sender;
@end

@interface SWFamilyProfileRowView : UIView
@property (nonatomic, weak) id<SWFamilyProfileRowViewDelegate> delegate;
- (void)showBottomBorder;
- (void)setShowDisclosureIndicatorIcon;

@end
