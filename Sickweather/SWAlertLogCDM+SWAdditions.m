//
//  SWAlertLogCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 4/11/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWAlertLogCDM+SWAdditions.h"

@implementation SWAlertLogCDM (SWAdditions)

+ (SWAlertLogCDM *)getAlertLogObjectForReportId:(NSString *)reportId
{
    // Define return var
    SWAlertLogCDM *alertLogObject = nil;
    
    // Create and config database request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWAlertLogCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"reportId" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"reportId = %@", reportId];
    
    // Execute fetch request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    if (!matches)
    {
        // Error
    }
    else if ([matches count] == 0)
    {
        // Found nothing, return nil
    }
    else
    {
        // Found match, return it
        alertLogObject = [matches lastObject];
    }
    
    // Return object
    return alertLogObject;
}

+ (SWAlertLogCDM *)createAlertLogObjectThatWeHaveStartedRegionMonitoringForForReport:(NSDictionary *)report region:(CLRegion *)region
{
    // Check return existing based on id (prevent dup log files based on region.identifier/reportId)
    SWAlertLogCDM *alertLog = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SWAlertLogCDM"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"reportId = %@", region.identifier];
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:fetchRequest error:&error];
    if ([matches count]) alertLog = [matches lastObject];
    if (alertLog) return alertLog; // Returns
    
    // Create new
    alertLog = [NSEntityDescription insertNewObjectForEntityForName:@"SWAlertLogCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
    NSData *reportAsData = [NSKeyedArchiver archivedDataWithRootObject:report];
    NSData *regionAsData = [NSKeyedArchiver archivedDataWithRootObject:region];
    alertLog.reportId = region.identifier;
    alertLog.reportDictionaryData = reportAsData;
    alertLog.regionObjectData = regionAsData;
    return alertLog; // Returns
}

+ (NSDictionary *)reportForRegionId:(NSString *)regionId
{
    NSDictionary *report = nil;
    SWAlertLogCDM *alertLog = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SWAlertLogCDM"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"reportId = %@", regionId];
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:fetchRequest error:&error];
    if ([matches count]) alertLog = [matches lastObject];
    if (alertLog)
    {
        report = [NSKeyedUnarchiver unarchiveObjectWithData:alertLog.reportDictionaryData];
    }
    return report;
}

+ (NSMutableArray *)regionsAlreadyFetchedFromWebServiceThatWeHaveStartedMonitoringFor
{
    NSMutableArray *regionsEntered = [NSMutableArray new];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SWAlertLogCDM"];
    NSArray *alerts = [[SWHelper helperManagedObjectContext] executeFetchRequest:fetchRequest error:NULL];
    for (SWAlertLogCDM *alert in alerts)
    {
        [regionsEntered addObject:[alert regionObject]];
    }
    return regionsEntered;
}

+ (NSMutableArray *)reportsAlreadyFetchedFromWebServiceThatWeHaveStartedMonitoringFor
{
    NSMutableArray *reportsEntered = [NSMutableArray new];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SWAlertLogCDM"];
    NSArray *alerts = [[SWHelper helperManagedObjectContext] executeFetchRequest:fetchRequest error:NULL];
    for (SWAlertLogCDM *alert in alerts)
    {
        [reportsEntered addObject:[alert reportDictionary]];
    }
    return reportsEntered;
}

- (CLRegion *)regionObject
{
    CLRegion *regionAsObjectFromData = [NSKeyedUnarchiver unarchiveObjectWithData:self.regionObjectData];
    return regionAsObjectFromData;
}

- (NSDictionary *)reportDictionary
{
    NSDictionary *reportAsObjectFromData = [NSKeyedUnarchiver unarchiveObjectWithData:self.reportDictionaryData];
    return reportAsObjectFromData;
}

@end
