//
//  SWAlertsFamilyItemTableViewCell.h
//  Sickweather
//
//  Created by John Erck on 12/12/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAlertsFamilyItemTableViewCell : UITableViewCell
- (void)updateForDict:(NSDictionary *)dict;
@end
