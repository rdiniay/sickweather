//
//  SWGroupsViewController.h
//  Sickweather
//
//  Created by John Erck on 8/21/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SEARCH_SECTION_HEADER_INDEX 0
#define MY_GROUPS_SECTION_HEADER_INDEX 1
#define GROUPS_AROUND_YOU_SECTION_HEADER_INDEX 2

@interface SWGroupsViewController : UIViewController
- (void)loadGroupAndPresentProfilePageForSWFoursquareGroupId:(NSString *)swFoursquareGroupId;
- (void)loadGroupAndPushProfilePageForSWFoursquareGroupId:(NSString *)swFoursquareGroupId;
@end
