//
//  SWAddNoteViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 1/31/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"
@interface SWAddNoteViewController : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL isFromFamilyScreen;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@end
