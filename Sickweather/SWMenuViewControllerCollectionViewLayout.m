//
//  SWMenuViewControllerCollectionViewLayout.m
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "SWMenuViewControllerCollectionViewLayout.h"
#import "NSAttributedString+Height.h"
#import "SWMenuViewController.h"

#define REGULAR_ROW_HEIGHT 30

@interface SWMenuViewControllerCollectionViewLayout ()
@property (strong, nonatomic) NSNumber *currentYOffset;
@property (strong, nonatomic) NSDictionary *layoutInformation;
@end

@implementation SWMenuViewControllerCollectionViewLayout

#pragma mark - Helpers

- (CGFloat)availableWidth
{
    return self.collectionView.bounds.size.width;
}

#pragma mark - Life Cycle Methods

// 1) Use the prepareLayout method to perform the up-front calculations needed to provide layout information.
// Note) At a minimum, you should compute enough information in this method to be able to return the overall size of the content area, which you return to the collection view in step 2.
- (void)prepareLayout
{
    // Init helper vars
    self.currentYOffset = @0;
    NSInteger currentYOffset = 0;
    NSMutableDictionary *layoutInformation = [NSMutableDictionary dictionary];
    NSMutableDictionary *supplementaryViewInformation = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellInformation = [NSMutableDictionary dictionary];
    NSIndexPath *indexPath;
    NSInteger numSections = [self.collectionView numberOfSections];
    for (NSInteger section = 0; section < numSections; section++)
    {
        NSInteger numItems = [self.collectionView numberOfItemsInSection:section];
        for (NSInteger item = 0; item < numItems; item++)
        {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            CGFloat totalExtraTitleMarginNeeded = [self.delegate totalExtraTitleMarginNeededForIndexPath:indexPath];
            CGFloat headerRowHeight = [self.delegate sectionHeaderPreferredVerticalHeightForIndexPath:indexPath];
            CGFloat rowHeight = REGULAR_ROW_HEIGHT;
            if (item == 0)
            {
                
                UICollectionViewLayoutAttributes *headerAttributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader withIndexPath:indexPath];
                headerAttributes.frame = CGRectMake(0, currentYOffset + 0, [self availableWidth], headerRowHeight);
                [supplementaryViewInformation setObject:headerAttributes forKey:indexPath];
                currentYOffset += headerRowHeight;
            }
            UICollectionViewLayoutAttributes *cellAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            NSAttributedString *attrStringForIndexPath = [self.delegate menuViewControllerCollectionViewLayoutDelegateAttributedTitleStringForStandardItemCollectionViewCell:indexPath];
            //NSLog(@"attrStringForIndexPath = %@", attrStringForIndexPath);
            CGFloat attrStringHeight = [attrStringForIndexPath heightForWidth:[self availableWidth]-totalExtraTitleMarginNeeded];
            //NSLog(@"attrStringHeight = %f", attrStringHeight);
            cellAttributes.frame = CGRectMake(0, currentYOffset + 0, [self availableWidth], attrStringHeight + rowHeight);
            [cellInformation setObject:cellAttributes forKey:indexPath];
            currentYOffset += cellAttributes.frame.size.height;
        }
    }
    [layoutInformation setObject:supplementaryViewInformation forKey:UICollectionElementKindSectionHeader];
    [layoutInformation setObject:cellInformation forKey:@"SWChecklistCollectionViewCell"];
    
    // Cache as props
    self.currentYOffset = [NSNumber numberWithInteger:currentYOffset];
    self.layoutInformation = layoutInformation;
}

// 2) Use the collectionViewContentSize method to return the overall size of the entire content area based on your initial calculations.
- (CGSize)collectionViewContentSize
{
    return CGSizeMake([self availableWidth], [self.currentYOffset floatValue]);
}

// 3) Use the layoutAttributesForElementsInRect: method to return the attributes for cells and views that are in the specified rectangle (basis of all layout right here).
- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *myAttributes = [NSMutableArray arrayWithCapacity:self.layoutInformation.count];
    for (NSString *key in self.layoutInformation)
    {
        NSDictionary *attributesDict = [self.layoutInformation objectForKey:key];
        for (NSIndexPath *key in attributesDict)
        {
            UICollectionViewLayoutAttributes *attributes = [attributesDict objectForKey:key];
            if (CGRectIntersectsRect(rect, attributes.frame))
            {
                [myAttributes addObject:attributes];
            }
        }
    }
    return myAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInformation[@"SWChecklistCollectionViewCell"][indexPath];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInformation[elementKind][indexPath];
}

static NSNumber *lastNewBoundsWidth;
- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    BOOL answer = NO;
    if (!lastNewBoundsWidth)
    {
        lastNewBoundsWidth = [NSNumber numberWithFloat:newBounds.size.width];
    }
    if ([lastNewBoundsWidth floatValue] != newBounds.size.width)
    {
        answer = YES;
    }
    else
    {
        answer = NO;
    }
    //NSLog(@"lastNewBoundsWidth = %@", lastNewBoundsWidth);
    //NSLog(@"newBoundsWidth = %@", [NSNumber numberWithFloat:newBounds.size.width]);
    lastNewBoundsWidth = [NSNumber numberWithFloat:newBounds.size.width];
    return answer;
}

/*
 layoutAttributesForSupplementaryViewOfKind:atIndexPath: (if your layout supports supplementary views)
 
 layoutAttributesForDecorationViewOfKind:atIndexPath: (if your layout supports decoration views)
 

 */

- (id)init
{
    if (self = [super init])
    {
        // Set props here later if needed
    }
    return self;
}

@end
