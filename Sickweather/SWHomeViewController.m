//
//  SWHomeViewController.m
//  Sickweather
//
//  Created by John Erck on 7/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWHomeViewController.h"
#import "Flurry.h"
#import <OneSignal/OneSignal.h>
#import "SWNavigationController.h"
#import "SWHomeTrendingIllnessRecordView.h"
#import "SWHomeForecastChartView.h"
#import "SWNewMapViewController.h"
#import "SWHomePageView.h"
#import "SWHomePageAddView.h"
#import "SWNewIllnessDetailViewController.h"
#import "Flurry.h"
#import "SWFeedbackView.h"
#import "SWWebViewViewController.h"
#import "SWConnectWithDoctorViewController.h"
#import "SWOnboardingViewController.h"
#import "SWMenuViewController.h"
#import "SWGroupsViewController.h"
#include <GJPG/GJPG.h>
#import "SWAppDelegate.h"

@interface SWHomeViewController () <UIScrollViewDelegate, SWHomePageAddViewDelegate, SWHomePageViewDelegate, SWNewMapViewControllerDelegate, UITabBarControllerDelegate, SWFeedbackViewDelegate, SWWebViewViewControllerDelegate, SWOnboardingViewControllerDelegate>

// NETWORK DATA
@property (strong, nonatomic) NSDictionary *latestSickScoreResponse;

// VIEW DATA
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIPageControl *sickScorePageControl;
@property (strong, nonatomic) UIScrollView *scrollViewVerticalContainer;
@property (strong, nonatomic) UIScrollView *scrollViewHorizontalContainer;
@property (strong, nonatomic) NSMutableDictionary *homePageNetworkResponses;
@property (strong, nonatomic) NSArray *homePageViews;
@property (strong, nonatomic) NSArray *homePageViewConstraints;
@property (strong, nonatomic) SWHomePageView *currentHomeScreen;
@property (strong, nonatomic) SWFeedbackView *feedbackView;
@property (strong, nonatomic) UIView *feedbackCancelView;
@property (strong, nonatomic) CLPlacemark *currentPlacemark;
@end

@implementation SWHomeViewController

- (void)showGroupUsingSWFoursquareId:(NSString *)swFoursquareId
{
	if ([[SWHelper helperGetTab1SlidingViewController].underLeftViewController isKindOfClass:[SWNavigationController class]]){
		UITabBarController *tabBar =  [SWHelper helperGetTabBarSlidingViewController];
		tabBar.selectedIndex = 4;
		if ([[[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]] length] > 0){
			UINavigationController *swSlidingVC = (UINavigationController*)[tabBar.viewControllers objectAtIndex:4];
			if ([swSlidingVC isKindOfClass:[UINavigationController class]]){
				SWGroupsViewController *groupVC = (SWGroupsViewController *)swSlidingVC.topViewController;
				if ([groupVC isKindOfClass:[SWGroupsViewController class]]){
					[groupVC loadGroupAndPushProfilePageForSWFoursquareGroupId:swFoursquareId];
				}
			}
		}
	}
}

#pragma mark - SWHomePageAddViewDelegate

-(void)duplicationPlaceError:(NSString *)message{
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
	UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
	[alert addAction:ok];
	[self presentViewController:alert animated:YES completion:nil];
}

- (void)homePageAddViewAddedLocation:(id)sender
{
    [self refreshHomePagesUsingMethod:@"refreshHomePagesHitNetworkOnlyIfNoCache"];
}

- (void)homePageAddViewDeletedLocation:(id)sender
{
    [self refreshHomePagesUsingMethod:@"refreshHomePagesDoNotHitNetwork"];
}
    
- (void)homePageAddViewReorderedLocations:(id)sender
{
    [self refreshHomePagesUsingMethod:@"refreshHomePagesDoNotHitNetwork"];
}

#pragma mark - SWNewMapViewControllerDelegate

#pragma mark - SWHomePageViewDelegate

- (void)homePageViewWantsToLoadTrendingMap:(id)sender
{
    if ([sender isKindOfClass:[SWHomePageView class]])
    {
        self.currentHomeScreen = sender;
        BOOL isCovidScore = [self.currentHomeScreen.currentObjectForKey isEqual:@"cvscore"];
        NSDictionary *dataDict = [self.currentHomeScreen getDataDict];
        NSArray *illnessQueryIds = @[@"33",@"1",@"2",@"4",@"7",@"15"]; // Respiratory
        //NSString * objectForKey = @"illnesses";
        NSString * objectForKey = isCovidScore ? @"trending_keywords" : @"illnesses";
        if ([[dataDict objectForKey:objectForKey] isKindOfClass:[NSArray class]])
        {
            illnessQueryIds = @[];
            for (NSDictionary *illness in [dataDict objectForKey:objectForKey])
            {
                //NSLog(@"illness = %@", illness);
                illnessQueryIds = [illnessQueryIds arrayByAddingObject:[illness objectForKey:@"illness_id"]];
            }
            illnessQueryIds = [dataDict objectForKey:@"top_illnesses"];
        }
        SWNewMapViewController *newMapVC = [[SWNewMapViewController alloc] init];
        newMapVC.delegate = self;
        newMapVC.isCovidScore = isCovidScore;
        [newMapVC updateForIllnessIDs:illnessQueryIds andLocation:[self.currentHomeScreen getLocation]];
        [self.navigationController pushViewController:newMapVC animated:YES];
    }
}

- (void)homePageViewTrendingIllnessRecordTapped:(id)sender
{
    if ([sender isKindOfClass:[SWHomePageView class]])
    {
        self.currentHomeScreen = sender;
        BOOL isCovidScore = [self.currentHomeScreen.currentObjectForKey isEqual:@"cvscore"];
        
        NSDictionary *rowDataDict = [self.currentHomeScreen getLastSelectedRowDataDict];
        if ([[rowDataDict objectForKey:@"illness_id"] isKindOfClass:[NSString class]])
        {
            NSString *illnessId = [rowDataDict objectForKey:@"illness_id"];
            /*
            SWNewMapViewController *newMapVC = [[SWNewMapViewController alloc] init];
            newMapVC.delegate = self;
            [newMapVC updateForIllnessIDs:@[illnessId] andLocation:[self.currentHomeScreen getLocation]];
            [self.navigationController pushViewController:newMapVC animated:YES];
            */
             NSString *illness_name = [rowDataDict objectForKey:@"illness_name"];
			if(illness_name != nil && ![illness_name isEqual:[NSNull null]] && ![illness_name isEqualToString:@""]){
				[SWHelper logFlurryEventsWithTag:[NSString stringWithFormat:@"Trending: %@",[illness_name capitalizedString]]];
			}
            // SHOW NEW ILLNESS DETAIL VIEW CONTROLLER
            SWNewIllnessDetailViewController *newIllnessDetailVC = [[SWNewIllnessDetailViewController alloc] init];
            //detailVC.mapScreenshotImage = croppedImage;
            //detailVC.customMapKitAnnotation = annotation;
            newIllnessDetailVC.illnessName = illness_name;
            newIllnessDetailVC.isCovidScore = isCovidScore;
            [newIllnessDetailVC updateForIllnessId:illnessId andLocation:[self.currentHomeScreen getLocation]];
            [self.navigationController pushViewController:newIllnessDetailVC animated:YES];
        }
    }
}

- (void)showConsentNoticeAlert:(NSNotification *)notification
{
    // todo: add first time check for those users who already have app on their device
    SWAppDelegate *appDelegate = (SWAppDelegate*) [UIApplication sharedApplication].delegate;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     if ([[notification name] isEqualToString:@"showConsentNoticeAlertIfLocationAvailable"] && ![defaults valueForKey: @"CONSENTAGREE"])
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Consent Notice" message:@"We at Sickweather think consumer access to the population health data that we collect is important, and we appreciate your support.  To afford keeping Sickweather accessible to consumers, we collect and share ad IDs and de-identified location data with third parties to help them conduct market research, traffic and health research, and ad customization. By proceeding you agree to the processing of personal data as described in our partners privacy notices. You can go to your device settings at any time to withdraw (or deny) your consent." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes, I Agree" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                // Do nothing, they cancelled out
                [defaults setBool:YES forKey:@"CONSENTAGREE"];
                [defaults setBool:YES forKey:@"IS_CONSENT_ASKED"];
                [appDelegate initializeXmodeSdk];
                if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways){
                    [self performSelector:@selector(getAlwaysPermission) withObject:nil afterDelay:1.0 ];
                }
            }];
            [alert addAction:ok];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No, Thank You" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                // Do nothing, they cancelled out
                [defaults setBool:YES forKey:@"CONSENTDISAGREE"];
                [defaults setBool:YES forKey:@"IS_CONSENT_ASKED"];
                [self performSelector:@selector(getAlwaysPermission) withObject:nil afterDelay:1.0 ];
            }];
            [alert addAction:cancel];
            UIAlertAction *privacyPolicy = [UIAlertAction actionWithTitle:@"Privacy Policy" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                // Do nothing, they cancelled out
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://xmode.io/privacy-policy.html"]];
            }];
            [alert addAction:privacyPolicy];
            [self presentViewController:alert animated:YES completion:^{}];
        }
}

- (void)getAlwaysPermission {
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
    {
        [[SWHelper helperLocationManager] requestAlwaysAuthorization];
    }
}
    
- (void)homePageViewTurnLocationOnButtonTapped:(id)sender
{
    /*
     AS OF IOS 11 AND FOR THE FORESEEABLE FUTURE, THE WAY TO ASK FOR LOCATION
     IS JUST LIKE YOU WOULD EXPECT, A LITTLE BIT AT A TIME, AND ASK FOR THE LEAST
     THING FIRST IN ORDER TO STEP OUR WAY INTO ALWAYS ACCESS. THIS IS WHAT PEOPLE
     ARE SAYING IS THE BEST, AND HAS THE ADDED BENEFIT OF NOT ALLOWING USERS TO DENY
     WHEN THEY ARE PRESENTED WITH THE ALWAYS REQUEST (OUR 2ND LOCATION PROMPT).
     */
    SWAppDelegate *appDelegate = (SWAppDelegate*) [UIApplication sharedApplication].delegate;
    // STEP 1: SEE IF WE HAVE EVER ASKED
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        // FIRST TIME USER SCENARIO, START BY ASKING JUST FOR WHEN IN USE
        [[[SWHelper helperAppDelegate] locationManager] requestWhenInUseAuthorization];
    }
    // STEP 2: SEE IF WE'VE BEEN FULLY DENIED
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        // YOU SIMPLY CANNOT ASK AGAIN ONCE YOU HAVE ASKED THE ONE TIME. YOU ARE DONE.
        //[[[SWHelper helperAppDelegate] locationManager] requestWhenInUseAuthorization]; <-- TE
        
        // FOR SOME REASON, THE USER HAS DECIDED TO REJECT US ACCESS TO THEIR LOCATION
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Turn Location On" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:actionOK];
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:^{}];
    }
    // STEP 3: SEE IF WE ONLY HAVE WHEN IN USE
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        // THIS IS IT, THIS IS OUR CHANCE TO NOW GO FOR ALWAYS ACCESS
        [appDelegate initializeXmodeSdk];
        [[[SWHelper helperAppDelegate] locationManager] requestAlwaysAuthorization];
    }
    // STEP 4: CHECK AND SEE IF WE ARE ALREADY RUNNING ON ALWAYS
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // BOOM SUCKA! IT'S ALREADY ONLINE!
    }
}

- (void)homePageViewManuallySearchForLocationButtonTapped:(id)sender
{
    NSLog(@"homePageViewManuallySearchForLocationButtonTapped");
    CGFloat newX = ([self.homePageViews count]-1) * self.scrollViewHorizontalContainer.bounds.size.width;
    [self.scrollViewHorizontalContainer scrollRectToVisible:CGRectMake(newX, 0, self.scrollViewHorizontalContainer.bounds.size.width, self.scrollViewHorizontalContainer.bounds.size.height) animated:YES];
    SWHomePageAddView *addView = [self.homePageViews lastObject];
    if ([addView isKindOfClass:[SWHomePageAddView class]])
    {
        [addView activateKeyboard];
    }
}

#pragma mark - UITabBarControllerDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if ([viewController isEqual:[tabBarController.viewControllers objectAtIndex:0]])
    {
	
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.scrollViewHorizontalContainer scrollRectToVisible:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) animated:YES];
    }
}

#pragma mark - Helpers

- (void)checkLocationUI
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // Make sure no location is hidden
        SWHomePageView *homepage = [self.homePageViews objectAtIndex:0];
        if ([homepage isKindOfClass:[SWHomePageView class]])
        {
            [homepage setupAsHasLocationUI];
        }
    }
    else
    {
        SWHomePageView *homepage = [self.homePageViews objectAtIndex:0];
        if ([homepage isKindOfClass:[SWHomePageView class]])
        {
            [homepage setupAsNoLocationUI];
        }
    }
}

- (void)refreshHomePages
{
    //[self refreshHomePagesUsingMethod:@"refreshHomePagesDoNotHitNetwork"];
    [self refreshHomePagesUsingMethod:@"refreshHomePagesForceNetwork"];
}

- (void)refreshHomePagesUsingMethod:(NSString *)method
{
    // FIRST THINGS FIRST, CLEAN UP OLD HOME SCREENS ENTIRELY
    if (!self.homePageViews) {self.homePageViews = @[];}
    if (!self.homePageViewConstraints) {self.homePageViewConstraints = @[];}
    [self.view removeConstraints:self.homePageViewConstraints];
    self.homePageViewConstraints = @[];
    for (UIView *view in self.homePageViews)
    {
        [view removeFromSuperview];
    }
    self.homePageViews = @[]; // DONE WITH THEM
    
    // NOW ADD "CURRENT LOCATION" PAGE (ALWAYS THE FIRST HOME PAGE SCREEN FOR ALL USERS)
    
    // INIT ALL REQUIRED UI ELEMENTS (AND ADD TO RUNNING LIST OF VIEW OBJECTS)
    SWHomePageView *currentLocationHomePage = [[SWHomePageView alloc] init];
    
    // CHECK CURRENT CURRENT LOCATION STATUS & CONFIGURE HOME PAGE ACCORDINGLY
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // WE DO NOW HAVE ALWAYS
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        // WE DO NOW HAVE ACCESS TO LOCATION, BUT ONLY WHILE USING THE APP
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        // WE HAVE NEVER ASKED, SO WE ASK WITH OUR OWN UI BUTTON WITH TITLE, TAP ME!
        [currentLocationHomePage setupAsNoLocationUI];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        // WE ASKED, AND THEY SAID NO!
        [currentLocationHomePage setupAsNoLocationUI];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        // THE USER IS RESTRICTED FROM ALLOWING LOCATION ON THIS DEVICE!
        [currentLocationHomePage setupAsNoLocationUI];
    }
    
    // ADD TO HOME PAGE VIEWS
    self.homePageViews = [self.homePageViews arrayByAddingObject:currentLocationHomePage];
    
    // INIT AUTO LAYOUT VIEWS DICT
    [self.viewsDictionary setObject:currentLocationHomePage forKey:@"currentLocationHomePage"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    currentLocationHomePage.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.scrollViewHorizontalContainer addSubview:currentLocationHomePage];
    
    // LAYOUT
    
    // currentLocationHomePage
    self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[currentLocationHomePage]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:@[[NSLayoutConstraint constraintWithItem:currentLocationHomePage attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]];
    self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[currentLocationHomePage]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // scrollViewHorizontalContainer (USE THIS CCC VIEW DRIVE THE HEIGHT OF THE HORIZONTAL SCROLL VIEW'S ENTIRE FRAME (NOT JUST IT'S CONTENT VIEW PORT))
    self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:@[[NSLayoutConstraint constraintWithItem:self.scrollViewHorizontalContainer attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:currentLocationHomePage attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]]];
    
    // CONFIG
    
    // currentLocationHomePage
    [currentLocationHomePage updateForSickScoreDict:[[SWHelper helperAppDelegate] getLatestCurrentLocationSickScoreSynchronouslyFromDisk]];
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // WE CURRENTLY DO HAVE ALWAYS, SKIP ANIMATION
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        // WE CURRENTLY DO HAVE WHEN IN USE, SKIP ANIMATION
    }
    else
    {
        // WE DO NOT HAVE ACCESS TO THEIR LOCATION, SHOW ANIMATION!
        /*
        NSDictionary *currentLocationSickScoreDict = [[SWHelper helperAppDelegate] getLatestCurrentLocationSickScoreSynchronouslyFromDisk];
        NSMutableDictionary *mutableDict = [currentLocationSickScoreDict mutableCopy];
        if ([mutableDict isKindOfClass:[NSMutableDictionary class]])
        {
            double interval = 0.05;
            for (int i=0; i<100; i=i+5)
            {
                [mutableDict setObject:@(i) forKey:@"sickscore"];
                [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:i*interval];
            }

            [mutableDict setObject:@(1) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:1*interval];
            [mutableDict setObject:@(8) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:2*interval];
            [mutableDict setObject:@(15) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:3*interval];
            [mutableDict setObject:@(22) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:4*interval];
            [mutableDict setObject:@(38) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:5*interval];
            [mutableDict setObject:@(49) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:6*interval];
            [mutableDict setObject:@(55) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:7*interval];
            [mutableDict setObject:@(62) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:8*interval];
            [mutableDict setObject:@(74) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:9*interval];
            [mutableDict setObject:@(82) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:10*interval];
            [mutableDict setObject:@(85) forKey:@"sickscore"];
            [currentLocationHomePage performSelector:@selector(updateForSickScoreDict:) withObject:[mutableDict copy] afterDelay:11*interval];
        }
    */
    }
    currentLocationHomePage.delegate = self;
    
    // OK, NOW ADD A HOME PAGE SCREEN FOR EACH USER SAVED LOCATION... (SCREENS 2 THROUGH HOWEVER MANY LOCATIONS THEY SAVE)
    NSDictionary *allUserSavedPlacemarks = [[SWHelper helperAppDelegate] getPlacemarksFromPlacesFile];
    NSUInteger totalCount = [[allUserSavedPlacemarks objectForKey:@"placemarks"] count];
    self.sickScorePageControl.numberOfPages = 2+totalCount;
    NSString *lastHomePageLayoutKey = nil;
    for (NSDictionary *placemarkDict in [allUserSavedPlacemarks objectForKey:@"placemarks"])
    {
        // GET CURRENT INDEX AND LAYOUT KEY
        NSUInteger index = [[allUserSavedPlacemarks objectForKey:@"placemarks"] indexOfObject:placemarkDict];
        //NSLog(@"%@ of %@ - %@", @(index + 1), @(totalCount), [placemarkDict objectForKey:@"locality"]);
        NSString *homePageLayoutKey = [NSString stringWithFormat:@"homePage%@", @(index)];
        
        // INIT ALL REQUIRED UI ELEMENTS (AND ADD TO RUNNING LIST OF VIEW OBJECTS)
        SWHomePageView *homePage = [[SWHomePageView alloc] init];
        self.homePageViews = [self.homePageViews arrayByAddingObject:homePage];
        
        // INIT AUTO LAYOUT VIEWS DICT
        [self.viewsDictionary setObject:homePage forKey:homePageLayoutKey];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        homePage.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.scrollViewHorizontalContainer addSubview:homePage];
        
        // LAYOUT
        if (index == 0)
        {
            // IS FIRST
            self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[currentLocationHomePage][%@]", homePageLayoutKey] options:0 metrics:0 views:self.viewsDictionary]];
            self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:@[[NSLayoutConstraint constraintWithItem:homePage attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]];
            self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[%@]-(0)-|", homePageLayoutKey] options:0 metrics:0 views:self.viewsDictionary]];
        }
        else
        {
            // IS EITHER MIDDLE OR LAST
            self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[%@][%@]", lastHomePageLayoutKey, homePageLayoutKey] options:0 metrics:0 views:self.viewsDictionary]];
            self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:@[[NSLayoutConstraint constraintWithItem:homePage attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]];
            self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[%@]-(0)-|", homePageLayoutKey] options:0 metrics:0 views:self.viewsDictionary]];
        }
        [self.view addConstraints:self.homePageViewConstraints];
        
        // CONFIG
        
        //NSLog(@"placemarkDict = %@", placemarkDict);
        homePage.delegate = self;
        NSString *dataLookupKey = [NSString stringWithFormat:@"%@%@", [placemarkDict objectForKey:@"latitude"], [placemarkDict objectForKey:@"longitude"]];
        NSDictionary *cachedResponse = nil;
        if ([self.homePageNetworkResponses objectForKey:dataLookupKey])
        {
            cachedResponse = [self.homePageNetworkResponses objectForKey:dataLookupKey];
            [homePage updateForSickScoreDict:cachedResponse];
        }
        if ([method isEqualToString:@"refreshHomePagesForceNetwork"] || ([method isEqualToString:@"refreshHomePagesHitNetworkOnlyIfNoCache"] && !cachedResponse))
        {
           // NSLog(@"NETWORK HIT %@ of %@: getSickScoreInRadius.php CALLED", @(index + 1), @(totalCount));
            [[SWHelper helperAppBackend] appBackendGetSickScoreInRadiusForLat:[[placemarkDict objectForKey:@"latitude"] floatValue] lon:[[placemarkDict objectForKey:@"longitude"] floatValue] usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([jsonResponseBody isKindOfClass:[NSDictionary class]])
                    {
                        NSMutableDictionary *myPlacemark = [NSMutableDictionary new];
                        if ([[placemarkDict objectForKey:@"locality"] isKindOfClass:[NSString class]])
                        {
                            [myPlacemark setObject:[placemarkDict objectForKey:@"locality"] forKey:@"locality"];
                        }
                        if ([[placemarkDict objectForKey:@"administrativeArea"] isKindOfClass:[NSString class]])
                        {
                            [myPlacemark setObject:[placemarkDict objectForKey:@"administrativeArea"] forKey:@"administrativeArea"];
                        }
                        if ([[placemarkDict objectForKey:@"latitude"] isKindOfClass:[NSString class]])
                        {
                            [myPlacemark setObject:[placemarkDict objectForKey:@"latitude"] forKey:@"latitude"];
                        }
                        if ([[placemarkDict objectForKey:@"longitude"] isKindOfClass:[NSString class]])
                        {
                            [myPlacemark setObject:[placemarkDict objectForKey:@"longitude"] forKey:@"longitude"];
                        }
                        NSMutableDictionary *newDict = [jsonResponseBody mutableCopy];
                        [newDict setObject:myPlacemark forKey:@"placemark"];
                        [homePage updateForSickScoreDict:newDict];
                        [self.homePageNetworkResponses setObject:newDict forKey:dataLookupKey];
                    }
                });
            }];
        }
        
        // UPDATE FOR NEXT CYCLE
        lastHomePageLayoutKey = homePageLayoutKey;
    }
    
    // NOW ADD THE "ADD LOCATION" PAGE (ALWAYS THE LAST HOME PAGE SCREEN FOR ALL USERS)
    // INIT ALL REQUIRED UI ELEMENTS (AND ADD TO RUNNING LIST OF VIEW OBJECTS)
    SWHomePageAddView *homePageAddView = [[SWHomePageAddView alloc] init];
    self.homePageViews = [self.homePageViews arrayByAddingObject:homePageAddView];
    
    // INIT AUTO LAYOUT VIEWS DICT
    [self.viewsDictionary setObject:homePageAddView forKey:@"homePageAddView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    homePageAddView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.scrollViewHorizontalContainer addSubview:homePageAddView];
    
    // LAYOUT
    
    // homePageAddView
    if ([lastHomePageLayoutKey isKindOfClass:[NSString class]])
    {
        // ATTACH TO LAST USER SAVED LOCATION HOME PAGE SCREEN...
        self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[%@][homePageAddView]-(0)-|", lastHomePageLayoutKey] options:0 metrics:0 views:self.viewsDictionary]];
        self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[homePageAddView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:@[[NSLayoutConstraint constraintWithItem:homePageAddView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]];
    }
    else
    {
        // MUST BE PRETTY NEW USER, NO SAVED LOCATIONS YET, ATTACH TO OPENING "CURRENT LOCATION" HOME PAGE SCREEN THAT ALL USERS HAVE...
        self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[currentLocationHomePage]-(0)-[homePageAddView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:@[[NSLayoutConstraint constraintWithItem:homePageAddView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]];
        self.homePageViewConstraints = [self.homePageViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[homePageAddView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    }
    [self.view addConstraints:self.homePageViewConstraints];
    
    // CONFIG
    
    // homePageAddView
    homePageAddView.delegate = self;
}

- (void)helperUpdateUpperLeftBarButtonItem
{
    UIImage *leftBarButtonItemImage = [UIImage imageNamed:@"avatar-empty"];
    SWUserCDM *user = [SWHelper helperCurrentUser];
    if (user.profilePicThumbnailImageData)
    {
        leftBarButtonItemImage = [UIImage imageWithData:user.profilePicThumbnailImageData];
    }
    UIImageView *leftBarButtonItemImageView = [[UIImageView alloc] initWithImage:leftBarButtonItemImage];
    CGFloat size = 29;
    leftBarButtonItemImageView.frame = CGRectMake(0, 0, size, size);
    leftBarButtonItemImageView.layer.cornerRadius = size/2.0;
    if (user.profilePicThumbnailImageData)
    {
        leftBarButtonItemImageView.layer.borderWidth = 2;
    }
    leftBarButtonItemImageView.layer.borderColor = [SWColor colorSickweatherBlue41x171x226].CGColor;
    leftBarButtonItemImageView.clipsToBounds = YES;
    [leftBarButtonItemImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(upperLeftBarButtonTapped:)]];
    
    // SET NAV BAR VIEW CONSTRAINTS FOR IOS 11
    NSLayoutConstraint *widthConstraint = [leftBarButtonItemImageView.widthAnchor constraintEqualToConstant:size];
    NSLayoutConstraint *heightConstraint = [leftBarButtonItemImageView.heightAnchor constraintEqualToConstant:size];
    [widthConstraint setActive:YES];
    [heightConstraint setActive:YES];
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButtonItemImageView];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

- (void)helperUpdateUpperLeftBarButtonItemLandscape
{
    UIImage *leftBarButtonItemImage = [UIImage imageNamed:@"avatar-empty"];
    SWUserCDM *user = [SWHelper helperCurrentUser];
    if (user.profilePicThumbnailImageData)
    {
        leftBarButtonItemImage = [UIImage imageWithData:user.profilePicThumbnailImageData];
    }
    UIImageView *leftBarButtonItemImageView = [[UIImageView alloc] initWithImage:leftBarButtonItemImage];
    CGFloat size = 24;
    leftBarButtonItemImageView.frame = CGRectMake(0, 0, size, size);
    leftBarButtonItemImageView.layer.cornerRadius = size/2.0;
    if (user.profilePicThumbnailImageData)
    {
        leftBarButtonItemImageView.layer.borderWidth = 1;
    }
    leftBarButtonItemImageView.layer.borderColor = [SWColor colorSickweatherBlue41x171x226].CGColor;
    leftBarButtonItemImageView.clipsToBounds = YES;
    [leftBarButtonItemImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(upperLeftBarButtonTapped:)]];
    
    // SET NAV BAR VIEW CONSTRAINTS FOR IOS 11
    NSLayoutConstraint *widthConstraint = [leftBarButtonItemImageView.widthAnchor constraintEqualToConstant:size];
    NSLayoutConstraint *heightConstraint = [leftBarButtonItemImageView.heightAnchor constraintEqualToConstant:size];
    [widthConstraint setActive:YES];
    [heightConstraint setActive:YES];
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButtonItemImageView];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

- (void)helperMakeSureUpperLeftButtonLooksRighGivenLocalData
{
    if (self.view.bounds.size.width > self.view.bounds.size.height)
    {
        [self helperUpdateUpperLeftBarButtonItemLandscape];
    }
    else
    {
        [self helperUpdateUpperLeftBarButtonItem];
    }
}

#pragma mark - NSNotificationCenter Callback Methods

- (void)didUpdateCurrentLocationPlacemarkNotification:(NSNotification *)notification
{
    NSString *error = [notification.userInfo objectForKey:@"error"];
    NSString *toggle = [notification.userInfo objectForKey:@"toggle"];
    CLPlacemark *objectForPlacemark = [notification.userInfo objectForKey:@"placemark"];
    CLPlacemark *placemark = objectForPlacemark ? objectForPlacemark : self.currentPlacemark;
    self.currentPlacemark = placemark;
    if (error)
    {
        //NSLog(@"error = %@", error);
    }
    else if (placemark)
    {
        if ([placemark isKindOfClass:[CLPlacemark class]])
        {
            //NSLog(@"isKindOfClass:[CLPlacemark class]");
            
            if ([toggle isEqualToString: @"cvscore"]) {
                [[SWHelper helperAppDelegate] updateLatestCurrentLocationCovidScoreFromNetworkSavingToDiskFinishingWithAsynchronousNotificationUsingPlacemark:placemark];
            }
            else {
                [[SWHelper helperAppDelegate] updateLatestCurrentLocationSickScoreFromNetworkSavingToDiskFinishingWithAsynchronousNotificationUsingPlacemark:placemark];
            }
        }
        if ([placemark isKindOfClass:[NSDictionary class]])
        {
            //NSLog(@"isKindOfClass:[NSDictionary class]");
        }
    }
}

// THIS METHOD IS HUGE, IT'S HOW THE CURRENT LOCATION SCREEN/CARD (THE FIRST ONE ALWAYS) GETS UPDATED
- (void)currentLocationSickScoreNotification:(NSNotification *)notification
{
    //NSLog(@"notification = %@", notification);
    if ([[notification name] isEqualToString:@"currentLocationSickScore.json"])
    {
        //[self.sickScoreActivityIndicatorView stopAnimating];
       // NSLog(@"currentLocationSickScore.json notification.userInfo = %@", notification.userInfo);
        
        // USER INFO HERE IS THE RESPONSE BACK FROM currentLocationSickScore.json
        if ([[notification.userInfo objectForKey:@"placemark_is_current_location"] isEqualToString:@"yes"])
        {
            SWHomePageView *currentLocationHomePage = [self.viewsDictionary objectForKey:@"currentLocationHomePage"];
            [currentLocationHomePage updateForSickScoreDict:notification.userInfo];
            [currentLocationHomePage setupAsHasLocationUI];
        }
    }
}

- (void)currentLocationCovidScoreNotification:(NSNotification *)notification
{
    //NSLog(@"notification = %@", notification);
    if ([[notification name] isEqualToString:@"currentLocationCovidScore.json"])
    {
        //[self.sickScoreActivityIndicatorView stopAnimating];
       // NSLog(@"currentLocationCovidScore.json notification.userInfo = %@", notification.userInfo);
        
        // USER INFO HERE IS THE RESPONSE BACK FROM currentLocationSickScore.json
        if ([[notification.userInfo objectForKey:@"placemark_is_current_location"] isEqualToString:@"yes"])
        {
            SWHomePageView *currentLocationHomePage = [self.viewsDictionary objectForKey:@"currentLocationHomePage"];
            [currentLocationHomePage updateForCovidScoreDict:notification.userInfo];
            [currentLocationHomePage setupAsHasLocationUI];
        }
    }
}

- (void)finishedUpdatingCoreDataWithLatestProfilePictureDataNotification:(NSNotification *)notification
{
    [self helperMakeSureUpperLeftButtonLooksRighGivenLocalData];
}

- (void)userDidJustLogOutNotification:(NSNotification *)notification
{
    [self helperMakeSureUpperLeftButtonLooksRighGivenLocalData];
}

- (void)localDatabaseIsReadyNotification:(NSNotification *)notification
{
    [[SWHelper helperAppDelegate] appDelegateUpdateUserLaskKnownCurrentLocationPlacemarkUsingBestMethodAvailableFinishingWithDidUpdateCurrentLocationPlacemarkNotfification];
}

- (void)showUserFeedBackFlow:(NSNotification *)notification
{
    if ([SWHelper helperHasPassedFilterTimeSinceLastDisplayedUserFeedbackPrompt])
    {
        // MAKE VISIBLE
        self.feedbackCancelView.hidden = NO;
        self.feedbackView.hidden = NO;
    }
}

- (void)appDidBecomeActiveAgain:(NSNotification *)notification
{
    [self checkLocationUI];
}

#pragma mark - UITapGestureRecognizer Events

- (IBAction)hideFeedbackView:(UIGestureRecognizer*)tapGes {
    self.feedbackCancelView.hidden = YES;
    self.feedbackView.hidden = YES;
    [self.feedbackView reset];
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.scrollViewVerticalContainer])
    {
        // STUB
    }
    else if ([scrollView isEqual:self.scrollViewHorizontalContainer])
    {
        [self.view endEditing:YES];
        CGFloat pageWidth = scrollView.frame.size.width;
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        self.sickScorePageControl.currentPage = page;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
	if ([scrollView.panGestureRecognizer translationInView:scrollView.superview].x > 0) {
		//handle dragging to the right
	} else {
		[SWHelper logFlurryEventsWithTag:@"Home Screen Swipe Left"];
	}
}


#pragma mark - SWFeedbackViewDelegate

- (void)feedbackViewAppStoreRatingRequestedUserSaidNo:(id)sender
{
    //NSLog(@"feedbackViewAppStoreRatingRequestedUserSaidNo");
    self.feedbackCancelView.hidden = YES;
    self.feedbackView.hidden = YES;
    [self.feedbackView reset];
    
    // LOG TO PREVENT NEXT TIME
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate new] forKey:@"dateObjLastDisplayedUserFeedbackPrompt"];
}

- (void)feedbackViewAppStoreRatingRequestedUserSaidYes:(id)sender
{
    //NSLog(@"feedbackViewAppStoreRatingRequestedUserSaidYes");
    self.feedbackCancelView.hidden = YES;
    self.feedbackView.hidden = YES;
    [self.feedbackView reset];
    NSURL *appStoreReviewURL = [NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id741036885?action=write-review"];
    [[UIApplication sharedApplication] openURL:appStoreReviewURL];
    
    // LOG TO PREVENT NEXT TIME
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate new] forKey:@"dateObjLastDisplayedUserFeedbackPrompt"];
}

- (void)feedbackViewFeedbackRequestedUserSaidYes:(id)sender
{
    //NSLog(@"feedbackViewFeedbackRequestedUserSaidYes");
    self.feedbackCancelView.hidden = YES;
    self.feedbackView.hidden = YES;
    [self.feedbackView reset];
    NSString *email = @"mailto:info@sickweather.com?subject=Sickweather iOS App Feedback";
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"email = %@", email);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
    // LOG TO PREVENT NEXT TIME
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate new] forKey:@"dateObjLastDisplayedUserFeedbackPrompt"];
}

- (void)feedbackViewFeedbackRequestedUserSaidNo:(id)sender
{
    //NSLog(@"feedbackViewFeedbackRequestedUserSaidNo");
    self.feedbackCancelView.hidden = YES;
    self.feedbackView.hidden = YES;
    [self.feedbackView reset];
    
    // LOG TO PREVENT NEXT TIME
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate new] forKey:@"dateObjLastDisplayedUserFeedbackPrompt"];
}

- (void)feedbackViewYesButtonTapped:(id)sender
{
    if ([sender isKindOfClass:[SWFeedbackView class]])
    {
        SWFeedbackView *feedbackView = sender;
        if ([feedbackView.serverPromptDict isKindOfClass:[NSDictionary class]])
        {
            // The yes side of a server side prompt object
            NSString *yesLink = [feedbackView.serverPromptDict objectForKey:@"yes_link"];
            if ([yesLink isKindOfClass:[NSString class]])
            {
                [Flurry logEvent:@"Illness Report Message Prompt Yes" withParameters:@{@"Message":[feedbackView.serverPromptDict objectForKey:@"message_text"]}];
                SWWebViewViewController *webViewVC = [[SWWebViewViewController alloc] init];
                [webViewVC webViewViewControllerLoadURL:[NSURL URLWithString:yesLink]];
                webViewVC.delegate = self;
                UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:webViewVC];
                [self presentViewController:navVC animated:YES completion:^{}];
            }
            self.feedbackCancelView.hidden = YES;
            self.feedbackView.hidden = YES;
            [self.feedbackView reset];
        }
        else
        {
            // Let regular feedback flow take place
        }
    }
}

- (void)feedbackViewNoButtonTapped:(id)sender
{
    if ([sender isKindOfClass:[SWFeedbackView class]])
    {
        SWFeedbackView *feedbackView = sender;
        if ([feedbackView.serverPromptDict isKindOfClass:[NSDictionary class]])
        {
            // The no side of a server side prompt object
            [Flurry logEvent:@"Illness Report Message Prompt No" withParameters:@{@"Message":[feedbackView.serverPromptDict objectForKey:@"message_text"]}];
            self.feedbackCancelView.hidden = YES;
            self.feedbackView.hidden = YES;
            [self.feedbackView reset];
        }
        else
        {
            // Let regular feedback flow take place
        }
    }
}

#pragma mark - SWWebViewViewControllerDelegate

- (void)webViewViewControllerScreenWantsToDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWOnboardingViewControllerDelegate

- (void)onboardingWantsToDismiss:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"user_has_passed_onboarding"];
    [userDefaults synchronize];
    [self dismissViewControllerAnimated:YES completion:^{
        [OneSignal registerForPushNotifications];
    }];
}

#pragma mark - Target/Action

- (void)upperLeftBarButtonTapped:(id)sender
{
    [Flurry logEvent:@"Menu Button Tapped"];
	[self.view endEditing:YES];
    [SWHelper helperGetTab1SlidingViewController].topViewAnchoredGesture = ECSlidingViewControllerAnchoredGesturePanning | ECSlidingViewControllerAnchoredGestureTapping;
    [[SWHelper helperGetTab1SlidingViewController] anchorTopViewToRightAnimated:YES onComplete:^{}];
}

- (void)upperRightBarButtonTapped:(id)sender
{
    NSURL *appURL = [NSURL URLWithString:@"wellvia://"];
	[Flurry logEvent:@"Call Doctor Button tapped"];
    if ([[UIApplication sharedApplication] canOpenURL:appURL])
    {
        [[UIApplication sharedApplication] openURL:appURL];
    }
    else
    {
        SWConnectWithDoctorViewController *vc = [[SWConnectWithDoctorViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Life Cycle Methods

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    //self.sickScoreIntBackgroundCircleLabel.layer.cornerRadius = self.sickScoreIntBackgroundCircleLabel.bounds.size.width/2.0;
}

- (void)dealloc
{
    // REMOVE OBSERVERS
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self checkLocationUI];
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // ADD OBSERVERS
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showConsentNoticeAlert:)
                                                 name:@"showConsentNoticeAlertIfLocationAvailable"
                                               object:nil];
    
    // TELL US WHEN THE USER'S CURRENT LOCATION HAS BEEN TRANSLATED TO A PLACEMARK OBJECT
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUpdateCurrentLocationPlacemarkNotification:)
                                                 name:kSWDidUpdateCurrentLocationPlacemarkNotification
                                               object:nil];
    
    // GET LATEST CURRENT LOCATION SICK SCORE NETWORK DATA
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(currentLocationSickScoreNotification:)
                                                 name:@"currentLocationSickScore.json"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                selector:@selector(currentLocationCovidScoreNotification:)
                                                    name:@"currentLocationCovidScore.json"
                                                  object:nil];
    
    // TELL US WHEN USER THUMBNAIL SAVED SUCCESSFULLY LOCALLY
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishedUpdatingCoreDataWithLatestProfilePictureDataNotification:)
                                                 name:kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification
                                               object:nil];
    
    // TELL US WHEN THE USER LOGS OUT
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userDidJustLogOutNotification:)
                                                 name:kSWUserDidJustLogOutNotification
                                               object:nil];
    
    // TELL US WHEN THE DATABASE IS READY
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localDatabaseIsReadyNotification:)
                                                 name:kSWCoreDataManagedDocumentDidLoadNotificationKey
                                               object:nil];
    
    // TELL US WHEN FEEDBACK CAN BE TAKEN FROM USER
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showUserFeedBackFlow:)
                                                 name:kSWShowUserFeedBackFlowNotificationKey
                                               object:nil];
    
    // MAKE SURE WE ARE TOLD ABOUT WHEN WE COME BACK ON SCREEN (USER PLAYING BACK AND FORTH WITH SETTINGS APP)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActiveAgain:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // PRE INIT
    self.homePageNetworkResponses = [NSMutableDictionary new];
    
    // SETUP LEFT MENU SYSTEM HERE IN TAB 1
    SWNavigationController *menuVCInSWNavVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWMenuViewControllerNavigationController"];
    if (![SWHelper helperGetTab1SlidingViewController].underLeftViewController)
    {
        [SWHelper helperGetTab1SlidingViewController].underLeftViewController = menuVCInSWNavVC;
    }
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.sickScorePageControl = [[UIPageControl alloc] init];
    self.scrollViewVerticalContainer = [[UIScrollView alloc] init];
    self.scrollViewHorizontalContainer = [[UIScrollView alloc] init];
    self.feedbackView = [[SWFeedbackView alloc] init];
    self.feedbackCancelView = [[UIView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.sickScorePageControl forKey:@"sickScorePageControl"];
    [self.viewsDictionary setObject:self.scrollViewVerticalContainer forKey:@"scrollViewVerticalContainer"];
    [self.viewsDictionary setObject:self.scrollViewHorizontalContainer forKey:@"scrollViewHorizontalContainer"];
    [self.viewsDictionary setObject:self.feedbackView forKey:@"feedbackView"];
    [self.viewsDictionary setObject:self.feedbackCancelView forKey:@"feedbackCancelView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.sickScorePageControl.translatesAutoresizingMaskIntoConstraints = NO;
    self.scrollViewVerticalContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.scrollViewHorizontalContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.feedbackView.translatesAutoresizingMaskIntoConstraints = NO;
    self.feedbackCancelView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollViewVerticalContainer];
    [self.scrollViewVerticalContainer addSubview:self.scrollViewHorizontalContainer];
    [self.scrollViewVerticalContainer addSubview:self.sickScorePageControl];
    [self.view addSubview:self.feedbackCancelView];
    [self.view addSubview:self.feedbackView];
    
    
    // FIX ANY Z INDEX ISSUES
    [self.view bringSubviewToFront:self.sickScorePageControl];
    
    // LAYOUT
    
    // sickScorePageControl
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[sickScorePageControl(15)]"] options:0 metrics:0 views:self.viewsDictionary]];
    if (@available(iOS 11.0, *)) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScorePageControl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-10]];
    } else {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScorePageControl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:-5]];
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScorePageControl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.scrollViewHorizontalContainer attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    // scrollViewVerticalContainer
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollViewVerticalContainer]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollViewVerticalContainer]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // scrollViewHorizontalContainer
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollViewHorizontalContainer]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.scrollViewHorizontalContainer attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollViewHorizontalContainer]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // feedbackView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[feedbackView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[feedbackView]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // feedbackView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[feedbackCancelView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[feedbackCancelView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // view
    //self.view.backgroundColor = [UIColor redColor];
    
    // Config TAB BAR
    self.tabBarController.delegate = self;
    
    // navBar
    self.navigationController.navigationBar.translucent = NO;
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [SWFont fontDuepuntozeroRegularFontOfSize:26];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"sickweather";
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    [self helperUpdateUpperLeftBarButtonItem];
    UIBarButtonItem *upperRightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"call-doctor-with-text"] style:UIBarButtonItemStylePlain target:self action:@selector(upperRightBarButtonTapped:)];
    [self.navigationItem setRightBarButtonItem:upperRightBarButtonItem];
    
    // sickScorePageControl
    self.sickScorePageControl.currentPage = 0;
    self.sickScorePageControl.numberOfPages = 2;
    //self.sickScorePageControl.tintColor = [UIColor orangeColor];
    self.sickScorePageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.sickScorePageControl.currentPageIndicatorTintColor = [UIColor grayColor];
    
    // scrollViewVerticalContainer
    self.scrollViewVerticalContainer.delegate = self;
    //self.scrollViewVerticalContainer.pagingEnabled = YES;
    //self.scrollViewVerticalContainer.backgroundColor = [UIColor orangeColor];
    self.scrollViewVerticalContainer.alwaysBounceVertical = YES;
    
    // scrollViewHorizontalContainer
    self.scrollViewHorizontalContainer.delegate = self;
    //self.scrollViewHorizontalContainer.backgroundColor = [UIColor greenColor];
    self.scrollViewHorizontalContainer.alwaysBounceHorizontal = YES;
    self.scrollViewHorizontalContainer.pagingEnabled = YES;
    self.scrollViewHorizontalContainer.showsHorizontalScrollIndicator = NO;
    
    // feedbackView
    self.feedbackView.hidden = YES;
    self.feedbackView.delegate = self;
    
    // feedbackCancelView
    self.feedbackCancelView.hidden = YES;
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideFeedbackView:)];
    [self.feedbackCancelView addGestureRecognizer:tapGes];
    
    // Start by refreshing home pages
    [self refreshHomePages];
	// CHECK UNIVERSAL LINK TAP
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *foursquareId = [userDefaults objectForKey:@"foursquareId"];
	if(foursquareId != nil && ![foursquareId isEqualToString:@""]) {
		 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			 [userDefaults setObject:nil forKey:@"foursquareId"];
			 [self showGroupUsingSWFoursquareId:foursquareId];
		});
	}
	
    // CHECK/SHOW ONBOARDING...
	
    if (![userDefaults boolForKey:@"user_has_passed_onboarding"])
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            SWOnboardingViewController *onboarding = [[SWOnboardingViewController alloc] init];
            onboarding.delegate = self;
            SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:onboarding];
            [self presentViewController:navVC animated:YES completion:^{}];
        });
    }
	
    SWAppDelegate *appDelegate = (SWAppDelegate*) [UIApplication sharedApplication].delegate;
    [appDelegate initializeXmodeSdk];
}

@end
