//
//  SWReportIllnessViewController.m
//  Sickweather
//
//  Created by John Erck on 9/2/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//
@import SWAnalytics;
#import "SWReportIllnessViewController.h"
#import "SWNewMapViewController.h"
#import "SWIllnessCDM+SWAdditions.h"
#import "SWReportCDM+SWAdditions.h"
#import "SWUserCDM+SWAdditions.h"
#import "SWAddMessageToReportViewController.h"
#import "SWReportPrivatelyView.h"
#import "Flurry.h"
#import "SWMessageBannerView.h"
#import "SWEventsTableViewController.h"
#import "SWBlockTabWithLoginView.h"

@interface SWReportIllnessViewController () <SWAddMessageToReportViewControllerDelegate, SWMessageBannerViewDelegate, SWBlockTabWithLoginViewDelegate, SWSignInWithFacebookViewControllerDelegate>

// UI VIEWS
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) NSArray *illnesses;
@property (strong, nonatomic) SWReportPrivatelyView *reportPrivatelyView;
@property (strong, nonatomic) SWBlockTabWithLoginView *blockTabWithLoginView;
@property (strong, nonatomic) NSMutableArray *selectedIllnesses;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIAlertView *locationServicesRequiredAlert;
@property (strong, nonatomic) UIButton *reportAnonymouslyButton;
@property (strong, nonatomic) NSMutableArray *reports;
@property (strong, nonatomic) NSArray *imageViews;
@end

@implementation SWReportIllnessViewController

#pragma mark - SWSignInWithFacebookViewControllerDelegate

- (void)didDismissWithSuccessfulLogin:(SWSignInWithFacebookViewController *)vc
{
    self.blockTabWithLoginView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didDismissWithoutLoggingIn:(SWSignInWithFacebookViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWBlockTabWithLoginViewDelegate

- (void)primaryButtonTapped:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWSignInWithFacebookViewController *signInWithFacebookViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
    signInWithFacebookViewController.delegate = self;
    UINavigationController *signInWithFacebookNavigationController = [[UINavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
    [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
}

#pragma mark - SWMessageBannerViewDelegate

- (void)messageBannerViewWantsToDismiss:(id)sender
{
     //NSLog(@"messageBannerViewWantsToDismiss");
     if ([sender isKindOfClass:[SWMessageBannerView class]])
     {
         SWMessageBannerView *messageBannerView = (SWMessageBannerView *)sender;
         [UIView animateWithDuration:1.0 animations:^{
         messageBannerView.alpha = 0;
         } completion:^(BOOL finished) {
             [messageBannerView removeFromSuperview];
         }];
     }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:self.locationServicesRequiredAlert])
    {
        if (buttonIndex == 0) // Cancel
        {
            // Do nothing
        }
        else // Go to settings
        {
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
        }
    }
}

#pragma mark - SWAddMessageToReportViewControllerDelegate

- (void)addMessageToReportViewControllerWantsToDismissAfterTaggingToGroupOrAddingMessage
{
    [self addBannerWithMessage:@"Thank you for sharing your report."];
    [self unsetAllCheckboxes];
    [self.reportPrivatelyView reset];
    [self dismissViewControllerAnimated:YES completion:^{}];
	[self handleUserNavigation];
	self.selectedIllnesses = [NSMutableArray new];
}

- (void)addMessageToReportViewControllerWantsToDismissWithoutAddingMessageOrTaggingToAnyGroups
{
    [self addBannerWithMessage:@"Thank you for your report."];
    [self unsetAllCheckboxes];
    [self.reportPrivatelyView reset];
    [self dismissViewControllerAnimated:YES completion:^{}];
	[self handleUserNavigation];
	self.selectedIllnesses = [NSMutableArray new];
}

#pragma mark - Helpers
-(void)handleUserNavigation{
	if(self.isFromEventsScreen) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kSWRefreshEventsNotification
															object:self
														  userInfo:@{@"message":@"illness reports"}];
		[self popBackWith:3];
	}else if (self.isFromFamilyScreen){
		[self navigateToEventsWith:@"illness reports"];
	}else {
		NSMutableArray *viewController = [NSMutableArray arrayWithArray: [self.navigationController viewControllers]];
		if (viewController.count >=1){
			SWNewMapViewController *newMapVC = [[SWNewMapViewController alloc] init];
			newMapVC.isFromIllnessReport = YES;
            
			[newMapVC updateForMyReports:[SWHelper helperLocationManager].location];
			[viewController addObject:newMapVC];
			[self.navigationController setViewControllers:viewController];
		}
	}
}

- (void)unsetAllCheckboxes
{
    for (UIImageView *imageView in self.imageViews)
    {
        imageView.image = [UIImage imageNamed:@"radio-off"];
    }
}

- (void)addBannerWithMessage:(NSString *)message
{
    // INIT ALL REQUIRED UI ELEMENTS
    SWMessageBannerView *messageBannerView = [[SWMessageBannerView alloc] init];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    messageBannerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:messageBannerView];
    
    // CONFIG
    
    // Config MESSAGE BANNER VIEW
    messageBannerView.delegate = self;
    [messageBannerView setMessage:message];
    
    // INIT AUTO LAYOUT VIEWS DICT
    NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
    [viewsDictionary setObject:messageBannerView forKey:@"messageBannerView"];
    
    // LAYOUT
    
    // Layout MESSAGE BANNER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[messageBannerView]|"] options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[messageBannerView(44)]"] options:0 metrics:0 views:viewsDictionary]];
}

-(void)addTrackerWith:(NSDictionary*)data{
	[[SWHelper helperAppBackend]appBackendAddTrackerUsingArgs:data completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		dispatch_async(dispatch_get_main_queue(), ^{
		});
	}];
}
#pragma mark - Navigation

-(void)navigateToEventsWith:(NSString*)message{
	NSMutableArray *viewController = [NSMutableArray arrayWithArray: [self.navigationController viewControllers]];
	if (viewController.count >=1){
		[viewController removeObjectsInRange:NSMakeRange(1, viewController.count-1)];
		SWEventsTableViewController *eventsTableViewController = [[UIStoryboard storyboardWithName:@"Family" bundle:nil] instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
		eventsTableViewController.title = [SWUserCDM currentUser].firstName;
		eventsTableViewController.showSuccessHeader = true;
		eventsTableViewController.successMessage = message;
		[viewController addObject:eventsTableViewController];
		[self.navigationController setViewControllers:viewController];
	}
}

-(void)popBackWith:(int)nb{
	NSArray *viewControllers = [self.navigationController viewControllers];
	if ([viewControllers count] > nb) {
		[self.navigationController popToViewController:[viewControllers objectAtIndex:[viewControllers count] - nb] animated:true];
		
	}
}

#pragma mark - Target Action

- (void)singleTap:(UITapGestureRecognizer *)tap
{
    CGPoint touchPoint = [tap locationInView:self.scrollView];
    //NSLog(@"touchPoint = %@", NSStringFromCGPoint(touchPoint));
    UIView *touchedView = [self.scrollView hitTest:touchPoint withEvent:nil];
    //NSLog(@"touchedView = %@", touchedView);
    //NSLog(@"touchedView.tag = %ld", (long)touchedView.tag);
    //NSLog(@"touchedView.subviews = %@", touchedView.subviews);
    if ([touchedView.subviews count] == 2)
    {
        for (UIView *subview in touchedView.subviews)
        {
            if ([subview isKindOfClass:[UIImageView class]])
            {
                UIImageView *imageView = (UIImageView *)subview;
                
                // Manage data
                if ([self.selectedIllnesses containsObject:[self.illnesses objectAtIndex:touchedView.tag]])
                {
                    // Is currently selected, so we want to unselect
                    imageView.image = [UIImage imageNamed:@"radio-off"];
                    [self.selectedIllnesses removeObject:[self.illnesses objectAtIndex:touchedView.tag]];
                }
                else
                {
                    // Is current not selected, so we want to select
                    imageView.image = [UIImage imageNamed:@"radio-on"];
                    [self.selectedIllnesses addObject:[self.illnesses objectAtIndex:touchedView.tag]];
                    
                }
                
                // Style button
                if (self.selectedIllnesses.count > 0)
                {
                    // Blue
                    [self.reportAnonymouslyButton setTitleColor:[SWColor colorSickweatherWhite255x255x255] forState:UIControlStateNormal];
                    [self.reportAnonymouslyButton setTitleColor:[SWColor color:[SWColor colorSickweatherWhite255x255x255] usingOpacity:0.5] forState:UIControlStateHighlighted];
                    [self.reportAnonymouslyButton.layer setBorderColor:[SWColor colorSickweatherBlue41x171x226].CGColor];
                    self.reportAnonymouslyButton.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
                }
                else
                {
                    // Gray
                    [self.reportAnonymouslyButton setTitleColor:[SWColor colorSickweatherLightGray204x204x204] forState:UIControlStateNormal];
                    [self.reportAnonymouslyButton setTitleColor:[SWColor color:[SWColor colorSickweatherLightGray204x204x204] usingOpacity:0.5] forState:UIControlStateHighlighted];
                    [self.reportAnonymouslyButton.layer setBorderColor:[SWColor colorSickweatherLightGray204x204x204].CGColor];
                    self.reportAnonymouslyButton.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
                }
            }
        }
    }
}

- (void)screenWantsToDismiss:(id)sender
{
    [self.delegate didDismissViewControllerWithoutAnyReports];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    [self reportAnonymouslyButtonTouchUpInside:self];
}

- (void)reportAnonymouslyButtonTouchUpInside:(id)sender
{
    if (self.selectedIllnesses.count > 0)
    {
        // IF WE HAVE NEVER PROMPTED FOR LOCATION, DO SO NOW
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Access Required" message:@"Reports are attributed to your current location. Is it okay for us to ask for access to your location?" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[SWHelper helperLocationManager] requestWhenInUseAuthorization];
            }];
            [alert addAction:ok];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                // Do nothing, they cancelled out
            }];
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:^{}];
        }
        
        // CHECK FOR CURRENT ACCESS (WE MIGHT ALREADY HAVE LOCATION ACCESS)
        else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
        {
            // We do have always auth, proceed...
            NSMutableArray *illnessNames = [[NSMutableArray alloc] init];
            self.reports = [NSMutableArray new];
            for (SWIllnessCDM *illness in self.selectedIllnesses)
            {
                SWReportCDM *report = [SWReportCDM logReportForUser:[SWUserCDM currentlyLoggedInUser] forIllness:illness atLocation:[SWHelper helperLocationManager].location.coordinate inManagedObjectContext:[SWHelper helperManagedObjectContext]];
                [self.reports addObject:report];
                [illnessNames addObject:illness.name];
            }
		
			SWAddMessageToReportViewController *addMessageToReportViewController = [[SWAddMessageToReportViewController alloc] init];
			if (!self.familyMember) {
				addMessageToReportViewController.delegate = self;
				addMessageToReportViewController.latestMenuServerResponse = self.latestMenuServerResponse;
				addMessageToReportViewController.serverGroupDict = self.serverGroupDict;
				UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:addMessageToReportViewController];
				[self presentViewController:navVC animated:YES completion:^{}];
			}else {
				[self.view bringSubviewToFront:self.activityIndicator];
				[self.activityIndicator startAnimating];
			}
		
            // Posting showUserFeedbackView notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kSWShowUserFeedBackFlowNotificationKey object:nil];
		
            // Log for debug
            //NSLog(@"reports = %@", reports);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                // Log to Flurry
				NSString *tag = @"";
				if (self.familyMember || self.isFromEventsScreen || self.isFromFamilyScreen){
					if (self.reportPrivatelyView.isOn){
						tag = @"Family > Illness Report > Report button tapped with Report Privately selected";
					}else {
						tag = @"Family > Illness Report > Report button tapped";
					}
				}else {
					tag = @"Report button tapped";
				}
				 [Flurry logEvent:tag withParameters:@{@"Number of Illnesses Checked": @(self.reports.count).stringValue}];
                // Log each report to the server one at a time...
                for (SWReportCDM *report in self.reports)
                {
                    // Log to Flurry
                    [Flurry logEvent:@"Report Illness" withParameters:@{@"Illness": report.illness.name}];
                    
                    // TODO: Family member illness report here or below
                    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                    NSMutableDictionary* familyArgs = [[NSMutableDictionary alloc] init];
					if (self.familyMember){
						NSString *familyId = [SWHelper getFamilId];
						[familyArgs setObject:(familyId != nil && ![familyId isEqual:[NSNull null]]) ? familyId : @"" forKey:@"family_id"];
						[familyArgs setObject:self.familyMember != nil ? self.familyMember.member_id : @"" forKey:@"member_id"];
					}
                    [[SWHelper helperAppBackend] appBackendSubmitReportToSickweatherServerForLat:[report.latitude stringValue] lon:[report.longitude stringValue] reportText:report.illness.selfReportText identifier:report.illness.unique visible:(self.reportPrivatelyView.isOn ? @"0" : @"1") familyArgs:familyArgs usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                        if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"])
                        {
                            // Boom. Da server munched this piece of data successfully
                            NSString *reportId = [jsonResponseBody objectForKey:@"report_id"];
                            if ([reportId isKindOfClass:[NSString class]])
                            {
								NSMutableDictionary * additionalData = [[SWUserCDM currentlyLoggedInUser] userProfileDataForSnow];
								if (additionalData == nil || additionalData == NULL){
									additionalData = [[NSMutableDictionary alloc] init];
								}
								NSMutableDictionary * params = [additionalData objectForKey:@"params"];
								[params setObject:report.illness.unique forKey:@"illness_id"];
								[params setObject:report.illness.name forKey:@"illness_name"];
                                [SWAnalytics updateLocation:[report.latitude  doubleValue] longitude:[report.longitude doubleValue] params:@{@"params":params}];
                                report.reportId = reportId;
                            }
                            NSString *feedId = [jsonResponseBody objectForKey:@"feed_id"];
                            if ([feedId isKindOfClass:[NSString class]])
                            {
                                report.feedId = feedId;
                            }
						
                            if ([[jsonResponseBody objectForKey:@"message_prompt"] isKindOfClass:[NSDictionary class]])
                            {
                                self.serverMessagePromptDict = [jsonResponseBody objectForKey:@"message_prompt"];
                            }
                        }
                        else
                        {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"We successfully logged your report to your iPhone's database. However, the call to the server failed with the following message: %@", [jsonResponseBody objectForKey:@"message"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                        dispatch_semaphore_signal(sema);
                    }];
                    // Wait on main thread in for loop forever until we get our dispatch_semaphore_signal call
                    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                    sema = NULL;
                    
                    // Hit the next loop and wait above^ until the request completes
                }
				if (self.familyMember){
					 dispatch_async(dispatch_get_main_queue(), ^{
						 [self handleUserNavigation];
					 });
				}else {
					 addMessageToReportViewController.reports = self.reports;
				}
                // Now that we are done with the for loop, we can set the reports into the vc
            });
        }
        // TELL THEM WE DON'T HAVE ACCESS, AND HOW TO FIX...
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Turn Location On" message:@"Reports are attributed to your current location. Turn location on to make reports." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
            [alertController addAction:actionOK];
            [alertController addAction:actionCancel];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
    }
    else
    {
        [SWHelper helperShowAlertWithTitle:@"You must not be sick!" message:@"Please choose at least one illness."];
    }
}

- (void)locationManagerDidUpdateLocationsNotification:(id)sender
{
    if ([sender isKindOfClass:[NSNotification class]])
    {
        //@{@"didUpdateLocations":locations}
        NSNotification *notif = (NSNotification *)sender;
        NSArray *locations = [notif.userInfo objectForKey:@"didUpdateLocations"];
        CLLocation *location = [locations lastObject];
        if (location)
        {
            NSString *lat = [NSString stringWithFormat:@"%@", @(location.coordinate.latitude)];
            NSString *lon = [NSString stringWithFormat:@"%@", @(location.coordinate.longitude)];
            NSString *perPage = @"1000";
            NSString *myGroups = @"1";
            [[SWHelper helperAppBackend] appBackendGetGroupsUsingArgs:@{@"lat":lat,@"lon":lon,@"per_page":perPage,@"my_groups":myGroups} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"jsonResponseBody = %@", jsonResponseBody);
                    self.latestMenuServerResponse = jsonResponseBody;
                });
            }];
        }
    }
}

- (void)locationManagerDidChangeAuthorizationStatusNotification:(id)sender
{
    
}

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	[self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Life Cycle Methods

- (void)dealloc
{
    // TEAR DOWN OBSERVERS
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSWLocationManagerDidUpdateLocationsNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSWLocationManagerDidChangeAuthorizationStatusNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([SWHelper helperUserIsLoggedIn])
    {
        self.blockTabWithLoginView.hidden = YES;
    }
    else
    {
        self.blockTabWithLoginView.hidden = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([SWUserCDM currentlyLoggedInUser])
    {
        NSString *lat = [[SWUserCDM currentlyLoggedInUser].userLastKnownCurrentLocationLatitude stringValue];
        NSString *lon = [[SWUserCDM currentlyLoggedInUser].userLastKnownCurrentLocationLongitude stringValue];
        if (!lat) {lat=@"0";}
        if (!lon) {lon=@"0";}
        NSString *perPage = @"1000";
        NSString *myGroups = @"1";
        [[SWHelper helperAppBackend] appBackendGetGroupsUsingArgs:@{@"lat":lat,@"lon":lon,@"per_page":perPage,@"my_groups":myGroups} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"jsonResponseBody = %@", jsonResponseBody);
                self.latestMenuServerResponse = jsonResponseBody;
            });
        }];
    }
    else
    {
        // SHOW LOGIN
        
    }
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // SETUP OBSERVERS
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationManagerDidUpdateLocationsNotification:)
                                                 name:kSWLocationManagerDidUpdateLocationsNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationManagerDidChangeAuthorizationStatusNotification:)
                                                 name:kSWLocationManagerDidChangeAuthorizationStatusNotification
                                               object:nil];
	//INIT
	if(self.familyMember){
		self.activityIndicator = [[UIActivityIndicatorView alloc]
								  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		self.activityIndicator.center=self.view.center;
		[self.view addSubview:self.activityIndicator];
	}
	
	
    // SETUP PROPS
    self.imageViews = @[];
    
    // LOAD DATA
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWIllnessCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"isGroupIllness = %@", @"no"];
    NSError *error = nil;
    self.illnesses = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    self.selectedIllnesses = [NSMutableArray new];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.reportPrivatelyView = [[SWReportPrivatelyView alloc] init];
    self.blockTabWithLoginView = [[SWBlockTabWithLoginView alloc] init];
    UILabel *headerLabel = [[UILabel alloc] init];
    self.reportAnonymouslyButton = [[UIButton alloc] init];
    UIView *reportAnonymouslyButtonContainer = [[UIView alloc] init];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.reportPrivatelyView.translatesAutoresizingMaskIntoConstraints = NO;
    self.blockTabWithLoginView.translatesAutoresizingMaskIntoConstraints = NO;
    headerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.reportAnonymouslyButton.translatesAutoresizingMaskIntoConstraints = NO;
    reportAnonymouslyButtonContainer.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.reportPrivatelyView];
    [self.view addSubview:self.blockTabWithLoginView];
    [self.contentView addSubview:headerLabel];
    [reportAnonymouslyButtonContainer addSubview:self.reportAnonymouslyButton];
    [self.view addSubview:reportAnonymouslyButtonContainer];
    
    // Config NAVIGATION BAR
	if (self.isFromEventsScreen || self.isFromFamilyScreen) {
		self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	}
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Report" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemTapped:)];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.title = @"Report";
    
    // Config SCROLL VIEW
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:singleTapGestureRecognizer];
    
    // Config reportPrivatelyView
    //reportPrivatelyView.backgroundColor = [UIColor redColor];
    
    // Config blockTabWithLoginView
    [self.blockTabWithLoginView updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.blockTabWithLoginView updateImage:[UIImage imageNamed:@"onboarding-icon-sickscore"]];
    [self.blockTabWithLoginView updateTitle:@"Your Reports"];
    [self.blockTabWithLoginView updateDescription:@"Log in to start submitting illness reports for you and your family members."];
    [self.blockTabWithLoginView updateButtonTitle:@"Login"];
    self.blockTabWithLoginView.delegate = self;
    
    // Config headerLabel
    headerLabel.text = @"Which of the following illnesses would you like to report?";
    headerLabel.font = [SWFont fontStandardFontOfSize:16];
    headerLabel.textColor = [SWColor colorSickweatherStyleGuideGray79x76x77];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.numberOfLines = 0;
    
    // Config REPORT ANONYMOUSLY BUTTON
    [self.reportAnonymouslyButton addTarget:self action:@selector(reportAnonymouslyButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportAnonymouslyButton setTitle:@"Report" forState:UIControlStateNormal];
    [self.reportAnonymouslyButton setTitle:@"Report" forState:UIControlStateHighlighted];
    [self.reportAnonymouslyButton.titleLabel setFont:[SWFont fontStandardBoldFontOfSize:17]];
    [self.reportAnonymouslyButton setBackgroundColor:[SWColor colorSickweatherWhite255x255x255]];
    [self.reportAnonymouslyButton setTitleColor:[SWColor colorSickweatherLightGray204x204x204] forState:UIControlStateNormal];
    [self.reportAnonymouslyButton setTitleColor:[SWColor color:[SWColor colorSickweatherLightGray204x204x204] usingOpacity:0.5] forState:UIControlStateHighlighted];
    [self.reportAnonymouslyButton.layer setBorderColor:[SWColor colorSickweatherLightGray204x204x204].CGColor];
    [self.reportAnonymouslyButton.layer setBorderWidth:1.0];
    [self.reportAnonymouslyButton.layer setCornerRadius:5];
    
    // Config REPORT ANONYMOUSLY BUTTON CONTAINER
    reportAnonymouslyButtonContainer.backgroundColor = [UIColor whiteColor];
    
    // Config CONTENT VIEW
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.scrollView, @"scrollView", self.contentView, @"contentView", self.reportPrivatelyView, @"reportPrivatelyView", self.blockTabWithLoginView, @"blockTabWithLoginView", headerLabel, @"headerLabel", nil];
    [self.viewsDictionary addEntriesFromDictionary:@{@"reportAnonymouslyButton":self.reportAnonymouslyButton}];
    [self.viewsDictionary addEntriesFromDictionary:@{@"reportAnonymouslyButtonContainer":reportAnonymouslyButtonContainer}];
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // reportPrivatelyView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[reportPrivatelyView]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[reportPrivatelyView]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // blockTabWithLoginView
    
    // TOP
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.blockTabWithLoginView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    
    // BOTTOM
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.blockTabWithLoginView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    
    // LEFT AND RIGHT
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[blockTabWithLoginView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // headerLabel
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[headerLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[reportPrivatelyView]-(20)-[headerLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout REPORT ANONYMOUSLY BUTTON
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(20)-[reportAnonymouslyButton]-(20)-|" options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.reportAnonymouslyButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:reportAnonymouslyButtonContainer attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-20]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[reportAnonymouslyButton(50)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout REPORT ANONYMOUSLY BUTTON CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[reportAnonymouslyButtonContainer]|" options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[reportAnonymouslyButtonContainer(90)]"] options:0 metrics:0 views:self.viewsDictionary]];
	if (@available(iOS 11.0, *)) {
		UILayoutGuide * guide = self.view.safeAreaLayoutGuide;
		[reportAnonymouslyButtonContainer.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
	}else {
		 [self.view addConstraint:[NSLayoutConstraint constraintWithItem:reportAnonymouslyButtonContainer attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-40]];
	}
    // INIT AND LAYOUT DYNAMICALLY BASED ON ILLNESSES ARRAY
    NSInteger index = 0;
    NSString *previousViewName = nil;
    NSString *firstViewName = nil;
    NSNumber *heightOfTappableContainerView = @36;
    for (SWIllnessCDM *illness in self.illnesses)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        UIView *tappableContainerView = [[UIView alloc] init];
        UIImageView *icon = [[UIImageView alloc] init];
        self.imageViews = [self.imageViews arrayByAddingObject:icon];
        UILabel *label = [[UILabel alloc] init];
        
        // ADD TO AUTO LAYOUT DICTIONARY
        NSString *currentTappableContainerViewName = [NSString stringWithFormat:@"tappableContainerView%li", (long)index];
        NSString *currentIconName = [NSString stringWithFormat:@"icon%li", (long)index];
        NSString *currentLabelName = [NSString stringWithFormat:@"label%li", (long)index];
        [self.viewsDictionary addEntriesFromDictionary:@{currentTappableContainerViewName:tappableContainerView, currentIconName:icon, currentLabelName:label}];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        tappableContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        icon.translatesAutoresizingMaskIntoConstraints = NO;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [tappableContainerView addSubview:icon];
        [tappableContainerView addSubview:label];
        [self.contentView addSubview:tappableContainerView];
        
        // CONFIG
        
        // Congig tappableContainerView
        tappableContainerView.tag = index;
        
        // Config icon
        icon.image = [UIImage imageNamed:@"radio-off"];
        icon.contentMode = UIViewContentModeCenter;
        
        // Congif label
        label.text = illness.name;
        label.font = [SWFont fontStandardFontOfSize:13];
        label.textColor = [SWColor colorSickweatherStyleGuideGray79x76x77];
        
        // LAYOUT
        
        // Layout icon
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[%@]", currentIconName] options:0 metrics:0 views:self.viewsDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[%@]|", currentIconName] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout label
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[%@]-(8)-[%@]", currentIconName, currentLabelName] options:0 metrics:0 views:self.viewsDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[%@]|", currentLabelName] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Make UI dynamically based on local database
        NSNumber *halfOfIllnessCount = [NSNumber numberWithDouble:ceil(self.illnesses.count/2.0)]; // e.g. 12
        //NSLog(@"index == %@", @(index));
        if (index == 0) // 1st item of first column
        {
            NSString *horizontalLayout = [NSString stringWithFormat:@"H:|[%@]", currentTappableContainerViewName];
            NSString *verticalLayout = [NSString stringWithFormat:@"V:[headerLabel]-(20)-[%@(heightOfTappableContainerView)]", currentTappableContainerViewName];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalLayout options:0 metrics:0 views:self.viewsDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalLayout options:0 metrics:@{@"heightOfTappableContainerView": heightOfTappableContainerView} views:self.viewsDictionary]];
        }
        else if (index < [halfOfIllnessCount integerValue]) // 2nd through 12th (all first column values except for the 1st item)
        {
            if (index == [halfOfIllnessCount integerValue] - 1) // 12th item (end of first column)
            {
                NSString *horizontalLayout = [NSString stringWithFormat:@"H:|[%@]", currentTappableContainerViewName];
                NSString *verticalLayout = [NSString stringWithFormat:@"V:[%@][%@(heightOfTappableContainerView)]-(120)-|", previousViewName, currentTappableContainerViewName];// !!!THIS IS OUR VERTICAL PUSH OUT VIEW!!!
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalLayout options:0 metrics:0 views:self.viewsDictionary]];
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalLayout options:0 metrics:@{@"heightOfTappableContainerView": heightOfTappableContainerView} views:self.viewsDictionary]];
            }
            else // 2nd through 11th item (middle values of first column)
            {
                NSString *horizontalLayout = [NSString stringWithFormat:@"H:|[%@]", currentTappableContainerViewName];
                NSString *verticalLayout = [NSString stringWithFormat:@"V:[%@][%@(heightOfTappableContainerView)]", previousViewName, currentTappableContainerViewName];
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalLayout options:0 metrics:0 views:self.viewsDictionary]];
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalLayout options:0 metrics:@{@"heightOfTappableContainerView": heightOfTappableContainerView} views:self.viewsDictionary]];
            }
        }
        else if (index == [halfOfIllnessCount integerValue]) // 13th item (start of second column)
        {
            NSString *horizontalLayout = [NSString stringWithFormat:@"H:[%@]-0-[%@]|", firstViewName, currentTappableContainerViewName]; // !!!THIS IS OUR HORIZONTAL PUSH OUT VIEW!!!
            NSString *verticalLayout = [NSString stringWithFormat:@"V:[headerLabel]-(20)-[%@(heightOfTappableContainerView)]", currentTappableContainerViewName];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalLayout options:0 metrics:0 views:self.viewsDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalLayout options:0 metrics:@{@"heightOfTappableContainerView": heightOfTappableContainerView} views:self.viewsDictionary]];
        }
        else // 14th item and beyond (all second column values except for the first one)
        {
            NSString *horizontalLayout = [NSString stringWithFormat:@"H:[%@]-0-[%@]", firstViewName, currentTappableContainerViewName];
            NSString *verticalLayout = [NSString stringWithFormat:@"V:[%@][%@(heightOfTappableContainerView)]", previousViewName, currentTappableContainerViewName];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalLayout options:0 metrics:0 views:self.viewsDictionary]];
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalLayout options:0 metrics:@{@"heightOfTappableContainerView": heightOfTappableContainerView} views:self.viewsDictionary]];
        }
        
        // Set width as half of view
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:tappableContainerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0]];
        
        // UPDATE ITERATION PROPS
        if (index == 0)
        {
            firstViewName = currentTappableContainerViewName;
        }
        index++;
        previousViewName = currentTappableContainerViewName;
        
        // COLOR FOR DEV
        //tappableContainerView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
        //label.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //[reportAnonymouslyButton setBackgroundColor:[UIColor brownColor]];
    }
    
    // Make sure button is on top
    [self.view bringSubviewToFront:reportAnonymouslyButtonContainer];
    [self.view bringSubviewToFront:self.reportAnonymouslyButton];
    [self.view bringSubviewToFront:self.blockTabWithLoginView];
    
    // Get latest data from server (the next screen expects to be given it)
    /*
     api_key - Your API key
     location - city, state location for search (optional when my_groups = 1)
     lat - latitude location parameter (can be used instead of or in addition to location)
     lon - longitude location parameter (can be used instead of or in addition to location)
     radius - search radius in km (optional when my_groups = 1)
     per_page - number of results to return per page (defaults to 10)
     start_index - pagination start index (defaults to 0)
     
     Optional Inputs
     search - venue or group keyword(s), searches group name
     my_groups = 1 - only includes groups in which user is a member
     ehash - user’s email MD5 hash (user authentication required for my_groups=1)
     phash - user’s password MD5 hash
     user_id - user’s unique ID
     */
    NSString *lat = [[SWUserCDM currentlyLoggedInUser].userLastKnownCurrentLocationLatitude stringValue];
    NSString *lon = [[SWUserCDM currentlyLoggedInUser].userLastKnownCurrentLocationLongitude stringValue];
    if (!lat) {lat=@"0";}
    if (!lon) {lon=@"0";}
    NSString *perPage = @"1000";
    NSString *myGroups = @"1";
    [[SWHelper helperAppBackend] appBackendGetGroupsUsingArgs:@{@"lat":lat,@"lon":lon,@"per_page":perPage,@"my_groups":myGroups} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"jsonResponseBody = %@", jsonResponseBody);
            self.latestMenuServerResponse = jsonResponseBody;
        });
    }];
    
    // SHOW LOGIN UI IF NOT LOGGED IN
//#import "SWBlockTabWithLoginView.h"

    
    // COLOR FOR DEV
    //self.contentView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    //self.scrollView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
}

@end
