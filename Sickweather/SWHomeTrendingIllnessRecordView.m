//
//  SWHomeTrendingIllnessRecordView.m
//  Sickweather
//
//  Created by John Erck on 7/13/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWHomeTrendingIllnessRecordView.h"

@interface SWHomeTrendingIllnessRecordView()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *illnessTypeCircleImageView;
@property (strong, nonatomic) UILabel *illnessShortCodeLabel;
@property (strong, nonatomic) UILabel *illnessNameLabel;
@property (strong, nonatomic) UILabel *reportCountLabel;
@property (strong, nonatomic) UIImageView *trendingIconImageView;
@property (strong, nonatomic) UIImageView *rightDisclosureArrowIconImageView;
@property (strong, nonatomic) UILabel *percentTrendingLabel;
@end

@implementation SWHomeTrendingIllnessRecordView

#pragma mark - Public Methods

- (void)updateForIllnessInfoDict:(NSDictionary *)illnessInfoDict
{
    if ([[illnessInfoDict objectForKey:@"illness_short_code"] isKindOfClass:[NSString class]])
    {
        self.illnessShortCodeLabel.attributedText = [[NSAttributedString alloc] initWithString:[illnessInfoDict objectForKey:@"illness_short_code"] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:30]}];
    }
    if ([[illnessInfoDict objectForKey:@"illness_name"] isKindOfClass:[NSString class]])
    {
		NSString *illnessName = ([[illnessInfoDict objectForKey:@"illness_name"] isEqualToString:@"RSV"]) ? [illnessInfoDict objectForKey:@"illness_name"] : [[illnessInfoDict objectForKey:@"illness_name"] capitalizedString];
        self.illnessNameLabel.attributedText = [[NSAttributedString alloc] initWithString:illnessName attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:20]}];
    }
    if ([[illnessInfoDict objectForKey:@"number_of_reports"] isKindOfClass:[NSString class]])
    {
        self.reportCountLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ reports", [illnessInfoDict objectForKey:@"number_of_reports"]] attributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
    }
    if ([[illnessInfoDict objectForKey:@"trending_percent"] isKindOfClass:[NSString class]])
    {
        UIColor *trendPercentColor = [UIColor grayColor];
        self.trendingIconImageView.image = [UIImage imageNamed:@"change-arrow-right"];
        if ([[illnessInfoDict objectForKey:@"trending"] isEqualToString:@"up"])
        {
            trendPercentColor = [SWColor colorSickweatherStyleGuideRed255x1x0];
            self.trendingIconImageView.image = [UIImage imageNamed:@"change-arrow-up"];
        }
        if ([[illnessInfoDict objectForKey:@"trending"] isEqualToString:@"down"])
        {
            trendPercentColor = [SWColor colorSickweatherStyleGuideGreen68x157x68];
            self.trendingIconImageView.image = [UIImage imageNamed:@"change-arrow-down"];
        }
        self.percentTrendingLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%%", [illnessInfoDict objectForKey:@"trending_percent"]] attributes:@{NSForegroundColorAttributeName:trendPercentColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12]}];
    }
}

#pragma mark - Life Cycle Methods

- (id)init
{
    self = [super init];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.illnessTypeCircleImageView = [[UIImageView alloc] init];
        self.illnessShortCodeLabel = [[UILabel alloc] init];
        self.illnessNameLabel = [[UILabel alloc] init];
        self.reportCountLabel = [[UILabel alloc] init];
        self.trendingIconImageView = [[UIImageView alloc] init];
        self.rightDisclosureArrowIconImageView = [[UIImageView alloc] init];
        self.percentTrendingLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.illnessTypeCircleImageView forKey:@"illnessTypeCircleImageView"];
        [self.viewsDictionary setObject:self.illnessShortCodeLabel forKey:@"illnessShortCodeLabel"];
        [self.viewsDictionary setObject:self.illnessNameLabel forKey:@"illnessNameLabel"];
        [self.viewsDictionary setObject:self.reportCountLabel forKey:@"reportCountLabel"];
        [self.viewsDictionary setObject:self.trendingIconImageView forKey:@"trendingIconImageView"];
        [self.viewsDictionary setObject:self.rightDisclosureArrowIconImageView forKey:@"rightDisclosureArrowIconImageView"];
        [self.viewsDictionary setObject:self.percentTrendingLabel forKey:@"percentTrendingLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.illnessTypeCircleImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.illnessShortCodeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.illnessNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.reportCountLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.trendingIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.rightDisclosureArrowIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.percentTrendingLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.illnessTypeCircleImageView];
        [self addSubview:self.illnessShortCodeLabel];
        [self addSubview:self.illnessNameLabel];
        [self addSubview:self.reportCountLabel];
        [self addSubview:self.trendingIconImageView];
        [self addSubview:self.rightDisclosureArrowIconImageView];
        [self addSubview:self.percentTrendingLabel];
        
        // LAYOUT
        
        // illnessTypeCircleImageView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[illnessTypeCircleImageView]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[illnessTypeCircleImageView]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.illnessTypeCircleImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.illnessTypeCircleImageView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
        
        // illnessShortCodeLabel
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.illnessShortCodeLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.illnessTypeCircleImageView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.illnessShortCodeLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.illnessTypeCircleImageView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // illnessNameLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[illnessTypeCircleImageView]-(20)-[illnessNameLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(15)-[illnessNameLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // reportCountLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[illnessTypeCircleImageView]-(20)-[reportCountLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[illnessNameLabel]-(5)-[reportCountLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // trendingIconImageView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[trendingIconImageView]-(10)-[percentTrendingLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.trendingIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.percentTrendingLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // rightDisclosureArrowIconImageView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rightDisclosureArrowIconImageView]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightDisclosureArrowIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.percentTrendingLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // percentTrendingLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[percentTrendingLabel]-(40)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[percentTrendingLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // self
        
        // illnessTypeCircleImageView
        self.illnessTypeCircleImageView.image = [UIImage imageNamed:@"illness-icon-outline"];
        
        // illnessShortCodeLabel
        
        // illnessNameLabel
        
        // reportCountLabel
        
        // trendingIconImageView
        
        // rightDisclosureArrowIconImageView
        self.rightDisclosureArrowIconImageView.image = [UIImage imageNamed:@"angle-right"];
        
        // percentTrendingLabel
    }
    return self;
}

@end

