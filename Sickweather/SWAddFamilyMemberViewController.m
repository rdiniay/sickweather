//
//  SWAddFamilyMemberViewController.m
//  Sickweather
//
//  Created by John Erck on 8/17/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "SWAddFamilyMemberViewController.h"
#import "SWEditMyProfileSectionHeaderView.h"
#import "SWEditMyProfileRowView.h"
#import "SWEditMyProfileHeaderView.h"
#import "SWSelectItemFromListViewController.h"
#import "SWDatePicker.h"
#import "SWEditMyProfileSectionHeaderView.h"
#import "TPKeyboardAvoidingScrollView.h"


#define rowHeight 50
#define DATE_FORMAT_FOR_UI @"yyyy-MM-dd"
//#define DATE_FORMAT_FOR_UI @"MMMM d, yyyy"
#define DATE_FORMAT_FROM_SERVER @"MM/dd/yyyy"

@interface SWAddFamilyMemberViewController () <SWEditMyProfileSectionHeaderViewDelegate, SWEditMyProfileRowViewDelegate, SWSelectItemFromListViewControllerDelegate, SWEditMyProfileHeaderViewDelegate, SWDatePickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) UITextField *activeField;
@property (weak, nonatomic) SWEditMyProfileRowView *activeRow;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) TPKeyboardAvoidingScrollView *scrollView;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *personHeaderView;
@property (strong, nonatomic) SWEditMyProfileHeaderView *editMyProfileHeaderView;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *aboutHeaderView;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *contactSectionHeader;
@property (strong, nonatomic) SWEditMyProfileRowView *emailRow;
@property (strong, nonatomic) SWEditMyProfileRowView *birthdayRow;
@property (strong, nonatomic) SWEditMyProfileRowView *genderRow;
@property (strong, nonatomic) SWDatePicker *datePicker;
@property (strong, nonatomic) NSLayoutConstraint *datePickerVerticalHeightConstraint;
@property (strong, nonatomic) NSString *firstNameValueToSubmitToServer;
@property (strong, nonatomic) NSString *lastNameValueToSubmitToServer;
@property (strong, nonatomic) NSString *genderValueToSubmitToServer;
@property (strong, nonatomic) NSDate *birthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *dayBirthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *monthBirthdayValueToSubmitToServer;
@property (strong, nonatomic) NSString *yearBirthdayValueToSubmitToServer;
@property (strong, nonatomic) UIImage *familyMemberProfilePicImage;
@property (strong, nonatomic) NSMutableDictionary *imageForStringURL;
@property (strong, nonatomic) NSMutableArray *stringURLCurrentlyLoading;
@property (strong, nonatomic) UIPopoverController *popOver;
@end

@implementation SWAddFamilyMemberViewController

#pragma mark - SWDatePickerDelegate

- (void)datePickerWantsToCancel:(SWDatePicker *)sender
{
    [self hideDatePicker];
}

- (void)datePickerWantsToDone:(SWDatePicker *)sender
{
    [self hideDatePicker];
    self.birthdayValueToSubmitToServer = sender.datePicker.date;
    NSDateFormatter *dayFormat = [[NSDateFormatter alloc] init];
    [dayFormat setDateFormat:@"dd"];
    self.dayBirthdayValueToSubmitToServer = [dayFormat stringFromDate:sender.datePicker.date];
    NSDateFormatter *monthFormat = [[NSDateFormatter alloc] init];
    [monthFormat setDateFormat:@"MM"];
    self.monthBirthdayValueToSubmitToServer = [monthFormat stringFromDate:sender.datePicker.date];
    NSDateFormatter *yearFormat = [[NSDateFormatter alloc] init];
    [yearFormat setDateFormat:@"yyyy"];
    self.yearBirthdayValueToSubmitToServer = [yearFormat stringFromDate:sender.datePicker.date];
    NSDate *myDate = sender.datePicker.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMAT_FOR_UI];
    NSString *prettyVersion = [dateFormat stringFromDate:myDate];
    [self.activeRow setRowTextFieldText:prettyVersion];
    [self.activeRow highlightNewlySelectedValue];
}

#pragma mark - SWEditMyProfileHeaderViewDelegate

- (NSString *)editMyProfileHeaderViewFirstNameString:(SWEditMyProfileHeaderView *)sender
{
    return (self.familyMember.name_first == nil || [self.familyMember.name_first isEqual:[NSNull null]]) ? @"" : self.familyMember.name_first;
}

- (NSString *)editMyProfileHeaderViewLastNameString:(SWEditMyProfileHeaderView *)sender
{
    return (self.familyMember.name_last == nil || [self.familyMember.name_last isEqual:[NSNull null]]) ? @"" : self.familyMember.name_last ;
}

- (UIImage *)editMyProfileHeaderViewProfilePicImage:(SWEditMyProfileHeaderView *)sender
{
    if (self.familyMember.photo_original == nil && self.familyMember.photo_thumb == nil) {
        [sender setProfilePicImage:[UIImage imageNamed:@"avatar-empty"]];
        return [UIImage imageNamed:@"avatar-empty"];
    }
    
    NSString *stringURL = self.familyMember.photo_original;
    UIImage *imageToReturn = [self.imageForStringURL objectForKey:stringURL];
    if (!imageToReturn)
        {
        if ([self.stringURLCurrentlyLoading containsObject:stringURL])
            {
                //NSLog(@"Skip, work in progress for stringURL %@", stringURL);
            }
        else
            {
                //NSLog(@"Start, fetch for stringURL %@", stringURL);
            [self.stringURLCurrentlyLoading addObject:stringURL]; // Log as currently loading
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
            [[session dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    //NSLog(@"Background Thread...");
                dispatch_async(dispatch_get_main_queue(), ^{
                        //NSLog(@"Main Thread...");
                    [self.stringURLCurrentlyLoading removeObject:stringURL]; // Remove from currently loading
                    UIImage *image = [UIImage imageWithData:data];
                    if (image)
                        {
                            [self.imageForStringURL setObject:image forKey:stringURL];
                            [sender setProfilePicImage:image];
                        }
                    else
                        {
                            [sender setProfilePicImage:[UIImage imageNamed:@"avatar-empty"]];
                        }
                });
            }] resume];
            }
        }
    return imageToReturn;
}

- (void)editMyProfileHeaderViewProfilePicTapped:(SWEditMyProfileHeaderView *)sender
{
    [self.view endEditing:YES];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select Profile Pic" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Cancel button tappped.
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Camera button tapped.
        [self showPhotoPickerUsingSourceType:UIImagePickerControllerSourceTypeCamera];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Photos library button tapped.
        [self showPhotoPickerUsingSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - SWEditMyProfileSectionHeaderViewDelegate

- (NSString *)editMyProfileSectionHeaderViewTitleString:(SWEditMyProfileSectionHeaderView *)sender
{
    if ([sender isEqual:self.personHeaderView])
    {
        return @"PERSON";
    }
    if ([sender isEqual:self.contactSectionHeader])
    {
        return @"CONTACT";
    }
    if ([sender isEqual:self.aboutHeaderView])
    {
        return @"ABOUT";
    }
    return @"";
}

#pragma mark - SWEditMyProfileRowViewDelegate

- (NSString *)editMyProfileRowViewTitleString:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.emailRow])
    {
    return (self.familyMember.email == nil || [self.familyMember.email isEqual:[NSNull null]]) ? @"" : self.familyMember.email ;
    }
    if ([sender isEqual:self.birthdayRow])
    {
    return (self.familyMember.birthdate == nil || [self.familyMember.birthdate isEqual:[NSNull null]]) ?  @"" : self.familyMember.birthdate;
    }
    if ([sender isEqual:self.genderRow])
    {
    return (self.familyMember.gender == nil || [self.familyMember.gender isEqual:[NSNull null]]) ? @"" : [self userDataGenderFormattedString];
    }
    return @"";
}

- (NSString *)editMyProfileRowViewPlaceholderString:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.emailRow])
    {
        return @"Email";
    }
    if ([sender isEqual:self.birthdayRow])
    {
        return @"Birthday";
    }
    if ([sender isEqual:self.genderRow])
    {
        return @"Gender";
    }
    return @"";
}

- (UIImage *)editMyProfileRowViewIconImage:(SWEditMyProfileRowView *)sender
{
    UIImage *image;
    if ([sender isEqual:self.emailRow])
    {
        image = [UIImage imageNamed:@"profile-icon-email"];
    }
    if ([sender isEqual:self.birthdayRow])
    {
        image = [UIImage imageNamed:@"profile-icon-birthdate"];
    }
    if ([sender isEqual:self.genderRow])
    {
        image = [UIImage imageNamed:@"profile-icon-gender"];
    }
    return image;
}

- (NSString *)userDataGenderFormattedString
{
    NSString *answer = self.familyMember.gender;
    if (answer){
        if ([answer isEqualToString:@"1"])
        {
            answer = @"Female";
        }else if ([answer isEqualToString:@"2"])
        {
            answer = @"Male";
        } else if ([answer isEqualToString:@"3"])
        {
            answer = @"Other";
        }else if ([answer isEqualToString:@"4"] || [answer isEqualToString:@"-1"] || [answer isEqualToString:@"0"])
        {
            answer = @"Not Specified";
        }
    }
    else
    {
        answer = @"Not Specified";
    }
    return answer;
}

- (void)editMyProfileRowWasTapped:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.emailRow])
    {
        self.activeRow = self.emailRow;
        SWSelectItemFromListViewController *selectItemFromListVC = [[SWSelectItemFromListViewController alloc] init];
        selectItemFromListVC.delegate = self;
        [self.navigationController pushViewController:selectItemFromListVC animated:YES];
    }
    if ([sender isEqual:self.birthdayRow])
    {
        [self.view endEditing:YES];
        self.activeRow = self.birthdayRow;
        [self showDatePickerUsingTitle:@"Birthday"];
    }
    if ([sender isEqual:self.genderRow])
    {
        self.activeRow = self.genderRow;
        SWSelectItemFromListViewController *selectItemFromListVC = [[SWSelectItemFromListViewController alloc] init];
        selectItemFromListVC.delegate = self;
        [self.navigationController pushViewController:selectItemFromListVC animated:YES];
    }
}

#pragma mark - SWSelectItemFromListViewControllerDelegate

- (NSString *)selectItemFromListViewControllerJSONFileToUseBaseNameWithoutExtension
{
    NSString *name;
    if ([self.activeRow isEqual:self.genderRow])
    {
        name = @"selectGender"; // .json in app's bundle
    }
    return name;
}

- (NSString *)selectItemFromListViewControllerSelectedSystemValue:(SWSelectItemFromListViewController *)sender
{
    NSString *systemValue;
    if ([self.activeRow isEqual:self.genderRow])
    {
        systemValue = self.genderValueToSubmitToServer;
    }
    return systemValue;
}

- (void)selectItemFromListViewControllerWantsToDismiss:(SWSelectItemFromListViewController *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selectItemFromListViewController:(SWSelectItemFromListViewController *)sender userSelectedRowDict:(NSDictionary *)rowDict
{
    if ([self.activeRow isEqual:self.genderRow])
    {
        if ([rowDict objectForKey:@"system_value"])
        {
            self.genderValueToSubmitToServer = [rowDict objectForKey:@"system_value"];
        }
    }
    [self.activeRow setRowTextFieldText:[rowDict objectForKey:@"name"]];
    [self.activeRow highlightNewlySelectedValue];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Target/Action

-(void)upDateModalObject{
    [self updateEmail];
    [self updateDateOfBirth];
    [self updateGender];
    [self updateFirstName];
    [self updateLastName];
}

-(void)updateEmail{
    if ([[self.emailRow.currentTextField text] length] > 0)
        {
            self.familyMember.email = [self.emailRow.currentTextField text];
        }else {
            self.familyMember.email = @"";
    }
}

-(void)updateDateOfBirth{
    if ([[self.birthdayRow.currentTextField text] length] > 0){
        self.familyMember.birthdate = [self.birthdayRow.currentTextField text];
    }
}

-(void)updateGender{
    if ([[self.genderRow.currentTextField text] length] > 0){
        self.familyMember.gender = self.genderValueToSubmitToServer;
    }
}


-(void)updateFirstName{
    if ([[self.editMyProfileHeaderView.firstNameTextField text] length] > 0){
        self.familyMember.name_first = [self.editMyProfileHeaderView.firstNameTextField text];
    }else {
        self.familyMember.name_first = @"";
    }
}

-(void)updateLastName{
    if ([[self.editMyProfileHeaderView.lastNameTextField text] length] > 0){
        self.familyMember.name_last = [self.editMyProfileHeaderView.lastNameTextField text];
    }else {
        self.familyMember.name_last = @"";
    }
}

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.delegate addFamilyMemberViewControllerWantsToCancel:self];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    // END EDITING
    [self.view endEditing:YES];
    [self hideDatePicker];
    
    // VALIDATE
    BOOL isValid = YES;
    if (isValid && [[self.editMyProfileHeaderView.firstNameTextField text] isEqualToString:@""])
    {
        isValid = NO;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing First Name" message:@"Please enter first name." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.editMyProfileHeaderView.firstNameTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if (isValid)
    {
        NSMutableDictionary *familyMemberInfo = [NSMutableDictionary new];
        if ([[self.editMyProfileHeaderView.firstNameTextField text] length] > 0)
        {
            [familyMemberInfo setObject:[self.editMyProfileHeaderView.firstNameTextField text] forKey:@"name_first"];
        }
    
        if ([[self.editMyProfileHeaderView.lastNameTextField text] length] > 0)
        {
            [familyMemberInfo setObject:[self.editMyProfileHeaderView.lastNameTextField text] forKey:@"name_last"];
        }
    
        if ([[self.emailRow.currentTextField text] length] > 0)
        {
            [familyMemberInfo setObject:[self.emailRow.currentTextField text] forKey:@"email"];
        }
    
        if ([[self.birthdayRow.currentTextField text] length] > 0)
        {
            [familyMemberInfo setObject:[self.birthdayRow.currentTextField text] forKey:@"birth_date"];
            [familyMemberInfo setObject:self.dayBirthdayValueToSubmitToServer forKey:@"day"];
            [familyMemberInfo setObject:self.monthBirthdayValueToSubmitToServer forKey:@"month"];
            [familyMemberInfo setObject:self.yearBirthdayValueToSubmitToServer forKey:@"year"];
            self.familyMember.birthdate = [self.birthdayRow.currentTextField text];
        }
    
        if ([[self.genderRow.currentTextField text] length] > 0)
        {
            self.familyMember.gender = self.genderValueToSubmitToServer;
            [familyMemberInfo setObject:self.genderValueToSubmitToServer forKey:@"gender"];
        }
    
        if (self.wantsToEdit){
             [familyMemberInfo setObject:self.familyMember.member_id forKey:@"member_id"];
             [familyMemberInfo setObject:self.familyMember.family_id forKey:@"family_id"];
        }
    
        // HIT NETWORK
    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage: self.wantsToEdit ? @"Updating Family Member..." : @"Adding Family Member..."];
        [[SWHelper helperAppBackend] appBackendAddFamilyMemberUsingArgs:familyMemberInfo usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) // Add Family member Success
                {
                    NSString* memberId = [jsonResponseBody objectForKey:@"member_id"];
                    NSString* familyId = [jsonResponseBody objectForKey:@"family_id"];
                    if (!self.wantsToEdit){
                        self.familyMember.confirmed = [jsonResponseBody objectForKey:@"confirmed"];
                    }
                    self.familyMember.family_id = familyId;
                    self.familyMember.member_id = memberId;
                    [self upDateModalObject];
                    if(self.familyMemberProfilePicImage) {  // Uploading Profile image true
                        // Upload image
                        NSDictionary* serverArgs = @{@"member_id":memberId, @"family_id":familyId};
                        [self uploadImageWith:serverArgs];
                    } else {
                        // family member created, dismiss now
                         dispatch_async(dispatch_get_main_queue(), ^{
                            [SWHelper helperDismissFullScreenActivityIndicator];
                            [self.delegate addFamilyMemberViewControllerWantsToDismiss:self withFamily:self.familyMember];
                         });
                    }
                } else { // Add Family member failure
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SWHelper helperDismissFullScreenActivityIndicator];
                        NSString *errorMessage = @"Ops! Family member not saved! Please try again";
                        if ([[jsonResponseBody objectForKey:@"message"] isKindOfClass:[NSString class]]) {
                            errorMessage = [jsonResponseBody objectForKey:@"message"];
                        }
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        }];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }
            });
        }];
    }
}

-(void)uploadImageWith:(NSDictionary*)args {
    [[SWHelper helperAppBackend] appBackendUploadFamilyMemberProfilePicUsingArgs:args Image:self.familyMemberProfilePicImage usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) // Upload image success
                {
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    self.familyMember.photo_original = [jsonResponseBody objectForKey:@"photo_original"];
                    if (self.wantsToEdit){
                        [self.delegate addFamilyMemberViewControllerWantsToDismiss:self withFamily:self.familyMember withPhoto:self.familyMemberProfilePicImage];
                    }else {
                        [self.delegate addFamilyMemberViewControllerWantsToDismiss:self withFamily:self.familyMember];
                    }
                } else { // Upload image failure
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    if (self.wantsToEdit) {
                        NSString *errorMessage = @"Ops! Image uploading failed!";
                        if ([[jsonResponseBody objectForKey:@"message"] isKindOfClass:[NSString class]]) {
                            errorMessage = [jsonResponseBody objectForKey:@"message"];
                        }
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        [self.delegate addFamilyMemberViewControllerWantsToDismiss:self withFamily:self.familyMember];
                    }
                }
        });
    }];
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self hideDatePicker];
}

#pragma mark - UIImagePickerControllerDelegate

// Tells the delegate that the user picked a still image or movie.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //NSLog(@"info = %@", info);
    [self.editMyProfileHeaderView setProfilePicImage:[info objectForKey:UIImagePickerControllerEditedImage]];
    self.familyMemberProfilePicImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

// Tells the delegate that the user cancelled the pick operation.
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //NSLog(@"Cancelled the photo picker process...");
    [self dismissViewControllerAnimated:YES completion:^{}];
}

// Tells the delegate that the user picked an image. (Deprecated in iOS 3.0. Use imagePickerController:didFinishPickingMediaWithInfo: instead.)
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    //NSLog(@"Did this get called?");
}

#pragma mark - Helpers

- (void)hideDatePicker
{
    // HIDE THE DATE PICKER
    self.datePickerVerticalHeightConstraint.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

- (void)showDatePickerUsingTitle:(NSString *)title
{
    // SHOW THE DATE PICKER
    [self.datePicker setTitle:title];
    if (self.birthdayValueToSubmitToServer != nil)
    {
        self.datePicker.datePicker.date = self.birthdayValueToSubmitToServer;
    }
    self.datePickerVerticalHeightConstraint.constant = 300;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

- (void)showPhotoPickerUsingSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        NSString *mediaType = AVMediaTypeVideo;
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
        if (authStatus == AVAuthorizationStatusAuthorized)
        {
            // do your logic
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.sourceType = sourceType;
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                self.popOver = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.popOver presentPopoverFromRect:CGRectMake(120, 110, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
                });
            }
            else
            {
                [self presentViewController:imagePicker animated:YES completion:^{}];
            }
        }
        else if (authStatus == AVAuthorizationStatusDenied)
        {
            // denied
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera Access Off" message:@"To turn camera access on, go to settings and turn camera access on." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
            [alertController addAction:ok];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (authStatus == AVAuthorizationStatusRestricted)
        {
            // restricted, normally won't happen
            [SWHelper helperShowAlertWithTitle:@"Camera Access Restricted"];
        }
        else if (authStatus == AVAuthorizationStatusNotDetermined)
        {
            // not determined?!
            [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
                if(granted)
                {
                    //NSLog(@"Granted access to %@", mediaType);
                }
                else
                {
                    //NSLog(@"Not granted access to %@", mediaType);
                }
            }];
        }
        else
        {
            // impossible, unknown authorization status
        }
    }
    else
    {
        [SWHelper helperShowAlertWithTitle:@"Selected Source Type Not Available"];
    }
}

- (void)addDatePicker
{
    if (![self.viewsDictionary objectForKey:@"datePicker"])
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.datePicker = [[SWDatePicker alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        [self.viewsDictionary setObject:self.datePicker forKey:@"datePicker"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.datePicker.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.view addSubview:self.datePicker];
        
        // LAYOUT
        
        // Layout DATE PICKER
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[datePicker]|" options:0 metrics:nil views:self.viewsDictionary]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[datePicker]|" options:0 metrics:nil views:self.viewsDictionary]];
        self.datePickerVerticalHeightConstraint = [NSLayoutConstraint constraintWithItem:self.datePicker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0];
        [self.view addConstraint:self.datePickerVerticalHeightConstraint];
        
        // CONFIG
        
        // Config DATE PICKER
        self.datePicker.delegate = self;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.datePicker.backgroundColor = [UIColor redColor];
    }
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[TPKeyboardAvoidingScrollView alloc] init];
    self.personHeaderView = [[SWEditMyProfileSectionHeaderView alloc] init];
    if (!self.familyMember){
        self.familyMember = [[SWFamilyMemberModal alloc] init];
    }else{
        if (self.familyMember.birthdate != nil && ![self.familyMember.birthdate isEqual:[NSNull null]] && ![self.familyMember.birthdate isEqualToString:@""]) {
            self.dayBirthdayValueToSubmitToServer =  [self.familyMember birthDateDayForServer];
            self.monthBirthdayValueToSubmitToServer = [self.familyMember birthDateMonthForServer];
            self.yearBirthdayValueToSubmitToServer = [self.familyMember birthDateYearForServer];
        }
        if (self.familyMember.gender != nil && ![self.familyMember.gender isEqual:[NSNull null]] && ![self.familyMember.gender isEqualToString:@""]) {
            self.genderValueToSubmitToServer = self.familyMember.gender;
        }
    }
    self.imageForStringURL = [NSMutableDictionary new];
    self.stringURLCurrentlyLoading = [NSMutableArray new];
    self.editMyProfileHeaderView = [[SWEditMyProfileHeaderView alloc] init];
    self.contactSectionHeader = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.emailRow = [[SWEditMyProfileRowView alloc] init];
    self.emailRow.currentTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailRow.currentTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.aboutHeaderView = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.birthdayRow = [[SWEditMyProfileRowView alloc] init];
    self.genderRow = [[SWEditMyProfileRowView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.personHeaderView forKey:@"personHeaderView"];
    [self.viewsDictionary setObject:self.editMyProfileHeaderView forKey:@"editMyProfileHeaderView"];
    [self.viewsDictionary setObject:self.contactSectionHeader forKey:@"contactSectionHeader"];
    [self.viewsDictionary setObject:self.emailRow forKey:@"emailRow"];
    [self.viewsDictionary setObject:self.aboutHeaderView forKey:@"aboutHeaderView"];
    [self.viewsDictionary setObject:self.genderRow forKey:@"genderRow"];
    [self.viewsDictionary setObject:self.birthdayRow forKey:@"birthdayRow"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.personHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
    self.editMyProfileHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
    self.aboutHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contactSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.birthdayRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.genderRow.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.personHeaderView];
    [self.scrollView addSubview:self.editMyProfileHeaderView];
    [self.scrollView addSubview:self.contactSectionHeader];
    [self.scrollView addSubview:self.aboutHeaderView];
    [self.scrollView addSubview:self.emailRow];
    [self.scrollView addSubview:self.genderRow];
    [self.scrollView addSubview:self.birthdayRow];
    
    
    // LAYOUT
    
    // scrollView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // personHeaderView
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.personHeaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[personHeaderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[personHeaderView(rowHeight)]"] options:0 metrics:@{@"rowHeight":@(rowHeight)} views:self.viewsDictionary]];
    
    // editMyProfileHeaderView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[editMyProfileHeaderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[personHeaderView][editMyProfileHeaderView(120)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    //Contact Header View
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contactSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[editMyProfileHeaderView]-(0)-[contactSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // emailRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[emailRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[contactSectionHeader][emailRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@(rowHeight)} views:self.viewsDictionary]];
    
    // aboutHeaderView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[aboutHeaderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[emailRow][aboutHeaderView(rowHeight)]"] options:0 metrics:@{@"rowHeight":@(rowHeight)} views:self.viewsDictionary]];
    
        // genderRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[genderRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[aboutHeaderView][genderRow(rowHeight)]-20-|"] options:0 metrics:@{@"rowHeight":@(rowHeight)} views:self.viewsDictionary]];
    
    // birthdayRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[birthdayRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[genderRow][birthdayRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@(rowHeight)} views:self.viewsDictionary]];
    
    
    
    // navBar
    self.navigationController.navigationBar.translucent = NO;
    
    // leftBarButtonItem
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(leftBarButtonItemTapped:)];
    
    // rightBarButtonItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(rightBarButtonItemTapped:)];
    
    // view
    self.view.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
    
    // scrollView
    self.scrollView.alwaysBounceVertical = YES;
    
    // personHeaderView
    //self.personHeaderView.backgroundColor = [UIColor redColor];
    self.personHeaderView.delegate = self;
    
    // editMyProfileHeaderView
    self.editMyProfileHeaderView.delegate = self;
    
    //Contact HeaderView
    self.contactSectionHeader.delegate = self;
    
    // aboutHeaderView
    self.aboutHeaderView.delegate = self;
    
    // emailRow
    self.emailRow.delegate = self;
    
    // birthdayRow
    self.birthdayRow.delegate = self;
    [self.birthdayRow addDisclosureIconAndTapFeature];
    
    // genderRow
    self.genderRow.delegate = self;
    [self.genderRow addDisclosureIconAndTapFeature];
    
    // datePicker
    [self addDatePicker];
}

@end
