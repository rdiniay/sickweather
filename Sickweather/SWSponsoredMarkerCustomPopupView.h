//
//  SWSponsoredMarkerCustomPopupView.h
//  Sickweather
//
//  Created by John Erck on 12/11/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWSponsoredMarkerMapKitAnnotationView.h"

@protocol SWSponsoredMarkerCustomPopupViewDelegate <NSObject>
@end

@interface SWSponsoredMarkerCustomPopupView : UIView
@property (weak, nonatomic) id <SWSponsoredMarkerCustomPopupViewDelegate>delegate;
@property (strong, nonatomic) SWSponsoredMarkerMapKitAnnotationView *annotationView;
@property (strong, nonatomic) UIView *bottomClearContentView;
- (void)setImage:(UIImage *)image;
- (void)setButtonBackgroundImage:(UIImage *)image;
- (void)setDistanceFromCurrentLocation:(NSString *)text;
@end
