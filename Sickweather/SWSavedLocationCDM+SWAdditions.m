//
//  SWSavedLocationCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 5/15/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSavedLocationCDM+SWAdditions.h"

@implementation SWSavedLocationCDM (SWAdditions)

+ (void)clearTable
{
    /*
    if (NO)
    {
        NSFetchRequest *savedLocations = [[NSFetchRequest alloc] init];
        [savedLocations setEntity:[NSEntityDescription entityForName:@"SWSavedLocationCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]]];
        [savedLocations setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError *error = nil;
        NSArray *records = [[SWHelper helperManagedObjectContext] executeFetchRequest:savedLocations error:&error];
        for (NSManagedObject *savedLocation in records)
        {
            [[SWHelper helperManagedObjectContext] deleteObject:savedLocation];
        }
        NSError *saveError = nil;
        [[SWHelper helperManagedObjectContext] save:&saveError];
    }
     */
}

+ (void)savedLocationDeleteRecordWithId:(NSNumber *)savedLocationId
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"SWSavedLocationCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]]];
    request.predicate = [NSPredicate predicateWithFormat:@"savedLocationId = %@", savedLocationId];
    [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *records = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    for (NSManagedObject *savedLocation in records)
    {
        [[SWHelper helperManagedObjectContext] deleteObject:savedLocation];
    }
    NSError *saveError = nil;
    [[SWHelper helperManagedObjectContext] save:&saveError];
}

+ (SWSavedLocationCDM *)savedLocationForId:(NSNumber *)savedLocationId
{
    // Define return var
    SWSavedLocationCDM *savedLocation = nil;
    
    // Create and config database request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWSavedLocationCDM"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"savedLocationId" ascending:YES]]; // Could add name to order by alpha
    request.predicate = [NSPredicate predicateWithFormat:@"savedLocationId = %@", savedLocationId];
    
    // Execute fetch request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    if (!matches)
    {
        // Error
    }
    else if ([matches count] == 0)
    {
        // Found nothing, return nil
    }
    else
    {
        // Found match, return it
        savedLocation = [matches lastObject];
    }
    
    // Return object
    return savedLocation;
}

+ (SWSavedLocationCDM *)createSavedLocationForPlacemark:(CLPlacemark *)placemark
{
    // Select max id, then increment by one then use that one...
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SWSavedLocationCDM"];
    fetchRequest.fetchLimit = 1;
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"savedLocationId" ascending:NO]];
    NSError *error;
    NSArray *records = [[SWHelper helperManagedObjectContext] executeFetchRequest:fetchRequest error:&error];
    NSNumber *nextId = [NSNumber numberWithInteger:1];
    if ([records count] == 1)
    {
        SWSavedLocationCDM *savedLocation = (SWSavedLocationCDM *)[records lastObject];
        nextId = savedLocation.savedLocationId;
        nextId = [NSNumber numberWithInteger:[nextId integerValue] + 1];
    }
    
    // Create new
    SWSavedLocationCDM *savedLocation = [NSEntityDescription insertNewObjectForEntityForName:@"SWSavedLocationCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
    NSData *placemarkAsData = [NSKeyedArchiver archivedDataWithRootObject:placemark];
    savedLocation.savedLocationId = nextId;
    savedLocation.placemarkObjectData = placemarkAsData;
    return savedLocation;
}

+ (NSArray *)savedLocations
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SWSavedLocationCDM"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"savedLocationId" ascending:NO]];
    
    NSError *error;
    NSArray *records = [[SWHelper helperManagedObjectContext] executeFetchRequest:fetchRequest error:&error];
    return records;
}

- (CLPlacemark *)placemark
{
    CLPlacemark *placemarkAsObjectFromData = [NSKeyedUnarchiver unarchiveObjectWithData:self.placemarkObjectData];
    return placemarkAsObjectFromData;
}

@end
