//
//  SWIllnessCDM.h
//  Sickweather
//
//  Created by John Erck on 2/10/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SWReportCDM;

@interface SWIllnessCDM : NSManagedObject

@property (nonatomic, retain) NSString * illnessDescription;
@property (nonatomic, retain) NSString * isGroupIllness;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * selfReportText;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) NSSet *report;
@end

@interface SWIllnessCDM (CoreDataGeneratedAccessors)

- (void)addReportObject:(SWReportCDM *)value;
- (void)removeReportObject:(SWReportCDM *)value;
- (void)addReport:(NSSet *)values;
- (void)removeReport:(NSSet *)values;

@end
