//
//  SWFamilyMemberModal.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 03/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWFamilyMemberModal : NSObject

@property (strong, nonatomic) NSString *family_id;
@property (strong, nonatomic) NSString *member_id;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *name_first;
@property (strong, nonatomic) NSString *name_last;
@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *photo_original;
@property (strong, nonatomic) NSString *photo_thumb;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *birthdate;
@property (strong, nonatomic) NSString *confirmed;
- (NSString *)birthDateDayForServer;
- (NSString *)birthDateMonthForServer;
- (NSString *)birthDateYearForServer;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
