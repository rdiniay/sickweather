//
//  SWMessageBannerView.h
//  Sickweather
//
//  Created by John Erck on 9/25/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWMessageBannerViewDelegate <NSObject>
- (void)messageBannerViewWantsToDismiss:(id)sender;
@end

@interface SWMessageBannerView : UIView
@property (weak, nonatomic) id <SWMessageBannerViewDelegate> delegate;
- (void)setDismissMethodAsAuto;
- (void)setDismissMethodAsButton;
- (void)setMessage:(NSString *)message;
@end
