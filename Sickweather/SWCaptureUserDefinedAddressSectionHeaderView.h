//
//  SWCaptureUserDefinedAddressSectionHeaderView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWCaptureUserDefinedAddressSectionHeaderView;

@protocol SWCaptureUserDefinedAddressSectionHeaderViewDelegate
- (NSString *)captureUserDefinedAddressSectionHeaderViewTitleString:(SWCaptureUserDefinedAddressSectionHeaderView *)sender;
@end

@interface SWCaptureUserDefinedAddressSectionHeaderView : UIView
@property (nonatomic, weak) id<SWCaptureUserDefinedAddressSectionHeaderViewDelegate> delegate;
@property (assign) NSUInteger sectionIndex;
@end
