//
//  UIImageView+SDLoadImage.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 03/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (SDLoadImage)
-(void)sdLoadImageWith:(NSString*)url placeholder:(UIImage*)placeholder;
@end
