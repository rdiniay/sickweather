//
//  SWSickCloudDetailViewController.h
//  Sickweather
//
//  Created by John Erck on 5/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWCustomMapKitAnnotation.h"

@interface SWSickCloudDetailViewController : UIViewController
@property (strong, nonatomic) UIImage *mapScreenshotImage;
@property (strong, nonatomic) SWCustomMapKitAnnotation *customMapKitAnnotation;
@end
