//
//  SWAppDelegate.h
//  Sickweather
//
//  Created by John Erck on 9/20/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SWUserCDM.h"
#import "SWAppBackend.h"
#import "SWGeocodeManager.h"
#import "SWSignInWithFacebookViewController.h"
#import <GooglePlaces/GooglePlaces.h>

typedef void (^SWAppDelegateArrayResponse)(NSArray *array);

@interface SWAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIManagedDocument *managedDocument; // Database connection
@property (strong, nonatomic) SWAppBackend *sickweatherAppBackend;
@property (strong, nonatomic) NSString *appBackendSickweatherAPIKey;
@property (strong, nonatomic) SWGeocodeManager *geocodeManager;
@property (strong, nonatomic) CLLocationManager *locationManager;

// HELPERS
- (void)helperInterpretCustomDeepLinkURL:(BOOL)fromBackground;
- (UIImageView *)launchScreenView;

// PROCESS ILLNESSES
- (void)getIllnessesSynchronouslyFollowedByAsynchronousNetworkUpdate:(SWAppDelegateArrayResponse)update; // of SWIllnessCDM
- (NSArray *)getReportsSynchronouslyFollowedByAsynchronousNetworkUpdate:(SWAppDelegateArrayResponse)update; // of SWReportCDM

// PROCESS USER
- (void)appDelegateUpdateUserBasedOnNetwork; // This is where we put all the code that happens "after they log in or on each app open if they are logged in"
- (void)appDelegateUpdateUserLaskKnownCurrentLocationPlacemarkUsingBestMethodAvailableFinishingWithDidUpdateCurrentLocationPlacemarkNotfification;

// PROCESS GET SICKSCORE ENDPOINT AND MANAGE FILES
- (NSDictionary *)getLatestCurrentLocationSickScoreSynchronouslyFromDisk;
- (NSDictionary *)updateLatestCurrentLocationSickScoreFromNetworkSavingToDiskFinishingWithAsynchronousNotificationUsingPlacemark:(CLPlacemark *)placemark;
- (NSDictionary *)updateLatestCurrentLocationCovidScoreFromNetworkSavingToDiskFinishingWithAsynchronousNotificationUsingPlacemark:(CLPlacemark *)placemark;

// PROCESS USERS SAVED PLACEMARKS
- (void)addPlacemarkToPlacesFile:(CLPlacemark *)placemark;
- (void)addGooglePlacemarkToPlacesFile:(GMSPlace *)place;
- (NSDictionary *)getPlacemarksFromPlacesFile;
- (void)savePlacemarksToPlacesFile:(NSDictionary *)placemarks;
- (void)initializeXmodeSdk;
- (void)initializeXmodeSdkOnLaunch;
@end
