//
//  SWMenuViewController.m
//  Sickweather
//
//  Created by John Erck on 8/21/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "Flurry.h"
#import "SWMenuViewController.h"
#import "SWMenuViewControllerCollectionViewLayout.h"
#import "SWMenuViewControllerStandardItemCollectionViewCell.h"
#import "SWMenuViewControllerStandardSectionHeaderCollectionReusableView.h"
#import "SWNavigationController.h"
#import "SWAboutViewController.h"
#import "SWGroupProfileViewController.h"
#import "SWMyProfileViewController.h"
#import "SWShareAppWithFriendsTableViewController.h"
#import "SWCreateGroupViewController.h"
#import "SWAlertSettingsViewController.h"

@interface SWMenuViewController () <SWMenuViewControllerStandardItemCollectionViewCellDelegate, SWMenuViewControllerCollectionViewLayoutDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SWSignInWithFacebookViewControllerDelegate, SWAboutViewControllerDelegate, SWGroupProfileViewControllerDelegate, SWMyProfileViewControllerDelegate, SWShareAppWithFriendsTableViewControllerDelegate, SWMenuViewControllerStandardSectionHeaderCollectionReusableViewDelegate, SWCreateGroupViewControllerDelegate, SWAlertSettingsViewControllerDelegate>
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) SWMenuViewControllerCollectionViewLayout *customCollectionViewLayout;
@property (strong, nonatomic) NSMutableDictionary *menuData; // menu.json
@property (assign) BOOL isPresentingFullScreenModalAndWantsStatusBarHidden;
@property (strong, nonatomic) NSString *currentSearchStringText;
@property (strong, nonatomic) SWMenuViewControllerStandardSectionHeaderCollectionReusableView *currentSearchHeaderView;
@property (strong, nonatomic) NSString *currentMyGroupsSectionDefaultRecordTitle;
@property (strong, nonatomic) NSDictionary *latestServerResponse;
@end

@implementation SWMenuViewController

#pragma mark - Public Methods

- (void)loadGroupAndPresentProfilePageForSWFoursquareGroupId:(NSString *)swFoursquareGroupId
{
    //NSLog(@"swFoursquareGroupId = %@", swFoursquareGroupId);
    SWGroupProfileViewController *groupProfileController = [[SWGroupProfileViewController alloc] init];
    groupProfileController.delegate = self;
    groupProfileController.serverGroupDict = @{};
    groupProfileController.latestMenuServerResponse = @{};
    SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:groupProfileController];
    [self presentViewController:navVC animated:YES completion:^{}];
    [[SWHelper helperAppBackend] appBackendGetGroupProfileUsingArgs:@{@"foursquare_id":swFoursquareGroupId,@"start_index":@"0",@"per_page":@"10"} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"responseBodyAsString = %@, requestURLAbsoluteString = %@", responseBodyAsString, requestURLAbsoluteString);
            [groupProfileController setThenLoadGetGroupProfileJSONObject:(NSMutableDictionary *)jsonResponseBody];
        });
    }];
}

#pragma mark - Custom Getters/Setters
- (NSString *)currentMyGroupsSectionDefaultRecordTitle
{
    // INIT DEFAULTS
    _currentMyGroupsSectionDefaultRecordTitle = @"Log in to start following groups...";
    SWUserCDM *loggedInUser = [SWUserCDM currentlyLoggedInUser];
    if ([loggedInUser isKindOfClass:[SWUserCDM class]])
    {
        if (self.currentSearchStringText.length > 0)
        {
            _currentMyGroupsSectionDefaultRecordTitle = [NSString stringWithFormat:@"None of your groups match \"%@\"...", self.currentSearchStringText];
        }
        else
        {
            if (self.latestServerResponse)
            {
                _currentMyGroupsSectionDefaultRecordTitle = @"You aren't following any groups yet...";
            }
            else
            {
                _currentMyGroupsSectionDefaultRecordTitle = @"Loading...";
            }
        }
    }
    return _currentMyGroupsSectionDefaultRecordTitle;
}

#pragma mark - Static Constants

static NSString * const menuViewControllerStandardItemCollectionViewCellReuseIdentifier = @"menuViewControllerStandardItemCollectionViewCellReuseIdentifier";
static NSString * const menuViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier = @"menuViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier";
//static NSString * const footerViewReuseIdentifier = @"FooterView";

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self helperIsAccountSettingsSection:indexPath])
    {
        if (indexPath.item == 0)
        {
            // My profile, or login
            if ([SWUserCDM currentlyLoggedInUser])
            {
                // My profile
                [Flurry logEvent:@"Menu - My Profile Button Tapped"];
                SWMyProfileViewController *myProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWMyProfileViewController"];
                myProfileVC.delegate = self;
                SWNavigationController *myProfileNavVC = [[SWNavigationController alloc] initWithRootViewController:myProfileVC];
                [self presentViewController:myProfileNavVC animated:YES completion:^{}];
            }
            else
            {
                // Login
                [self helperLaunchLoginFlow];
            }
        }
        else if (indexPath.item == 1)
        {
            // Manage alerts
            [Flurry logEvent:@"Menu - Manage Alerts Button Tapped"];
            SWAlertSettingsViewController *alertSettingsVC = [[SWAlertSettingsViewController alloc] init];
            alertSettingsVC.delegate = self;
            SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:alertSettingsVC];
            [self presentViewController:navVC animated:YES completion:^{}];
//            [self.navigationController pushViewController:alertSettingsVC animated:YES];
        }
        else if (indexPath.item == 2)
        {
            // Invite your friends
            [Flurry logEvent:@"Menu - Invite Your Friends Button Tapped"];
            SWShareAppWithFriendsTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWShareAppWithFriendsTableViewController"];
            [vc configureForDayToDayUse];
            vc.delegate = self;
            SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:vc];
            [self presentViewController:navVC animated:YES completion:^{}];
        }
        else if (indexPath.item == 3)
        {
            // About
            [Flurry logEvent:@"Menu - About Button Tapped"];
            SWAboutViewController *aboutController = [[SWAboutViewController alloc] init];
            aboutController.delegate = self;
            SWNavigationController *aboutNavigationController = [[SWNavigationController alloc] initWithRootViewController:aboutController];
            [self presentViewController:aboutNavigationController animated:YES completion:^{}];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSUInteger itemsCount = 0;
    NSInteger index = 0;
    for (NSDictionary *sectionDict in [[self.menuData objectForKey:@"menu"] objectForKey:@"sections"])
    {
        if (index == section)
        {
            itemsCount = [[sectionDict objectForKey:@"rows"] count];
            break;
        }
        index++;
    }
    return itemsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:menuViewControllerStandardItemCollectionViewCellReuseIdentifier forIndexPath:indexPath];
    if ([cell isKindOfClass:[SWMenuViewControllerStandardItemCollectionViewCell class]])
    {
        SWMenuViewControllerStandardItemCollectionViewCell *standardItemCell = (SWMenuViewControllerStandardItemCollectionViewCell *)cell;
        standardItemCell.delegate = self;
        [standardItemCell setTitleAttributedString:[self titleAttributedStringForIndexPath:indexPath]];
        if ([self helperIsAccountSettingsSection:indexPath])
        {
            if (indexPath.item == 0)
            {
                // My account settings
                [standardItemCell showTopBorder];
                [standardItemCell setIconImageName:@"menu-icon-my-account"];
            }
            else if (indexPath.item == 1)
            {
                // Manage alerts
                [standardItemCell setIconImageName:@"menu-icon-manage-alerts"];
            }
            else if (indexPath.item == 2)
            {
                // Invite your friends
                [standardItemCell setIconImageName:@"menu-icon-invite-friends"];
            }
            else if (indexPath.item == 3)
            {
                // About
                [standardItemCell setIconImageName:@"menu-icon-about"];
            }
        }
        else
        {
            [standardItemCell hideIconImageView];
        }
    }
    //cell.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        SWMenuViewControllerStandardSectionHeaderCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:menuViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier forIndexPath:indexPath];
        headerView.delegate = self;
        [headerView prepareForReuse];
        headerView.styleStringKey = @"regularSectionStyle";
        [headerView setTitle:[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"title"]];
        reusableview = headerView;
    }
    if (kind == UICollectionElementKindSectionFooter)
    {
        /*
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerViewReuseIdentifier forIndexPath:indexPath];
        reusableview = footerview;
         */
    }
    return reusableview;
}

#pragma mark - SWAlertSettingsViewControllerDelegate

- (void)dismissAlertSettings:(SWAlertSettingsViewController *)vc
{
    [vc dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - SWMyProfileViewControllerDelegate

- (void)myProfileViewControllerWantsToPopOffTheStack:(SWMyProfileViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateLoginRelatedRecords]; // The user could have logged out
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - SWSignInWithFacebookViewControllerDelegate

- (void)didDismissWithSuccessfulLogin:(SWSignInWithFacebookViewController *)vc
{
    self.isPresentingFullScreenModalAndWantsStatusBarHidden = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"Back to menu");
        [self helperUpdateLoginRelatedRecords];
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

- (void)didDismissWithoutLoggingIn:(SWSignInWithFacebookViewController *)vc
{
    self.isPresentingFullScreenModalAndWantsStatusBarHidden = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"Back to menu");
        [self helperUpdateLoginRelatedRecords];
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - SWMenuViewControllerCollectionViewLayoutDelegate

- (NSAttributedString *)menuViewControllerCollectionViewLayoutDelegateAttributedTitleStringForStandardItemCollectionViewCell:(NSIndexPath *)indexPath
{
    return [self titleAttributedStringForIndexPath:indexPath];
}

- (CGFloat)sectionHeaderPreferredVerticalHeightForIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)totalExtraTitleMarginNeededForIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *titleLeftMargin = @(50);
    NSNumber *titleRightMargin = @(16); /* THESE VALUES NEED TO BE IN SYNC WITH PARTNER LAYOUT FILE'S SETTINGS */
    if (![self helperIsAccountSettingsSection:indexPath])
    {
        titleLeftMargin = @(10);
        if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
        {
            titleRightMargin = @(0);
        }
    }
    return titleLeftMargin.floatValue + titleRightMargin.floatValue;
}

#pragma mark - SWMenuViewControllerStandardItemCollectionViewCellDelegate

- (void)menuViewControllerStandardItemCollectionViewCellDelegateExampleAction1:(SWMenuViewControllerStandardItemCollectionViewCell *)cell
{
    //NSLog(@"menuViewControllerStandardItemCollectionViewCellDelegateExampleAction1");
}

#pragma mark - SWShareAppWithFriendsTableViewControllerDelegate

- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulMessageSend:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismissBySkipping:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismiss:(SWShareAppWithFriendsTableViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}


#pragma mark - SWMenuViewControllerStandardSectionHeaderCollectionReusableViewDelegate

- (void)searchBarTextDidChange:(NSString *)searchText
{
    //NSLog(@"searchText = %@", searchText);
    [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;
{
    // Log to Flurry
    [Flurry logEvent:@"Search All Goups, Search Bar Activated"];
}

- (void)createNewGroupButtonTapped:(SWMenuViewControllerStandardSectionHeaderCollectionReusableView *)sender
{
    // Log to Flurry
    [Flurry logEvent:@"Create New Group Button Tapped"];
    if ([SWUserCDM currentlyLoggedInUser])
    {
        SWCreateGroupViewController *vc = [[SWCreateGroupViewController alloc] init];
        vc.delegate = self;
        SWNavigationController *navVC = [[SWNavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navVC animated:YES completion:^{}];
    }
    else
    {
        [self helperLaunchLoginFlow];
    }
}

#pragma mark - SWCreateGroupViewControllerDelegate

- (void)createGroupViewControllerWantsToDismissDueToUserCancel:(SWCreateGroupViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)createGroupViewControllerWantsToDismissAfterSuccessfullyCreatingNewGroup:(SWCreateGroupViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - SWAboutViewControllerDelegate

- (void)aboutViewControllerWantsToDismiss
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWGroupProfileViewControllerDelegate

- (void)groupProfileViewControllerWantsToDismiss
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:self.currentSearchStringText];
    }];
}

#pragma mark - Helpers

- (void)loadJSONData
{
    self.menuData = (NSMutableDictionary *)[SWHelper helperLoadJSONObjectFromBundleDocsUsingName:@"menu" forceFeedFromBundle:YES];
    [self helperUpdateLoginRelatedRecords];
}

- (void)helperUpdateLoginRelatedRecords
{
    NSMutableArray *accountSettingsRows = [[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:ACCOUNT_SETTINGS_SECTION_HEADER_INDEX] objectForKey:@"rows"];
    [accountSettingsRows removeObjectAtIndex:0];
    if ([SWUserCDM currentlyLoggedInUser])
    {
        [accountSettingsRows insertObject:@{@"title":@"My profile"} atIndex:0];
    }
    else
    {
        [accountSettingsRows insertObject:@{@"title":@"Log in/Create account"} atIndex:0];
    }
    [self.collectionView reloadData];
}

- (NSMutableArray *)helperRowsInSection:(NSIndexPath *)indexPath
{
    NSMutableArray *rowsInSection = [[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"];
    return rowsInSection;
}

- (void)helperLaunchLoginFlow
{
    [Flurry logEvent:@"Menu - Log In/Create Account Button Tapped"];
    self.isPresentingFullScreenModalAndWantsStatusBarHidden = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    SWSignInWithFacebookViewController *signInWithFacebookViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
    signInWithFacebookViewController.delegate = self;
    SWNavigationController *signInWithFacebookNavigationController = [[SWNavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
    [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
}

- (NSAttributedString *)titleAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    NSAttributedString *answer;
    if ([self helperIsDefaultEmptyMyGroupsRecord:indexPath])
    {
        NSDictionary *dict = [[self helperRowsInSection:indexPath] objectAtIndex:indexPath.item];
        [dict setValue:self.currentMyGroupsSectionDefaultRecordTitle forKey:@"title"];
        answer = [[NSAttributedString alloc] initWithString:[[[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"title"] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
    }
    else
    {
        NSString *title = [[[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"title"];
        if ([title isEqualToString:@"Loading..."])
        {
            answer = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
        }
        else
        {
            answer = [[NSAttributedString alloc] initWithString:[[[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"title"] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226]}];
        }
    }
    return answer;
}

- (NSAttributedString *)addressAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"[[[[[[self.menuData objectForKey:@\"menu\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] objectForKey:@\"rows\"] objectAtIndex:indexPath.item] objectForKey:@\"address\"] = %@", [[[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item] objectForKey:@"address"]);
    //NSLog(@"[[[[[self.menuData objectForKey:@\"menu\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] objectForKey:@\"rows\"] objectAtIndex:indexPath.item] = %@", [[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item]);
    //NSLog(@"[[[[self.menuData objectForKey:@\"menu\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] objectForKey:@\"rows\"] = %@", [[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"]);
    //NSLog(@"[[[self.menuData objectForKey:@\"menu\"] objectForKey:@\"sections\"] objectAtIndex:indexPath.section] = %@", [[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section]);
    //NSLog(@"[[self.menuData objectForKey:@\"menu\"] objectForKey:@\"sections\"] = %@", [[self.menuData objectForKey:@"menu"] objectForKey:@"sections"]);
    //NSLog(@"[self.menuData objectForKey:@\"menu\"] = %@", [self.menuData objectForKey:@"menu"]);
    //NSLog(@"self.menuData = %@", self.menuData);
    NSDictionary *object = [[[[[self.menuData objectForKey:@"menu"] objectForKey:@"sections"] objectAtIndex:indexPath.section] objectForKey:@"rows"] objectAtIndex:indexPath.item];
    NSString *address = [object objectForKey:@"address"];
    if (!address || [address isKindOfClass:[NSNull class]])
    {
        address = @"";
    }
    else
    {
        /*
         // If we had lat/lon per group we could do this...
        CLLocation *currentLocation = [SWHelper helperLocationManager].location;
        CLLocation *markerLocation = [[CLLocation alloc] initWithLatitude:[[annotation.serverDict objectForKey:@"lat"] doubleValue] longitude:[[annotation.serverDict objectForKey:@"lon"] doubleValue]];
        CLLocationDistance distance = [currentLocation distanceFromLocation:markerLocation];
        NSString *distanceFromCurrentLocation = [NSString stringWithFormat:@"%.1f mi",(distance/1609.344)];
        //NSLog(@"Calculated Miles %@", distanceFromCurrentLocation);
        [popupView setDistanceFromCurrentLocation:distanceFromCurrentLocation];
        */
    }
    return [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:10],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
}

- (BOOL)helperIsAccountSettingsSection:(NSIndexPath *)indexPath
{
    return indexPath.section == ACCOUNT_SETTINGS_SECTION_HEADER_INDEX;
}

- (BOOL)helperIsDefaultEmptyMyGroupsRecord:(NSIndexPath *)indexPath
{
    BOOL answer = NO;
    return answer;
}

- (void)helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:(NSString *)searchText
{
    
}

#pragma mark - Public Methods

#pragma mark - Life Cycle Methods

- (BOOL)prefersStatusBarHidden
{
    if (self.isPresentingFullScreenModalAndWantsStatusBarHidden == YES)
    {
        return YES;
    }
    if (self.view.bounds.size.width > self.view.bounds.size.height)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self helperUpdateLoginRelatedRecords];
}

- (void)viewDidDisappear:(BOOL)animated
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view endEditing:YES];
    });
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSWDidUpdateCurrentLocationPlacemarkNotification object:nil];
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // Tell us when the user's current location has been translated to a placemark object (if ever, permissions, etc)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:)
                                                 name:kSWDidUpdateCurrentLocationPlacemarkNotification
                                               object:nil];
    
    // LOAD JSON DATA
    [self loadJSONData];
    
    // PRE LOAD GROUPS DATA
    [self helperUpdateGroupsUsingWebServiceUsingUserCurrentLocationAndSearchText:@""];
    
    // PRE INIT
    self.customCollectionViewLayout = [[SWMenuViewControllerCollectionViewLayout alloc] init];
    self.customCollectionViewLayout.delegate = self;
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.customCollectionViewLayout];
    self.collectionView.delegate = self;
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.collectionView forKey:@"collectionView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.collectionView];
    
    // LAYOUT
    
    // Layout COLLECTION VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config NAVIGATION BAR
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.title = @"Menu";
    
    // Config COLLECTION VIEW
    [self.collectionView registerClass:[SWMenuViewControllerStandardItemCollectionViewCell class] forCellWithReuseIdentifier:menuViewControllerStandardItemCollectionViewCellReuseIdentifier];
    [self.collectionView registerClass:[SWMenuViewControllerStandardSectionHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:menuViewControllerStandardSectionHeaderCollectionReusableViewReuseIdentifier];
    //    [self.collectionView registerClass:[SWChecklistCollectionReusableViewFooter class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:footerViewReuseIdentifier];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    // COLOR VIEWS
}

@end
