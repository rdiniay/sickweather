//
//  SWTemperatureAlertView.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 08/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWTemperatureView.h"
#import "SWTemperatureGuageView.h"

@interface SWTemperatureView (){
	float temp;
}
@property float *savedTemperature;

@property (weak, nonatomic) IBOutlet UIButton *saveTemperatureButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelbutton;
@property (weak, nonatomic) IBOutlet UILabel *healthStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureStatusLabel;
-(void) cancelView:(UIGestureRecognizer*)tapGes;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet SWTemperatureGuageView *temperatureGuageView;

@end

@implementation SWTemperatureView

- (void)initializeWithTemperature:(float)temperature withTemperatureScale:(NSString*)tempScale
{
    // Adding tap outside view area.
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelView:)];
	tapGes.cancelsTouchesInView =  NO;
    [self addGestureRecognizer:tapGes];
    
    // Setting Labels
    self.temperatureStatusLabel.attributedText = [self getFormattedTemperatureString:0.0 withTemperatureScale:tempScale];
    float animationPeriod = 0.5;
    if (self.savedTemperature != &(temperature)){
        self.healthStatusLabel.text = @"";
        self.temperatureGuageView.temperature = 0.0;
        [self.temperatureGuageView setNeedsDisplay];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            for (int i = self.savedTemperature == nil ? 0 : (int)self.savedTemperature; i < (int)temperature; i ++) {
                usleep(animationPeriod/100 * 1000000); // sleep in microseconds
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.temperatureStatusLabel.attributedText = [self getFormattedTemperatureString:(float)i withTemperatureScale:tempScale];
                    self.temperatureGuageView.temperature = i;
                    [self.temperatureGuageView setNeedsDisplay];
                });
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                self.temperatureStatusLabel.attributedText = [self getFormattedTemperatureString:temperature withTemperatureScale:tempScale];
                float x = temperature;
				BOOL isCelsius = [tempScale isEqualToString:@"C"] ? true : false;
                self.savedTemperature = &(x);
				self.healthStatusLabel.text = isCelsius ? [SWHelper updateFeverSeverityWithCelcius:temperature] : [SWHelper updateFeverSeverityWithFarenheit:temperature];
                self.temperatureGuageView.temperature = temperature;
				self.temperatureGuageView.isCelsiusTemperatureScaleType = isCelsius;
                [self.temperatureGuageView setNeedsDisplay];
            });
        });
    }
    
    // Buttons Styling...
    self.saveTemperatureButton.tintColor = [UIColor whiteColor];
    self.saveTemperatureButton.layer.cornerRadius = 5;
    self.saveTemperatureButton.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    
    self.cancelbutton.tintColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
    self.cancelbutton.layer.cornerRadius = 6;
    self.cancelbutton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
    self.cancelbutton.layer.borderWidth = 2.0;
}

-(NSAttributedString*)getFormattedTemperatureString:(float)temperature withTemperatureScale:(NSString*)type
{
	temp = temperature;
    NSString *temperatureString = [[NSString alloc] initWithFormat:@"%.01f°%@",(float)temperature,type];
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString: temperatureString];
    [attributedString addAttribute:NSBaselineOffsetAttributeName
                             value:@(15.0)
                             range:NSMakeRange(attributedString.length-1, 1)];
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]
                             range:NSMakeRange(attributedString.length-2, 2)];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:@"raleway-semibold" size:20]
                             range:NSMakeRange(attributedString.length-1, 1)];
    return attributedString;
}

-(void) cancelView:(UIGestureRecognizer*)tapGes
{
    if (tapGes == nil) {
        [self.delegate performViewCancel];
        return;
    }
    CGPoint location = [tapGes locationInView:self.contentView];
    if(location.x < 0 || location.y < 0){
        [self.delegate performViewCancel];
        return;
    }
    if(location.x > self.contentView.frame.size.width || location.y > self.contentView.frame.size.height){
        [self.delegate performViewCancel];
        return;
    }
}

- (IBAction)onCancel:(id)sender {
    [self cancelView:nil];
}
- (IBAction)onSaveTemperatureButton:(id)sender {
	[self.delegate saveTemperatureWith:temp severity:self.healthStatusLabel.text];
	[self cancelView:nil];
}
@end
