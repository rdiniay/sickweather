//
//  SWConnectWithDoctorViewController.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 19/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWConnectWithDoctorViewController.h"
#import "SWConnectWithDoctorView.h"

@interface SWConnectWithDoctorViewController () <SWConnectWithDoctorViewDelegate>

@property SWConnectWithDoctorView *connectWithDoctorView;
@end

@implementation SWConnectWithDoctorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Connect with a Doctor";
	// Config LEFT BAR BUTTON ITEM
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    // Setting view
    self.connectWithDoctorView = [[NSBundle mainBundle] loadNibNamed:@"ConnectWithDoctorView" owner:nil options:nil][0];
    self.connectWithDoctorView.delegate = self;
    [self.view addSubview:self.connectWithDoctorView];
    self.connectWithDoctorView.frame = self.view.frame;
    self.connectWithDoctorView.imageHeightConstraint.constant = self.view.frame.size.width * 2 / 3 ;
    [self.connectWithDoctorView initialize];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	[self.navigationController popViewControllerAnimated:true];
}

#pragma mark - ConnectWithDoctor Delegates

- (void)signUp {
	[SWHelper logFlurryEventsWithTag:@"Sign Up for WellVia tapped"];
    NSURL *url = [NSURL URLWithString:@"http://sick.io/wellvia"];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)launchApp {
    NSURL *appURL = [NSURL URLWithString:@"wellvia://"]; // in case user installs app after coming to this view controller
    
    if ([[UIApplication sharedApplication] canOpenURL:appURL])
    {
        [[UIApplication sharedApplication] openURL:appURL];
    }
    else
    {
        NSURL *appStoreURL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/wellvia/id1114544465?mt=8"];
        [[UIApplication sharedApplication] openURL:appStoreURL];
    }
}

@end
