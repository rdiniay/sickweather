//
//  SWGroupsViewControllerStandardItemCollectionViewCell.h
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWGroupsViewControllerStandardItemCollectionViewCell;

@protocol SWGroupsViewControllerStandardItemCollectionViewCellDelegate
- (void)groupsViewControllerStandardItemCollectionViewCellDelegateExampleAction1:(SWGroupsViewControllerStandardItemCollectionViewCell *)cell;
@end

@interface SWGroupsViewControllerStandardItemCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) id<SWGroupsViewControllerStandardItemCollectionViewCellDelegate> delegate;
- (void)setIconImageName:(NSString *)imageName;
- (void)setTitleAttributedString:(NSAttributedString *)titleAttributedString;
- (void)setAddressAttributedString:(NSAttributedString *)addressAttributedString;
- (void)showTopBorder;
- (void)hideIconImageView;
- (void)styleTitleForInfoMessage;
@end
