//
//  SWAPIFetcher.h
//  Sickweather
//
//  Created by John Erck on 3/3/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SWAppBackendJSONCompletionHandlerArrayResponse)(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString);
typedef void (^SWAppBackendJSONCompletionHandler)(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString);
typedef void (^SWAppBackendImageCompletionHandler)(UIImage *image, NSHTTPURLResponse *response, NSError *error);
typedef void (^SWAPIFetcherJSONCompletionHandler)(NSDictionary *jsonResponseBody, NSHTTPURLResponse *responseHeaders, NSError *error);
typedef void (^SWAPIFetcherImageCompletionHandler)(NSData *data, NSURLResponse *response, NSError *error);

@interface SWAppBackend : NSObject

@property (strong, nonatomic) NSString *appBackendSickweatherAPIKey;

- (void)appBackendWriteDataFileNamed:(NSString *)fileName fileContentType:(NSString *)fileContentType fileContent:(NSData *)fileContent usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendWriteFileNamed:(NSString *)fileName fileContent:(NSString *)fileContent usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendLogSelfReportToSickweatherServerForLat:(NSString *)lat lon:(NSString *)lon reportText:(NSString *)reportText identifier:(NSString *)identifier visible:(NSString *)visibleZeroOrOne usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendSubmitReportToSickweatherServerForLat:(NSString *)lat lon:(NSString *)lon reportText:(NSString *)reportText identifier:(NSString *)identifier visible:(NSString *)visibleZeroOrOne familyArgs:(NSMutableDictionary*)familyArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetForecastForLat:(NSString *)lat lon:(NSString *)lon loc:(NSString *)loc zip:(NSString *)zip usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetLocallyCachedImageForStringURL:(NSString *)stringURL alwaysUsingCompletionHandler:(SWAppBackendImageCompletionHandler)completionHandler;
- (void)appBackendGetSickScoreInRadiusForLat:(CGFloat)lat lon:(CGFloat)lon usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetCovidScoreForLat:(CGFloat)lat lon:(CGFloat)lon
    usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetCovidContentByKeyword:(NSString *)illnesId usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetContentByKeyword:(NSString *)illnesId usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendUpdateLocationForUserUsingLat:(CGFloat)lat lon:(CGFloat)lon extraArgs:(NSDictionary *)extraArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetCovidMarkersInRadiusForIllnessTypeID:illnessTypeId lat:(CGFloat)lat lon:(CGFloat)lon limit:(NSNumber *)limit usingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler;
- (void)appBackendGetMarkersInRadiusForIllnessTypeID:illnessTypeId lat:(CGFloat)lat lon:(CGFloat)lon limit:(NSNumber *)limit usingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler;
- (void)appBackendGetSponsoredMarkersInRadiusForIllnessIds:(NSString *)illnessIds lat:(NSString *)lat lon:(NSString *)lon limit:(NSString *)limit usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendSearchForMedicationUsingArgs:(NSString *)name usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetCovidIllnessesUsingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler;
- (void)appBackendGetIllnessesUsingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler;
- (void)appBackendGetSymptomsUsingCompletionHandler:(SWAppBackendJSONCompletionHandlerArrayResponse)completionHandler;
- (void)appBackendGetTrackerTypersUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetFamilyIdUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetSelfReportsUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetUserProfileUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetUserLocationViaIPAddressUsingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendDeleteFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendAddFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendRequestToAddFamilyMemberUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendUploadFamilyMemberProfilePicUsingArgs:(NSDictionary*)serverArgs Image:(UIImage *)image usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetEventsUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetAlertsUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendUpdateUserProfileUsingArgs:(NSDictionary *)serverArgs usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendUpdateUserProfilePicImage:(UIImage *)image usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetGroupsUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendGetGroupProfileUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendSetPushIdUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendClaimGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendReportGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendReportUserOfGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendRemoveGroupAdminUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendTurnGroupNotificationsOnOffUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendSubmitMessageToGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendCreateGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendDeleteGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendAddSympathyToGroupMessageUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendFollowGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendUnfollowGroupUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendDeleteGroupMessageUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendDeleteReportUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendBlockGroupFollowerUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendAddGroupAdminUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendSubmitReportsToGroupWithMessageUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;
- (void)appBackendAddTrackerUsingArgs:(NSDictionary *)serverArgs completionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;

- (void)fetchIllnessDefinitionForIllnessTypeId:(NSString *)illnessTypeId usingCompletionHandler:(SWAPIFetcherJSONCompletionHandler)completionHandler;

- (void)signIntoSickweatherUsingFacebookAccessToken:(NSString *)facebookAccessToken
                             usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;

- (void)signIntoSickweatherUsingEmail:(NSString *)email
                             password:(NSString *)password
               usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;

- (void)verifySickweatherEmailHash:(NSString *)emailHash
                             andPasswordHash:(NSString *)passwordHash
               usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;

- (void)createSickweatherAccountUsingFirstName:(NSString *)firstName
                                      lastName:(NSString *)lastName
                                         email:(NSString *)email
                                      password:(NSString *)password
                        usingCompletionHandler:(SWAppBackendJSONCompletionHandler)completionHandler;

- (UIImage *)fetchImage:(NSString *)link usingCompletionHandler:(SWAPIFetcherImageCompletionHandler)completionHandler;

@end
