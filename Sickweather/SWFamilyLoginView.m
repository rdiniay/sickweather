//
//  SWFamilyLoginView.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 11/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyLoginView.h"

@interface SWFamilyLoginView ()
@end

@implementation SWFamilyLoginView

- (IBAction)onCreateAccountButton:(id)sender {
    [self.delegate createAnAccountButtonPressed];
}
- (IBAction)onLoginButton:(id)sender {
    [self.delegate loginButtonPressed];
}

@end
