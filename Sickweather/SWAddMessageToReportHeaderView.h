//
//  SWAddMessageToReportMessageTableViewHeader.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWAddMessageToReportHeaderView;

@protocol SWAddMessageToReportMessageTableViewHeaderDelegate
@end

@interface SWAddMessageToReportHeaderView : UIView
@property (nonatomic, weak) id<SWAddMessageToReportMessageTableViewHeaderDelegate> delegate;
@end
