//
//  SWEditMyProfileSectionHeaderView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWEditMyProfileSectionHeaderView;

@protocol SWEditMyProfileSectionHeaderViewDelegate
- (NSString *)editMyProfileSectionHeaderViewTitleString:(SWEditMyProfileSectionHeaderView *)sender;
@end

@interface SWEditMyProfileSectionHeaderView : UIView
@property (nonatomic, weak) id<SWEditMyProfileSectionHeaderViewDelegate> delegate;
@end
