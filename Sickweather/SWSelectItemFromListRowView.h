//
//  SWSelectItemFromListRowView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWSelectItemFromListRowView;

@protocol SWSelectItemFromListRowViewDelegate
- (NSString *)selectItemFromListRowViewTitleString:(SWSelectItemFromListRowView *)sender;
- (void)selectItemFromListRowWasTapped:(SWSelectItemFromListRowView *)sender;
@end

@interface SWSelectItemFromListRowView : UIView
@property (nonatomic, weak) id<SWSelectItemFromListRowViewDelegate> delegate;
@property (assign) NSUInteger sectionIndex;
@property (assign) NSUInteger rowIndex;
- (void)showBottomBorder;
- (void)showCheckmark;
@end
