//
//  SWSelectItemFromListSectionHeaderView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWSelectItemFromListSectionHeaderView;

@protocol SWSelectItemFromListSectionHeaderViewDelegate
- (NSString *)selectItemFromListSectionHeaderViewTitleString:(SWSelectItemFromListSectionHeaderView *)sender;
@end

@interface SWSelectItemFromListSectionHeaderView : UIView
@property (nonatomic, weak) id<SWSelectItemFromListSectionHeaderViewDelegate> delegate;
@property (assign) NSUInteger sectionIndex;
@end
