//
//  SWSponsoredMarkerMapKitAnnotationView.m
//  Sickweather
//
//  Created by John Erck on 3/31/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMarkerMapKitAnnotationView.h"
#import "SWSponsoredMarkerMapKitAnnotation.h"

@implementation SWSponsoredMarkerMapKitAnnotationView

- (UIView *)leftCalloutAccessoryView
{
    return nil;
    /*
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 100, 100);
    view.backgroundColor = [SWColors sickweatherBlue41x171x226];
    return view;
     */
    /*
    UIImage *image = [UIImage imageNamed:@"drive-icon"];
    UIButton *button = [[UIButton alloc] init];
    button.tag = [SWConstants drivingDirectionsLeftAccessoryControlTag];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button sizeToFit];
    button.frame = CGRectMake(0, 0, 50, 46);
    
    //button.frame = CGRectMake(0, 0, 40, 100);
    //button.backgroundColor = [UIColor redColor];
    //button.titleLabel.text = @"Left";
    return button;
     */
}

- (UIView *)rightCalloutAccessoryView
{
    return nil;
    /*
    UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    button.tag = [SWConstants launchSafariRightAccessoryControlTag];
    //button.frame = CGRectMake(0, 0, 30, 30);
    //button.backgroundColor = [UIColor purpleColor];
    //button.titleLabel.text = @"Right";
    return button;
    */
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        // Init code here
    }
    return self;
}

- (id)initWithCovidAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        // Init code here
        self.image = [UIImage imageNamed:@"map-marker-covid-public"];
    }
    return self;
}

@end
