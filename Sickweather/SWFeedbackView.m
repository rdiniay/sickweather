//
//  SWFeedbackView.m
//  Sickweather
//
//  Created by John Erck on 1/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWFeedbackView.h"

@interface SWFeedbackView ()
@property (strong, nonatomic) NSString *currentStateFlag;
@property (strong, nonatomic) UIColor *primaryTextColor;
@property (strong, nonatomic) UIColor *backgroundColor;
@property (strong, nonatomic) NSMutableDictionary *myViewsDictionary;
@property (strong, nonatomic) UIView *myBackgroundView;
@property (strong, nonatomic) UILabel *myTitleLabel;
@property (strong, nonatomic) UIView *myCenterView;
@property (strong, nonatomic) UIButton *noButton;
@property (strong, nonatomic) UIButton *yesButton;
@end

@implementation SWFeedbackView

#pragma mark - Public Methods

- (void)reset
{
    self.currentStateFlag = @"have-you-enjoyed-using-sickweather";
    self.myTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Enjoying Sickweather?" attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}];
    [self.noButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Not really" attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
    [self.yesButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Yes!" attributes:@{NSForegroundColorAttributeName:self.backgroundColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
}

- (void)resetUsingServerPromptDict:(NSDictionary *)serverPromptDict
{
    self.serverPromptDict = serverPromptDict;
    self.currentStateFlag = @"have-you-enjoyed-using-sickweather";
    self.myTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:[serverPromptDict objectForKey:@"message_text"] attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}];
    [self.noButton setAttributedTitle:[[NSAttributedString alloc] initWithString:[serverPromptDict objectForKey:@"no_title"] attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
    [self.yesButton setAttributedTitle:[[NSAttributedString alloc] initWithString:[serverPromptDict objectForKey:@"yes_title"] attributes:@{NSForegroundColorAttributeName:self.backgroundColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
}

#pragma mark - Helpers

- (NSInteger)fontSize
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 20; // Designed for iPad
    }
    else if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        //NSLog(@"height = %@", @([[UIScreen mainScreen] bounds].size.height));
        if ([[UIScreen mainScreen] bounds].size.height == 480)
        {
            return 14; // Designed for short tiny iPhone
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            return 14; // Designed for iPhone 5 like iPhone
        }
    }
    return 17; // Designed for iPhone 7
    
}

#pragma mark - Target/Action

- (void)yesButtonTapped:(id)sender
{
    [self.delegate feedbackViewYesButtonTapped:self];
    if ([self.currentStateFlag isEqualToString:@"have-you-enjoyed-using-sickweather"])
    {
        self.currentStateFlag = @"5-star-us-on-the-app-store";
        self.myTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Would you be willing to rate us?" attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}];
        [self.noButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"No, thanks" attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
        [self.yesButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Sure" attributes:@{NSForegroundColorAttributeName:self.backgroundColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
    }
    else if ([self.currentStateFlag isEqualToString:@"5-star-us-on-the-app-store"])
    {
        [self.delegate feedbackViewAppStoreRatingRequestedUserSaidYes:self];
    }
    else if ([self.currentStateFlag isEqualToString:@"would-you-mind-giving-us-some-feedback"])
    {
        [self.delegate feedbackViewFeedbackRequestedUserSaidYes:self];
    }
}

- (void)noButtonTapped:(id)sender
{
    [self.delegate feedbackViewNoButtonTapped:self];
    if ([self.currentStateFlag isEqualToString:@"have-you-enjoyed-using-sickweather"])
    {
        self.currentStateFlag = @"would-you-mind-giving-us-some-feedback";
        self.myTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Would you mind giving us some feedback?" attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}];
        [self.noButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"No, thanks" attributes:@{NSForegroundColorAttributeName:self.primaryTextColor,NSFontAttributeName:[SWFont fontStandardFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
        [self.yesButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Ok, sure" attributes:@{NSForegroundColorAttributeName:self.backgroundColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:[self fontSize]]}] forState:UIControlStateNormal];
    }
    else if ([self.currentStateFlag isEqualToString:@"5-star-us-on-the-app-store"])
    {
        [self.delegate feedbackViewAppStoreRatingRequestedUserSaidNo:self];
    }
    else if ([self.currentStateFlag isEqualToString:@"would-you-mind-giving-us-some-feedback"])
    {
        [self.delegate feedbackViewFeedbackRequestedUserSaidNo:self];
    }
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT DEFAULTS
        NSNumber *buttonWidth = [NSNumber numberWithInteger:120];
        NSNumber *buttonHeight = [NSNumber numberWithInteger:44];
        self.backgroundColor = [UIColor whiteColor];
        self.primaryTextColor = [SWColor colorSickweatherBlue41x171x226];
        
        // INIT VIEWS
        self.myBackgroundView = [[UIView alloc] init];
        self.myTitleLabel = [[UILabel alloc] init];
        self.myCenterView = [[UIView alloc] init];
        self.noButton = [[UIButton alloc] init];
        self.yesButton = [[UIButton alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        NSMutableDictionary *myViewsDictionary = [NSMutableDictionary new];
        [myViewsDictionary setObject:self.myBackgroundView forKey:@"myBackgroundView"];
        [myViewsDictionary setObject:self.myTitleLabel forKey:@"myTitleLabel"];
        [myViewsDictionary setObject:self.myCenterView forKey:@"myCenterView"];
        [myViewsDictionary setObject:self.noButton forKey:@"noButton"];
        [myViewsDictionary setObject:self.yesButton forKey:@"yesButton"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        self.myTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.myCenterView.translatesAutoresizingMaskIntoConstraints = NO;
        self.noButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.yesButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
        [self addSubview:self.myBackgroundView];
        [self.myBackgroundView addSubview:self.myTitleLabel];
        [self.myBackgroundView addSubview:self.myCenterView];
        [self.myBackgroundView addSubview:self.noButton];
        [self.myBackgroundView addSubview:self.yesButton];
        
        // LAYOUT
        NSArray *myConstraints = @[];
        
        // myBackgroundView
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[myBackgroundView]-(0)-|"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[myBackgroundView]-(0)-|"] options:0 metrics:0 views:myViewsDictionary]];
        
        // myTitleLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[myTitleLabel]-(20)-|"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[myTitleLabel]"] options:0 metrics:0 views:myViewsDictionary]];
        
        // myCenterView
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[myCenterView(1)]"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[myCenterView]-(0)-|"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.myCenterView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.myBackgroundView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        
        // noButton
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[noButton(buttonWidth)]-(10)-[myCenterView]"] options:0 metrics:@{@"buttonWidth":buttonWidth} views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[myTitleLabel]-(20)-[noButton(buttonHeight)]"] options:0 metrics:@{@"buttonHeight":buttonHeight} views:myViewsDictionary]];
        
        // yesButton
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[myCenterView]-(10)-[yesButton(buttonWidth)]"] options:0 metrics:@{@"buttonWidth":buttonWidth} views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[myTitleLabel]-(20)-[yesButton(buttonHeight)]-(20)-|"] options:0 metrics:@{@"buttonHeight":buttonHeight} views:myViewsDictionary]];
        
        // ADD CONSTRAINTS
        [self addConstraints:myConstraints];
        
        // CONFIG
        
        // myBackgroundView
        self.myBackgroundView.backgroundColor = self.backgroundColor;
        
        // myTitleLabel
        self.myTitleLabel.numberOfLines = 0;
        self.myTitleLabel.backgroundColor = self.backgroundColor;
        self.myTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        // myCenterView
        self.myCenterView.backgroundColor = [UIColor clearColor];
        
        // noButton
        self.noButton.backgroundColor = self.backgroundColor;
        self.noButton.layer.cornerRadius = 6;
        self.noButton.layer.borderColor = self.primaryTextColor.CGColor;
        self.noButton.layer.borderWidth = 1.0;
        [self.noButton addTarget:self action:@selector(noButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // yesButton
        self.yesButton.backgroundColor = self.primaryTextColor;
        self.yesButton.layer.cornerRadius = 6;
        self.yesButton.layer.borderColor = self.primaryTextColor.CGColor;
        self.yesButton.layer.borderWidth = 1.0;
        [self.yesButton addTarget:self action:@selector(yesButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // Reset (to init state)
        [self reset];
    }
    return self;
}

@end
