//
//  SWShareAppWithFriendsTableViewController.m
//  Sickweather
//
//  Created by John Erck on 1/28/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Flurry.h"
#import <MessageUI/MessageUI.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SWShareAppWithFriendsTableViewController.h"
#import "SWFacebookFriendTableViewCell.h"
#import "NSMutableDictionary+SWAdditions.h"
#import "SWAppDelegate.h"
#import "SWShareWithFacebookModel.h"
#import "SWShareWithContactModel.h"

@interface SWShareAppWithFriendsTableViewController () <MFMessageComposeViewControllerDelegate, FBSDKAppInviteDialogDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *accessDeniedTitleLable;
@property (weak, nonatomic) IBOutlet UIView *accessDeniedContainerView;
@property (weak, nonatomic) IBOutlet UILabel *accessDeniedDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *columnIndex0Button;
@property (assign, nonatomic) BOOL columnIndex0IsSelected;
@property (weak, nonatomic) IBOutlet UIButton *columnIndex1Button;
@property (assign, nonatomic) BOOL columnIndex1IsSelected;
@property (strong, nonatomic) NSArray *friends;
@property (strong, nonatomic) NSArray *contacts;
@property (strong, nonatomic) NSArray *friendSortedKeys;
@property (strong, nonatomic) NSArray *contactSortedKeys;
@property (strong, nonatomic) NSMutableDictionary *friendImagesByIndexPathString;
@property (strong, nonatomic) NSMutableDictionary *selectedFriendsByIndexPathString;
@property (strong, nonatomic) NSMutableDictionary *selectedContactsByIndexPathString;
@property (strong, nonatomic) NSMutableDictionary *friendAThroughZSectionDictionary;
@property (strong, nonatomic) NSMutableDictionary *contactAThroughZSectionDictionary;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blueSliderLeftMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blueSliderFixedWidthConstraint;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (assign) BOOL shouldConfigureForDayToDayUse;
@end

@implementation SWShareAppWithFriendsTableViewController

#pragma mark - Public Methods

- (void)configureForDayToDayUse
{
    self.shouldConfigureForDayToDayUse = YES;
}

#pragma mark - FBSDKAppInviteDialogDelegate

/*!
 @abstract Sent to the delegate when the app invite completes without error.
 @param appInviteDialog The FBSDKAppInviteDialog that completed.
 @param results The results from the dialog.  This may be nil or empty.
 */
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results
{
    if ([[results objectForKey:@"completionGesture"] isEqualToString:@"cancel"] && [[results objectForKey:@"didComplete"] isEqualToNumber:@1])
    {
        [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Cancel"}];
        [self columnIndex0WasSelected:self];
    }
    else
    {
        [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Send"}];
        [self.delegate shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:self];
    }
}

/*!
 @abstract Sent to the delegate when the app invite encounters an error.
 @param appInviteDialog The FBSDKAppInviteDialog that completed.
 @param error The error.
 */
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
{
    [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Errored, Passed Them Anyways"}];
    [self.delegate shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:self];
}

#pragma mark - Move to helpers...

- (NSArray *)dataSource
{
    NSArray *dataSource;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        dataSource = self.contacts;
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        dataSource = self.friends;
    }
    return dataSource;
}

- (NSArray *)dataSourceSortedKeys
{
    NSArray *dataSource;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        dataSource = self.contactSortedKeys;
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        dataSource = self.friendSortedKeys;
    }
    return dataSource;
}

- (NSMutableDictionary *)dataSourceIndexDictionary
{
    NSMutableDictionary *dataSource;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        dataSource = self.contactAThroughZSectionDictionary;
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        dataSource = self.friendAThroughZSectionDictionary;
    }
    return dataSource;
}

- (UIImage *)dataSourceImageForIndexPath:(NSIndexPath *)indexPath
{
    UIImage *image = nil;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        
        // Get contact data
        NSString *key = [self.contactSortedKeys objectAtIndex:indexPath.section];
        NSArray *sectionContacts = [self.contactAThroughZSectionDictionary objectForKey:key];
        SWShareWithContactModel *contact = [sectionContacts objectAtIndex:indexPath.row];
        image = contact.image;
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        image = [self.friendImagesByIndexPathString objectForKey:[self stringKeyForIndexPath:indexPath]];
    }
    return image;
}

- (NSMutableDictionary *)dataSourceSelectedPersonsByIndexPathString
{
    NSMutableDictionary *dict = nil;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        dict = self.selectedContactsByIndexPathString;
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        dict = self.selectedFriendsByIndexPathString;
    }
    return dict;
}

- (void)dataSourceClearSelectedPersonsByIndexPathString
{
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        self.selectedContactsByIndexPathString = [NSMutableDictionary new];
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        self.selectedFriendsByIndexPathString = [NSMutableDictionary new];
    }
}

- (SWShareWithPersonModel *)dataSourcePersonForIndexPath:(NSIndexPath *)indexPath
{
    SWShareWithPersonModel *model = nil;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        @try {
            // Get contact data
            NSString *key = [self.contactSortedKeys objectAtIndex:indexPath.section];
            NSArray *sectionContacts = [self.contactAThroughZSectionDictionary objectForKey:key];
            model = [sectionContacts objectAtIndex:indexPath.row];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        @try {
            // Get friend data
            NSString *key = [self.friendSortedKeys objectAtIndex:indexPath.section];
            NSArray *sectionFriends = [self.friendAThroughZSectionDictionary objectForKey:key];
            model = [sectionFriends objectAtIndex:indexPath.row];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    }
    return model;
}

- (void)setAllColumnIndexIsSelectedToNo
{
    self.columnIndex0IsSelected = NO;
    self.columnIndex1IsSelected = NO;
}

- (void)refreshDoneButton
{
    if ([[self dataSourceSelectedPersonsByIndexPathString] count] == 0)
    {
        [self.doneButton setImage:[UIImage imageNamed:@"done-btn-without-check"] forState:UIControlStateNormal];
    }
    else
    {
        [self.doneButton setImage:[UIImage imageNamed:@"done-btn-with-check"] forState:UIControlStateNormal];
    }
}

#pragma mark - Target Action

- (void)screenWantsToDisimss:(id)sender
{
    [self.delegate shareAppWithFriendsTableViewControllerWantsToDismiss:self];
}

- (IBAction)columnIndex0WasSelected:(id)sender
{
    // Ask for access to contacts book!
    //NSLog(@"// Ask for access to contacts book!");
    if ([self.selectedFriendsByIndexPathString count] > 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Share via Text Message" message:@"People you have selected under \"Facebook\" will not receive an invite if you choose to share via Text Message." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [Flurry logEvent:@"Toggle Contact Type" withParameters:@{@"Type": @"Text Message"}];
    
    [self setAllColumnIndexIsSelectedToNo];
    self.columnIndex0IsSelected = YES;
    [UIView animateWithDuration:0.5 animations:^{
        self.blueSliderLeftMarginConstraint.constant = 0;
        [self.view layoutIfNeeded];
    }];
    
    // Really good address book docs
    // http://www.apeth.com/iOSBook/ch31.html
    
    CFErrorRef err = nil;
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, &err);
    if (nil == addressBookRef)
    {
        //NSLog(@"line %@ error: %@", @(__LINE__), err);
        
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
        //NSLog(@"// The user has previously denied access, Send an alert telling user to change privacy setting in settings app");
        [self showContactsLockedScreen];
    }
    else
    {
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
        {
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error)
                                                     {
                                                         // This gets executed on a background thread, FYI...
                                                         
                                                         if (granted)
                                                         {
                                                             // First time access has been granted, add the contact
                                                             //NSLog(@"// First time access has been granted, add the contact");
                                                             [self updateContactsDataSourceUsingAddressBookRef:addressBookRef];
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [self.tableView reloadData];
                                                                 [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                                                                 [self removeContactsLockedScreen];
                                                             });
                                                         }
                                                         else
                                                         {
                                                             // User denied access
                                                             // Display an alert telling user the contact could not be added
                                                             //NSLog(@"// User denied access, Display an alert telling user the contact could not be added");
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [self showContactsLockedScreen];
                                                             });
                                                         }
                                                     });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
        {
            // The user has previously given access, add the contact
            //NSLog(@"// The user has previously given access, add the contact");
            [self removeContactsLockedScreen];
            [self updateContactsDataSourceUsingAddressBookRef:addressBookRef];
            [self.tableView reloadData];
            if ([self.contacts count] > 0)
            {
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            }
        }
        else
        {
            // The user has previously denied access
            // Send an alert telling user to change privacy setting in settings app
            //NSLog(@"// The user has previously denied access, Send an alert telling user to change privacy setting in settings app");
            [self showContactsLockedScreen];
        }
    }
    [self refreshDoneButton];
}

- (IBAction)columnIndex1WasSelected:(id)sender
{
    //FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] initWithAppLinkURL:[NSURL URLWithString:@"https://fb.me/967541529946850"]]; // Sickweather App Link
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init]; // Sickweather App Link
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/967541529946850"];
    /*
     echo file_get_contents('https://fb.me/967541529946850'); prints:
     
     <html>
     <head>
     <title>Sickweather</title>
     <meta property="fb:app_id" content="176924102341934" />
     <meta property="al:ios:url" content="sickweather://" />
     <meta property="al:ios:app_name" content="Sickweather" />
     <meta property="al:ios:app_store_id" content="741036885" />
     <meta property="al:android:package" content="com.sickweather.sickweather" />
     <meta property="al:android:app_name" content="Sickweather" />
     <meta property="al:web:should_fallback" content="false" />
     <meta http-equiv="refresh" content="0;url=http://www.sickweather.com/" />
     </head>
     <body>Redirecting...</body>
     </html>
     */
    
    // Optionally set previewImageURL
    // The suggested image size is 1,200 x 628 pixels with an image ratio of 1.9:1.
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"http://www.sickweather.com/images/refresh/hero-fpo.jpg"];
    
    // Present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
    FBSDKAppInviteDialog *myFBAppInviteDialog = [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
    if ([myFBAppInviteDialog canShow])
    {
        // Great...
        //NSLog(@"Now showing myFBAppInviteDialog...");
    }
    else
    {
        [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Failed, Passed Them Anyways 2"}];
        [self.delegate shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:self];
        
    }
    [self refreshDoneButton];
}

- (IBAction)protectAllTapped:(id)sender
{
    [Flurry logEvent:@"Protect All Tapped"];
    
    if ([[self dataSourceSelectedPersonsByIndexPathString] count] == [[self dataSource] count])
    {
        // Unselect all
        [self dataSourceClearSelectedPersonsByIndexPathString];
    }
    else
    {
        // Select all
        for (NSString *key in [self dataSourceSortedKeys])
        {
            NSInteger section = [[self dataSourceSortedKeys] indexOfObject:key];
            NSArray *personsInSection = [[self dataSourceIndexDictionary] valueForKey:key];
            for (NSDictionary *person in personsInSection)
            {
                
                //NSLog(@"person = %@", person);
                /*
                 TEXT MESSAGE:
                 person = {
                 addressBookRecordId = 2001;
                 email = "ABMultiValueRef 0x170c6cf40 with 2 value(s)\n    0: _$!<Home>!$_ (0x170627ac0) - aburtzel@gmail.com (0x170656620)\n    1: _$!<Other>!$_ (0x170627aa0) - aburtzel@gmail.com (0x1706565f0)\n";
                 firstName = Aaron;
                 image = "<UIImage: 0x17029d790>";
                 lastName = Burtzel;
                 name = "Aaron Burtzel";
                 phone = "(320) 293-5356";
                 }
                 
                 FACEBOOK:
                 person = {
                 "first_name" = Aaron;
                 id = 1272900104;
                 "last_name" = Burtzel;
                 name = "Aaron Thomas Burtzel";
                 username = "aaron.burtzel";
                 }
                 */
                
                NSInteger row = [personsInSection indexOfObject:person];
                NSIndexPath *indexPathToSelect = [NSIndexPath indexPathForRow:row inSection:section];
                [[self dataSourceSelectedPersonsByIndexPathString] setObject:person forKey:[self stringKeyForIndexPath:indexPathToSelect]];
            }
        }
        //NSLog(@"dataSourcePersonDictByIndexPathString count = %@", @([[self dataSourceSelectedPersonsByIndexPathString] count]));
        //NSLog(@"dataSourcePersonDictByIndexPathString = %@", [self dataSourceSelectedPersonsByIndexPathString]);
    }
    [self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationNone];
    [self refreshDoneButton];
}

- (IBAction)okayDoneTapped:(id)sender
{
    NSString *networkValue = @"";
    NSString *selectedCountValue = @"0";
    NSMutableArray *recipients = [NSMutableArray new];
    NSMutableArray *selectedIDs = [NSMutableArray new];
    BOOL logToFlurry = NO;
    if (self.columnIndex0IsSelected)
    {
        // Text Message
        networkValue = @"TXT";
        NSMutableArray *recipientsWithoutNumbers = [NSMutableArray new];
        for (NSString *key in [self dataSourceSelectedPersonsByIndexPathString])
        {
            SWShareWithContactModel *contactModel = [[self dataSourceSelectedPersonsByIndexPathString] objectForKey:key];
            if (contactModel.phone)
            {
                [recipients addObject:contactModel.phone];
            }
            else
            {
                [recipientsWithoutNumbers addObject:contactModel];
            }
            if ([recipients count] >= [self kMaxTextMessageRecipientCount])
            {
                break;
            }
        }
        selectedCountValue = [NSString stringWithFormat:@"%@", @([recipients count])];
        if ([recipientsWithoutNumbers count] > 0)
        {
            NSArray *names = @[];
            for (SWShareWithContactModel *contact in recipientsWithoutNumbers)
            {
                if (contact.name) {names = [names arrayByAddingObject:contact.name];}
            }
            NSString *namesString = [names componentsJoinedByString:@", "];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Phone Numbers" message:[NSString stringWithFormat:@"The following people don't have phone numbers on file: %@. To fix, either add a phone number for each person listed above (in the Contacts app) or remove them from your selection and try again.", namesString] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            if ([recipients count] > 0)
            {
                // Launch text message dialog
				if ([MFMessageComposeViewController  canSendText]){
					 logToFlurry = YES;
					MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
					messageController.messageComposeDelegate = self;
					messageController.recipients = recipients;
					messageController.body = @"Sickweather for iPhone http://sick.io/app";
					[self presentViewController:messageController animated:YES completion:^{}];
				}else {
					UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Can't Send Message" message:@"Your device settings do not allow sending message." preferredStyle:UIAlertControllerStyleAlert];
					UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
					}];
					[alert addAction:ok];
					[self presentViewController:alert animated:YES completion:nil];
				}
            }
            else
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are You Sure?" message:@"You didn't select any friends. Sickweather works better with friends." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes, Skip" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self.delegate shareAppWithFriendsTableViewControllerWantsToDismissBySkipping:self];
                }];
                [alert addAction:ok];
                UIAlertAction *selectFriends = [UIAlertAction actionWithTitle:@"Select Friends" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
                [alert addAction:selectFriends];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
    else if (self.columnIndex1IsSelected)
    {
        // Facebook
        networkValue = @"Facebook";
        for (NSString *key in [self dataSourceSelectedPersonsByIndexPathString])
        {
            SWShareWithFacebookModel *facebookRecord = [[self dataSourceSelectedPersonsByIndexPathString] objectForKey:key];
            if (facebookRecord.facebookId)
            {
                [selectedIDs addObject:facebookRecord.facebookId];
            }
            if ([selectedIDs count] >= [self kMaxFacebookRecipientCount])
            {
                break;
            }
        }
        selectedCountValue = [NSString stringWithFormat:@"%@", @([selectedIDs count])];
        if ([selectedIDs count] > 0)
        {
            // Launch share dialog
            logToFlurry = YES;
            
            // 1202818324 James Sajor
            // 752115272 Graham Dodge
            // Display the requests dialog
            /*
            [FBWebDialogs
             presentRequestsDialogModallyWithSession:nil
             message:@"Check this app out"
             title:@"Share"
             parameters:@{@"to": [selectedIDs componentsJoinedByString:@","]}
             handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                 if (error)
                 {
                     // Error launching the dialog or sending the request.
                     //NSLog(@"Error sending request.");
                     [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Error"}];
                 }
                 else
                 {
                     if (result == FBWebDialogResultDialogNotCompleted)
                     {
                         // User clicked the "x" icon
                         //NSLog(@"User canceled request.");
                         [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Cancel"}];
                     }
                     else
                     {
                         // Handle the send request callback
                         NSDictionary *urlParams = [SWHelper helperParseURLParams:[resultURL query]];
                         if (![urlParams valueForKey:@"request"])
                         {
                             // User clicked the Cancel button
                             //NSLog(@"User canceled request.");
                             [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Cancel"}];
                         }
                         else
                         {
                             // User clicked the Send button
                             NSString *requestID = [urlParams valueForKey:@"request"];
                             //NSLog(@"Request ID: %@", requestID);
                             [Flurry logEvent:@"Facebook Invite Window" withParameters:@{@"Action": @"Send"}];
                             [self.delegate shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:self];
                         }
                     }
                 }
             }];
             */
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are You Sure?" message:@"You didn't select any friends. Sickweather works better with friends." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes, Skip" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
            [alert addAction:ok];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Select Friends" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
            [alert addAction:cancel];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    if (logToFlurry)
    {
        [Flurry logEvent:@"Done" withParameters:@{@"Screen Name": @"Invite Friend Screen"}];
        [Flurry logEvent:@"Onboarding Share App Done Tapped" withParameters:@{@"Network": networkValue, @"Selected User Count": selectedCountValue}];
    }
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    if (result == MessageComposeResultCancelled)
    {
        [Flurry logEvent:@"TXT Invite Window" withParameters:@{@"Action": @"Cancel"}];
        //NSLog(@"MessageComposeResultCancelled");
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    else if (result == MessageComposeResultFailed)
    {
        [Flurry logEvent:@"TXT Invite Window" withParameters:@{@"Action": @"Error"}];
        //NSLog(@"MessageComposeResultFailed");
        [self dismissViewControllerAnimated:YES completion:^{
            UIAlertController *enterEmailAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"An error occurred. Please try again." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
            [enterEmailAlertController addAction:ok];
            [self presentViewController:enterEmailAlertController animated:YES completion:nil];
        }];
    }
    else if (result == MessageComposeResultSent)
    {
        //NSLog(@"MessageComposeResultSent");
        [Flurry logEvent:@"TXT Invite Window" withParameters:@{@"Action": @"Send"}];
        [self.delegate shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulMessageSend:self];
    }
}

#pragma mark - Customer Getters and Setters

- (NSArray *)friends
{
    if (!_friends) {
        _friends = [NSArray array];
    }
    return _friends;
}

- (NSMutableDictionary *)friendImagesByIndexPathString
{
    if (!_friendImagesByIndexPathString) {
        _friendImagesByIndexPathString = [NSMutableDictionary dictionary];
    }
    return _friendImagesByIndexPathString;
}

- (NSMutableDictionary *)selectedFriendsByIndexPathString
{
    if (!_selectedFriendsByIndexPathString) {
        _selectedFriendsByIndexPathString = [NSMutableDictionary dictionary];
    }
    return _selectedFriendsByIndexPathString;
}

- (NSMutableDictionary *)selectedContactsByIndexPathString
{
    if (!_selectedContactsByIndexPathString) {
        _selectedContactsByIndexPathString = [NSMutableDictionary dictionary];
    }
    return _selectedContactsByIndexPathString;
}

#pragma mark - Helpers

- (NSInteger)kMaxTextMessageRecipientCount
{
    return 50;
}

- (NSInteger)kMaxFacebookRecipientCount
{
    return 50;
}

- (void)resetFriendImages
{
    self.friendImagesByIndexPathString = [NSMutableDictionary new];
}

- (NSString *)stringKeyForIndexPath:(NSIndexPath *)indexPath
{
    return [NSString stringWithFormat:@"%@.%@", @(indexPath.section), @(indexPath.row)];
}

- (ABRecordRef)contactForIndexPath:(NSIndexPath *)indexPath
{
    // Get friend data
    NSString *key = [self.contactSortedKeys objectAtIndex:indexPath.section];
    NSArray *sectionContacts = [self.contactAThroughZSectionDictionary objectForKey:key];
    ABRecordRef contact = (__bridge ABRecordRef)[sectionContacts objectAtIndex:indexPath.row];
    return contact;
}

- (void)scrollToTopHotSpotTapped:(id)sender
{
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
}

- (void)showContactsLockedScreen
{
    [self.spinner stopAnimating];
    //NSLog(@"showContactsLockedScreen...");
    [self.view bringSubviewToFront:self.accessDeniedContainerView];
    self.accessDeniedTitleLable.text = @"Update Contact Settings";
    self.accessDeniedDescriptionLabel.text = @"To allow us to list your contacts here, go to Settings > Privacy > Contacts > Sickweather and flip the switch to the on position.";
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Contacts Required" message:@"Access to your contacts lets us list them here so you can easily text this app to a friend. To update, tap \"Go to Settings\" and flip the switch next to \"Contacts\" on." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:ok];
    UIAlertAction *goToSettings = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:appSettings];
    }];
    [alert addAction:goToSettings];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)removeContactsLockedScreen
{
    [self.spinner stopAnimating];
    //NSLog(@"removeContactsLockedScreen...");
    [self.view sendSubviewToBack:self.accessDeniedContainerView];
}

- (void)showFacebookLockedScreen
{
    [self.spinner stopAnimating];
    //NSLog(@"showFacebookLockedScreen...");
    self.accessDeniedTitleLable.text = @"Update Facebook Settings";
    self.accessDeniedDescriptionLabel.text = @"To allow us to list your friends here, go to Settings > Privacy > Facebook > Sickweather and flip the switch to the on position.";
    [self.view bringSubviewToFront:self.accessDeniedContainerView];
}

- (void)removeFacebookLockedScreen
{
    [self.spinner stopAnimating];
    //NSLog(@"removeFacebookLockedScreen...");
    [self.view sendSubviewToBack:self.accessDeniedContainerView];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Update model
    NSDictionary *personDict = [[self dataSourceSelectedPersonsByIndexPathString] objectForKey:[self stringKeyForIndexPath:indexPath]];
    if (personDict)
    {
        // Is selected, unselect
        [[self dataSourceSelectedPersonsByIndexPathString] removeObjectForKey:[self stringKeyForIndexPath:indexPath]];
        [Flurry logEvent:@"Friend Unselected"];
    }
    else
    {
        // Is not selected, select
        SWShareWithPersonModel *person = [self dataSourcePersonForIndexPath:indexPath];
        [[self dataSourceSelectedPersonsByIndexPathString] setObject:person forKey:[self stringKeyForIndexPath:indexPath]];
        [Flurry logEvent:@"Friend Selected"];
    }
    
    // Reload cell
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self refreshDoneButton];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [[self dataSourceSortedKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *key = [[self dataSourceSortedKeys] objectAtIndex:section];
    return [[[self dataSourceIndexDictionary] valueForKey:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return ([[self dataSourceSortedKeys] objectAtIndex:section]);
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [self dataSourceSortedKeys];
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return index;
}

- (BOOL)indexPathIsSelected:(NSIndexPath *)indexPath
{
    NSDictionary *selected = [[self dataSourceSelectedPersonsByIndexPathString] objectForKey:[self stringKeyForIndexPath:indexPath]];
    if (selected)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get cell
    static NSString *CellIdentifier = @"SWFacebookFriendTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Check for our custom type
    if ([cell isKindOfClass:[SWFacebookFriendTableViewCell class]])
    {
        // Cast type
        SWFacebookFriendTableViewCell *fbCell = (SWFacebookFriendTableViewCell *)cell;
        
        // Set as white on select background
        fbCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // Configure select button
        fbCell.selectedCircleView.layer.cornerRadius = fbCell.selectedCircleView.bounds.size.width/2;
        fbCell.selectedCircleView.layer.masksToBounds = YES;
        fbCell.selectedCircleView.layer.borderWidth = 1;
        fbCell.selectedCircleView.layer.borderColor = [[SWColor colorSickweatherBlue41x171x226] CGColor];
        if ([fbCell.selectedCircleView.subviews count] == 0) {
            UIImageView *checkmark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"white-check"]];
            [fbCell.selectedCircleView addSubview:checkmark];
            checkmark.contentMode = UIViewContentModeScaleAspectFit;
            checkmark.frame = CGRectMake(0, 0, 35, 35);
            checkmark.center = CGPointMake(fbCell.selectedCircleView.bounds.size.width/2, fbCell.selectedCircleView.bounds.size.height/2);
        }
        
        // Configure selected state
        if ([self indexPathIsSelected:indexPath])
        {
            // Set as blue
            fbCell.selectedCircleView.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
        }
        else
        {
            // Set as white
            fbCell.selectedCircleView.backgroundColor = [UIColor whiteColor];
        }
        
        // Get person data
        SWShareWithPersonModel *person = [self dataSourcePersonForIndexPath:indexPath];
        
        // Start fresh
        fbCell.profilePicImageView.image = nil;
        
        // Maybe get already loaded image data
        UIImage *image = [self dataSourceImageForIndexPath:indexPath];
        
        // Set name
        fbCell.nameLabel.text = person.name;
        
        // Config image placeholder
        fbCell.noImagePlaceholderView.layer.cornerRadius = fbCell.noImagePlaceholderView.bounds.size.width/2;
        fbCell.noImagePlaceholderView.layer.masksToBounds = YES;
        [fbCell.contentView bringSubviewToFront:fbCell.noImagePlaceholderView];
        
        // Config image
        fbCell.profilePicImageView.layer.cornerRadius = fbCell.profilePicImageView.bounds.size.width/2;
        fbCell.profilePicImageView.layer.masksToBounds = YES;
        CGFloat imageSizeToRequest = [[UIScreen mainScreen] scale] * fbCell.profilePicImageView.bounds.size.width;
        
        // Set image
        if (image)
        {
            fbCell.profilePicImageView.image = image;
            [fbCell.contentView bringSubviewToFront:fbCell.profilePicImageView];
        }
        else
        {
            // Facebook
            if (self.columnIndex1IsSelected)
            {
                SWShareWithFacebookModel *personFacebook = (SWShareWithFacebookModel *)person;
                fbCell.profilePicImageView.image = nil; // Placeholder later?
                
                NSString *urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=square&height=%@&width=%@", personFacebook.facebookId, @(imageSizeToRequest), @(imageSizeToRequest)];
                //NSLog(@"urlString = %@", urlString);
                UIImage *image = [[SWHelper helperAppBackend] fetchImage:urlString usingCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.columnIndex1IsSelected && [[self.tableView indexPathsForVisibleRows] containsObject:indexPath])
                        {
                            @try {
                                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                            }
                            @catch (NSException *exception) {
                                // Whatev
                            }
                            @finally {
                                // Whatev
                            }
                        }
                    });
                }];
                if (image)
                {
                    fbCell.profilePicImageView.image = image;
                    [fbCell.contentView bringSubviewToFront:fbCell.profilePicImageView];
                    [self.friendImagesByIndexPathString setObject:image forKey:[self stringKeyForIndexPath:indexPath]]; // Add to cache
                }
            }
        }
    }
    return cell;
}

#pragma mark - Lifecycle

- (BOOL)prefersStatusBarHidden
{
    if (self.shouldConfigureForDayToDayUse)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

// Support only portrait
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)updateContactsDataSourceUsingAddressBookRef:(ABAddressBookRef)addressBookRef
{
    if ([self.contacts count] == 0)
    {
        CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        NSArray *abContactArray = CFBridgingRelease(people);
        NSMutableArray *contacts = [NSMutableArray new];
        for (NSUInteger loop = 0; loop < [abContactArray count]; loop++)
        {
            // Create and set base record
            SWShareWithContactModel *contact = [SWShareWithContactModel new];
            [contacts addObject:contact];
            
            // Get address book record
            ABRecordRef personRecord = (__bridge ABRecordRef)[abContactArray objectAtIndex:loop];
            
            // Set source object
            contact.addressBookRecordRef = personRecord;
        }
        
        // Sort & set into self props
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedStandardCompare:)];
        self.contacts = [contacts sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        //NSLog(@"self.contacts = %@", self.contacts);
        
        NSMutableArray *alphabet = [NSMutableArray new];
        for (char a = 'a'; a <= 'z'; a++) {[alphabet addObject:[NSString stringWithFormat:@"%c", a]];}
        NSMutableArray *clauses = [NSMutableArray new];
        for (NSString *character in alphabet)
        {
            NSString *clause = [NSString stringWithFormat:@"name BEGINSWITH[cd] '%@'", character];
            [clauses addObject:clause];
        }
        //NSLog(@"clauses = %@", clauses);
        NSString *predicateString = [clauses componentsJoinedByString:@" OR "];
        //NSLog(@"predicateString = %@", predicateString);
        
        NSPredicate *startsWithLetterPredicate = [NSPredicate predicateWithFormat:predicateString];
        self.contacts = [self.contacts filteredArrayUsingPredicate:startsWithLetterPredicate];
        //NSLog(@"self.contacts = %@", self.contacts);
        
        self.contactAThroughZSectionDictionary = [NSMutableDictionary createDictionaryForSectionIndex:self.contacts];
        self.contactSortedKeys = [[self.contactAThroughZSectionDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]; // Output, in this case, is {'A', 'B', 'C', 'E'}
    }
    else
    {
        // Do nothing...
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self resetFriendImages];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // Check context
    if (self.shouldConfigureForDayToDayUse)
    {
        // Config NAVIGATION CONTROLLER
        self.navigationController.navigationBar.translucent = NO;
        
        // Config LEFT BAR BUTTON ITEM
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(screenWantsToDisimss:)];
        
        // Config TITLE
        self.title = @"Invite Friends";
    }
    
    // Add our spinner
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = CGPointMake(self.view.bounds.size.width/2, 154);
    [self.spinner startAnimating];
    [self.view addSubview:self.spinner];
    
    // Set default selected column
    self.columnIndex0IsSelected = YES;
    
    // Add hot spot at top for quick scroll
    UIView *scrollHotSpot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
    scrollHotSpot.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollToTopHotSpotTapped:)];
    [scrollHotSpot addGestureRecognizer:tapGestureRecognizer];
    [self.view addSubview:scrollHotSpot];
    
    // Set initial state
    self.blueSliderLeftMarginConstraint.constant = 0;
    self.blueSliderFixedWidthConstraint.constant = 160;
    [self refreshDoneButton];
    
    // Ask for access to contacts book!
    //NSLog(@"// Ask for access to contacts book!");
    
    // Really good address book docs
    // http://www.apeth.com/iOSBook/ch31.html
    
    CFErrorRef err = nil;
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, &err);
    if (nil == addressBookRef)
    {
        //NSLog(@"line %@ error: %@", @(__LINE__), err);
        
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
        //NSLog(@"// The user has previously denied access, Send an alert telling user to change privacy setting in settings app");
        [self showContactsLockedScreen];
    }
    else
    {
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
        {
            // Throw a spinner up in hur
            [self.spinner startAnimating];
            
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error)
                                                     {
                                                         // This gets executed on a background thread, FYI...
                                                         
                                                         if (granted)
                                                         {
                                                             // First time access has been granted, add the contact
                                                             //NSLog(@"// First time access has been granted, add the contact");
                                                             [self updateContactsDataSourceUsingAddressBookRef:addressBookRef];
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [self.tableView reloadData];
                                                                 [self.spinner stopAnimating];
                                                                 [self removeContactsLockedScreen];
                                                             });
                                                         }
                                                         else
                                                         {
                                                             // User denied access
                                                             // Display an alert telling user the contact could not be added
                                                             //NSLog(@"// User denied access, Display an alert telling user the contact could not be added");
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [self showContactsLockedScreen];
                                                             });
                                                         }
                                                     });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
        {
            // The user has previously given access, add the contact
            //NSLog(@"// The user has previously given access, add the contact");
            [self updateContactsDataSourceUsingAddressBookRef:addressBookRef];
            [self.tableView reloadData];
            [self.spinner stopAnimating];
            [self removeContactsLockedScreen];
        }
        else
        {
            // The user has previously denied access
            // Send an alert telling user to change privacy setting in settings app
            //NSLog(@"// The user has previously denied access, Send an alert telling user to change privacy setting in settings app");
            [self showContactsLockedScreen];
        }
    }
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

@end
