//
//  SWMenuViewControllerCollectionViewLayout.h
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWMenuViewControllerCollectionViewLayoutDelegate
- (NSAttributedString *)menuViewControllerCollectionViewLayoutDelegateAttributedTitleStringForStandardItemCollectionViewCell:(NSIndexPath *)indexPath;
- (CGFloat)sectionHeaderPreferredVerticalHeightForIndexPath:(NSIndexPath *)indexPath;
- (CGFloat)totalExtraTitleMarginNeededForIndexPath:(NSIndexPath *)indexPath;
@end

@interface SWMenuViewControllerCollectionViewLayout : UICollectionViewLayout
@property (nonatomic, weak) id<SWMenuViewControllerCollectionViewLayoutDelegate> delegate;
@end
