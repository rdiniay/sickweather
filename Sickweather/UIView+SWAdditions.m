//
//  UIView+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 11/21/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "UIView+SWAdditions.h"

@implementation UIView (SWAdditions)

- (NSMutableArray *)allSubviews
{
    NSMutableArray *arr= [[NSMutableArray alloc] init];
    [arr addObject:self];
    for (UIView *subview in self.subviews)
    {
        [arr addObjectsFromArray:(NSArray*)[subview allSubviews]];
    }
    return arr;
}

@end
