//
//  SWSponsoredMarkerCustomPopupView.m
//  Sickweather
//
//  Created by John Erck on 12/11/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMarkerCustomPopupView.h"
#import "SWSponsoredMarkerMapKitAnnotation.h"

@interface SWSponsoredMarkerCustomPopupView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *topWhiteContentView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIButton *specialOfferButton;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *distanceLabel;
@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UILabel *phoneLabel;
@property (strong, nonatomic) UILabel *directionsLabel;
@property (strong, nonatomic) UIImageView *nippleImageView;
@property (strong, nonatomic) NSLayoutConstraint *specialOfferButtonHeightConstraint;
@end

@implementation SWSponsoredMarkerCustomPopupView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWSponsoredMarkerCustomPopupViewDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setAnnotationView:(SWSponsoredMarkerMapKitAnnotationView *)annotationView
{
    _annotationView = annotationView;
    SWSponsoredMarkerMapKitAnnotation *annotation = (SWSponsoredMarkerMapKitAnnotation *)annotationView.annotation;
    //NSLog(@"annotation.serverDict = %@", annotation.serverDict);
    if ([[annotation.serverDict objectForKey:@"special_offers_img"] length] > 0)
    {
        CGFloat width = 345;
        CGFloat height = 168;
        self.frame = CGRectMake((-1*width/2)+annotationView.frame.size.width/2, -1*height, width, height);
    }
    else
    {
        self.specialOfferButtonHeightConstraint.constant = 0;
        CGFloat width = 345;
        CGFloat height = 168-45; // B/c we won't have button
        self.frame = CGRectMake((-1*width/2)+annotationView.frame.size.width/2, -1*height, width, height);
    }
    NSString *title = [annotation.serverDict objectForKey:@"title"];
    if (title)
    {
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:14],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    }
    NSString *address = [annotation.serverDict objectForKey:@"display_text"];
    if (address)
    {
        self.addressLabel.attributedText = [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77]}];
    }
    NSString *phone = [annotation.serverDict objectForKey:@"phone"];
    if (phone)
    {
        self.phoneLabel.attributedText = [[NSAttributedString alloc] initWithString:phone attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideWalgreensRed227x24x55]}];
    }
    self.directionsLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Get Directions" attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideWalgreensRed227x24x55]}];
}

#pragma mark - Public Methods

- (void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

- (void)setButtonBackgroundImage:(UIImage *)image
{
    [self.specialOfferButton setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)setDistanceFromCurrentLocation:(NSString *)text
{
    self.distanceLabel.attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:14],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
}

#pragma mark - Helpers

#pragma mark - Target/Action

- (void)specialOfferButtonTouchUpInside:(id)sender
{
    [SWHelper helperShowAlertWithTitle:@"Button tapped!!"];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.contentView = [[UIView alloc] init];
        self.topWhiteContentView = [[UIView alloc] init];
        self.imageView = [[UIImageView alloc] init];
        self.specialOfferButton = [[UIButton alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.distanceLabel = [[UILabel alloc] init];
        self.addressLabel = [[UILabel alloc] init];
        self.phoneLabel = [[UILabel alloc] init];
        self.directionsLabel = [[UILabel alloc] init];
        self.bottomClearContentView = [[UIView alloc] init];
        self.nippleImageView = [[UIImageView alloc] init];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.topWhiteContentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.specialOfferButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.phoneLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.directionsLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomClearContentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.nippleImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.contentView];
        [self.contentView addSubview:self.topWhiteContentView];
        [self.topWhiteContentView addSubview:self.imageView];
        [self.topWhiteContentView addSubview:self.specialOfferButton];
        [self.topWhiteContentView addSubview:self.titleLabel];
        [self.topWhiteContentView addSubview:self.distanceLabel];
        [self.topWhiteContentView addSubview:self.addressLabel];
        [self.topWhiteContentView addSubview:self.phoneLabel];
        [self.topWhiteContentView addSubview:self.directionsLabel];
        [self.contentView addSubview:self.bottomClearContentView];
        [self.bottomClearContentView addSubview:self.nippleImageView];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
        [self.viewsDictionary setObject:self.topWhiteContentView forKey:@"topWhiteContentView"];
        [self.viewsDictionary setObject:self.imageView forKey:@"imageView"];
        [self.viewsDictionary setObject:self.specialOfferButton forKey:@"specialOfferButton"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.viewsDictionary setObject:self.distanceLabel forKey:@"distanceLabel"];
        [self.viewsDictionary setObject:self.addressLabel forKey:@"addressLabel"];
        [self.viewsDictionary setObject:self.phoneLabel forKey:@"phoneLabel"];
        [self.viewsDictionary setObject:self.directionsLabel forKey:@"directionsLabel"];
        [self.viewsDictionary setObject:self.bottomClearContentView forKey:@"bottomClearContentView"];
        [self.viewsDictionary setObject:self.nippleImageView forKey:@"nippleImageView"];
        
        // LAYOUT
        
        // Layout CONTENT VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TOP WHITE CONTENT VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topWhiteContentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topWhiteContentView]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[imageView(90)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[imageView(90)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout SPECIAL OFFERS BUTTON
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(5)-[specialOfferButton]-(5)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[imageView]-(5)-[specialOfferButton]-(5)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        self.specialOfferButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:self.specialOfferButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:0 attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40];
        [self addConstraint:self.specialOfferButtonHeightConstraint];
        
        // Layout TITLE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[imageView]-(10)-[titleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[titleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout DISTANCE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[distanceLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(5)-[distanceLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ADDRESS LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[imageView]-(10)-[addressLabel]-(5)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(5)-[addressLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout PHONE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[imageView]-(10)-[phoneLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.phoneLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.imageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        
        // Layout DIRECTIONS LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[directionsLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.directionsLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.imageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        
        // Layout BOTTOM CLEAR CONTENT VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[bottomClearContentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[topWhiteContentView][bottomClearContentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout NIPPLE VIEW
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.nippleImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.bottomClearContentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.nippleImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.bottomClearContentView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.nippleImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.bottomClearContentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.nippleImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomClearContentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        // CONFIG
        
        // Config SELF
        self.tag = [SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewTag];
        
        // Config CONTENT VIEW
        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = 5;
        self.clipsToBounds = YES;
        
        // Config TOP WHITE CONTENT VIEW
        self.topWhiteContentView.backgroundColor = [UIColor whiteColor];
        self.topWhiteContentView.layer.cornerRadius = 5;
        
        // Config IMAGE VIEW
        self.imageView.image = [SWHelper helperOnePixelImageUsingColor:[UIColor blackColor]];
        self.imageView.layer.cornerRadius = 2.5;
        self.imageView.clipsToBounds = YES;
        self.imageView.tag = [SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewProfilePicImageViewTag];
        
//        self.sponsoredMarkerTargetURL = annotation.sponsoredMarkerTargetURL;
        
        // Config SPECIAL OFFER BUTTON
        [self.specialOfferButton.layer setCornerRadius:2.5];
        [self.specialOfferButton addTarget:self action:@selector(specialOfferButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        self.specialOfferButton.tag = [SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewSpecialOfferButtonTag];
        
        // Config TITLE LABEL
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config DISTANCE LABEL
        self.distanceLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config ADDRESS LABEL
        self.addressLabel.textAlignment = NSTextAlignmentLeft;
        self.addressLabel.numberOfLines = 0;
        
        // Config PHONE LABEL
        self.phoneLabel.textAlignment = NSTextAlignmentLeft;
        self.phoneLabel.tag = [SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewPhoneViewTag];
        
        // Config DIRECTIONS LABEL
        self.directionsLabel.textAlignment = NSTextAlignmentLeft;
        self.directionsLabel.tag = [SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewGetDirectionsViewTag];
        
        // Config BOTTOM CLEAR CONTENT VIEW
        self.bottomClearContentView.backgroundColor = [UIColor clearColor];
        
        // Config NIPPLE VIEW
        self.nippleImageView.image = [UIImage imageNamed:@"sponsored-marker-popup-nipple"];
        self.nippleImageView.contentMode = UIViewContentModeTop;
        
        // COLOR FOR DEV PURPOSES
        //self.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        //self.contentView.backgroundColor = [SWColor color:[UIColor lightGrayColor] usingOpacity:0.5];
        //self.topWhiteContentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
        //self.imageView.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
        //self.titleLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.distanceLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.addressLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.phoneLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.directionsLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.bottomClearContentView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        //self.nippleImageView.backgroundColor = [SWColor color:[UIColor whiteColor] usingOpacity:1.0];
    }
    return self;
}

@end
