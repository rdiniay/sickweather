//
//  SWSelectItemFromListViewController.h
//  Sickweather
//
//  Created by John Erck on 10/7/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWSelectItemFromListViewController;

@protocol SWSelectItemFromListViewControllerDelegate <NSObject>
- (NSString *)selectItemFromListViewControllerJSONFileToUseBaseNameWithoutExtension;
- (NSString *)selectItemFromListViewControllerSelectedSystemValue:(SWSelectItemFromListViewController *)sender;
- (void)selectItemFromListViewControllerWantsToDismiss:(SWSelectItemFromListViewController *)sender;
- (void)selectItemFromListViewController:(SWSelectItemFromListViewController *)sender userSelectedRowDict:(NSDictionary *)rowDict;
@end

@interface SWSelectItemFromListViewController : UIViewController
@property (nonatomic, weak) id<SWSelectItemFromListViewControllerDelegate> delegate;
@end
