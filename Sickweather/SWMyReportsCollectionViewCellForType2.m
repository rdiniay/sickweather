//
//  SWMyReportsCollectionViewCellForType2.m
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWMyReportsCollectionViewCellForType2.h"

@interface SWMyReportsCollectionViewCellForType2 ()
@property (strong, nonatomic) NSArray *containerInsetConstraints;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *myContainerView;
@property (strong, nonatomic) UIButton *myButton;
@property (strong, nonatomic) UILabel *myLabel;
@property (strong, nonatomic) UIView *topBorderView;
@end

@implementation SWMyReportsCollectionViewCellForType2

#pragma mark - Public Methods

- (void)myReportsCollectionViewCellForType2SetMarginLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom
{
	[self.contentView removeConstraints:self.containerInsetConstraints];
	self.containerInsetConstraints = @[];
	self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[myContainerView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary]];
	self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[myContainerView]-(bottom)-|" options:0 metrics:@{@"top":top,@"bottom":bottom} views:self.viewsDictionary]];
	[self.contentView addConstraints:self.containerInsetConstraints];
}

- (void)myReportsCollectionViewCellForType2SetButtonAttributedString:(NSAttributedString *)attributedString
{
    [self.myButton setAttributedTitle:attributedString forState:UIControlStateNormal];
}

- (void)myReportsCollectionViewCellForType2SetImage:(UIImage *)image
{
    
}

- (void)myReportsCollectionViewCellForType2SetLabelAttributedString:(NSAttributedString *)attributedString
{
    self.myLabel.attributedText = attributedString;
}

- (void)myReportsCollectionViewCellForType2SetTextViewAttributedString:(NSAttributedString *)attributedString
{
    
}

#pragma mark - Target/Action

- (void)myButtonTapped:(id)sender
{
    [self.delegate myReportsCollectionViewCellForType2TargetAction1:self];
}

#pragma mark - Life Cycle Methods

// http://stackoverflow.com/a/24626651/394969 <-- pointInside & hitTest (for buttons with negative offsets)

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    // Check for YES via super
    if ([super pointInside:point withEvent:event])
    {
        return YES;
    }
    
    // Before returning NO, check to see if it is within myButton
    return !self.myButton.hidden && [self.myButton pointInside:[self.myButton convertPoint:point fromView:self] withEvent:event];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    // Check for hit within myButton
    UIView *view = [self.myButton hitTest:[self.myButton convertPoint:point fromView:self] withEvent:event];
    if (view == nil)
    {
        view = [super hitTest:point withEvent:event];
    }
    return view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.containerInsetConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.myContainerView = [[UIView alloc] init];
        self.myButton = [[UIButton alloc] init];
        self.myLabel = [[UILabel alloc] init];
        self.topBorderView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.myContainerView forKey:@"myContainerView"];
        [self.viewsDictionary setObject:self.myButton forKey:@"myButton"];
        [self.viewsDictionary setObject:self.myLabel forKey:@"myLabel"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.myButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.myLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.myContainerView];
        [self.myContainerView addSubview:self.myButton];
        [self.myContainerView addSubview:self.myLabel];
        [self.myContainerView addSubview:self.topBorderView];
        
        // LAYOUT
        
        // Layout myContainerView
        self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[myContainerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        self.containerInsetConstraints = [self.containerInsetConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[myContainerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:self.containerInsetConstraints];
        
        // Layout myButton
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[myButton]-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[myButton]-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout myLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[myLabel]-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[myLabel]-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout topBorderView
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[topBorderView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[topBorderView(1)]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config contentView
        self.contentView.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
        
        // Config topBorderView
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Layout myButton
        [self.myButton addTarget:self action:@selector(myButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        //self.myButton.backgroundColor = [UIColor orangeColor];
        //self.myButton.layer.cornerRadius = 10;
        //self.myButton.layer.borderColor = [UIColor redColor].CGColor;
        //self.myButton.layer.borderWidth = 1;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.myContainerView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
