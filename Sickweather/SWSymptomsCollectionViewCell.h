//
//  SWSymptomsCollectionViewCell.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWSymptomsModal.h"

@interface SWSymptomsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *ImgView;
-(void)populateCellWith:(SWSymptomsModal*)symptom;
@end
