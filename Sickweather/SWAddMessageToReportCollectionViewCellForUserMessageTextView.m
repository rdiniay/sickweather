//
//  SWAddMessageToReportCollectionViewCellForUserMessageTextView.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportCollectionViewCellForUserMessageTextView.h"
#import "SWColor.h"
#import "SWFont.h"

@interface SWAddMessageToReportCollectionViewCellForUserMessageTextView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@end

@implementation SWAddMessageToReportCollectionViewCellForUserMessageTextView

#pragma mark - Public Methods

- (void)addMessageToReportCollectionViewCellForUserMessageTextViewSetUserMessageTextViewAttributedString:(NSAttributedString *)text;
{
    self.textView.attributedText = text;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.defaultLabel = [[UILabel alloc] init];
        self.textView = [[UITextView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.defaultLabel forKey:@"defaultLabel"];
        [self.viewsDictionary setObject:self.textView forKey:@"textView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.defaultLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.textView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.textView];
        [self.contentView addSubview:self.defaultLabel]; // Put defaultLabel on top
        
        // LAYOUT
        
        // Layout DEFAULT LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[defaultLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(15)-[defaultLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TEXT VIEW
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[textView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[textView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config SELF
        self.contentView.layer.cornerRadius = 15;
        self.contentView.layer.borderWidth = 1.0;
        self.contentView.layer.borderColor = [SWColor colorSickweatherLightGray204x204x204].CGColor;
        
        // Config DEFAULT LABEL
        self.defaultLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Add a message..." attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
        
        // Config TEXT VIEW
//        self.textView.scrollEnabled = NO;
        self.textView.scrollsToTop = NO;
        self.textView.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.textView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
