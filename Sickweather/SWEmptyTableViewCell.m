//
//  SWEmptyTableViewCell.m
//  Sickweather
//
//  Created by Shan Shafiq on 1/17/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWEmptyTableViewCell.h"

@implementation SWEmptyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
