//
//  SWMyReportsCollectionViewCellForIllnessReportInstance.h
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyReportsCollectionViewCellForIllnessReportInstance;

@protocol SWMyReportsCollectionViewCellForIllnessReportInstance <NSObject>
- (void)myReportsCollectionViewCellForIllnessReportInstanceDeleteButtonTapped:(SWMyReportsCollectionViewCellForIllnessReportInstance *)sender;
- (void)myReportsCollectionViewCellForIllnessReportInstancePrivatePublicIconButtonTapped:(SWMyReportsCollectionViewCellForIllnessReportInstance *)sender;
@end

@interface SWMyReportsCollectionViewCellForIllnessReportInstance : UICollectionViewCell
@property (nonatomic, weak) id<SWMyReportsCollectionViewCellForIllnessReportInstance> delegate;
@property (nonatomic, strong) NSDictionary *serverDict;
- (void)myReportsCollectionViewCellForIllnessReportInstanceSetMarginLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
- (void)myReportsCollectionViewCellForIllnessReportInstanceSetButtonAttributedString:(NSAttributedString *)attributedString;
- (void)myReportsCollectionViewCellForIllnessReportInstanceSetImage:(UIImage *)image;
- (void)myReportsCollectionViewCellForIllnessReportInstanceSetLabelAttributedString:(NSAttributedString *)attributedString;
- (void)myReportsCollectionViewCellForIllnessReportInstanceSetTextViewAttributedString:(NSAttributedString *)attributedString;
@end
