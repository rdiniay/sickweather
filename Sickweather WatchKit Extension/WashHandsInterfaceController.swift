//
//  WashHandsInterfaceController.swift
//  Sickweather
//
//  Created by John Erck on 3/4/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class WashHandsInterfaceController: WKInterfaceController, WCSessionDelegate {
    /** Called when the session has completed activation. If session state is WCSessionActivationStateNotActivated there will be an error with more details. */
    @available(watchOS 2.2, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        // STUB...
    }


    var currentSecondCountdownInt = 20
    var timerIsRunning = false
    var timer1 = Timer()
    var lastDidDeactivateTime = Date()
    var timerWasRunningWhenDidDeactivate = false
    
    @IBOutlet weak var table: WKInterfaceTable!
    @IBOutlet weak var poweredByGroup: WKInterfaceGroup!
    @IBOutlet weak var resumeResetGroup: WKInterfaceGroup!
    @IBOutlet weak var readyGroup: WKInterfaceGroup!
    
    func setupTimerForDefaultState() {
        if let timerRowController = table.rowController(at: 0) as? WashHandsInterfaceTableRowControllerForTypeTimer {
            currentSecondCountdownInt = 20
            if (timer1.isValid) {
                timer1.invalidate()
            }
            timerIsRunning = false
            poweredByGroup.setHidden(true)
            resumeResetGroup.setHidden(true)
            readyGroup.setHidden(false)
            timerRowController.timerCurrentCountLabel.setText("\(SWHelper.helperHandWashTimerDuration())")
            timerRowController.timerGroup.setBackgroundImageNamed("timer-frame-")
            timerRowController.timerGroup.startAnimatingWithImages(in: NSMakeRange(0, 1), duration: 1.0, repeatCount: 1)
        }
    }
    
    @objc func timerDidFire(_ userInfo: AnyObject) {
        if let timerRowController = table.rowController(at: 0) as? WashHandsInterfaceTableRowControllerForTypeTimer {
            //print("timerDidFire")
            currentSecondCountdownInt -= 1
            timerRowController.timerCurrentCountLabel.setText("\(currentSecondCountdownInt)")
            if currentSecondCountdownInt == 0 {
                setTitle("Done")
                currentSecondCountdownInt = 20
                if (timer1.isValid) {
                    timer1.invalidate()
                }
                timerIsRunning = false
                poweredByGroup.setHidden(false)
                resumeResetGroup.setHidden(true)
                readyGroup.setHidden(true)
                timerRowController.timerCurrentCountLabel.setText("✔︎")
                timerRowController.timerSublabel.setText("FINISHED")
//                timerRowController.timerGroup.setBackgroundImageNamed("timer-animated-image")
//                timerRowController.timerGroup.stopAnimating()
            }
        }
    }
    
    @IBAction func resumeButtonTapped() {
        resumePauseToggle()
    }
    
    @IBAction func resetButtonTapped() {
        setupTimerForDefaultState()
    }
    
    func currentFrame() -> Int {
        let progress = 1.0 - Double(currentSecondCountdownInt)/20.0
        //print("progress = \(progress)")
        var currentFrame = Int(progress * Double(SWHelper.helperHandWashTimerTotalFrameCount()))
        if currentFrame == 0 {
            currentFrame = 1
        }
        return currentFrame
    }
    
    func startTimer(_ skipAnimateWithImagesInRange: Bool = false) {
        let rowController: AnyObject? = table.rowController(at: 0) as AnyObject?
        if rowController is WashHandsInterfaceTableRowControllerForTypeTimer {
            let timerRowController = rowController as! WashHandsInterfaceTableRowControllerForTypeTimer
            setTitle("Cancel")
            timerRowController.timerCurrentCountLabel.setText("\(currentSecondCountdownInt)")
            timerRowController.timerSublabel.setText("SECONDS")
            timer1 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(WashHandsInterfaceController.timerDidFire(_:)), userInfo: nil, repeats: true)
            if (skipAnimateWithImagesInRange) {
                // Do nothing
            } else {
                timerRowController.timerGroup.startAnimatingWithImages(in: NSMakeRange(currentFrame(), SWHelper.helperHandWashTimerTotalFrameCount() - currentFrame()), duration: Double(currentSecondCountdownInt), repeatCount: 1)
            }
            timerIsRunning = true
            resumeResetGroup.setHidden(true)
            poweredByGroup.setHidden(false)
            
            var session: WCSession? = nil;
            if WCSession.isSupported()
            {
                session = WCSession.default
                session!.delegate = self
                session!.activate()
            }
            
            session?.sendMessage(["handleWatchKitExtensionRequestMethod": "clearAllHandWashTimerCompleteNotifications"], replyHandler: { (response) -> Void in
                //print("WE ARE HERE, response = \(response).")
                session?.sendMessage(["handleWatchKitExtensionRequestMethod": "presentLocalNotificationForHandWashTimerComplete", "fireDateSecondsFromNow":NSNumber(value: self.currentSecondCountdownInt as Int)], replyHandler: { (response) -> Void in
                    //print("WE ARE HERE, response = \(response).")
                    }, errorHandler: { (error) -> Void in
                        
                    })
                }, errorHandler: { (error) -> Void in
                
            })
        }
    }
    
    func pauseTimer(_ clearOutstandingNotfications: Bool = true) {
        let rowController: AnyObject? = table.rowController(at: 0) as AnyObject?
        if rowController is WashHandsInterfaceTableRowControllerForTypeTimer {
            let timerRowController = rowController as! WashHandsInterfaceTableRowControllerForTypeTimer
            if (timer1.isValid) {
                timer1.invalidate()
            }
            timerRowController.timerGroup.stopAnimating()
            timerIsRunning = false
            resumeResetGroup.setHidden(false)
            poweredByGroup.setHidden(true)
            if clearOutstandingNotfications {
                var session: WCSession? = nil;
                if WCSession.isSupported()
                {
                    session = WCSession.default
                    session!.delegate = self
                    session!.activate()
                }
                
                session?.sendMessage(["handleWatchKitExtensionRequestMethod": "clearAllHandWashTimerCompleteNotifications"], replyHandler: { (response) -> Void in
                    
                    }, errorHandler: { (error) -> Void in
                        
                })
            }
        }
    }
    
    func resumePauseToggle() {
        let rowController: AnyObject? = table.rowController(at: 0) as AnyObject?
        if rowController is WashHandsInterfaceTableRowControllerForTypeTimer {
            //let timerRowController = rowController as! WashHandsInterfaceTableRowControllerForTypeTimer
            readyGroup.setHidden(true)
            if timerIsRunning {
                //print("PAUSE THE TIMER!!!")
                pauseTimer()
            } else {
                //print("START THE TIMER!!!")
                startTimer()
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let selectedRow: AnyObject? = table.rowController(at: rowIndex) as AnyObject?
        if selectedRow is WashHandsInterfaceTableRowControllerForTypeTimer {
            resumePauseToggle()
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        table.insertRows(at: IndexSet(integer: 0), withRowType: "WashHandsInterfaceTableRowControllerForTypeTimer")
        setupTimerForDefaultState()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        //print("willActivate")
        if timerWasRunningWhenDidDeactivate {
            //print("timerWasRunningWhenDidDeactivate")
            //print("lastDidDeactivateTime.timeIntervalSinceNow = \(lastDidDeactivateTime.timeIntervalSinceNow)") // -48.1600030064583
            let elapsedSeconds = lastDidDeactivateTime.timeIntervalSinceNow * -1.0 // Goes to -48
            if elapsedSeconds > Double(currentSecondCountdownInt) {
                setupTimerForDefaultState()
            } else {
                currentSecondCountdownInt = currentSecondCountdownInt - Int(elapsedSeconds)
                startTimer(true)
            }
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        //print("didDeactivate")
        if timerIsRunning {
            //print("timerIsRunning")
            lastDidDeactivateTime = Date()
            //print("lastDidDeactivateTime = \(lastDidDeactivateTime)")
            timerWasRunningWhenDidDeactivate = true
            //print("timerWasRunningWhenDidDeactivate = \(timerWasRunningWhenDidDeactivate)")
        } else {
            //print("timerIsNOTRunning")
            timerWasRunningWhenDidDeactivate = false
            //print("timerWasRunningWhenDidDeactivate = \(timerWasRunningWhenDidDeactivate)")
        }
        pauseTimer(false)
    }

}
