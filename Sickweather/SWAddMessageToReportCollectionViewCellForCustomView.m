//
//  SWAddMessageToReportCollectionViewCellForCustomView.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportCollectionViewCellForCustomView.h"
#import "SWColor.h"
#import "SWFont.h"

@interface SWAddMessageToReportCollectionViewCellForCustomView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSArray *customViewConstraints;
@property (strong, nonatomic) UIView *customView;
@end

@implementation SWAddMessageToReportCollectionViewCellForCustomView

#pragma mark - Public Methods

- (void)addMessageToReportCollectionViewCellForCustomViewSetView:(UIView *)view
{
    [self addMessageToReportCollectionViewCellForCustomViewSetView:view left:@0 right:@0 top:@0 bottom:@0];
}

- (void)addMessageToReportCollectionViewCellForCustomViewSetView:(UIView *)view left:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom
{
    if (self.customView)
    {
        [self.customView removeFromSuperview];
    }
    if (self.customViewConstraints)
    {
        [self.contentView removeConstraints:self.customViewConstraints];
    }
    if ([self.viewsDictionary objectForKey:@"customView"])
    {
        [self.viewsDictionary removeObjectForKey:@"customView"];
    }
    self.customView = view;
    self.customViewConstraints = @[];
    [self.viewsDictionary setObject:self.customView forKey:@"customView"];
    self.customView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.customView];
    self.customViewConstraints = [self.customViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[customView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary]];
    self.customViewConstraints = [self.customViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[customView]-(bottom)-|" options:0 metrics:@{@"top":top,@"bottom":bottom} views:self.viewsDictionary]];
    [self.contentView addConstraints:self.customViewConstraints];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.viewsDictionary = [NSMutableDictionary new];
        self.customViewConstraints = @[];
        self.clipsToBounds = NO;
    }
    return self;
}

@end

