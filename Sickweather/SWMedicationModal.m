//
//  SWMedicationModal.m
//  Sickweather
//
//  Created by Shan Shafiq on 2/14/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWMedicationModal.h"

@implementation SWMedicationModal
- (instancetype)init
{
	self = [super init];
	return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if (self) {
		if ([NSNull null] != [dictionary valueForKey:@"name"]) self.name = [dictionary valueForKey:@"name"]; else self.name = @"";
		if ([NSNull null] != [dictionary valueForKey:@"dose"]) self.dose = [dictionary valueForKey:@"dose"]; else self.dose = @"";
		if ([NSNull null] != [dictionary valueForKey:@"dose_unit"]) self.dose_unit = [dictionary valueForKey:@"dose_unit"]; else self.dose_unit = @"";
		if ([NSNull null] != [dictionary valueForKey:@"notes"]) self.notes = [dictionary valueForKey:@"notes"]; else self.notes = @"";
	}
	return self;
}
@end
