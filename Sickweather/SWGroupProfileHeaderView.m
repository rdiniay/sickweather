//
//  SWGroupProfileMessageTableViewHeader.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileHeaderView.h"

@interface SWGroupProfileHeaderView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIView *backgroundImageViewOverlayView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UILabel *reportCountDigitLabel;
@property (strong, nonatomic) UILabel *reportCountTitleLabel;
@property (strong, nonatomic) UILabel *followerCountDigitLabel;
@property (strong, nonatomic) UILabel *followerCountTitleLabel;
@property (strong, nonatomic) UILabel *adminCountDigitLabel;
@property (strong, nonatomic) UILabel *adminCountTitleLabel;
@property (strong, nonatomic) UIView *rowOfButtonsContainerView;
@property (strong, nonatomic) UIButton *followButton;
@property (strong, nonatomic) UIButton *claimButton;
@property (strong, nonatomic) UIButton *ellipsesButton;
@end

@implementation SWGroupProfileHeaderView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWGroupProfileMessageTableViewHeaderDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    [self reloadDelegateInfo];
}

#pragma mark - Taget/Action

- (void)followButtonTouchUpInside:(id)sender
{
    [self.delegate groupProfileHeaderViewFollowButtonTapped:self];
}

- (void)claimButtonTouchUpInside:(id)sender
{
    [self.delegate groupProfileHeaderViewClaimButtonTapped:self];
}

- (void)ellipsesButtonTouchUpInside:(id)sender
{
    [self.delegate groupProfileHeaderViewEllipsesButtonTapped:self];
}

#pragma mark - Public Methods

- (void)reloadDelegateInfo
{
    // UPDATE PROPS
    
    CGFloat countDigitFontSize = 24;
    CGFloat countTitleFontSize = 16;
    CGFloat buttonTextSize = 14;
    
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate groupProfileHeaderViewTitleString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:22]}];
    
    self.addressLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate groupProfileHeaderViewAddressString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
    
    self.reportCountDigitLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate groupProfileHeaderViewReportCountString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:countDigitFontSize]}];
    
    self.reportCountTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"REPORTS" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardFontOfSize:countTitleFontSize]}];
    
    self.followerCountDigitLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate groupProfileHeaderViewFollowerCountString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:countDigitFontSize]}];
    
    self.followerCountTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"FOLLOWERS" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardFontOfSize:countTitleFontSize]}];
    
    self.adminCountDigitLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate groupProfileHeaderViewAdminCountString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:countDigitFontSize]}];
    
    self.adminCountTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"ADMINS" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardFontOfSize:countTitleFontSize]}];
    
    if ([self.delegate groupProfileHeaderViewUserIsFollowingGroup:self] == YES)
    {
        [self.followButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"   FOLLOWING   " attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:buttonTextSize],NSForegroundColorAttributeName:[UIColor whiteColor]}] forState:UIControlStateNormal];
        self.followButton.backgroundColor = [SWColor colorSickweatherStyleGuideGreen68x157x68];
        self.followButton.layer.cornerRadius = 6;
        self.followButton.layer.borderColor = [SWColor colorSickweatherStyleGuideGreen68x157x68].CGColor;
        self.followButton.layer.borderWidth = 1;
    }
    else
    {
        [self.followButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"   FOLLOW   " attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:buttonTextSize],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGreen68x157x68]}] forState:UIControlStateNormal];
        self.followButton.backgroundColor = [UIColor whiteColor];
        self.followButton.layer.cornerRadius = 6;
        self.followButton.layer.borderColor = [SWColor colorSickweatherStyleGuideGreen68x157x68].CGColor;
        self.followButton.layer.borderWidth = 1;
    }
    
    if ([self.delegate groupProfileHeaderViewUserIsGroupAdmin:self] == YES)
    {
        [self.claimButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"   ADMIN   " attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:buttonTextSize],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideOrange252x111x6]}] forState:UIControlStateNormal];
        self.claimButton.backgroundColor = [UIColor whiteColor];
        self.claimButton.layer.cornerRadius = 6;
        self.claimButton.layer.borderColor = [SWColor colorSickweatherStyleGuideOrange252x111x6].CGColor;
        self.claimButton.layer.borderWidth = 1;
    }
    else
    {
        [self.claimButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"   CLAIM   " attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:buttonTextSize],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideOrange252x111x6]}] forState:UIControlStateNormal];
        self.claimButton.backgroundColor = [UIColor whiteColor];
        self.claimButton.layer.cornerRadius = 6;
        self.claimButton.layer.borderColor = [SWColor colorSickweatherStyleGuideOrange252x111x6].CGColor;
        self.claimButton.layer.borderWidth = 1;
        if ([[self.delegate groupProfileHeaderViewAdminCountString:self] integerValue] > 0)
        {
            self.claimButton.hidden = YES;
        }
    }
    
    [self.ellipsesButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"  ...  " attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:buttonTextSize*2],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224]}] forState:UIControlStateNormal];
    self.ellipsesButton.backgroundColor = [UIColor whiteColor];
    self.ellipsesButton.layer.cornerRadius = 6;
    self.ellipsesButton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
    self.ellipsesButton.layer.borderWidth = 1;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.backgroundImageView = [[UIImageView alloc] init];
        self.backgroundImageViewOverlayView = [[UIView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.addressLabel = [[UILabel alloc] init];
        self.reportCountDigitLabel = [[UILabel alloc] init];
        self.reportCountTitleLabel = [[UILabel alloc] init];
        self.followerCountDigitLabel = [[UILabel alloc] init];
        self.followerCountTitleLabel = [[UILabel alloc] init];
        self.adminCountDigitLabel = [[UILabel alloc] init];
        self.adminCountTitleLabel = [[UILabel alloc] init];
        self.rowOfButtonsContainerView = [[UIView alloc] init];
        self.followButton = [[UIButton alloc] init];
        self.claimButton = [[UIButton alloc] init];
        self.ellipsesButton = [[UIButton alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.backgroundImageView forKey:@"backgroundImageView"];
        [self.viewsDictionary setObject:self.backgroundImageViewOverlayView forKey:@"backgroundImageViewOverlayView"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.viewsDictionary setObject:self.addressLabel forKey:@"addressLabel"];
        [self.viewsDictionary setObject:self.reportCountDigitLabel forKey:@"reportCountDigitLabel"];
        [self.viewsDictionary setObject:self.reportCountTitleLabel forKey:@"reportCountTitleLabel"];
        [self.viewsDictionary setObject:self.followerCountDigitLabel forKey:@"followerCountDigitLabel"];
        [self.viewsDictionary setObject:self.followerCountTitleLabel forKey:@"followerCountTitleLabel"];
        [self.viewsDictionary setObject:self.adminCountDigitLabel forKey:@"adminCountDigitLabel"];
        [self.viewsDictionary setObject:self.adminCountTitleLabel forKey:@"adminCountTitleLabel"];
        [self.viewsDictionary setObject:self.rowOfButtonsContainerView forKey:@"rowOfButtonsContainerView"];
        [self.viewsDictionary setObject:self.followButton forKey:@"followButton"];
        [self.viewsDictionary setObject:self.claimButton forKey:@"claimButton"];
        [self.viewsDictionary setObject:self.ellipsesButton forKey:@"ellipsesButton"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundImageViewOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.reportCountDigitLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.reportCountTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.followerCountDigitLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.followerCountTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.adminCountDigitLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.adminCountTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowOfButtonsContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.followButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.claimButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.ellipsesButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.backgroundImageView];
        [self addSubview:self.backgroundImageViewOverlayView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.addressLabel];
        [self addSubview:self.reportCountDigitLabel];
        [self addSubview:self.reportCountTitleLabel];
        [self addSubview:self.followerCountDigitLabel];
        [self addSubview:self.followerCountTitleLabel];
        [self addSubview:self.adminCountDigitLabel];
        [self addSubview:self.adminCountTitleLabel];
        [self addSubview:self.rowOfButtonsContainerView];
        [self.rowOfButtonsContainerView addSubview:self.followButton];
        [self.rowOfButtonsContainerView addSubview:self.claimButton];
        [self.rowOfButtonsContainerView addSubview:self.ellipsesButton];
        
        // LAYOUT
        
        // Layout IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout IMAGE VIEW OVERLAY VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TITLE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[titleLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(15)-[titleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ADDRESS LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[addressLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(5)-[addressLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout REPORT COUNT DIGIT LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[reportCountDigitLabel]-100-[followerCountDigitLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[addressLabel]-(15)-[reportCountDigitLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout REPORT COUNT TITLE LABEL
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.reportCountTitleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.reportCountDigitLabel attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[reportCountDigitLabel]-(5)-[reportCountTitleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout FOLLOWER COUNT DIGIT LABEL <-- Everything is anchored w.r.t. to this guy...
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.followerCountDigitLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[addressLabel]-(15)-[followerCountDigitLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout FOLLOWER COUNT TITLE LABEL
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.followerCountTitleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.followerCountDigitLabel attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[followerCountDigitLabel]-(5)-[followerCountTitleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ADMIN COUNT DIGIT LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[followerCountDigitLabel]-100-[adminCountDigitLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[addressLabel]-(15)-[adminCountDigitLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ADMIN COUNT TITLE LABEL
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adminCountTitleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.adminCountDigitLabel attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[adminCountDigitLabel]-(5)-[adminCountTitleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ROW OF BUTTONS CONTAINER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[rowOfButtonsContainerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[rowOfButtonsContainerView(40)]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout FOLLOW BUTTON
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[followButton]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[followButton(30)]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        //[self addConstraint:[NSLayoutConstraint constraintWithItem:self.followButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.rowOfButtonsContainerView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout CLAIM BUTTON
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[followButton]-(15)-[claimButton]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[claimButton(30)]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        //[self addConstraint:[NSLayoutConstraint constraintWithItem:self.claimButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.rowOfButtonsContainerView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout ELLIPSES BUTTON
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[ellipsesButton]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[ellipsesButton(30)]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        //[self addConstraint:[NSLayoutConstraint constraintWithItem:self.ellipsesButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.rowOfButtonsContainerView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // CONFIG
        
        // Config HEADER
        self.backgroundColor = [UIColor clearColor];
        
        // Config TITLE LABEL
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config IMAGE VIEW
        //self.backgroundImageView.image = [UIImage imageNamed:@"profile-map-background"];
        //self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        //self.backgroundImageView.clipsToBounds = YES;
        
        // Config IMAGE VIEW OVERLAY VIEW
        //self.backgroundImageViewOverlayView.backgroundColor = [SWColor color:[SWColor colorSickweatherStyleGuideBlue37x169x224] usingOpacity:0.9];
        
        // Config ADDRESS LABEL
        self.addressLabel.textAlignment = NSTextAlignmentCenter;
        self.addressLabel.numberOfLines = 1;
        
        // Config REPORT COUNT DIGIT LABEL
        self.reportCountDigitLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config REPORT COUNT TITLE LABEL
        self.reportCountTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config FOLLOWER COUNT DIGIT LABEL
        self.followerCountDigitLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config FOLLOWER COUNT TITLE LABEL
        self.followerCountTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config ADMIN COUNT DIGIT LABEL
        self.adminCountDigitLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config ADMIN COUNT TITLE LABEL
        self.adminCountTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config ROW OF BUTTONS CONTAINER VIEW
        self.rowOfButtonsContainerView.backgroundColor = [UIColor whiteColor];
        
        // Config FOLLOW BUTTON
        [self.followButton addTarget:self action:@selector(followButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        // Config CLAIM BUTTON
        [self.claimButton addTarget:self action:@selector(claimButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        // Config ELLIPSES BUTTON
        [self.ellipsesButton addTarget:self action:@selector(ellipsesButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        // COLOR FOR DEVELOPMENT PURPOSES
        /*
        self.titleLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        self.addressLabel.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
        self.reportCountDigitLabel.backgroundColor = [SWColor color:[UIColor orangeColor] usingOpacity:0.5];
        self.reportCountTitleLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        self.followerCountDigitLabel.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
        self.followerCountTitleLabel.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
        self.adminCountDigitLabel.backgroundColor = [SWColor color:[UIColor cyanColor] usingOpacity:0.5];
        self.adminCountTitleLabel.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
        self.rowOfButtonsContainerView.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
        self.followButton.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
        self.claimButton.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
        self.ellipsesButton.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
         */
    }
    return self;
}

@end
