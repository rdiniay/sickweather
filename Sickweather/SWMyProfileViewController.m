//
//  SWMyProfileViewController.m
//  Sickweather
//
//  Created by John Erck on 9/28/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWMyProfileViewController.h"
#import "SWMyProfileHeaderView.h"
#import "SWMyProfileSectionHeaderView.h"
#import "SWMyProfileRowView.h"
#import "SWEditMyProfileViewController.h"
#import "NSString+FontAwesome.h"
#import "Flurry.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDImageCache.h>
#import "SWMyReportsViewController.h"

#define staticHeaderImageViewDefaultHeight 200
#define staticHeaderImageViewDefaultAlpha 0.25
#define staticHeaderImageOverlayViewDefaultAlpha 0.0
#define rowHeight 50

@interface SWMyProfileViewController () <SWMyProfileRowViewDelegate, SWMyProfileHeaderViewDelegate, SWMyProfileSectionHeaderViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, SWEditMyProfileViewControllerDelegate, SWMyReportsViewControllerDelegate>
@property (strong, nonatomic) NSDictionary *myProfileData;
@property (strong, nonatomic) UIImageView *staticHeaderImageView;
@property (strong, nonatomic) UIView *staticHeaderImageOverlayView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) SWMyProfileHeaderView *headerView;
@property (strong, nonatomic) SWMyProfileSectionHeaderView *reportHistorySectionHeader;
@property (strong, nonatomic) SWMyProfileRowView *reportHistoryRow;
@property (strong, nonatomic) SWMyProfileSectionHeaderView *contactSectionHeader;
@property (strong, nonatomic) SWMyProfileRowView *emailRow;
@property (strong, nonatomic) SWMyProfileSectionHeaderView *aboutSectionHeader;
@property (strong, nonatomic) SWMyProfileRowView *genderRow;
@property (strong, nonatomic) SWMyProfileRowView *raceEthnicityRow;
@property (strong, nonatomic) SWMyProfileRowView *birthdayRow;
@property (strong, nonatomic) SWMyProfileRowView *locationRow;
@property (strong, nonatomic) SWMyProfileRowView *healthCarePreferenceRow;
@property (strong, nonatomic) UIView *logoutButtonContainer;
@property (strong, nonatomic) UIButton *logoutButton;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSMutableDictionary *imageForStringURL;
@property (strong, nonatomic) NSMutableArray *stringURLCurrentlyLoading;
@property (strong, nonatomic) NSLayoutConstraint *staticHeaderImageViewHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *staticHeaderImageOverlayViewHeightConstraint;
@end

@implementation SWMyProfileViewController

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat y = -scrollView.contentOffset.y;
    //NSLog(@"y = %f", y);
    
    // HANDLE ZOOM/FADE EFFECT
    if (y > 0)
    {
        self.staticHeaderImageViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight + y;
        self.staticHeaderImageOverlayViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight + y;
        self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
        self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
        CGFloat scale = y/self.view.bounds.size.height;
        if (scale > 1)
        {
            scale = 1;
        }
        CGFloat newAlpha = staticHeaderImageViewDefaultAlpha + (0.75*scale);
        //NSLog(@"newAlpha = %f", newAlpha);
        self.staticHeaderImageView.alpha = newAlpha;
        newAlpha = staticHeaderImageOverlayViewDefaultAlpha + (1.0*scale);
        //NSLog(@"newAlpha = %f", newAlpha);
        self.staticHeaderImageOverlayView.alpha = newAlpha;
    }
    else
    {
        self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
        self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
    }
    
    // HANDLE HIDING IMAGE IF NEEDED
    if (y < -200)
    {
        self.staticHeaderImageView.hidden = YES;
    }
    else
    {
        self.staticHeaderImageView.hidden = NO;
    }
}

#pragma mark - SWEditMyProfileViewControllerDelegate

- (void)editMyProfileViewControllerWantsToDismiss:(SWEditMyProfileViewController *)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)editMyProfileViewControllerWantsToDismissWithUpdateProfilePic:(UIImage *)image sender:(SWEditMyProfileViewController *)sender
{
    // Gets profile pic to update right away, before the edit screen drops down with animation, better UX, updated pic is just there, which is, we believe, what the user expects here
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.headerView setProfilePicImage:image];
    });
    [self dismissViewControllerAnimated:YES completion:^{
        [self.headerView setProfilePicImage:image];
    }];
}

#pragma mark - SWMyProfileRowViewDelegate

- (NSString *)myProfileRowViewTitleString:(SWMyProfileRowView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.reportHistoryRow])
    {
        title = @"Report History";
    }
    else if ([sender isEqual:self.emailRow])
    {
        title = [self userDataEmailString];
    }
    else if ([sender isEqual:self.genderRow])
    {
        title = [self userDataGenderFormattedString];
    }
    else if ([sender isEqual:self.raceEthnicityRow])
    {
        title = [self userDataRaceEthnicityFormattedString];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        title = [self userDataDateOfBirthFormattedString];
    }
    else if ([sender isEqual:self.locationRow])
    {
        title = [self userDataLocationString];
    }
    else if ([sender isEqual:self.healthCarePreferenceRow])
    {
        title = [self userDataHealthCarePreferenceString];
    }
    if (![title isKindOfClass:[NSString class]])
    {
        title = @""; // Make sure it's a string...
    }
    return title;
}

- (UIImage *)myProfileRowViewIconImage:(SWMyProfileRowView *)sender
{
    UIImage *image;
    if ([sender isEqual:self.reportHistoryRow])
    {
        image = [UIImage imageNamed:@"profile-icon-report-history"];
    }
    else if ([sender isEqual:self.emailRow])
    {
        image = [UIImage imageNamed:@"profile-icon-email"];
    }
    else if ([sender isEqual:self.genderRow])
    {
        image = [UIImage imageNamed:@"profile-icon-gender"];
    }
    else if ([sender isEqual:self.raceEthnicityRow])
    {
        image = [UIImage imageNamed:@"profile-icon-ethnicity"];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        image = [UIImage imageNamed:@"profile-icon-birthdate"];
    }
    else if ([sender isEqual:self.locationRow])
    {
        image = [UIImage imageNamed:@"profile-icon-location"];
    }
    else if ([sender isEqual:self.healthCarePreferenceRow])
    {
        image = [UIImage imageNamed:@"profile-icon-medicine"];
    }
    return image;
}

- (void)myProfileRowViewWasTapped:(SWMyProfileRowView *)sender
{
    if ([sender isEqual:self.reportHistoryRow])
    {
        // Log to Flurry
        [Flurry logEvent:@"My Profile, Report History button tapped"];
        
        SWMyReportsViewController *vc = [[SWMyReportsViewController alloc] init];
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - SWMyProfileSectionHeaderViewDelegate

- (NSString *)myProfileSectionHeaderViewTitleString:(SWMyProfileSectionHeaderView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.reportHistorySectionHeader])
    {
        title = @"REPORT HISTORY";
    }
    else if ([sender isEqual:self.contactSectionHeader])
    {
        title = @"CONTACT";
    }
    else if ([sender isEqual:self.aboutSectionHeader])
    {
        title = @"ABOUT";
    }
    return title;
}

#pragma mark - SWMyProfileHeaderViewDelegate

- (NSString *)myProfileHeaderViewNameString:(SWMyProfileHeaderView *)sender
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] fullName];
    if (answer)
    {
        return answer;
    }
    return @"Unknown";
}

- (NSString *)myProfileHeaderViewUsernameString:(SWMyProfileHeaderView *)sender
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].username;
    if (answer)
    {
        return answer;
    }
    return @"@unknown";
}

- (UIImage *)myProfileHeaderViewProfilePicImage:(SWMyProfileHeaderView *)sender
{
    NSString *stringURL = [self userDataProfilePicStringURL];
    UIImage *imageToReturn = [self.imageForStringURL objectForKey:stringURL];
    if (!imageToReturn)
    {
        if ([self.stringURLCurrentlyLoading containsObject:stringURL])
        {
            //NSLog(@"Skip, work in progress for stringURL %@", stringURL);
        }
        else
        {
            //NSLog(@"Start, fetch for stringURL %@", stringURL);
			if(stringURL != nil && ![stringURL isEqual:[NSNull null]] && ![stringURL isEqualToString:@""]){
				[self.stringURLCurrentlyLoading addObject:stringURL]; // Log as currently loading
				NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
				[[session dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
							//NSLog(@"Background Thread...");
					dispatch_async(dispatch_get_main_queue(), ^{
							//NSLog(@"Main Thread...");
						[self.stringURLCurrentlyLoading removeObject:stringURL]; // Remove from currently loading
						UIImage *image = [UIImage imageWithData:data];
						if (image)
							{
							[self.imageForStringURL setObject:image forKey:stringURL]; // Load to cache
																					   //NSLog(@"Loaded image to cache for stringURL %@", stringURL);
							[sender setProfilePicImage:image];
							}
						else
							{
							[sender setProfilePicImage:[UIImage imageNamed:@"avatar-empty"]];
							}
					});
				}] resume];
			}
        }
    }
    return imageToReturn;
}

#pragma mark - SWMyReportsViewControllerDelegate

- (void)myReportsViewControllerWantsToDismiss
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Target Action

- (void)logoutButtonTouchUpInside:(id)sender
{
    // Log user out
    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"Logging out..."];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SWUserCDM logUserOut];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSWUserDidJustLogOutNotification
                                                            object:self
                                                          userInfo:@{}];
		[[SDImageCache sharedImageCache] clearMemory];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PopToFamilyView" object:self];
        [SWHelper helperDismissFullScreenActivityIndicator];
        [self.delegate myProfileViewControllerWantsToPopOffTheStack:self];
    });
}

- (void)leftBarButtonItemTapped:(id)sender
{
    return [self.delegate myProfileViewControllerWantsToPopOffTheStack:self];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    [Flurry logEvent:@"My Profile - Edit Button Tapped"];
    SWEditMyProfileViewController *editMyProfileVC = [[SWEditMyProfileViewController alloc] init];
    editMyProfileVC.delegate = self;
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:editMyProfileVC];
    navVC.navigationBar.translucent = NO;
    [self presentViewController:navVC animated:YES completion:^{}];
    //[self.navigationController pushViewController:editMyProfileVC animated:YES];
}

- (void)viewTapped:(id)sender
{
    //[SWHelper helperShowAlertWithTitle:@"Somebody touched me. I felt energy leave me."];
}

#pragma mark - Helpers

- (NSString *)userDataProfilePicStringURL
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].photoThumb;
    if (answer)
    {
        return answer;
    }
    return [[self.myProfileData objectForKey:@"user"] objectForKey:@"profile_pic_url"];
}

- (NSString *)userDataFirstNameString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].firstName;
    if (answer)
    {
        return answer;
    }
    return [[self.myProfileData objectForKey:@"user"] objectForKey:@"first_name"];
}

- (NSString *)userDataLastNameString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].lastName;
    if (answer)
    {
        return answer;
    }
    return [[self.myProfileData objectForKey:@"user"] objectForKey:@"last_name"];
}

- (NSString *)userDataEmailString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].email;
    if (answer)
    {
        return answer;
    }
    return @"unknown";
    return [[self.myProfileData objectForKey:@"user"] objectForKey:@"email"];
}

- (NSString *)userDataGenderString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].gender;
    if (answer)
    {
        return answer;
    }
    return [[self.myProfileData objectForKey:@"user"] objectForKey:@"gender"];
}

- (NSString *)userDataRaceEthnicityString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].raceEthnicity;
    if (answer)
    {
        return answer;
    }
    return [[self.myProfileData objectForKey:@"user"] objectForKey:@"race"];
}

- (NSString *)userDataGenderFormattedString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].gender;
    if (answer)
    {
        if ([answer isEqualToString:@"1"])
        {
            answer = @"Female";
        }
        if ([answer isEqualToString:@"2"])
        {
            answer = @"Male";
        }
        if ([answer isEqualToString:@"3"])
        {
            answer = @"Other";
        }
        if ([answer isEqualToString:@"4"])
        {
            answer = @"Not Specified";
        }
    }
    else
    {
        [SWUserCDM currentlyLoggedInUser].gender = @"4";
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)userDataRaceEthnicityFormattedString
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].raceEthnicity;
    if (answer)
    {
        if ([answer isEqualToString:@"0"])
        {
            answer = @"Not Specified";
        }
        if ([answer isEqualToString:@"1"])
        {
            answer = @"American Indian / Alaskan Native";
        }
        if ([answer isEqualToString:@"2"])
        {
            answer = @"Asian";
        }
        if ([answer isEqualToString:@"3"])
        {
            answer = @"Black or African American";
        }
        if ([answer isEqualToString:@"4"])
        {
            answer = @"Hispanic or Latino";
        }
        if ([answer isEqualToString:@"5"])
        {
            answer = @"Native Hawaiian or Other Pacific Islander";
        }
        if ([answer isEqualToString:@"6"])
        {
            answer = @"White";
        }
    }
    else
    {
        [SWUserCDM currentlyLoggedInUser].raceEthnicity = @"0";
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)userDataDateOfBirthFormattedString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] birthDateForUI];
    if (answer)
    {
        // Take at face value
    }
    else
    {
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)userDataLocationString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] userLocationForUI];
    if (answer)
    {
        // Take at face value
    }
    else
    {
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)userDataHealthCarePreferenceString
{
    NSString *answer = [[SWUserCDM currentlyLoggedInUser] healthCarePrefForUI];
    if (answer)
    {
        // Take at face value
    }
    else
    {
        answer = @"No Preference";
    }
    return answer;
}

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.headerView.delegate = self;
    self.reportHistoryRow.delegate = self;
    self.emailRow.delegate = self;
    self.genderRow.delegate = self;
    self.raceEthnicityRow.delegate = self;
    self.birthdayRow.delegate = self;
    self.locationRow.delegate = self;
    self.healthCarePreferenceRow.delegate = self;
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // REGISTER FOR NOTIFICATIONS (IF ANY)
    
    // INIT SELF
    self.imageForStringURL = [NSMutableDictionary new];
    self.stringURLCurrentlyLoading = [NSMutableArray new];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.staticHeaderImageView = [[UIImageView alloc] init];
    self.staticHeaderImageOverlayView = [[UIView alloc] init];
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.headerView = [[SWMyProfileHeaderView alloc] init];
    self.reportHistorySectionHeader = [[SWMyProfileSectionHeaderView alloc] init];
    self.reportHistoryRow = [[SWMyProfileRowView alloc] init];
    self.contactSectionHeader = [[SWMyProfileSectionHeaderView alloc] init];
    self.emailRow = [[SWMyProfileRowView alloc] init];
    self.aboutSectionHeader = [[SWMyProfileSectionHeaderView alloc] init];
    self.genderRow = [[SWMyProfileRowView alloc] init];
    self.raceEthnicityRow = [[SWMyProfileRowView alloc] init];
    self.birthdayRow = [[SWMyProfileRowView alloc] init];
    self.locationRow = [[SWMyProfileRowView alloc] init];
    self.healthCarePreferenceRow = [[SWMyProfileRowView alloc] init];
    self.logoutButtonContainer = [[UIView alloc] init];
    self.logoutButton = [[UIButton alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.staticHeaderImageView forKey:@"staticHeaderImageView"];
    [self.viewsDictionary setObject:self.staticHeaderImageOverlayView forKey:@"staticHeaderImageOverlayView"];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.headerView forKey:@"headerView"];
    [self.viewsDictionary setObject:self.reportHistorySectionHeader forKey:@"reportHistorySectionHeader"];
    [self.viewsDictionary setObject:self.reportHistoryRow forKey:@"reportHistoryRow"];
    [self.viewsDictionary setObject:self.contactSectionHeader forKey:@"contactSectionHeader"];
    [self.viewsDictionary setObject:self.emailRow forKey:@"emailRow"];
    [self.viewsDictionary setObject:self.aboutSectionHeader forKey:@"aboutSectionHeader"];
    [self.viewsDictionary setObject:self.genderRow forKey:@"genderRow"];
    [self.viewsDictionary setObject:self.raceEthnicityRow forKey:@"raceEthnicityRow"];
    [self.viewsDictionary setObject:self.birthdayRow forKey:@"birthdayRow"];
    [self.viewsDictionary setObject:self.locationRow forKey:@"locationRow"];
    [self.viewsDictionary setObject:self.healthCarePreferenceRow forKey:@"healthCarePreferenceRow"];
    [self.viewsDictionary setObject:self.logoutButtonContainer forKey:@"logoutButtonContainer"];
    [self.viewsDictionary setObject:self.logoutButton forKey:@"logoutButton"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.staticHeaderImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.staticHeaderImageOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.reportHistorySectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.reportHistoryRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.contactSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.aboutSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.genderRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.raceEthnicityRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.birthdayRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.locationRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.healthCarePreferenceRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.logoutButtonContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.logoutButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.staticHeaderImageView];
    [self.view addSubview:self.staticHeaderImageOverlayView];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.headerView];
    [self.contentView addSubview:self.reportHistorySectionHeader];
    [self.contentView addSubview:self.reportHistoryRow];
    [self.contentView addSubview:self.contactSectionHeader];
    [self.contentView addSubview:self.emailRow];
    [self.contentView addSubview:self.aboutSectionHeader];
    [self.contentView addSubview:self.genderRow];
    [self.contentView addSubview:self.raceEthnicityRow];
    [self.contentView addSubview:self.birthdayRow];
    [self.contentView addSubview:self.locationRow];
    [self.contentView addSubview:self.healthCarePreferenceRow];
    [self.contentView addSubview:self.logoutButtonContainer];
    [self.logoutButtonContainer addSubview:self.logoutButton];
    
    // LAYOUT
    
    // Layout STATIC HEADER IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[staticHeaderImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[staticHeaderImageView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.staticHeaderImageViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.staticHeaderImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:staticHeaderImageViewDefaultHeight];
    [self.view addConstraint:self.staticHeaderImageViewHeightConstraint];
    
    // Layout STATIC HEADER OVERLAY IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[staticHeaderImageOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[staticHeaderImageOverlayView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.staticHeaderImageOverlayViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.staticHeaderImageOverlayView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:staticHeaderImageViewDefaultHeight];
    [self.view addConstraint:self.staticHeaderImageOverlayViewHeightConstraint];
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout HEADER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[headerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[headerView(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.headerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
    
    // Layout REPORT HISTORY SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[reportHistorySectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[headerView]-(0)-[reportHistorySectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout REPORT HISTORY ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[reportHistoryRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[reportHistorySectionHeader]-(0)-[reportHistoryRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout CONTACT SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contactSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[reportHistoryRow]-(0)-[contactSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout EMAIL ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[emailRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[contactSectionHeader]-(0)-[emailRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout ABOUT SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[aboutSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[emailRow]-(0)-[aboutSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout GENDER ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[genderRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[aboutSectionHeader]-(0)-[genderRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout RACE/ETHNICITY ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[raceEthnicityRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[genderRow]-(0)-[raceEthnicityRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout BIRTHDAY ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[birthdayRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[raceEthnicityRow]-(0)-[birthdayRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout LOCATION ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[locationRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[birthdayRow]-(0)-[locationRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout HEALTH CARE PREFERENCE ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[healthCarePreferenceRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[locationRow]-(0)-[healthCarePreferenceRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout LOGOUT BUTTON CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[logoutButtonContainer]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[healthCarePreferenceRow]-(0)-[logoutButtonContainer(110)]-|"] options:0 metrics:0 views:self.viewsDictionary]];// !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR HEIGHT !!!
    
    // Layout LOGOUT BUTTON
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[logoutButton]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(30)-[logoutButton]-(30)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)]];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.navigationBar.translucent = NO;
    
    // Config TITLE
    self.title = @"My Profile";
    self.navigationItem.titleView = nil;
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config STATIC HEADER IMAGE VIEW
    self.staticHeaderImageView.image = [UIImage imageNamed:@"profile-map-background"];
    self.staticHeaderImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.staticHeaderImageView.clipsToBounds = YES;
    self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
    
    // Config STATIC HEADER IMAGE OVERLAY VIEW
    self.staticHeaderImageOverlayView.backgroundColor = [UIColor clearColor];
    self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // Config HEADER VIEW
    self.headerView.delegate = self;
    
    // Config REPORT HISTORY SECTION HEADER
    self.reportHistorySectionHeader.delegate = self;
    
    // Config REPORT HISTORY ROW
    self.reportHistoryRow.delegate = self;
    [self.reportHistoryRow setShowDisclosureIndicatorIcon];
    
    // Config CONTACT SECTION HEADER
    self.contactSectionHeader.delegate = self;
    
    // Config EMAIL ROW
    self.emailRow.delegate = self;
    
    // Config ABOUT SECTION HEADER
    self.aboutSectionHeader.delegate = self;
    
    // Config GENDER ROW
    self.genderRow.delegate = self;
    
    // Config RACE/ETHNICITY ROW
    self.raceEthnicityRow.delegate = self;
    
    // Config BIRTHDAY ROW
    self.birthdayRow.delegate = self;
    
    // Config LOCATION ROW
    self.locationRow.delegate = self;
    
    // Config HEALTH CARE PREFERENCE ROW
    self.healthCarePreferenceRow.delegate = self;
    [self.healthCarePreferenceRow showBottomBorder];
    
    // Config LOGOUT BUTTON
    [self.logoutButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Logout" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:17]}] forState:UIControlStateNormal];
    [self.logoutButton setBackgroundColor:[SWColor colorSickweatherWhite255x255x255]];
    [self.logoutButton.layer setBorderColor:[SWColor colorSickweatherGrayText135x135x135].CGColor];
    [self.logoutButton.layer setBorderWidth:0.5];
    [self.logoutButton.layer setCornerRadius:2];
    [self.logoutButton addTarget:self action:@selector(logoutButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config LOGOUT BUTTON CONTAINER
    self.logoutButtonContainer.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    
    // COLOR FOR DEVELOPMENT PURPOSES
//    self.staticHeaderImageView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
//    self.staticHeaderImageOverlayView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
//    self.scrollView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
//    self.contentView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
//    self.headerView.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
//    self.reportHistorySectionHeader.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
//    self.reportHistoryRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
//    self.contactSectionHeader.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
//    self.emailRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
//    self.aboutSectionHeader.backgroundColor = [SWColor color:[UIColor orangeColor] usingOpacity:0.5];
//    self.genderRow.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
//    self.raceEthnicityRow.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
//    self.birthdayRow.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
//    self.locationRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
//    self.healthCarePreferenceRow.backgroundColor = [SWColor color:[UIColor cyanColor] usingOpacity:0.5];
//    self.logoutButton.backgroundColor = [SWColor color:[UIColor grayColor] usingOpacity:0.5];
}

@end
