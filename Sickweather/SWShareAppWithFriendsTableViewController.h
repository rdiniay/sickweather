//
//  SWShareAppWithFriendsTableViewController.h
//  Sickweather
//
//  Created by John Erck on 1/28/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWShareAppWithFriendsTableViewController;

@protocol SWShareAppWithFriendsTableViewControllerDelegate <NSObject>
- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulMessageSend:(SWShareAppWithFriendsTableViewController *)vc;
- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:(SWShareAppWithFriendsTableViewController *)vc;
- (void)shareAppWithFriendsTableViewControllerWantsToDismissBySkipping:(SWShareAppWithFriendsTableViewController *)vc;
- (void)shareAppWithFriendsTableViewControllerWantsToDismiss:(SWShareAppWithFriendsTableViewController *)vc;
@end

@interface SWShareAppWithFriendsTableViewController : UIViewController
@property (weak, nonatomic) id <SWShareAppWithFriendsTableViewControllerDelegate> delegate;
- (void)configureForDayToDayUse;
@end
