//
//  SWSlidingViewController.m
//  Sickweather
//
//  Created by John Erck on 10/9/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWSlidingViewController.h"

@interface SWSlidingViewController ()

@end

@implementation SWSlidingViewController

- (BOOL)shouldAutorotate
{
    return [self.topViewController shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

- (void)resetTopViewAnimated:(BOOL)animated
{
    [super resetTopViewAnimated:animated onComplete:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWECSlidingViewControllerDidSlideTopViewControllerBackOnStackNotification
                                                        object:self
                                                      userInfo:@{}];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.anchorRightPeekAmount = [SWHelper helperECSlidingViewControllerAnchorRightPeekAmount];
}

@end
