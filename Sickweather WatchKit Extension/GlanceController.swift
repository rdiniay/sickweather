//
//  GlanceController.swift
//  Sickweather WatchKit Extension
//
//  Created by John Erck on 2/18/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class GlanceController: WKInterfaceController, WCSessionDelegate {
    /** Called when the session has completed activation. If session state is WCSessionActivationStateNotActivated there will be an error with more details. */
    @available(watchOS 2.2, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        // STUB...
    }

    
    var sickScore = 0 // 0-100
    var locationName = "Scanning..."
    @IBOutlet weak var locationLabel: WKInterfaceLabel!
    @IBOutlet weak var riskLabel: WKInterfaceLabel!
    @IBOutlet weak var sickScoreIntLabel: WKInterfaceLabel!
    @IBOutlet weak var circleGroup: WKInterfaceGroup!
    
    func updateUIForCurrentSickScoreValue() {
        
        // Get color risk object
        let currentColorRiskGlanceBackgroundImageName = SWHelper.helperSickScoreGetColorRiskGlanceBackgroundImageNameForScore(sickScore)
        
        // Configure background
        circleGroup.setBackgroundImageNamed(currentColorRiskGlanceBackgroundImageName.glanceBackgroundImageName)
        
        // Update location label
        locationLabel.setText(locationName)
        
        // Update risk label
        if locationName == "No Location" {
            riskLabel.setText("Set via iPhone")
        } else {
            riskLabel.setText(currentColorRiskGlanceBackgroundImageName.risk)
        }
        riskLabel.setTextColor(currentColorRiskGlanceBackgroundImageName.color)
        
        // Set SickScore
        if sickScore == 0 {
            sickScoreIntLabel.setText("--")
        } else {
            sickScoreIntLabel.setText("\(sickScore)")
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        var session: WCSession? = nil;
        if WCSession.isSupported()
        {
            session = WCSession.default
            session!.delegate = self
            session!.activate()
        }
        
        // Issue dismissable system location prompt one time on watch
        let myLocationManager = CLLocationManager();
        myLocationManager.requestAlwaysAuthorization();
        
        // Configure interface objects here.
        updateUIForCurrentSickScoreValue()
        
        // Get updated sick score
        session?.sendMessage(["handleWatchKitExtensionRequestMethod": "getSickScoreForCurrentLocation"], replyHandler: { (response) -> Void in
            // Handle response from iphone app
            /*
             "sickscore": "88",
             "location_name": "Huntington Beach",
             "top_illness_ids": ["1","2","3"],
             "top_illness_names": ["Flu","Cold","Fever"],
             "top_illness_icon_letters": ["Fl","Co","Fe"]
             */
            
            if let sickScore = response["sickscore"] as? String {
                self.sickScore = Int(sickScore)!
            }
            if let locationName = response["location_name"] as? String {
                self.locationName = locationName
            }
            self.updateUIForCurrentSickScoreValue()
        }, errorHandler: { (error) -> Void in
                //print(error)
        })
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
