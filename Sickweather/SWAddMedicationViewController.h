//
//  SWAddMedicationViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 1/31/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"
#import "SWAddMedicationNameViewController.h"

@interface SWAddMedicationViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate,SWAddMedicationNameViewControllerDelegate>
@property (nonatomic) BOOL isFromFamilyScreen;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@property (nonatomic,weak) IBOutlet UIButton *nameButton;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,weak) IBOutlet UIButton *unitButton;
@property (nonatomic,weak) IBOutlet UITextField *doseTextField;
@property (nonatomic,weak) IBOutlet UITextView *notesTextView;
@end
