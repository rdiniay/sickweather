//
//  SWColors.m
//  Sickweather
//
//  Created by John Erck on 1/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWColor.h"

@implementation SWColor

+ (UIColor *)colorSickweatherBlue41x171x226
{
    return [UIColor colorWithRed:41/255.0 green:171/255.0 blue:226/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherFacebookBlue59x89x152;
{
    return [UIColor colorWithRed:59/255.0 green:89/255.0 blue:152/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherGreen76x217x100
{
    return [UIColor colorWithRed:76/255.0 green:217/255.0 blue:100/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherBlueSelected2x146x207
{
    return [UIColor colorWithRed:2/255.0 green:146/255.0 blue:207/255.0 alpha:1.0];
}

+ (UIColor *)sickweatherSuperLightGray247x247x247
{
    return [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherGrayText135x135x135
{
    return [UIColor colorWithRed:135/255.0 green:135/255.0 blue:135/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherLightGray204x204x204
{
    return [UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherDarkGrayText104x104x104
{
    return [UIColor colorWithRed:104/255.0 green:104/255.0 blue:104/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherWhite255x255x255
{
    return [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherBlack0x0x0
{
    return [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideBlue37x169x224
{
    return [UIColor colorWithRed:37/255.0 green:169/255.0 blue:224/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideRed255x1x0
{
    return [UIColor colorWithRed:255/255.0 green:1/255.0 blue:0/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideOrange252x111x6
{
    return [UIColor colorWithRed:252/255.0 green:111/255.0 blue:6/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideYellow249x237x11
{
    return [UIColor colorWithRed:249/255.0 green:237/255.0 blue:11/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideGrayDark35x31x32
{
    return [UIColor colorWithRed:35/255.0 green:31/255.0 blue:32/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideGray79x76x77
{
    return [UIColor colorWithRed:79/255.0 green:76/255.0 blue:77/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideGrayLight133x133x134
{
    return [UIColor colorWithRed:133/255.0 green:133/255.0 blue:134/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideGreen68x157x68
{
    return [UIColor colorWithRed:68/255.0 green:157/255.0 blue:68/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideOffwhite246x246x246
{
    return [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideWalgreensRed227x24x55
{
    return [UIColor colorWithRed:227/255.0 green:24/255.0 blue:55/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherStyleGuideSearchBackground230x230x230
{
    return [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0];
}

+ (UIColor *)color:(UIColor *)color usingOpacity:(CGFloat)opacity;
{
    const CGFloat *channels = CGColorGetComponents(color.CGColor);
    return [UIColor colorWithRed:channels[0] green:channels[1] blue:channels[2] alpha:opacity];
}

+ (UIColor *)colorChatBoxGray
{
    return [UIColor colorWithRed:210/255.0 green:213/255.0 blue:219/255.0 alpha:1.0];
}

+ (UIColor *)colorChatBoxGrayAccent
{
    return [UIColor colorWithRed:187/255.0 green:194/255.0 blue:201/255.0 alpha:1.0];
}

+ (UIColor *)colorSickweatherGreen68x157x68
{
	return [UIColor colorWithRed:68/255.0 green:157/255.0 blue:68/255.0 alpha:1.0];
}

@end
