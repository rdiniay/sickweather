//
//  SWCreateGroupViewController.h
//  Sickweather
//
//  Created by John Erck on 5/19/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWCreateGroupViewController;

@protocol SWCreateGroupViewControllerDelegate <NSObject>
- (void)createGroupViewControllerWantsToDismissAfterSuccessfullyCreatingNewGroup:(SWCreateGroupViewController *)sender;
- (void)createGroupViewControllerWantsToDismissDueToUserCancel:(SWCreateGroupViewController *)sender;
@end

@interface SWCreateGroupViewController : UIViewController
@property (nonatomic, assign) id <SWCreateGroupViewControllerDelegate> delegate;
@end
