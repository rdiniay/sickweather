//
//  SWGeocodeManager.h
//  Sickweather
//
//  Created by John Erck on 5/24/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWGeocodeManager : NSObject

- (void)reverseGeocodeLocation:(CLLocation *)location completionHandler:(CLGeocodeCompletionHandler)completionHandler;
- (void)geocodeAddressString:(NSString *)addressString completionHandler:(CLGeocodeCompletionHandler)completionHandler;

- (id)initWithGeocoder:(CLGeocoder *)geocoder;

@end
