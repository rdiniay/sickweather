//
//  SWSettings.m
//  Sickweather
//
//  Created by John Erck on 10/1/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWSettings.h"

@implementation SWSettings

+ (SWSettings *)sharedInstance
{
    static SWSettings *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SWSettings alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (NSNumber *)sicknessReportGeofenceRadiusInMeters
{
    // Geofence circle radius measured in meters
    return @(200); // 400 meters = 0.25 miles
}

- (NSTimeInterval)minimumBackgroundFetchInterval
{
    return 60*60*12; // Once every 12 hours
}

- (CLLocationDegrees)defaultZoomLevelLatLonDelta
{
    return 0.1; // Good zoom on HB...
}

- (CLLocationDegrees)sponsoredMarkerUpperLimitZoomLevelLatLonDelta
{
    return 0.2;
}

- (CLLocationDegrees)radarLowerLimitZoomLevelLatLonDelta
{
    return 3;
}

- (CLLocationDegrees)animatedRadarDefaultZoomLevelLatLonDelta
{
    return 50;
}

- (NSUInteger)cacheMemoryCapacity
{
    return 1048576 * 100;
}

- (NSUInteger)cacheDiskCapacity
{
    return 1048576 * 200;
}

- (NSString *)cacheDiskPath
{
    return @"app_cache";
}

- (double)requestTimeoutIntervalInSeconds
{
    return 30.0;
}

@end
