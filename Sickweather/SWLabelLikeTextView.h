//
//  SWLabelLikeTextView.h
//  Sickweather
//
//  Created by John Erck on 4/23/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWLabelLikeTextView : UITextView

@end
