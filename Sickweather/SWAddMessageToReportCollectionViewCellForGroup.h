//
//  SWAddMessageToReportCollectionViewCellForExampleView1.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAddMessageToReportCollectionViewCellForGroup : UICollectionViewCell
- (void)addMessageToReportCollectionViewCellForGroupUpdateDefaultInsetsLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
- (void)setServerDict:(NSDictionary *)serverDict;
- (void)setStyleAsSingleRecordCap;
- (void)setStyleAsTopCap;
- (void)setStyleAsCenterCap;
- (void)setStyleAsBottomCap;
- (void)setStyleAsSelected;
- (void)setStyleAsUnselected;
@end
