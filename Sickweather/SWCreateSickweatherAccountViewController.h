//
//  SWCreateSickweatherAccountViewController.h
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWCreateSickweatherAccountViewController;

@protocol SWCreateSickweatherAccountViewControllerDelegate <NSObject>
- (void)createSickweatherAccountViewControllerDidDismissWithSuccessfulLogin:(SWCreateSickweatherAccountViewController *)vc;
- (void)createSickweatherAccountViewControllerDidDismissWithoutLoggingIn:(SWCreateSickweatherAccountViewController *)vc;
@end

@interface SWCreateSickweatherAccountViewController : UIViewController
@property (weak, nonatomic) id <SWCreateSickweatherAccountViewControllerDelegate> delegate;
@end
