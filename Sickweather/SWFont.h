//
//  SWFonts.h
//  Sickweather
//
//  Created by John Erck on 1/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWFont : NSObject

+ (void)fontLogAllOptions;
+ (UIFont *)fontDuepuntozeroRegularFontOfSize:(CGFloat)size;
+ (UIFont *)fontDuepuntozeroBlackFontOfSize:(CGFloat)size;
+ (UIFont *)fontDuepuntozeroBoldFontOfSize:(CGFloat)size;
+ (UIFont *)fontStandardFontOfSize:(CGFloat)size;
+ (UIFont *)fontStandardBoldFontOfSize:(CGFloat)size;
+ (UIFont *)fontAwesomeOfSize:(CGFloat)size;

@end
