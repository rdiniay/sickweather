//
//  SWFamilyMemberModal.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 03/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyMemberModal.h"

@implementation SWFamilyMemberModal
- (instancetype)init
{
    self = [super init];
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
		if ([NSNull null] != [dictionary valueForKey:@"family_id"]) self.family_id = [dictionary valueForKey:@"family_id"]; else self.family_id = @"";
        if ([NSNull null] != [dictionary valueForKey:@"member_id"]) self.member_id = [dictionary valueForKey:@"member_id"]; else self.member_id = @"";
        if ([NSNull null] != [dictionary valueForKey:@"user_id"]) self.user_id = [dictionary valueForKey:@"user_id"]; else self.user_id = @"";
        if ([NSNull null] != [dictionary valueForKey:@"name_first"] && ![[dictionary valueForKey:@"name_first"] isEqualToString:@""]) self.name_first = [dictionary valueForKey:@"name_first"]; else self.name_first = @"Member";
        if ([NSNull null] != [dictionary valueForKey:@"name_last"]) self.name_last = [dictionary valueForKey:@"name_last"]; else self.name_last = @"";
        if ([NSNull null] != [dictionary valueForKey:@"nickname"]) self.nickname = [dictionary valueForKey:@"nickname"]; else self.nickname = @"";
        if ([NSNull null] != [dictionary valueForKey:@"email"]) self.email = [dictionary valueForKey:@"email"]; else self.email = @"";
        if ([NSNull null] != [dictionary valueForKey:@"photo_original"]) self.photo_original = [dictionary valueForKey:@"photo_original"]; else self.photo_original = @"";
        if ([NSNull null] != [dictionary valueForKey:@"photo_thumb"]) self.photo_thumb = [dictionary valueForKey:@"photo_thumb"]; else self.photo_thumb = @"";
        if ([NSNull null] != [dictionary valueForKey:@"gender"]) self.gender = [dictionary valueForKey:@"gender"]; else self.gender = @"";
        if ([NSNull null] != [dictionary valueForKey:@"birthdate"]) self.birthdate = [dictionary valueForKey:@"birth_date"]; else self.birthdate = @"";
		if ([NSNull null] != [dictionary valueForKey:@"confirmed"]) self.confirmed = [dictionary valueForKey:@"confirmed"]; else self.confirmed = @"";
        
    }
    return self;
}

- (NSString *)birthDateDayForServer
{
	NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
	[outputDateFormat setDateFormat:@"dd"];
	return [outputDateFormat stringFromDate:[self birthDateAsDate]];
}

- (NSString *)birthDateMonthForServer
{
	NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
	[outputDateFormat setDateFormat:@"MM"];
	return [outputDateFormat stringFromDate:[self birthDateAsDate]];
}

- (NSString *)birthDateYearForServer
{
	NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
	[outputDateFormat setDateFormat:@"yyyy"];
	return [outputDateFormat stringFromDate:[self birthDateAsDate]];
}

- (NSDate *)birthDateAsDate
{
	// The server serves up dates like so: 1985-09-02
	NSDateFormatter *serverDateFormat = [[NSDateFormatter alloc] init];
	[serverDateFormat setDateFormat:@"yyyy-MM-dd"];
	NSDate *date = [serverDateFormat dateFromString:self.birthdate];
	return date;
}

@end
