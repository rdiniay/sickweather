//
//  SWAddFamilyMemberViewController.h
//  Sickweather
//
//  Created by John Erck on 8/17/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@protocol SWAddFamilyMemberViewControllerDelegate
- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender withFamily:(SWFamilyMemberModal*)member;
- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender;
- (void)addFamilyMemberViewControllerWantsToCancel:(id)sender;
@optional
- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender withFamily:(SWFamilyMemberModal*)member withPhoto:(UIImage*)image;
@end

@interface SWAddFamilyMemberViewController : UIViewController
@property (nonatomic, weak) id<SWAddFamilyMemberViewControllerDelegate> delegate;
@property (strong, nonatomic) SWFamilyMemberModal *familyMember;
@property (nonatomic) BOOL wantsToEdit;
@end
