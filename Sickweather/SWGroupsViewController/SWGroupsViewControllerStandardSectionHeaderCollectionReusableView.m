//
//  SWGroupsViewControllerStandardSectionHeaderCollectionReusableView.m
//  Sickweather
//
//  Created by John Erck on 8/25/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "SWGroupsViewControllerStandardSectionHeaderCollectionReusableView.h"
#import "NSString+FontAwesome.h"

@interface SWGroupsViewControllerStandardSectionHeaderCollectionReusableView () <UISearchBarDelegate>
@property (strong, nonatomic) NSMutableArray *updateStyleConstraints;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UIButton *createNewGroupButton;
@end

@implementation SWGroupsViewControllerStandardSectionHeaderCollectionReusableView

#pragma mark - Custom Getters/Setters

- (void)setStyleStringKey:(NSString *)styleStringKey
{
    _styleStringKey = styleStringKey;
    [self helpersUpdateStyle];
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    [self helpersUpdateStyle];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.delegate searchBarTextDidChange:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.delegate searchBarTextDidBeginEditing:searchBar];
}

#pragma mark - Public Methods

- (void)setCurrentSearchStringText:(NSString *)searchStringText
{
    self.searchBar.text = searchStringText;
}

- (void)setFocusOnSearch
{
    [self.searchBar becomeFirstResponder];
}

- (void)showCreateNewGroupButton
{
    self.createNewGroupButton.hidden = NO;
}

#pragma mark - Target/Action

- (void)createNewGroupButtonTapped:(id)sender
{
    [self.delegate createNewGroupButtonTapped:self];
}

#pragma mark - Helpers

- (void)helpersUpdateStyle
{
    [self removeConstraints:self.updateStyleConstraints];
    self.updateStyleConstraints = [NSMutableArray new];
    if ([self.styleStringKey isEqualToString:@"searchSectionStyle"])
    {
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[searchBar]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[searchBar]|"] options:0 metrics:0 views:self.viewsDictionary]];
        if (self.title)
        {
            [self.titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:self.title attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12], NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}]];
        }
        self.backgroundColor = [SWColor colorSickweatherStyleGuideSearchBackground230x230x230];
    }
    else if ([self.styleStringKey isEqualToString:@"subSectionStyle"])
    {
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[createNewGroupButton]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[createNewGroupButton]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[titleLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(8)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        if (self.title)
        {
            NSMutableAttributedString *data = [[NSMutableAttributedString alloc] initWithString:self.title attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12], NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}];
            if ([self.title isEqualToString:@"GROUPS AROUND YOU"])
            {
                CGFloat foursquareFontSize = 10;
                [data appendAttributedString:[[NSAttributedString alloc] initWithString:@"  provided by foursquare" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardFontOfSize:foursquareFontSize]}]];
                //[data appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaFoursquare] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontAwesomeOfSize:foursquareFontSize]}]];
                //[data appendAttributedString:[[NSAttributedString alloc] initWithString:@")" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardFontOfSize:foursquareFontSize]}]];
            }
            [self.titleLabel setAttributedText:data];
        }
        self.backgroundColor = [SWColor colorSickweatherStyleGuideGrayLight133x133x134];
    }
    else // regularSectionStyle
    {
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[titleLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.updateStyleConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        if (self.title)
        {
            [self.titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:self.title attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12], NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77]}]];
        }
        self.backgroundColor = [UIColor clearColor];
    }
    [self addConstraints:self.updateStyleConstraints];
    [self layoutIfNeeded];
}

#pragma mark - Life Cycle Methods

- (void)prepareForReuse
{
    self.createNewGroupButton.hidden = YES;
    [self helpersUpdateStyle];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASIC PROPS
        self.updateStyleConstraints = [NSMutableArray new];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.titleLabel = [[UILabel alloc] init];
        self.searchBar = [[UISearchBar alloc] init];
        self.createNewGroupButton = [[UIButton alloc] init];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.searchBar.translatesAutoresizingMaskIntoConstraints = NO;
        self.createNewGroupButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // CREATE AUTO LAYOUT REFERENCE DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.viewsDictionary setObject:self.searchBar forKey:@"searchBar"];
        [self.viewsDictionary setObject:self.createNewGroupButton forKey:@"createNewGroupButton"];
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.titleLabel];
        [self addSubview:self.searchBar];
        [self addSubview:self.createNewGroupButton];
        
        // LAYOUT
        
        // Layout TITLE LABEL
        // (done above per style)
        
        // CONFIG
        
        // Config TITLE LABEL
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        
        // Config SEARCH BAR
        self.searchBar.delegate = self;
        self.searchBar.placeholder = @"Search All Groups";
        
        // Config CREATE NEW GROUP BUTTON
        NSMutableAttributedString *faPlusCircle = [[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaPlusCircle] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontAwesomeOfSize:18]}];
        [faPlusCircle appendAttributedString:[[NSAttributedString alloc] initWithString:@"  Create Group   " attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}]];
        [self.createNewGroupButton setAttributedTitle:faPlusCircle forState:UIControlStateNormal];
        [self.createNewGroupButton addTarget:self action:@selector(createNewGroupButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // COLOR FOR DEV
        //self.titleLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.searchBar.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.createNewGroupButton.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
    }
    return self;
}

@end
