//
//  SWOnboardingViewController.h
//  Sickweather
//
//  Created by John Erck on 12/20/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWOnboardingViewControllerDelegate
- (void)onboardingWantsToDismiss:(id)sender;
@end

@interface SWOnboardingViewController : UIViewController
@property (nonatomic, weak) id<SWOnboardingViewControllerDelegate> delegate;
@end

