//
//  SWReportModel.m
//  Sickweather
//
//  Created by John Erck on 10/16/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWReportModel.h"
#import "SWSettings.h"

@interface SWReportModel () <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

// http://stackoverflow.com/questions/3935574/can-i-use-objective-c-blocks-as-properties
@property(readwrite, copy) SWReportRequestCompletionHandler completionHandler;
@property (strong, nonatomic) NSURLSession *reportsRequestSession;
@property (strong, nonatomic) NSMutableData *reportsRequestResponseData;

@end

@implementation SWReportModel

#pragma mark - Custom Getters/Setters

- (NSMutableData *)reportsRequestResponseData
{
    if (!_reportsRequestResponseData) {
        _reportsRequestResponseData = [[NSMutableData alloc] init];
    }
    return _reportsRequestResponseData;
}

#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    self.reportsRequestSession = nil; // (Don't let executeReportsRequestForIllnessTypeID think this is still around managing a running a task)
    
//    //NSLog(@"%s", __FUNCTION__);
    // After invalidating the session, when all outstanding tasks have been canceled or have finished, the session sends the delegate a URLSession:didBecomeInvalidWithError: message. When that delegate method returns, the session disposes of its strong reference to the delegate.
    
    // Important: The session object keeps a strong reference to the delegate until your app explicitly invalidates the session. If you do not invalidate the session, your app leaks memory.
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
//    //NSLog(@"%s", __FUNCTION__);
    // Ok great, we can now make use of prop that hold's response data...
    
    // Convert to JSON
    NSError *jsonError = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:self.reportsRequestResponseData options:kNilOptions error:&jsonError];
    
    // Future TODO: Take each NSDict and convert to SWReportModel
    // before returning the collection. This is the proper spot
    // to go from "string land" back to "typed land".
    
    // Execute user provided callback on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        self.completionHandler(jsonArray, jsonError);
    });
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
//    //NSLog(@"%s", __FUNCTION__);
    // String it all together...
    [self.reportsRequestResponseData appendData:data];
}

#pragma mark - Public Methods

- (void)executeReportsRequestForIllnessTypeID:(NSString *)illnessTypeID lat:(CLLocationDegrees)lat lon:(CLLocationDegrees)lon limit:(NSNumber *)limit withCompletionHandler:(SWReportRequestCompletionHandler)completionHandler;
{
    // Don't do anything for 0,0 requests
    if (lat == 0 && lon == 0)
    {
        completionHandler(nil, nil);
        return;
    }
    // This is basically the only way "into" this class. Therefore,
    // if we write this method properly, there shouldn't really be
    // any way for the client of this object to use it improperly
    // (i.e. if client issues rapid fire requests, we should be okay)
    
    // Okay, this method gets called from the main thread.
    // All the delegate callbacks do NOT happen on the main thread.
    
    // Now, with that said, we first need to know if we already
    // have a well formed session that we can make use of, else
    // we will create one.
    if (!self.reportsRequestSession)
    {
        // Create & configure session configuration object
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.allowsCellularAccess = YES; // Wifi & cellular
        config.timeoutIntervalForRequest = 30.0;
        config.timeoutIntervalForResource = 60.0;
        config.HTTPMaximumConnectionsPerHost = 1;
        config.URLCache = [NSURLCache sharedURLCache];
        config.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
        
        // Create session object
        self.reportsRequestSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    
    // Cancel all outstanding tasks since we've been asked to
    // fetch new data, which means any tasks that haven't yet
    // returned are working on getting irrelevant information.
    // Therefore, we cancel them all.
    //[self.reportsRequestSession.delegateQueue cancelAllOperations];
    [self.reportsRequestSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        for (NSURLSessionDataTask *dataTask in dataTasks) {
            [dataTask cancel];
        }
    }];
    
    // Log completion handler block to object property
    self.completionHandler = completionHandler;
    
    // Reset data prop to be empty
    self.reportsRequestResponseData = [[NSMutableData alloc] init];
    
    // Create URL
    NSString *baseEndpointURL = @"https://mobilesvc.sickweather.com/ws/v1.1/getMarkersInRadius.php";
    NSString *endPointWithArgs = [NSString stringWithFormat:@"%@?ids=%@&lat=%f&lon=%f&limit=%@&api_key=%@", baseEndpointURL, illnessTypeID, lat, lon, limit, [SWHelper helperSickweatherAPIKey]];
    NSURL *dynamicURL = [NSURL URLWithString:endPointWithArgs];
    
    // Add data task to session
    NSURLSessionDataTask *reportsRequestSessionDataTask = [self.reportsRequestSession dataTaskWithURL:dynamicURL];
    
    // Print some debug info...
    //SWDLog(@"DATA TASK STARTING UP FOR URL = %@", dynamicURL);
    //SWDLog(@"DATA TASK ids = %@", illnessTypeID);
    //SWDLog(@"DATA TASK lat = %f", lat);
    //SWDLog(@"DATA TASK lon = %f", lon);
    //SWDLog(@"DATA TASK limit = %@", limit);
    
    // Start the data task we've already added to our session (note: data tasks initialize into a suspended state)
    [reportsRequestSessionDataTask resume];
}

- (id)initUsingDict:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        self.id = dict[@"id"];
        self.illness = dict[@"illness"];
        self.lat = dict[@"lat"];
        self.lon = dict[@"lon"];
        self.timestamp = dict[@"timestamp"];
    }
    return self;
}

@end
