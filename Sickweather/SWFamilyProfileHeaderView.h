//
//  SWFamilyProfileHeaderView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWFamilyProfileHeaderView;

@protocol SWFamilyProfileHeaderViewDelegate
- (NSString *)familyProfileHeaderViewNameString:(SWFamilyProfileHeaderView *)sender;
- (NSString *)familyProfileHeaderViewNicknameString:(SWFamilyProfileHeaderView *)sender;
- (UIImage *)familyProfileHeaderViewProfilePicImage:(SWFamilyProfileHeaderView *)sender;
@end

@interface SWFamilyProfileHeaderView : UIView
@property (nonatomic, weak) id<SWFamilyProfileHeaderViewDelegate> delegate;
- (void)setProfilePicImage:(UIImage *)image;
@end
