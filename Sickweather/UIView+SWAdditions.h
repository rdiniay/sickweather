//
//  UIView+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 11/21/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SWAdditions)

- (NSMutableArray *)allSubviews;

@end
