//
//  SWShareWithFacebookModel.m
//  Sickweather
//
//  Created by John Erck on 2/12/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWShareWithFacebookModel.h"

@implementation SWShareWithFacebookModel

- (void)setFacebookGraphUser:(NSDictionary *)facebookGraphUser
{
    _facebookGraphUser = facebookGraphUser;
    self.facebookId = [facebookGraphUser objectForKey:@"id"];
}

@end
