//
//  SWIllnessModel.h
//  Sickweather
//
//  Created by John Erck on 10/14/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWModel.h"

typedef void(^SWIllnessesRequestCompletionHandler)(NSArray *illnesses, NSError *error);

@interface SWIllnessModel : SWModel

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *illnessDescription;
- (id)initUsingDict:(NSDictionary *)illness;
+ (NSString *)nameForId:(NSString *)identifier;

@end
