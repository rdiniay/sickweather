//
//  SWGroupProfileCollectionViewCellForMessageSpacer.h
//  Sickweather
//
//  Created by John Erck on 1/22/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWGroupProfileCollectionViewCellForMessageSpacer;

@protocol SWGroupProfileCollectionViewCellForMessageSpacerDelegate
- (void)groupProfileCollectionViewCellForMessageSpacerMessageActionButtonTapped:(SWGroupProfileCollectionViewCellForMessageSpacer *)sender;
@end

@interface SWGroupProfileCollectionViewCellForMessageSpacer : UICollectionViewCell
@property (nonatomic, weak) id<SWGroupProfileCollectionViewCellForMessageSpacerDelegate> delegate;
- (void)groupProfileCollectionViewCellForMessageSpacerSetMarginLeft:(NSNumber *)left right:(NSNumber *)right;
@end
