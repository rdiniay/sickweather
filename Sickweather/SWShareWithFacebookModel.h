//
//  SWShareWithFacebookModel.h
//  Sickweather
//
//  Created by John Erck on 2/12/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWShareWithPersonModel.h"

@interface SWShareWithFacebookModel : SWShareWithPersonModel
@property (strong, nonatomic) NSString *facebookId;
@property (strong, nonatomic) NSDictionary *facebookGraphUser;
@end
