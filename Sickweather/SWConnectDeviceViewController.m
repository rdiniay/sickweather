//
//  SWConnectDeviceViewController.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 07/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWConnectDeviceViewController.h"
#import "SWTemperatureView.h"
#import "SWAddSymptomsViewController.h"

@interface SWConnectDeviceViewController ()
@property (strong, nonatomic) THERMConnection *thermConnection;
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (nonatomic) BOOL isCelcius;
@property BOOL debugLogs;
@property BOOL userInteraction;
@property (strong, nonatomic) SWTemperatureView * temperatureView;
@property (strong, nonatomic) NSDate * lastTimeStamp;
@property (weak, nonatomic) IBOutlet UIImageView *deviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;

@end

@implementation SWConnectDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.debugLogs = YES; // Set to YES to trace bluetooth device activity
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    [self setDeviceType];
    self.lastTimeStamp = [[NSDate alloc] init];
	self.userInteraction = NO;
    BOOL showForgetButton = NO;
    switch (self.deviceType) {
        case swaive:
            showForgetButton = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothSwaiveDeviceKey]];
            break;
        case kinsa:
            showForgetButton = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothKinsaDeviceKey]];
            break;
        case philips:
            showForgetButton = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants bluetoothPhilipsDeviceKey]];
            break;
    }
    if (showForgetButton){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Forget" style:UIBarButtonItemStylePlain target:self action:@selector(forgetDevice:)];
    }
	self.isCelcius = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants isCelsiusTemperatureTypeDefaultKey]];
	[self.switchTempScale setRightSelected:self.isCelcius];
//    [self initializeTemperatureViewWith:103]; // uncomment and set the value for debugging without device.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.thermConnection = [THERMConnection sharedManager];
    self.thermConnection.delegate = self;
    [self.thermConnection initialize];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(self.peripheral != nil){
        [self.thermConnection disconnectTHERM:self.peripheral];
        self.peripheral = nil;
    }
    self.thermConnection.delegate = nil;
    self.thermConnection = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Target/Actions

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)setDeviceType
{
    UIImage* deviceImage;
    switch (self.deviceType) {
        case philips:
            deviceImage = [UIImage imageNamed:@"thermometer-philips"];
            break;
        case kinsa:
            deviceImage = [UIImage imageNamed:@"thermometer-kinsa"];
            break;
        default:
            deviceImage = [UIImage imageNamed:@"thermometer-swaive"];
            break;
    }
    self.deviceImageView.image = deviceImage;
    [self.deviceImageView setNeedsDisplay];
}

- (IBAction)temperatureScaleValueChanged:(Switch *)sender {
	if (self.userInteraction) {
		self.isCelcius = sender.rightSelected;
		[[NSUserDefaults standardUserDefaults] setBool:self.isCelcius forKey:[SWConstants isCelsiusTemperatureTypeDefaultKey]];
		[self.thermConnection setTemperatureScale:self.isCelcius ? celsius : farenheit];
	}
	self.userInteraction = true;
}

- (void)setConnected
{
    self.statusLabel.text = [NSString stringWithFormat:(@"%@ Connected!"), tickMark];
    self.statusLabel.textColor = [SWColor colorSickweatherStyleGuideGreen68x157x68];
    self.instructionLabel.text = @"Take a temperature reading and we'll record the result in Sickweather.";
    switch (self.deviceType) {
        case swaive:
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[SWConstants bluetoothSwaiveDeviceKey]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        case kinsa:
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[SWConstants bluetoothKinsaDeviceKey]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        case philips:
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[SWConstants bluetoothPhilipsDeviceKey]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Forget" style:UIBarButtonItemStylePlain target:self action:@selector(forgetDevice:)];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceListingReload" object:nil];
}

- (void)setDisconnected
{
    self.statusLabel.text = @"Looking for Device...";
    self.statusLabel.textColor = [SWColor colorSickweatherBlack0x0x0];
    self.instructionLabel.text = @"Make sure your thermometer is turned on and within range of your device.";
}

-(void)forgetDevice:(id)sender
{
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        switch (self.deviceType) {
            case swaive:
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:[SWConstants bluetoothSwaiveDeviceKey]];
                break;
            case kinsa:
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:[SWConstants bluetoothKinsaDeviceKey]];
                break;
            case philips:
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:[SWConstants bluetoothPhilipsDeviceKey]];
                break;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceListingReload" object:nil];
    }];
    [self.navigationController popViewControllerAnimated:YES];
    [CATransaction commit];
}

#pragma mark - Temperature View Controls/Delegate

-(void) initializeTemperatureViewWith:(float)temperature tempScale:(NSString*)type
{
    if (self.temperatureView == nil) {
        self.temperatureView = [[NSBundle mainBundle] loadNibNamed:@"SWTemperatureView" owner:nil options:nil][0];
        self.temperatureView.delegate = self;
        self.temperatureView.frame = [[UIApplication sharedApplication] keyWindow].frame;
        self.temperatureView.clipsToBounds = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:self.temperatureView];
    }
    [self.temperatureView initializeWithTemperature:temperature withTemperatureScale:type];
}

- (void)performViewCancel
{
    self.temperatureView.hidden = YES;
    [self.temperatureView removeFromSuperview];
    self.temperatureView = nil;
}

#pragma mark - Add Tracker

-(void)saveTemperatureWith:(float)temp severity:(NSString*)status{
	BOOL isCelsius = [[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants isCelsiusTemperatureTypeDefaultKey]];
	NSNumberFormatter *twoDecimalPlacesFormatter = [[NSNumberFormatter alloc]init];
	[twoDecimalPlacesFormatter setMaximumFractionDigits:1];
	[twoDecimalPlacesFormatter setMinimumFractionDigits:0];
	NSString *tempValue = [NSString stringWithFormat:@"%@",[twoDecimalPlacesFormatter stringFromNumber:[NSNumber numberWithFloat: isCelsius? [SWHelper convertCelsiusToFarenheit:temp] : temp]]];
	[self addTrackerWith:@{@"member_id":self.familyMember != nil ? self.familyMember.member_id : @"", @"type":[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeBluetoothTemperature],@"value":tempValue}];
	UIStoryboard *familyStoryboard = [UIStoryboard storyboardWithName:@"Family" bundle:nil];
	SWAddSymptomsViewController *addSymptomsViewController = [familyStoryboard instantiateViewControllerWithIdentifier:@"SWAddSymptomsViewController"];
    addSymptomsViewController.title = @"Symptoms";
	addSymptomsViewController.isFromBlueToothTempScreen = true;
	addSymptomsViewController.temp = [NSString stringWithFormat:@"%@%@",[twoDecimalPlacesFormatter stringFromNumber:[NSNumber numberWithFloat: temp]], isCelsius ? @"°C" : @"°F"];
    addSymptomsViewController.isFamilyMember = self.isFamilyMember;
    addSymptomsViewController.familyMember = self.familyMember;
	addSymptomsViewController.severity = status;
	[self.navigationController pushViewController:addSymptomsViewController animated:YES];
}

-(void)addTrackerWith:(NSDictionary*)data{
	[[SWHelper helperAppBackend]appBackendAddTrackerUsingArgs:data completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		dispatch_async(dispatch_get_main_queue(), ^{
		});
	}];
}

#pragma mark - Thermometer Delegates

- (void)deviceConnection:(CBPeripheral *)peripheral {
    if (self.debugLogs) {
        NSLog(@"Device Connected: %@", peripheral.name);
    }
    self.lastTimeStamp = [[NSDate alloc] init];
    [self setConnected];
}

- (void)deviceDisconnection:(CBPeripheral *)peripheral {
    if (self.debugLogs) {
        NSLog(@"Device deviceDisconnection: %@", peripheral.name);
    }
    [self setDisconnected];
}

- (void)deviceDiscovered:(CBPeripheral *)peripheral {
    if (self.debugLogs) {
        NSLog(@"Device deviceDiscovered: %@", peripheral.description);
    }
    if((self.deviceType == swaive && [peripheral.name isEqualToString:@"SWT1A"]) ||
       (self.deviceType == philips && ([peripheral.name isEqualToString:@"Philips ear thermometer"] || [peripheral.name containsString:@"SCH740"] )) ||
       (self.deviceType == kinsa && [peripheral.name containsString:@"KS_"])){
        self.peripheral = peripheral;
        [self.thermConnection connectTHERM:self.peripheral];
    }
}

- (void)getBatteryLevel:(int)value {
    if (self.debugLogs) {
        NSLog(@"Device getBatteryLevel");
    }
}

- (void)getDeviceTemperatureScale:(NSString *)scale {
    if (self.debugLogs) {
        NSLog(@"Device getDeviceTemperatureScale");
    }
	self.isCelcius = [scale isEqualToString:@"C"] ? true : false;
}

- (void)getTemperature:(float)temperatureValue scale:(NSString *)temperatureScale date:(NSDate *)temperatureTimestamp deviceDiscovered:(CBPeripheral *)peripheral {
    // Checking invalid values for Kinsa
	if ((self.deviceType == kinsa && [peripheral.name containsString:@"KS_"]) && (temperatureValue<80 || temperatureValue>120)){
		return;
	}
	
	// Checking invalid values for Philips
	if ((self.deviceType == philips && ([peripheral.name isEqualToString:@"Philips ear thermometer"] || [peripheral.name containsString:@"SCH740"])) && (temperatureValue<27 || temperatureValue>49)){
		return;
	}
	
	// Checking invalid values for Swaive
	if (self.deviceType == swaive && [peripheral.name isEqualToString:@"SWT1A"]){
		if (temperatureValue<80 || temperatureValue>120) {
				return;
		}
	}
	
	// making sure that reading is not old, also stopping multiple temperature readings
	NSTimeInterval secondsBetween = [[[NSDate alloc] init] timeIntervalSinceDate:self.lastTimeStamp];
	if (secondsBetween < 1){
		return;
	}
	NSString * currentDeviceTempScale;
	self.lastTimeStamp = [[NSDate alloc] init];
	if (self.deviceType == swaive && [peripheral.name isEqualToString:@"SWT1A"]) {
		if (self.isCelcius) {
			[self.switchTempScale setRightSelected:true];
			currentDeviceTempScale = @"C";
			temperatureValue = [SWHelper convertFarenhietToCelsius:temperatureValue];
		}else{
			currentDeviceTempScale = @"F";
			[self.switchTempScale setRightSelected:false];
		}
	} else if (self.deviceType == philips && ([peripheral.name isEqualToString:@"Philips ear thermometer"] || [peripheral.name containsString:@"SCH740"])) {
		currentDeviceTempScale = @"C";
		if (!self.isCelcius) {
			currentDeviceTempScale = @"F";
			temperatureValue = [SWHelper convertCelsiusToFarenheit:temperatureValue];
		}
	}else if (self.deviceType == kinsa && [peripheral.name containsString:@"KS_"]){
		currentDeviceTempScale = @"F";
		if (self.isCelcius) {
			currentDeviceTempScale = @"C";
			temperatureValue = [SWHelper convertFarenhietToCelsius:temperatureValue];
		}
	}
    [self initializeTemperatureViewWith:temperatureValue tempScale:currentDeviceTempScale];
}

@end
