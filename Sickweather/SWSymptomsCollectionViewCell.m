//
//  SWSymptomsCollectionViewCell.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWSymptomsCollectionViewCell.h"
#import "SWColor.h"

@implementation SWSymptomsCollectionViewCell

-(void)populateCellWith:(SWSymptomsModal*)symptom {
	self.lblName.text = symptom.name;
	if (symptom.isSelected) {
		self.lblName.textColor = [SWColor colorSickweatherGreen68x157x68];
		self.ImgView.image = [self getSelectedSymptomsImage:symptom.name];
	}else {
		self.lblName.textColor = [SWColor colorSickweatherBlue41x171x226];
		self.ImgView.image = [self getUnSelectedSymptomsImage:symptom.name];
	}
}

#pragma mark - Helper

-(UIImage*)getSelectedSymptomsImage:(NSString*)name{
	if ([name isEqualToString:@"No Symptoms"]){
		return [UIImage imageNamed:@"symptom-icon-no-symptoms-selected"];
	}else if ([name isEqualToString:@"Cough"]){
		return [UIImage imageNamed:@"symptom-icon-cough-selected"];
	}else if ([name isEqualToString:@"Fatigue"]){
		return [UIImage imageNamed:@"symptom-icon-fatigue-selected"];
	}else if ([name isEqualToString:@"Chills"]){
		return [UIImage imageNamed:@"symptom-icon-chills-selected"];
	}else if ([name isEqualToString:@"Runny Nose"]){
		return [UIImage imageNamed:@"symptom-icon-runny-nose-selected"];
	}else if ([name isEqualToString:@"Headache"]){
		return [UIImage imageNamed:@"symptom-icon-headache-selected"];
	}else if ([name isEqualToString:@"Body Aches"]){
		return [UIImage imageNamed:@"symptom-icon-body-aches-selected"];
	}else if ([name isEqualToString:@"Sore Throat"]){
		return [UIImage imageNamed:@"symptom-icon-sore-throat-selected"];
	}else if ([name isEqualToString:@"Stomach Ache"]){
		return [UIImage imageNamed:@"symptom-icon-stomach-ache-selected"];
	}else if ([name isEqualToString:@"Nausea"]){
		return [UIImage imageNamed:@"symptom-icon-nausea-selected"];
	}else if ([name isEqualToString:@"Earache"]){
		return [UIImage imageNamed:@"symptom-icon-ear-ache-selected"];
	}else if ([name isEqualToString:@"Short Of Breath"]){
		return [UIImage imageNamed:@"symptom-icon-short-breath-selected"];
	}else if ([name isEqualToString:@"Diarrhea"]){
		return [UIImage imageNamed:@"symptom-icon-diarrhea-selected"];
	}else if ([name isEqualToString:@"Vomiting"]){
		return [UIImage imageNamed:@"symptom-icon-vomiting-selected"];
	}else if ([name isEqualToString:@"Blurred Vision"]){
		return [UIImage imageNamed:@"symptom-icon-blurred-vision-selected"];
	}else{
		return [UIImage imageNamed:@"symptom-icon-vertigo-selected"];
	}
}

-(UIImage*)getUnSelectedSymptomsImage:(NSString*)name{
	if ([name isEqualToString:@"No Symptoms"]){
		return [UIImage imageNamed:@"symptom-icon-no-symptoms-unselected"];
	}else if ([name isEqualToString:@"Cough"]){
		return [UIImage imageNamed:@"symptom-icon-cough-unselected"];
	}else if ([name isEqualToString:@"Fatigue"]){
		return [UIImage imageNamed:@"symptom-icon-fatigue-unselected"];
	}else if ([name isEqualToString:@"Chills"]){
		return [UIImage imageNamed:@"symptom-icon-chills-unselected"];
	}else if ([name isEqualToString:@"Runny Nose"]){
		return [UIImage imageNamed:@"symptom-icon-runny-nose-unselected"];
	}else if ([name isEqualToString:@"Headache"]){
		return [UIImage imageNamed:@"symptom-icon-headache-unselected"];
	}else if ([name isEqualToString:@"Body Aches"]){
		return [UIImage imageNamed:@"symptom-icon-body-aches-unselected"];
	}else if ([name isEqualToString:@"Sore Throat"]){
		return [UIImage imageNamed:@"symptom-icon-sore-throat-unselected"];
	}else if ([name isEqualToString:@"Stomach Ache"]){
		return [UIImage imageNamed:@"symptom-icon-stomach-ache-unselected"];
	}else if ([name isEqualToString:@"Nausea"]){
		return [UIImage imageNamed:@"symptom-icon-nausea-unselected"];
	}else if ([name isEqualToString:@"Earache"]){
		return [UIImage imageNamed:@"symptom-icon-ear-ache-unselected"];
	}else if ([name isEqualToString:@"Short Of Breath"]){
		return [UIImage imageNamed:@"symptom-icon-short-breath-unselected"];
	}else if ([name isEqualToString:@"Diarrhea"]){
		return [UIImage imageNamed:@"symptom-icon-diarrhea-unselected"];
	}else if ([name isEqualToString:@"Vomiting"]){
		return [UIImage imageNamed:@"symptom-icon-vomiting-unselected"];
	}else if ([name isEqualToString:@"Blurred Vision"]){
		return [UIImage imageNamed:@"symptom-icon-blurred-vision-unselected"];
	}else{
		return [UIImage imageNamed:@"symptom-icon-vertigo-unselected"];
	}
}

@end
