//
//  SWHomeTrendingIllnessRecordView.h
//  Sickweather
//
//  Created by John Erck on 7/13/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWHomeTrendingIllnessRecordView : UIView
- (void)updateForIllnessInfoDict:(NSDictionary *)illnessInfoDict;
@end
