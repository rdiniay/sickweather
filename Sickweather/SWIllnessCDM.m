//
//  SWIllnessCDM.m
//  Sickweather
//
//  Created by John Erck on 2/10/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "SWIllnessCDM.h"
#import "SWReportCDM.h"


@implementation SWIllnessCDM

@dynamic illnessDescription;
@dynamic isGroupIllness;
@dynamic name;
@dynamic selfReportText;
@dynamic unique;
@dynamic sortOrder;
@dynamic report;

@end
