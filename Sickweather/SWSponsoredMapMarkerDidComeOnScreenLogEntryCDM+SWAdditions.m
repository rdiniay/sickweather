//
//  SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 12/16/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM+SWAdditions.h"

@implementation SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM (SWAdditions)

+ (SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM *)sponsoredMapMarkerDidComeOnScreenLogEntryInsertNewUsingSponsoredMarkerAnnotation:(SWSponsoredMarkerMapKitAnnotation *)sponsoredMarkerAnnotation
{
    // Define return var
    SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM *record = nil;
    
    // Create new
    record = [NSEntityDescription insertNewObjectForEntityForName:@"SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
    record.sponsoredMapMarkerId = sponsoredMarkerAnnotation.identifier;
    record.dateSeen = [NSDate date];
    
    // Return object
    return record;
}

@end
