//
//  SWFamilyMemberCollectionViewCell.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 01/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyMemberCollectionViewCell.h"
#import "SWEventsTableViewController.h"
#import "SWFamilyMemberModal.h"
#import "UIImageView+SDLoadImage.h"

@interface SWFamilyMemberCollectionViewCell()
@property (weak, nonatomic) SWFamilyMemberModal *memberData;
@property BOOL isCurrentUser;

@property (weak, nonatomic) IBOutlet UIView *youBackgroundCircleView;
@property (weak, nonatomic) IBOutlet UIImageView *youImageView;
@property (weak, nonatomic) IBOutlet UILabel *youLabel;

@end

@implementation SWFamilyMemberCollectionViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
}

-(void)initWithData:(SWFamilyMemberModal*)memberData isCurrentUser:(BOOL)isCurrentUser // make data parameters
{
    // local variables
    self.memberData = memberData;
    self.isCurrentUser = isCurrentUser;
    
    // youBackgroundCircleView
    self.youBackgroundCircleView.backgroundColor = [UIColor whiteColor];
    self.youBackgroundCircleView.layer.cornerRadius = self.youBackgroundCircleView.frame.size.width / 2;
    self.youBackgroundCircleView.layer.borderWidth = 1;
    self.youBackgroundCircleView.layer.borderColor = [SWColor colorSickweatherLightGray204x204x204].CGColor;
    
    // youImageView
    UIImage *youImage = [UIImage imageNamed:@"avatar-empty"];
    if (self.isCurrentUser) {
        SWUserCDM *user = [SWHelper helperCurrentUser];
        if (user.profilePicThumbnailImageData)
        {
            youImage = [UIImage imageWithData:user.profilePicThumbnailImageData];
        }
        self.youImageView.image = youImage;
    } else {
        [self.youImageView sdLoadImageWith:self.memberData.photo_original placeholder:youImage];
    }
    self.youImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.youImageView.layer.cornerRadius = self.youImageView.frame.size.width / 2;
    self.youImageView.clipsToBounds = YES;
    
    
    // youLabel
    NSString* nameString = @"";
    if (self.isCurrentUser) {
        nameString = @"You";
    } else {
        nameString = self.memberData.name_first;
    }
    if ([self.memberData.confirmed isEqualToString:@"1"] || self.isCurrentUser) {
        self.youLabel.attributedText = [[NSAttributedString alloc] initWithString:nameString ? nameString : @"" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
    }else{
        if (nameString != nil && ![nameString isEqual:[NSNull null]]) {
            NSString *tempNameString = [NSString stringWithFormat:@"%@\n(Pending)",nameString];
            NSMutableAttributedString *attributedConditionsString = [[NSMutableAttributedString alloc] initWithString:tempNameString];
            [attributedConditionsString addAttributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]} range:[tempNameString rangeOfString:nameString]];
            [attributedConditionsString addAttributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:15]} range:[tempNameString rangeOfString:@"\n(Pending)"]];
            self.youLabel.attributedText = attributedConditionsString;
        }
    }
}

@end
