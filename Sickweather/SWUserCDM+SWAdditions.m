//
//  SWUserCDM+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWUserCDM+SWAdditions.h"

@implementation SWUserCDM (SWAdditions)

#pragma mark - Helper Methods

+ (NSArray *)getAllUsers
{
    // Create and config database request
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWUserCDM"];
    
    // Execute fetch request
    NSError *error = nil;
    NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
    return matches;
}

#pragma mark - Public Methods

#pragma mark - Public Class Methods

+ (SWUserCDM *)createOrGetAsUpdatedLoggedInUserUsingUserId:(NSString *)sickweatherUserId
                                                 emailHash:(NSString *)emailHash
                                              passwordHash:(NSString *)passwordHash
                                                 firstName:(NSString *)firstName
{
    // Well, for starters, get the currentUser!
    SWUserCDM *currentUser = [SWUserCDM currentUser];
    
    // Check and see if current currentUser matches the incoming "log me in" SW ID...
    if ([currentUser.unique isEqualToString:sickweatherUserId])
    {
        // Well then, update the user and return it!
        currentUser.emailHash = emailHash;
        currentUser.passwordHash = passwordHash;
        currentUser.firstName = firstName;
        currentUser.userCurrentUserKey = kSWCoreDataIsCurrentUserKeyYes;
    }
    else
    {
        // Check
        if (!currentUser.unique)
        {
            // Well then, the current user we have is not owned yet so it must be
            // assumed that the user is transitioning from a "non network connected user"
            // to one with a sickweather user id from the server...
            currentUser.unique = sickweatherUserId; // Give it a unique!
            currentUser.emailHash = emailHash;
            currentUser.passwordHash = passwordHash;
            currentUser.firstName = firstName;
            currentUser.userCurrentUserKey = kSWCoreDataIsCurrentUserKeyYes;
        }
        else
        {
            // Set as not current, must have been owned by another sickweather user id!
            currentUser.userCurrentUserKey = kSWCoreDataIsCurrentUserKeyNo;
            
            // Create new and set as current
            currentUser = [NSEntityDescription insertNewObjectForEntityForName:@"SWUserCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
            currentUser.unique = sickweatherUserId;
            currentUser.emailHash = emailHash;
            currentUser.passwordHash = passwordHash;
            currentUser.firstName = firstName;
            currentUser.userCurrentUserKey = kSWCoreDataIsCurrentUserKeyYes;
        }
    }
    
    // Save id as currently logged in user
    
    // Used for when we want to know whether or not we have a logged in user even before our managed
    // object context is loaded (like on app open since managed context open takes time and is async)
    [[NSUserDefaults standardUserDefaults] setObject:currentUser.unique forKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Return the current user...
    return currentUser;
}

+ (SWUserCDM *)currentlyLoggedInUser
{
    // Define return var
    SWUserCDM *currentUser = nil;
    
    // Guard w/ context check
    if ([SWHelper helperManagedObjectContext])
    {
        // Get id of currently logged in user
        NSString *currentUserSickweatherId = [[NSUserDefaults standardUserDefaults] stringForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]];
        if ([currentUserSickweatherId length] > 0)
        {
            // Create and config database request
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWUserCDM"];
            request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"unique" ascending:YES]];
            request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", currentUserSickweatherId];
            
            // Execute fetch request
            NSError *error = nil;
            NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
            currentUser = [matches lastObject]; // Just always go for the last object, there should only be one
        }
    }
    
    // Return user
    return currentUser;
}

+ (SWUserCDM *)currentUser
{
    // Define return var
    SWUserCDM *user = nil;
    
    // Guard w/ context check
    if ([SWHelper helperManagedObjectContext])
    {
        //NSLog(@"Total user count: %lu", (unsigned long)[SWUserCDM getAllUsers].count);
        
        // Check for existing logged in user object...
        user = [SWUserCDM currentlyLoggedInUser];
        if (!user)
        {
            // Check for existing "current user"
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWUserCDM"];
            request.predicate = [NSPredicate predicateWithFormat:@"userCurrentUserKey = %@", kSWCoreDataIsCurrentUserKeyYes];
            NSError *error = nil;
            NSArray *matches = [[SWHelper helperManagedObjectContext] executeFetchRequest:request error:&error];
            if (matches.count == 0)
            {
                //NSLog(@"No current user on file...");
            }
            else if (matches.count == 1)
            {
                //NSLog(@"Ok cool, we have exactly one current user, this makes sense...");
            }
            else
            {
                //NSLog(@"Uh oh, we have more than one current user...");
            }
            user = [matches lastObject];
            if (!user)
            {
                // Create new "current user"
                user = [NSEntityDescription insertNewObjectForEntityForName:@"SWUserCDM" inManagedObjectContext:[SWHelper helperManagedObjectContext]];
                user.userCurrentUserKey = kSWCoreDataIsCurrentUserKeyYes;
            }
        }
    }
    
    // Return user
    return user;
}

+ (SWUserCDM *)logUserOut
{
    SWUserCDM *objectToDelete = [SWUserCDM currentUser]; // This of course must come before setting to @"" (next line)
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]];
	[SWHelper setFamilyId:@""];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSSet *iterItems = [NSSet setWithSet:objectToDelete.reports];
    for (NSManagedObject *obj in iterItems)
    {
        [[SWHelper helperManagedObjectContext] deleteObject:obj];
        //NSError *saveError = nil;
        //[[SWHelper helperManagedObjectContext] save:&saveError];
        //NSLog(@"saveError = %@", saveError);
    }
    [[SWHelper helperManagedObjectContext] deleteObject:objectToDelete];
    NSError *saveError = nil;
    [[SWHelper helperManagedObjectContext] save:&saveError];
    //NSLog(@"saveError = %@", saveError);
    return [SWUserCDM currentUser];
}

#pragma mark - Public Instance Methods

- (NSDate *)birthDateAsDate
{
    // The server serves up dates like so: 1985-09-02
    NSDateFormatter *serverDateFormat = [[NSDateFormatter alloc] init];
    [serverDateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [serverDateFormat dateFromString:self.birthDate];
    return date;
}

- (NSString *)fullName
{
    return [[NSString alloc] initWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (NSString *)genderForUI
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].gender;
    if (answer)
    {
        if ([answer isEqualToString:@"1"])
        {
            answer = @"Female";
        }
        if ([answer isEqualToString:@"2"])
        {
            answer = @"Male";
        }
        if ([answer isEqualToString:@"3"])
        {
            answer = @"Other";
        }
        if ([answer isEqualToString:@"4"])
        {
            answer = @"Not Specified";
        }
    }
    return answer;
}

- (NSString *)raceEthnicityForUI
{
    NSString *answer = [SWUserCDM currentlyLoggedInUser].raceEthnicity;
    if (answer)
    {
        if ([answer isEqualToString:@"0"])
        {
            answer = @"Not Specified";
        }
        if ([answer isEqualToString:@"1"])
        {
            answer = @"American Indian / Alaskan Native";
        }
        if ([answer isEqualToString:@"2"])
        {
            answer = @"Asian";
        }
        if ([answer isEqualToString:@"3"])
        {
            answer = @"Black or African American";
        }
        if ([answer isEqualToString:@"4"])
        {
            answer = @"Hispanic or Latino";
        }
        if ([answer isEqualToString:@"5"])
        {
            answer = @"Native Hawaiian or Other Pacific Islander";
        }
        if ([answer isEqualToString:@"6"])
        {
            answer = @"White";
        }
    }
    else
    {
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)birthDateForUI
{
    NSDateFormatter *uiDateFormat = [[NSDateFormatter alloc] init];
    [uiDateFormat setDateFormat:@"MMMM d, yyyy"];
    return [uiDateFormat stringFromDate:[self birthDateAsDate]];
}

-(NSInteger) ageForUI
{
	NSDate* now = [NSDate date];
	NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
									   components:NSCalendarUnitYear
									   fromDate:[self birthDateAsDate]
									   toDate:now
									   options:0];
	return [ageComponents year];
}

- (NSString *)healthCarePrefForUI
{
    //Western/Conventional=1, Holistic/Alternative=2, Both=3, No Preference=0
    NSString *answer = self.medPref;
    if ([answer isEqualToString:@"0"])
    {
        return @"No Preference";
    }
    else if ([answer isEqualToString:@"1"])
    {
        return @"Western/Conventional";
    }
    else if ([answer isEqualToString:@"2"])
    {
        return @"Holistic/Alternative";
    }
    else if ([answer isEqualToString:@"3"])
    {
        return @"Both (Western & Holistic)";
    }
    else
    {
        // Well den, set it!
        self.medPref = @"0";
        return @"No Preference";
    }
}

-(NSMutableDictionary*)userProfileDataForSnow{
	NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
	NSMutableDictionary * dic = [[NSMutableDictionary alloc] init];
	if (self.birthDate != nil && self.birthDate != NULL) {
		NSInteger age = [self ageForUI];
		if (age > 0) {
			[params setObject:[NSString stringWithFormat: @"%ld", (long)age] forKey:@"age"];
		}
	}
	
	if (self.medPref != nil && self.medPref != NULL){
		[params setObject:[self healthCarePrefForUI] forKey:@"med_pref"];
	}
	
	if (self.gender != nil && self.gender != NULL){
		[params setObject:[self genderForUI] forKey:@"gender"];
	}

	if (self.raceEthnicity != nil && self.raceEthnicity != NULL){
		[params setObject:[self raceEthnicityForUI] forKey:@"ethnicity"];
	}
	[dic setObject:params forKey:@"params"];
	return dic;
}


- (NSString *)userLocationForUI
{
    return [SWHelper helperUserLocationCityStateCountryFormattedStringForUICity:self.city state:self.state country:self.country];
}

- (NSString *)birthDateDayForServer
{
    NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
    [outputDateFormat setDateFormat:@"dd"];
    return [outputDateFormat stringFromDate:[self birthDateAsDate]];
}

- (NSString *)birthDateMonthForServer
{
    NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
    [outputDateFormat setDateFormat:@"MM"];
    return [outputDateFormat stringFromDate:[self birthDateAsDate]];
}

- (NSString *)birthDateYearForServer
{
    NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
    [outputDateFormat setDateFormat:@"yyyy"];
    return [outputDateFormat stringFromDate:[self birthDateAsDate]];
}

- (NSString *)formattedAddress
{
    NSString *formattedAddress = @"";
    if ([self.country length] > 0 && [self.state length] > 0 && [self.city length] > 0)
    {
        formattedAddress = [NSString stringWithFormat:@"%@, %@ %@", self.city, self.state, self.country];
    }
    else if ([self.country length] > 0 && [self.state length] > 0)
    {
        formattedAddress = [NSString stringWithFormat:@"%@ %@", self.state, self.country];
    }
    else if ([self.country length] > 0)
    {
        formattedAddress = [NSString stringWithFormat:@"%@", self.country];
    }
    return formattedAddress;
}

- (CLPlacemark *)userLastKnownCurrentLocationPlacemarkObject
{
    CLPlacemark *placemark = nil;
    if (self.userLastKnownCurrentLocationPlacemark)
    {
        placemark = [NSKeyedUnarchiver unarchiveObjectWithData:self.userLastKnownCurrentLocationPlacemark];
    }
    return placemark;
}

- (void)setProfilePicThumbnailImageDataUsingImage:(UIImage *)image
{
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    self.profilePicThumbnailImageData = imageData;
}

@end
