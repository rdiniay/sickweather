//
//  SWMyReportsViewController.h
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWMyReportsViewControllerDelegate <NSObject>
- (void)myReportsViewControllerWantsToDismiss;
@end

@interface SWMyReportsViewController : UIViewController
@property (nonatomic, assign) id <SWMyReportsViewControllerDelegate> delegate;
@end
