//
//  SWCreateSickweatherAccountViewController.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

@import SWAnalytics;
#import "Flurry.h"
#import "SWCreateSickweatherAccountViewController.h"
#import "SWAppDelegate.h"
#import "SWUserCDM+SWAdditions.h"

@interface SWCreateSickweatherAccountViewController () <UITextFieldDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *topContainer;
@property (strong, nonatomic) UIView *formContainer;
@property (strong, nonatomic) UITextField *activeField;
@property (strong, nonatomic) UITextField *firstNameTextField;
@property (strong, nonatomic) UITextField *lastNameTextField;
@property (strong, nonatomic) UITextField *emailTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UITextField *passwordAgainTextField;
@property (strong, nonatomic) UIButton *submitButton;
@property (strong, nonatomic) UIButton *signInWithDifferentAccountButton;
@property (strong, nonatomic) UIView *backgroundImageView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIButton *dismissButton;
@property (assign) BOOL keyboardIsComingUp;
@property (strong, nonatomic) NSLayoutConstraint *topContainerHeightConstraint;
@property (strong, nonatomic) UITextField *validationTargetTextField;
@end

@implementation SWCreateSickweatherAccountViewController

#pragma mark - UIScrollViewDelegate

static CGFloat lastContentY = 0;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //NSLog(@"fabsf(scrollView.contentOffset.y = %f) - fabsf(lastContentY = %f) = %f", scrollView.contentOffset.y, lastContentY, fabs(scrollView.contentOffset.y) - fabs(lastContentY));
    //NSLog(@"%f - %f = %f", fabs(scrollView.contentOffset.y), fabs(lastContentY), fabs(scrollView.contentOffset.y) - fabs(lastContentY));
    if (fabs(scrollView.contentOffset.y) - fabs(lastContentY) > 40 || fabs(scrollView.contentOffset.y) - fabs(lastContentY) < -40)
    {
        if (!self.keyboardIsComingUp)
        {
            [self.activeField resignFirstResponder];
            lastContentY = scrollView.contentOffset.y;
        }
    }
}

#pragma mark - Target/Action

- (void)submitButtonTapped:(id)sender
{
    if ([self.firstNameTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.firstNameTextField;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing First Name" message:@"Please enter your first name" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.validationTargetTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([self.lastNameTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.lastNameTextField;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Last Name" message:@"Please enter your last name" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.validationTargetTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([self.emailTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.emailTextField;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Email" message:@"Please enter your email address" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.validationTargetTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([self.passwordTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.passwordTextField;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Password" message:@"Please enter the password you'd like to use" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.validationTargetTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([self.passwordAgainTextField.text isEqualToString:@""])
    {
        self.validationTargetTextField = self.passwordAgainTextField;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Password Confirmation" message:@"Please confirm your password by entering it a 2nd time" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.validationTargetTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (![self.passwordAgainTextField.text isEqualToString:self.passwordTextField.text])
    {
        self.validationTargetTextField = self.passwordTextField;
        self.passwordTextField.text = @"";
        self.passwordAgainTextField.text = @"";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Password Mismatch" message:@"Your confirmation password did not match your password" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.validationTargetTextField becomeFirstResponder];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self.activityIndicator startAnimating];
        [[SWHelper helperAppBackend] createSickweatherAccountUsingFirstName:self.firstNameTextField.text lastName:self.lastNameTextField.text email:self.emailTextField.text password:self.passwordTextField.text usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicator stopAnimating];
                if ([[jsonResponseBody objectForKey:@"join_status"] isEqualToString:@"success"])
                {
                   SWUserCDM *loggedInUser = [SWUserCDM createOrGetAsUpdatedLoggedInUserUsingUserId:[jsonResponseBody objectForKey:@"user_id"] emailHash:[jsonResponseBody objectForKey:@"email"] passwordHash:[jsonResponseBody objectForKey:@"password"] firstName:[jsonResponseBody objectForKey:@"name_first"]];
                    if (loggedInUser)
                    {
						NSString *currentUserSWId = [SWHelper helperGetCurrentUserSWIdFromDefaults];
						if ([currentUserSWId length] > 0){
							[SWAnalytics setUserWithUserId:currentUserSWId];
						}
						// Success!
						[Flurry logEvent:@"Create Account 2"];
						[self getFamilyMemberIdFromBackend];
						[self.delegate createSickweatherAccountViewControllerDidDismissWithSuccessfulLogin:self];
                    }
                    else
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please try again." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                        [SWHelper helperLogErrorToAppBackendForJSONResponseBody:jsonResponseBody responseBodyAsString:responseBodyAsString responseHeaders:responseHeaders requestError:requestError jsonParseError:jsonParseError requestURLAbsoluteString:requestURLAbsoluteString file:__FILE__ prettyFunction:__PRETTY_FUNCTION__ line:__LINE__];
                    }
                }
                else
                {
                    if ([jsonResponseBody objectForKey:@"errorText"])
                    {
                        NSString *errorText = [jsonResponseBody objectForKey:@"errorText"];
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:errorText preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please try again." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                        [SWHelper helperLogErrorToAppBackendForJSONResponseBody:jsonResponseBody responseBodyAsString:responseBodyAsString responseHeaders:responseHeaders requestError:requestError jsonParseError:jsonParseError requestURLAbsoluteString:requestURLAbsoluteString file:__FILE__ prettyFunction:__PRETTY_FUNCTION__ line:__LINE__];
                    }
                }
            });
        }];
    }
}

- (void)dismissSelf:(id)sender
{
    [Flurry logEvent:@"Back To Sign In"];
    [self.delegate createSickweatherAccountViewControllerDidDismissWithoutLoggingIn:self];
}

-(void)getFamilyMemberIdFromBackend{
	if ([SWUserCDM currentlyLoggedInUser]) {
		[[SWHelper helperAppBackend]appBackendGetFamilyIdUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
			if (jsonResponseBody){
				if ([jsonResponseBody valueForKey:@"family_id"] != nil &&  ![[jsonResponseBody valueForKey:@"family_id"] isEqual:[NSNull null]] && ![[jsonResponseBody valueForKey:@"family_id"] isEqualToString:@""]){
					[SWHelper setFamilyId:[jsonResponseBody valueForKey:@"family_id"]];
				}
			}
		}];
	}
}

#pragma mark - Keyboard Management Methods

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    self.keyboardIsComingUp = YES;
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    // Calculate the frame of the scrollview, in self.view's coordinate system
    UIScrollView *scrollView    = self.scrollView;
    CGRect scrollViewRect       = [self.view convertRect:scrollView.frame fromView:scrollView.superview];
    
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    
    // Figure out where the two frames overlap, and set the content offset of the scrollview appropriately
    CGRect hiddenScrollViewRect = CGRectIntersection(scrollViewRect, kbRect);
    if (!CGRectIsNull(hiddenScrollViewRect))
    {
        UIEdgeInsets contentInsets       = UIEdgeInsetsMake(0.0,  0.0, hiddenScrollViewRect.size.height,  0.0);
        scrollView.contentInset          = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }
    [self performSelector:@selector(scrollToActiveTextField) withObject:nil afterDelay:0.1]; // Delay is required for iOS to handle things as expected
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.scrollView.contentInset          = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)scrollToActiveTextField
{
    if (self.activeField)
    {
        CGRect visibleRect = self.activeField.frame;
        visibleRect        = [self.scrollView convertRect:visibleRect fromView:self.activeField.superview];
        visibleRect        = CGRectInset(visibleRect, 0.0f, -60.0f);
        [self.scrollView scrollRectToVisible:visibleRect animated:YES];
    }
}

// Register for notifications helper
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if ([textField isEqual:self.firstNameTextField])
    {
        [self.firstNameTextField resignFirstResponder];
        [self.lastNameTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.lastNameTextField])
    {
        [self.lastNameTextField resignFirstResponder];
        [self.emailTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.emailTextField])
    {
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.passwordTextField])
    {
        [self.passwordTextField resignFirstResponder];
        [self.passwordAgainTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.passwordAgainTextField])
    {
        [self.passwordAgainTextField resignFirstResponder];
        [self submitButtonTapped:self];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
}

#pragma mark - Lifecycle Methods

// Support only portrait
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // This code enables us to design our purple formContainer to be any size we need it to be, and the view above gets
    // its height based on device screen height and how much space we ate up using formContainer (which "pushes out" on
    // its edges)...
    
    //NSLog(@"self.formContainer.frame = %@", NSStringFromCGRect(self.formContainer.frame));
    if (self.formContainer.frame.size.height > 0)
    {
        CGFloat minTopSpaceWeWillGoTo = 170;
        CGFloat availableTopSpace = [UIScreen mainScreen].bounds.size.height - self.formContainer.frame.size.height;
        if (availableTopSpace < minTopSpaceWeWillGoTo)
        {
            self.topContainerHeightConstraint.constant = minTopSpaceWeWillGoTo;
        }
        else
        {
            self.topContainerHeightConstraint.constant = availableTopSpace;
        }
        // Delay is again needed in order to get our new contentSize to go into effect
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view setNeedsUpdateConstraints];
        });
    }
    self.scrollView.contentSize = self.contentView.frame.size;
}

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.topContainer = [[UIView alloc] init];
    self.formContainer = [[UIView alloc] init];
    self.dismissButton = [[UIButton alloc] init];
    self.firstNameTextField = [[UITextField alloc] init];
    self.lastNameTextField = [[UITextField alloc] init];
    self.emailTextField = [[UITextField alloc] init];
    self.passwordTextField = [[UITextField alloc] init];
    self.passwordAgainTextField = [[UITextField alloc] init];
    self.submitButton = [[UIButton alloc] init];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cloud-sickweather-going-around"]];
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_scrollView, _contentView, _topContainer, _formContainer, _dismissButton, _firstNameTextField, _lastNameTextField, _emailTextField, _passwordTextField, _passwordAgainTextField, _submitButton, _backgroundImageView, _activityIndicator);
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.topContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.formContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.firstNameTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.lastNameTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.passwordTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.passwordAgainTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.submitButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView]; // View adds scroll view
    [self.scrollView addSubview:self.contentView]; // Scroll view adds content view
    [self.contentView addSubview:self.topContainer]; // Content view adds top container
    [self.contentView addSubview:self.formContainer]; // Content view adds form container
    [self.topContainer addSubview:self.backgroundImageView]; // Top container adds image view
    [self.topContainer addSubview:self.dismissButton];
    [self.topContainer addSubview:self.activityIndicator];
    [self.formContainer addSubview:self.firstNameTextField]; // Form container holds everything else
    [self.formContainer addSubview:self.lastNameTextField];
    [self.formContainer addSubview:self.emailTextField];
    [self.formContainer addSubview:self.passwordTextField];
    [self.formContainer addSubview:self.passwordAgainTextField];
    [self.formContainer addSubview:self.submitButton];
    
    // COLOR VIEWS
    /*
    self.view.backgroundColor = [UIColor redColor];
    self.scrollView.backgroundColor = [UIColor yellowColor];
    self.contentView.backgroundColor = [UIColor greenColor];
    self.topContainer.backgroundColor = [UIColor grayColor];
    self.activityIndicator.backgroundColor = [UIColor brownColor];
    self.formContainer.backgroundColor = [UIColor purpleColor];
    self.dismissButton.backgroundColor = [UIColor orangeColor];
    */
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_contentView]" options:0 metrics:0 views:viewsDictionary]];
    // Still laying out CONTENT VIEW (pin to self.view left and right, let height be based on what we put inside)
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:0
                                                                         toItem:self.view
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1.0
                                                                       constant:0];
    [self.view addConstraint:leftConstraint];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:0
                                                                          toItem:self.view
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1.0
                                                                        constant:0];
    [self.view addConstraint:rightConstraint];
    
    // Layout TOP CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_topContainer]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_topContainer]" options:0 metrics:0 views:viewsDictionary]];
    self.topContainerHeightConstraint = [NSLayoutConstraint constraintWithItem:self.topContainer
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:0
                                                                     attribute:0
                                                                    multiplier:1
                                                                      constant:0]; // We won't know this value until viewDidLayoutSubviews
    [self.view addConstraint:self.topContainerHeightConstraint];
    
    // Layout FORM CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_formContainer]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_topContainer][_formContainer]|" options:0 metrics:0 views:viewsDictionary]]; // <-- Anchor PUSH OUT constraint happens here
    
    // Layout TOP CONTAINER ITEMS
    
    // Layout DISMISS BUTTON
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:15]];
    
    // Layout IMAGE VIEW
    NSLayoutConstraint *backgroundImageViewCenterX = [NSLayoutConstraint constraintWithItem:self.backgroundImageView
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:0
                                                                  toItem:self.topContainer
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1.0
                                                                constant:0];
    [self.view addConstraint:backgroundImageViewCenterX];
    NSLayoutConstraint *backgroundImageViewCenterY = [NSLayoutConstraint constraintWithItem:self.backgroundImageView
                                                                                  attribute:NSLayoutAttributeCenterY
                                                                                  relatedBy:0
                                                                                     toItem:self.topContainer
                                                                                  attribute:NSLayoutAttributeCenterY
                                                                                 multiplier:1.0
                                                                                   constant:0];
    [self.view addConstraint:backgroundImageViewCenterY];
    
    // Layout ACTIVITY INDICATOR
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.activityIndicator
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.topContainer
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1.0
                                                                constant:0];
    [self.view addConstraint:centerX];
    NSLayoutConstraint *topY = [NSLayoutConstraint constraintWithItem:self.activityIndicator
                                                            attribute:NSLayoutAttributeTop
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self.backgroundImageView
                                                            attribute:NSLayoutAttributeBottom
                                                           multiplier:1.0
                                                             constant:20];
    [self.view addConstraint:topY];
    
    // Layout FORM CONTAINER ITEMS
    
    // Layout FIRST NAME TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_firstNameTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(15)-[_firstNameTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout LAST NAME TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_lastNameTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_firstNameTextField]-(10)-[_lastNameTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout EMAIL TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_emailTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_lastNameTextField]-(10)-[_emailTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout PASSWORD TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_passwordTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_emailTextField]-(10)-[_passwordTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout PASSWORD AGAIN TEXT FIELD
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_passwordAgainTextField]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_passwordTextField]-(10)-[_passwordAgainTextField(40)]" options:0 metrics:0 views:viewsDictionary]];
    
    // Layout SIGN IN WITH SICKWEATHER BUTTON
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[_submitButton]-(15)-|" options:0 metrics:0 views:viewsDictionary]];
    [self.formContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_passwordAgainTextField]-(10)-[_submitButton(40)]-(60)-|" options:0 metrics:0 views:viewsDictionary]]; // <-- Anchor PUSH OUT constraint happens here
    
    // CONFIG ALL VIEWS
    
    // Config VIEW
    
    // Add tap (for dismissing keyboard)
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = NO;
    
    // Config IMAGE VIEW
    self.backgroundImageView.contentMode = UIViewContentModeCenter;
    
    // Config FIRST NAME TEXT FIELD
    self.firstNameTextField.delegate = self;
    self.firstNameTextField.placeholder = @"First Name";
    self.firstNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.firstNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.firstNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.firstNameTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.firstNameTextField.enablesReturnKeyAutomatically = YES;
    self.firstNameTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.firstNameTextField.keyboardType = UIKeyboardTypeAlphabet;
    self.firstNameTextField.returnKeyType = UIReturnKeyNext;
    self.firstNameTextField.secureTextEntry = NO;
    self.firstNameTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.firstNameTextField.layer.borderWidth = 0.5;
    self.firstNameTextField.layer.cornerRadius = 5.0;
    self.firstNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config LAST NAME TEXT FIELD
    self.lastNameTextField.delegate = self;
    self.lastNameTextField.placeholder = @"Last Name";
    self.lastNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.lastNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.lastNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.lastNameTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.lastNameTextField.enablesReturnKeyAutomatically = YES;
    self.lastNameTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.lastNameTextField.keyboardType = UIKeyboardTypeAlphabet;
    self.lastNameTextField.returnKeyType = UIReturnKeyNext;
    self.lastNameTextField.secureTextEntry = NO;
    self.lastNameTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.lastNameTextField.layer.borderWidth = 0.5;
    self.lastNameTextField.layer.cornerRadius = 5.0;
    self.lastNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config EMAIL TEXT FIELD
    self.emailTextField.delegate = self;
    self.emailTextField.placeholder = @"Email";
    self.emailTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.emailTextField.enablesReturnKeyAutomatically = YES;
    self.emailTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.returnKeyType = UIReturnKeyNext;
    self.emailTextField.secureTextEntry = NO;
    self.emailTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.emailTextField.layer.borderWidth = 0.5;
    self.emailTextField.layer.cornerRadius = 5.0;
    self.emailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config PASSWORD TEXT FIELD
    self.passwordTextField.delegate = self;
    self.passwordTextField.placeholder = @"Password";
    self.passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.passwordTextField.enablesReturnKeyAutomatically = YES;
    self.passwordTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.passwordTextField.keyboardType = UIKeyboardTypeDefault;
    self.passwordTextField.returnKeyType = UIReturnKeyGo;
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.passwordTextField.layer.borderWidth = 0.5;
    self.passwordTextField.layer.cornerRadius = 5.0;
    self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config PASSWORD AGAIN TEXT FIELD
    self.passwordAgainTextField.delegate = self;
    self.passwordAgainTextField.placeholder = @"Re-enter Password";
    self.passwordAgainTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.passwordAgainTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordAgainTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordAgainTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    self.passwordAgainTextField.enablesReturnKeyAutomatically = YES;
    self.passwordAgainTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.passwordAgainTextField.keyboardType = UIKeyboardTypeDefault;
    self.passwordAgainTextField.returnKeyType = UIReturnKeyGo;
    self.passwordAgainTextField.secureTextEntry = YES;
    self.passwordAgainTextField.layer.borderColor = [SWColor colorSickweatherDarkGrayText104x104x104].CGColor;
    self.passwordAgainTextField.layer.borderWidth = 0.5;
    self.passwordAgainTextField.layer.cornerRadius = 5.0;
    self.passwordAgainTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // Config CREATE SICKWEATHER ACCOUNT BUTTON
    [self.submitButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"CREATE ACCOUNT" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:13]}] forState:UIControlStateNormal];
    [self.submitButton setBackgroundColor:[SWColor colorSickweatherBlue41x171x226]];
    [self.submitButton.layer setBorderColor:[SWColor colorSickweatherBlue41x171x226].CGColor];
    [self.submitButton.layer setBorderWidth:0.5];
    [self.submitButton.layer setCornerRadius:2];
    [self.submitButton addTarget:self action:@selector(submitButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config DISMISS BUTTON
    [self.dismissButton setImage:[UIImage imageNamed:@"close-icon"] forState:UIControlStateNormal];
    [self.dismissButton addTarget:self action:@selector(dismissSelf:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config KEYBOARD NOTIFICATIONS
    // https://developer.apple.com/library/ios/documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html
    [self registerForKeyboardNotifications];
    
    // Config ACTIVITY INDICATOR
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    //[self.activityIndicator startAnimating]; // For design time debugging...
}

@end
