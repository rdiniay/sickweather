//
//  SWSymptomsModal.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWSymptomsModal.h"

@implementation SWSymptomsModal
- (instancetype)init
{
	self = [super init];
	return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if (self) {
		if ([NSNull null] != [dictionary valueForKey:@"id"]) self.id = [dictionary valueForKey:@"id"];
		if ([NSNull null] != [dictionary valueForKey:@"name"]) self.name = [dictionary valueForKey:@"name"];
	}
	return self;
}
@end
