//
//  SWMyProfileHeaderView.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWMyProfileHeaderView.h"

#define PROFILE_PIC_WIDTH_HEIGHT 100

@interface SWMyProfileHeaderView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIView *backgroundImageViewOverlayView;
@property (strong, nonatomic) UIImageView *profilePicImageView;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *usernameLabel;
@end

@implementation SWMyProfileHeaderView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWMyProfileHeaderViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    if ([self.delegate myProfileHeaderViewProfilePicImage:self])
    {
        self.profilePicImageView.image = [self.delegate myProfileHeaderViewProfilePicImage:self];
    }
    self.nameLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate myProfileHeaderViewNameString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
    self.usernameLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate myProfileHeaderViewUsernameString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
}

#pragma mark - Public Methods

- (void)setProfilePicImage:(UIImage *)image
{
    self.profilePicImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.profilePicImageView.image = image;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.backgroundImageView = [[UIImageView alloc] init];
        self.backgroundImageViewOverlayView = [[UIView alloc] init];
        self.profilePicImageView = [[UIImageView alloc] init];
        self.nameLabel = [[UILabel alloc] init];
        self.usernameLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.backgroundImageView forKey:@"backgroundImageView"];
        [self.viewsDictionary setObject:self.backgroundImageViewOverlayView forKey:@"backgroundImageViewOverlayView"];
        [self.viewsDictionary setObject:self.profilePicImageView forKey:@"profilePicImageView"];
        [self.viewsDictionary setObject:self.nameLabel forKey:@"nameLabel"];
        [self.viewsDictionary setObject:self.usernameLabel forKey:@"usernameLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundImageViewOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
        self.profilePicImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.usernameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.backgroundImageView];
        [self addSubview:self.backgroundImageViewOverlayView];
        [self addSubview:self.profilePicImageView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.usernameLabel];
        
        // LAYOUT
        
        // Layout BACKGROUND IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout BACKGROUND IMAGE VIEW OVERLAY VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout PROFILE PIC IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[profilePicImageView(wh)]"] options:0 metrics:@{@"wh":@PROFILE_PIC_WIDTH_HEIGHT} views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[profilePicImageView(wh)]"] options:0 metrics:@{@"wh":@PROFILE_PIC_WIDTH_HEIGHT} views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.profilePicImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.profilePicImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:0.75
                                                          constant:0.0]];
        
        // Layout NAME LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[nameLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[profilePicImageView]-(10)-[nameLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout USERNAME LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[usernameLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[nameLabel]-(0)-[usernameLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config BACKGROUND IMAGE VIEW
        self.backgroundImageView.image = [UIImage imageNamed:@"map-background.jpg"];
        self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backgroundImageView.clipsToBounds = YES;
        self.backgroundImageView.hidden = YES;
        
        // Config BACKGROUND IMAGE VIEW OVERLAY VIEW
        self.backgroundImageViewOverlayView.backgroundColor = [SWColor color:[SWColor colorSickweatherWhite255x255x255] usingOpacity:0.75];
        self.backgroundImageViewOverlayView.hidden = YES;
        
        // Config PROFILE PIC IMAGE VIEW
        self.profilePicImageView.image = [UIImage animatedImageNamed:@"loading-" duration:0.75];
        self.profilePicImageView.contentMode = UIViewContentModeCenter; // For spinner image
        self.profilePicImageView.clipsToBounds = YES;
        self.profilePicImageView.layer.cornerRadius = PROFILE_PIC_WIDTH_HEIGHT/2.0;
        
        // Config NAME LABEL
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        
        // Config USERNAME LABEL
        self.usernameLabel.textAlignment = NSTextAlignmentCenter;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //        self.backgroundImageView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        //        self.backgroundImageViewOverlayView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
        //        self.profilePicImageView.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //        self.nameLabel.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
        //        self.usernameLabel.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
    }
    return self;
}

@end
