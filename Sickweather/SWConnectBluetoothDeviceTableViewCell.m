//
//  SWConnectBluetoothDeviceTableViewCell.m
//  Sickweather
//
//  Created by Muhammad Javed on 06/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWConnectBluetoothDeviceTableViewCell.h"
#import "SWConnectDeviceViewController.h"

@interface SWConnectBluetoothDeviceTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *deviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *deviceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deviceTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *recommendedBannerImageView;
@property (weak, nonatomic) IBOutlet UIButton *connectDeviceButton;
@property (weak, nonatomic) IBOutlet UIButton *buyItButton;

@end

@implementation SWConnectBluetoothDeviceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.connectDeviceButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Connect Device" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:15]}] forState:UIControlStateNormal];
    self.connectDeviceButton.layer.cornerRadius = 5;
    self.connectDeviceButton.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    
    [self.buyItButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Buy it" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:15]}] forState:UIControlStateNormal];
    self.buyItButton.layer.cornerRadius = 6;
    self.buyItButton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
    self.buyItButton.layer.borderWidth = 2.0;
    
    if ([UIScreen mainScreen].bounds.size.width < 350) {
    }
}

#pragma mark - Setter Methods

- (void)setDeviceTitle:(NSString*)title
{
    self.deviceTitleLabel.text = title;
}

- (void)setDeviceImage:(UIImage*)image
{
    self.deviceImageView.image = image;
}

- (void)setRecommendedBanner:(BOOL)flag
{
    self.recommendedBannerImageView.hidden = !flag;
}

#pragma mark - Action/Target Methods

- (IBAction)connectDeviceButtonPressed:(id)sender {
    [self.delegate connectBluetoothDeviceTableViewCellConnectButtonTapped:self.indexPath];
}

- (IBAction)buyItButtonPressed:(id)sender {
    [self.delegate connectBluetoothDeviceTableViewCellBuyItButtonTapped:self.indexPath];
}

@end
