//
//  SWWelcomeSliderBottomContainerView.swift
//  Sickweather
//
//  Created by John Erck on 7/17/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit

@objc protocol SWWelcomeSliderBottomContainerViewDelegate: class {
    
    func mainButtonTouchUpInside(_ view: SWWelcomeSliderBottomContainerView)
    func altButtonTouchUpInside(_ view: SWWelcomeSliderBottomContainerView)
    
}

@objc class SWWelcomeSliderBottomContainerView: UIView {
    
    weak var delegate: SWWelcomeSliderBottomContainerViewDelegate?
    var titleText = ""
    var bodyText = ""
    var buttonText = ""
    var altButtonText = ""
    var titleLabel = UILabel()
    var bodyLabel = UILabel()
    var mainButton = UIButton()
    var altButton = UIButton()
    var titleLabelTopSpacing = NSLayoutConstraint()
    var titleBodySpacing = NSLayoutConstraint()
    var mainButtonBottomSpacing = NSLayoutConstraint()
    var mainButtonHeight = NSLayoutConstraint()
    var altButtonTopSpacing = NSLayoutConstraint()
    
    @objc func mainButtonTouchUpInside() {
        delegate?.mainButtonTouchUpInside(self)
    }
    
    @objc func altButtonTouchUpInside() {
        delegate?.altButtonTouchUpInside(self)
    }
    
    func updateForCurrentTraitCollection() {
        
        /* DEBUG SECTION SO YOU CAN LOG CURRENT VALUES */
        
        let printCurrent = true;
        if (printCurrent && SWHelper.helperIsIOS8OrHigher())
        {
            
        }
        
        /* DEEFINE ALL OUR DEFAULT VALUES (ANY/ANY) */
        
        // Note, defaults are designed around ***iPhone 6 Plus***
        var titleLabelFontSize: CGFloat = 22
        var bodyLabelFontSize: CGFloat = 17
        var mainButtonBackgroundColor = SWColor.colorSickweatherWhite255x255x255()
        var mainButtonTextColor =  SWColor.colorSickweatherStyleGuideBlue37x169x224() //UIColor.init(colorLiteralRed: 37/255.0, green: 169/255.0, blue: 224/255.0, alpha: 1.0)
        let mainButtonFontSize: CGFloat = 17
        var altButtonFontSize: CGFloat = 17
        titleLabelTopSpacing.constant = 20
        titleBodySpacing.constant = 20
        if altButtonText.count > 0 {
            mainButtonBackgroundColor = SWColor.colorSickweatherStyleGuideBlue37x169x224()
            mainButtonTextColor = SWColor.colorSickweatherWhite255x255x255()
            mainButton.layer.borderWidth = 0
        }
        
        /* DEFINE TRAIT SPECIFIC UPDATES PER UI ELEMENT */
        
        // Define debug/switch var for layout testing/development
        let applyTraitSpecificUpdates = true
        
        /* TITLE LABEL */
        if applyTraitSpecificUpdates && SWHelper.helperIsIOS8OrHigher() {
            /*
            // Horizontal Compact
            if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact {
                
            }
            
            // Horizontal Regular
            if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
                
            }
            
            // Vertical Compact
            if traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.Compact {
                
            }
            
            // Vertical Regular
            if traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.Regular {
                
            }
            */
        }
        
        /* BODY LABEL */
        if applyTraitSpecificUpdates && SWHelper.helperIsIOS8OrHigher() {
            
        }
        
        /* MAIN BUTTON */
        if applyTraitSpecificUpdates && SWHelper.helperIsIOS8OrHigher() {
            
        }
        
        /* ALT BUTTON */
        if applyTraitSpecificUpdates && SWHelper.helperIsIOS8OrHigher() {
            
        }
        
        /* DEFINE DEVICE SPECIFIC UPDATES PER UI ELEMENT */
        
        //println("UIScreen.mainScreen().bounds.size.height = \(UIScreen.mainScreen().bounds.size.height)")
        
        // iPhone 4s
        if UIScreen.main.bounds.size.height == 480 {
            titleLabelFontSize = 18
            bodyLabelFontSize = 13
            titleLabelTopSpacing.constant = 14
            titleBodySpacing.constant = 10
            mainButtonBottomSpacing.constant = -50
            mainButtonHeight.constant = 40
            altButtonTopSpacing.constant = 0
            altButtonFontSize = 14
        }
        
        // iPhone 5, iPhone 5s
        if UIScreen.main.bounds.size.height == 568 {
            titleLabelFontSize = 18
            bodyLabelFontSize = 13
            titleLabelTopSpacing.constant = 14
            titleBodySpacing.constant = 10
            mainButtonBottomSpacing.constant = -60
            mainButtonHeight.constant = 40
            altButtonTopSpacing.constant = 0
            altButtonFontSize = 14
        }
        
        // iPhone 6
        if UIScreen.main.bounds.size.height == 667 {
            // No variations needed
        }
        
        // iPhone 6 Plus
        if UIScreen.main.bounds.size.height == 736 {
            // Defaults are already programmed for this device
        }
        
        // iPad 2, iPad Air, iPad Retina
        if UIScreen.main.bounds.size.height == 1024 {
            titleLabelFontSize = 36
            bodyLabelFontSize = 30
            titleLabelTopSpacing.constant = 30
            titleBodySpacing.constant = 20
        }
        
        /* APPLY ALL UPDATES */
        
        // Attributed strings, etc
        titleLabel.attributedText = NSAttributedString(string: titleText, attributes: [NSAttributedString.Key.foregroundColor: SWColor.colorSickweatherStyleGuideGrayDark35x31x32(), NSAttributedString.Key.font: SWFont.fontStandardBoldFont(ofSize: titleLabelFontSize)])
        bodyLabel.attributedText = NSAttributedString(string: bodyText, attributes: [NSAttributedString.Key.foregroundColor: SWColor.colorSickweatherStyleGuideGray79x76x77(), NSAttributedString.Key.font: SWFont.fontStandardFont(ofSize: bodyLabelFontSize)])
        mainButton.setAttributedTitle(NSAttributedString(string: buttonText, attributes:[NSAttributedString.Key.foregroundColor: mainButtonTextColor, NSAttributedString.Key.font: SWFont.fontStandardBoldFont(ofSize: mainButtonFontSize)]), for: UIControl.State())
        mainButton.setAttributedTitle(NSAttributedString(string: buttonText, attributes: [NSAttributedString.Key.foregroundColor: SWColor.color(mainButtonTextColor, usingOpacity: 0.8), NSAttributedString.Key.font: SWFont.fontStandardBoldFont(ofSize: mainButtonFontSize)]), for: UIControl.State.highlighted)
        mainButton.setBackgroundImage(SWHelper.helperOnePixelImage(using: SWColor.color(mainButtonBackgroundColor, usingOpacity: 1.0)), for: UIControl.State())
        mainButton.setBackgroundImage(SWHelper.helperOnePixelImage(using: SWColor.color(mainButtonBackgroundColor, usingOpacity: 0.8)), for: UIControl.State.highlighted)
        altButton.setAttributedTitle(NSAttributedString(string: altButtonText, attributes: [NSAttributedString.Key.foregroundColor: SWColor.colorSickweatherGrayText135x135x135(), NSAttributedString.Key.font: SWFont.fontStandardFont(ofSize: altButtonFontSize)]), for: UIControl.State())
        altButton.setAttributedTitle(NSAttributedString(string: altButtonText, attributes: [NSAttributedString.Key.foregroundColor: SWColor.color(SWColor.colorSickweatherGrayText135x135x135(), usingOpacity: 0.8), NSAttributedString.Key.font: SWFont.fontStandardFont(ofSize: altButtonFontSize)]), for: UIControl.State.highlighted)
        
        // Constraints
        setNeedsUpdateConstraints()
    }
    
    @available(iOS 8.0, *)
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateForCurrentTraitCollection()
    }
    
    @objc init(titleText : String, bodyText : String, buttonText : String, altButtonText : String, delegate : SWWelcomeSliderBottomContainerViewDelegate, frame : CGRect) {
        
        // Store user provided values
        self.delegate = delegate
        self.titleText = titleText
        self.bodyText = bodyText
        self.buttonText = buttonText
        self.altButtonText = altButtonText
        
        // Invoke super's designated initializer now that we've initialized all our properties
        super.init(frame: frame)
        
        // Create and configure UI
        
        self.backgroundColor = UIColor.white
        
        // titleLabel
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        
        // bodyLabel
        bodyLabel.textAlignment = NSTextAlignment.center
        bodyLabel.numberOfLines = 3
        bodyLabel.translatesAutoresizingMaskIntoConstraints = false
        bodyLabel.sizeToFit()
        addSubview(bodyLabel)
        
        // mainButton
        mainButton.clipsToBounds = true
        mainButton.layer.borderColor = SWColor.colorSickweatherStyleGuideGrayLight133x133x134().cgColor
        mainButton.layer.borderWidth = 0.5
        mainButton.layer.cornerRadius = 3
        mainButton.addTarget(self, action: #selector(mainButtonTouchUpInside), for: UIControl.Event.touchUpInside)
        mainButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(mainButton)
        
        // altButton
        altButton.backgroundColor = SWColor.colorSickweatherWhite255x255x255()
        altButton.addTarget(self, action: #selector(altButtonTouchUpInside), for: UIControl.Event.touchUpInside)
        altButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(altButton)
        if (altButtonText.count == 0) {
            // Hide
            altButton.isUserInteractionEnabled = false
            altButton.layer.opacity = 0
        }
        
        // Layout
        let viewsDictionary = ["titleLabel":titleLabel,"bodyLabel":bodyLabel,"mainButton":mainButton,"altButton":altButton] as [String : Any]
        
        // Sizing constraints
        
        // titleLabel
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[titleLabel]-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary) as [NSLayoutConstraint])
        titleLabelTopSpacing = NSLayoutConstraint(item: titleLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0)
        self.addConstraint(titleLabelTopSpacing)
        
        // bodyLabel
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[bodyLabel]-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary) as [NSLayoutConstraint])
        titleBodySpacing = NSLayoutConstraint(item: bodyLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: titleLabel, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0)
        self.addConstraint(titleBodySpacing)
        
        // mainButton
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[mainButton]-(20)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary) as [NSLayoutConstraint])
        mainButtonBottomSpacing = NSLayoutConstraint(item: mainButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: -80)
        self.addConstraint(mainButtonBottomSpacing)
        mainButtonHeight = NSLayoutConstraint(item: mainButton, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1.0, constant: 50)
        self.addConstraint(mainButtonHeight)
        
        // altButton
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(20)-[altButton]-(20)-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary) as [NSLayoutConstraint])
        altButtonTopSpacing = NSLayoutConstraint(item: altButton, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mainButton, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 12)
        self.addConstraint(altButtonTopSpacing)
        
        // Update
        updateForCurrentTraitCollection()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
