//
//  SWWelcomeView.m
//  Sickweather
//
//  Created by John Erck on 10/9/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWWelcomeView.h"

@implementation SWWelcomeView

- (void)sharedInitializer
{
    // TBD...
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self sharedInitializer];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self sharedInitializer];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
