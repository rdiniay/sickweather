//
//  SWFamilyProfileViewController.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyProfileViewController.h"
#import "SWFamilyProfileHeaderView.h"
#import "SWFamilyProfileSectionHeaderView.h"
#import "SWFamilyProfileRowView.h"
#import "SWEditMyProfileViewController.h"
#import "NSString+FontAwesome.h"
#import "Flurry.h"
#import "SWAddFamilyMemberViewController.h"

#define staticHeaderImageViewDefaultHeight 200
#define staticHeaderImageViewDefaultAlpha 0.25
#define staticHeaderImageOverlayViewDefaultAlpha 0.0
#define rowHeight 50

@interface SWFamilyProfileViewController () <SWAddFamilyMemberViewControllerDelegate,SWFamilyProfileRowViewDelegate, SWFamilyProfileHeaderViewDelegate, SWFamilyProfileSectionHeaderViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSDictionary *familyProfileData;
@property (strong, nonatomic) UIImageView *staticHeaderImageView;
@property (strong, nonatomic) UIView *staticHeaderImageOverlayView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) SWFamilyProfileHeaderView *headerView;
@property (strong, nonatomic) SWFamilyProfileSectionHeaderView *contactSectionHeader;
@property (strong, nonatomic) SWFamilyProfileRowView *emailRow;
@property (strong, nonatomic) SWFamilyProfileSectionHeaderView *aboutSectionHeader;
@property (strong, nonatomic) SWFamilyProfileRowView *genderRow;
@property (strong, nonatomic) SWFamilyProfileRowView *birthdayRow;
@property (strong, nonatomic) UIView *deleteFamilyMemberButtonContainer;
@property (strong, nonatomic) UIButton *deleteFamilyMemberButton;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSMutableDictionary *imageForStringURL;
@property (strong, nonatomic) NSMutableArray *stringURLCurrentlyLoading;
@property (strong, nonatomic) NSLayoutConstraint *staticHeaderImageViewHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *staticHeaderImageOverlayViewHeightConstraint;
@end

@implementation SWFamilyProfileViewController

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat y = -scrollView.contentOffset.y;
    // HANDLE ZOOM/FADE EFFECT
    if (y > 0)
    {
        self.staticHeaderImageViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight + y;
        self.staticHeaderImageOverlayViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight + y;
        self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
        self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
        CGFloat scale = y/self.view.bounds.size.height;
        if (scale > 1)
        {
            scale = 1;
        }
        CGFloat newAlpha = staticHeaderImageViewDefaultAlpha + (0.75*scale);
        self.staticHeaderImageView.alpha = newAlpha;
        newAlpha = staticHeaderImageOverlayViewDefaultAlpha + (1.0*scale);
        self.staticHeaderImageOverlayView.alpha = newAlpha;
    }
    else
    {
        self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
        self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
    }
    
    // HANDLE HIDING IMAGE IF NEEDED
    if (y < -200)
    {
        self.staticHeaderImageView.hidden = YES;
    }
    else
    {
        self.staticHeaderImageView.hidden = NO;
    }
}

#pragma mark - SWAddFamilyMemberViewControllerDelegate

- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

-(void)addFamilyMemberViewControllerWantsToDismiss:(id)sender withFamily:(SWFamilyMemberModal *)member withPhoto:(UIImage *)image{
    // Gets profile pic to update right away, before the edit screen drops down with animation, better UX, updated pic is just there, which is, we believe, what the user expects here
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.headerView setProfilePicImage:image];
    });
    [self dismissViewControllerAnimated:YES completion:^{
        [self.headerView setProfilePicImage:image];
    }];
    [self refreshFamilyListing];
}


- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender withFamily:(SWFamilyMemberModal*)member
{
    [self refreshFamilyListing];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)addFamilyMemberViewControllerWantsToCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWFamilyProfileRowViewDelegate

- (NSString *)familyProfileRowViewTitleString:(SWFamilyProfileRowView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.emailRow])
    {
        title = [self userDataEmailString];
    }
    else if ([sender isEqual:self.genderRow])
    {
        title = [self userDataGenderFormattedString];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        title = [self userDataDateOfBirthFormattedString];
    }
    if (![title isKindOfClass:[NSString class]])
    {
        title = @""; // Make sure it's a string...
    }
    return title;
}

- (UIImage *)familyProfileRowViewIconImage:(SWFamilyProfileRowView *)sender
{
    UIImage *image;
    if ([sender isEqual:self.emailRow])
    {
        image = [UIImage imageNamed:@"profile-icon-email"];
    }
    else if ([sender isEqual:self.genderRow])
    {
        image = [UIImage imageNamed:@"profile-icon-gender"];
    }
    else if ([sender isEqual:self.birthdayRow])
    {
        image = [UIImage imageNamed:@"profile-icon-birthdate"];
    }
    return image;
}

- (void)familyProfileRowViewWasTapped:(SWFamilyProfileRowView *)sender
{
    
}

#pragma mark - SWFamilyProfileSectionHeaderViewDelegate

- (NSString *)familyProfileSectionHeaderViewTitleString:(SWFamilyProfileSectionHeaderView *)sender
{
    NSString *title = @"";
    if ([sender isEqual:self.contactSectionHeader])
    {
        title = @"CONTACT";
    }
    else if ([sender isEqual:self.aboutSectionHeader])
    {
        title = @"ABOUT";
    }
    return title;
}

#pragma mark - SWFamilyProfileHeaderViewDelegate

- (NSString *)familyProfileHeaderViewNameString:(SWFamilyProfileHeaderView *)sender
{
    NSString *answer = self.familyMember.name_first;
    if (self.familyMember.name_last){
        [answer stringByAppendingString:[[NSString alloc] initWithFormat:@" %@", self.familyMember.name_last]];
    }
    if (answer)
    {
        return [NSString stringWithFormat:@"%@\nShafiq",answer];
    }
    return @"Unknown";
}

- (NSString *)familyProfileHeaderViewNicknameString:(SWFamilyProfileHeaderView *)sender
{
    NSString *answer = self.familyMember.nickname;
	if (self.familyMember && (([self.familyMember.confirmed isKindOfClass:[NSString class]] && [self.familyMember.confirmed isEqualToString:@"0"]) || self.familyMember.confirmed == nil)) {
		return @"(Pending)";
	}
    if (answer)
    {
        return answer;
    }
    return @"";
}

- (UIImage *)familyProfileHeaderViewProfilePicImage:(SWFamilyProfileHeaderView *)sender
{
    if (self.familyMember.photo_original == nil && self.familyMember.photo_thumb == nil) {
        [sender setProfilePicImage:[UIImage imageNamed:@"avatar-empty"]];
        return [UIImage imageNamed:@"avatar-empty"];
    }
    
    NSString *stringURL = [self userDataProfilePicStringURL];
    UIImage *imageToReturn = [self.imageForStringURL objectForKey:stringURL];
    if (!imageToReturn)
    {
        if ([self.stringURLCurrentlyLoading containsObject:stringURL])
        {
            //NSLog(@"Skip, work in progress for stringURL %@", stringURL);
        }
        else
        {
            //NSLog(@"Start, fetch for stringURL %@", stringURL);
            [self.stringURLCurrentlyLoading addObject:stringURL]; // Log as currently loading
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
            [[session dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                //NSLog(@"Background Thread...");
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"Main Thread...");
                    [self.stringURLCurrentlyLoading removeObject:stringURL]; // Remove from currently loading
                    UIImage *image = [UIImage imageWithData:data];
                    if (image)
                    {
                        [self.imageForStringURL setObject:image forKey:stringURL]; // Load to cache
                        //NSLog(@"Loaded image to cache for stringURL %@", stringURL);
                        [sender setProfilePicImage:image];
                    }
                    else
                    {
                        [sender setProfilePicImage:[UIImage imageNamed:@"avatar-empty"]];
                    }
                });
            }] resume];
        }
    }
    return imageToReturn;
}

#pragma mark - SWFamilyReportsViewControllerDelegate

- (void)familyReportsViewControllerWantsToDismiss
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Target Action

- (void)deleteFamilyMemberButtonTouchUpInside:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Family Member" message:@"Are you sure you want to delete this member?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteFamilyMember];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:delete];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    [Flurry logEvent:@"Family Profile - Edit Button Tapped"];
    SWAddFamilyMemberViewController *vc = [[SWAddFamilyMemberViewController alloc] init];
    vc.delegate = self;
    vc.wantsToEdit = true;
    vc.familyMember = self.familyMember;
    vc.title = [[NSString alloc] initWithFormat:(@"Edit %@'s Profile"), self.familyMember.name_first];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:^{}];
}

- (void)viewTapped:(id)sender
{
    //[SWHelper helperShowAlertWithTitle:@"Somebody touched me. I felt energy leave me."];
}

-(void)deleteFamilyMember{
    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"Deleting family member..."];
    [[SWHelper helperAppBackend]appBackendDeleteFamilyMemberUsingArgs:@{@"member_id":self.familyMember.member_id} usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) {
                [SWHelper helperDismissFullScreenActivityIndicator];
                [self popToFamilyView];
            }else {
                [SWHelper helperDismissFullScreenActivityIndicator];
				UIAlertController *alert = [UIAlertController alertControllerWithTitle:[jsonResponseBody objectForKey:@"message"] message:nil preferredStyle:UIAlertControllerStyleAlert];
				UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
				[alert addAction:ok];
				[self presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
}

-(void)popToFamilyView{
	if (self.familyMember && [self.familyMember.confirmed isKindOfClass:[NSString class]] &&  [self.familyMember.confirmed isEqualToString:@"1"]) {
		 [self popBackWith:3];
	}else {
		 [self popBackWith:2];
	}
    [self refreshFamilyListing];
}

-(void)refreshFamilyListing{
    self.title = [[NSString alloc] initWithFormat:(@"Edit %@'s Profile"), self.familyMember.name_first];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSWRefreshFamilyMembersNotification
                                                        object:self
                                                      userInfo:nil];
}

-(void)popBackWith:(int)nb{
    NSArray *viewControllers = [self.navigationController viewControllers];
    if ([viewControllers count] >= nb) {
        [self.navigationController popToViewController:[viewControllers objectAtIndex:[viewControllers count] - nb] animated:true];
        
    }
}

#pragma mark - Helpers

- (NSString *)userDataProfilePicStringURL
{
    NSString *answer = self.familyMember.photo_original;
    if (answer)
    {
        return answer;
    }
    return [[self.familyProfileData objectForKey:@"user"] objectForKey:@"profile_pic_url"]; // TODO: Check why user is sent here
}

- (NSString *)userDataFirstNameString
{
    NSString *answer = self.familyMember.name_first;
    if (answer)
    {
        return answer;
    }
    return [[self.familyProfileData objectForKey:@"user"] objectForKey:@"first_name"];
}

- (NSString *)userDataLastNameString
{
    NSString *answer = self.familyMember.name_last;
    if (answer && ![answer isEqual:[NSNull null]] && ![answer isEqualToString:@""])
    {
        return answer;
    }
    return [[self.familyProfileData objectForKey:@"user"] objectForKey:@"last_name"];
}

- (NSString *)userDataEmailString
{
    NSString *answer = self.familyMember.email;
    if (answer && ![answer isEqual:[NSNull null]] && ![answer isEqualToString:@""])
    {
        return answer;
    }
    return @"Unknown";
}

- (NSString *)userDataGenderString
{
    NSString *answer = self.familyMember.gender;
    if (answer && ![answer isEqual:[NSNull null]] && ![answer isEqualToString:@""])
    {
        return answer;
    }
    return @"Not Specified";
}

- (NSString *)userDataGenderFormattedString
{
    NSString *answer = self.familyMember.gender;
    if (answer)
    {
        if ([answer isEqualToString:@"1"])
        {
            answer = @"Female";
        }else if ([answer isEqualToString:@"2"])
        {
            answer = @"Male";
        }else if ([answer isEqualToString:@"3"])
        {
            answer = @"Other";
        }else if ([answer isEqualToString:@"4"])
        {
            answer = @"Not Specified";
        }else {
            answer = @"Not Specified";
        }
    }
    else
    {
        answer = @"Not Specified";
    }
    return answer;
}

- (NSString *)userDataDateOfBirthFormattedString
{
    NSString *answer = self.familyMember.birthdate;
    if (answer == nil || [answer isEqual: [NSNull null]] || [answer isEqualToString:@""])
    {
        answer = @"Not Specified";
    }
    return answer;
}

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.headerView.delegate = self;
    self.emailRow.delegate = self;
    self.genderRow.delegate = self;
    self.birthdayRow.delegate = self;
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT SELF
    self.imageForStringURL = [NSMutableDictionary new];
    self.stringURLCurrentlyLoading = [NSMutableArray new];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.staticHeaderImageView = [[UIImageView alloc] init];
    self.staticHeaderImageOverlayView = [[UIView alloc] init];
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.headerView = [[SWFamilyProfileHeaderView alloc] init];
    self.contactSectionHeader = [[SWFamilyProfileSectionHeaderView alloc] init];
    self.emailRow = [[SWFamilyProfileRowView alloc] init];
    self.aboutSectionHeader = [[SWFamilyProfileSectionHeaderView alloc] init];
    self.genderRow = [[SWFamilyProfileRowView alloc] init];
    self.birthdayRow = [[SWFamilyProfileRowView alloc] init];
    self.deleteFamilyMemberButtonContainer = [[UIView alloc] init];
    self.deleteFamilyMemberButton = [[UIButton alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.staticHeaderImageView forKey:@"staticHeaderImageView"];
    [self.viewsDictionary setObject:self.staticHeaderImageOverlayView forKey:@"staticHeaderImageOverlayView"];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.headerView forKey:@"headerView"];
    [self.viewsDictionary setObject:self.contactSectionHeader forKey:@"contactSectionHeader"];
    [self.viewsDictionary setObject:self.emailRow forKey:@"emailRow"];
    [self.viewsDictionary setObject:self.aboutSectionHeader forKey:@"aboutSectionHeader"];
    [self.viewsDictionary setObject:self.genderRow forKey:@"genderRow"];
    [self.viewsDictionary setObject:self.birthdayRow forKey:@"birthdayRow"];
    [self.viewsDictionary setObject:self.deleteFamilyMemberButtonContainer forKey:@"deleteFamilyMemberButtonContainer"];
    [self.viewsDictionary setObject:self.deleteFamilyMemberButton forKey:@"deleteFamilyMemberButton"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.staticHeaderImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.staticHeaderImageOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contactSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.aboutSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.genderRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.birthdayRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.deleteFamilyMemberButtonContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.deleteFamilyMemberButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.staticHeaderImageView];
    [self.view addSubview:self.staticHeaderImageOverlayView];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.headerView];
    [self.contentView addSubview:self.contactSectionHeader];
    [self.contentView addSubview:self.emailRow];
    [self.contentView addSubview:self.aboutSectionHeader];
    [self.contentView addSubview:self.genderRow];
    [self.contentView addSubview:self.birthdayRow];
    [self.contentView addSubview:self.deleteFamilyMemberButtonContainer];
    [self.deleteFamilyMemberButtonContainer addSubview:self.deleteFamilyMemberButton];
    
    // LAYOUT
    
    // Layout STATIC HEADER IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[staticHeaderImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[staticHeaderImageView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.staticHeaderImageViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.staticHeaderImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:staticHeaderImageViewDefaultHeight];
    [self.view addConstraint:self.staticHeaderImageViewHeightConstraint];
    
    // Layout STATIC HEADER OVERLAY IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[staticHeaderImageOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[staticHeaderImageOverlayView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.staticHeaderImageOverlayViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.staticHeaderImageOverlayView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:staticHeaderImageViewDefaultHeight];
    [self.view addConstraint:self.staticHeaderImageOverlayViewHeightConstraint];
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout HEADER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[headerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[headerView(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.headerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
    
    // Layout CONTACT SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contactSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[headerView]-(0)-[contactSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout EMAIL ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[emailRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[contactSectionHeader]-(0)-[emailRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout ABOUT SECTION HEADER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[aboutSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[emailRow]-(0)-[aboutSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout GENDER ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[genderRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[aboutSectionHeader]-(0)-[genderRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout BIRTHDAY ROW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[birthdayRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[genderRow]-(0)-[birthdayRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout DELETE FAMILY MEMBER BUTTON CONTAINER
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[deleteFamilyMemberButtonContainer]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[birthdayRow]-(0)-[deleteFamilyMemberButtonContainer(110)]-|"] options:0 metrics:0 views:self.viewsDictionary]];// !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR HEIGHT !!!
    
    // Layout DELETE FAMILY MEMBER BUTTON
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[deleteFamilyMemberButton]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(30)-[deleteFamilyMemberButton]-(30)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)]];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.navigationBar.translucent = NO;
    
    // Config TITLE
    self.title = [[NSString alloc] initWithFormat:(@"%@'s Profile"), self.familyMember.name_first];
    self.navigationItem.titleView = nil;
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
	if (self.familyMember && [self.familyMember.confirmed isKindOfClass:[NSString class]] && [self.familyMember.confirmed isEqualToString:@"1"]) {
		 self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(rightBarButtonItemTapped:)];
	}
	
    // Config STATIC HEADER IMAGE VIEW
    self.staticHeaderImageView.image = [UIImage imageNamed:@"profile-map-background"];
    self.staticHeaderImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.staticHeaderImageView.clipsToBounds = YES;
    self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
    
    // Config STATIC HEADER IMAGE OVERLAY VIEW
    self.staticHeaderImageOverlayView.backgroundColor = [UIColor clearColor];
    self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // Config HEADER VIEW
    self.headerView.delegate = self;
    
    // Config CONTACT SECTION HEADER
    self.contactSectionHeader.delegate = self;
    
    // Config EMAIL ROW
    self.emailRow.delegate = self;
    
    // Config ABOUT SECTION HEADER
    self.aboutSectionHeader.delegate = self;
    
    // Config GENDER ROW
    self.genderRow.delegate = self;
    
    // Config BIRTHDAY ROW
    self.birthdayRow.delegate = self;
    [self.birthdayRow showBottomBorder];
    
    // Config LOGOUT BUTTON
    [self.deleteFamilyMemberButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Delete Family Member" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:17]}] forState:UIControlStateNormal];
    [self.deleteFamilyMemberButton setBackgroundColor:[SWColor colorSickweatherWhite255x255x255]];
    [self.deleteFamilyMemberButton.layer setBorderColor:[SWColor colorSickweatherGrayText135x135x135].CGColor];
    [self.deleteFamilyMemberButton.layer setBorderWidth:0.5];
    [self.deleteFamilyMemberButton.layer setCornerRadius:2];
    [self.deleteFamilyMemberButton addTarget:self action:@selector(deleteFamilyMemberButtonTouchUpInside:)  forControlEvents:UIControlEventTouchUpInside]; // TODO: change method name
    
    // Config LOGOUT BUTTON CONTAINER
    self.deleteFamilyMemberButtonContainer.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
 
}

@end
