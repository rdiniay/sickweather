//
//  SWTagFlowCollectionViewCell.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportCollectionViewCellForShareButton.h"
#import "SWColor.h"
#import "SWFont.h"
#import "NSString+FontAwesome.h"

@interface SWAddMessageToReportCollectionViewCellForShareButton ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIButton *shareButton;
@end

@implementation SWAddMessageToReportCollectionViewCellForShareButton

#pragma mark - Public Methods

#pragma mark - Target/Action

- (void)shareButtonTapped:(id)sender
{
    [self.delegate addMessageToReportCollectionViewCellForShareButtonDelegateButtonTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.shareButton = [[UIButton alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.shareButton forKey:@"shareButton"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.shareButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.shareButton];
        
        // LAYOUT
        
        // Layout SHARE BUTTON
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[shareButton]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[shareButton]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config SHARE BUTTON
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Share" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224], NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}];
        [self.shareButton setAttributedTitle:attributedTitle forState:UIControlStateNormal];
        [self.shareButton.layer setBorderColor:[SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor];
        [self.shareButton.layer setBorderWidth:0.5];
        [self.shareButton.layer setCornerRadius:15];
        [self.shareButton addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.shareButton.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
