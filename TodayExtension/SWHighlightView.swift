//
//  SWHighlightView.swift
//  Sickweather
//
//  Created by John Erck on 5/20/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit

protocol SWHighlightViewDelegate {
    func highlightViewTouchUpInside(_ view: UIView)
    func highlightViewTouchCancelled(_ view: UIView)
    func highlightViewBaseColor(_ view: UIView) -> UIColor
    func highlightViewHighlightColor(_ view: UIView) -> UIColor
}

class SWHighlightView: UIView {
    
    var delegate:SWHighlightViewDelegate! = nil
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        backgroundColor = delegate.highlightViewHighlightColor(self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        backgroundColor = delegate.highlightViewBaseColor(self)
        delegate.highlightViewTouchUpInside(self)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        backgroundColor = delegate.highlightViewBaseColor(self)
        delegate.highlightViewTouchCancelled(self)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
