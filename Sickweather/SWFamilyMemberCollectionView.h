//
//  SWFamilyMemberCollectionView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 01/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"


@protocol SWFamilyMemberCollectionViewDelegate
-(void)familyMemberItemSelected:(SWFamilyMemberModal*)member;
-(void)youItemSelected;
@end

@interface SWFamilyMemberCollectionView : UIView
@property (nonatomic, weak) id<SWFamilyMemberCollectionViewDelegate> delegate;
-(void) initializeCollectionViewWith:(NSMutableArray*)familyMembers;
@end
