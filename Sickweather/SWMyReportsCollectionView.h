//
//  SWMyReportsCollectionView.h
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWMyReportsCollectionView : UICollectionView

@end
