//
//  SWConstants.m
//  Sickweather
//
//  Created by John Erck on 10/10/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWConstants.h"

@implementation SWConstants

+ (NSString *)masterSwitchForSickweatherProximityBasedIllnessAlertsIsOnDefaultsKey
{
    return @"sickweatherAlertsOn"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)hasPassedWelcomeToSickweatherSlideThroughScreensDefaultsKey
{
    return @"hasPassedWelcomeToSickweatherSlideThroughScreensDefaultsKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)hasPassedInitialAlertsSetupScreenDefaultsKey
{
    return @"hasPassedWelcomeScreen"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)hasDismissedLoginScreenUserDefaultsKey
{
    return @"dismissedLoginScreenAsFirstTimeUserDefaultsKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)hasBeenTippedAboutGeofenceOnButNoIllnessesSelectedDefaultsKey
{
    return @"kSWHasBeenTippedAboutGeofenceOnButNoIllnessesSelected"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)currentlyLoggedInUserSickweatherIdDefaultsKey
{
    return @"currentlyLoggedInUserSickweatherIdDefaultsKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)hasDismissedTodayWidgetAdViewDefaultsKey
{
    return @"hasDismissedTodayWidgetAdViewDefaultsKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)symptomsDefaultKey
{
	return @"symptomsDefaultKey";  // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)trackerTypesDefaultKey
{
	return @"trackerTypesDefaultKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)familyIdDefaultKey
{
	return @"familyIdDefaultKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)cityDefaultKey
{
	return @"cityDefaultKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (NSString *)isCelsiusTemperatureTypeDefaultKey
{
	return @"isCelsiusTemperatureTypeDefaultKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

+ (CGFloat)fontSizeForSignInFlowGrayLinkButtons
{
    return 14;
}

+ (NSString *)bluetoothSwaiveDeviceKey
{
    return @"bluetoothSwaiveDeviceKey"; // HAS BEEN RELEASED, DON'T CHANGE
}
+ (NSString *)bluetoothKinsaDeviceKey
{
    return @"bluetoothKinsaDeviceKey"; // HAS BEEN RELEASED, DON'T CHANGE
}
+ (NSString *)bluetoothPhilipsDeviceKey
{
    return @"bluetoothPhilipsDeviceKey"; // HAS BEEN RELEASED, DON'T CHANGE
}

// Tags (must be unique within usage set - open)
+ (NSInteger)drivingDirectionsLeftAccessoryControlTag {return 1;}
+ (NSInteger)launchSafariRightAccessoryControlTag {return 2;}
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewTag {return 3;}
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewPhoneViewTag {return 4;}
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewGetDirectionsViewTag {return 5;}
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewSpecialOfferButtonTag {return 6;}
+ (NSInteger)sponsoredMapMarkerAnnotationViewCustomCalloutViewProfilePicImageViewTag {return 7;}
// Tags (must be unique within usage set - close)

// NSNotificationCenter notification name constants (http://stackoverflow.com/a/539191/394969)
NSString *const kSWLocationManagerDidEnterRegionNotification = @"kSWLocationManagerDidEnterRegionNotification";
NSString *const kSWLocationManagerDidDetermineStateNotification = @"kSWLocationManagerDidDetermineStateNotification";
NSString *const kSWLocationManagerMonitoringDidFailForRegionWithErrorNotification = @"kSWLocationManagerMonitoringDidFailForRegionWithErrorNotification";
NSString *const kSWLocationManagerDidStartMonitoringForRegionNotification = @"kSWLocationManagerDidStartMonitoringForRegionNotification";
NSString *const kSWLocationManagerDidUpdateLocationsNotification = @"kSWLocationManagerDidUpdateLocationsNotification";
NSString *const kSWLocationManagerDidChangeAuthorizationStatusNotification = @"kSWLocationManagerDidChangeAuthorizationStatusNotification";
NSString *const kSWCoreDataManagedDocumentDidLoadNotificationKey = @"kSWCoreDataManagedDocumentDidLoadNotificationKey";
NSString *const kSWDidUpdateCurrentLocationPlacemarkNotification = @"kSWDidUpdateCurrentLocationPlacemarkNotification";
NSString *const kSWECSlidingViewControllerDidSlideTopViewControllerBackOnStackNotification = @"kSWECSlidingViewControllerDidSlideTopViewControllerBackOnStackNotification";
NSString *const kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification = @"kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification";
NSString *const kSWUserDidJustLogOutNotification = @"kSWUserDidJustLogOutNotification";
NSString *const kSWRefreshFamilyMembersNotification = @"kSWRefreshFamilyMembersNotification";
NSString *const kSWRefreshEventsNotification = @"kSWRefreshEventsNotification";

NSString *const kSWShowUserFeedBackFlowNotificationKey = @"kSWShowUserFeedBackFlowNotificationKey";

// Current user key, for coredata fetch of current user object
NSString *const kSWCoreDataIsCurrentUserKeyYes = @"kSWCoreDataIsCurrentUserKeyYes";
NSString *const kSWCoreDataIsCurrentUserKeyNo = @"kSWCoreDataIsCurrentUserKeyNo";

@end
