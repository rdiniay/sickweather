//
//  SWDatePicker.m
//  Sickweather
//
//  Created by John Erck on 10/14/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWDatePicker.h"

@interface SWDatePicker ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *topBorder;
@property (strong, nonatomic) UIToolbar *toolbar;
@property (strong, nonatomic) UILabel *titleLabel;
@end

@implementation SWDatePicker

#pragma mark - Public Methods

- (void)setTitle:(NSString *)title
{
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName: [SWColor colorSickweatherStyleGuideGrayDark35x31x32],
                                                                                                             NSFontAttributeName: [SWFont fontStandardBoldFontOfSize:18]}];
    [self.titleLabel sizeToFit];
}

#pragma mark - Target Action

- (void)cancelWasTapped:(id)sender
{
    [self.delegate datePickerWantsToCancel:self];
}

- (void)doneWasTapped:(id)sender
{
    [self.delegate datePickerWantsToDone:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.topBorder = [[UIView alloc] init];
        self.toolbar = [[UIToolbar alloc] init];
        self.datePicker = [[UIDatePicker alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.topBorder forKey:@"topBorder"];
        [self.viewsDictionary setObject:self.toolbar forKey:@"toolbar"];
        [self.viewsDictionary setObject:self.datePicker forKey:@"datePicker"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.topBorder.translatesAutoresizingMaskIntoConstraints = NO;
        self.toolbar.translatesAutoresizingMaskIntoConstraints = NO;
        self.datePicker.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.topBorder];
        [self addSubview:self.toolbar];
        [self addSubview:self.datePicker];
        
        // LAYOUT
        
        // Layout TOP BORDER
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topBorder]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topBorder(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TOOLBAR
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[toolbar]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[topBorder][toolbar(44)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout DATE PICKER
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[datePicker]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[toolbar]-(20)-[datePicker]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config SELF
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        // Config TOP BORDER
        self.topBorder.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config TOOLBAR
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelWasTapped:)];
        UIBarButtonItem *spaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        self.titleLabel = [[UILabel alloc] init];
        UIBarButtonItem *title = [[UIBarButtonItem alloc] initWithCustomView:self.titleLabel];
        UIBarButtonItem *spaceRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneWasTapped:)];
        [self.toolbar setItems:@[cancel,spaceLeft,title,spaceRight,done]];
        
        // Config DATE PICKER
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.maximumDate = [NSDate date];
        
        // COLOR FOR DEVELOPMENT PURPOSES
    }
    return self;
}

@end
