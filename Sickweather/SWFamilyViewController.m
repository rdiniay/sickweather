//
//  SWFamilyViewController.m
//  Sickweather
//
//  Created by John Erck on 8/17/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyViewController.h"
#import "SWAddFamilyMemberViewController.h"
#import "SWConnectBluetoothDeviceTableViewController.h"
#import "SWMyProfileViewController.h"
#import "SWEditMyProfileViewController.h"
#import "SWMyReportsViewController.h"
#import "SWNavigationController.h"
#import "SWEventsTableViewController.h"
#import "SWFamilyLoginView.h"
#import "SWCreateSickweatherAccountViewController.h"
#import "SWAddNewEventTableViewController.h"
#import "SWBlockTabWithLoginView.h"
#import "SWFamilyMemberCollectionView.h"
#import "SWFamilyMemberModal.h"
#import "SWSignInWithSickweatherViewController.h"
#import "SWFamilyProfileViewController.h"

@interface SWFamilyViewController () <SWAddFamilyMemberViewControllerDelegate, SWSignInWithSickweatherViewControllerDelegate , SWConnectBluetoothDeviceViewControllerDelegate, SWMyProfileViewControllerDelegate, SWEditMyProfileViewControllerDelegate, SWMyReportsViewControllerDelegate, SWSignInWithFacebookViewControllerDelegate, SWFamilyLoginViewDelegate, SWCreateSickweatherAccountViewControllerDelegate, SWBlockTabWithLoginViewDelegate, SWFamilyMemberCollectionViewDelegate>
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) SWBlockTabWithLoginView *blockTabWithLoginView;
@property (strong, nonatomic) UIButton *addFamilyMemberButton;
@property (strong, nonatomic) UIButton *addNewEventButton;
@property (strong, nonatomic) UIButton *connectBluetoothDeviceButton;
@property (strong, nonatomic) SWFamilyLoginView *familyLoginView;
@property (strong, nonatomic) SWFamilyMemberCollectionView* familyCollectionView;
@property (strong, nonatomic) NSMutableArray* familyMemberArray;
@property (strong, nonatomic) UIActivityIndicatorView* familyLoadingIndicator;
@end


@implementation SWFamilyViewController

#pragma mark - SWSignInWithFacebookViewControllerDelegate

- (void)didDismissWithSuccessfulLogin:(SWSignInWithFacebookViewController *)vc
{
    [self viewWillAppear:true];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didDismissWithoutLoggingIn:(SWSignInWithFacebookViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWSignInWithSickweatherViewControllerDelegate
- (void)signInWithSickweatherViewControllerDidDismissWithSuccessfulLogin:(SWSignInWithSickweatherViewController *)vc{
    [self refreshFamilyMembers];
}

- (void)signInWithSickweatherViewControllerDidDismissWithoutLoggingIn:(SWSignInWithSickweatherViewController *)vc{
    
}

#pragma mark - SWCreateSickweatherAccountViewControllerDelegate

- (void)createSickweatherAccountViewControllerDidDismissWithSuccessfulLogin:(SWCreateSickweatherAccountViewController *)vc
{
    [[SWHelper helperAppDelegate] appDelegateUpdateUserBasedOnNetwork];
    [self refreshFamilyMembers];
    [vc dismissViewControllerAnimated:YES completion:^{}];
}

- (void)createSickweatherAccountViewControllerDidDismissWithoutLoggingIn:(SWCreateSickweatherAccountViewController *)vc
{
        // Does not get forwarded. Client of this class should never concern itself with this. We just disimss.
    [vc dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWAddFamilyMemberViewControllerDelegate

- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{}];
}
- (void)addFamilyMemberViewControllerWantsToDismiss:(id)sender withFamily:(SWFamilyMemberModal*)member
{
    [self dismissViewControllerAnimated:YES completion:^{}];
    [self familyMemberItemSelected:member];
    [self refreshFamilyMembers];
}

- (void)addFamilyMemberViewControllerWantsToCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWConnectBluetoothDeviceTableViewControllerDelegate

- (void)connectBluetoothDeviceViewControllerWantsToDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
    //    [self refreshFamilyMembers]; // TODO: see what we have to do here
}

- (void)connectBluetoothDeviceViewControllerWantsToCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SWMyProfileViewControllerDelegate

- (void)myProfileViewControllerWantsToPopOffTheStack:(SWMyProfileViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWEditMyProfileViewControllerDelegate

- (void)editMyProfileViewControllerWantsToDismiss:(SWEditMyProfileViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)editMyProfileViewControllerWantsToDismissWithUpdateProfilePic:(UIImage *)image sender:(SWEditMyProfileViewController *)sender
{
    NSLog(@"Update pic...");
}

#pragma mark - SWMyReportsViewControllerDelegate

- (void)myReportsViewControllerWantsToDismiss
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SWFamilyMemberCollectionViewDelegate

- (void)familyMemberItemSelected:(SWFamilyMemberModal *)member {
	if ([member.confirmed isEqualToString:@"1"]) {
		SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
		eventsTableViewController.isFamilyMember = YES;
		eventsTableViewController.familyMember = member;
		[self.navigationController pushViewController:eventsTableViewController animated:YES];
	}else{
		SWFamilyProfileViewController *vc = [[SWFamilyProfileViewController alloc] init];
		vc.familyMember = member;
		[self.navigationController pushViewController:vc animated:YES];
	}
}

- (void)youItemSelected {
    SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
    [self.navigationController pushViewController:eventsTableViewController animated:YES];
}

#pragma mark - SWFamilyLoginView

-(void)loginButtonPressed
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWSignInWithFacebookViewController *signInWithFacebookViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
    signInWithFacebookViewController.delegate = self;
    SWNavigationController *signInWithFacebookNavigationController = [[SWNavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
    [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
}

-(void)createAnAccountButtonPressed
{
    SWCreateSickweatherAccountViewController *createSickweatherAccountViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SWCreateSickweatherAccountViewController"];
    createSickweatherAccountViewController.delegate = self;
    [self presentViewController:createSickweatherAccountViewController animated:YES completion:nil];
}

#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        self.familyLoginView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 44);
    }
    else if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation) && !UIInterfaceOrientationIsLandscape(currentOrientation)){
        self.familyLoginView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, self.view.frame.size.width);
    }
}

#pragma mark - Target/Action

- (void)addFamilyMemberButtonTapped:(id)sender
{
	//Add Flurry Event
	[SWHelper logFlurryEventsWithTag:@"Family > Add a Family Member button tapped"];
    SWAddFamilyMemberViewController *vc = [[SWAddFamilyMemberViewController alloc] init];
    vc.delegate = self;
	vc.title = @"Add a Family Member";
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:^{}];
}

- (void)addNewEventButtonTapped:(id)sender
{
    //Add Flurry Event
    [SWHelper logFlurryEventsWithTag:@"Family > Add a New Event button tapped"];
    SWAddNewEventTableViewController *addNewEventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddNewEventTableViewController"];
    addNewEventsTableViewController.isFromFamilyScreen = true;
    [self.navigationController pushViewController:addNewEventsTableViewController animated:YES];
}

- (void)connectBluetoothDeviceButtonTapped:(id)sender
{
    //Add Flurry Event
    [SWHelper logFlurryEventsWithTag:@"Family > Connect a Bluetooth Device button tapped"];
    SWConnectBluetoothDeviceTableViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWConnectBluetoothDeviceTableViewController"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES ];
}

- (void)profileTap:(id)sender
{
    SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
    eventsTableViewController.title = [SWUserCDM currentUser].firstName;
    [self.navigationController pushViewController:eventsTableViewController animated:YES];
}

#pragma mark - Helpers
- (void)refreshFamilyMembersWithNotification:(NSNotification*)notification{
    [self refreshFamilyMembers];
}

-(void)getFamilyMemberIdFromBackend{
    if ([SWUserCDM currentlyLoggedInUser]) {
        [[SWHelper helperAppBackend]appBackendGetFamilyIdUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (jsonResponseBody){
                     if ([jsonResponseBody valueForKey:@"family_id"] != nil && ![[jsonResponseBody valueForKey:@"family_id"] isEqual:[NSNull null]] && ![[jsonResponseBody valueForKey:@"family_id"] isEqualToString:@""]){
                         [SWHelper setFamilyId:[jsonResponseBody valueForKey:@"family_id"]];
                         [self refreshFamilyMembers];
                     }
                 }
             });
        }];
    }
}

- (void)refreshFamilyMembers
{
    if ([SWHelper helperUserIsLoggedIn])
    {
    NSString *familyId = [SWHelper getFamilId];
    if (familyId != nil && ![familyId isEqual:[NSNull null]] && ![familyId isEqualToString:@""]) {
        [[SWHelper helperAppBackend] appBackendGetFamilyMemberUsingArgs:@{@"family_id":(familyId != nil && ![familyId isEqualToString:@""]) ? familyId : @""} usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.familyMemberArray = [[NSMutableArray alloc] init];
                for (NSDictionary* familyMember in jsonResponseBody[@"family"]) {
                    SWFamilyMemberModal *member = [[SWFamilyMemberModal alloc] initWithDictionary:familyMember];
                    [self.familyMemberArray addObject:member];
                }
                [self.familyCollectionView initializeCollectionViewWith:self.familyMemberArray];
                [self.familyLoadingIndicator stopAnimating];
            });
        }];
        } else {
            // Clearing the data if any exist
            self.familyMemberArray = [[NSMutableArray alloc] init];
        }
    }
}

-(void)popToFamilyView:(NSNotification *)notification
{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (int i = (int)allViewControllers.count-1; i>=0; i--) {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(void)reloadProfileImage:(NSNotification *)notification
{
    // youImageView
    UIImage *youImage = [UIImage imageNamed:@"avatar-empty"];
    SWUserCDM *user = [SWHelper helperCurrentUser];
    if (user.profilePicThumbnailImageData)
    {
        youImage = [UIImage imageWithData:user.profilePicThumbnailImageData];
    }
}

- (void)userDidJustLogOutNotification:(NSNotification *)notification
{
    [self refreshFamilyMembers];
}

#pragma mark - SWBlockTabWithLoginViewDelegate

- (void)primaryButtonTapped:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWSignInWithFacebookViewController *signInWithFacebookViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
    signInWithFacebookViewController.delegate = self;
    UINavigationController *signInWithFacebookNavigationController = [[UINavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
    [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
}

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (([SWUserCDM currentlyLoggedInUser]))
    {
        self.blockTabWithLoginView.hidden = YES;
		NSString * familyId = [SWHelper getFamilId];
		if (familyId == nil || [familyId isEqual:[NSNull null]] || [familyId isEqualToString:@""]) {
			[self getFamilyMemberIdFromBackend];
		}else{
			[self refreshFamilyMembers];
		}
    }
    else
    {
        self.blockTabWithLoginView.hidden = NO;
    }
	
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLayoutSubviews
{
    CGPoint point = self.familyCollectionView.center;
    point.y -= 16.0f;
    self.familyLoadingIndicator.center = point;
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    self.navigationController.title = @"Family";
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.blockTabWithLoginView = [[SWBlockTabWithLoginView alloc] init];
    self.familyCollectionView = [[NSBundle mainBundle] loadNibNamed:@"SWFamilyMemberCollectionView" owner:nil options:nil][0];
    //    [[SWFamilyMemberCollectionView alloc] init];
    self.addFamilyMemberButton = [[UIButton alloc] init];
    self.addNewEventButton = [[UIButton alloc] init];
    self.connectBluetoothDeviceButton = [[UIButton alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.blockTabWithLoginView forKey:@"blockTabWithLoginView"];
    [self.viewsDictionary setObject:self.familyCollectionView forKey:@"familyCollectionView"];
    [self.viewsDictionary setObject:self.addFamilyMemberButton forKey:@"addFamilyMemberButton"];
    [self.viewsDictionary setObject:self.addNewEventButton forKey:@"addNewEventButton"];
    [self.viewsDictionary setObject:self.connectBluetoothDeviceButton forKey:@"connectBluetoothDeviceButton"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.blockTabWithLoginView.translatesAutoresizingMaskIntoConstraints = NO;
    self.familyCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.addFamilyMemberButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.addNewEventButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.connectBluetoothDeviceButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.blockTabWithLoginView];
    [self.scrollView addSubview:self.familyCollectionView];
    [self.scrollView addSubview:self.addFamilyMemberButton];
    [self.scrollView addSubview:self.addNewEventButton];
    [self.scrollView addSubview:self.connectBluetoothDeviceButton];
    
    // LAYOUT
    
    // scrollView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // blockTabWithLoginView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[blockTabWithLoginView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[blockTabWithLoginView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // familyCollectionView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[familyCollectionView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.familyCollectionView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(40)-[familyCollectionView(170)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // addFamilyMemberButton
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[addFamilyMemberButton]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[familyCollectionView]-(40)-[addFamilyMemberButton(55)]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.addFamilyMemberButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.familyCollectionView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    // addNewEventButton
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[addNewEventButton]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[addFamilyMemberButton]-(20)-[addNewEventButton(55)]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.addNewEventButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.familyCollectionView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    // connectBluetoothDeviceButton
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[connectBluetoothDeviceButton]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[addNewEventButton]-(20)-[connectBluetoothDeviceButton(55)]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // self
    self.title = @"Family";
    
    // navBar
    self.navigationController.navigationBar.translucent = NO;
    
    // view
    self.view.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    
    // scrollView
    self.scrollView.alwaysBounceVertical = YES;
    
    // blockTabWithLoginView
    [self.blockTabWithLoginView updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.blockTabWithLoginView updateImage:[UIImage imageNamed:@"onboarding-icon-family-health"]];
    [self.blockTabWithLoginView updateTitle:@"Track Family Health"];
    [self.blockTabWithLoginView updateDescription:@"Log in to record illnesses, symptoms, temperatures, and medication safely and securely within Sickweather."];
    [self.blockTabWithLoginView updateButtonTitle:@"Login"];
    self.blockTabWithLoginView.delegate = self;
    
    // familyCollectionView
    [self.familyCollectionView initializeCollectionViewWith:nil];
    self.familyCollectionView.delegate = self;
    
    // familyLoadingIndicator
    self.familyLoadingIndicator = [[UIActivityIndicatorView alloc] init];
    [self.view addSubview:self.familyLoadingIndicator];
    [self.view bringSubviewToFront:self.familyLoadingIndicator];
    self.familyLoadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    self.familyLoadingIndicator.color = UIColor.whiteColor;
    if ([SWHelper helperUserIsLoggedIn]) {
        [self.familyLoadingIndicator startAnimating];
    }
    
    // addFamilyMemberButton
    [self.addFamilyMemberButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Add a Family Member" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
    self.addFamilyMemberButton.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    self.addFamilyMemberButton.layer.cornerRadius = 5;
    [self.addFamilyMemberButton addTarget:self action:@selector(addFamilyMemberButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // addNewEventButton
    [self.addNewEventButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Add a New Event" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
    self.addNewEventButton.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    self.addNewEventButton.layer.cornerRadius = 5;
    [self.addNewEventButton addTarget:self action:@selector(addNewEventButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // connectBluetoothDeviceButton
    [self.connectBluetoothDeviceButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Connect a Bluetooth Device" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
    self.connectBluetoothDeviceButton.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
    self.connectBluetoothDeviceButton.layer.cornerRadius = 6;
    self.connectBluetoothDeviceButton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
    self.connectBluetoothDeviceButton.layer.borderWidth = 2.0;
    [self.connectBluetoothDeviceButton addTarget:self action:@selector(connectBluetoothDeviceButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
    // Adding notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popToFamilyView:)
                                                 name:@"PopToFamilyView"
                                               object:nil];
    
    
    //Refresh Members
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFamilyMembersWithNotification:)
                                                 name:kSWRefreshFamilyMembersNotification
                                               object:nil];

    
    // Tell us when we are sure we have the user's thumbnail profile pic saved locally
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadProfileImage:)
                                                 name:kSWFinishedUpdatingCoreDataWithLatestProfilePictureDataNotification
                                               object:nil];
    
    // Tell us if the user logs out
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadProfileImage:)
                                                 name:kSWUserDidJustLogOutNotification
                                               object:nil];
    
    // TELL US WHEN THE USER LOGS OUT
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userDidJustLogOutNotification:)
                                                 name:kSWUserDidJustLogOutNotification
                                               object:nil];
}

@end
