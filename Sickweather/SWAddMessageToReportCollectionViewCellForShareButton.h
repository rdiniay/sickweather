//
//  SWTagFlowCollectionViewCell.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWAddMessageToReportCollectionViewCellForShareButton;

@protocol SWAddMessageToReportCollectionViewCellForShareButtonDelegate
- (void)addMessageToReportCollectionViewCellForShareButtonDelegateButtonTapped:(SWAddMessageToReportCollectionViewCellForShareButton *)sender;
@end

@interface SWAddMessageToReportCollectionViewCellForShareButton : UICollectionViewCell
@property (nonatomic, weak) id<SWAddMessageToReportCollectionViewCellForShareButtonDelegate> delegate;
@end
