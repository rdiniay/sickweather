//
//  SWEventModal.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/6/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWMedicationModal.h"

@interface SWEventModal : NSObject
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *memberId;
@property (strong, nonatomic) NSString *memberName;
@property (strong, nonatomic) NSString *trackerValue;
@property (strong, nonatomic) NSString *trackType;
@property (strong, nonatomic) NSString *createdBy;
@property (strong, nonatomic) NSString *dateCreated;
@property (strong, nonatomic) NSString *lastModifiedBy;
@property (strong, nonatomic) NSString *dateLastModified;
@property (strong, nonatomic) NSString *illnessDisplayWord;
@property (strong, nonatomic) NSString *locale;
//Medication
@property (strong, nonatomic) SWMedicationModal *medication;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
