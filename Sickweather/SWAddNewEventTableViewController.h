//
//  SWAddNewEventTableViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/8/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@interface SWAddNewEventTableViewController : UITableViewController
@property(nonatomic) BOOL isFromFamilyScreen;
@property(nonatomic) BOOL isFromEventsScreen;
@property(nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@end
