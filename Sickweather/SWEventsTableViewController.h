//
//  SWEventsTableViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/5/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@interface SWEventsTableViewController : UITableViewController
@property (nonatomic) BOOL showSuccessHeader;
@property (nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@property (nonatomic,strong) NSString* successMessage;
@end
