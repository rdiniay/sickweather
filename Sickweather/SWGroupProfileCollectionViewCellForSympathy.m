//
//  SWGroupProfileCollectionViewCellForSympathy.m
//  Sickweather
//
//  Created by John Erck on 5/6/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForSympathy.h"
#import "SWGroupProfileViewController.h"

@interface SWGroupProfileCollectionViewCellForSympathy ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSArray *adjustableViewConstraints;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UIImageView *sympathyImageView;
@property (strong, nonatomic) UILabel *sympathyCountLabel;
@property (strong, nonatomic) NSString *count;
@property (strong, nonatomic) NSLayoutConstraint *imageViewWidthConstraint;
@end

@implementation SWGroupProfileCollectionViewCellForSympathy

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForSympathyBumpCount
{
    self.count = [@([self.count integerValue] + 1) stringValue];
    [self groupProfileCollectionViewCellForSympathySetSympathyDict:nil];
    //self.sympathyImageView.image = [UIImage imageNamed:@"sw-sympathy_achievement-color"];
}

- (void)groupProfileCollectionViewCellForSympathySetIconImage:(UIImage *)image
{
    //image = [UIImage imageNamed:@"sw-sympathy_achievement-color"];
    self.sympathyImageView.image = image;
    CGFloat currentRatio = image.size.width/image.size.height;
    CGFloat fixedAspectWidth = SYMPATHY_CELL_HEIGHT*currentRatio;
    self.imageViewWidthConstraint.constant = [@(fixedAspectWidth) integerValue];
}

- (void)groupProfileCollectionViewCellForSympathySetSympathyDict:(NSDictionary *)dict
{
    //NSLog(@"sympathy dict = %@", dict);
	/*
	dict = {
		count = 0;
		icon = "https://mobilesvc.sickweather.com/images/sw-sympathy_achievement-gray.png";
	}
	*/
    // Normalize to string count
    if (!self.count)
    {
        self.count = @"0";
    }
    if ([[dict objectForKey:@"count"] isKindOfClass:[NSNumber class]])
    {
        self.count = [[dict objectForKey:@"count"] stringValue];
    }
    if ([[dict objectForKey:@"count"] isKindOfClass:[NSString class]])
    {
        self.count = [dict objectForKey:@"count"];
    }
    
    // Switch based on 0 or else
    NSString *countAsTextString = @"";
    if ([self.count isEqualToString:@"0"])
    {
        countAsTextString = @"Add sympathy";
    }
    else
    {
        countAsTextString = [[@"+" stringByAppendingString:self.count] stringByAppendingString:@" sympathy"];
    }
    
    // Set text
    [self.sympathyCountLabel setAttributedText:[[NSAttributedString alloc] initWithString:countAsTextString attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12],NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226]}]];
}

- (void)groupProfileCollectionViewCellForSympathyUpdateDefaultInsetsLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom
{
    if (self.adjustableViewConstraints)
    {
        [self.contentView removeConstraints:self.adjustableViewConstraints];
    }
    self.adjustableViewConstraints = @[];
    self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[containerView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary]];
    self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[containerView]-(bottom)-|" options:0 metrics:@{@"top":top,@"bottom":bottom} views:self.viewsDictionary]];
    [self.contentView addConstraints:self.adjustableViewConstraints];
}

#pragma mark - Target/Action

- (void)sympathyImageViewTapped:(id)sender
{
    [self.delegate groupProfileCollectionViewCellForSympathyButtonTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.adjustableViewConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.containerView = [[UIView alloc] init];
        self.sympathyImageView = [[UIImageView alloc] init];
        self.sympathyCountLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.containerView forKey:@"containerView"];
        [self.viewsDictionary setObject:self.sympathyImageView forKey:@"sympathyImageView"];
        [self.viewsDictionary setObject:self.sympathyCountLabel forKey:@"sympathyCountLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.sympathyImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.sympathyCountLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.containerView];
        [self.containerView addSubview:self.sympathyImageView];
        [self.containerView addSubview:self.sympathyCountLabel];
        
        // LAYOUT
        
        // Layout CONTAINER VIEW
        [self groupProfileCollectionViewCellForSympathyUpdateDefaultInsetsLeft:@0 right:@0 top:@0 bottom:@0];
        
        // Layout SYMPATHY IMAGE VIEW
        [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[sympathyImageView]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[sympathyImageView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        self.imageViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.sympathyImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SYMPATHY_CELL_HEIGHT];
        [self.containerView addConstraint:self.imageViewWidthConstraint];
        
        // Layout SYMPATHY COUNT LABEL
        [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[sympathyImageView]-(10)-[sympathyCountLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[sympathyCountLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config SYMPATHY IMAGE VIEW
        self.sympathyImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.sympathyImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *sympathyImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sympathyImageViewTapped:)];
        [self.sympathyImageView addGestureRecognizer:sympathyImageViewTap];
        //self.sympathyImageView.backgroundColor = [UIColor redColor];
        
        // Config SYMPATH COUNT LABEL
        UITapGestureRecognizer *sympathyCountLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sympathyImageViewTapped:)];
        self.sympathyCountLabel.userInteractionEnabled = YES;
        [self.sympathyCountLabel addGestureRecognizer:sympathyCountLabelTap];
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.containerView.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
        //self.sympathyImageView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        //self.sympathyCountLabel.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
    }
    return self;
}

@end
