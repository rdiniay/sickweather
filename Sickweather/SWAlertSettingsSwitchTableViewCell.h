//
//  SWAlertSettingsSwitchTableViewCell.h
//  Sickweather
//
//  Created by John Erck on 12/19/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//mySwitchFlippedOn

@protocol SWAlertSettingsSwitchTableViewCellDelegate
- (void)mySwitchFlippedOn:(id)sender;
- (void)mySwitchFlippedOff:(id)sender;
@end

@interface SWAlertSettingsSwitchTableViewCell : UITableViewCell
@property (nonatomic, weak) id<SWAlertSettingsSwitchTableViewCellDelegate> delegate;
- (void)updateForDict:(NSDictionary *)dict;
- (void)styleAsActive;
- (void)styleAsInactive;
- (void)styleAsOn;
- (void)styleAsOff;
@end
