//
//  SWSettingsTableViewCell.h
//  Sickweather
//
//  Created by John Erck on 10/9/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWSettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *onOffSwitch;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
