//
//  SWEditMyProfileViewController.h
//  Sickweather
//
//  Created by John Erck on 9/28/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWEditMyProfileViewController;

@protocol SWEditMyProfileViewControllerDelegate <NSObject>
- (void)editMyProfileViewControllerWantsToDismiss:(SWEditMyProfileViewController *)sender;
- (void)editMyProfileViewControllerWantsToDismissWithUpdateProfilePic:(UIImage *)image sender:(SWEditMyProfileViewController *)sender;
@end

@interface SWEditMyProfileViewController : UIViewController
@property (nonatomic, weak) id<SWEditMyProfileViewControllerDelegate> delegate;
@end
