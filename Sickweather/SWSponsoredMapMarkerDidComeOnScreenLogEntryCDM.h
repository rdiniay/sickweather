//
//  SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM.h
//  Sickweather
//
//  Created by John Erck on 12/27/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM : NSManagedObject

@property (nonatomic, retain) NSDate * dateSeen;
@property (nonatomic, retain) NSString * sponsoredMapMarkerId;

@end
