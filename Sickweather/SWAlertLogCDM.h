//
//  SWAlertLogCDM.h
//  Sickweather
//
//  Created by John Erck on 4/14/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SWAlertLogCDM : NSManagedObject

@property (nonatomic, retain) NSData * regionObjectData;
@property (nonatomic, retain) NSData * reportDictionaryData;
@property (nonatomic, retain) NSString * reportId;
@property (nonatomic, retain) NSNumber * hasTriggeredLocalNotification;

@end
