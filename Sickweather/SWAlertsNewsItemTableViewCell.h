//
//  SWAlertsNewsItemTableViewCell.h
//  Sickweather
//
//  Created by John Erck on 8/23/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SWAlertsNewsItemTableViewCell;

@protocol SWAlertsNewsItemTableViewCellDelegate <NSObject>
- (void)shareButtonTapped:(UIButton *)sender;
@end

@interface SWAlertsNewsItemTableViewCell : UITableViewCell
- (void)updateForDict:(NSDictionary *)dict;
@property (weak, nonatomic) id <SWAlertsNewsItemTableViewCellDelegate> delegate;
@end
