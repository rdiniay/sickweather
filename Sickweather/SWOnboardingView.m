//
//  SWOnboardingView.m
//  Sickweather
//
//  Created by John Erck on 12/20/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWOnboardingView.h"

@interface SWOnboardingView ()

// UI VIEWS
@property (strong, nonatomic) NSMutableDictionary *myViewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *topImageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;

@end

@implementation SWOnboardingView

#pragma mark - Public Methods

- (void)updateBackgroundColor:(UIColor *)color
{
    self.backgroundColor = color;
}

- (void)updateImage:(UIImage *)image
{
    self.topImageView.image = image;
}

- (void)updateTitle:(NSString *)title
{
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:26]}];
}

- (void)updateDescription:(NSString *)description
{
    self.descriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:description attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:18]}];
}

#pragma mark - Target/Action

- (void)viewWasTapped:(id)sender
{
    [self.delegate onboardingViewWasTapped:self];
}

#pragma mark - Helpers

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT DEFAULTS
        
        // INIT VIEWS
        self.scrollView = [[UIScrollView alloc] init];
        self.topImageView = [[UIImageView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.descriptionLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.myViewsDictionary = [NSMutableDictionary new];
        [self.myViewsDictionary setObject:self.scrollView forKey:@"scrollView"];
        [self.myViewsDictionary setObject:self.topImageView forKey:@"topImageView"];
        [self.myViewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.myViewsDictionary setObject:self.descriptionLabel forKey:@"descriptionLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
        self.topImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
        [self addSubview:self.scrollView];
        [self.scrollView addSubview:self.topImageView];
        [self.scrollView addSubview:self.titleLabel];
        [self.scrollView addSubview:self.descriptionLabel];
        
        // LAYOUT
        NSArray *myConstraints = @[];
        
        // scrollView
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // topImageView
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[topImageView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.topImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[topImageView(300)]"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // titleLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[titleLabel]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[topImageView]-(5)-[titleLabel]"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // descriptionLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[descriptionLabel]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(10)-[descriptionLabel]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // ADD CONSTRAINTS
        [self addConstraints:myConstraints];
        
        // CONFIG
        
        // self
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)];
        [self addGestureRecognizer:tap];
        
        // scrollView
        self.scrollView.alwaysBounceVertical = NO;
        
        // topImageView
        self.topImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        // titleLabel
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        
        // descriptionLabel
        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.descriptionLabel.numberOfLines = 0;
    }
    return self;
}

@end

