//
//  SWAddMessageToReportCollectionViewCellForExampleView1.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportCollectionViewCellForGroup.h"
#import "SWColor.h"
#import "SWFont.h"
#import "TKRoundedView.h"
#import "NSString+FontAwesome.h"

@interface SWAddMessageToReportCollectionViewCellForGroup ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSArray *adjustableViewConstraints;
@property (strong, nonatomic) TKRoundedView *containerView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UILabel *checkmarkLabel;
@end

@implementation SWAddMessageToReportCollectionViewCellForGroup

#pragma mark - Public Methods

- (void)addMessageToReportCollectionViewCellForGroupUpdateDefaultInsetsLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
{
    if (self.adjustableViewConstraints)
    {
        [self.contentView removeConstraints:self.adjustableViewConstraints];
    }
    self.adjustableViewConstraints = @[];
    self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[containerView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary]];
    self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[containerView]-(bottom)-|" options:0 metrics:@{@"top":top,@"bottom":bottom} views:self.viewsDictionary]];
    [self.contentView addConstraints:self.adjustableViewConstraints];
}

- (void)setServerDict:(NSDictionary *)serverDict
{
    //NSLog(@"serverDict = %@", serverDict);
    NSString *name = [serverDict objectForKey:@"name"];
    if ([name isKindOfClass:[NSString class]])
    {
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:name attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224]}];
    }
    NSString *address = [serverDict objectForKey:@"address"];
    if ([address isKindOfClass:[NSString class]])
    {
        self.addressLabel.attributedText = [[NSAttributedString alloc] initWithString:address attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    }
}

- (void)setStyleAsSingleRecordCap
{
    self.containerView.roundedCorners = TKRoundedCornerTopLeft | TKRoundedCornerTopRight | TKRoundedCornerBottomLeft | TKRoundedCornerBottomRight;
    self.containerView.borderColor = [SWColor colorSickweatherLightGray204x204x204];
    self.containerView.fillColor = [UIColor whiteColor];
    self.containerView.drawnBordersSides = TKDrawnBorderSidesLeft | TKDrawnBorderSidesTop | TKDrawnBorderSidesRight | TKDrawnBorderSidesBottom;
    self.containerView.borderWidth = 1.0f;
    self.containerView.cornerRadius = 15.0f;
}

- (void)setStyleAsTopCap
{
    self.containerView.roundedCorners = TKRoundedCornerTopLeft | TKRoundedCornerTopRight;
    self.containerView.borderColor = [SWColor colorSickweatherLightGray204x204x204];
    self.containerView.fillColor = [UIColor whiteColor];
    self.containerView.drawnBordersSides = TKDrawnBorderSidesLeft | TKDrawnBorderSidesTop | TKDrawnBorderSidesRight;
    self.containerView.borderWidth = 1.0f;
    self.containerView.cornerRadius = 15.0f;
}

- (void)setStyleAsCenterCap
{
    self.containerView.roundedCorners = TKRoundedCornerNone;
    self.containerView.borderColor = [SWColor colorSickweatherLightGray204x204x204];
    self.containerView.fillColor = [UIColor whiteColor];
    self.containerView.drawnBordersSides = TKDrawnBorderSidesLeft | TKDrawnBorderSidesTop | TKDrawnBorderSidesRight;
    self.containerView.borderWidth = 1.0f;
    self.containerView.cornerRadius = 15.0f;
}

- (void)setStyleAsBottomCap
{
    self.containerView.roundedCorners = TKRoundedCornerBottomLeft | TKRoundedCornerBottomRight;
    self.containerView.borderColor = [SWColor colorSickweatherLightGray204x204x204];
    self.containerView.fillColor = [UIColor whiteColor];
    self.containerView.drawnBordersSides = TKDrawnBorderSidesLeft | TKDrawnBorderSidesTop | TKDrawnBorderSidesRight | TKDrawnBorderSidesBottom;
    self.containerView.borderWidth = 1.0f;
    self.containerView.cornerRadius = 15.0f;
}

- (void)setStyleAsSelected
{
    self.checkmarkLabel.hidden = NO;
}

- (void)setStyleAsUnselected
{
    self.checkmarkLabel.hidden = YES;
}

#pragma mark - Life Cycle Methods

- (void)prepareForReuse
{
    // TBD...
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.adjustableViewConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.containerView = [[TKRoundedView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.addressLabel = [[UILabel alloc] init];
        self.checkmarkLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.containerView forKey:@"containerView"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.viewsDictionary setObject:self.addressLabel forKey:@"addressLabel"];
        [self.viewsDictionary setObject:self.checkmarkLabel forKey:@"checkmarkLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.checkmarkLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.containerView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.addressLabel];
        [self.contentView addSubview:self.checkmarkLabel];
        
        // LAYOUT
        
        // Layout CONTAINER VIEW
        self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[containerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[containerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:self.adjustableViewConstraints];
        
        // Layout TITLE LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(20)-[titleLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[titleLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ADDRESS LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(20)-[addressLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[addressLabel]-(10)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout CHECKMARK LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[checkmarkLabel]-(20)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[checkmarkLabel]-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config CONTAINER VIEW
        
        // Config TITLE LABEL
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        
        // Config ADDRESS LABEL
        self.addressLabel.textAlignment = NSTextAlignmentLeft;
        
        // Config CHECKMARK LABEL
        self.checkmarkLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaCheck] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGreen68x157x68], NSFontAttributeName:[SWFont fontAwesomeOfSize:20]}];
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.containerView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end

