//
//  SWDateFlowCollectionViewCell.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAddMessageToReportCollectionViewCellForLabel2 : UICollectionViewCell
- (void)addMessageToReportCollectionViewCellForLabel2SetLabel2AttributedString:(NSAttributedString *)date;
@end
