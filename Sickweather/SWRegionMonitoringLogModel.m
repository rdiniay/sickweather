//
//  SWRegionMonitoringLogModel.m
//  Sickweather
//
//  Created by John Erck on 11/3/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWRegionMonitoringLogModel.h"

@implementation SWRegionMonitoringLogModel

+ (SWRegionMonitoringLogModel *)sharedInstance
{
    static SWRegionMonitoringLogModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SWRegionMonitoringLogModel alloc] init];
        // Do any other initialisation stuff here
        sharedInstance.lastLoadedCommaSeparatedGeofenceIllnessIDs = @"";
    });
    return sharedInstance;

}

@end
