//
//  SWNewMapViewController.m
//  Sickweather
//
//  Created by John Erck on 7/25/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

// SDK IMPORTS
#import <MapKit/MapKit.h>

// MY OWN HEADER FILE
#import "SWNewMapViewController.h"

// CUSTOM ANNOTATIONS
#import "SWCustomMapKitAnnotation.h"
#import "SWPrivateReportMapKitAnnotation.h"
#import "SWSponsoredMarkerMapKitAnnotation.h"

// CUSTOM ANNOTATION VIEWS
#import "SWCustomMapKitAnnotationView.h"
#import "SWPrivateReportMapKitAnnotationView.h"
#import "SWSponsoredMarkerMapKitAnnotationView.h"

// CUSTOM, SEMI-SUSPECT, NAV CONTROLLER
#import "SWNavigationController.h" // Maybe get rid of this??

// TAP MENU, SHOW LIST OF ILLNESSES VIEW CONTROLLER/PICKER
#import "SWIllnessPickerViewController.h"

// TAP CLOUD, SHOW NEW ILLNESS DETAIL VIEW CONTROLLER
#import "SWNewIllnessDetailViewController.h"

@interface SWNewMapViewController () <MKMapViewDelegate, SWIllnessPickerDelegate>

// VIEWS
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) UILabel *annotationCountDevLabel;
@property (strong, nonatomic) UILabel *annotationTypeNamesDevLabel;
@property (strong, nonatomic) UIButton *currentLocationButton;

// DATA
@property (strong, nonatomic) NSArray *illnessIDs;
@property (strong, nonatomic) CLLocation *mapLastRefreshLocation;
@property (assign) BOOL initalMarkerZoomHasTakenPlace;

@end

@implementation SWNewMapViewController

#pragma mark - Public Methods

- (void)updateForMyReports:(CLLocation *)location{
	self.mapLastRefreshLocation = location;
    NSString * objectFromDocsUsingName = self.isCovidScore ? @"trending_keywords" : @"illnesses";
    [self sicknessTypeIdSelected:@"myReports" objectFromDocsUsingName: objectFromDocsUsingName];
	self.initalMarkerZoomHasTakenPlace = YES;
}

- (void)updateForIllnessIDs:(NSArray *)illnessIDs andLocation:(CLLocation *)location
{
    // LOG FOR LATER REFERENCE
    self.illnessIDs = illnessIDs;
    self.mapLastRefreshLocation = location;
	
    // HIT THE NETWORK LOOKING FOR REPORTS NEAR THE MAP'S CENTER LOCATION
    if (self.isCovidScore) {
        
        [[SWHelper helperAppBackend] appBackendGetCovidMarkersInRadiusForIllnessTypeID:[self.illnessIDs componentsJoinedByString:@","] lat:location.coordinate.latitude lon:location.coordinate.longitude limit:@(100) usingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            
            // RIGHT AWAY SWITCH BACK TO MAIN THREAD FOR PROCESSING
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString * objectFromDocsUsingName = @"trending_keywords";
                [self parseMarkersInRadiusWithIllnessIDs:illnessIDs jsonResponseBody:jsonResponseBody objectFromDocsUsingName: objectFromDocsUsingName];
            });
        }];
    }
    else {
        [[SWHelper helperAppBackend] appBackendGetMarkersInRadiusForIllnessTypeID:[self.illnessIDs componentsJoinedByString:@","] lat:location.coordinate.latitude lon:location.coordinate.longitude limit:@(100) usingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            
            // RIGHT AWAY SWITCH BACK TO MAIN THREAD FOR PROCESSING
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString * objectFromDocsUsingName = @"illnesses";
                [self parseMarkersInRadiusWithIllnessIDs:illnessIDs jsonResponseBody:jsonResponseBody objectFromDocsUsingName: objectFromDocsUsingName];
            });
        }];
    }
}

-(void)parseMarkersInRadiusWithIllnessIDs: (NSArray*)illnessIDs
                         jsonResponseBody:(NSArray *)jsonResponseBody
                          objectFromDocsUsingName: (NSString *)objectFromDocsUsingName {

    // DUMP FOR DEBUG
    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
    
    // NOW, ITERATE THROUGH EACH OF THE REPORTS RETURNED TO US BY THE SERVER
    if (jsonResponseBody && [jsonResponseBody isKindOfClass:[NSArray class]]){
        for (NSDictionary *report in jsonResponseBody)
            {
            @try{
                if ([report isKindOfClass:[NSDictionary class]]){
                        // DROP USER LOCATION PREDICATE
                    NSPredicate *dropUserLocationPredicate = [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
                        return ![object isKindOfClass:[MKUserLocation class]];
                    }];
                    NSArray *cleanAnnotations = [self.mapView.annotations filteredArrayUsingPredicate:dropUserLocationPredicate];
                    if ([report objectForKey:@"id"] != nil && ![[report objectForKey:@"id"] isEqual:[NSNull null]]){
                            // CHECK TO SEE IF THIS REPORT IS ALREADY ON THE MAP
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier==%@",[report objectForKey:@"id"]];
                        if (predicate != nil) {
                            NSArray *results = [cleanAnnotations filteredArrayUsingPredicate:predicate];
                            if ([results count] == 0)
                                {
                                    // REPORT NOT YET ON MAP YET, TIME TO ADD
                                    //NSLog(@"results = %@", results);
                                
                                    // ADD TO MAP
                                    [[self mapView] addAnnotation:[SWNewMapViewController classHelperGetAnnotationForReportDict:report objectFromDocsUsingName:objectFromDocsUsingName]];
                                }
                        }
                    }
                }
            }@catch (NSException * e) {
                [SWHelper addNonFatalExceptionWith:e];
            }
        }
    }
    
    // UPDATE LABEL WITH NEW UPDATED COUNT
    self.annotationCountDevLabel.text = [@([self.mapView.annotations count]) stringValue];
    
    // UPDATE ILLNESS MAP TITLE/NAME
    self.annotationTypeNamesDevLabel.text = [SWHelper helperIllnessNameForId:[illnessIDs componentsJoinedByString:@","] objectFromDocsUsingName:objectFromDocsUsingName];
    
    // DO THE INITIAL ZOOM IF NEEDED
    if (self.initalMarkerZoomHasTakenPlace == YES)
    {
        // NOTHING TO DO THIS TIME AROUND...
    }
    else
    {
        // SET INITIAL ZOOM BASED ON FIRST ROUND OF FETCHED MARKERS!!
        
        // DROP USER LOCATION PREDICATE
        NSPredicate *dropUserLocationPredicate = [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
            return ![object isKindOfClass:[MKUserLocation class]];
        }];
        NSArray *cleanAnnotations = [self.mapView.annotations filteredArrayUsingPredicate:dropUserLocationPredicate];
        [self.mapView showAnnotations:cleanAnnotations animated:YES];
        
        // FLAG THAT WE DONE DOING THIS STUFF NOW
        self.initalMarkerZoomHasTakenPlace = YES;
    }
}

+ (NSObject <MKAnnotation>*)classHelperGetAnnotationForReportDict:(NSDictionary *)report objectFromDocsUsingName: (NSString *) objectFromDocsUsingName
{
    /*
     GET USER REPORTS
     getUserReports.php REPORT OBJECT
     {
     "id": "410374",
     "illness_id": "4",
     "display_word": "Common Cold",
     "user_id": 138128,
     "feed_text": "sick | sick",
     "lat": "31.92239952087",
     "lon": "-87.75298309326",
     "posted_time": "2017-05-03 13:40:33"
     }
     
     GET MARKERS IN RADIUS
     getMarkersInRadius.php REPORT OBJECT
     {
     "id": "27127186",
     "illness": "4",
     "illness_word": "common cold",
     "lat": "31.92239952087",
     "lon": "-87.75298309326",
     "timestamp": "05/03/2017 01:43:26 PM",
     "user_id": "138128",
     "visible": "0"
     }
     */
    
    /* NORMALIZE "GET USER REPORT" TO "GET MARKER IN RADIUS REPORT" */
    
    if ([report objectForKey:@"illness_id"])
    {
        // ^ THIS IS OUR INDICATOR THAT WE ARE WORKING WITH A "MY REPORTS" REPORT OBJECT
        NSMutableDictionary *normalizedReport = [[NSMutableDictionary alloc] init];
        
        // id
        if ([report objectForKey:@"id"])
        {
            [normalizedReport setObject:[report objectForKey:@"id"] forKey:@"id"];
        }
        
        // illness
        if ([report objectForKey:@"illness_id"])
        {
            [normalizedReport setObject:[report objectForKey:@"illness_id"] forKey:@"illness"];
        }
        
        // illness_word
        if ([report objectForKey:@"display_word"])
        {
            [normalizedReport setObject:[report objectForKey:@"display_word"] forKey:@"illness_word"]; // May need to lowercase this.. TBD...
        }
        
        // lat
        if ([report objectForKey:@"lat"])
        {
            [normalizedReport setObject:[report objectForKey:@"lat"] forKey:@"lat"];
        }
        
        // lon
        if ([report objectForKey:@"lon"])
        {
            [normalizedReport setObject:[report objectForKey:@"lon"] forKey:@"lon"];
        }
        
        // timestamp (e.g. "timestamp": "05/03/2017 01:43:26 PM",)
        if ([report objectForKey:@"posted_time"]) // (e.g. "posted_time": "2017-05-03 13:40:33")
        {
            // target is: 05/03/2017 01:43:26 PM
            // source is: 2017-05-03 13:40:33
            
            // Create date formattter
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            // Define server source format
            NSString *sourceFormat = @"yyyy-MM-dd HH:mm:ss";
            
            // Convert timestampString to NSDate object
            [dateFormatter setDateFormat:sourceFormat];
            NSDate *date = [dateFormatter dateFromString:[report objectForKey:@"posted_time"]];
            
            // Convert NSDate object to desired output format (test)
            
            // Define server target format
            NSString *targetFormat = @"MM/dd/yyyy hh:mm:ss a";
            [dateFormatter setDateFormat:targetFormat];
            NSString *formattedDateOutputTest = [dateFormatter stringFromDate:date];
            //NSLog(@"date full circle back to string: %@", formattedDateOutputTest);
            
            [normalizedReport setObject:formattedDateOutputTest forKey:@"timestamp"];
        }
        
        // user_id
        if ([report objectForKey:@"user_id"])
        {
            [normalizedReport setObject:[[report objectForKey:@"user_id"] stringValue] forKey:@"user_id"];
        }
        
        // visible
        [normalizedReport setObject:@"0" forKey:@"visible"];
        if ([report objectForKey:@"visible"])
        {
            [normalizedReport setObject:[report objectForKey:@"visible"] forKey:@"visible"];
        }
        
        // Assign our normailzed to "getMarkersInRadius.php REPORT OBJECT" to our main variable...
        report = normalizedReport;
    }
    else
    {
        // WE ARE GOOD TO GO, ALREADY WORKING WITH A "GET MARKERS IN RADIUS" REPORT OBJECT
    }
    
    // PHP to Objective C date format translation
    // m/d/Y h:i:s A
    // MM/d/yyyy h:m:s a          note: d = d, h = h, s = s
    
    // GET FORMATTED ILLNESS WORD
    NSString *formattedIllnessWord = [SWHelper helperIllnessNameForId:[report objectForKey:@"illness"] objectFromDocsUsingName: objectFromDocsUsingName];
    
    // GET TIMESTAMP AS STRING
    NSString *timestampString = [report objectForKey:@"timestamp"];
    
    // CREATE DATE FORMATTER
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // DEFINE SERVER DATE FORMAT
    NSString *serverDateFormat = @"MM/dd/yyyy hh:mm:ss a";
	
	
	NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
	NSRange containsA = [formatStringForHours rangeOfString:@"a"];
	if (containsA.location == NSNotFound) {
		timestampString = [timestampString stringByReplacingOccurrencesOfString:@" AM" withString:@""];
		timestampString = [timestampString stringByReplacingOccurrencesOfString:@" PM" withString:@""];
	}
	
    // CONVERT STRING TIME TO NSDATE
    [dateFormatter setDateFormat:serverDateFormat];
    NSTimeZone *tz = [NSTimeZone timeZoneWithAbbreviation:@"UTC"]; // INIT WITH SERVER TIMEZONE
    [dateFormatter setTimeZone:tz];
    NSDate *date = [dateFormatter dateFromString:timestampString]; // GET OUR DATE OBJECT
    
    // NOW THAT WE HAVE OUR CORRECT DATE OBJ, UPDATE FORMATTER FOR USER'S LOCAL TIMEZONE
    tz = [NSTimeZone localTimeZone]; // US WEST COAST FOR JRE
    [dateFormatter setTimeZone:tz];
    
    // FULL CIRCLE FORMATTING TEST
    //NSString *formattedDateOutputTest = [dateFormatter stringFromDate:date];
    //NSLog(@"date full circle back to string: %@", formattedDateOutputTest);
    
    // GET FORMATTED DATE
    [dateFormatter setDateFormat:@"MMM d"]; // Oct 16
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    // GET FORMATTED TIME
    [dateFormatter setDateFormat:@"h:mm a"]; // 4:23 pm
    NSString *formattedTime = [dateFormatter stringFromDate:date];
    
    // SET TITLE
    NSString *title = [NSString stringWithFormat:@"%@ reported", formattedIllnessWord];
    
    // SET SUBTITLE
    NSString *subtitle = [NSString stringWithFormat:@"%@ @ %@", formattedDate, formattedTime];
    
    // CREATE 2D PIN COORDINATE
    CLLocationCoordinate2D pinLocation = CLLocationCoordinate2DMake([[report objectForKey:@"lat"] doubleValue], [[report objectForKey:@"lon"] doubleValue]);
    
    // CREATE MARKER BASED ON TYPE...
    NSObject <MKAnnotation> *markerAnno = nil;
    if ([[report objectForKey:@"visible"] isEqualToString:@"1"])
    {
        // PUBLIC
        SWCustomMapKitAnnotation *publicReportMapKitAnnotation = [[SWCustomMapKitAnnotation alloc] initWithCoordinate:pinLocation identifier:[report objectForKey:@"id"] title:title subtitle:subtitle typeId:[report objectForKey:@"illness"]];
        publicReportMapKitAnnotation.report = report;
        publicReportMapKitAnnotation.reportDate = date;
        
        // SET RETURN VAR
        markerAnno = publicReportMapKitAnnotation;
    }
    else
    {
        // PRIVATE
        NSString *currentUserSWId = [[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]];
        //NSLog(@"currentUserSWId = %@", currentUserSWId);
        //NSLog(@"report = %@", report);
        if ([currentUserSWId length] > 0 && [[report objectForKey:@"user_id"] isEqualToString:currentUserSWId])
        {
            SWPrivateReportMapKitAnnotation *privateReportMapKitAnnotation = [[SWPrivateReportMapKitAnnotation alloc] initWithCoordinate:pinLocation identifier:[report objectForKey:@"id"] title:title subtitle:subtitle typeId:[report objectForKey:@"illness"]];
            privateReportMapKitAnnotation.report = report;
            privateReportMapKitAnnotation.reportDate = date;
            
            // SET RETURN VAR
            markerAnno = privateReportMapKitAnnotation;
        }
    }
    return markerAnno;
}

#pragma mark - MKMapViewDelegate

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // The annotation view to display for the specified annotation or nil if you want to display a standard annotation view.
    
    // Check and see if we're looking at the user's current location dot
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        // Then don't trample the user location annotation (pulsing blue dot).
        return nil;
    }
    
    // The annotation view objects act like cells in a tableview. When off screen,
    // they are added to a queue waiting to be reused. This code mirrors that for
    // getting a table cell. First, check if the queue has available annotation views
    // of the right type, identified by the identifier string. If nil is returned,
    // then allocate a new annotation view.
    
    // Define return var
    MKAnnotationView *annotationViewToReturn;
    
    // PUBLIC MARKER
    if ([annotation isKindOfClass:[SWCustomMapKitAnnotation class]])
    {
        // Create reuse id (static for performance)
        static NSString *sickweatherCloudAnnotationReuseId = @"sickweatherCloudAnnotationReuseId";
        
        // The result of the call is being cast (SWCustomMapKitAnnotationView *) to the correct
        // view class or else the compiler complains
        SWCustomMapKitAnnotationView *annotationView = (SWCustomMapKitAnnotationView *)[self.mapView
                                                                                        dequeueReusableAnnotationViewWithIdentifier:sickweatherCloudAnnotationReuseId];
        
        // Check for nil
        if(annotationView == nil)
        {
            // Create for the first time since we haven't yet
            if (self.isCovidScore) {
                annotationView = [[SWCustomMapKitAnnotationView alloc] initWithCovidAnnotation:annotation reuseIdentifier:sickweatherCloudAnnotationReuseId];
            }
            else {
                annotationView = [[SWCustomMapKitAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:sickweatherCloudAnnotationReuseId];
            }
        }
        
        // Tapping the pin produces callout
        annotationView.canShowCallout = YES;
        
        // And then finally we return our annotationView to the caller!
        annotationViewToReturn = annotationView;
    }
    
    // PRIVATE MARKER
    else if ([annotation isKindOfClass:[SWPrivateReportMapKitAnnotation class]])
    {
        // Create reuse id (static for performance)
        static NSString *privateReportMapKitAnnotationReuseId = @"privateReportMapKitAnnotationReuseId";
        
        // Dequeue marker
        SWPrivateReportMapKitAnnotationView *annotationView = (SWPrivateReportMapKitAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:privateReportMapKitAnnotationReuseId];
        
        // Check for nil
        if(annotationView == nil)
        {
            // Create for the first time since we haven't yet
            if (self.isCovidScore) {
                annotationView = [[SWPrivateReportMapKitAnnotationView alloc] initWithCovidAnnotation:annotation reuseIdentifier:privateReportMapKitAnnotationReuseId];
            }
            else {
                annotationView = [[SWPrivateReportMapKitAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:privateReportMapKitAnnotationReuseId];
            }
        }
        
        // Tapping the pin produces callout
        annotationView.canShowCallout = YES;
        
        // And then finally we return our annotationView to the caller!
        annotationViewToReturn = annotationView;
    }
    
    // SPONSORED MARKER
    else if ([annotation isKindOfClass:[SWSponsoredMarkerMapKitAnnotation class]])
    {
        // Cast so we can call methods on it
        SWSponsoredMarkerMapKitAnnotation *sponsoredMarkerAnnotation = (SWSponsoredMarkerMapKitAnnotation *)annotation;
        
        // Create reuse id (static for performance)
        static NSString *sponsoredMarkerMapKitAnnotationViewReuseId = @"SWSponsoredMarkerMapKitAnnotationViewReuseId";
        
        // Cast
        SWSponsoredMarkerMapKitAnnotationView *annotationView = (SWSponsoredMarkerMapKitAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:sponsoredMarkerMapKitAnnotationViewReuseId];
        
        // Check
        if(annotationView == nil)
        {
            // Create (for the first time since we haven't yet)
            if (self.isCovidScore) {
               annotationView = [[SWSponsoredMarkerMapKitAnnotationView alloc] initWithCovidAnnotation:annotation reuseIdentifier:sponsoredMarkerMapKitAnnotationViewReuseId];

           }
           else {
               
               annotationView = [[SWSponsoredMarkerMapKitAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:sponsoredMarkerMapKitAnnotationViewReuseId];

           }
        }
        
        // Make sure we don't have any outstanding custom callout views
        UIView *sponsoredMapMarkerAnnotationViewCustomCalloutView = [annotationView viewWithTag:[SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewTag]];
        //NSLog(@"sponsoredMapMarkerAnnotationViewCustomCalloutView = %@", sponsoredMapMarkerAnnotationViewCustomCalloutView);
        [sponsoredMapMarkerAnnotationViewCustomCalloutView removeFromSuperview];
        
        // Set image each time
        UIImage *image = [[SWHelper helperAppBackend] fetchImage:sponsoredMarkerAnnotation.markerImgURL usingCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *newImage = [UIImage imageWithData:data scale:[UIScreen mainScreen].scale];
                annotationView.image = newImage;
                [annotationView sizeToFit];
                annotationView.centerOffset = CGPointMake(0, -newImage.size.height/2);
            });
        }];
        if (image)
        {
            annotationView.image = image;
            [annotationView sizeToFit];
        }
        
        // Tapping the pin produces callout
        annotationView.canShowCallout = NO; // We have a custom one
        
        //annotationView.draggable = YES; // Uncommment to debug using new_sponsor-marker_walgreens-dev-crosshairs-with-outline and NO offset to gather the perfect lat lon coors for development and perfect placement purposes. Search for addSponsoredMarkersForCoordinate to see where you'd set the values based on incoming marker ID to override the web service's inaccurate lat/lon vals.
        
        // And then finally we return our annotationView to the caller!
        annotationViewToReturn = annotationView;
    }
    
    // Serve it up!
    return annotationViewToReturn;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    // PUBLIC
    if ([view isKindOfClass:[SWCustomMapKitAnnotationView class]])
    {
        // GET ANNOTATION OBJECT
        SWCustomMapKitAnnotation *annotation = (SWCustomMapKitAnnotation *)view.annotation;
        
        // SHOW NEW ILLNESS DETAIL VIEW CONTROLLER
        SWNewIllnessDetailViewController *newIllnessDetailVC = [[SWNewIllnessDetailViewController alloc] init];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        newIllnessDetailVC.isCovidScore = self.isCovidScore;
        [newIllnessDetailVC updateForIllnessId:annotation.typeId andLocation:location];
        [self.navigationController pushViewController:newIllnessDetailVC animated:YES];
    }
    // PRIVATE
    if ([view isKindOfClass:[SWPrivateReportMapKitAnnotationView class]])
    {
        // GET ANNOTATION OBJECT
        SWPrivateReportMapKitAnnotation *annotation = (SWPrivateReportMapKitAnnotation *)view.annotation;
        
        // SHOW NEW ILLNESS DETAIL VIEW CONTROLLER
        SWNewIllnessDetailViewController *newIllnessDetailVC = [[SWNewIllnessDetailViewController alloc] init];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        newIllnessDetailVC.isCovidScore = self.isCovidScore;
        [newIllnessDetailVC updateForIllnessId:annotation.typeId andLocation:location];
        [self.navigationController pushViewController:newIllnessDetailVC animated:YES];
    }
    // SPONSORED
    if ([view isKindOfClass:[SWSponsoredMarkerMapKitAnnotationView class]])
    {
        /*
         SWSponsoredMarkerMapKitAnnotation *annotation = (SWSponsoredMarkerMapKitAnnotation *)view.annotation;
         if (control.tag == [SWConstants drivingDirectionsLeftAccessoryControlTag])
         {
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@", annotation.lat, annotation.lon]]];
         }
         else if (control.tag == [SWConstants launchSafariRightAccessoryControlTag])
         {
         [Flurry logEvent:@"Sponsored Marker URL Tapped" withParameters:@{@"url": annotation.sponsoredMarkerTargetURL}];
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:annotation.sponsoredMarkerTargetURL]];
         }
         */
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    NSLog(@"REGION DID CHANGE");
    
    // CHECK, SKIP, IF CENTER OF MAP EXACTLY IN CENTER/STARTING POINT (USER WON'T EVER PUT IT HERE, IT'S AN INIT THING)
    NSLog(@"%@ %@", @(mapView.centerCoordinate.latitude), @(mapView.centerCoordinate.longitude));
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:5];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    if ([[formatter stringFromNumber:[NSNumber numberWithDouble:mapView.centerCoordinate.latitude]] isEqualToString:@"37.13284"] && [[formatter stringFromNumber:[NSNumber numberWithDouble:mapView.centerCoordinate.longitude]] isEqualToString:@"-95.78558"])
    {
        return;
    }
    
    // OR SKIP IF WE ARE ON MY REPORTS
    if ([[self.illnessIDs componentsJoinedByString:@","] isEqualToString:@"myReports"])
    {
        return;
    }
    
    // ELSE, UPDATE AS THEY EXPLORE
    NSLog(@"randolf-iscovidscore: %i", self.isCovidScore);
    [self updateForIllnessIDs:self.illnessIDs andLocation:[[CLLocation alloc] initWithLatitude:mapView.centerCoordinate.latitude longitude:mapView.centerCoordinate.longitude]];
}

// http://stackoverflow.com/questions/1857160/how-can-i-create-a-custom-pin-drop-animation-using-mkannotationview/7045916#7045916
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    // INIT STARTING VAR
    MKAnnotationView *aV;
    for (aV in views)
    {
        // SKIP IF WE ARE LOOKING AT THE USER'S CURRENT LOCATION ANNOTATION
        if ([aV.annotation isKindOfClass:[MKUserLocation class]])
        {
            continue;
        }
        
        // SKIP IF ALREADY VISIBLE
        MKMapPoint point =  MKMapPointForCoordinate(aV.annotation.coordinate);
        if (!MKMapRectContainsPoint(self.mapView.visibleMapRect, point))
        {
            continue;
        }
        
        // DEFINE THE END GAME
        CGRect endFrame = aV.frame;
        
        // MOVE ANNOTATION OUT OF VIEW...
        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - self.view.frame.size.height, aV.frame.size.width, aV.frame.size.height);
        
        // ANIMATE THE DROP
        [UIView animateWithDuration:0.5 delay:0.04*[views indexOfObject:aV] options:UIViewAnimationOptionCurveLinear animations:^{
            
            // SET THE END FRAME
            aV.frame = endFrame;
            
        }completion:^(BOOL finished){
            
            // ANIMATE SQUASH
            if (finished)
            {
                [UIView animateWithDuration:0.05 animations:^{
                    aV.transform = CGAffineTransformMakeScale(1.0, 0.8);
                }completion:^(BOOL finished){
                    [UIView animateWithDuration:0.1 animations:^{
                        aV.transform = CGAffineTransformIdentity;
                    }];
                }];
            }
        }];
    }
}

#pragma mark - Helpers

- (void)addPinToMapForReportServerDict:(NSDictionary *)report
{
    /*
     GET USER REPORTS
     getUserReports.php REPORT OBJECT
     {
     "id": "410374",
     "illness_id": "4",
     "display_word": "Common Cold",
     "user_id": 138128,
     "feed_text": "sick | sick",
     "lat": "31.92239952087",
     "lon": "-87.75298309326",
     "posted_time": "2017-05-03 13:40:33"
     }
     
     GET MARKERS IN RADIUS
     getMarkersInRadius.php REPORT OBJECT
     {
     "id": "27127186",
     "illness": "4",
     "illness_word": "common cold",
     "lat": "31.92239952087",
     "lon": "-87.75298309326",
     "timestamp": "05/03/2017 01:43:26 PM",
     "user_id": "138128",
     "visible": "0"
     }
     */
    
    /* NORMALIZE "GET USER REPORT" TO "GET MARKER IN RADIUS REPORT" */
    
    if ([report objectForKey:@"illness_id"])
    {
        // ^ THIS IS OUR INDICATOR THAT WE ARE WORKING WITH A "MY REPORTS" REPORT OBJECT
        NSMutableDictionary *normalizedReport = [[NSMutableDictionary alloc] init];
        
        // id
        if ([report objectForKey:@"id"])
        {
            [normalizedReport setObject:[report objectForKey:@"id"] forKey:@"id"];
        }
        
        // illness
        if ([report objectForKey:@"illness_id"])
        {
            [normalizedReport setObject:[report objectForKey:@"illness_id"] forKey:@"illness"];
        }
        
        // illness_word
        if ([report objectForKey:@"display_word"])
        {
            [normalizedReport setObject:[report objectForKey:@"display_word"] forKey:@"illness_word"]; // May need to lowercase this.. TBD...
        }
        
        // lat
        if ([report objectForKey:@"lat"])
        {
            [normalizedReport setObject:[report objectForKey:@"lat"] forKey:@"lat"];
        }
        
        // lon
        if ([report objectForKey:@"lon"])
        {
            [normalizedReport setObject:[report objectForKey:@"lon"] forKey:@"lon"];
        }
        
        // timestamp (e.g. "timestamp": "05/03/2017 01:43:26 PM",)
        if ([report objectForKey:@"posted_time"]) // (e.g. "posted_time": "2017-05-03 13:40:33")
        {
            // target is: 05/03/2017 01:43:26 PM
            // source is: 2017-05-03 13:40:33
            
            // Create date formattter
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            // Define server source format
            NSString *sourceFormat = @"yyyy-MM-dd HH:mm:ss";
            
            // Convert timestampString to NSDate object
            [dateFormatter setDateFormat:sourceFormat];
            NSDate *date = [dateFormatter dateFromString:[report objectForKey:@"posted_time"]];
            
            // Convert NSDate object to desired output format (test)
            
            // Define server target format
            NSString *targetFormat = @"MM/dd/yyyy hh:mm:ss a";
            [dateFormatter setDateFormat:targetFormat];
            NSString *formattedDateOutputTest = [dateFormatter stringFromDate:date];
            //NSLog(@"date full circle back to string: %@", formattedDateOutputTest);
            
            [normalizedReport setObject:formattedDateOutputTest forKey:@"timestamp"];
        }
        
        // user_id
        if ([report objectForKey:@"user_id"])
        {
            [normalizedReport setObject:[[report objectForKey:@"user_id"] stringValue] forKey:@"user_id"];
        }
        
        // visible
        [normalizedReport setObject:@"0" forKey:@"visible"];
        if ([report objectForKey:@"visible"])
        {
            [normalizedReport setObject:[report objectForKey:@"visible"] forKey:@"visible"];
        }
        
        // Assign our normailzed to "getMarkersInRadius.php REPORT OBJECT" to our main variable...
        report = normalizedReport;
    }
    else
    {
        // WE ARE GOOD TO GO, ALREADY WORKING WITH A "GET MARKERS IN RADIUS" REPORT OBJECT
    }
    
    // PHP to Objective C date format translation
    // m/d/Y h:i:s A
    // MM/d/yyyy h:m:s a          note: d = d, h = h, s = s
    
    // GET FORMATTED ILLNESS WORD
    NSString *formattedIllnessWord = [SWHelper helperIllnessNameForId:[report objectForKey:@"illness"] objectFromDocsUsingName:@"my-networking-illnesses"];
    
    // GET TIMESTAMP AS STRING
    NSString *timestampString = [report objectForKey:@"timestamp"];
    
    // CREATE DATE FORMATTER
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // DEFINE SERVER DATE FORMAT
    NSString *serverDateFormat = @"MM/dd/yyyy hh:mm:ss a";
    
    // CONVERT STRING TIME TO NSDATE
    [dateFormatter setDateFormat:serverDateFormat];
    NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"US/Eastern"]; // INIT WITH SERVER TIMEZONE
    [dateFormatter setTimeZone:tz];
    NSDate *date = [dateFormatter dateFromString:timestampString]; // GET OUR DATE OBJECT
    
    // NOW THAT WE HAVE OUR CORRECT DATE OBJ, UPDATE FORMATTER FOR USER'S LOCAL TIMEZONE
    tz = [NSTimeZone systemTimeZone]; // US WEST COAST FOR JRE
    [dateFormatter setTimeZone:tz];
    
    // FULL CIRCLE FORMATTING TEST
    //NSString *formattedDateOutputTest = [dateFormatter stringFromDate:date];
    //NSLog(@"date full circle back to string: %@", formattedDateOutputTest);
    
    // GET FORMATTED DATE
    [dateFormatter setDateFormat:@"MMM d"]; // Oct 16
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    // GET FORMATTED TIME
    [dateFormatter setDateFormat:@"h:mm a"]; // 4:23 pm
    NSString *formattedTime = [dateFormatter stringFromDate:date];
    
    // SET TITLE
    NSString *title = [NSString stringWithFormat:@"%@ reported", formattedIllnessWord];
    
    // SET SUBTITLE
    NSString *subtitle = [NSString stringWithFormat:@"%@ @ %@", formattedDate, formattedTime];
    
    // CREATE 2D PIN COORDINATE
    CLLocationCoordinate2D pinLocation = CLLocationCoordinate2DMake([[report objectForKey:@"lat"] doubleValue], [[report objectForKey:@"lon"] doubleValue]);
    
    // CREATE MARKER BASED ON TYPE...
    if ([[report objectForKey:@"visible"] isEqualToString:@"1"])
    {
        // PUBLIC
        SWCustomMapKitAnnotation *publicReportMapKitAnnotation = [[SWCustomMapKitAnnotation alloc] initWithCoordinate:pinLocation identifier:[report objectForKey:@"id"] title:title subtitle:subtitle typeId:[report objectForKey:@"illness"]];
        publicReportMapKitAnnotation.report = report;
        publicReportMapKitAnnotation.reportDate = date;
        
        // ADD TO MAP
        [[self mapView] addAnnotation:publicReportMapKitAnnotation];
    }
    else
    {
        // PRIVATE
        NSString *currentUserSWId = [[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]];
        //NSLog(@"currentUserSWId = %@", currentUserSWId);
        //NSLog(@"report = %@", report);
        if ([currentUserSWId length] > 0 && [[report objectForKey:@"user_id"] isEqualToString:currentUserSWId])
        {
            SWPrivateReportMapKitAnnotation *privateReportMapKitAnnotation = [[SWPrivateReportMapKitAnnotation alloc] initWithCoordinate:pinLocation identifier:[report objectForKey:@"id"] title:title subtitle:subtitle typeId:[report objectForKey:@"illness"]];
            privateReportMapKitAnnotation.report = report;
            privateReportMapKitAnnotation.reportDate = date;
            
            // ADD TO MAP
            [[self mapView] addAnnotation:privateReportMapKitAnnotation];
        }
    }
}

#pragma mark - Action Methods

- (void)countLabelTapped:(id)sender
{
    [self.mapView showAnnotations:self.mapView.annotations animated:YES];
}

- (void)nameLabelTapped:(id)sender
{
    [self upperRightBarButtonTapped:self];
}

- (void)upperRightBarButtonTapped:(id)sender
{
    UINavigationController *navVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SWIllnessPickerViewControllerNavigationController"];
    SWIllnessPickerViewController *vc = (SWIllnessPickerViewController *)[[navVC viewControllers] objectAtIndex:0];
    vc.delegate = self; // Set self as destination's delegate
    vc.isCovidScore = self.isCovidScore;
    vc.selectedSicknessTypeId = [self.illnessIDs componentsJoinedByString:@","];
    [self presentViewController:navVC animated:YES completion:^{}];
}

- (void)currentLocationButtonTapped:(id)sender
{
    // STEP 1: SEE IF WE HAVE EVER ASKED
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        // FIRST TIME USER SCENARIO, START BY ASKING JUST FOR WHEN IN USE
        [[[SWHelper helperAppDelegate] locationManager] requestWhenInUseAuthorization];
    }
    // STEP 2: SEE IF WE'VE BEEN FULLY DENIED
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        // YOU SIMPLY CANNOT ASK AGAIN ONCE YOU HAVE ASKED THE ONE TIME. YOU ARE DONE.
        //[[[SWHelper helperAppDelegate] locationManager] requestWhenInUseAuthorization]; <-- TE
        
        // FOR SOME REASON, THE USER HAS DECIDED TO REJECT US ACCESS TO THEIR LOCATION
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Turn Location On" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:actionOK];
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:^{}];
    }
    // STEP 3: SEE IF WE ONLY HAVE WHEN IN USE
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        // THIS IS IT, THIS IS OUR CHANCE TO NOW GO FOR ALWAYS ACCESS
        [[[SWHelper helperAppDelegate] locationManager] requestAlwaysAuthorization];
    }
    // STEP 4: CHECK AND SEE IF WE ARE ALREADY RUNNING ON ALWAYS
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // BOOM SUCKA! IT'S ALREADY ONLINE!
    }
    
    // IF WE HAVE A LOCAITON, GO FOR IT
    if ([[SWHelper helperLocationManager].location isKindOfClass:[CLLocation class]])
    {
        [self.mapView setCenterCoordinate:[SWHelper helperLocationManager].location.coordinate animated:YES];
    }
}

#pragma mark - SWIllnessPickerDelegate

- (void)sicknessTypeIdSelected:(NSString *)sicknessTypeId objectFromDocsUsingName: (NSString *) objectFromDocsUsingName
{
    NSLog(@"sicknessTypeId = %@", sicknessTypeId);
    [self.mapView removeAnnotations:self.mapView.annotations];
    if ([sicknessTypeId isEqualToString:@"myReports"])
    {
        // LOG
        self.illnessIDs = @[sicknessTypeId];
        
        // DO MY REPORTS
        [[SWHelper helperAppBackend] appBackendGetSelfReportsUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // DUMP FOR DEBUG
                NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                
                // NOW, ITERATE THROUGH EACH OF THE REPORTS RETURNED TO US BY THE SERVER
				NSArray* reports = [jsonResponseBody objectForKey:@"reports"];
                for (NSDictionary *report in reports)
                {
                    NSLog(@"report = %@", report);
                    
                    // DROP USER LOCATION PREDICATE
                    NSPredicate *dropUserLocationPredicate = [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
                        return ![object isKindOfClass:[MKUserLocation class]];
                    }];
                    NSArray *cleanAnnotations = [self.mapView.annotations filteredArrayUsingPredicate:dropUserLocationPredicate];
                    
                    // CHECK TO SEE IF THIS REPORT IS ALREADY ON THE MAP
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier==%@",[report objectForKey:@"id"]];
                    NSArray *results = [cleanAnnotations filteredArrayUsingPredicate:predicate];
                    if ([results count] == 0)
                    {
                        // REPORT NOT YET ON MAP YET, TIME TO ADD
                        //NSLog(@"results = %@", results);
                        
                        // ADD TO MAP
						NSObject <MKAnnotation> *markerAnnotation = [SWNewMapViewController classHelperGetAnnotationForReportDict:report objectFromDocsUsingName:objectFromDocsUsingName];
						[[self mapView] addAnnotation:markerAnnotation];
					
						// CENTER MAP ON PIN IF IS THE FIRST RESULT (NEAREST TO GIVEN LOCATION)
						if (self.isFromIllnessReport && [reports indexOfObject:report] == 0)
							{
								// CENTER MAP ON IT
							[self.mapView setCenterCoordinate:markerAnnotation.coordinate animated:YES];
							
								// AUTO-SELECT IT FOR THE UI
							[self.mapView selectAnnotation:markerAnnotation animated:YES];
							}
						}
                    }
				

                
                // UPDATE LABEL WITH NEW UPDATED COUNT
                self.annotationCountDevLabel.text = [@([self.mapView.annotations count]) stringValue];
                
                // UPDATE ILLNESS MAP TITLE/NAME
                self.annotationTypeNamesDevLabel.text = @"My Reports";
                
                // DO THE INITIAL ZOOM IF NEEDED
                if (self.initalMarkerZoomHasTakenPlace == YES)
                {
                    // NOTHING TO DO THIS TIME AROUND...
                }
                else
                {
                    // SET INITIAL ZOOM BASED ON FIRST ROUND OF FETCHED MARKERS!!
                    
                    // DROP USER LOCATION PREDICATE
                    NSPredicate *dropUserLocationPredicate = [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
                        return ![object isKindOfClass:[MKUserLocation class]];
                    }];
                    NSArray *cleanAnnotations = [self.mapView.annotations filteredArrayUsingPredicate:dropUserLocationPredicate];
                    [self.mapView showAnnotations:cleanAnnotations animated:YES];
                    
                    // FLAG THAT WE DONE DOING THIS STUFF NOW
                    self.initalMarkerZoomHasTakenPlace = YES;
                }
            });
        }];
    }
    else
    {
        // DO ILLNESS CALL
        [self updateForIllnessIDs:[sicknessTypeId componentsSeparatedByString:@","] andLocation:[[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude]];
    }
}

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	[self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Life Cycle Methods

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.mapView = [[MKMapView alloc] init];
    self.annotationCountDevLabel = [[UILabel alloc] init];
    self.annotationTypeNamesDevLabel = [[UILabel alloc] init];
    self.currentLocationButton = [[UIButton alloc] init];
	if (self.isFromIllnessReport) {
		// Config LEFT BAR BUTTON ITEM
		self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	}
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.mapView forKey:@"mapView"];
    [self.viewsDictionary setObject:self.annotationCountDevLabel forKey:@"annotationCountDevLabel"];
    [self.viewsDictionary setObject:self.annotationTypeNamesDevLabel forKey:@"annotationTypeNamesDevLabel"];
    [self.viewsDictionary setObject:self.currentLocationButton forKey:@"currentLocationButton"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.mapView.translatesAutoresizingMaskIntoConstraints = NO;
    self.annotationCountDevLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.annotationTypeNamesDevLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.currentLocationButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.mapView];
    [self.view addSubview:self.annotationCountDevLabel];
    [self.view addSubview:self.annotationTypeNamesDevLabel];
    [self.view addSubview:self.currentLocationButton];
    
    // LAYOUT
    
    // mapView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[mapView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[mapView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // annotationCountDevLabel
    if (@available(iOS 11.0, *))
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.annotationCountDevLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
    }
    else
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.annotationCountDevLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
    }
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[annotationCountDevLabel(40)]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[annotationCountDevLabel(40)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // annotationTypeNamesDevLabel
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[annotationCountDevLabel]-(10)-[annotationTypeNamesDevLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    if (@available(iOS 11.0, *))
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.annotationTypeNamesDevLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10]];
    }
    else
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.annotationTypeNamesDevLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10]];
    }
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[annotationTypeNamesDevLabel(40)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // currentLocationButton
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[currentLocationButton(40)]"] options:0 metrics:0 views:self.viewsDictionary]];
    if (@available(iOS 11.0, *))
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.currentLocationButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
    }
    else
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.currentLocationButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
    }
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[currentLocationButton(40)]"] options:0 metrics:0 views:self.viewsDictionary]];
    if (@available(iOS 11.0, *))
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.currentLocationButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-10]];
    }
    else
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.currentLocationButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-60]];
    }
    
    // CONFIG
    
    // self
    self.title = @"Explore the Map";
    self.view.backgroundColor = [UIColor whiteColor];
	
	if (!self.isFromIllnessReport) {
		// navBar
        if (!self.isCovidScore) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon"] landscapeImagePhone:[UIImage imageNamed:@"menu-icon-landscape"] style:UIBarButtonItemStylePlain target:self action:@selector(upperRightBarButtonTapped:)];
        }
	}
    // mapView
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    
    // IF WE HAVE A STARTING LOCAITON AT THIS STAGE, INIT THE MAP WITH OUR BEST GUESS ZOOM
    // WITHOUT ANIMATION, AS A STARITNG POINT (ZOOM WILL UPDATE TO SMART-LEVEL ZOOM AFTER
    // OUR FIRST REQUEST TO FETCH MARKERS).
    if (self.mapLastRefreshLocation)
    {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.mapLastRefreshLocation.coordinate, 10000, 10000);
        [self.mapView setRegion:region animated:NO];
    }
    
    // annotationCountDevLabel
    self.annotationCountDevLabel.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
    self.annotationCountDevLabel.layer.cornerRadius = 20;
    self.annotationCountDevLabel.clipsToBounds = YES;
    self.annotationCountDevLabel.textAlignment = NSTextAlignmentCenter;
    self.annotationCountDevLabel.textColor = [UIColor whiteColor];
    UITapGestureRecognizer *countLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countLabelTapped:)];
    [self.annotationCountDevLabel addGestureRecognizer:countLabelTap];
    self.annotationCountDevLabel.userInteractionEnabled = YES;
    
    // annotationTypeNamesDevLabel
    self.annotationTypeNamesDevLabel.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
    self.annotationTypeNamesDevLabel.layer.cornerRadius = 20;
    self.annotationTypeNamesDevLabel.clipsToBounds = YES;
    self.annotationTypeNamesDevLabel.textAlignment = NSTextAlignmentCenter;
    self.annotationTypeNamesDevLabel.textColor = [UIColor whiteColor];
	if (!self.isFromIllnessReport) {
		UITapGestureRecognizer *nameLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nameLabelTapped:)];
		[self.annotationTypeNamesDevLabel addGestureRecognizer:nameLabelTap];
		self.annotationTypeNamesDevLabel.userInteractionEnabled = YES;
	}
    // currentLocationButton
    self.currentLocationButton.backgroundColor = [UIColor whiteColor];
    [self.currentLocationButton setImage:[UIImage imageNamed:@"location-icon"] forState:UIControlStateNormal];
    self.currentLocationButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    self.currentLocationButton.layer.cornerRadius = 10;
    [self.currentLocationButton addTarget:self action:@selector(currentLocationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
}

@end
