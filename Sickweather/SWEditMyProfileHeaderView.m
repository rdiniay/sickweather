//
//  SWEditMyProfileHeaderView.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWEditMyProfileHeaderView.h"

#define PROFILE_PIC_WIDTH_HEIGHT 100

@interface SWEditMyProfileHeaderView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *profilePicImageView;
@property (strong, nonatomic) UIView *dividerLineView;
@end

@implementation SWEditMyProfileHeaderView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWEditMyProfileHeaderViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    if ([self.delegate editMyProfileHeaderViewProfilePicImage:self])
    {
        self.profilePicImageView.image = [self.delegate editMyProfileHeaderViewProfilePicImage:self];
    }
    self.firstNameTextField.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate editMyProfileHeaderViewFirstNameString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
    self.lastNameTextField.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate editMyProfileHeaderViewLastNameString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
}

#pragma mark - Public Methods

- (void)setProfilePicImage:(UIImage *)image
{
    self.profilePicImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.profilePicImageView.image = image;
}

- (UIImage *)profilePicImage
{
    return self.profilePicImageView.image;
}

#pragma mark - Target Action

- (void)profilePicImageViewTapped:(id)sender
{
    [self.delegate editMyProfileHeaderViewProfilePicTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.profilePicImageView = [[UIImageView alloc] init];
        self.firstNameTextField = [[UITextField alloc] init];
        self.dividerLineView = [[UIView alloc] init];
        self.lastNameTextField = [[UITextField alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.profilePicImageView forKey:@"profilePicImageView"];
        [self.viewsDictionary setObject:self.firstNameTextField forKey:@"firstNameTextField"];
        [self.viewsDictionary setObject:self.dividerLineView forKey:@"dividerLineView"];
        [self.viewsDictionary setObject:self.lastNameTextField forKey:@"lastNameTextField"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.profilePicImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.firstNameTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.dividerLineView.translatesAutoresizingMaskIntoConstraints = NO;
        self.lastNameTextField.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.profilePicImageView];
        [self addSubview:self.firstNameTextField];
        [self addSubview:self.dividerLineView];
        [self addSubview:self.lastNameTextField];
        
        // LAYOUT
        
        // Layout PROFILE PIC IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[profilePicImageView(wh)]"] options:0 metrics:@{@"wh":@PROFILE_PIC_WIDTH_HEIGHT} views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[profilePicImageView(wh)]"] options:0 metrics:@{@"wh":@PROFILE_PIC_WIDTH_HEIGHT} views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.profilePicImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0
                                                          constant:0.0]];
        
        // Layout FIRST NAME TEXT FIELD
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[profilePicImageView]-(15)-[firstNameTextField]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[firstNameTextField]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.firstNameTextField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:0.5 constant:0.0]];
        
        // Layout DIVIDER LINE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[profilePicImageView]-(15)-[dividerLineView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[dividerLineView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.dividerLineView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout LAST NAME TEXT FIELD
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[profilePicImageView]-(15)-[lastNameTextField]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[lastNameTextField]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lastNameTextField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:0.5 constant:0.0]];
        
        // CONFIG
        
        // Config SELF
        self.backgroundColor = [UIColor whiteColor];
        
        // Config PROFILE PIC IMAGE VIEW
        self.profilePicImageView.image = [UIImage animatedImageNamed:@"loading-" duration:0.75];
        self.profilePicImageView.contentMode = UIViewContentModeCenter; // For spinner image
        self.profilePicImageView.clipsToBounds = YES;
        self.profilePicImageView.layer.cornerRadius = PROFILE_PIC_WIDTH_HEIGHT/2.0;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePicImageViewTapped:)];
        [self.profilePicImageView addGestureRecognizer:tap];
        self.profilePicImageView.userInteractionEnabled = YES;
        
        // Config FIRST NAME TEXT FIELD
        self.firstNameTextField.textAlignment = NSTextAlignmentLeft;
        self.firstNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.firstNameTextField.placeholder = @"First name";
        
        // Config DIVIDER LINE VIEW
        self.dividerLineView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config LAST NAME TEXT FIELD
        self.lastNameTextField.textAlignment = NSTextAlignmentLeft;
        self.lastNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.lastNameTextField.placeholder = @"Last name";
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.profilePicImageView.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
        //self.firstNameTextField.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
        //self.lastNameTextField.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
    }
    return self;
}

@end
