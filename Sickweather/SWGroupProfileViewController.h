//
//  SWGroupProfileViewController.h
//  Sickweather
//
//  Created by John Erck on 11/16/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SYMPATHY_CELL_HEIGHT 44

@protocol SWGroupProfileViewControllerDelegate <NSObject>
- (void)groupProfileViewControllerWantsToDismiss;
@end

@interface SWGroupProfileViewController : UIViewController
@property (nonatomic, assign) id <SWGroupProfileViewControllerDelegate> delegate;
@property (strong, nonatomic) NSDictionary *serverGroupDict;
@property (strong, nonatomic) NSDictionary *latestMenuServerResponse;
@property (assign) BOOL setAsFollowing;
@property (assign) BOOL setAsAdmin;
- (void)loadForFoursquareId:(NSString *)foursquareId; // simplest method
- (void)setThenLoadGetGroupProfileJSONObject:(NSMutableDictionary *)dict;
@end
