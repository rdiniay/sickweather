//
//  SWWelcomeViewController.m
//  Sickweather
//
//  Created by John Erck on 1/9/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "Flurry.h"
#import "SWWelcomeViewController.h"
#import "SWAppDelegate.h"
#import "SWShareAppWithFriendsTableViewController.h"
#import "Sickweather-Swift.h"

@interface SWWelcomeViewController () <UIScrollViewDelegate, SWShareAppWithFriendsTableViewControllerDelegate, SWWelcomeSliderBottomContainerViewDelegate>
@property (assign, nonatomic) BOOL userIsLoggedInWithFacebook;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIButton *inviteFriendsNowButton;
@property (strong, nonatomic) UIButton *inviteFriendsLaterButton;
@property (strong, nonatomic) UIView *launchScreenView;
@property (assign) BOOL launchScreenViewWithFadeOut;
@property (strong, nonatomic) SWWelcomeSliderBottomContainerView *bottomContainerView1;
@property (strong, nonatomic) SWWelcomeSliderBottomContainerView *bottomContainerView2;
@property (strong, nonatomic) SWWelcomeSliderBottomContainerView *bottomContainerView3;
@property (strong, nonatomic) SWWelcomeSliderBottomContainerView *bottomContainerView4;
@property (strong, nonatomic) SWWelcomeSliderBottomContainerView *bottomContainerView5;
@end

@implementation SWWelcomeViewController

#pragma mark - Public

- (void)loadWithLaunchScreenViewCoverUsingView:(UIView *)view
{
    self.launchScreenViewWithFadeOut = NO;
    self.launchScreenView = view;
}

- (void)loadWithoutLaunchScreenViewCover
{
    self.launchScreenViewWithFadeOut = NO;
    [self.launchScreenView removeFromSuperview];
    self.launchScreenView = nil;
}

- (void)loadWithFadeOutLaunchScreenViewCoverUsingView:(UIView *)view
{
    self.launchScreenViewWithFadeOut = YES;
    self.launchScreenView = view;
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - Custom Getters and Setters

- (BOOL)userIsLoggedInWithFacebook
{
    if (!_userIsLoggedInWithFacebook) {
        _userIsLoggedInWithFacebook = NO;
    }
    return _userIsLoggedInWithFacebook;
}

#pragma mark - SWWelcomeSliderBottomContainerViewDelegate

- (void)mainButtonTouchUpInside:(SWWelcomeSliderBottomContainerView *)view
{
    if ([view isEqual:self.bottomContainerView1])
    {
        //NSLog(@"bottomContainerView1");
        [self scrollViewTapped:self];
    }
    else if ([view isEqual:self.bottomContainerView2])
    {
        //NSLog(@"bottomContainerView2");
        [self scrollViewTapped:self];
    }
    else if ([view isEqual:self.bottomContainerView3])
    {
        //NSLog(@"bottomContainerView3");
        [self scrollViewTapped:self];
    }
    else if ([view isEqual:self.bottomContainerView4])
    {
        //NSLog(@"bottomContainerView4");
        [self scrollViewTapped:self];
    }
    else if ([view isEqual:self.bottomContainerView5])
    {
        [self inviteFriendsNowButtonPressed:self];
    }
}

- (void)altButtonTouchUpInside:(SWWelcomeSliderBottomContainerView *)view
{
    [self inviteFriendsLaterButtonPressed:self];
}

#pragma mark - Helpers

#pragma mark - Target Action

- (void)inviteFriendsNowButtonPressed:(id)sender
{
    [Flurry logEvent:@"Invite Friends" withParameters:@{@"Screen Name": @"Onboarding"}];
    SWShareAppWithFriendsTableViewController *shareAppWithFriendsVC = (SWShareAppWithFriendsTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SWShareAppWithFriendsTableViewController"];
    shareAppWithFriendsVC.delegate = self;
    [self presentViewController:shareAppWithFriendsVC animated:YES completion:^{}];
}

- (void)inviteFriendsLaterButtonPressed:(id)sender
{
    [Flurry logEvent:@"Invite Friends Later"];
    [self.delegate userWantsToDismiss:self];
}

- (void)scrollViewTapped:(id)sender
{
    NSInteger current = self.pageControl.currentPage * self.view.bounds.size.width;
    NSInteger nextContentOffsetX = current + self.view.bounds.size.width;
    if (nextContentOffsetX > self.scrollView.contentSize.width - self.view.bounds.size.width)
    {
        // Then do nothing, they're at the end...
        CGFloat orig = self.scrollView.contentOffset.x;
        [self.scrollView setContentOffset:CGPointMake(orig + 20, 0) animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.scrollView setContentOffset:CGPointMake(orig, 0) animated:YES];
        });
    }
    else
    {
        [self.scrollView setContentOffset:CGPointMake(nextContentOffsetX, 0) animated:YES];
        self.pageControl.currentPage = self.pageControl.currentPage + 1;
    }
}

#pragma mark - SWShareAppWithFriendsTableViewControllerDelegate

- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulMessageSend:(SWShareAppWithFriendsTableViewController *)vc
{
    [self.delegate userWantsToDismiss:self];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismissDueToSuccessfulFacebookRequestSend:(SWShareAppWithFriendsTableViewController *)vc
{
    [self.delegate userWantsToDismiss:self];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismissBySkipping:(SWShareAppWithFriendsTableViewController *)vc
{
    [self.delegate userWantsToDismiss:self];
}

- (void)shareAppWithFriendsTableViewControllerWantsToDismiss:(SWShareAppWithFriendsTableViewController *)vc
{
    [self.delegate userWantsToDismiss:self];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    NSUInteger page = sender.contentOffset.x / self.view.bounds.size.width;
    [self.pageControl setCurrentPage:page];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:[SWConstants hasPassedWelcomeToSickweatherSlideThroughScreensDefaultsKey]])
    {
        return NO;
    }
    return YES;
}

// Support only portrait
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Define sections by percent
    CGFloat topPercent = 0.6;
    CGFloat bottomPercent = 1-topPercent;
    
    // Image views
    UIImageView *slide1;
    UIImageView *slide2;
    UIImageView *slide3;
    UIImageView *slide4;
    UIImageView *slide5;
  
    slide1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1-powered-by-social-media"]];
    slide2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"2-help-your-community"]];
    slide3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"3-forecasts-on-demand"]];
    slide4 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"4-today-widget-apple-watch"]];
    slide5 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"5-invite-friends-family"]];
    
    slide1.contentMode = UIViewContentModeScaleAspectFill;
    slide2.contentMode = UIViewContentModeScaleAspectFill;
    slide3.contentMode = UIViewContentModeScaleAspectFill;
    slide4.contentMode = UIViewContentModeScaleAspectFill;
    slide5.contentMode = UIViewContentModeScaleAspectFill;
    
    slide1.backgroundColor = [UIColor redColor];
    slide2.backgroundColor = [UIColor greenColor];
    slide3.backgroundColor = [UIColor grayColor];
    slide4.backgroundColor = [UIColor orangeColor];
    slide5.backgroundColor = [UIColor blueColor];
    
    slide1.clipsToBounds = YES;
    slide2.clipsToBounds = YES;
    slide3.clipsToBounds = YES;
    slide4.clipsToBounds = YES;
    slide5.clipsToBounds = YES;
    
    // Helper vars
    CGFloat x = [UIScreen mainScreen].bounds.size.width;
    CGFloat y = 0.0;
    CGFloat w = [UIScreen mainScreen].bounds.size.width;
    CGFloat h = [UIScreen mainScreen].bounds.size.height * topPercent;
    
    // Position images
    slide1.frame = CGRectMake(x*0, y, w, h);
    slide2.frame = CGRectMake(x*1, y, w, h);
    slide3.frame = CGRectMake(x*2, y, w, h);
    slide4.frame = CGRectMake(x*3, y, w, h);
    slide5.frame = CGRectMake(x*4, y, w, h);
    
//    SWW
    
    // Scroll view
    UIScrollView *scrollView = [UIScrollView new];
    scrollView.delegate = self;
    scrollView.frame = [[UIScreen mainScreen] bounds];
    scrollView.contentSize = CGSizeMake(w*5, h);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.bounces = NO;
    [scrollView addSubview:slide1];
    [scrollView addSubview:slide2];
    [scrollView addSubview:slide3];
    [scrollView addSubview:slide4];
    [scrollView addSubview:slide5];
    [scrollView setPagingEnabled:YES];
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped:)];
    [self.scrollView addGestureRecognizer:tap];
    
    // Create and add bottom views
    
    self.bottomContainerView1 = [[SWWelcomeSliderBottomContainerView alloc] initWithTitleText:@"Powered by Social Media" bodyText:[NSString stringWithFormat:@"%@\n%@\n%@", @"Sickweather uses the power of social media", @"to detect nearby illnesses in real-time to", @"keep you safe and informed."] buttonText:@"Next" altButtonText:@"" delegate:self frame:CGRectMake(x*0, topPercent*[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, bottomPercent*[UIScreen mainScreen].bounds.size.height)];
    [self.scrollView addSubview:self.bottomContainerView1];
    
    self.bottomContainerView2 = [[SWWelcomeSliderBottomContainerView alloc] initWithTitleText:@"Sharing Is Caring" bodyText:[NSString stringWithFormat:@"%@\n%@\n%@", @"Keep your community healthy by informing", @"others at your favorite locations when you", @"are sick."] buttonText:@"Next" altButtonText:@"" delegate:self frame:CGRectMake(x*1, topPercent*[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, bottomPercent*[UIScreen mainScreen].bounds.size.height)];
    [self.scrollView addSubview:self.bottomContainerView2];
    
    self.bottomContainerView3 = [[SWWelcomeSliderBottomContainerView alloc] initWithTitleText:@"Forecasts on Demand" bodyText:[NSString stringWithFormat:@"%@\n%@", @"Search for Sickweather forecasts around the", @"world to be prepared before you travel."] buttonText:@"Next" altButtonText:@"" delegate:self frame:CGRectMake(x*2, topPercent*[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, bottomPercent*[UIScreen mainScreen].bounds.size.height)];
    [self.scrollView addSubview:self.bottomContainerView3];
    
    self.bottomContainerView4 = [[SWWelcomeSliderBottomContainerView alloc] initWithTitleText:@"Today Widget & Apple Watch" bodyText:[NSString stringWithFormat:@"%@\n%@\n%@", @"Add Sickweather to your iPhone's Today", @"screen or to your Apple Watch to receive", @"real-time SickScores."] buttonText:@"Next" altButtonText:@"" delegate:self frame:CGRectMake(x*3, topPercent*[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, bottomPercent*[UIScreen mainScreen].bounds.size.height)];
    [self.scrollView addSubview:self.bottomContainerView4];
    
    self.bottomContainerView5 = [[SWWelcomeSliderBottomContainerView alloc] initWithTitleText:@"Invite Friends & Family" bodyText:[NSString stringWithFormat:@"%@\n%@", @"Keep your friends and family safe - share the", @"app, not germs."] buttonText:@"Invite Friends" altButtonText:@"Maybe later" delegate:self frame:CGRectMake(x*4, topPercent*[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, bottomPercent*[UIScreen mainScreen].bounds.size.height)];
    [self.scrollView addSubview:self.bottomContainerView5];
    
    // Page control
    self.pageControl = [[UIPageControl alloc] init];
    self.pageControl.pageIndicatorTintColor = [SWColor color:[SWColor colorSickweatherStyleGuideGray79x76x77] usingOpacity:0.3];
    self.pageControl.currentPageIndicatorTintColor = [SWColor colorSickweatherStyleGuideGray79x76x77];
    self.pageControl.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height - 20);
    self.pageControl.numberOfPages = 5;
    self.pageControl.currentPage = 0;
    [self.view addSubview:self.pageControl];
    
    // Add invite friends now button
    self.inviteFriendsNowButton = [[UIButton alloc] init];
    [self.inviteFriendsNowButton addTarget:self action:@selector(inviteFriendsNowButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inviteFriendsNowButton setBackgroundImage:[UIImage imageNamed:@"invite-btn-normal"] forState:UIControlStateNormal];
    [self.inviteFriendsNowButton setBackgroundImage:[UIImage imageNamed:@"invite-btn-down"] forState:UIControlStateApplication];
    [self.inviteFriendsNowButton sizeToFit];
//    [self.inviteFriendsNowButton setCenter:CGPointMake(slide4.frame.origin.x + ([UIScreen mainScreen].bounds.size.width/2), yLabelOffset + slide3Text.bounds.size.height + 40)];
//    [self.scrollView addSubview:self.inviteFriendsNowButton];
    
    // Add invite friends later button
    self.inviteFriendsLaterButton = [[UIButton alloc] init];
    [self.inviteFriendsLaterButton addTarget:self action:@selector(inviteFriendsLaterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inviteFriendsLaterButton setTitle:@"Maybe later" forState:UIControlStateNormal];
    [self.inviteFriendsLaterButton setTitle:@"Maybe later" forState:UIControlStateHighlighted];
    [self.inviteFriendsLaterButton.titleLabel setFont:[SWFont fontStandardFontOfSize:16]];
    [self.inviteFriendsLaterButton setTitleColor:[SWColor colorSickweatherGrayText135x135x135] forState:UIControlStateNormal];
    [self.inviteFriendsLaterButton setTitleColor:[SWColor colorSickweatherDarkGrayText104x104x104] forState:UIControlStateHighlighted];
    [self.inviteFriendsLaterButton sizeToFit];
//    [self.inviteFriendsLaterButton setCenter:CGPointMake(slide4.frame.origin.x + ([UIScreen mainScreen].bounds.size.width/2), yLabelOffset + slide3Text.bounds.size.height + 90)];
//    [self.scrollView addSubview:self.inviteFriendsLaterButton];
    
    // Add if set
    if (self.launchScreenView)
    {
        [self.view addSubview:self.launchScreenView];
        if (self.launchScreenViewWithFadeOut)
        {
            self.launchScreenViewWithFadeOut = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:1.0 delay:0 options:0 animations:^{
                    self.launchScreenView.layer.opacity = 0;
                } completion:^(BOOL finished) {
                    [self.navigationController setNavigationBarHidden:NO animated:YES];
                    [self.launchScreenView removeFromSuperview];
                    self.launchScreenView = nil;
                }];
            });
        }
    }
}

@end
