//
//  main.m
//  Sickweather
//
//  Created by John Erck on 9/20/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SWAppDelegate class]));
    }
}
