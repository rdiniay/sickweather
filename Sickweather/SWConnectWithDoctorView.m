//
//  SWConnectWithDoctorView.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 19/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWConnectWithDoctorView.h"

@implementation SWConnectWithDoctorView

-(void)initialize
{
    // Changing button layouts
    // signUpButton
    [self.signUpButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Sign Up for WellVia" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
    self.signUpButton.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    self.signUpButton.layer.cornerRadius = 5;
}

- (IBAction)signUpPressed:(id)sender {
    [self.delegate signUp];
}



@end
