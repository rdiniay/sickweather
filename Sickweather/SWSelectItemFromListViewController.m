//
//  SWSelectItemFromListViewController.m
//  Sickweather
//
//  Created by John Erck on 10/7/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWSelectItemFromListViewController.h"
#import "SWSelectItemFromListSectionHeaderView.h"
#import "SWSelectItemFromListRowView.h"
#import "SWSelectItemFromListViewController.h"
#import "NSString+FontAwesome.h"

#define rowHeight 50

@interface SWSelectItemFromListViewController () <SWSelectItemFromListRowViewDelegate, SWSelectItemFromListSectionHeaderViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSMutableDictionary *selectItemFromListData; // selectItemFromList.json
@property (weak, nonatomic) UITextField *activeField;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) SWSelectItemFromListSectionHeaderView *contactSectionHeader;
@property (strong, nonatomic) SWSelectItemFromListRowView *emailRow;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UITextField *validationTargetTextField;
@end

@implementation SWSelectItemFromListViewController

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWSelectItemFromListViewControllerDelegate>)delegate
{
    _delegate = delegate;
    [self loadJSONDataUsingFileBaseNameStringWithoutExtension:[delegate selectItemFromListViewControllerJSONFileToUseBaseNameWithoutExtension]];
    [[self.selectItemFromListData objectForKey:@"list"] setObject:[self helperGetRowIndexForSystemValue:[delegate selectItemFromListViewControllerSelectedSystemValue:self]] forKey:@"selected_row_index"];
}

#pragma mark - UIScrollViewDelegate

#pragma mark - SWSelectItemFromListRowViewDelegate

- (NSString *)selectItemFromListRowViewTitleString:(SWSelectItemFromListRowView *)sender
{
    return [self listDataRowNameForSectionIndex:sender.sectionIndex rowIndex:sender.rowIndex];
}

- (void)selectItemFromListRowWasTapped:(SWSelectItemFromListRowView *)sender
{
    [self.delegate selectItemFromListViewController:self userSelectedRowDict:[self listDataRowForSectionIndex:sender.sectionIndex rowIndex:sender.rowIndex]];
    [[self.selectItemFromListData objectForKey:@"list"] setObject:@(sender.sectionIndex) forKey:@"selected_section_index"];
    [[self.selectItemFromListData objectForKey:@"list"] setObject:@(sender.rowIndex) forKey:@"selected_row_index"];
    [self saveJSONDataUsingFileBaseNameStringWithoutExtension:[self.delegate selectItemFromListViewControllerJSONFileToUseBaseNameWithoutExtension]];
}

#pragma mark - SWSelectItemFromListSectionHeaderViewDelegate

- (NSString *)selectItemFromListSectionHeaderViewTitleString:(SWSelectItemFromListSectionHeaderView *)sender
{
    return [self listDataHeaderNameForSectionIndex:sender.sectionIndex];
}

#pragma mark - Target Action

- (void)leftBarButtonItemTapped:(id)sender
{
    return [self.delegate selectItemFromListViewControllerWantsToDismiss:self];
}

#pragma mark - Helpers

- (NSString *)helperGetRowIndexForSystemValue:(NSString *)systeValue
{
    NSString *answer = @"";
    NSUInteger count = 0;
    for (NSDictionary *row in [[[[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:0] objectForKey:@"rows"])
    {
        //NSLog(@"system value = %@", [row objectForKey:@"system_value"]);
        //NSLog(@"systemValue = %@", systeValue);
        if ([[row objectForKey:@"system_value"] isEqualToString:systeValue])
        {
            answer = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)count];
        }
        count++;
    }
    return answer;
}

- (NSString *)listDataHeaderNameForSectionIndex:(NSUInteger)sectionIndex;
{
    return [[[[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"name"];
}

- (NSString *)listDataRowNameForSectionIndex:(NSUInteger)sectionIndex rowIndex:(NSUInteger)rowIndex;
{
    return [[[[[[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"rows"] objectAtIndex:rowIndex] objectForKey:@"name"];
}

- (NSDictionary *)listDataRowForSectionIndex:(NSUInteger)sectionIndex rowIndex:(NSUInteger)rowIndex
{
    return [[[[[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"rows"] objectAtIndex:rowIndex];
}

- (NSString *)listDataRowSystemNameForSectionIndex:(NSUInteger)sectionIndex rowIndex:(NSUInteger)rowIndex
{
    return [[[[[[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"headers"] objectAtIndex:sectionIndex] objectForKey:@"rows"] objectAtIndex:rowIndex] objectForKey:@"system_value"];
}

- (NSString *)listDataGetTitle
{
    return [[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"title"];
}

- (NSNumber *)listDataGetSelectedRowIndex
{
    return [[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"selected_row_index"];
}

- (NSNumber *)listDataGetSelectedSectionIndex
{
    return [[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"selected_section_index"];
}

- (void)loadJSONDataUsingFileBaseNameStringWithoutExtension:(NSString *)fileBaseNameStringWithoutExtension // e.g. selectItemFromList
{
    self.selectItemFromListData = (NSMutableDictionary *)[SWHelper helperLoadJSONObjectFromBundleDocsUsingName:fileBaseNameStringWithoutExtension forceFeedFromBundle:YES];
}

- (void)saveJSONDataUsingFileBaseNameStringWithoutExtension:(NSString *)fileBaseNameStringWithoutExtension // e.g. selectItemFromList
{
    // GET DOCUMENTS PATH
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", fileBaseNameStringWithoutExtension]];
    //NSLog(@"documentsPath %@", documentsPath);
    
    // GET JSON DATA BASED ON self.selectItemFromListData
    NSError *jsonSerializationError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.selectItemFromListData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&jsonSerializationError];
    
    // GET JSON STRING BASED ON JSON DATA
    NSString *jsonString;
    if (!jsonData)
    {
        //NSLog(@"Got an error: %@", jsonSerializationError);
    }
    else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    // SAVE JSON STRING (self.selectItemFromListData) TO DOCUMENTS PATH
    NSError *errorDocumentsPathJSONString;
    [jsonString writeToFile:documentsPath atomically:YES encoding:NSUTF8StringEncoding error:&errorDocumentsPathJSONString];
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT HELPER PROPS
    self.viewsDictionary = [NSMutableDictionary new];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config TITLE
    self.title = [self listDataGetTitle];
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // COLOR FOR DEVELOPMENT PURPOSES
    //    self.scrollView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    //    self.contentView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
    //    self.contactSectionHeader.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
    //    self.emailRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
    
    // HANDLE HEADERS AND ROWS
    NSUInteger currentHeaderIndex = 0;
    NSArray *headers = [[self.selectItemFromListData objectForKey:@"list"] objectForKey:@"headers"];
    NSString *previousRowKey = nil;
    for (NSDictionary *header in headers)
    {
        // GET HEADER INDEX
        NSNumber *headerIndex = [NSNumber numberWithUnsignedInteger:currentHeaderIndex];
        NSString *headerKey = [NSString stringWithFormat:@"header%@", headerIndex];
        BOOL isLastHeader = NO;
        if ([headers count] - 1 == [headerIndex integerValue])
        {
            isLastHeader = YES;
        }
        
        // INIT
        SWSelectItemFromListSectionHeaderView *headerView = [[SWSelectItemFromListSectionHeaderView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        [self.viewsDictionary setObject:headerView forKey:headerKey];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        headerView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:headerView];
        
        // LAYOUT
        
        // Layout HEADER
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[%@]|", headerKey] options:0 metrics:0 views:self.viewsDictionary]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
        if (previousRowKey)
        {
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[%@][%@(rowHeight)]", previousRowKey, headerKey] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
        }
        else
        {
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[%@(rowHeight)]", headerKey] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
        }
        
        // CONFIG
        
        // Config HEADER
        headerView.sectionIndex = [headerIndex integerValue];
        headerView.delegate = self;
        
        // HANDLE SECTION ROWS
        NSUInteger currentRowIndex = 0;
        NSArray *rows = [header objectForKey:@"rows"];
        for (int i = 0; i < rows.count; i++)
        {
            //NSLog(@"row.name = %@", [row objectForKey:@"name"]);
            
            // GET ROW INDEX
            NSNumber *rowIndex = [NSNumber numberWithUnsignedInteger:currentRowIndex];
            //NSLog(@"rowIndex = %@", rowIndex);
            NSString *rowKey = [NSString stringWithFormat:@"row%@_%@", headerIndex, rowIndex];
            //NSLog(@"rowKey = %@", rowKey);
            BOOL isLastRow = [[header objectForKey:@"rows"] count] - 1 == [rowIndex integerValue];
            
            // INIT
            SWSelectItemFromListRowView *rowView = [[SWSelectItemFromListRowView alloc] init];
            
            // INIT AUTO LAYOUT VIEWS DICT
            [self.viewsDictionary setObject:rowView forKey:rowKey];
            
            // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
            rowView.translatesAutoresizingMaskIntoConstraints = NO;
            
            // ESTABLISH VIEW HIERARCHY
            [self.contentView addSubview:rowView];
            
            // LAYOUT
            
            // Layout ROW
            NSString *layoutString = [NSString stringWithFormat:@"H:|[%@]|", rowKey];
            //NSLog(@"layoutString = %@", layoutString);
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:layoutString options:0 metrics:0 views:self.viewsDictionary]];
            if (currentRowIndex == 0 && headerKey)
            {
                layoutString = [NSString stringWithFormat:@"V:[%@]-(0)-[%@(rowHeight)]", headerKey, rowKey];
                //NSLog(@"layoutString = %@", layoutString);
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:layoutString options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
            }
            else if (previousRowKey)
            {
                layoutString = [NSString stringWithFormat:@"V:[%@]-(0)-[%@(rowHeight)]", previousRowKey, rowKey];
                //NSLog(@"layoutString = %@", layoutString);
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:layoutString options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
            }
            else
            {
                // This block shouldn't get executed
            }
            if (isLastRow && isLastHeader)
            {
                layoutString = [NSString stringWithFormat:@"V:[%@]-|", rowKey];
                //NSLog(@"layoutString = %@", layoutString);
                [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:layoutString options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR HEIGHT !!!
            }
            
            // CONFIG
            
            // Config ROW
            rowView.sectionIndex = [headerIndex integerValue];
            rowView.rowIndex = [rowIndex integerValue];
            rowView.delegate = self;
            if ([self listDataGetSelectedSectionIndex].integerValue == [headerIndex integerValue] && [self listDataGetSelectedRowIndex].integerValue == [rowIndex integerValue])
            {
                [rowView showCheckmark];
            }
            
            // UPDATE LOOP VARS
            previousRowKey = rowKey;
            currentRowIndex++;
        }
        
        // UPDATE LOOP VARS
        currentHeaderIndex++;
    }
}

@end
