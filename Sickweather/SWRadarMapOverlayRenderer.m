//
//  SWRadarMapOverlayRenderer.m
//  Sickweather
//
//  Created by John Erck on 5/1/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWRadarMapOverlayRenderer.h"

@interface SWRadarMapOverlayRenderer ()
@property (strong, nonatomic) UIImage *overlayImage;
@end

@implementation SWRadarMapOverlayRenderer

-(id)initWithOverlay:(id<MKOverlay>)overlay usingImage:(UIImage *)overlayImage
{
    self = [super initWithOverlay:overlay];
    if (self) {
        self.overlayImage = overlayImage;
    }
    return self;
}

- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context
{
    CGImageRef imageReference = self.overlayImage.CGImage;
    
    MKMapRect theMapRect = self.overlay.boundingMapRect;
    CGRect theRect = [self rectForMapRect:theMapRect];
    
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0.0, -theRect.size.height);
    CGContextDrawImage(context, theRect, imageReference);
}

@end
