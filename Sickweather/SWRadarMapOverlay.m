//
//  SWRadarMapOverlay.m
//  Sickweather
//
//  Created by John Erck on 5/1/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWRadarMapOverlay.h"

@implementation SWRadarMapOverlay

- (void)setBoundingMapRect:(MKMapRect)boundingMapRect
{
    _boundingMapRect = boundingMapRect;
}

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    _coordinate = coordinate;
}

- (void)setOverlayImageStringURL:(NSString *)overlayImageStringURL
{
    _overlayImageStringURL = overlayImageStringURL;
}

- (id)initUsingBoundingMapRect:(MKMapRect)boundingMapRect andCoordinate:(CLLocationCoordinate2D)coordinate withOverlayImageStringURL:(NSString *)overlayImageStringURL
{
    self = [super init];
    if (self) {
        self.boundingMapRect = boundingMapRect;
        self.coordinate = coordinate;
        self.overlayImageStringURL = overlayImageStringURL;
    }
    return self;
}

@end
