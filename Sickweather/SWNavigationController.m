//
//  SWNavigationController.m
//  Sickweather
//
//  Created by John Erck on 11/13/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWNavigationController.h"

@interface SWNavigationController ()

@end

@implementation SWNavigationController

- (BOOL)shouldAutorotate
{
    return [self.topViewController shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

- (BOOL)prefersStatusBarHidden
{
    //NSLog(@"self.topViewController = %@", self.topViewController);
    return [self.topViewController prefersStatusBarHidden];
}

@end
