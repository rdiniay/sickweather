//
//  SWOnboardingViewController.m
//  Sickweather
//
//  Created by John Erck on 12/20/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWOnboardingViewController.h"
#import "SWOnboardingView.h"

@interface SWOnboardingViewController () <UIScrollViewDelegate>

// UI VIEWS
@property (strong, nonatomic) NSMutableDictionary *myViewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) SWOnboardingView *view0;
@property (strong, nonatomic) SWOnboardingView *view1;
@property (strong, nonatomic) SWOnboardingView *view2;
@property (strong, nonatomic) SWOnboardingView *view3;
@property (strong, nonatomic) SWOnboardingView *view4;
@property (strong, nonatomic) UIImageView *cloudImageView;
@property (strong, nonatomic) UIPageControl *pageControl;

@end

@implementation SWOnboardingViewController

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
    if(page == 4){
        self.navigationItem.rightBarButtonItem.title = @"Done";
    } else {
        self.navigationItem.rightBarButtonItem.title = @"Skip";
    }
}

#pragma mark - Target/Action

- (void)rightBarButtonItemTapped:(id)sender
{
    [self.delegate onboardingWantsToDismiss:self];
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT VIEWS
    self.scrollView = [[UIScrollView alloc] init];
    self.view0 = [[SWOnboardingView alloc] init];
    self.view1 = [[SWOnboardingView alloc] init];
    self.view2 = [[SWOnboardingView alloc] init];
    self.view3 = [[SWOnboardingView alloc] init];
    self.view4 = [[SWOnboardingView alloc] init];
    self.cloudImageView = [[UIImageView alloc] init];
    self.pageControl = [[UIPageControl alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.myViewsDictionary = [NSMutableDictionary new];
    [self.myViewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.myViewsDictionary setObject:self.view0 forKey:@"view0"];
    [self.myViewsDictionary setObject:self.view1 forKey:@"view1"];
    [self.myViewsDictionary setObject:self.view2 forKey:@"view2"];
    [self.myViewsDictionary setObject:self.view3 forKey:@"view3"];
    [self.myViewsDictionary setObject:self.view4 forKey:@"view4"];
    [self.myViewsDictionary setObject:self.cloudImageView forKey:@"cloudImageView"];
    [self.myViewsDictionary setObject:self.pageControl forKey:@"pageControl"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.view0.translatesAutoresizingMaskIntoConstraints = NO;
    self.view1.translatesAutoresizingMaskIntoConstraints = NO;
    self.view2.translatesAutoresizingMaskIntoConstraints = NO;
    self.view3.translatesAutoresizingMaskIntoConstraints = NO;
    self.view4.translatesAutoresizingMaskIntoConstraints = NO;
    self.cloudImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.pageControl.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    BOOL orinetation = UIInterfaceOrientationIsLandscape(orientation);
    float size = (orientation) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height;
    if (size < 600){ // hiding the view behind so its not visible on smaller device
        [self.view addSubview:self.cloudImageView];
    }
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.view0];
    [self.scrollView addSubview:self.view1];
    [self.scrollView addSubview:self.view2];
    [self.scrollView addSubview:self.view3];
    [self.scrollView addSubview:self.view4];
    if (size >= 600){ // placing it in its correct place for screen sizes that dont block content.
        [self.view addSubview:self.cloudImageView];
    }
    [self.view addSubview:self.pageControl];
    if (orinetation){
        [self.cloudImageView setHidden:YES];
    }else{
        [self.cloudImageView setHidden:NO];
    }
    // LAYOUT
    NSArray *myConstraints = @[];
    
    // scrollView
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    // view0
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[view0]"] options:0 metrics:0 views:self.myViewsDictionary]];
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.view0 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.view0 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:1.0 constant:-40]]; // PUSH OUT VERTICALLY
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[view0]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    // view1
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[view0]-(0)-[view1]"] options:0 metrics:0 views:self.myViewsDictionary]];
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.view1 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[view1]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    // view2
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[view1]-(0)-[view2]"] options:0 metrics:0 views:self.myViewsDictionary]];
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.view2 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[view2]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    // view3
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[view2]-(0)-[view3]"] options:0 metrics:0 views:self.myViewsDictionary]];
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.view3 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[view3]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    // view4
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[view3]-(0)-[view4]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]]; // PUSH OUT HORIZONTALLY
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.view4 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[view4]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    // cloudImageView
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[cloudImageView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[cloudImageView(180)]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
    
    
    // pageControl
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.pageControl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.pageControl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:-20]];
    
    // ADD CONSTRAINTS
    [self.view addConstraints:myConstraints];
    
    // CONFIG
    
    // navigationBar
    self.navigationController.navigationBar.translucent = NO;
    
    // rightBarButtonItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Skip" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemTapped:)];
    
    // view
    self.view.backgroundColor = [UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0];
    
    // scrollView
    self.scrollView.delegate = self;
    self.scrollView.bounces = NO;
    self.scrollView.alwaysBounceVertical = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    // view0
    [self.view0 updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.view0 updateImage:[UIImage imageNamed:@"onboarding-icon-welcome"]];
    [self.view0 updateTitle:@"Welcome to Sickweather"];
    [self.view0 updateDescription:@"Stay in the know with the world's largest illness crowdsourcing community, which records millions of reports each month."];
    
    // view1
    [self.view1 updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.view1 updateImage:[UIImage imageNamed:@"onboarding-icon-sickscore"]];
    [self.view1 updateTitle:@"SickScore® Wherever You Go"];
    [self.view1 updateDescription:@"Always know the contagion level and the top trending illnesses in your city and travel destinations - before you go."];
    
    // view2
    [self.view2 updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.view2 updateImage:[UIImage imageNamed:@"onboarding-icon-family-health"]];
    [self.view2 updateTitle:@"Track Family Health"];
    [self.view2 updateDescription:@"Record illnesses, symptoms, temperatures, and medication safely and securely within Sickweather."];
    
    // view3
    [self.view3 updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.view3 updateImage:[UIImage imageNamed:@"onboarding-icon-sick-zone-alerts"]];
    [self.view3 updateTitle:@"SickZone Alerts"];
    [self.view3 updateDescription:@"Receive location-based notifications with SickZone Alerts, and important health newsflashes in your community"];
    
    // view4
    [self.view4 updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.view4 updateImage:[UIImage imageNamed:@"onboarding-icon-groups"]];
    [self.view4 updateTitle:@"Sickweather Groups"];
    [self.view4 updateDescription:@"Stay in the know about what's going on at your local schools, public spaces, and other areas nearby."];
    
    // cloudImageView
    self.cloudImageView.image = [UIImage imageNamed:@"onboarding-icon-cloud-pager"];
    self.cloudImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // pageControl
    self.pageControl.numberOfPages = 5;
    self.pageControl.pageIndicatorTintColor = [SWColor colorSickweatherDarkGrayText104x104x104];
    self.pageControl.currentPageIndicatorTintColor = [SWColor colorSickweatherBlue41x171x226];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        [self.cloudImageView setHidden:NO];
    }
    else if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation) && !UIInterfaceOrientationIsLandscape(currentOrientation)){
        [self.cloudImageView setHidden:YES];
    }
}

@end
