//
//  SWReportCDM+CoreDataProperties.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SWReportCDM.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWReportCDM (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *feedText;
@property (nullable, nonatomic, retain) NSNumber *latitude;
@property (nullable, nonatomic, retain) NSNumber *longitude;
@property (nullable, nonatomic, retain) NSString *postedTimestampUTC;
@property (nullable, nonatomic, retain) NSDate *reportDate;
@property (nullable, nonatomic, retain) NSString *reportId;
@property (nullable, nonatomic, retain) NSString *feedId;
@property (nullable, nonatomic, retain) SWIllnessCDM *illness;
@property (nullable, nonatomic, retain) SWUserCDM *owner;

@end

NS_ASSUME_NONNULL_END
