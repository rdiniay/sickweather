//
//  NSString+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 12/7/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "NSString+SWAdditions.h"

@implementation NSString (SWAdditions)

- (NSString *)urlencode
{
    return [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
