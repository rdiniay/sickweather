//
//  SWUserCDM+CoreDataProperties.m
//  Sickweather
//
//  Created by John Erck on 4/13/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SWUserCDM+CoreDataProperties.h"

@implementation SWUserCDM (CoreDataProperties)

@dynamic birthDate;
@dynamic city;
@dynamic country;
@dynamic email;
@dynamic emailHash;
@dynamic firstName;
@dynamic gender;
@dynamic lastName;
@dynamic medPref;
@dynamic passwordHash;
@dynamic photoOriginal;
@dynamic photoThumb;
@dynamic raceEthnicity;
@dynamic state;
@dynamic status;
@dynamic unique;
@dynamic userCurrentUserKey;
@dynamic userLastKnownCurrentLocationLatitude;
@dynamic userLastKnownCurrentLocationLongitude;
@dynamic userLastKnownCurrentLocationPlacemark;
@dynamic username;
@dynamic profilePicThumbnailImageData;
@dynamic reports;

@end
