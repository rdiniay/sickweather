//
//  SWSymptomsModal.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWSymptomsModal : NSObject
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *name;
@property (nonatomic) BOOL isSelected;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
