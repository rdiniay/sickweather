//
//  SWNewIllnessDetailViewController.h
//  Sickweather
//
//  Created by John Erck on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWNewIllnessDetailViewController : UIViewController

@property (nonatomic) BOOL isCovidScore;
@property (nonatomic) NSString * illnessName;

- (void)updateForIllnessId:(NSString *)illnessId andLocation:(CLLocation *)location;

@end
