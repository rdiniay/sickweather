//
//  SWCustomMapKitAnnotationView.h
//  Sickweather
//
//  Created by John Erck on 10/16/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SWCustomMapKitAnnotationView : MKAnnotationView

- (id)initWithCovidAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;
@end
