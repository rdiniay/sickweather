//
//  SWHomePageView.h
//  Sickweather
//
//  Created by John Erck on 7/25/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWHomePageViewDelegate
- (void)homePageViewWantsToLoadTrendingMap:(id)sender;
- (void)homePageViewTrendingIllnessRecordTapped:(id)sender;
- (void)homePageViewTurnLocationOnButtonTapped:(id)sender;
- (void)homePageViewManuallySearchForLocationButtonTapped:(id)sender;
@end

@interface SWHomePageView : UIView
@property (nonatomic, weak) id<SWHomePageViewDelegate> delegate;
@property (strong, nonatomic) UIView *mapOverlayView;
@property (strong, nonatomic) NSString *currentObjectForKey;
- (void)updateForSickScoreDict:(NSDictionary *)dict;
- (void)updateForCovidScoreDict:(NSDictionary *)dict;
- (NSDictionary *)getLastSelectedRowDataDict;
- (NSDictionary *)getDataDict;
- (void)setupAsNoLocationUI;
- (void)setupAsHasLocationUI;
- (CLLocation *)getLocation;
@end
