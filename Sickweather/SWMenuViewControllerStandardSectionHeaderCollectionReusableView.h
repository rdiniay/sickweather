//
//  SWMenuViewControllerStandardSectionHeaderCollectionReusableView.h
//  Sickweather
//
//  Created by John Erck on 8/25/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMenuViewControllerStandardSectionHeaderCollectionReusableView;

@protocol SWMenuViewControllerStandardSectionHeaderCollectionReusableViewDelegate
- (void)searchBarTextDidChange:(NSString *)searchText;
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;
- (void)createNewGroupButtonTapped:(SWMenuViewControllerStandardSectionHeaderCollectionReusableView *)sender;
@end

@interface SWMenuViewControllerStandardSectionHeaderCollectionReusableView : UICollectionReusableView
@property (nonatomic, weak) id<SWMenuViewControllerStandardSectionHeaderCollectionReusableViewDelegate> delegate;
@property (strong, nonatomic) NSString *styleStringKey;
@property (strong, nonatomic) NSString *title;
- (void)setCurrentSearchStringText:(NSString *)searchStringText;
- (void)setFocusOnSearch;
- (void)showCreateNewGroupButton;

@end
