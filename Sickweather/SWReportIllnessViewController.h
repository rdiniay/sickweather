//
//  SWReportIllnessViewController.h
//  Sickweather
//
//  Created by John Erck on 9/2/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@protocol SWReportIllnessViewControllerDelegate <NSObject>
- (void)didDismissViewControllerWithNewReports:(NSArray *)reports sender:(id)sender;
- (void)didDismissViewControllerWithoutAnyReports;
@end

@interface SWReportIllnessViewController : UIViewController
@property (nonatomic, assign) id <SWReportIllnessViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *groupId;
@property (nonatomic) BOOL isFromEventsScreen;
@property (nonatomic) BOOL isFromFamilyScreen;
@property (nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@property (strong, nonatomic) NSDictionary *latestMenuServerResponse;
@property (strong, nonatomic) NSDictionary *serverGroupDict;
@property (strong, nonatomic) NSDictionary *serverMessagePromptDict;
@end
