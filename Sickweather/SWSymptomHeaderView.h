//
//  SWSymptomHeaderView.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWSymptomHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@end
