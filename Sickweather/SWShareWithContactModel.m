//
//  SWShareWithContactModel.m
//  Sickweather
//
//  Created by John Erck on 2/12/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWShareWithContactModel.h"

@implementation SWShareWithContactModel

- (void)setAddressBookRecordRef:(ABRecordRef)addressBookRecordRef
{
    _addressBookRecordRef = addressBookRecordRef;
    
    // Check record type (for person)
    if (ABRecordGetRecordType(addressBookRecordRef) == kABPersonType)
    {
        // Get person record props
        
        // Get Id
        ABRecordID personRecordId = ABRecordGetRecordID(addressBookRecordRef);
        NSString *personRecordIdString = [NSString stringWithFormat:@"%d",personRecordId];
        if (personRecordIdString) {
            self.addressBookRecordId = personRecordIdString;
        }
        
        // Get first and last name
        NSString *firstNameString = (__bridge NSString *)ABRecordCopyValue(addressBookRecordRef, kABPersonFirstNameProperty);
        if (firstNameString) {
            self.firstName = firstNameString;
        }
        
        NSString *lastNameString = (__bridge NSString *)ABRecordCopyValue(addressBookRecordRef, kABPersonLastNameProperty);
        if (lastNameString) {
            self.lastName = lastNameString;
        }
        
        // Get email
        NSString *contactEmail = (__bridge NSString *)ABRecordCopyValue(addressBookRecordRef, kABPersonEmailProperty);
        if (contactEmail) {
            self.email = contactEmail;
        }
        
        // Get first phone number
        ABMultiValueRef phoneNumberMultiValue = ABRecordCopyValue(addressBookRecordRef, kABPersonPhoneProperty);
        NSString *phoneNumber = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phoneNumberMultiValue, 0);
        if (phoneNumber) {
            self.phone = phoneNumber;
        }
        
        // Set fullName
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstNameString, lastNameString];
        self.name = fullName;
        
        // Get profile picture image
        CFDataRef profilePictureDataRef = ABPersonCopyImageData(addressBookRecordRef);
        NSData *profilePictureData = (__bridge_transfer NSData *)profilePictureDataRef;
        UIImage *profilePictureImage = [UIImage imageWithData:profilePictureData];
        //NSLog(@"profilePictureImage = %@", profilePictureImage);
        if (profilePictureImage) {
            self.image = profilePictureImage;
        }
    }
}

@end
