//
//  SWAlertsSympathyItemTableViewCell.m
//  Sickweather
//
//  Created by John Erck on 12/12/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWAlertsSympathyItemTableViewCell.h"

@interface SWAlertsSympathyItemTableViewCell ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *insetContainerView;
@property (strong, nonatomic) UIImageView *typeIconImageView;
@property (strong, nonatomic) UILabel *typeLabel;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIView *dividerLine;
@property (strong, nonatomic) UILabel *timeLabel;
@end

@implementation SWAlertsSympathyItemTableViewCell

#pragma mark - Public Methods

- (void)updateForDict:(NSDictionary *)dict
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([[dict objectForKey:@"timestamp"] isKindOfClass:[NSNumber class]] || [[dict objectForKey:@"timestamp"] rangeOfCharacterFromSet:notDigits].location == NSNotFound){
        //NSLog(@"TIMESTAMP IS NUMBER");
        NSNumber *timestamp = [dict objectForKey:@"timestamp"];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[timestamp doubleValue]];
        
        // START BY ESTABLISHING CATCH ALL DATE FORMAT AND COLOR
        NSDateFormatter *formatter =  [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd, yyyy"];
        
        // STARTING TIME/COLOR
        NSString *timeStatement = [formatter stringFromDate:date];
        UIColor *timeColor = [SWColor colorSickweatherGrayText135x135x135];
        
        // DO SOME TESTS, REFINE UI, IF WE CAN...
        NSDate *now = [NSDate new];
        double nowDiff = [now timeIntervalSince1970] - [date timeIntervalSince1970];
        if (nowDiff < 60)
        {
            timeStatement = @"Less than a minute ago";
            timeColor = [SWColor colorSickweatherStyleGuideOrange252x111x6];
        }
        else if (nowDiff < 60*60)
        {
            timeStatement = @"Less than an hour ago";
            timeColor = [SWColor colorSickweatherStyleGuideOrange252x111x6];
        }
        
        // SET FINAL ATTRIBUTED STRING
        self.timeLabel.attributedText = [[NSAttributedString alloc] initWithString:timeStatement attributes:@{NSForegroundColorAttributeName:timeColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12]}];
    }
    self.typeIconImageView.image = [UIImage imageNamed:@"alert-icon-sympathy"];
    self.typeLabel.attributedText = [[NSAttributedString alloc] initWithString:@"SYMPATHY ALERT" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideOrange252x111x6],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12]}];
    if ([[dict objectForKey:@"title"] isKindOfClass:[NSString class]])
    {
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:[dict objectForKey:@"title"] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}];
    }
}

#pragma mark - Life Cycle Methods

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.insetContainerView = [[UIView alloc] init];
        self.typeIconImageView = [[UIImageView alloc] init];
        self.typeLabel = [[UILabel alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.dividerLine = [[UIView alloc] init];
        self.timeLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.insetContainerView forKey:@"insetContainerView"];
        [self.viewsDictionary setObject:self.typeIconImageView forKey:@"typeIconImageView"];
        [self.viewsDictionary setObject:self.typeLabel forKey:@"typeLabel"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.viewsDictionary setObject:self.dividerLine forKey:@"dividerLine"];
        [self.viewsDictionary setObject:self.timeLabel forKey:@"timeLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.insetContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.typeIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.typeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.dividerLine.translatesAutoresizingMaskIntoConstraints = NO;
        self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.insetContainerView];
        [self.insetContainerView addSubview:self.typeIconImageView];
        [self.insetContainerView addSubview:self.typeLabel];
        [self.insetContainerView addSubview:self.titleLabel];
        [self.insetContainerView addSubview:self.dividerLine];
        [self.insetContainerView addSubview:self.timeLabel];
        
        // LAYOUT
        
        // insetContainerView
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[insetContainerView]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[insetContainerView]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // typeIconImageView
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[typeIconImageView(12)]"] options:0 metrics:0 views:self.viewsDictionary]];
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[typeIconImageView(12)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.typeIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.typeLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // typeLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[typeIconImageView]-(5)-[typeLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[typeLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // titleLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[titleLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[typeLabel]-(10)-[titleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // dividerLine
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[dividerLine]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(10)-[dividerLine(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // timeLabel
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[timeLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[dividerLine]-(10)-[timeLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // view
        self.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
        
        // contentView
        self.contentView.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
        
        // insetContainerView
        self.insetContainerView.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        self.insetContainerView.layer.borderWidth = 1;
        self.insetContainerView.layer.borderColor = [SWColor colorSickweatherStyleGuideSearchBackground230x230x230].CGColor;
        self.insetContainerView.layer.cornerRadius = 4;
        
        // typeLabel
        self.typeLabel.numberOfLines = 0;
        self.typeLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Breaking News!\n\nYou about to get sick!" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        // typeIconImageView
        
        // titleLabel
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Breaking News!\n\nYou about to get sick!" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
        
        // dividerLine
        self.dividerLine.backgroundColor = [SWColor colorSickweatherStyleGuideSearchBackground230x230x230];
        
        // timeLabel
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
