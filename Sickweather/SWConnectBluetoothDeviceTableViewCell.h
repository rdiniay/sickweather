//
//  SWConnectBluetoothDeviceTableViewCell.h
//  Sickweather
//
//  Created by Muhammad Javed on 06/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWConnectBluetoothDeviceTableViewCell;

@protocol SWConnectBluetoothDeviceTableViewCellDelegate
- (void)connectBluetoothDeviceTableViewCellConnectButtonTapped:(NSIndexPath *_Nonnull)indexPath;
- (void)connectBluetoothDeviceTableViewCellBuyItButtonTapped:(NSIndexPath *_Nonnull)indexPath;
@end

@interface SWConnectBluetoothDeviceTableViewCell : UITableViewCell
@property (strong, nonatomic) NSIndexPath * _Nonnull indexPath;
@property (nonatomic, weak) _Nullable id<SWConnectBluetoothDeviceTableViewCellDelegate> delegate;
- (void)setDeviceTitle:(NSString*_Nonnull)title;
- (void)setDeviceImage:(UIImage*_Nonnull)image;
- (void)setRecommendedBanner:(BOOL)flag;
@end
