//
//  SWFacebookSignInSubController.h
//  Sickweather
//
//  Created by John Erck on 3/31/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SWFacebookSignInSubControllerLoginCompletionHandler)(SWUserCDM *loggedInUser);

@interface SWFacebookSignInSubController : NSObject
@property (weak, nonatomic) UIViewController *presentingViewController;
- (void)loginUsingCompletionHandler:(SWFacebookSignInSubControllerLoginCompletionHandler)completionHandler;
@end
