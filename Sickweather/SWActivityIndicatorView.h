//
//  SWActivityIndicatorView.h
//  Sickweather
//
//  Created by John Erck on 10/6/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWActivityIndicatorViewDelegate <NSObject>
- (NSString *)activityIndicatorViewMessage:(id)sender;
@end

@interface SWActivityIndicatorView : UIView
@property (weak, nonatomic) id <SWActivityIndicatorViewDelegate> delegate;
- (void)startAnimating;
- (void)stopAnimating;
- (void)setMessageText:(NSString *)messageText;
- (void)addSuccessMessage:(NSString *)successMessage;
- (void)styleForInlineCenterScreenUse;
- (id)initUsingLight;
- (id)initUsingDark;
@end
