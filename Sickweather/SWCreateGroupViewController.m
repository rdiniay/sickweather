//
//  SWCreateGroupViewController.m
//  Sickweather
//
//  Created by John Erck on 5/19/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWCreateGroupViewController.h"
#import "SWEditMyProfileSectionHeaderView.h"
#import "SWEditMyProfileRowView.h"
#import "SWSelectItemFromListViewController.h"
#import "SWCaptureUserDefinedAddressViewController.h"
#import "Flurry.h"

#define rowHeight 50

@interface SWCreateGroupViewController () <UIScrollViewDelegate, SWEditMyProfileSectionHeaderViewDelegate, SWEditMyProfileRowViewDelegate, UITextFieldDelegate, SWSelectItemFromListViewControllerDelegate, SWCaptureUserDefinedAddressViewControllerDelegate>
@property (weak, nonatomic) UITextField *activeField;
@property (weak, nonatomic) SWEditMyProfileRowView *activeRow;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) SWEditMyProfileSectionHeaderView *groupInfoSectionHeader;
@property (strong, nonatomic) SWEditMyProfileRowView *groupNameRow;
@property (strong, nonatomic) SWEditMyProfileRowView *groupPrivacyRow;
@property (strong, nonatomic) SWEditMyProfileRowView *groupLocationRow;
@property (strong, nonatomic) SWEditMyProfileRowView *groupPhoneRow;
@property (strong, nonatomic) NSString *groupNameValueToSubmitToServer;
@property (strong, nonatomic) NSString *cityLocationValueToSubmitToServer;
@property (strong, nonatomic) NSString *stateLocationValueToSubmitToServer;
@property (strong, nonatomic) NSString *countryLocationValueToSubmitToServer;
@property (strong, nonatomic) NSString *phoneValueToSubmitToServer;
@property (strong, nonatomic) NSString *groupPrivacyValueToSubmitToServer;
@property (assign) BOOL kbIsUp;
@end

@implementation SWCreateGroupViewController

#pragma mark - SWCaptureUserDefinedAddressViewControllerDelegate

- (void)captureUserDefinedAddressViewController:(SWCaptureUserDefinedAddressViewController *)sender userInputCityString:(NSString *)cityString stateRegionString:(NSString *)stateRegionString countryString:(NSString *)countryString
{
    //NSLog(@"cityString = %@, stateRegionString = %@, countryString = %@", cityString, stateRegionString, countryString);
    self.cityLocationValueToSubmitToServer = cityString;
    self.stateLocationValueToSubmitToServer = stateRegionString;
    self.countryLocationValueToSubmitToServer = countryString;
    NSString *newLocation = [SWHelper helperUserLocationCityStateCountryFormattedStringForUICity:cityString state:stateRegionString country:countryString];
    [self.activeRow setRowTextFieldText:newLocation];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.activeRow highlightNewlySelectedValue];
    }];
}

- (NSString *)captureUserDefinedAddressViewControllerCity
{
    return self.cityLocationValueToSubmitToServer;
}

- (NSString *)captureUserDefinedAddressViewControllerState
{
    return self.stateLocationValueToSubmitToServer;
}

- (NSString *)captureUserDefinedAddressViewControllerCountry
{
    return self.countryLocationValueToSubmitToServer;
}

- (void)captureUserDefinedAddressViewControllerWantsToDismiss:(SWCaptureUserDefinedAddressViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - SWSelectItemFromListViewControllerDelegate

- (NSString *)selectItemFromListViewControllerJSONFileToUseBaseNameWithoutExtension
{
    NSString *name;
    if ([self.activeRow isEqual:self.groupPrivacyRow])
    {
        name = @"selectGroupPrivacy"; // .json in app's bundle
    }
    return name;
}

- (NSString *)selectItemFromListViewControllerSelectedSystemValue:(SWSelectItemFromListViewController *)sender
{
    NSString *systemValue;
    if ([self.activeRow isEqual:self.groupPrivacyRow])
    {
        systemValue = self.groupPrivacyValueToSubmitToServer;
    }
    return systemValue;
}

- (void)selectItemFromListViewControllerWantsToDismiss:(SWSelectItemFromListViewController *)sender
{
    if ([self.activeRow isEqual:self.groupPrivacyRow])
    {
        if (!self.groupPrivacyValueToSubmitToServer)
        {
            self.groupPrivacyValueToSubmitToServer = @"true";
            [self.activeRow setRowTextFieldText:@"Private"];
            [self.activeRow highlightNewlySelectedValue];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selectItemFromListViewController:(SWSelectItemFromListViewController *)sender userSelectedRowDict:(NSDictionary *)rowDict
{
    if ([self.activeRow isEqual:self.groupPrivacyRow])
    {
        if ([rowDict objectForKey:@"system_value"])
        {
            self.groupPrivacyValueToSubmitToServer = [rowDict objectForKey:@"system_value"];
        }
    }
    [self.activeRow setRowTextFieldText:[rowDict objectForKey:@"name"]];
    [self.activeRow highlightNewlySelectedValue];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.kbIsUp)
    {
        [self.view endEditing:YES];
    }
}

#pragma mark - SWEditMyProfileSectionHeaderViewDelegate

- (NSString *)editMyProfileSectionHeaderViewTitleString:(SWEditMyProfileSectionHeaderView *)sender
{
    return @"GROUP INFO";
}

#pragma mark - SWEditMyProfileRowViewDelegate

- (NSString *)editMyProfileRowViewTitleString:(SWEditMyProfileRowView *)sender
{
    return @"";
}

- (NSString *)editMyProfileRowViewPlaceholderString:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.groupNameRow])
    {
        return @"Name";
    }
    if ([sender isEqual:self.groupPrivacyRow])
    {
        return @"Privacy";
    }
    if ([sender isEqual:self.groupLocationRow])
    {
        return @"Location (optional)";
    }
    if ([sender isEqual:self.groupPhoneRow])
    {
        return @"Phone (optional)";
    }
    return @"";
}

- (UIImage *)editMyProfileRowViewIconImage:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.groupNameRow])
    {
        return [UIImage imageNamed:@"profile-icon-username"];
    }
    if ([sender isEqual:self.groupPrivacyRow])
    {
        return [UIImage imageNamed:@"group-icon-lock"];
    }
    if ([sender isEqual:self.groupLocationRow])
    {
        return [UIImage imageNamed:@"profile-icon-location"];
    }
    if ([sender isEqual:self.groupPhoneRow])
    {
        return [UIImage imageNamed:@"group-icon-phone"];
    }
    return [UIImage imageNamed:@"profile-icon-username"];
}

- (void)editMyProfileRowWasTapped:(SWEditMyProfileRowView *)sender
{
    if ([sender isEqual:self.groupNameRow])
    {
        // Do nothing
    }
    if ([sender isEqual:self.groupPrivacyRow])
    {
        self.activeRow = self.groupPrivacyRow;
        SWSelectItemFromListViewController *selectItemFromListVC = [[SWSelectItemFromListViewController alloc] init];
        selectItemFromListVC.delegate = self;
        [self.navigationController pushViewController:selectItemFromListVC animated:YES];
    }
    if ([sender isEqual:self.groupLocationRow])
    {
        self.activeRow = self.groupLocationRow;
        SWCaptureUserDefinedAddressViewController *captureUserDefinedAddressVC = [[SWCaptureUserDefinedAddressViewController alloc] init];
        captureUserDefinedAddressVC.delegate = self;
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:captureUserDefinedAddressVC];
        [self presentViewController:navVC animated:YES completion:^{}];
    }
    if ([sender isEqual:self.groupPhoneRow])
    {
        // Do nothing
    }
}

#pragma mark Keyboard Management Methods (call register from viewDidLoad)

// Register for notifications helper
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //NSLog(@"keyboardWillShow");
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    // Update state var
    self.kbIsUp = YES;
    
    // Calculate the frame of the scrollview, in self.view's coordinate system
    UIScrollView *scrollView    = self.scrollView;
    CGRect scrollViewRect       = [self.view convertRect:scrollView.frame fromView:scrollView.superview];
    
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    
    // Figure out where the two frames overlap, and set the content offset of the scrollview appropriately
    CGRect hiddenScrollViewRect = CGRectIntersection(scrollViewRect, kbRect);
    if (!CGRectIsNull(hiddenScrollViewRect))
    {
//        UIEdgeInsets contentInsets       = UIEdgeInsetsMake(0.0,  0.0, hiddenScrollViewRect.size.height,  0.0);
//        scrollView.contentInset          = contentInsets;
//        scrollView.scrollIndicatorInsets = contentInsets;
    }
    //[self performSelector:@selector(scrollToActiveTextField) withObject:nil afterDelay:0.1]; // Delay is required for iOS to handle things as expected
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    // Update state var
    self.kbIsUp = NO;
    
    //self.scrollView.contentInset          = UIEdgeInsetsZero;
    //self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)scrollToActiveTextField
{
    return;
    /*
    if (self.activeField)
    {
        CGRect visibleRect = self.activeField.frame;
        visibleRect        = [self.scrollView convertRect:visibleRect fromView:self.activeField.superview];
        visibleRect        = CGRectInset(visibleRect, 0.0f, -00.0f);
        [self.scrollView scrollRectToVisible:visibleRect animated:YES];
    }
    */
}

#pragma mark - UITextFieldDelegate (part of Keyboard Management Methods)

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if ([textField isEqual:[self.groupNameRow currentTextField]])
    {
        [[self.groupNameRow currentTextField] resignFirstResponder];
        //[[self.emailRow currentTextField] becomeFirstResponder];
    }
    if ([textField isEqual:[self.groupPrivacyRow currentTextField]])
    {
        [[self.groupPrivacyRow currentTextField] resignFirstResponder];
        //[[self.emailRow currentTextField] becomeFirstResponder];
    }
    if ([textField isEqual:[self.groupLocationRow currentTextField]])
    {
        [[self.groupLocationRow currentTextField] resignFirstResponder];
        //[[self.emailRow currentTextField] becomeFirstResponder];
    }
    if ([textField isEqual:[self.groupPhoneRow currentTextField]])
    {
        [[self.groupPhoneRow currentTextField] resignFirstResponder];
        //[[self.emailRow currentTextField] becomeFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
}

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	//Log to Flurry
	[SWHelper logFlurryEventsWithTag:@"Create Group > X button tapped"];
	//Dismiss
    [self.delegate createGroupViewControllerWantsToDismissDueToUserCancel:self];
}

- (void)rightBarButtonItemTapped:(id)sender
{
    // Dismiss keyboard
    [self.view endEditing:YES];
	//Log to Flurry
	[SWHelper logFlurryEventsWithTag:@"Create Group > Save button tapped"];
	//Save Logic
    self.groupNameValueToSubmitToServer = [self.groupNameRow currentTextField].text;
    self.phoneValueToSubmitToServer = [self.groupPhoneRow currentTextField].text;
    BOOL valid = YES;
    //NSLog(@"self.groupNameValueToSubmitToServer = %@", self.groupNameValueToSubmitToServer);
    if (valid && [[self.groupNameValueToSubmitToServer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
    {
        valid = NO;
        [SWHelper helperShowAlertWithTitle:@"Missing Name" message:@""];
    }
    //NSLog(@"self.groupPrivacyValueToSubmitToServer = %@", self.groupPrivacyValueToSubmitToServer);
    if (valid && !self.groupPrivacyValueToSubmitToServer)
    {
        valid = NO;
        [SWHelper helperShowAlertWithTitle:@"Missing Privacy" message:@""];
    }
    if (valid)
    {
        // Log to Flurry
        [Flurry logEvent:@"Create New Group Save Button Tapped"];
        
        // Block screen
        [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
        
        // Hit network
        /*
         - group_name - user’s desired name for the group
         - street/PO box
         - city name
         - state name or abbreviation
         - zip/postal code
         - country name or abbreviation
         - formatted phone number
         - whether group should be public (false) or private (true)
         */
        /*
         NSDictionary *serverArgs = @{
         @"group_name":[self.groupNameRow currentTextField].text,
         @"address":@"",
         @"city":self.cityLocationValueToSubmitToServer,
         @"state":self.stateLocationValueToSubmitToServer,
         @"zip":@"",
         @"country":self.countryLocationValueToSubmitToServer,
         @"phone":@"",
         @"closed":@"true",
         };
         */
        NSMutableDictionary *serverArgs = [NSMutableDictionary new];
        if ([self.groupNameValueToSubmitToServer length] > 0)
        {
            [serverArgs setObject:self.groupNameValueToSubmitToServer forKey:@"group_name"];
        }
        /*
         if ([self.addressLocationValueToSubmitToServer length] > 0)
         {
         [serverArgs setObject:self.addressLocationValueToSubmitToServer forKey:@"address"];
         }
         */
        if ([self.cityLocationValueToSubmitToServer length] > 0)
        {
            [serverArgs setObject:self.cityLocationValueToSubmitToServer forKey:@"city"];
        }
        if ([self.stateLocationValueToSubmitToServer length] > 0)
        {
            [serverArgs setObject:self.stateLocationValueToSubmitToServer forKey:@"state"];
        }
        /*
         if ([self.zipLocationValueToSubmitToServer length] > 0)
         {
         [serverArgs setObject:self.zipLocationValueToSubmitToServer forKey:@"zip"];
         }
         */
        if ([self.countryLocationValueToSubmitToServer length] > 0)
        {
            [serverArgs setObject:self.countryLocationValueToSubmitToServer forKey:@"country"];
        }
        if ([self.phoneValueToSubmitToServer length] > 0)
        {
            [serverArgs setObject:self.phoneValueToSubmitToServer forKey:@"phone"];
        }
        if ([self.groupPrivacyValueToSubmitToServer length] > 0)
        {
            [serverArgs setObject:self.groupPrivacyValueToSubmitToServer forKey:@"closed"];
        }
        //NSLog(@"serverArgs = %@", serverArgs);
        [[SWHelper helperAppBackend] appBackendCreateGroupUsingArgs:serverArgs completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                [SWHelper helperDismissFullScreenActivityIndicator];
                [self.delegate createGroupViewControllerWantsToDismissAfterSuccessfullyCreatingNewGroup:self];
            });
        }];
    }
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT SELF
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.groupInfoSectionHeader = [[SWEditMyProfileSectionHeaderView alloc] init];
    self.groupNameRow = [[SWEditMyProfileRowView alloc] init];
    self.groupPrivacyRow = [[SWEditMyProfileRowView alloc] init];
    self.groupLocationRow = [[SWEditMyProfileRowView alloc] init];
    self.groupPhoneRow = [[SWEditMyProfileRowView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.groupInfoSectionHeader forKey:@"groupInfoSectionHeader"];
    [self.viewsDictionary setObject:self.groupNameRow forKey:@"groupNameRow"];
    [self.viewsDictionary setObject:self.groupPrivacyRow forKey:@"groupPrivacyRow"];
    [self.viewsDictionary setObject:self.groupLocationRow forKey:@"groupLocationRow"];
    [self.viewsDictionary setObject:self.groupPhoneRow forKey:@"groupPhoneRow"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.groupInfoSectionHeader.translatesAutoresizingMaskIntoConstraints = NO;
    self.groupNameRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.groupPrivacyRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.groupLocationRow.translatesAutoresizingMaskIntoConstraints = NO;
    self.groupPhoneRow.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.groupInfoSectionHeader];
    [self.contentView addSubview:self.groupNameRow];
    [self.contentView addSubview:self.groupPrivacyRow];
    [self.contentView addSubview:self.groupLocationRow];
    [self.contentView addSubview:self.groupPhoneRow];
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout HEADER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[groupInfoSectionHeader]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[groupInfoSectionHeader(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.groupInfoSectionHeader attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
    
    // Layout groupNameRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[groupNameRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[groupInfoSectionHeader]-(0)-[groupNameRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout groupPrivacyRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[groupPrivacyRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[groupNameRow]-(0)-[groupPrivacyRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout groupLocationRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[groupLocationRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[groupPrivacyRow]-(0)-[groupLocationRow(rowHeight)]"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]];
    
    // Layout groupPhoneRow
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[groupPhoneRow]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[groupLocationRow]-(0)-[groupPhoneRow(rowHeight)]-|"] options:0 metrics:@{@"rowHeight":@rowHeight} views:self.viewsDictionary]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR HEIGHT !!!
    
    // CONFIG
    
    // Config KEYBOARD NOTIFICATIONS
    // https://developer.apple.com/library/ios/documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html
    [self registerForKeyboardNotifications];
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
    
    // Config TITLE
    self.title = @"Create a Group";
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config RIGHT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // Config USERNAME SECTION HEADER
    self.groupInfoSectionHeader.delegate = self;
    
    // Config groupNameRow
    self.groupNameRow.delegate = self;
    [self.groupNameRow currentTextField].delegate = self;
    [self.groupNameRow currentTextField].autocorrectionType = UITextAutocorrectionTypeDefault;
    [self.groupNameRow currentTextField].autocapitalizationType = UITextAutocapitalizationTypeWords;
    [self.groupNameRow currentTextField].spellCheckingType = UITextSpellCheckingTypeYes;
    [self.groupNameRow currentTextField].enablesReturnKeyAutomatically = YES;
    [self.groupNameRow currentTextField].keyboardType = UIKeyboardTypeDefault;
    [self.groupNameRow currentTextField].returnKeyType = UIReturnKeyNext;
    [self.groupNameRow currentTextField].secureTextEntry = NO;
    
    // Config groupPrivacyRow
    self.groupPrivacyRow.delegate = self;
    [self.groupPrivacyRow addDisclosureIconAndTapFeature];
    
    // Config groupLocationRow
    self.groupLocationRow.delegate = self;
    [self.groupLocationRow addDisclosureIconAndTapFeature];
    
    // Config groupPhoneRow
    self.groupPhoneRow.delegate = self;
    [self.groupPhoneRow currentTextField].delegate = self;
    [self.groupPhoneRow currentTextField].autocorrectionType = UITextAutocorrectionTypeDefault;
    [self.groupPhoneRow currentTextField].autocapitalizationType = UITextAutocapitalizationTypeWords;
    [self.groupPhoneRow currentTextField].spellCheckingType = UITextSpellCheckingTypeYes;
    [self.groupPhoneRow currentTextField].enablesReturnKeyAutomatically = YES;
    [self.groupPhoneRow currentTextField].keyboardType = UIKeyboardTypePhonePad;
    [self.groupPhoneRow currentTextField].returnKeyType = UIReturnKeyNext;
    [self.groupPhoneRow currentTextField].secureTextEntry = NO;
    [self.groupPhoneRow showBottomBorder];
    
    /*
     // RUN NETWORK TEST...
     [[SWHelper helperAppBackend] appBackendGetUserProfileUsingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
     //NSLog(@"requestURLAbsoluteString = %@, responseBodyAsString = %@", requestURLAbsoluteString, responseBodyAsString);
     }];
     */
    
    // COLOR FOR DEVELOPMENT PURPOSES
    //    self.scrollView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    //    self.contentView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
    //    self.groupInfoSectionHeader.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
    //    self.groupNameRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
    //    self.groupPrivacyRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
    //    self.groupLocationRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
    //    self.groupPhoneRow.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
}

@end
