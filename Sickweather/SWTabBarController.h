//
//  SWTabBarController.h
//  Sickweather
//
//  Created by Shan Shafiq on 1/24/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWTabBarController : UITabBarController

@end
