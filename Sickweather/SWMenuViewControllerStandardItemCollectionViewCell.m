//
//  SWMenuViewControllerStandardItemCollectionViewCell.m
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "SWMenuViewControllerStandardItemCollectionViewCell.h"

@interface SWMenuViewControllerStandardItemCollectionViewCell ()
@property (assign) BOOL shouldShowTopBorder;
@property (assign) BOOL shouldHideIconImageView;
@property (assign) BOOL styleTitleForInfoMessageBool;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UIImageView *iconImageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;
@property (strong, nonatomic) UIImageView *discloserIndicatorImageView;
@property (strong, nonatomic) UIView *bottomBorderView;
@end

@implementation SWMenuViewControllerStandardItemCollectionViewCell

#pragma mark - Public Methods

- (void)setIconImageName:(NSString *)imageName
{
    self.iconImageView.image = [UIImage imageNamed:imageName];
    [self removeAndReapplyLayoutConstraints];
}

- (void)setTitleAttributedString:(NSAttributedString *)titleAttributedString
{
    self.titleLabel.attributedText = titleAttributedString;
    [self removeAndReapplyLayoutConstraints];
}

- (void)setAddressAttributedString:(NSAttributedString *)addressAttributedString
{
    self.descriptionLabel.attributedText = addressAttributedString;
    [self removeAndReapplyLayoutConstraints];
}

- (void)showTopBorder
{
    self.shouldShowTopBorder = YES;
    [self removeAndReapplyLayoutConstraints];
}

- (void)hideIconImageView
{
    self.shouldHideIconImageView = YES;
    [self removeAndReapplyLayoutConstraints];
}

- (void)styleTitleForInfoMessage
{
    self.styleTitleForInfoMessageBool = YES;
    [self removeAndReapplyLayoutConstraints];
}

#pragma mark - Helpers

- (void)removeAndReapplyLayoutConstraints
{
    // REMOVE
    [self.contentView removeConstraints:self.contentView.constraints];
    
    // CREATE AUTO LAYOUT REFERENCE DICT
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_topBorderView, _iconImageView, _titleLabel, _descriptionLabel, _discloserIndicatorImageView, _bottomBorderView);
    
    // Layout TOP BORDER VIEW
    NSNumber *topBorderHeight = @0;
    if (self.shouldShowTopBorder)
    {
        topBorderHeight = @1;
    }
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[_topBorderView]|"] options:0 metrics:0 views:viewsDictionary]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_topBorderView(topBorderHeight)]"] options:0 metrics:@{@"topBorderHeight":topBorderHeight} views:viewsDictionary]];
    
    // Layout ICON IMAGE VIEW
    NSNumber *titleLeftMargin = @(50);
    NSNumber *titleRightMargin = @(16); /* THESE VALUES NEED TO BE IN SYNC WITH PARTNER LAYOUT FILE'S SETTINGS */
    if (self.shouldHideIconImageView)
    {
        self.iconImageView.hidden = YES;
        titleLeftMargin = @(10);
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[_iconImageView(0)]"] options:0 metrics:0 views:viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_iconImageView]|"] options:0 metrics:0 views:viewsDictionary]];
    }
    else
    {
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[_iconImageView]"] options:0 metrics:0 views:viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_iconImageView]|"] options:0 metrics:0 views:viewsDictionary]];
    }
    
    // Layout TITLE and DESCRIPTION based on style type...
    if (self.styleTitleForInfoMessageBool == YES)
    {
        // Layout TITLE LABEL
        titleRightMargin = @(0);
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(titleLeftMargin)-[_titleLabel]-(titleRightMargin)-|"] options:0 metrics:@{@"titleLeftMargin":titleLeftMargin,@"titleRightMargin":titleRightMargin} views:viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_titleLabel]|"] options:0 metrics:0 views:viewsDictionary]];
    }
    else
    {
        if ([self.descriptionLabel.attributedText.string isKindOfClass:[NSString class]] && self.descriptionLabel.attributedText.string.length > 0)
        {
            // You will have two labels
            
            // Layout TITLE LABEL
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(titleLeftMargin)-[_titleLabel]-(titleRightMargin)-|"] options:0 metrics:@{@"titleLeftMargin":titleLeftMargin,@"titleRightMargin":titleRightMargin} views:viewsDictionary]];
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(8)-[_titleLabel]"] options:0 metrics:0 views:viewsDictionary]];
            
            // Layout DESCRIPTION LABEL
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(titleLeftMargin)-[_descriptionLabel]-|"] options:0 metrics:@{@"titleLeftMargin":titleLeftMargin,@"titleRightMargin":titleRightMargin} views:viewsDictionary]];
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[_descriptionLabel]-(8)-|"] options:0 metrics:0 views:viewsDictionary]];
        }
        else
        {
            // You will have one label
            
            // Layout TITLE LABEL
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(titleLeftMargin)-[_titleLabel]-(titleRightMargin)-|"] options:0 metrics:@{@"titleLeftMargin":titleLeftMargin,@"titleRightMargin":titleRightMargin} views:viewsDictionary]];
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_titleLabel]|"] options:0 metrics:0 views:viewsDictionary]];
        }
    }
    
    // Layout DISCLOSURE INDICATOR IMAGE VIEW
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[_discloserIndicatorImageView]-(10)-|"] options:0 metrics:0 views:viewsDictionary]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[_discloserIndicatorImageView]|"] options:0 metrics:0 views:viewsDictionary]];
    self.discloserIndicatorImageView.hidden = NO;
    if (self.styleTitleForInfoMessageBool)
    {
        self.discloserIndicatorImageView.hidden = YES;
    }
    
    // Layout BOTTOM BORDER VIEW
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[_bottomBorderView]|"] options:0 metrics:0 views:viewsDictionary]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[_bottomBorderView(1)]|"] options:0 metrics:0 views:viewsDictionary]];
    
    // UPDATE
    [self layoutIfNeeded];
}

#pragma mark - Life Cycle Methods

- (void)prepareForReuse
{
    self.shouldShowTopBorder = NO;
    self.shouldHideIconImageView = NO;
    self.styleTitleForInfoMessageBool = NO;
    self.iconImageView.hidden = NO;
    self.descriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:@""];
    [self removeAndReapplyLayoutConstraints];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.topBorderView = [[UIView alloc] init];
        self.iconImageView = [[UIImageView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.descriptionLabel = [[UILabel alloc] init];
        self.discloserIndicatorImageView = [[UIImageView alloc] init];
        self.bottomBorderView = [[UIView alloc] init];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.discloserIndicatorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.topBorderView];
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.descriptionLabel];
        [self.contentView addSubview:self.discloserIndicatorImageView];
        [self.contentView addSubview:self.bottomBorderView];
        
        // CONFIG
        
        // Config CONTENT VIEW
        self.contentView.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        self.contentView.clipsToBounds = YES; // Makes so we can have zero height cell and have nothing show
        
        // Config TOP BORDER VIEW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config ICON IMAGE VIEW
        self.iconImageView.contentMode = UIViewContentModeCenter;
        
        // Config TITLE LABEL
        self.titleLabel.numberOfLines = 0;
        
        // Config DESCRIPTION LABEL
        self.descriptionLabel.numberOfLines = 0;
        
        // Config DISCLOSURE INDICATOR IMAGE VIEW
        self.discloserIndicatorImageView.image = [UIImage imageNamed:@"menu-icon-disclosure-arrow"];
        self.discloserIndicatorImageView.contentMode = UIViewContentModeCenter;
        
        // Config BOTTOM BORDER VIEW
        self.bottomBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // COLOR
        //self.contentView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
        //self.titleLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        //self.descriptionLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        
        // LAYOUT
        [self removeAndReapplyLayoutConstraints];
    }
    return self;
}

@end
