//
//  SWBuyDeviceViewController.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 12/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWBuyDeviceViewController : UIViewController
@property NSString *urlString;
@property NSString *viewTitle;
@end
