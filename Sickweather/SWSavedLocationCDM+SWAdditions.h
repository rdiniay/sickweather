//
//  SWSavedLocationCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 5/15/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSavedLocationCDM.h"

@interface SWSavedLocationCDM (SWAdditions)

+ (void)clearTable;
+ (void)savedLocationDeleteRecordWithId:(NSNumber *)savedLocationId;
+ (SWSavedLocationCDM *)savedLocationForId:(NSNumber *)savedLocationId;
+ (SWSavedLocationCDM *)createSavedLocationForPlacemark:(CLPlacemark *)placemark;
+ (NSArray *)savedLocations;

- (CLPlacemark *)placemark;

@end
