//
//  SWFeedbackView.h
//  Sickweather
//
//  Created by John Erck on 1/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWFeedbackViewDelegate
- (void)feedbackViewAppStoreRatingRequestedUserSaidNo:(id)sender;
- (void)feedbackViewAppStoreRatingRequestedUserSaidYes:(id)sender;
- (void)feedbackViewFeedbackRequestedUserSaidYes:(id)sender;
- (void)feedbackViewFeedbackRequestedUserSaidNo:(id)sender;
- (void)feedbackViewYesButtonTapped:(id)sender;
- (void)feedbackViewNoButtonTapped:(id)sender;
@end

@interface SWFeedbackView : UIView
@property (nonatomic, weak) id<SWFeedbackViewDelegate> delegate;
@property (strong, nonatomic) NSDictionary *serverPromptDict;
- (void)reset;
- (void)resetUsingServerPromptDict:(NSDictionary *)serverPromptDict;
@end
