//
//  SWPrivateReportMapKitAnnotationView.m
//  Sickweather
//
//  Created by John Erck on 5/1/17.
//  Copyright (c) 2017 Sickweather, LLC. All rights reserved.
//

#import "SWPrivateReportMapKitAnnotationView.h"
#import "SWPrivateReportMapKitAnnotation.h"

@interface SWPrivateReportMapKitAnnotationView ()
@property (strong, nonatomic) UIView *myLeftCallout;
@property (strong, nonatomic) UIButton *myButton;
@end

@implementation SWPrivateReportMapKitAnnotationView

- (UIView *)leftCalloutAccessoryView
{
    // Define tags
    NSInteger topLabelTag = 100;
    NSInteger bottomLabelTag = 200;
    
    // Create views and hierarchy
    if (!self.myLeftCallout)
    {
        NSUInteger boxWidth = 60;
        NSUInteger boxHeight = 100; // This is just an arbitrary number really, just a big one to get the overflow vertically that we're looking for
        NSUInteger boxLeftPadding = 8;
        NSUInteger boxTopPadding = 6;
        self.myLeftCallout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, boxWidth, boxHeight)];
        UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(boxLeftPadding, boxTopPadding, (boxWidth - (boxLeftPadding * 2)), 24)];
        topLabel.tag = topLabelTag;
        [topLabel setTextAlignment:NSTextAlignmentCenter];
        UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, boxWidth, 12)];
        bottomLabel.tag = bottomLabelTag;
        [bottomLabel setTextAlignment:NSTextAlignmentCenter];
        topLabel.font = [UIFont boldSystemFontOfSize:26];
        topLabel.textAlignment = NSTextAlignmentCenter;
        topLabel.textColor = [UIColor whiteColor];
        bottomLabel.font = [UIFont systemFontOfSize:9];
        bottomLabel.textAlignment = NSTextAlignmentCenter;
        bottomLabel.textColor = [UIColor whiteColor];
        [self.myLeftCallout addSubview:topLabel];
        [self.myLeftCallout addSubview:bottomLabel];
    }
    
    // Get annotation object
    SWPrivateReportMapKitAnnotation *annotation = (SWPrivateReportMapKitAnnotation *)self.annotation;
    
    // Get dates
    NSDate *now = [NSDate date];
    NSDate *reportDate = annotation.reportDate;
    
    // Get diff interval
    NSTimeInterval secondsBetween = [now timeIntervalSinceDate:reportDate];
    
    // Get days ago
    int numberOfDays = secondsBetween / 86400;
    NSNumber *daysAgo = @(numberOfDays);
    
    // Color code!!
    /*
     if (annotation.isACurrentGeofenceToo) {
     self.myLeftCallout.backgroundColor = [UIColor colorWithRed:255/255.0 green:64/255.0 blue:64/255.0 alpha:1];
     }
     */
    if ([daysAgo doubleValue] <= 1) {
        // RED (1 day ago):  rgba(255, 64, 64, 1.0)
        self.myLeftCallout.backgroundColor = [UIColor colorWithRed:255/255.0 green:64/255.0 blue:64/255.0 alpha:1];
    } else if ([daysAgo doubleValue] <= 2) {
        // ORANGE (2 days ago):  rgba(247, 137, 30, 1.0)
        self.myLeftCallout.backgroundColor = [UIColor colorWithRed:247/255.0 green:137/255.0 blue:30/255.0 alpha:1];
    } else if ([daysAgo doubleValue] <= 6) {
        // YELLOW (3-6 days ago):  rgba(242, 222, 37, 1.0)
        self.myLeftCallout.backgroundColor = [UIColor colorWithRed:242/255.0 green:222/255.0 blue:37/255.0 alpha:1];
    } else if ([daysAgo doubleValue] <= 14) {
        // BLUE (7-14 days ago (labeled in weeks)):  rgba(37, 169, 224, 1.0)
        self.myLeftCallout.backgroundColor = [UIColor colorWithRed:37/255.0 green:169/255.0 blue:224/255.0 alpha:1];
    } else {
        // BLUE (catch all)
        self.myLeftCallout.backgroundColor = [UIColor colorWithRed:37/255.0 green:169/255.0 blue:224/255.0 alpha:1];
    }
    
    // Get days ago text
    NSString *daysAgoText = @"days ago";
    if ([daysAgo integerValue] == 1) {
        daysAgoText = @"day ago";
    }
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    /*
    NSDateComponents *days = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:reportDate
                                                          toDate:now
                                                         options:NSCalendarWrapComponents];
    //NSLog(@"days %ld", (long)[days day]); // e.g. days 479
    */
    
    NSDateComponents *months = [gregorianCalendar components:NSCalendarUnitMonth
                                                  fromDate:reportDate
                                                    toDate:now
                                                   options:NSCalendarWrapComponents];
    //NSLog(@"months %ld", (long)[months month]); // e.g. months 15
    
    NSDateComponents *years = [gregorianCalendar components:NSCalendarUnitYear
                                                    fromDate:reportDate
                                                      toDate:now
                                                     options:NSCalendarWrapComponents];
    //NSLog(@"years %ld", (long)[years year]); // e.g. years 1
    
    // Update days ago text and int to reflect weeks, months, years (for self reports)...
    
    // Days, already handled above
    
    if ([years year] == 1)
    {
        daysAgo = @1;
        daysAgoText = @"year ago";
    }
    else if ([years year] > 1)
    {
        daysAgo = [NSNumber numberWithInteger:[years year]];
        daysAgoText = @"years ago";
    }
    else if ([months month] == 1)
    {
        daysAgo = @1;
        daysAgoText = @"month ago";
    }
    else if ([months month] > 1)
    {
        daysAgo = [NSNumber numberWithInteger:[months month]];
        daysAgoText = @"months ago";
    }
    else if ([daysAgo integerValue] > 21)
    {
        daysAgo = @3;
        daysAgoText = @"weeks ago";
    }
    else if ([daysAgo integerValue] > 14)
    {
        daysAgo = @2;
        daysAgoText = @"weeks ago";
    }
    else if ([daysAgo integerValue] > 14)
    {
        //NSLog(@"Will never get executed, fake edit so we can pass in the full commit msg for previous commit...");
    }
    else
    {
        // Already handled...
    }
    
    // Get existing labels and update
    UILabel *myTopLabel = (UILabel *)[self.myLeftCallout viewWithTag:topLabelTag];
    UILabel *myBottomLabel = (UILabel *)[self.myLeftCallout viewWithTag:bottomLabelTag];
    myTopLabel.text = [daysAgo stringValue];
    myBottomLabel.text = daysAgoText;
    
    // Return self.myLeftCallout
    return self.myLeftCallout;
}

- (UIButton *)rightCalloutAccessoryView
{
    if (!self.myButton)
    {
        self.myButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    }
    return self.myButton;
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.image = [UIImage imageNamed:@"map-marker-private"];
    }
    return self;
}

- (id)initWithCovidAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.image = [UIImage imageNamed:@"map-marker-covid-public"];
    }
    return self;
}


@end
