//
//  SWPrivateReportMapKitAnnotationView.h
//  Sickweather
//
//  Created by John Erck on 5/1/17.
//  Copyright (c) 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SWPrivateReportMapKitAnnotationView : MKAnnotationView

- (id)initWithCovidAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;

@end
