//
//  SickScoreInterfaceTableRowControllerForTypeTopIllness.swift
//  Sickweather
//
//  Created by John Erck on 2/19/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit
import WatchKit

class SickScoreInterfaceTableRowControllerForTypeTopIllness: NSObject {
    @IBOutlet weak var containerGroup: WKInterfaceGroup!
    @IBOutlet weak var shortNameInterfaceLabel: WKInterfaceLabel!
    @IBOutlet weak var longNameInterfaceLabel: WKInterfaceLabel!
    @IBOutlet weak var bottomOfRowInterfaceSeparator: WKInterfaceSeparator!
}