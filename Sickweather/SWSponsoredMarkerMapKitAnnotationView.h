//
//  SWSponsoredMarkerMapKitAnnotationView.h
//  Sickweather
//
//  Created by John Erck on 3/31/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface SWSponsoredMarkerMapKitAnnotationView : MKAnnotationView

- (id)initWithCovidAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;

@end
