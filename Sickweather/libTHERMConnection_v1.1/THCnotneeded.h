//
//  THCnotneeded.h
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/15/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#ifndef THCnotneeded_h
#define THCnotneeded_h

- (void)deviceConnection:(CBPeripheral *)peripheral;
- (void)deviceDisconnection:(CBPeripheral *)peripheral;
- (void)deviceDiscovered:(CBPeripheral *)peripheral;
/*
 - (void)getFirmwareVersion:(NSString *)value;
 - (void)getHardwareVersion:(NSString *)value;
 - (void)getManufacturerName:(NSString *)value;
 - (void)getModelNumber:(NSString *)value;
 - (void)getSerialNumber:(NSString *)value;
 - (void)getSoftwareVersion:(NSString *)value;
 - (void)getSystemID:(NSData *)value;
 */
- (void)getBatteryLevel:(int)value;

//- (void)getAlertStatus:(int)value;
//- (void)getLEDBrightness:(int)value;

- (void)getDeviceTemperatureScale:(NSString *)scale;
- (void)getTemperature:(float)temperatureValue scale:(NSString *)temperatureScale date:(NSDate *)temperatureTimestamp;

@end

@interface THERMConnection : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate> {
    id <THERMConnectionDelegate> _delegate;
}

@property (nonatomic, strong)   id <THERMConnectionDelegate> delegate;
@property (readonly, nonatomic) CBCentralManager *bluetoothManager;
@property (readonly, nonatomic) NSMutableArray *peripherals;

+ (THERMConnection *)sharedManager;
- (void)initialize;
- (void)connectTHERM:(CBPeripheral *)peripheral;
- (void)disconnectTHERM:(CBPeripheral *)peripheral;
//- (NSString *)setLEDBrightness:(int)value;
- (void)setTemperatureScale:(int)scale;


#endif /* THCnotneeded_h */
