//
//  SWTodayWidgetAdvertisementView.m
//  Sickweather
//
//  Created by John Erck on 8/12/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import "SWTodayWidgetAdvertisementView.h"

@interface SWTodayWidgetAdvertisementView ()
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *contentViewHorizontalPushOutView;
@property (strong, nonatomic) NSLayoutConstraint *contentViewHorizontalPushOutViewWidthConstraint;
@property (strong, nonatomic) UIImageView *downArrowImage;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIImageView *sickScoreImage;
@property (strong, nonatomic) UILabel *bodyLabel;
@property (strong, nonatomic) UIButton *dismissButton;
@property (strong, nonatomic) UIButton *remindMeLaterButton;
@end

@implementation SWTodayWidgetAdvertisementView

#pragma mark - Target/Action

- (void)dismissButtonDidTouchUpInside:(id)sender
{
    [self.delegate todayWidgetAdvertisementViewDidTouchUpInsideDismissButton:self];
}

- (void)remindMeLaterButtonDidTouchUpInside:(id)sender
{
    [self.delegate todayWidgetAdvertisementViewDidTouchUpInsideRemindMeLaterButton:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT
        self.scrollView = [[UIScrollView alloc] init];
        self.contentView = [[UIView alloc] init];
        self.contentViewHorizontalPushOutView = [[UIView alloc] init];
        self.downArrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"today-overlay-arrow"]];
        self.titleLabel = [[UILabel alloc] init];
        self.sickScoreImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"today-overlay-sickscore"]];
        self.bodyLabel = [[UILabel alloc] init];
        self.dismissButton = [[UIButton alloc] init];
        self.remindMeLaterButton = [[UIButton alloc] init];
        
        // CONFIG
        
        // Scroll view
        self.scrollView.alwaysBounceHorizontal = NO;
        self.scrollView.alwaysBounceVertical = NO;
        
        // Down arrow image
        self.downArrowImage.contentMode = UIViewContentModeTop;
        
        // Title label
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Know the contagion level of your\ncurrent location with SickScore" attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18],NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}];
        
        // SickScore image
        self.sickScoreImage.contentMode = UIViewContentModeScaleAspectFit;
        
        // Body label
        self.bodyLabel.numberOfLines = 0;
        self.bodyLabel.textAlignment = NSTextAlignmentCenter;
        //self.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:@"To install the SickScore widget on your Today screen\npull down from the top of the screen, tap Edit, and add\nSickweather to the top of your Today Widgets." attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:11],NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}];
        self.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:@"To install the SickScore widget on your Today screen\npull down from the top of the screen, tap Today, scroll\nto bottom, tap Edit, then add Sickweather to the\ntop of your list." attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:11],NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}];
        
        // Dismiss button
        [self.dismissButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Okay, got it!" attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226]}] forState:UIControlStateNormal];
        [self.dismissButton.layer setBorderColor:[SWColor colorSickweatherBlue41x171x226].CGColor];
        [self.dismissButton.layer setBorderWidth:0.5];
        [self.dismissButton.layer setCornerRadius:2];
        [self.dismissButton addTarget:self action:@selector(dismissButtonDidTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        // Remind me later button
        [self.remindMeLaterButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"remind me later" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]}] forState:UIControlStateNormal];
        [self.remindMeLaterButton addTarget:self action:@selector(remindMeLaterButtonDidTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        // COLOR
        
        // Scroll view
        //self.scrollView.backgroundColor = [UIColor redColor];
        //self.contentView.backgroundColor = [UIColor greenColor];
        
        // MAKE ACCESSIBLE TO AUTO LAYOUT
        NSDictionary *viewsDictionary = @{
                                          @"scrollView":self.scrollView,
                                          @"contentView":self.contentView,
                                          @"contentViewHorizontalPushOutView":self.contentViewHorizontalPushOutView,
                                          @"downArrowImage":self.downArrowImage,
                                          @"titleLabel":self.titleLabel,
                                          @"sickScoreImage":self.sickScoreImage,
                                          @"bodyLabel":self.bodyLabel,
                                          @"dismissButton":self.dismissButton,
                                          @"remindMeLaterButton":self.remindMeLaterButton,
                                          };
        
        // CONFIG TO USE AUTO LAYOUT
        self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.contentViewHorizontalPushOutView.translatesAutoresizingMaskIntoConstraints = NO;
        self.downArrowImage.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreImage.translatesAutoresizingMaskIntoConstraints = NO;
        self.bodyLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.remindMeLaterButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ADD TO THE VIEW HIERARCHY
        [self addSubview:self.scrollView];
        [self.scrollView addSubview:self.contentView];
        [self.contentView addSubview:self.contentViewHorizontalPushOutView];
        [self.contentView addSubview:self.downArrowImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.sickScoreImage];
        [self.contentView addSubview:self.bodyLabel];
        [self.contentView addSubview:self.dismissButton];
        [self.contentView addSubview:self.remindMeLaterButton];
        
        // ADD AUTO LAYOUT RULES
        
        // Scroll view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:0 metrics:nil views:viewsDictionary]];
        
        // Content view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contentView]|" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentView]|" options:0 metrics:nil views:viewsDictionary]];
        
        // Content view horizontal push out view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[contentViewHorizontalPushOutView(1)]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentViewHorizontalPushOutView]|" options:0 metrics:nil views:viewsDictionary]];
        self.contentViewHorizontalPushOutViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.contentViewHorizontalPushOutView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
        [self addConstraint:self.contentViewHorizontalPushOutViewWidthConstraint];
        
        // Down arrow image
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[downArrowImage]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[downArrowImage]|" options:0 metrics:nil views:viewsDictionary]];
        
        // Title label
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[downArrowImage]-(30)-[titleLabel]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[titleLabel]|" options:0 metrics:nil views:viewsDictionary]];
        
        // SickScore image
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel]-(20)-[sickScoreImage]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(18)-[sickScoreImage]-(18)-|" options:0 metrics:nil views:viewsDictionary]];
        
        // Body label
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sickScoreImage]-(20)-[bodyLabel]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(18)-[bodyLabel]-(18)-|" options:0 metrics:nil views:viewsDictionary]];
        
        // Dismiss button
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bodyLabel]-(20)-[dismissButton(40)]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[dismissButton(140)]" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.dismissButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        
        // Remind me later button
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[dismissButton]-(20)-[remindMeLaterButton]-|" options:0 metrics:nil views:viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[remindMeLaterButton]|" options:0 metrics:nil views:viewsDictionary]];
    }
    return self;
}

@end
