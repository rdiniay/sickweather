//
//  WashHandsInterfaceTableRowControllerForTypeTimer.swift
//  Sickweather
//
//  Created by John Erck on 3/4/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit
import WatchKit

class WashHandsInterfaceTableRowControllerForTypeTimer: NSObject {
    @IBOutlet weak var timerGroup: WKInterfaceGroup!
    @IBOutlet weak var timerCurrentCountLabel: WKInterfaceLabel!
    @IBOutlet weak var timerSublabel: WKInterfaceLabel!
}
