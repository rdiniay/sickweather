//
//  SWAddTemperatureViewController.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTemperatureGuageView.h"
#import "SWFamilyMemberModal.h"
#import "Sickweather-Swift.h"

@interface SWAddTemperatureViewController : UIViewController

// Properties
@property (nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;

// Outlets
@property (strong , nonatomic) IBOutlet SWTemperatureGuageView *guageView;
@property (strong , nonatomic) IBOutlet UILabel *lblSeverity;
@property (strong , nonatomic) IBOutlet UITextField *tempTxtField;
@property (strong , nonatomic) IBOutlet UIButton *connectBluetoothDeviceButton;
@property (weak, nonatomic) IBOutlet UIView *knownDeviceListView;
@property (weak, nonatomic) IBOutlet UIView *swaiveDevice;
@property (weak, nonatomic) IBOutlet UIView *kinsaDevice;
@property (weak, nonatomic) IBOutlet UIView *philipsDevice;

// Constraint outlets
@property (weak, nonatomic) IBOutlet Switch *switchTemperatureScale;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *knownDeviceListTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *knownDeviceListHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *swaiveDeviceHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *swaiveDeviceBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *kinsaDeviceHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *kinsaDeviceBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *philipsDeviceHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *philipsDeviceBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *connectBluetoothDeviceButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *connectBluetoothDeviceTopConstraint;
//Action
- (IBAction)temperatureScaleValueChanged:(Switch *)sender;

@end
