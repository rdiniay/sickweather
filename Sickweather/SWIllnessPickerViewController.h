//
//  SWIllnessPickerViewController.h
//  Sickweather
//
//  Created by John Erck on 10/11/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWIllnessPickerDelegate <NSObject>

- (void)sicknessTypeIdSelected:(NSString *)sicknessTypeId objectFromDocsUsingName: (NSString *) objectFromDocsUsingName;

@end

@interface SWIllnessPickerViewController : UITableViewController
@property (nonatomic, assign) id <SWIllnessPickerDelegate> delegate;
@property (nonatomic, strong) NSString *selectedSicknessTypeId;
@property (nonatomic) BOOL isCovidScore;
@end
