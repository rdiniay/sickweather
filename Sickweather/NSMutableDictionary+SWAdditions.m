//
//  NSMutableDictionary+SWAdditions.m
//  Sickweather
//
//  Created by John Erck on 1/31/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "NSMutableDictionary+SWAdditions.h"

@implementation NSMutableDictionary (SWAdditions)

+ (NSMutableDictionary *)createDictionaryForSectionIndex:(NSArray *)array
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (char firstChar = 'a'; firstChar <= 'z'; firstChar++)
    {
        NSString *firstCharacter = [NSString stringWithFormat:@"%c", firstChar];
        NSArray *content = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name beginswith[cd] %@", firstCharacter]];
        NSMutableArray *mutableContent = [NSMutableArray arrayWithArray:content];
        
        if ([mutableContent count] > 0)
        {
            NSString *key = [firstCharacter uppercaseString];
            [dict setObject:mutableContent forKey:key];
            //NSLog(@"%@: %lu", key, (unsigned long)[mutableContent count]);
        }
    }
    return dict;
}

@end
