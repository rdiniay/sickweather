//
//  SWMyReportsMessageTableViewHeader.m
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWMyReportsHeaderView.h"

@interface SWMyReportsHeaderView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIView *backgroundImageViewOverlayView;
@property (strong, nonatomic) UILabel *titleLabel;
@end

@implementation SWMyReportsHeaderView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWMyReportsMessageTableViewHeaderDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    [self reloadDelegateInfo];
}

#pragma mark - Taget/Action

#pragma mark - Public Methods

- (void)reloadDelegateInfo
{
    // UPDATE PROPS
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.backgroundImageView = [[UIImageView alloc] init];
        self.backgroundImageViewOverlayView = [[UIView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.backgroundImageView forKey:@"backgroundImageView"];
        [self.viewsDictionary setObject:self.backgroundImageViewOverlayView forKey:@"backgroundImageViewOverlayView"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundImageViewOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.backgroundImageView];
        [self addSubview:self.backgroundImageViewOverlayView];
        [self addSubview:self.titleLabel];
        
        // LAYOUT
        
        // Layout IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout IMAGE VIEW OVERLAY VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TITLE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[titleLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(15)-[titleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config HEADER
        
        // Config IMAGE VIEW
        //self.backgroundImageView.image = [UIImage imageNamed:@""];
        self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backgroundImageView.clipsToBounds = YES;
        
        // Config IMAGE VIEW OVERLAY VIEW
        self.backgroundImageViewOverlayView.backgroundColor = [SWColor color:[SWColor colorSickweatherDarkGrayText104x104x104] usingOpacity:0.9];
        
        // Config TITLE LABEL
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        /*
        self.titleLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
         */
    }
    return self;
}

@end
