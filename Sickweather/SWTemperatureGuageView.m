//
//  SWTemperatureGuageView.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWTemperatureGuageView.h"
#define DEG_TO_RAD(angle) ((angle) / 180.0 * M_PI)
#define ARC_LENGTH 2.47312

@interface SWTemperatureGuageView ()
@property (nonatomic) CGRect viewRect;
@property int meterCount; // used for upper guage limit
@end

@implementation SWTemperatureGuageView

- (void)drawRect:(CGRect)rect {
    self.viewRect = rect;
	if (self.isCelsiusTemperatureScaleType) {
		if (self.temperature > 65) {
			self.temperature = 65.0;
		}
	}else{
		if (self.temperature > 150) {
			self.temperature = 150.0;
		}
	}
	
    // The guage consists of 47 fragments
    self.arcFragments = [[NSMutableArray alloc] init];
    for (int i = 0; i < 47; i++) {
        // Explanation:
        // - 155' is the starting angle of first fragment, all are going to be drawn clockwise
        // - Each fragment is separated with space itself would take, so the gap between two  fragment's starting angle would be two fragments. Hence ARC_LENGTH*2 is used
        // - Lastly, end angle is increased by ARC_LENGTH compared to start angle; for its actual length.
        struct ArcFragment* fragment = malloc(sizeof(struct ArcFragment));
        fragment->startAngle = 155 + i*ARC_LENGTH*2;
        fragment->endAngle = 155 + i*ARC_LENGTH*2 + ARC_LENGTH;
        NSValue* value = [NSValue valueWithBytes:fragment objCType:@encode(struct ArcFragment)];
        [self.arcFragments addObject:value];
    }
	if (self.isCelsiusTemperatureScaleType){
		self.meterCount = self.temperature / 65 * 47;// metercount is scaled between 0' to 65' on a 47 unit guage
		
	}else {
		self.meterCount = self.temperature / 150 * 47; // metercount is scaled between 0' to 150' on a 47 unit guage
		
	}
    [self drawAllFragments];
}

-(void)drawAllFragments
{
    UIColor *guageColor;
	if (self.isCelsiusTemperatureScaleType){
		if(self.temperature >= 38.9){
			guageColor = [UIColor colorWithRed:255/255.0 green:1/255.0 blue:0/255.0 alpha:1.0];
		} else if(self.temperature >= 37.8){
			guageColor = [UIColor colorWithRed:252/255.0 green:111/255.0 blue:6/255.0 alpha:1.0];
		} else if(self.temperature >= 37.4){
			guageColor = [UIColor colorWithRed:249/255.0 green:237/255.0 blue:11/255.0 alpha:1.0];
		} else {
			guageColor = [UIColor colorWithRed:68/255.0 green:157/255.0 blue:68/255.0 alpha:1.0];
		}
	}else {
		if(self.temperature >= 102){
			guageColor = [UIColor colorWithRed:255/255.0 green:1/255.0 blue:0/255.0 alpha:1.0];
		} else if(self.temperature >= 99.4){
			guageColor = [UIColor colorWithRed:252/255.0 green:111/255.0 blue:6/255.0 alpha:1.0];
		} else if(self.temperature >= 98){
			guageColor = [UIColor colorWithRed:249/255.0 green:237/255.0 blue:11/255.0 alpha:1.0];
		} else {
			guageColor = [UIColor colorWithRed:68/255.0 green:157/255.0 blue:68/255.0 alpha:1.0];
		}
	}
    for (int i = 0; i<self.meterCount; i++) {
        struct ArcFragment fragment;
        NSValue* value = [self.arcFragments objectAtIndex:i];
        [value getValue:&fragment];
        [self drawFragment:fragment :guageColor];
    }
    
    for (int i = self.meterCount; i<self.arcFragments.count; i++) {
        struct ArcFragment fragment;
        NSValue* value = [self.arcFragments objectAtIndex:i];
        [value getValue:&fragment];
        [self drawFragment:fragment :[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];
    }
}

-(void) drawFragment:(struct ArcFragment)fragment :(UIColor*)color
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 20.0);
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextAddArc(context,
                    (self.viewRect.origin.x+self.viewRect.size.width)/2 , // x of center
                    (self.viewRect.origin.y+self.viewRect.size.height)*2/3, // y of center
                    self.viewRect.size.height*3/5, // radius
                    DEG_TO_RAD(fragment.startAngle), // starting angle
                    DEG_TO_RAD(fragment.endAngle), // ending angle
                    0); // clockwise:1, c-clockwise:0
    CGContextStrokePath(context);
}

@end
