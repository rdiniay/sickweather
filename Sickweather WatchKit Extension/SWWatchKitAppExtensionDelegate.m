//
//  SWWatchKitAppExtensionDelegate.m
//  Sickweather
//
//  Created by John Erck on 9/29/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import "SWWatchKitAppExtensionDelegate.h"

@interface SWWatchKitAppExtensionDelegate () <WKExtensionDelegate, WCSessionDelegate>

@end

@implementation SWWatchKitAppExtensionDelegate

#pragma mark - WCSessionDelegate

- (void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(NSError *)error
{
    //NSLog(@"activationDidCompleteWithState = %@", @(activationState));
}

#pragma mark - WKExtensionDelegate

// The ExtensionDelegate's applicationDidFinishLaunching will only get called when the WatchKit App is launched
- (void)applicationDidFinishLaunching
{
    //NSLog(@"SWWatchKitAppExtensionDelegate applicationDidFinishLaunching");
}

// In the case of a complication update, the app doesn't get launched (only the extension) therefore that method
// never gets invoked. If you move the WCSession activate code to the ExtensionDelegate's init method, you will find it works.
- (id)init
{
    self = [super init];
    if (self)
    {
        //NSLog(@"SWWatchKitAppExtensionDelegate init");
        self.session = [WCSession defaultSession];
        self.session.delegate = self;
        [self.session activateSession];
        if (self.session.activationState == WCSessionActivationStateActivated)
        {
            //NSLog(@"WCSession is WCSessionActivationStateActivated");
        }
    }
    return self;
}

@end
