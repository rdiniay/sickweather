//
//  SWHandWashTimerImageGenerator.swift
//  Sickweather
//
//  Created by John Erck on 3/18/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import Foundation
import UIKit

@objc class SWHandWashTimerImageGenerator: NSObject {
    
    class func redColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 1/255.0, blue: 0/255.0, alpha: 1)
    }
    
    class func orangeColor() -> UIColor {
        return UIColor(red: 252/255.0, green: 111/255.0, blue: 6/255.0, alpha: 1)
    }
    
    class func yellowColor() -> UIColor {
        return UIColor(red: 249/255.0, green: 237/255.0, blue: 11/255.0, alpha: 1)
    }
    
    class func blueColor() -> UIColor {
        return UIColor(red: 41/255.0, green: 171/255.0, blue: 226/255.0, alpha: 1)
    }
    
    class func clearColor() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    }
    
    class func grayCircleColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.15)
    }
    
    class func degree360InRadians() -> Double {
        return Double(Double.pi)*2
    }
    
    class func getRadiansCompleteForFrameOfTotal(_ frame: Int, total: Int) -> Double {
        return degree360InRadians()*(Double(frame)/Double(total))
    }
    
    class func loadImages(_ images: [UIImage], framePosition: Int = 0) {
        var images = images
        if images.count == 0 {
            // We're done...
        } else {
            print("images count = \(images.count)")
            if let firstImage = images.first {
                let firstImageData = firstImage.pngData()
                SWHelper.helperAppDelegate().sickweatherAppBackend.appBackendWriteDataFileNamed("timer-frame-\(framePosition)@2x.png", fileContentType: "image/png", fileContent: firstImageData) { (jsonResponseBody, responseBodyAsString, responseHeaders, requestError, jsonParseError, requestURLAbsoluteString) -> Void in
                    print("jsonResponseBody = \(jsonResponseBody!)")
                    print("responseBodyAsString = \(responseBodyAsString!)")
                    print("responseHeaders = \(responseHeaders!)")
                    print("requestError = \(requestError!)")
                    print("jsonParseError = \(jsonParseError!)")
                    print("requestURLAbsoluteString = \(requestURLAbsoluteString!)")
                    images.remove(at: 0)
                    self.loadImages(images, framePosition: framePosition + 1)
                }
            }
        }
    }
    
    class func createImages() {
        
        let π = Double(Double.pi)
        let startingBlueFillColor = UIColor(red: 36/255.0, green: 169/255.0, blue: 225/255.0, alpha: 1)
        let totalFrameCount = Int(SWHelper.helperHandWashTimerTotalFrameCount().intValue) // Should sync with watch app extension's SWHelper.helperHandWashTimerTotalFrameCount()
//        let timerRedColor = UIColor(hue: 0.0, saturation: 1.0, brightness: 1.0, alpha: 1.0)
//        let timerOrangeColor = UIColor(hue: 0.07, saturation: 0.97, brightness: 0.98, alpha: 1.0)
//        let timerYellowColor = UIColor(hue: 0.16, saturation: 0.94, brightness: 0.96, alpha: 1.0)
        let timerGreenColor = UIColor(hue: 0.33, saturation: 0.56, brightness: 0.61, alpha: 1.0)
        let timerWhiteColor = UIColor(hue: 0.0, saturation: 0.0, brightness: 1.0, alpha: 1.0)
        var imagesToAnimate = Array<UIImage>()
        for frameindex in 0...totalFrameCount {
            print("\(frameindex)")
            let radiansComplete = getRadiansCompleteForFrameOfTotal(frameindex, total: totalFrameCount)
            print("radiansComplete = \(radiansComplete)")
            
            var currentColor = timerWhiteColor
            let durationPerThird = Double(SWHelper.helperHandWashTimerDuration().doubleValue)/3.0
            let framesPerThird = durationPerThird * Double(SWHelper.helperHandWashTimerFramesPerSecond())
            var frameIndexAdjustedForCurrentThird = 0
            
            // Red to orange...
            if radiansComplete < (degree360InRadians()/3)*1 {
                
                // let timerRedColor = UIColor(hue: 0.0, saturation: 1.0, brightness: 1.0, alpha: 1.0)
                // let timerOrangeColor = UIColor(hue: 0.07, saturation: 0.97, brightness: 0.98, alpha: 1.0)
                frameIndexAdjustedForCurrentThird = frameindex
                let hueStarting = 0.0
                let hueEnding = 0.07
                let hueRangeToIterate = hueEnding - hueStarting
                let hueStep = hueRangeToIterate / framesPerThird
                let hueCurrent = hueStarting + Double(frameIndexAdjustedForCurrentThird) * hueStep
                
                let saturationStarting = 1.0
                let saturationEnding = 0.97
                let saturationRangeToIterate = saturationEnding - saturationStarting
                let saturationStep = saturationRangeToIterate / framesPerThird
                let saturationCurrent = saturationStarting + Double(frameIndexAdjustedForCurrentThird) * saturationStep
                
                let brightnessStarting = 1.0
                let brightnessEnding = 0.98
                let brightnessRangeToIterate = brightnessEnding - brightnessStarting
                let brightnessStep = brightnessRangeToIterate / framesPerThird
                let brightnessCurrent = brightnessStarting + Double(frameIndexAdjustedForCurrentThird) * brightnessStep
                
                currentColor = UIColor(hue: CGFloat(hueCurrent), saturation: CGFloat(saturationCurrent), brightness: CGFloat(brightnessCurrent), alpha: 1.0)
                
                if radiansComplete == 0 {
                    currentColor = timerWhiteColor
                }
                
            }
            // Orange to yellow...
            else if radiansComplete < (degree360InRadians()/3)*2 {
                
                // let timerOrangeColor = UIColor(hue: 0.07, saturation: 0.97, brightness: 0.98, alpha: 1.0)
                // let timerYellowColor = UIColor(hue: 0.16, saturation: 0.94, brightness: 0.96, alpha: 1.0)
                frameIndexAdjustedForCurrentThird = frameindex - (totalFrameCount/3) * 1
                let hueStarting = 0.07
                let hueEnding = 0.16
                let hueRangeToIterate = hueEnding - hueStarting
                let hueStep = hueRangeToIterate / framesPerThird
                let hueCurrent = hueStarting + Double(frameIndexAdjustedForCurrentThird) * hueStep
                
                let saturationStarting = 0.97
                let saturationEnding = 0.94
                let saturationRangeToIterate = saturationEnding - saturationStarting
                let saturationStep = saturationRangeToIterate / framesPerThird
                let saturationCurrent = saturationStarting + Double(frameIndexAdjustedForCurrentThird) * saturationStep
                
                let brightnessStarting = 0.98
                let brightnessEnding = 0.96
                let brightnessRangeToIterate = brightnessEnding - brightnessStarting
                let brightnessStep = brightnessRangeToIterate / framesPerThird
                let brightnessCurrent = brightnessStarting + Double(frameIndexAdjustedForCurrentThird) * brightnessStep
                
                currentColor = UIColor(hue: CGFloat(hueCurrent), saturation: CGFloat(saturationCurrent), brightness: CGFloat(brightnessCurrent), alpha: 1.0)
                
            }
            // Yellow to green...
            else if radiansComplete < (degree360InRadians()/3)*3 {
                
                // let timerYellowColor = UIColor(hue: 0.16, saturation: 0.94, brightness: 0.96, alpha: 1.0)
                // let timerGreenColor = UIColor(hue: 0.33, saturation: 0.56, brightness: 0.61, alpha: 1.0)
                frameIndexAdjustedForCurrentThird = frameindex - (totalFrameCount/3) * 2
                let hueStarting = 0.16
                let hueEnding = 0.33
                let hueRangeToIterate = hueEnding - hueStarting
                let hueStep = hueRangeToIterate / framesPerThird
                let hueCurrent = hueStarting + Double(frameIndexAdjustedForCurrentThird) * hueStep
                
                let saturationStarting = 0.94
                let saturationEnding = 0.56
                let saturationRangeToIterate = saturationEnding - saturationStarting
                let saturationStep = saturationRangeToIterate / framesPerThird
                let saturationCurrent = saturationStarting + Double(frameIndexAdjustedForCurrentThird) * saturationStep
                
                let brightnessStarting = 0.96
                let brightnessEnding = 0.61
                let brightnessRangeToIterate = brightnessEnding - brightnessStarting
                let brightnessStep = brightnessRangeToIterate / framesPerThird
                let brightnessCurrent = brightnessStarting + Double(frameIndexAdjustedForCurrentThird) * brightnessStep
                
                currentColor = UIColor(hue: CGFloat(hueCurrent), saturation: CGFloat(saturationCurrent), brightness: CGFloat(brightnessCurrent), alpha: 1.0)
                
            }
            // Green...
            else {
                currentColor = timerWhiteColor
            }
            
            let topStroke = 4.0
            let bottomStroke = 4.0
            var startAngle = 0.0
            var endAngle = 0.0
            
            // Create the drawing context
            let size = CGSize(width: 110, height: 110)
            let opaque = true
            let scale: CGFloat = 2.0 // The only scale for Apple Watch as of right now
            UIGraphicsBeginImageContextWithOptions(size, opaque, scale)
            
            // Get the context
            let ctx = UIGraphicsGetCurrentContext()
            
            // Set base drawing props
            ctx?.setLineCap(CGLineCap.round)
            
            // Set circle drawing props
            ctx?.setLineWidth(CGFloat(bottomStroke) * scale)
            if radiansComplete == degree360InRadians() {
                ctx?.setFillColor(timerGreenColor.cgColor)
                ctx?.setStrokeColor(timerWhiteColor.cgColor)
            } else if radiansComplete == 0 {
                ctx?.setFillColor(startingBlueFillColor.cgColor)
                currentColor = timerWhiteColor
            } else {
                ctx?.setStrokeColor(grayCircleColor().cgColor)
                ctx?.setFillColor(UIColor.clear.cgColor)
            }
            
            // Draw base circle
            var circleRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            circleRect = circleRect.insetBy(dx: 5, dy: 5)
            ctx?.strokeEllipse(in: circleRect)
            ctx?.fillEllipse(in: circleRect)
            
            // Update drawing props for top layer arc
            ctx?.setLineWidth(CGFloat(topStroke) * scale)
            
            let centerX: CGFloat = 55
            let centerY: CGFloat = 55
            let radius: CGFloat = 50
            
            print("degree360InRadians/4 = \(degree360InRadians()/4)")
            if radiansComplete > degree360InRadians()/4 || radiansComplete == 0 {
                // The "2nd half" of how we draw things...
                //println("The 2nd half of how we draw things...")
                startAngle = -π/2
                endAngle = startAngle + degree360InRadians()/4
                //CGContextAddArc(ctx, centerX, centerY, radius, CGFloat(startAngle), CGFloat(endAngle), 0)
                ctx?.addArc(center: CGPoint(x: centerX, y: centerY), radius: radius, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: false)
                //println("CGContextAddArc(ctx, centerX = \(centerX), centerY = \(centerY), radius = \(radius), startAngle = \(startAngle), endAngle = \(endAngle), 0)")
                ctx?.setStrokeColor(currentColor.cgColor)
                ctx?.strokePath()
                
                startAngle = 0
                endAngle = radiansComplete - degree360InRadians()/4
                //CGContextAddArc(ctx, centerX, centerY, radius, CGFloat(startAngle), CGFloat(endAngle), 0)
                ctx?.addArc(center: CGPoint(x: centerX, y: centerY), radius: radius, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: false)
                //println("CGContextAddArc(ctx, centerX = \(centerX), centerY = \(centerY), radius = \(radius), startAngle = \(startAngle), endAngle = \(endAngle), 0)")
                ctx?.setStrokeColor(currentColor.cgColor)
                ctx?.strokePath()
            }
            else
            {
                // The "1st half" of how we draw things...
                //println("The 1st half of how we draw things...")
                startAngle = -π/2
                endAngle = startAngle + radiansComplete
                //CGContextAddArc(ctx, centerX, centerY, radius, CGFloat(startAngle), CGFloat(endAngle), 0)
                ctx?.addArc(center: CGPoint(x: centerX, y: centerY), radius: radius, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: false)
                //println("CGContextAddArc(ctx, centerX = \(centerX), centerY = \(centerY), radius = \(radius), startAngle = \(startAngle), endAngle = \(endAngle), 0)")
                ctx?.setStrokeColor(currentColor.cgColor)
                ctx?.strokePath()
            }
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            imagesToAnimate.append(image!)
            
            UIGraphicsEndImageContext()
        }
        
        loadImages(imagesToAnimate)
    }
}
