//
//  SWColor.swift
//  Sickweather
//
//  Created by John Erck on 5/1/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit

class SWColor: NSObject
{
    class func redColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 1/255.0, blue: 0/255.0, alpha: 1)
    }
    
    class func orangeColor() -> UIColor {
        return UIColor(red: 252/255.0, green: 111/255.0, blue: 6/255.0, alpha: 1)
    }
    
    class func yellowColor() -> UIColor {
        return UIColor(red: 249/255.0, green: 237/255.0, blue: 11/255.0, alpha: 1)
    }
    
    class func blueColor() -> UIColor {
        return UIColor(red: 41/255.0, green: 171/255.0, blue: 226/255.0, alpha: 1)
    }
    
    class func clearColor() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    }
    
    class func whiteColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
    }
    
    class func blackColor() -> UIColor {
        return UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
    }
    
    class func grayColor() -> UIColor
    {
        return UIColor(red: 170/255.0, green: 164/255.0, blue: 174/255.0, alpha: 1)
    }
    
    class func grayCircleColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.15)
    }
}
