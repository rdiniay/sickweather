//
//  SWOnboardingView.h
//  Sickweather
//
//  Created by John Erck on 12/20/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWOnboardingViewDelegate
- (void)onboardingViewWasTapped:(id)sender;
@end

@interface SWOnboardingView : UIView
@property (nonatomic, weak) id<SWOnboardingViewDelegate> delegate;
- (void)updateBackgroundColor:(UIColor *)color;
- (void)updateImage:(UIImage *)image;
- (void)updateTitle:(NSString *)title;
- (void)updateDescription:(NSString *)description;
@end
