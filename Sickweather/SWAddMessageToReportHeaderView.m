//
//  SWAddMessageToReportMessageTableViewHeader.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportHeaderView.h"
#import "SWColor.h"
#import "SWFont.h"

@interface SWAddMessageToReportHeaderView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIView *backgroundImageViewOverlayView;
@property (strong, nonatomic) UILabel *titleLabel;
@end

@implementation SWAddMessageToReportHeaderView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWAddMessageToReportMessageTableViewHeaderDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    [self reloadDelegateInfo];
}

#pragma mark - Taget/Action

#pragma mark - Public Methods

- (void)reloadDelegateInfo
{
    // UPDATE PROPS
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.backgroundImageView = [[UIImageView alloc] init];
        self.backgroundImageViewOverlayView = [[UIView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.backgroundImageView forKey:@"backgroundImageView"];
        [self.viewsDictionary setObject:self.backgroundImageViewOverlayView forKey:@"backgroundImageViewOverlayView"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundImageViewOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.backgroundImageView];
        [self addSubview:self.backgroundImageViewOverlayView];
        [self addSubview:self.titleLabel];
        
        // LAYOUT
        
        // Layout IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout IMAGE VIEW OVERLAY VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[backgroundImageViewOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TITLE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[titleLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(30)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config HEADER
        
        // Config IMAGE VIEW
        //self.backgroundImageView.image = [UIImage imageNamed:@""];
        self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backgroundImageView.clipsToBounds = YES;
        
        // Config IMAGE VIEW OVERLAY VIEW
        
        // Config TITLE LABEL
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Would you like to share your report with any of your groups?" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
        self.titleLabel.numberOfLines = 0;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        /*
        self.titleLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
         */
    }
    return self;
}

@end
