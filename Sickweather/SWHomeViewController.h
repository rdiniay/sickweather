//
//  SWHomeViewController.h
//  Sickweather
//
//  Created by John Erck on 7/11/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWHomeViewController : UIViewController
- (void)showGroupUsingSWFoursquareId:(NSString *)swFoursquareId;
@end
