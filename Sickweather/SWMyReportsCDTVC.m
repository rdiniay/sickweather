//
//  SWMyReportsCDTVC.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWMyReportsCDTVC.h"
#import "SWReportCDM.h"

@interface SWMyReportsCDTVC ()

@end

@implementation SWMyReportsCDTVC

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    if (managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SWReportCDM"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"unique" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = nil; // all reports
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get cell
    static NSString *CellIdentifier = @"SWMyReportsCDTVCCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Get data
    SWReportCDM *report = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Config cell
    cell.textLabel.text = [NSString stringWithFormat:@"Example data = %@", report];
    
    // Return cell
    return cell;
}

@end
