//
//  SWSymptomsWithTempHeaderView.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/10/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWSymptomsWithTempHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblFever;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@end
