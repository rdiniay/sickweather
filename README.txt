OUTSTANDING ISSUES WORTH NOTING:

1) https://github.com/flurry/flurry-ios-sdk/issues/91
^ As of 14 hours ago (10/11/2017), these guys have NOT published a fix for what we are seeing in the logs:

Main Thread Checker: UI API called on a background thread: -[UIApplication statusBarOrientation]
PID: 1277, TID: 830644, Thread name: (none), Queue name: com.Flurry.Analytics.Session, QoS: 0
Backtrace:
4   Sickweather                         0x0000000104ffb37c +[FlurryiOSUtil canvasInLandscapeRight] + 56
5   Sickweather                         0x0000000104ffb320 +[FlurryiOSUtil canvasInLandscape] + 32

^ this junk. Not critical. Ignore for now, fix later with their updated SDK (8.3.0) whenever they release it.

2) Right now we build and archive for "Architectures: armv7 and arm64". arm64 is the newest and best. Before that was armv7 and before that was armv6.
Checkout this chart here: https://developer.apple.com/library/content/documentation/DeviceInformation/Reference/iOSDeviceCompatibility/DeviceCompatibilityMatrix/DeviceCompatibilityMatrix.html
All the new phones can do everything. However, the iPhone 5c is the first device (when going back in time) that cannot run arm64 code. The iPhone 5s can.
We could go to arm64 exclusive if ever relevant in the not too far out future

3) Right now we are seeing "Bitcode: Not included" as part of our archiving and distribution process. It would be great to support
Bitcode at some point. I believe we don't currently suppor this due to some of our embedded SDKs being so old (pre-bitcode era).
However, let's make it a goal to upgrade those SDKs and get our app up to speed with Bitcode.

4) I have to set my location service setting to “always” in order to report. If I set my location service to “while using the app” it still won’t let me report. Is “always” necessary to report only?