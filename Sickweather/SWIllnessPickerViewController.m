//
//  SWIllnessPickerViewController.m
//  Sickweather
//
//  Created by John Erck on 10/11/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "Flurry.h"
#import "SWIllnessPickerViewController.h"
#import "SWSettings.h"
#import "SWIllnessModel.h"
#import "SWIllnessCDM+SWAdditions.h"
#import "SWNavigationController.h"

#define MY_REPORTS_SECTION 0
#define GROUP_ILLNESS_SECTION 1
#define INDIVIDUAL_ILLNESS_SECTION 2

@interface SWIllnessPickerViewController () <UITableViewDataSource, UITableViewDelegate, SWSignInWithFacebookViewControllerDelegate>
//@property (strong, nonatomic) NSArray *getIllnessesArray;
@property (nonatomic, strong) NSArray *illnesses;
@property (nonatomic, strong) NSArray *groupedIllnesses;
@property (nonatomic, strong) NSArray *individualIllnesses;
@property (nonatomic, strong) NSIndexPath *selectedSicknessIndexPath;
@end

@implementation SWIllnessPickerViewController

#pragma mark - Custom Getters/Setters

- (NSIndexPath *)selectedSicknessIndexPath
{
    if (!_selectedSicknessIndexPath) {
        _selectedSicknessIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    return _selectedSicknessIndexPath;
}

#pragma mark - SWSignInWithFacebookViewControllerDelegate

- (void)didDismissWithSuccessfulLogin:(SWSignInWithFacebookViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didDismissWithoutLoggingIn:(SWSignInWithFacebookViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Target/Action

- (IBAction)dismissIconPressed:(id)sender
{
    [Flurry logEvent:@"Close" withParameters:@{@"Screen Name": @"Illness Map"}];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MY_REPORTS_SECTION)
    {
        return 50;
    }
    if (indexPath.section == GROUP_ILLNESS_SECTION)
    {
        return 50;
    }
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MY_REPORTS_SECTION)
    {
        if (![SWHelper helperUserIsLoggedIn])
        {
            // ASK THEM TO LOG IN...
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWSignInWithFacebookViewController *signInWithFacebookViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWSignInWithFacebookViewController"];
            signInWithFacebookViewController.delegate = self;
            SWNavigationController *signInWithFacebookNavigationController = [[SWNavigationController alloc] initWithRootViewController:signInWithFacebookViewController];
            [self presentViewController:signInWithFacebookNavigationController animated:YES completion:^{}];
            return;
        }
    }
    
    // Get cells
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
    UITableViewCell *previouslySelectedCell = [self.tableView cellForRowAtIndexPath:self.selectedSicknessIndexPath];
    
    // Switch the previously selected cell's image to a checkmark-clear
    UIImage *checkmarkClear = [UIImage imageNamed:@"checkmark-clear"];
    previouslySelectedCell.imageView.image = checkmarkClear;
    
    // Switch the selected cell's image to a checkmark
    UIImage *checkmark = [UIImage imageNamed:@"checkmark"];
    selectedCell.imageView.image = checkmark;
    
    // Get data
    SWIllnessCDM *illness;
    if (indexPath.section == MY_REPORTS_SECTION)
    {
		if ([SWIllnessCDM illnessGetMyReports].count > 0){
			 illness = [[SWIllnessCDM illnessGetMyReports] objectAtIndex:0];
		}
    }
    if (indexPath.section == GROUP_ILLNESS_SECTION)
    {
		illness = self.groupedIllnesses[indexPath.row];
    }
    if (indexPath.section == INDIVIDUAL_ILLNESS_SECTION)
    {
		illness = self.individualIllnesses[indexPath.row];
    }
	
    // Update model & delegate
	if (illness != nil) {
		self.selectedSicknessTypeId = illness.unique;
		self.selectedSicknessIndexPath = indexPath;
        NSString *objectFromDocsUsingName = self.isCovidScore ? @"trending_keywords" : @"illnesses";
        [self.delegate sicknessTypeIdSelected:illness.unique objectFromDocsUsingName:objectFromDocsUsingName];
	}
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == MY_REPORTS_SECTION)
    {
        return @"Reported Illnesses";
    }
    if (section == GROUP_ILLNESS_SECTION)
    {
        return @"Grouped Illnesses";
    }
    if (section == INDIVIDUAL_ILLNESS_SECTION)
    {
        return @"Individual Illnesses";
    }
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    if (section == MY_REPORTS_SECTION)
    {
        return 1;
    }
    if (section == GROUP_ILLNESS_SECTION)
    {
        return [self.groupedIllnesses count];
    }
    if (section == INDIVIDUAL_ILLNESS_SECTION)
    {
        return [self.individualIllnesses count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Create cell
    NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:18],NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0]};
    NSDictionary *detailedAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104]};
   // NSDictionary *sponsoredAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104]};
    static NSString *CellIdentifier = kSWStoryboardIDIllnessPickerTableViewCell;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Get cell data
    SWIllnessCDM *illness;
    if (indexPath.section == MY_REPORTS_SECTION)
    {
        NSArray* illnessesArray = [SWIllnessCDM illnessGetMyReports];
        if(illnessesArray.count > 0) {
            illness = [illnessesArray objectAtIndex:0];
            cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:illness.name attributes:titleAttributes];
        }
    }
    if (indexPath.section == GROUP_ILLNESS_SECTION)
    {
        illness = self.groupedIllnesses[indexPath.row];
        cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:illness.name attributes:titleAttributes];
//        for (NSDictionary *serverIllness in self.getIllnessesArray)
//        {
//            if ([[serverIllness objectForKey:@"id"] isEqualToString:illness.unique])
//            {
//                if ([[serverIllness objectForKey:@"sponsor_text"] isKindOfClass:[NSString class]])
//                {
//                    NSMutableAttributedString *titleAttrText = [[NSMutableAttributedString alloc] init];
//                    [titleAttrText appendAttributedString:[[NSAttributedString alloc] initWithString:illness.name attributes:titleAttributes]];
//                    [titleAttrText appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [serverIllness objectForKey:@"sponsor_text"]] attributes:sponsoredAttributes]];
//                    cell.textLabel.attributedText = titleAttrText;
//                }
//            }
//        }
    }
    if (indexPath.section == INDIVIDUAL_ILLNESS_SECTION)
    {
        illness = self.individualIllnesses[indexPath.row];
        cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:illness.name attributes:titleAttributes];
    }
    
    // Configure cell
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.detailTextLabel.attributedText = [[NSAttributedString alloc] initWithString:(illness.illnessDescription != nil ? illness.illnessDescription : [[NSString alloc] init])  attributes:detailedAttributes];
    if ([illness.unique isEqualToString:self.selectedSicknessTypeId])
    {
        // Switch image to checkmark!!
        UIImage *checkmark = [UIImage imageNamed:@"checkmark"];
        cell.imageView.image = checkmark;
    }
    else
    {
        // Switch image to checkmark-clear!!
        UIImage *checkmark = [UIImage imageNamed:@"checkmark-clear"];
        cell.imageView.image = checkmark;
    }
    
    // Serve it up!!
    return cell;
}

#pragma mark - Life Cycle Methods

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissIconPressed:)];
    
    // Config RIGHT BAR BUTTON ITEM
    self.navigationController.navigationItem.rightBarButtonItem = nil;
    self.navigationController.navigationBar.translucent = NO;
    
    // Get network controlled getIllnesses.php data
//    NSData *data = [NSData dataWithContentsOfFile:[SWHelper helperGetIllnessesPath]];
//    if (data)
//    {
//        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        //NSLog(@"myString = %@", myString);
//        NSError *jsonParseError;
//        self.getIllnessesArray = (NSArray *)[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
//    }
	
    // Load options
    self.individualIllnesses = [SWIllnessCDM illnessGetAllIndividuals];
    self.groupedIllnesses = [SWIllnessCDM illnessGetAllGrouped];
    
    // RELOAD TABLE
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
