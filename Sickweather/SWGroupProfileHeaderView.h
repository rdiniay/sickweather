//
//  SWGroupProfileMessageTableViewHeader.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWGroupProfileHeaderView;

@protocol SWGroupProfileMessageTableViewHeaderDelegate
- (NSString *)groupProfileHeaderViewTitleString:(SWGroupProfileHeaderView *)sender;
- (NSString *)groupProfileHeaderViewAddressString:(SWGroupProfileHeaderView *)sender;
- (NSString *)groupProfileHeaderViewReportCountString:(SWGroupProfileHeaderView *)sender;
- (NSString *)groupProfileHeaderViewFollowerCountString:(SWGroupProfileHeaderView *)sender;
- (NSString *)groupProfileHeaderViewAdminCountString:(SWGroupProfileHeaderView *)sender;
- (BOOL)groupProfileHeaderViewUserIsFollowingGroup:(SWGroupProfileHeaderView *)sender;
- (BOOL)groupProfileHeaderViewUserIsGroupAdmin:(SWGroupProfileHeaderView *)sender;
- (void)groupProfileHeaderViewFollowButtonTapped:(SWGroupProfileHeaderView *)sender;
- (void)groupProfileHeaderViewClaimButtonTapped:(SWGroupProfileHeaderView *)sender;
- (void)groupProfileHeaderViewEllipsesButtonTapped:(SWGroupProfileHeaderView *)sender;
@end

@interface SWGroupProfileHeaderView : UIView
@property (nonatomic, weak) id<SWGroupProfileMessageTableViewHeaderDelegate> delegate;
@property (assign) NSUInteger sectionIndex;
- (void)reloadDelegateInfo;
@end
