//
//  SWMapKitAnnotation.m
//  Sickweather
//
//  Created by John Erck on 5/1/17.
//  Copyright (c) 2017 Sickweather, LLC. All rights reserved.
//

#import "SWPrivateReportMapKitAnnotation.h"

@implementation SWPrivateReportMapKitAnnotation

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    _coordinate = coordinate;
}

- (void)setIdentifier:(NSString *)identifier
{
    _identifier = identifier;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    _subtitle = subtitle;
}

- (void)setTypeId:(NSString *)typeId
{
    _typeId = typeId;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate identifier:(NSString *)identifier title:(NSString *)title subtitle:(NSString *)subtitle typeId:(NSString *)typeId
{
    self = [super init];
    if (self) {
        self.coordinate = coordinate;
        self.identifier = identifier;
        self.title = title;
        self.subtitle = subtitle;
        self.typeId = typeId;
    }
    return self;
}

@end
