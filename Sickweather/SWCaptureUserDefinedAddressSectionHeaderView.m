//
//  SWCaptureUserDefinedAddressSectionHeaderView.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWCaptureUserDefinedAddressSectionHeaderView.h"

@interface SWCaptureUserDefinedAddressSectionHeaderView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UILabel *sectionLabel;
@end

@implementation SWCaptureUserDefinedAddressSectionHeaderView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWCaptureUserDefinedAddressSectionHeaderViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    self.sectionLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate captureUserDefinedAddressSectionHeaderViewTitleString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12]}];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.topBorderView = [[UIView alloc] init];
        self.sectionLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        [self.viewsDictionary setObject:self.sectionLabel forKey:@"sectionLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.sectionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.topBorderView];
        [self addSubview:self.sectionLabel];
        
        // LAYOUT
        
        // Layout TOP HEADER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topBorderView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout SECTION LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[sectionLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[sectionLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config HEADER
        self.backgroundColor = [SWColor colorSickweatherStyleGuideOffwhite246x246x246];
        
        // Config TOP BORDER ROW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config SECTION LABEL
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //        self.sectionLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
