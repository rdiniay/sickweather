//
//  SWSponsoredMarkerMapKitAnnotation.m
//  Sickweather
//
//  Created by John Erck on 3/31/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMarkerMapKitAnnotation.h"

@implementation SWSponsoredMarkerMapKitAnnotation

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    _coordinate = coordinate;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    _subtitle = subtitle;
}

- (void)setName:(NSString *)name
{
    _name = name;
}

- (void)setPhone:(NSString *)phone
{
    _phone = phone;
}

- (void)setIdentifier:(NSString *)identifier
{
    _identifier = identifier;
}

- (void)setDisplayText:(NSString *)displayText
{
    _displayText = displayText;
}

- (void)setMarkerImgURL:(NSString *)markerImgURL
{
    _markerImgURL = markerImgURL;
}

- (void)setPopupLogoURL:(NSString *)popupLogoURL
{
    _popupLogoURL = popupLogoURL;
}

- (void)setSponsoredMarkerTargetURL:(NSString *)sponsoredMarkerTargetURL
{
    _sponsoredMarkerTargetURL = sponsoredMarkerTargetURL;
}

- (void)setLat:(NSString *)lat
{
    _lat = lat;
}

- (void)setLon:(NSString *)lon
{
    _lon = lon;
}

- (void)setServerDict:(NSDictionary *)serverDict
{
    _serverDict = serverDict;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate name:(NSString *)name phone:(NSString *)phone title:(NSString *)title subtitle:(NSString *)subtitle identifier:(NSString *)identifier displayText:(NSString *)displayText markerImgURL:(NSString *)markerImgURL popupLogoURL:(NSString *)popupLogoURL sponsoredMarkerTargetURL:(NSString *)sponsoredMarkerTargetURL lat:(NSString *)lat lon:(NSString *)lon serverDict:(NSDictionary *)serverDict
{
    self = [super init];
    if (self) {
        self.coordinate = coordinate;
        self.title = title;
        self.subtitle = @"";//subtitle;
        self.name = name;
        self.phone = phone;
        self.identifier = identifier;
        self.displayText = displayText;
        self.markerImgURL = markerImgURL;
        self.popupLogoURL = popupLogoURL;
        self.sponsoredMarkerTargetURL = sponsoredMarkerTargetURL;
        self.lat = lat;
        self.lon = lon;
        self.serverDict = serverDict;
    }
    return self;
}

@end
