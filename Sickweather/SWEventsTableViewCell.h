//
//  SWEventsTableViewCell.h
//  Sickweather
//
//  Created by Shan Shafiq on 12/6/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWEventModal.h"
@interface SWEventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textViewType;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCreatedAt;
@property (weak, nonatomic) IBOutlet UIImageView *typeImgView;
@property (weak, nonatomic) IBOutlet UIImageView *typeCircleImgView;
@property (weak, nonatomic) IBOutlet UIImageView *typeCircleSubImgView;
@property (weak, nonatomic) IBOutlet UIView *verticalLine;
-(void)populateCellWith:(SWEventModal*)event;
@end
