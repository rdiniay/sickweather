//
//  SWSickCloudDetailViewController.m
//  Sickweather
//
//  Created by John Erck on 5/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "Flurry.h"
#import "SWSickCloudDetailViewController.h"

@interface SWSickCloudDetailViewController () <MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *mapScreenshotImageView;
@property (weak, nonatomic) IBOutlet UILabel *shareThisReportLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareViaFacebookButton;
@property (weak, nonatomic) IBOutlet UIButton *shareViaTwitterButton;
@property (weak, nonatomic) IBOutlet UIButton *shareViaTextButton;
@property (weak, nonatomic) IBOutlet UIView *separatorLine;
@property (weak, nonatomic) IBOutlet UILabel *illnessTypeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *illnessTypeDescriptionLabel;
@end

@implementation SWSickCloudDetailViewController

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    if (result == MessageComposeResultCancelled)
    {
        //NSLog(@"MessageComposeResultCancelled");
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    else if (result == MessageComposeResultFailed)
    {
        //NSLog(@"MessageComposeResultFailed");
        [self dismissViewControllerAnimated:YES completion:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occurred. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }];
    }
    else if (result == MessageComposeResultSent)
    {
        //NSLog(@"MessageComposeResultSent");
        
        // Dismiss page...
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [Flurry logEvent:@"Marker Description Shared" withParameters:@{@"Network": @"Text"}];
        });
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

#pragma mark - Helpers

- (NSString *)getShareText
{
    NSString *name = [self.customMapKitAnnotation.title substringToIndex:([self.customMapKitAnnotation.title length] - [@" reported" length])];
    return [NSString stringWithFormat:@"%@ is going around. Get the app: http://sick.io/app", name];
}

#pragma mark - Target Action

- (void)shareViaFacebookImageViewTapped:(UITapGestureRecognizer *)tap
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [vc setInitialText:[self getShareText]];
        [vc addImage:self.mapScreenshotImage];
        vc.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result)
            {
                case SLComposeViewControllerResultCancelled:
                    //  This means the user cancelled without sending the Tweet
                    break;
                case SLComposeViewControllerResultDone:
                    //  This means the user hit 'Send'
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Flurry logEvent:@"Marker Description Shared" withParameters:@{@"Network": @"Facebook"}];
                    });
                    break;
            }
        };
        [self presentViewController:vc animated:YES completion:^{}];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Share via Facebook is not setup. Log into Facebook via the Settings app and then try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)shareViaTwitterImageViewTapped:(UITapGestureRecognizer *)tap
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *vc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [vc setInitialText:[self getShareText]];
        [vc addImage:self.mapScreenshotImage];
        vc.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result)
            {
                case SLComposeViewControllerResultCancelled:
                    //  This means the user cancelled without sending the Tweet
                    break;
                case SLComposeViewControllerResultDone:
                    //  This means the user hit 'Send'
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Flurry logEvent:@"Marker Description Shared" withParameters:@{@"Network": @"Twitter"}];
                    });
                    break;
            }
        };
        [self presentViewController:vc animated:YES completion:^{}];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Share via Twitter is not setup. Log into Twitter via the Settings app and then try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)shareViaTextImageViewTapped:(UITapGestureRecognizer *)tap
{
    if ([MFMessageComposeViewController canSendText] == YES)
    {
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        messageController.body = [self getShareText];
        if ([MFMessageComposeViewController canSendAttachments] == YES)
        {
            // We use jpg rather than png b/c is smaller file size
            //NSData *pngData = UIImagePNGRepresentation(self.phrameImage);
            NSData *jpgData = UIImageJPEGRepresentation(self.mapScreenshotImage, 1.0); // 1.0 is best quality
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsPath = [paths objectAtIndex:0]; // Get the docs directory
            //NSString *filename = @"image.png";
            NSString *filename = @"image.jpg";
            NSString *filePath = [documentsPath stringByAppendingPathComponent:filename]; // Add the file name
            //[pngData writeToFile:filePath atomically:YES]; // Write the file
            [jpgData writeToFile:filePath atomically:YES]; // Write the file
            //[messageController addAttachmentData:pngData typeIdentifier:@"public.data" filename:filename];
            [messageController addAttachmentData:jpgData typeIdentifier:@"public.data" filename:filename];
        }
        [self presentViewController:messageController animated:YES completion:^{}];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Service Unavailable" message:@"This device is unable to send text messages." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)upperRightBarButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Lifecycle Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [self updateLayoutForNewOrientation: [[UIApplication sharedApplication] statusBarOrientation]];
}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    [self updateLayoutForNewOrientation: interfaceOrientation];
}

/*
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
*/

- (void)updateLayoutForNewOrientation:(UIInterfaceOrientation)orientation
{
    /*
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        
    }
    else
    {
        
    }
    */
}

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // CONFIG
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(upperRightBarButtonTapped:)];
    
    // Config SCROLL VIEW
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config MAP SCREENSHOT IMAGE VIEW
    self.mapScreenshotImageView.image = self.mapScreenshotImage;
    
    // Config SHARE THIS REPORT LABEL
    self.shareThisReportLabel.textColor = [SWColor colorSickweatherBlue41x171x226];
    self.shareThisReportLabel.font = [SWFont fontStandardBoldFontOfSize:14];
    
    // Config SHARE VIA FACEBOOK BUTTON
    [self.shareViaFacebookButton addTarget:self action:@selector(shareViaFacebookImageViewTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config SHARE VIA TWITTER BUTTON
    [self.shareViaTwitterButton addTarget:self action:@selector(shareViaTwitterImageViewTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config SHARE VIA TEXT BUTTON
    [self.shareViaTextButton addTarget:self action:@selector(shareViaTextImageViewTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config SEPARATOR LINE
    self.separatorLine.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
    
    // Config ILLNESS TYPE NAME LABEL
    self.illnessTypeNameLabel.text = @"";
    self.illnessTypeNameLabel.textColor = [SWColor colorSickweatherGrayText135x135x135];
    self.illnessTypeNameLabel.font = [SWFont fontStandardFontOfSize:26];
    
    // Config ILLNESS TYPE DESCRIPTION LABEL
    self.illnessTypeDescriptionLabel.text = @"";
    self.illnessTypeDescriptionLabel.textColor = [SWColor colorSickweatherGrayText135x135x135];
    self.illnessTypeDescriptionLabel.font = [SWFont fontStandardFontOfSize:14];
    
    // COLOR VIEWS FOR DEBUG
    self.scrollView.backgroundColor = [UIColor purpleColor];
    self.contentView.backgroundColor = [UIColor greenColor];
    self.illnessTypeNameLabel.backgroundColor = [UIColor orangeColor];
    
    // COLOR VIEWS FOR PRODUCTION
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.illnessTypeNameLabel.backgroundColor = [UIColor whiteColor];
    
    // Load from network
    [[SWHelper helperAppBackend] fetchIllnessDefinitionForIllnessTypeId:self.customMapKitAnnotation.typeId usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSHTTPURLResponse *responseHeaders, NSError *error) {
        // {"id":"2","word_display":"bronchitis","descr":"Inflammation of the mucous membrane in the bronchial tubes. It typically causes bronchospasm and coughing."}
        if ([[jsonResponseBody objectForKey:@"word_display"] isKindOfClass:[NSString class]])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *illnessName = (NSString *)[jsonResponseBody objectForKey:@"word_display"];
                self.illnessTypeNameLabel.text = [illnessName capitalizedString];
                if ([self.illnessTypeNameLabel.text isEqualToString:@"Rsv"]) {
                    self.illnessTypeNameLabel.text = @"RSV";
                }
                [self.illnessTypeNameLabel sizeToFit];
                
                // Log to Flurry
                [Flurry logEvent:@"Marker Info Bubble Tapped" withParameters:@{@"Illness": self.illnessTypeNameLabel.text}];
            });
        }
        if ([[jsonResponseBody objectForKey:@"descr"] isKindOfClass:[NSString class]])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *illnessDescription = (NSString *)[jsonResponseBody objectForKey:@"descr"];
                self.illnessTypeDescriptionLabel.text = illnessDescription;
                [self.illnessTypeDescriptionLabel sizeToFit];
                [self updateLayoutForNewOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
            });
        }
    }];
}

@end
