//
//  SWReportPrivatelyView.m
//  Sickweather
//
//  Created by John Erck on 3/21/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWReportPrivatelyView.h"

@interface SWReportPrivatelyView ()
@property (strong, nonatomic) NSString *currentStateFlag;
@property (strong, nonatomic) UIColor *primaryTextColor;
@property (strong, nonatomic) NSMutableDictionary *myViewsDictionary;
@property (strong, nonatomic) UILabel *myTitleLabel;
@property (strong, nonatomic) UILabel *mySubtitleLabel;
@property (strong, nonatomic) UIImageView *icon;
@end

@implementation SWReportPrivatelyView

#pragma mark - Public Methods

- (void)reset
{
    self.backgroundColor = [UIColor whiteColor];
    self.isOn = NO;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [SWColor colorSickweatherStyleGuideGrayLight133x133x134].CGColor;
    self.myTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Report Privately" attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77]}];
    self.mySubtitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Illnesses reported privately are not added to the Sickweather maps and are only visible by you." attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:14],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
    self.icon.image = [UIImage imageNamed:@"radio-off"];
    self.icon.contentMode = UIViewContentModeCenter;
}

#pragma mark - Target/Action

- (void)tapped:(id)sender
{
    if (self.isOn)
    {
        [self reset];
    }
    else
    {
        self.isOn = YES;
        self.icon.image = [UIImage imageNamed:@"radio-on"];
        self.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
        self.layer.borderWidth = 2.0;
        self.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - Helpers

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT DEFAULTS
        self.layer.cornerRadius = 6;
        
        // INIT VIEWS
        self.myTitleLabel = [[UILabel alloc] init];
        self.mySubtitleLabel = [[UILabel alloc] init];
        self.icon = [[UIImageView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        NSMutableDictionary *myViewsDictionary = [NSMutableDictionary new];
        [myViewsDictionary setObject:self.myTitleLabel forKey:@"myTitleLabel"];
        [myViewsDictionary setObject:self.mySubtitleLabel forKey:@"mySubtitleLabel"];
        [myViewsDictionary setObject:self.icon forKey:@"icon"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.mySubtitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.icon.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
        [self addSubview:self.myTitleLabel];
        [self addSubview:self.mySubtitleLabel];
        [self addSubview:self.icon];
        
        // LAYOUT
        NSArray *myConstraints = @[];
        
        // myTitleLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(40)-[myTitleLabel]-(20)-|"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[myTitleLabel]"] options:0 metrics:0 views:myViewsDictionary]];
        
        // mySubtitleLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(40)-[mySubtitleLabel]-(20)-|"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[myTitleLabel]-(5)-[mySubtitleLabel]-(20)-|"] options:0 metrics:0 views:myViewsDictionary]];
        
        // icon
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[icon]"] options:0 metrics:0 views:myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[icon]"] options:0 metrics:0 views:myViewsDictionary]];
        
        // ADD CONSTRAINTS
        [self addConstraints:myConstraints];
        
        // CONFIG
        
        // self
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tap];
        
        // myTitleLabel
        self.myTitleLabel.textAlignment = NSTextAlignmentLeft;
        self.myTitleLabel.numberOfLines = 0;
        
        // mySubtitleLabel
        self.mySubtitleLabel.textAlignment = NSTextAlignmentLeft;
        self.mySubtitleLabel.numberOfLines = 0;
        
        // icon
        
        // Reset (to init state)
        [self reset];
        
    }
    return self;
}

@end
