//
//  SWAddNoteViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 1/31/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWAddNoteViewController.h"
#import "SWColor.h"
#import "SWEventsTableViewController.h"
@interface SWAddNoteViewController ()
@property (nonatomic , strong) NSString *placeHolderText;
@end

@implementation SWAddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = @"Notes";
	// Config Navigation Bar Items
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemTapped:)];
	//Text Field Placeholder
	self.placeHolderText = (self.familyMember) ? [NSString stringWithFormat:@"How is %@ feeling today...",self.familyMember.name_first] :  @"How are you feeling today...";
	self.descriptionTxtView.text = self.placeHolderText;
	self.descriptionTxtView.textColor = [UIColor lightGrayColor];
	self.descriptionTxtView.tintColor = [SWColor colorSickweatherBlue41x171x226];
	//Add Gesture
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
	[self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Target/Action

-(void)hideKeyboard{
	[self.view endEditing:YES];
}

- (void)leftBarButtonItemTapped:(id)sender
{
	[self hideKeyboard];
	[self.navigationController popViewControllerAnimated:true];
}

- (void)rightBarButtonItemTapped:(id)sender
{
	//Add Flurry Event
	[SWHelper logFlurryEventsWithTag:@"Family > Notes > Save Notes button tapped"];
	if ([self.descriptionTxtView.text isEqualToString:@""] || [self.descriptionTxtView.text isEqualToString:self.placeHolderText]) {
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Add notes to save" preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
		[alert addAction:ok];
		[self presentViewController:alert animated:YES completion:nil];
		return;
	}
	[self.view endEditing:true];
	[self addTrackerWith:@{@"type":[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeNotes], @"member_id":self.familyMember != nil ? self.familyMember.member_id : @"", @"value":self.descriptionTxtView.text}];
}

#pragma mark - User Navigation

-(void)popBackWith:(int)nb{
	NSArray *viewControllers = [self.navigationController viewControllers];
	if ([viewControllers count] > nb) {
		[self.navigationController popToViewController:[viewControllers objectAtIndex:[viewControllers count] - nb] animated:true];
	}
}


-(void)navigateToEventsWith:(NSString*)message{
	NSMutableArray *viewController = [NSMutableArray arrayWithArray: [self.navigationController viewControllers]];
	if (viewController.count >=1){
		[viewController removeObjectsInRange:NSMakeRange(1, viewController.count-1)];
		SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
		if (self.familyMember) {
			eventsTableViewController.title = self.familyMember.name_first;
		} else {
			eventsTableViewController.title = [SWUserCDM currentUser].firstName;
		}
		eventsTableViewController.showSuccessHeader = true;
		eventsTableViewController.successMessage = message;
		eventsTableViewController.isFamilyMember = self.familyMember ? YES : NO;
		eventsTableViewController.familyMember = self.familyMember;
		[viewController addObject:eventsTableViewController];
		[self.navigationController setViewControllers:viewController];
	}
	
}

#pragma mark - Add Tracker

-(void)addTrackerWith:(NSDictionary*)data{
	[self.activityIndicator startAnimating];
	[[SWHelper helperAppBackend]appBackendAddTrackerUsingArgs:data completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.activityIndicator stopAnimating];
			if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) {
				if (self.isFromFamilyScreen) {
					[self navigateToEventsWith:@"notes"];
				}else {
					[[NSNotificationCenter defaultCenter] postNotificationName:kSWRefreshEventsNotification
																		object:self
																	  userInfo:@{@"message":@"notes"}];
					[self popBackWith:3];
				}
			}else {
				UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"An Error Occurred" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
				UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
				[alert addAction:ok];
				[self presentViewController:alert animated:YES completion:nil];
			}
		});
	}];
}

#pragma UITextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
	if([text isEqualToString:@"\n"]){
		textView.text = [NSString stringWithFormat:@"%@%@",textView.text , @"\n"];
		return NO;
	}
	return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
	if ([textView.text isEqualToString:@""]) {
		textView.text = self.placeHolderText;
		textView.textColor = [UIColor lightGrayColor];
	}
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
	if (textView.textColor == [UIColor lightGrayColor]){
		textView.text = nil;
		textView.textColor = [UIColor blackColor];
	}
}

@end
