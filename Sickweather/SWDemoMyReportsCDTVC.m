//
//  SWDemoMyReportsCDTVC.m
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWDemoMyReportsCDTVC.h"
#import "SWReportCDM+SWAdditions.h"

@interface SWDemoMyReportsCDTVC ()

@end

@implementation SWDemoMyReportsCDTVC

- (IBAction)refresh
{
    [self.refreshControl beginRefreshing];
    dispatch_queue_t fetchQ = dispatch_queue_create("Fetch data queue...", NULL);
    dispatch_async(fetchQ, ^{
//        NSURL *url = [NSURL URLWithString:@"https://dl.dropbox.com/s/uqtf40y4vvs1b8h/checkLogin.php"];
//        NSData *data = [NSData dataWithContentsOfURL:url];
//         NSDictionary *checkLoginDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        [self.managedObjectContext performBlock:^{
            // Put the "report" into the database
            // Clearly a temp hack here for now...
            //NSDictionary *standardReportResponse = @{@"report_id": [checkLoginDict objectForKey:@"user_id"]};
            //[SWReportCDM reportWithSWAPIStandardReportResponseDict:standardReportResponse inManagedObjectContext:self.managedObjectContext];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
        });
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!self.managedObjectContext) [self useDemoDocument];
}

- (void)useDemoDocument
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"Demo Document"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]])
    {
        // Create it
        [document saveToURL:url forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
                [self refresh];
            }
        }];
    }
    else if (document.documentState == UIDocumentStateClosed)
    {
        // Open it
        [document openWithCompletionHandler:^(BOOL success) {
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
            }
        }];
    }
    else
    {
        // Try to use it
        self.managedObjectContext = document.managedObjectContext;
    }
}

@end
