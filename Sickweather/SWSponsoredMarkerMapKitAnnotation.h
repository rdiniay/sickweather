//
//  SWSponsoredMarkerMapKitAnnotation.h
//  Sickweather
//
//  Created by John Erck on 3/31/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SWSponsoredMarkerMapKitAnnotation : NSObject <MKAnnotation>

// MKAnnotation protocol open
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
// MKAnnotation protocol open

// Sickweather extensions open
@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, copy) NSString *phone;
@property (nonatomic, readonly, copy) NSString *identifier;
@property (nonatomic, readonly, copy) NSString *displayText;
@property (nonatomic, readonly, copy) NSString *markerImgURL;
@property (nonatomic, readonly, copy) NSString *popupLogoURL;
@property (nonatomic, readonly, copy) NSString *sponsoredMarkerTargetURL;
@property (nonatomic, readonly, copy) NSString *lat;
@property (nonatomic, readonly, copy) NSString *lon;
@property (nonatomic, readonly, copy) NSDictionary *serverDict;
// Sickweather extensions close

/*
 "display_text" = "The Shops at Arundel Preserve\n
 \n7698 Dorchester Blvd\n
 \nHanover, MD 21076\n
 \n1-888-808-6483";
 id = 22;
 lat = "39.15470886230";
 lon = "-76.74201965332";
 "marker_img" = "http://www.sickweather.com/images/markers/sponsor-marker_righttime@2x.png";
 "popup_logo" = "http://www.sickweather.com/images/markers/sponsor-annotation-logo_righttime@2x.png";
 title = "Righttime Medical Care";
 url = "http://www.myrighttime.com
 \n";
 "url_link" = "myrighttime.com
 */
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
                    name:(NSString *)name
                   phone:(NSString *)phone
                   title:(NSString *)title
                subtitle:(NSString *)subtitle
              identifier:(NSString *)identifier
             displayText:(NSString *)displayText
            markerImgURL:(NSString *)markerImgURL
            popupLogoURL:(NSString *)popupLogoURL
sponsoredMarkerTargetURL:(NSString *)sponsoredMarkerTargetURL
                     lat:(NSString *)lat
                     lon:(NSString *)lon
                     serverDict:(NSDictionary *)serverDict;

@end
