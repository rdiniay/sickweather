//
//  SWGroupProfileCollectionViewCellForEmptyMessageArea.m
//  Sickweather
//
//  Created by John Erck on 1/23/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForEmptyMessageArea.h"

@interface SWGroupProfileCollectionViewCellForEmptyMessageArea ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSArray *adjustableViewConstraints;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIButton *inviteFriendsButton;
@end

@implementation SWGroupProfileCollectionViewCellForEmptyMessageArea

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForEmptyMessageAreaUpdateDefaultInsetsLeft:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
{
    if (self.adjustableViewConstraints)
    {
        [self.contentView removeConstraints:self.adjustableViewConstraints];
    }
    self.adjustableViewConstraints = @[];
    self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[containerView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary]];
    self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[containerView]-(bottom)-|" options:0 metrics:@{@"top":top,@"bottom":bottom} views:self.viewsDictionary]];
    [self.contentView addConstraints:self.adjustableViewConstraints];
}

#pragma mark - Target/Action

- (void)shareThisGroupButtonTapped:(id)sender
{
    [self.delegate groupProfileCollectionViewCellForEmptyMessageAreaShareGroupButtonTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.adjustableViewConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.containerView = [[UIView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.inviteFriendsButton = [[UIButton alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.containerView forKey:@"containerView"];
        [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.viewsDictionary setObject:self.inviteFriendsButton forKey:@"inviteFriendsButton"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.inviteFriendsButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.containerView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.inviteFriendsButton];
        
        // LAYOUT
        
        // Layout containerView
        self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[containerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        self.adjustableViewConstraints = [self.adjustableViewConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[containerView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:self.adjustableViewConstraints];
        
        // Layout titleLabel
        //[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        //[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[titleLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(20)-[titleLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout inviteFriendsButton
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[inviteFriendsButton]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel]-(20)-[inviteFriendsButton(40)]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config containerView
        //self.containerView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config titleLabel
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Tell your friends about this group to get the conversation started!" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        
        // Config inviteFriendsButton
        [self.inviteFriendsButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"SHARE THIS GROUP" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:14],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGreen68x157x68]}] forState:UIControlStateNormal];
        self.inviteFriendsButton.backgroundColor = [UIColor whiteColor];
        self.inviteFriendsButton.layer.cornerRadius = 6;
        self.inviteFriendsButton.layer.borderColor = [SWColor colorSickweatherStyleGuideGreen68x157x68].CGColor;
        self.inviteFriendsButton.layer.borderWidth = 1;
        [self.inviteFriendsButton addTarget:self action:@selector(shareThisGroupButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.containerView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end

