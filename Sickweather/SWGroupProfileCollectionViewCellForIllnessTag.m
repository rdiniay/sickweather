//
//  SWTagFlowCollectionViewCell.m
//  Sickweather
//
//  Created by John Erck on 1/20/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForIllnessTag.h"

@interface SWGroupProfileCollectionViewCellForIllnessTag ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UILabel *tagLabel;
@end

@implementation SWGroupProfileCollectionViewCellForIllnessTag

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForIllnessTagSetNameAttributedString:(NSAttributedString *)name
{
    self.tagLabel.attributedText = name;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.tagLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.tagLabel forKey:@"tagLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.tagLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.tagLabel];
        
        // LAYOUT
        
        // Layout TAG LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[tagLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[tagLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config TAG LABEL
        self.tagLabel.layer.cornerRadius = 10;
        self.tagLabel.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
        self.tagLabel.layer.borderWidth = 1.0;
        self.tagLabel.textAlignment = NSTextAlignmentCenter;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.tagLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
