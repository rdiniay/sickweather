//
//  SWLabelLikeTextView.m
//  Sickweather
//
//  Created by John Erck on 4/23/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWLabelLikeTextView.h"

@implementation SWLabelLikeTextView

- (BOOL)canBecomeFirstResponder
{
    return NO;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.editable = NO;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
