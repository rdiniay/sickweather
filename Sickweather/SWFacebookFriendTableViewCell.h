//
//  SWFacebookFriendTableViewCell.h
//  Sickweather
//
//  Created by John Erck on 1/28/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWFacebookFriendTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicImageView;
@property (weak, nonatomic) IBOutlet UIView *selectedCircleView;
@property (weak, nonatomic) IBOutlet UIView *noImagePlaceholderView;
@end
