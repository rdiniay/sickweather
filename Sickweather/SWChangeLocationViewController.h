//
//  SWChangeLocationViewController.h
//  Sickweather
//
//  Created by John Erck on 5/7/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWChangeLocationViewControllerDelegate <NSObject>
- (void)didSuccessfullyCompleteSearchWithResultPlacemark:(CLPlacemark *)placemark;
@end

@interface SWChangeLocationViewController : UIViewController
@property (nonatomic, assign) id <SWChangeLocationViewControllerDelegate> delegate;
@end
