//
//  SWRadarMapOverlayRenderer.h
//  Sickweather
//
//  Created by John Erck on 5/1/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface SWRadarMapOverlayRenderer : MKOverlayRenderer

-(id)initWithOverlay:(id<MKOverlay>)overlay usingImage:(UIImage *)overlayImage;

@end
