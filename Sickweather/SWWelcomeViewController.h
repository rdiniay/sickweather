//
//  SWWelcomeViewController.h
//  Sickweather
//
//  Created by John Erck on 1/9/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWWelcomeViewController;

@protocol SWWelcomeViewControllerDelegate <NSObject>
- (void)userWantsToDismiss:(SWWelcomeViewController *)vc;
@end

@interface SWWelcomeViewController : UIViewController
@property (nonatomic, assign) id <SWWelcomeViewControllerDelegate> delegate;
- (void)loadWithLaunchScreenViewCoverUsingView:(UIView *)view;
- (void)loadWithoutLaunchScreenViewCover;
- (void)loadWithFadeOutLaunchScreenViewCoverUsingView:(UIView *)view;
@end
