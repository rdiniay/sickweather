//
//  SWFacebookFriendTableViewCell.m
//  Sickweather
//
//  Created by John Erck on 1/28/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWFacebookFriendTableViewCell.h"

@implementation SWFacebookFriendTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
