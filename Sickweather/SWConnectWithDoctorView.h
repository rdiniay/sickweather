//
//  SWConnectWithDoctorView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 19/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWConnectWithDoctorViewDelegate
-(void)signUp;
-(void)launchApp;
@end

@interface SWConnectWithDoctorView : UIView
@property (nonatomic, weak) id<SWConnectWithDoctorViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
-(void)initialize;
@end
