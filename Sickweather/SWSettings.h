//
//  SWSettings.h
//  Sickweather
//
//  Created by John Erck on 10/1/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SWSettings : NSObject

+ (SWSettings *)sharedInstance;
- (NSNumber *)sicknessReportGeofenceRadiusInMeters;
- (NSTimeInterval)minimumBackgroundFetchInterval;
- (CLLocationDegrees)defaultZoomLevelLatLonDelta;
- (CLLocationDegrees)sponsoredMarkerUpperLimitZoomLevelLatLonDelta;
- (CLLocationDegrees)radarLowerLimitZoomLevelLatLonDelta;
- (CLLocationDegrees)animatedRadarDefaultZoomLevelLatLonDelta;
- (NSUInteger)cacheMemoryCapacity;
- (NSUInteger)cacheDiskCapacity;
- (NSString *)cacheDiskPath;
- (double)requestTimeoutIntervalInSeconds;

@end
