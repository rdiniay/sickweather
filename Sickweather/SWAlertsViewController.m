//
//  SWAlertsViewController.m
//  Sickweather
//
//  Created by John Erck on 8/23/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>
#import "SWAlertsViewController.h"
#import "SWAlertsNewsItemTableViewCell.h"
#import "SWAlertsSickZoneItemTableViewCell.h"
#import "SWAlertsGroupItemTableViewCell.h"
#import "SWAlertsSympathyItemTableViewCell.h"
#import "SWAlertsFamilyItemTableViewCell.h"
#import "SWWebViewViewController.h"
#import "SWNewMapViewController.h"
#import "SWGroupProfileViewController.h"
#import "SWAlertSettingsViewController.h"
#import "SWBlockTabWithLoginView.h"

@interface SWAlertsViewController () <UITableViewDelegate, UITableViewDataSource, SWAlertsNewsItemTableViewCellDelegate, SWNewMapViewControllerDelegate, SWGroupProfileViewControllerDelegate, SWBlockTabWithLoginViewDelegate>

// NETWORK DATA
@property (strong, nonatomic) NSDictionary *latestAlertsResponse;

// UI VIEWS
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *notificationsAreOffContainer;
@property (strong, nonatomic) UILabel *notificationsAreOffLabel;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SWBlockTabWithLoginView *noLocationContainerView;
@property (strong, nonatomic) UILabel *noAlertsFullScreenLabel;
@property (nonatomic) BOOL isLoaded;

// UI CONSTRAINTS
@property (strong, nonatomic) NSLayoutConstraint *notificationsAreOffContainerHeight;

@end

@implementation SWAlertsViewController

#pragma mark - SWBlockTabWithLoginViewDelegate

- (void)primaryButtonTapped:(id)sender
{
    /*
     AS OF IOS 11 AND FOR THE FORESEEABLE FUTURE, THE WAY TO ASK FOR LOCATION
     IS JUST LIKE YOU WOULD EXPECT, A LITTLE BIT AT A TIME, AND ASK FOR THE LEAST
     THING FIRST IN ORDER TO STEP OUR WAY INTO ALWAYS ACCESS. THIS IS WHAT PEOPLE
     ARE SAYING IS THE BEST, AND HAS THE ADDED BENEFIT OF NOT ALLOWING USERS TO DENY
     WHEN THEY ARE PRESENTED WITH THE ALWAYS REQUEST (OUR 2ND LOCATION PROMPT).
     */
    
    // STEP 1: SEE IF WE HAVE EVER ASKED
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        // FIRST TIME USER SCENARIO, START BY ASKING JUST FOR WHEN IN USE
        [[[SWHelper helperAppDelegate] locationManager] requestWhenInUseAuthorization];
    }
    // STEP 2: SEE IF WE'VE BEEN FULLY DENIED
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        // YOU SIMPLY CANNOT ASK AGAIN ONCE YOU HAVE ASKED THE ONE TIME. YOU ARE DONE.
        //[[[SWHelper helperAppDelegate] locationManager] requestWhenInUseAuthorization]; <-- TE
        
        // FOR SOME REASON, THE USER HAS DECIDED TO REJECT US ACCESS TO THEIR LOCATION
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Turn Location On" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:actionOK];
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:^{}];
    }
    // STEP 3: SEE IF WE ONLY HAVE WHEN IN USE
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        // THIS IS IT, THIS IS OUR CHANCE TO NOW GO FOR ALWAYS ACCESS
        [[[SWHelper helperAppDelegate] locationManager] requestAlwaysAuthorization];
    }
    // STEP 4: CHECK AND SEE IF WE ARE ALREADY RUNNING ON ALWAYS
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // BOOM SUCKA! IT'S ALREADY ONLINE!
    }
}

#pragma mark - SWGroupProfileViewControllerDelegate

- (void)groupProfileViewControllerWantsToDismiss
{
    
}

#pragma mark - SWAlertsNewsItemTableViewCellDelegate

-(void)shareButtonTapped:(UIButton *)sender{
		// create new dispatch queue in background
	CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath *hitIndex = [self.tableView indexPathForRowAtPoint:hitPoint];
	dispatch_queue_t queue = dispatch_queue_create("openActivityIndicatorQueue", NULL);
	 dispatch_async(queue, ^{
		 if (hitIndex){
			 NSDictionary *item = [[self.latestAlertsResponse objectForKey:@"alerts"] objectAtIndex:hitIndex.row];
			 NSString *stringURL = @"https://sickweather.com";
			 if ([[item objectForKey:@"web_url"] isKindOfClass:[NSString class]]){
				 stringURL = [item objectForKey:@"web_url"];
			 }
             NSString *textToShare = @"News";
             NSURL *myWebsite = [NSURL URLWithString:stringURL];
             NSArray *objectsToShare = @[textToShare, myWebsite];
             UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
             NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                            UIActivityTypePrint,
                                            UIActivityTypeAssignToContact,
                                            UIActivityTypeSaveToCameraRoll,
                                            UIActivityTypeAddToReadingList,
                                            UIActivityTypePostToFlickr,
											UIActivityTypePostToVimeo, UIActivityTypeOpenInIBooks, UIActivityTypeAddToReadingList, UIActivityTypeAddToReadingList];
             activityVC.excludedActivityTypes = excludeActivities;
			 
			 dispatch_async(dispatch_get_main_queue(), ^{
				 if ([SWHelper isIpad]){
					 activityVC.modalPresentationStyle = UIModalPresentationPopover;
					 activityVC.popoverPresentationController.sourceView = self.view;
					 [self presentViewController:activityVC animated:YES completion:nil];
				 }else{
					 [self presentViewController:activityVC animated:YES completion:nil];
				 }
			 });
		 }
	 });
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    NSDictionary *item = [[self.latestAlertsResponse objectForKey:@"alerts"] objectAtIndex:indexPath.row];
    if ([cell isKindOfClass:[SWAlertsGroupItemTableViewCell class]])
    {
        // PUSH GROUP PROFILE
        [SWHelper logFlurryEventsWithTag:@"Groups Alert tapped"];
        NSString *swFoursquareGroupId = @"sw_76a49b41a80c131008d83e38b80cad111462807149";
        if ([[item objectForKey:@"foursquare_id"] isKindOfClass:[NSString class]])
        {
            swFoursquareGroupId = [item objectForKey:@"foursquare_id"];
        }
        SWGroupProfileViewController *groupProfile = [[SWGroupProfileViewController alloc] init];
        groupProfile.delegate = self;
        [groupProfile loadForFoursquareId:swFoursquareGroupId];
        [self.navigationController pushViewController:groupProfile animated:YES];
    }
    else if ([cell isKindOfClass:[SWAlertsSickZoneItemTableViewCell class]])
    {
        // PUSH ILLNESS MAP
        [SWHelper logFlurryEventsWithTag:@"Location Alert tapped"];
        NSString *illnessID = @"1";
        if ([[item objectForKey:@"illness_id"] isKindOfClass:[NSString class]])
        {
            illnessID = [item objectForKey:@"illness_id"];
        }
        SWNewMapViewController *newMapVC = [[SWNewMapViewController alloc] init];
        newMapVC.delegate = self;
        [newMapVC updateForIllnessIDs:[illnessID componentsSeparatedByString:@","] andLocation:[SWHelper helperLocationManager].location];
        [self.navigationController pushViewController:newMapVC animated:YES];
    }
    else if ([cell isKindOfClass:[SWAlertsSympathyItemTableViewCell class]])
    {
        // PUSH GROUP PROFILE
        [SWHelper logFlurryEventsWithTag:@"Sympthy Alert tapped"];
        NSString *swFoursquareGroupId = @"sw_76a49b41a80c131008d83e38b80cad111462807149";
        if ([[item objectForKey:@"foursquare_id"] isKindOfClass:[NSString class]])
        {
            swFoursquareGroupId = [item objectForKey:@"foursquare_id"];
        }
        SWGroupProfileViewController *groupProfile = [[SWGroupProfileViewController alloc] init];
        groupProfile.delegate = self;
        [groupProfile loadForFoursquareId:swFoursquareGroupId];
        [self.navigationController pushViewController:groupProfile animated:YES];
    }
    else if ([cell isKindOfClass:[SWAlertsNewsItemTableViewCell class]])
    {
        // OPEN NEWS LINK
        [SWHelper logFlurryEventsWithTag:@"Breaking News Alert tapped"];
        NSString *stringURL = @"https://sickweather.com";
        if ([[item objectForKey:@"web_url"] isKindOfClass:[NSString class]])
        {
            stringURL = [item objectForKey:@"web_url"];
        }
        SWWebViewViewController *webViewVC = [[SWWebViewViewController alloc] init];
        webViewVC.title = @"Sickweather";
        [webViewVC webViewViewControllerLoadURL:[NSURL URLWithString:stringURL]];
        [self.navigationController pushViewController:webViewVC animated:YES];
    }
    else if ([cell isKindOfClass:[SWAlertsFamilyItemTableViewCell class]])
    {
        // MAP TAP TO FAMILY TAB
        [SWHelper logFlurryEventsWithTag:@"Family Alert tapped"];
        [self.tabBarController setSelectedIndex:1];
    }
    self.isLoaded = true;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightValue = 0;
    NSDictionary *dict = [[self.latestAlertsResponse objectForKey:@"alerts"] objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"description"] isKindOfClass:[NSString class]])
    {
        // GET CELL TITLE TEXT
        NSString *description = [dict objectForKey:@"description"];
        
        // MUST MATCH CELL CODE OF COURSE...
        NSAttributedString *titleLabelAttributedString = [[NSAttributedString alloc] initWithString:description attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}];
        
        // SET WIDTH BASED ON SCREEN
        CGFloat width = self.view.bounds.size.width - 40;
        /*
        if (@available(iOS 11.0, *))
        {
            NSLog(@"width = %@", @(width));
            NSLog(@"self.view.bounds.size.width = %@", @(self.view.bounds.size.width));
            NSLog(@"self.view.safeAreaLayoutGuide.layoutFrame.size.width = %@", @(self.view.safeAreaLayoutGuide.layoutFrame.size.width));
            NSLog(@"self.view.safeAreaLayoutGuide.layoutFrame.size.height = %@", @(self.view.safeAreaLayoutGuide.layoutFrame.size.height));
            width = self.view.bounds.size.width - 40 - (self.view.bounds.size.width - self.view.safeAreaLayoutGuide.layoutFrame.size.width);
            NSLog(@"width = %@", @(width));
        }
        */
        
        // GET SIZE
        CGRect rect = [titleLabelAttributedString boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        // SET RETURN VALUE ACCORDINGLY
        heightValue = rect.size.height;
        
        //NSLog(@"THIS STRING %@ HAS HEIGHT %@ BASED ON CONSTRAINING WIDTH OF %@", description, @(heightValue), @(width));
    }
    return heightValue + 100;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.latestAlertsResponse)
    {
        return [[self.latestAlertsResponse objectForKey:@"alerts"] count];
    }
    return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellToReturn = nil;
    if ([[self.latestAlertsResponse objectForKey:@"alerts"] isKindOfClass:[NSArray class]])
    {
        NSDictionary *dict = [[self.latestAlertsResponse objectForKey:@"alerts"] objectAtIndex:indexPath.row];
        if ([[dict objectForKey:@"type"] isEqualToString:@"sickzone"])
        {
            // USE THE SICKZONE CELL TYPE
            
            SWAlertsSickZoneItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWAlertsSickZoneItemTableViewCell"];
            if (cell == nil)
            {
                cell = [[SWAlertsSickZoneItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SWAlertsSickZoneItemTableViewCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [cell updateForDict:dict];
            cellToReturn = cell;
        }
        else if ([[dict objectForKey:@"type"] isEqualToString:@"news"])
        {
            // USE THE NEWS CELL TYPE
            
            SWAlertsNewsItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWAlertsNewsItemTableViewCell"];
            if (cell == nil)
            {
                cell = [[SWAlertsNewsItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SWAlertsNewsItemTableViewCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
			cell.delegate = self;
            [cell updateForDict:dict];
            cellToReturn = cell;
        }
        else if ([[dict objectForKey:@"type"] isEqualToString:@"group"])
        {
            // USE THE GROUP CELL TYPE
            
            SWAlertsGroupItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWAlertsGroupItemTableViewCell"];
            if (cell == nil)
            {
                cell = [[SWAlertsGroupItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SWAlertsGroupItemTableViewCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [cell updateForDict:dict];
            cellToReturn = cell;
        }
        else if ([[dict objectForKey:@"type"] isEqualToString:@"sympathy"])
        {
            // USE THE SYMPATHY CELL TYPE
            
            SWAlertsSympathyItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWAlertsSympathyItemTableViewCell"];
            if (cell == nil)
            {
                cell = [[SWAlertsSympathyItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SWAlertsSympathyItemTableViewCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [cell updateForDict:dict];
            cellToReturn = cell;
        }
        else if ([[dict objectForKey:@"type"] isEqualToString:@"family"])
        {
            // USE THE FAMILY CELL TYPE
            
            SWAlertsFamilyItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWAlertsFamilyItemTableViewCell"];
            if (cell == nil)
            {
                cell = [[SWAlertsFamilyItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SWAlertsFamilyItemTableViewCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [cell updateForDict:dict];
            cellToReturn = cell;
        }
        else
        {
            //NSLog(@"dict = %@", dict);
        }
    }
    cellToReturn.selectionStyle = UITableViewCellSelectionStyleNone;
    return cellToReturn;
}

#pragma mark - Target/Action

- (void)settingsButtonTapped:(id)sender
{
//    if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications])
//    {
//        // LINK THEM TO SETTINGS
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notifications Are Off" message:@"Tap \"Go to Settings\" below to turn notifications on for Sickweather." preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
//        [alert addAction:cancel];
//        UIAlertAction *goToSettings = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//        }];
//        [alert addAction:goToSettings];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
//    else
//    {
    [SWHelper logFlurryEventsWithTag:@"Alerts > Settings button tapped"];
    SWAlertSettingsViewController *alertSettingsVC = [[SWAlertSettingsViewController alloc] init];
    [self.navigationController pushViewController:alertSettingsVC animated:YES];
//    }
}

- (void)appDidBecomeActiveAgain:(id)sender
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {
        // WE DO HAVE ACCESS TO THEIR LOCATION
        [self hideNoLocationUI];
        [self getAlerts];
    }
    else
    {
        // WE NEED ACCESS TO THEIR LOCATION BEFORE WE CAN RENDER ANY FURTHER
        [self showNoLocationUI];
    }
}

#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

#pragma mark - Helpers

- (void)getAlerts
{
    if ([SWHelper helperLocationManager].location)
    {
        // PICK DATA ATTRIBUTES TO PASS UP TO SERVER
        NSMutableDictionary *dataForServer = [NSMutableDictionary new];
        
        // Location Coordinates
        NSString *lat = [@([SWHelper helperLocationManager].location.coordinate.latitude) stringValue];
        NSString *lon = [@([SWHelper helperLocationManager].location.coordinate.longitude) stringValue];
        
        // Setting location coordinates to send to server
        [dataForServer setObject:lat forKey:@"lat"];
        [dataForServer setObject:lon forKey:@"lon"];
        
        // ADD USER ILLNESS TYPE PREFS
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *networkIllnesses = [SWHelper helperLoadJSONObjectFromDocsUsingName:@"my-network-illnesses"];
        NSArray *individualIllnesses = @[];
        //    NSLog(@"myNetworkIllnesses = %@", networkIllnesses);
        for (NSDictionary *illness in networkIllnesses)
        {
            if (![[illness objectForKey:@"id"] containsString:@","])
            {
                // IS NOT GROUP, IS INDIVIDUAL, ADD
                individualIllnesses = [individualIllnesses arrayByAddingObject:[illness objectForKey:@"id"]];
            }
        }
        //NSLog(@"individualIllnesses = %@", individualIllnesses);
        NSArray *illnessIdsOn = @[];
        for (NSString *individualIllnessId in individualIllnesses)
        {
            //NSLog(@"individualIllnessId = %@", individualIllnessId);
            NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_for_id_%@_yes_no", individualIllnessId];
            if ([[defaults stringForKey:defaultsKey] isEqualToString:@"yes"])
            {
                illnessIdsOn = [illnessIdsOn arrayByAddingObject:individualIllnessId];
            }
        }
        //NSLog(@"illnessIdsOn = %@", illnessIdsOn);
        [dataForServer setObject:[illnessIdsOn componentsJoinedByString:@","] forKey:@"alert_when_near"];
        
        [[SWHelper helperAppBackend] appBackendGetAlertsUsingArgs:dataForServer usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"fail"])
                {
                    // FAIL
                    self.noAlertsFullScreenLabel.hidden = NO;
                    self.noAlertsFullScreenLabel.attributedText = [[NSAttributedString alloc] initWithString: @"No new alerts around you. We’ll notify you, or check back later" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
                    // old message: [jsonResponseBody objectForKey:@"message"]
                    /*
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No New Alerts" message:[jsonResponseBody objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
                    [alert addAction:cancel];
                    [self presentViewController:alert animated:YES completion:nil];
                    */
                }
                else
                {
                    self.noAlertsFullScreenLabel.hidden = YES;
                    self.latestAlertsResponse = jsonResponseBody;
                    [self.tableView reloadData];
                }
                if([self.refreshControl  isRefreshing]){
                    [self.refreshControl endRefreshing];
                }
            });
        }];
    }
}

- (void)showNoLocationUI
{
    self.noLocationContainerView.hidden = NO;
    self.noAlertsFullScreenLabel.hidden = YES;
}

- (void)hideNoLocationUI
{
    self.noLocationContainerView.hidden = YES;
}

#pragma mark - NSNotificationCenter Callback Methods

- (void)didUpdateCurrentLocationPlacemarkNotification:(NSNotification *)notification
{
    NSString *error = [notification.userInfo objectForKey:@"error"];
    CLPlacemark *placemark = [notification.userInfo objectForKey:@"placemark"];
    if (error)
    {
        //NSLog(@"error = %@", error);
    }
    else if (placemark)
    {
        NSLog(@"CURRENT LOCATION placemark = %@", placemark);
        if ([placemark isKindOfClass:[CLPlacemark class]])
        {
            //NSLog(@"isKindOfClass:[CLPlacemark class]");
            self.noLocationContainerView.hidden = YES;
            [self getAlerts];
        }
        if ([placemark isKindOfClass:[NSDictionary class]])
        {
            //NSLog(@"isKindOfClass:[NSDictionary class]");
        }
    }
}

#pragma mark - Life Cycle Methods

- (void)dealloc
{
    // REMOVE OBSERVERS
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // HACK: Setting this here again, because otherwise was faded out
    // when the user pops the Alert Settings screen off the stack back
    // to this screen.
    
    // rightBarButtonItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonTapped:)];
    [self updateAlertsAndUI];
}

-(void)updateAlertsAndUI{
        // DETERMINE LOCATION ACCESS STATUS
    
        // HIT NETWORK GET ALERTS
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
        {
            // WE DO HAVE ACCESS TO THEIR LOCATION
        [self hideNoLocationUI];
        if (!self.isLoaded) {
            [self getAlerts];
        }
        self.isLoaded = false;
		}
    else {
            // WE NEED ACCESS TO THEIR LOCATION BEFORE WE CAN RENDER ANY FURTHER
        [self showNoLocationUI];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)refreshAlerts:(UIRefreshControl *)refreshControl
{
    self.isLoaded = false;
    [self updateAlertsAndUI];
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // TELL US WHEN THE USER'S CURRENT LOCATION HAS BEEN TRANSLATED TO A PLACEMARK OBJECT
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUpdateCurrentLocationPlacemarkNotification:)
                                                 name:kSWDidUpdateCurrentLocationPlacemarkNotification
                                               object:nil];
    
    // MAKE SURE WE ARE TOLD ABOUT WHEN WE COME BACK ON SCREEN (USER PLAYING BACK AND FORTH WITH SETTINGS APP)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActiveAgain:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.notificationsAreOffContainer = [[UIView alloc] init];
    self.notificationsAreOffLabel = [[UILabel alloc] init];
    self.tableView = [[UITableView alloc] init];
    self.noLocationContainerView = [[SWBlockTabWithLoginView alloc] init];
    self.noAlertsFullScreenLabel = [[UILabel alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.notificationsAreOffContainer forKey:@"notificationsAreOffContainer"];
    [self.viewsDictionary setObject:self.notificationsAreOffLabel forKey:@"notificationsAreOffLabel"];
    [self.viewsDictionary setObject:self.tableView forKey:@"tableView"];
    [self.viewsDictionary setObject:self.noLocationContainerView forKey:@"noLocationContainerView"];
    [self.viewsDictionary setObject:self.noAlertsFullScreenLabel forKey:@"noAlertsFullScreenLabel"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.notificationsAreOffContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.notificationsAreOffLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.noLocationContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.noAlertsFullScreenLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.notificationsAreOffContainer];
    [self.notificationsAreOffContainer addSubview:self.notificationsAreOffLabel];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.noLocationContainerView];
    [self.view addSubview:self.noAlertsFullScreenLabel];
    
    // LAYOUT
    
    // notificationsAreOffContainer
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[notificationsAreOffContainer]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[notificationsAreOffContainer]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.notificationsAreOffContainerHeight = [NSLayoutConstraint constraintWithItem:self.notificationsAreOffContainer attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0];
    [self.view addConstraint:self.notificationsAreOffContainerHeight];
    
    // notificationsAreOffLabel
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[notificationsAreOffLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[notificationsAreOffLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // tableView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[tableView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[notificationsAreOffContainer][tableView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // noLocationContainerView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[noLocationContainerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    if (@available(iOS 11.0, *)) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationContainerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationContainerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    } else {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationContainerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationContainerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    }
    
    // noAlertsFullScreenLabel
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[noAlertsFullScreenLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[noAlertsFullScreenLabel]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // navBar
    self.navigationController.navigationBar.translucent = NO;
    
    // rightBarButtonItem
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonTapped:)];
    
    // self
    self.title = @"Alerts";
    
    // view
    self.view.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    
    // notificationsAreOffContainer
    
    // notificationsAreOffLabel
     
    // tableView
    [self.tableView registerClass:[SWAlertsNewsItemTableViewCell class] forCellReuseIdentifier:@"SWAlertsNewsItemTableViewCell"];
    [self.tableView registerClass:[SWAlertsSickZoneItemTableViewCell class] forCellReuseIdentifier:@"SWAlertsSickZoneItemTableViewCell"];
    [self.tableView registerClass:[SWAlertsGroupItemTableViewCell class] forCellReuseIdentifier:@"SWAlertsGroupItemTableViewCell"];
    [self.tableView registerClass:[SWAlertsSympathyItemTableViewCell class] forCellReuseIdentifier:@"SWAlertsSympathyItemTableViewCell"];
    [self.tableView registerClass:[SWAlertsFamilyItemTableViewCell class] forCellReuseIdentifier:@"SWAlertsFamilyItemTableViewCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // noLocationContainerView
    [self.noLocationContainerView updateBackgroundColor:[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0]];
    [self.noLocationContainerView updateImage:[UIImage imageNamed:@"onboarding-icon-sick-zone-alerts"]];
    [self.noLocationContainerView updateTitle:@"SickZone Alerts"];
    [self.noLocationContainerView updateDescription:@"Receive location-based notifications with SickZone Alerts, and important health news flashes in your community."];
    [self.noLocationContainerView updateButtonTitle:@"Enable Location Services"];
    self.noLocationContainerView.delegate = self;
    
    // noAlertsFullScreenLabel
    self.noAlertsFullScreenLabel.hidden = YES;
    self.noAlertsFullScreenLabel.textAlignment = NSTextAlignmentCenter;
    self.noAlertsFullScreenLabel.numberOfLines = 0;
    self.noAlertsFullScreenLabel.backgroundColor = [SWColor sickweatherSuperLightGray247x247x247];
    
		//Add Refresh Control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshAlerts:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

@end

