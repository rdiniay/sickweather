//
//  SWWevViewViewController.m
//  Sickweather
//
//  Created by John Erck on 4/18/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "SWWebViewViewController.h"

@interface SWWebViewViewController ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) NSURL *urlToLoad;
@end

@implementation SWWebViewViewController

#pragma mark - Public Methods

- (void)webViewViewControllerLoadURL:(NSURL *)url
{
    self.urlToLoad = url;
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.urlToLoad]];
}

#pragma mark - Target/Action

- (void)closeIconTapped:(id)sender
{
    [self.delegate webViewViewControllerScreenWantsToDismiss:self];
}

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // Invoke super
    [super viewDidLoad];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.webView = [[WKWebView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.webView forKey:@"webView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.webView];
    
    // LAYOUT
    
    // webView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[webView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[webView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // view
    self.view.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
    /*
    // navBar
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(closeIconTapped:)];
    self.navigationController.navigationBar.translucent = NO;
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [SWFont fontDuepuntozeroRegularFontOfSize:26];
    titleLabel.textColor = [SWColor colorSickweatherBlue41x171x226];
    titleLabel.text = @"sickweather";
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    */
    // Load URL if set
    if (self.urlToLoad)
    {
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.urlToLoad]];
    }
}

@end
