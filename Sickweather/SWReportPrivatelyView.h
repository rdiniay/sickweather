//
//  SWReportPrivatelyView.h
//  Sickweather
//
//  Created by John Erck on 3/21/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWReportPrivatelyView : UIView
@property (assign) BOOL isOn;
- (void)reset;
@end
