//
//  SWCaptureUserDefinedAddressRowView.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWCaptureUserDefinedAddressRowView.h"

@interface SWCaptureUserDefinedAddressRowView ()
@property (assign) BOOL shouldShowTopBorder;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *rowIconImageView;
@property (strong, nonatomic) UILabel *rowLabelView;
@property (strong, nonatomic) UITextField *rowTextField;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UIView *bottomBorderView;
@end

@implementation SWCaptureUserDefinedAddressRowView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWCaptureUserDefinedAddressRowViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    self.rowLabelView.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate captureUserDefinedAddressRowViewTitleString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}];
    
    // This is just a very weird autolayout bug fix, not sure why "City" is unique and is the only one that needs a width set for it...
    if ([self.rowLabelView.attributedText.string isEqualToString:@"City"])
    {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowLabelView(<=40)]"] options:0 metrics:0 views:self.viewsDictionary]];
    }
}

#pragma mark - Public Methods

- (void)showBottomBorder
{
    self.bottomBorderView.hidden = NO;
}

- (void)setUserText:(NSString *)userText
{
    self.rowTextField.text = userText;
}

- (NSString *)userText
{
    return self.rowTextField.text;
}

#pragma mark - Target Action

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.rowIconImageView = [[UIImageView alloc] init];
        self.rowLabelView = [[UILabel alloc] init];
        self.rowTextField = [[UITextField alloc] init];
        self.topBorderView = [[UIView alloc] init];
        self.bottomBorderView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.rowIconImageView forKey:@"rowIconImageView"];
        [self.viewsDictionary setObject:self.rowLabelView forKey:@"rowLabelView"];
        [self.viewsDictionary setObject:self.rowTextField forKey:@"rowTextField"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        [self.viewsDictionary setObject:self.bottomBorderView forKey:@"bottomBorderView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.rowIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowLabelView.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.rowIconImageView];
        [self addSubview:self.rowLabelView];
        [self addSubview:self.rowTextField];
        [self addSubview:self.topBorderView];
        [self addSubview:self.bottomBorderView];
        
        // LAYOUT
        
        // Layout ROW ICON IMAGE VIEW (MAKING IMAGE VIEW 0 LEFT MARGIN, 0 WIDTH TO HIDE BUT STILL KEEP IN HERE FOR LATER USE IF NEEDED)
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[rowIconImageView(0)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rowIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout ROW LABEL VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowIconImageView]-(15)-[rowLabelView]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[rowLabelView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout SECTION TEXT FIELD
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowLabelView]-(15)-[rowTextField]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[rowTextField]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TOP BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topBorderView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout BOTTOM BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[bottomBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[bottomBorderView(1)]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config ROW
        self.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        
        // Config ROW ICON IMAGE VIEW
        self.rowIconImageView.contentMode = UIViewContentModeCenter;
        
        // Config ROW LABEL VIEW
        //self.rowLabelView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
        
        // Config ROW TEXT FIELD
        self.rowTextField.userInteractionEnabled = YES;
        self.rowTextField.textAlignment = NSTextAlignmentRight;
        self.rowTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        //self.rowTextField.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
        
        // Config TOP BORDER ROW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config BOTTOM BORDER ROW
        self.bottomBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        self.bottomBorderView.hidden = YES;
        
        // COLOR FOR DEVELOPMENT PURPOSES
    }
    return self;
}

@end
