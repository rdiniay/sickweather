//
//  SWReportModel.h
//  Sickweather
//
//  Created by John Erck on 10/16/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWModel.h"
#import <CoreLocation/CoreLocation.h>

typedef void(^SWReportRequestCompletionHandler)(NSArray *reports, NSError *error);

@interface SWReportModel : SWModel

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *illness;
@property (strong, nonatomic) NSString *lat;
@property (strong, nonatomic) NSString *lon;
@property (strong, nonatomic) NSString *timestamp;
- (id)initUsingDict:(NSDictionary *)report;
- (void)executeReportsRequestForIllnessTypeID:(NSString *)illnessTypeID lat:(CLLocationDegrees)lat lon:(CLLocationDegrees)lon limit:(NSNumber *)limit withCompletionHandler:(SWReportRequestCompletionHandler)completionHandler;

@end
