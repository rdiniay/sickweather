//
//  SWGroupProfileCollectionViewCellForCustomView.h
//  Sickweather
//
//  Created by John Erck on 1/21/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWGroupProfileCollectionViewCellForCustomView : UICollectionViewCell
- (void)groupProfileCollectionViewCellForCustomViewSetView:(UIView *)view;
- (void)groupProfileCollectionViewCellForCustomViewSetView:(UIView *)view left:(NSNumber *)left right:(NSNumber *)right top:(NSNumber *)top bottom:(NSNumber *)bottom;
@end
