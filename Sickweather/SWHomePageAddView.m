    //
    //  SWHomePageAddView.m
    //  Sickweather
    //
    //  Created by John Erck on 7/26/17.
    //  Copyright © 2017 Sickweather, LLC. All rights reserved.
    //

#import <MapKit/MapKit.h>
#import "SWHomePageAddView.h"
#import "SWChangeLocationViewController.h"
#import <GooglePlaces/GooglePlaces.h>

@interface SWHomePageAddView() <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, GMSAutocompleteTableDataSourceDelegate>
@property (strong, nonatomic) NSDictionary *allUserSavedPlacemarks;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSLayoutConstraint *tableViewHeightConstraint;
@property (strong, nonatomic) MKMapView *myMapView;
@property (strong, nonatomic) UIView *mapOverlayView;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) UITableViewController *resultsController;
@property (strong, nonatomic) GMSAutocompleteTableDataSource *tableDataSource;
@property int searchDelayCount;
@end

@implementation SWHomePageAddView

#pragma mark - Public Methods

- (void)activateKeyboard
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.searchBar becomeFirstResponder];
    });
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
        {
        [[self.allUserSavedPlacemarks objectForKey:@"placemarks"] removeObjectAtIndex:indexPath.row];
        [[SWHelper helperAppDelegate] savePlacemarksToPlacesFile:self.allUserSavedPlacemarks];
		[self.delegate homePageAddViewDeletedLocation:self];
	}
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [[self.allUserSavedPlacemarks objectForKey:@"placemarks"] exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    [[SWHelper helperAppDelegate] savePlacemarksToPlacesFile:self.allUserSavedPlacemarks];
    if (@available(iOS 11.0, *))
        {
        [self.tableView performBatchUpdates:^{
            [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
        } completion:^(BOOL finished) {
            [self.delegate homePageAddViewReorderedLocations:self];
        }];
        }
    else
        {
        [self.tableView beginUpdates];
        [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
        [self.tableView endUpdates];
        [self.delegate homePageAddViewReorderedLocations:self];
        }
}

#pragma mark - GMSAutocompleteTableDataSourceDelegate

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    [self.searchBar resignFirstResponder];
    if (place != nil) {
		NSDictionary *allUserSavedPlacemarks = [[SWHelper helperAppDelegate] getPlacemarksFromPlacesFile];
		if (allUserSavedPlacemarks.count > 0) {
			for (NSDictionary *placemarkDict in [allUserSavedPlacemarks objectForKey:@"placemarks"]){
				NSString *latitude = [placemarkDict objectForKey:@"latitude"];
				NSString *longitude = [placemarkDict objectForKey:@"longitude"];
				NSString *savedLatitude = [NSString stringWithFormat:@"%@", @(place.coordinate.latitude)];
				NSString *savedLongitude =  [NSString stringWithFormat:@"%@", @(place.coordinate.longitude)];
				if ([latitude isEqualToString:savedLatitude] && [longitude isEqualToString:savedLongitude]) {
					[self.delegate duplicationPlaceError:@"You already have this location in list."];
					return;
				}
			}
		}
        [[SWHelper helperAppDelegate] addGooglePlacemarkToPlacesFile:place];
        [self.delegate homePageAddViewAddedLocation:self];
    }
}

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    if(error){
        NSLog(@"%@",error.localizedDescription);
    }
}

- (void)didRequestAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.resultsController.tableView reloadData];
}

- (void)didUpdateAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.resultsController.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.allUserSavedPlacemarks objectForKey:@"placemarks"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil)
        {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    
    NSDictionary *userSavedLocation = (NSDictionary *)[[self.allUserSavedPlacemarks objectForKey:@"placemarks"] objectAtIndex:indexPath.row];
    cell.textLabel.text = [userSavedLocation objectForKey:@"locality"];
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(![self.resultsController.view isDescendantOfView:self]) {
        [self addSearchResultsView];
    }
    if ([searchText isEqualToString:@""]) {
        [self removeSearchResultsView];
        return;
    }
    double delayInSeconds = 0.8;
    self.searchDelayCount++;
    int count = self.searchDelayCount;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(count == self.searchDelayCount){
            [self.tableDataSource sourceTextHasChanged:searchText];
        }
    });
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self addSearchResultsView];
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    [self removeSearchResultsView];
    return YES;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self removeSearchResultsView];
    [self.spinner startAnimating];
    [[SWHelper helperGeocodeManager] geocodeAddressString:searchBar.text completionHandler:^(NSArray* placemarks, NSError* error) {
        if ([placemarks count] > 0 && error == nil){
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
			NSDictionary *allUserSavedPlacemarks = [[SWHelper helperAppDelegate] getPlacemarksFromPlacesFile];
			if (allUserSavedPlacemarks.count > 0) {
				for (NSDictionary *placemarkDict in [allUserSavedPlacemarks objectForKey:@"placemarks"]){
					NSString *latitude = [placemarkDict objectForKey:@"latitude"];
					NSString *longitude = [placemarkDict objectForKey:@"longitude"];
					NSString *savedLatitude = [NSString stringWithFormat:@"%@", @(placemark.location.coordinate.latitude)];
					NSString *savedLongitude =  [NSString stringWithFormat:@"%@", @(placemark.location.coordinate.longitude)];
					if ([latitude isEqualToString:savedLatitude] && [longitude isEqualToString:savedLongitude]) {
						[self.delegate duplicationPlaceError:@"You already have this location in list."];
						return;
					}
				}
			}
            [[SWHelper helperAppDelegate] addPlacemarkToPlacesFile:placemark];
            [self.delegate homePageAddViewAddedLocation:self];
            }
        else
            {
                // No results found
            }
        [self.spinner stopAnimating];
    }];
}

#pragma mark - Target/Action

-(void)addSearchResultsView{
    UIViewController *vc = [self viewController];
        //if ([vc isKindOfClass:[SWHomeViewController class]]){
    [vc addChildViewController: self.resultsController];
        //Add the results controller.
    self.resultsController.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.resultsController.view.alpha = 0.0f;
    [self addSubview:self.resultsController.view];
    
        // Layout it out below the text field using auto layout.
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:[searchBar]-[resultView]-(0)-|"
                          options:0
                          metrics:nil
                          views:@{
                                  @"searchBar" : self.searchBar,
                                  @"resultView" : _resultsController.view
                                  }]];
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"H:|-(0)-[resultView]-(0)-|"
                          options:0
                          metrics:nil
                          views:@{
                                  @"resultView" : _resultsController.view
                                  }]];
    
        // Force a layout pass otherwise the table will animate in weirdly.
    [self layoutIfNeeded];
    
        // Reload the data.
    [self.resultsController.tableView reloadData];
    
        // Animate in the results.
    [UIView animateWithDuration:0.5 animations:^{
        self.resultsController.view.alpha = 1.0f;
    }
                     completion:^(BOOL finished) {
                         [self.resultsController didMoveToParentViewController:vc];}];
        //}
}

-(void)removeSearchResultsView{
    [_resultsController willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.5 animations:^{
        self.resultsController.view.alpha = 0.0f;
    }completion:^(BOOL finished) {
        [self.resultsController.view removeFromSuperview];
        [self.resultsController removeFromParentViewController];
    }];
}

- (void)didUpdateCurrentLocationPlacemarkNotification:(NSNotification *)notification
{
    NSString *error = [notification.userInfo objectForKey:@"error"];
    CLPlacemark *placemark = [notification.userInfo objectForKey:@"placemark"];
    if (error)
        {
        }
    else if (placemark)
        {
        self.myMapView.showsUserLocation = YES;
        [self.myMapView setCenterCoordinate:placemark.location.coordinate];
        }
}

- (void)tableViewTapped:(id)sender
{
    [self endEditing:YES];
}

#pragma mark - Life Cycle Methods

- (void)dealloc
{
        // REMOVE OBSERVERS
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)init
{
    self = [super init];
    if (self)
        {
            // TELL US WHEN THE USER'S CURRENT LOCATION HAS BEEN TRANSLATED TO A PLACEMARK OBJECT
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didUpdateCurrentLocationPlacemarkNotification:)
                                                     name:kSWDidUpdateCurrentLocationPlacemarkNotification
                                                   object:nil];
        
            // PRE INIT
        self.allUserSavedPlacemarks = [[SWHelper helperAppDelegate] getPlacemarksFromPlacesFile];
        
            // INIT ALL REQUIRED UI ELEMENTS
        self.searchBar = [[UISearchBar alloc] init];
        self.tableView = [[UITableView alloc] init];
        self.myMapView = [[MKMapView alloc] init];
        self.mapOverlayView = [[UIView alloc] init];
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
            // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.searchBar forKey:@"searchBar"];
        [self.viewsDictionary setObject:self.tableView forKey:@"tableView"];
        [self.viewsDictionary setObject:self.myMapView forKey:@"myMapView"];
        [self.viewsDictionary setObject:self.mapOverlayView forKey:@"mapOverlayView"];
        [self.viewsDictionary setObject:self.spinner forKey:@"spinner"];
        
            // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.searchBar.translatesAutoresizingMaskIntoConstraints = NO;
        self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
        self.myMapView.translatesAutoresizingMaskIntoConstraints = NO;
        self.mapOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
        self.spinner.translatesAutoresizingMaskIntoConstraints = NO;
        
            // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.searchBar];
        [self addSubview:self.tableView];
        [self addSubview:self.myMapView];
        [self addSubview:self.mapOverlayView];
        [self addSubview:self.spinner];
        
            // LAYOUT
        
            // searchBar
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[searchBar]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[searchBar(60)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
            // tableView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[tableView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[searchBar]-(0)-[tableView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
            // myMapView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[myMapView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[tableView]-(0)-[myMapView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
            // mapOverlayView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[mapOverlayView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[tableView]-(0)-[mapOverlayView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
            // spinner
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[searchBar]-(20)-[spinner]"] options:0 metrics:0 views:self.viewsDictionary]];
        
            // CONFIG
        
            // searchBar
        self.searchBar.delegate = self;
        self.searchBar.placeholder = @"Search for locations to add...";
        self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
        // tableView
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView setEditing:YES animated:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableViewTapped:)];
        [tap setCancelsTouchesInView:NO];
        [self.tableView addGestureRecognizer:tap];
        [self.tableView setAlwaysBounceVertical:NO];
        
        // mapOverlayView
        self.mapOverlayView.backgroundColor = [UIColor whiteColor];
        self.mapOverlayView.alpha = 0.8;
        
        // Setup the results view controller.
        self.tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        filter.type = kGMSPlacesAutocompleteTypeFilterCity;
        self.tableDataSource.autocompleteFilter = filter;
        self.tableDataSource.delegate = self;
        self.resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
        self.resultsController.tableView.delegate = _tableDataSource;
        self.resultsController.tableView.dataSource = _tableDataSource;
    }
    return self;
}

- (UIViewController *)viewController {
    if ([self.nextResponder isKindOfClass:UIViewController.class])
        return (UIViewController *)self.nextResponder;
    else
        return nil;
}

@end
