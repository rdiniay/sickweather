//
//  SWReportCDM+CoreDataProperties.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SWReportCDM+CoreDataProperties.h"

@implementation SWReportCDM (CoreDataProperties)

@dynamic feedText;
@dynamic latitude;
@dynamic longitude;
@dynamic postedTimestampUTC;
@dynamic reportDate;
@dynamic reportId;
@dynamic feedId;
@dynamic illness;
@dynamic owner;

@end
