//
//  SWGroupsViewControllerCollectionViewLayout.h
//  Sickweather
//
//  Created by John Erck on 8/24/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWGroupsViewControllerCollectionViewLayoutDelegate
- (NSAttributedString *)groupsViewControllerCollectionViewLayoutDelegateAttributedTitleStringForStandardItemCollectionViewCell:(NSIndexPath *)indexPath;
- (CGFloat)sectionHeaderPreferredVerticalHeightForIndexPath:(NSIndexPath *)indexPath;
- (CGFloat)totalExtraTitleMarginNeededForIndexPath:(NSIndexPath *)indexPath;
@end

@interface SWGroupsViewControllerCollectionViewLayout : UICollectionViewLayout
@property (nonatomic, weak) id<SWGroupsViewControllerCollectionViewLayoutDelegate> delegate;
@end
