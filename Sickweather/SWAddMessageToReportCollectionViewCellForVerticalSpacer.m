//
//  SWAddMessageToReportCollectionViewCellForVerticalSpacer.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportCollectionViewCellForVerticalSpacer.h"
#import "NSString+FontAwesome.h"
#import "SWColor.h"
#import "SWFont.h"

@interface SWAddMessageToReportCollectionViewCellForVerticalSpacer ()
@property (strong, nonatomic) NSArray *horizontalConstraints;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *myView;
@end

@implementation SWAddMessageToReportCollectionViewCellForVerticalSpacer

#pragma mark - Public Methods

#pragma mark - Target/Action

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.horizontalConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.myView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.myView forKey:@"myView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.myView];
        
        // LAYOUT
        
        // Layout MY VIEW
        self.horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[myView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary];
        [self.contentView addConstraints:self.horizontalConstraints];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[myView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config MY VIEW
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.myView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
