//
//  SWBlockTabWithLoginView.m
//  Sickweather
//
//  Created by John Erck on 12/20/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWBlockTabWithLoginView.h"

@interface SWBlockTabWithLoginView ()

// UI VIEWS
@property (strong, nonatomic) NSMutableDictionary *myViewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *topImageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;
@property (strong, nonatomic) UIButton *primaryButton;

@end

@implementation SWBlockTabWithLoginView

#pragma mark - Public Methods

- (void)updateBackgroundColor:(UIColor *)color
{
    self.backgroundColor = color;
}

- (void)updateImage:(UIImage *)image
{
    self.topImageView.image = image;
}

- (void)updateTitle:(NSString *)title
{
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:26]}];
}

- (void)updateDescription:(NSString *)description
{
    self.descriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:description attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:18]}];
}

- (void)updateButtonTitle:(NSString *)title
{
    [self.primaryButton setAttributedTitle:[[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
}

#pragma mark - Target/Action

- (void)primaryButtonTapped:(id)sender
{
    [self.delegate primaryButtonTapped:self];
}

#pragma mark - Helpers

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT DEFAULTS
        
        // INIT VIEWS
        self.scrollView = [[UIScrollView alloc] init];
        self.topImageView = [[UIImageView alloc] init];
        self.titleLabel = [[UILabel alloc] init];
        self.descriptionLabel = [[UILabel alloc] init];
        self.primaryButton = [[UIButton alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.myViewsDictionary = [NSMutableDictionary new];
        [self.myViewsDictionary setObject:self.scrollView forKey:@"scrollView"];
        [self.myViewsDictionary setObject:self.topImageView forKey:@"topImageView"];
        [self.myViewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
        [self.myViewsDictionary setObject:self.descriptionLabel forKey:@"descriptionLabel"];
        [self.myViewsDictionary setObject:self.primaryButton forKey:@"primaryButton"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
        self.topImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.primaryButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
        [self addSubview:self.scrollView];
        [self.scrollView addSubview:self.topImageView];
        [self.scrollView addSubview:self.titleLabel];
        [self.scrollView addSubview:self.descriptionLabel];
        [self.scrollView addSubview:self.primaryButton];
        
        // LAYOUT
        NSArray *myConstraints = @[];
        
        // scrollView
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[scrollView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // topImageView
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[topImageView]-(0)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:self.topImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[topImageView(300)]"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // titleLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[titleLabel]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[topImageView]-(5)-[titleLabel]"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // descriptionLabel
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[descriptionLabel]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(10)-[descriptionLabel]"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // primaryButton
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[primaryButton]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        myConstraints = [myConstraints arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[descriptionLabel]-(40)-[primaryButton(60)]-(20)-|"] options:0 metrics:0 views:self.myViewsDictionary]];
        
        // ADD CONSTRAINTS
        [self addConstraints:myConstraints];
        
        // CONFIG
        
        // scrollView
        self.scrollView.alwaysBounceVertical = YES;
        
        // topImageView
        self.topImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        // titleLabel
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        
        // descriptionLabel
        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.descriptionLabel.numberOfLines = 0;
        
        // primaryButton
        self.primaryButton.backgroundColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
        self.primaryButton.layer.cornerRadius = 6;
        [self.primaryButton addTarget:self action:@selector(primaryButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

@end
