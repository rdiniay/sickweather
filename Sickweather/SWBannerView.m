//
//  SWBannerView.m
//  Sickweather
//
//  Created by John Erck on 10/24/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWBannerView.h"

@interface SWBannerView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UILabel *messageLabel;
@property (strong, nonatomic) UIButton *dismissButton;
@end

@implementation SWBannerView

#pragma mark - Public Methods

- (void)updateMessage:(NSString *)message
{
    self.messageLabel.attributedText = [[NSAttributedString alloc] initWithString:message attributes:[SWBannerView getMessageTextAttributes]];
}

#pragma mark - Target/Action

- (void)dismissButtonTapped:(id)sender
{
    [self.delegate bannerViewWantsToDismiss:self];
}

#pragma mark - Helpers

+ (NSDictionary *)getMessageTextAttributes
{
    return @{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16],NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255]};
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.messageLabel = [[UILabel alloc] init];
        self.dismissButton = [[UIButton alloc] init];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.messageLabel];
        [self addSubview:self.dismissButton];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.messageLabel forKey:@"messageLabel"];
        [self.viewsDictionary setObject:self.dismissButton forKey:@"dismissButton"];
        
        // LAYOUT
        
        // Layout MESSAGE LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[messageLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[messageLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout DISMISS BUTTON
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[dismissButton]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[messageLabel]-[dismissButton]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config CONTENT VIEW
        self.backgroundColor = [SWColor colorSickweatherStyleGuideGreen68x157x68];
        
        // Config MESSAGE LABEL
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.numberOfLines = 0;
        
        // Config DISMISS BUTTON
        [self.dismissButton setTitle:@"OK" forState:UIControlStateNormal];
        [self.dismissButton addTarget:self action:@selector(dismissButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.dismissButton.layer setCornerRadius:10];
        [self.dismissButton.layer setBorderColor:[UIColor whiteColor].CGColor];
        [self.dismissButton.layer setBorderWidth:1.0];
    }
    return self;
}

@end
