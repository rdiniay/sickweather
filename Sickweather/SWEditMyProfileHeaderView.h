//
//  SWEditMyProfileHeaderView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWEditMyProfileHeaderView;

@protocol SWEditMyProfileHeaderViewDelegate
- (NSString *)editMyProfileHeaderViewFirstNameString:(SWEditMyProfileHeaderView *)sender;
- (NSString *)editMyProfileHeaderViewLastNameString:(SWEditMyProfileHeaderView *)sender;
- (UIImage *)editMyProfileHeaderViewProfilePicImage:(SWEditMyProfileHeaderView *)sender;
- (void)editMyProfileHeaderViewProfilePicTapped:(SWEditMyProfileHeaderView *)sender;
@end

@interface SWEditMyProfileHeaderView : UIView
@property (nonatomic, weak) id<SWEditMyProfileHeaderViewDelegate> delegate;
@property (strong, nonatomic) UITextField *firstNameTextField;
@property (strong, nonatomic) UITextField *lastNameTextField;
- (void)setProfilePicImage:(UIImage *)image;
- (UIImage *)profilePicImage;
@end
