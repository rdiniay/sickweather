//
//  SWEditMyProfileRowView.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWEditMyProfileRowView.h"

@interface SWEditMyProfileRowView ()
@property (assign) BOOL shouldShowTopBorder;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *rowIconImageView;
@property (strong, nonatomic) UITextField *rowTextField;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UIView *bottomBorderView;
@end

@implementation SWEditMyProfileRowView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWEditMyProfileRowViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    if ([self.delegate editMyProfileRowViewIconImage:self])
    {
        self.rowIconImageView.image = [self.delegate editMyProfileRowViewIconImage:self];
    }
    [self setRowTextFieldText:[self.delegate editMyProfileRowViewTitleString:self]];
    self.rowTextField.placeholder = [self.delegate editMyProfileRowViewPlaceholderString:self];
	self.rowTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

#pragma mark - Public Methods

- (void)setRowTextFieldText:(NSString *)text;
{
    self.rowTextField.attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
}

- (void)showBottomBorder
{
    self.bottomBorderView.hidden = NO;
}

- (NSString *)currentText
{
    return self.rowTextField.text;
}

- (UITextField *)currentTextField
{
    return self.rowTextField;
}

- (void)addDisclosureIconAndTapFeature
{
    self.rowTextField.userInteractionEnabled = NO;
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
}

- (void)highlightNewlySelectedValue
{
    UIView *overlayView = [[UIView alloc] initWithFrame:self.bounds];
    overlayView.backgroundColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
    overlayView.alpha = 0.0;
    [self addSubview:overlayView];
    [UIView animateWithDuration:0.25 animations:^{
        overlayView.alpha = 0.5;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.25 animations:^{
            overlayView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [overlayView removeFromSuperview];
        }];
    }];
}

#pragma mark - Target Action

- (void)viewWasTapped:(id)sender
{
    [self.delegate editMyProfileRowWasTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.rowIconImageView = [[UIImageView alloc] init];
        self.rowTextField = [[UITextField alloc] init];
        self.topBorderView = [[UIView alloc] init];
        self.bottomBorderView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.rowIconImageView forKey:@"rowIconImageView"];
        [self.viewsDictionary setObject:self.rowTextField forKey:@"rowTextField"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        [self.viewsDictionary setObject:self.bottomBorderView forKey:@"bottomBorderView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.rowIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.rowIconImageView];
        [self addSubview:self.rowTextField];
        [self addSubview:self.topBorderView];
        [self addSubview:self.bottomBorderView];
        
        // LAYOUT
        
        // Layout ROW ICON IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[rowIconImageView(20)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rowIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout SECTION LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowIconImageView]-(15)-[rowTextField]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[rowTextField]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TOP BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topBorderView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout BOTTOM BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[bottomBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[bottomBorderView(1)]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config ROW
        self.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        
        // Config ROW ICON IMAGE VIEW
        self.rowIconImageView.contentMode = UIViewContentModeCenter;
        
        // Config ROW TEXT FIELD
        self.rowTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.rowTextField.placeholder = @"Example placeholder...";
        self.rowTextField.font = [SWFont fontStandardFontOfSize:16];
        self.rowTextField.textColor = [SWColor colorSickweatherStyleGuideGray79x76x77];
        
        // Config TOP BORDER ROW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config BOTTOM BORDER ROW
        self.bottomBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        self.bottomBorderView.hidden = YES;
        
        // COLOR FOR DEVELOPMENT PURPOSES
//        self.rowIconImageView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
//        self.rowTextField.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
