//
//  SWGroupProfileCollectionViewCellForMessageSpacer.m
//  Sickweather
//
//  Created by John Erck on 1/22/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForMessageSpacer.h"
#import "NSString+FontAwesome.h"

@interface SWGroupProfileCollectionViewCellForMessageSpacer ()
@property (strong, nonatomic) NSArray *horizontalConstraints;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *myView;
@property (strong, nonatomic) UIButton *myMessageActionsButton;
@end

@implementation SWGroupProfileCollectionViewCellForMessageSpacer

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForMessageSpacerSetMarginLeft:(NSNumber *)left right:(NSNumber *)right
{
    [self.contentView removeConstraints:self.horizontalConstraints];
    self.horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[myView]-(right)-|" options:0 metrics:@{@"left":left,@"right":right} views:self.viewsDictionary];
    [self.contentView addConstraints:self.horizontalConstraints];
}

#pragma mark - Target/Action

- (void)myMessageActionsButtonTapped:(id)sender
{
    [self.delegate groupProfileCollectionViewCellForMessageSpacerMessageActionButtonTapped:self];
}

#pragma mark - Life Cycle Methods

// http://stackoverflow.com/a/24626651/394969 <-- pointInside & hitTest (for buttons with negative offsets)

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if ([super pointInside:point withEvent:event])
    {
        return YES;
    }
    
    //Check to see if it is within the my message actions button
    return !self.myMessageActionsButton.hidden && [self.myMessageActionsButton pointInside:[self.myMessageActionsButton convertPoint:point fromView:self] withEvent:event];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *view = [self.myMessageActionsButton hitTest:[self.myMessageActionsButton convertPoint:point fromView:self] withEvent:event];
    if (view == nil)
    {
        view = [super hitTest:point withEvent:event];
    }
    return view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        self.horizontalConstraints = @[];
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.myView = [[UIView alloc] init];
        self.myMessageActionsButton = [[UIButton alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.myView forKey:@"myView"];
        [self.viewsDictionary setObject:self.myMessageActionsButton forKey:@"myMessageActionsButton"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myView.translatesAutoresizingMaskIntoConstraints = NO;
        self.myMessageActionsButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.myView];
        [self.contentView addSubview:self.myMessageActionsButton];
        
        // LAYOUT
        
        // Layout MY VIEW
        self.horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[myView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary];
        [self.contentView addConstraints:self.horizontalConstraints];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[myView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout MY MESSAGE ACTIONS BUTTON
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[myMessageActionsButton]-(-18)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(9)-[myMessageActionsButton]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config MY VIEW
        self.myView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Layout MY MESSAGE ACTIONS BUTTON
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:[NSString awesomeIcon:FaAngleDoubleDown] attributes:@{NSFontAttributeName:[SWFont fontAwesomeOfSize:13],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayLight133x133x134]}];
        [self.myMessageActionsButton setAttributedTitle:title forState:UIControlStateNormal];
        [self.myMessageActionsButton addTarget:self action:@selector(myMessageActionsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//        self.myMessageActionsButton.backgroundColor = [UIColor whiteColor];
//        self.myMessageActionsButton.layer.cornerRadius = 10;
//        self.myMessageActionsButton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
//        self.myMessageActionsButton.layer.borderWidth = 1;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.myView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
