//
//  SWDatePicker.h
//  Sickweather
//
//  Created by John Erck on 10/14/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWDatePicker;

@protocol SWDatePickerDelegate <NSObject>
- (void)datePickerWantsToCancel:(SWDatePicker *)sender;
- (void)datePickerWantsToDone:(SWDatePicker *)sender;
@end

@interface SWDatePicker : UIView
@property (nonatomic, weak) id<SWDatePickerDelegate> delegate;
@property (strong, nonatomic) UIDatePicker *datePicker;
- (void)setTitle:(NSString *)title;
@end
