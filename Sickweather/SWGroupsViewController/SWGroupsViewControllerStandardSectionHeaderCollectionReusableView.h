//
//  SWGroupsViewControllerStandardSectionHeaderCollectionReusableView.h
//  Sickweather
//
//  Created by John Erck on 8/25/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWGroupsViewControllerStandardSectionHeaderCollectionReusableView;

@protocol SWGroupsViewControllerStandardSectionHeaderCollectionReusableViewDelegate
- (void)searchBarTextDidChange:(NSString *)searchText;
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;
- (void)createNewGroupButtonTapped:(SWGroupsViewControllerStandardSectionHeaderCollectionReusableView *)sender;
@end

@interface SWGroupsViewControllerStandardSectionHeaderCollectionReusableView : UICollectionReusableView
@property (nonatomic, weak) id<SWGroupsViewControllerStandardSectionHeaderCollectionReusableViewDelegate> delegate;
@property (strong, nonatomic) NSString *styleStringKey;
@property (strong, nonatomic) NSString *title;
- (void)setCurrentSearchStringText:(NSString *)searchStringText;
- (void)setFocusOnSearch;
- (void)showCreateNewGroupButton;

@end
