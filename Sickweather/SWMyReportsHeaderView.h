//
//  SWMyReportsMessageTableViewHeader.h
//  Sickweather
//
//  Created by John Erck on 2/16/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyReportsHeaderView;

@protocol SWMyReportsMessageTableViewHeaderDelegate
@end

@interface SWMyReportsHeaderView : UIView
@property (nonatomic, weak) id<SWMyReportsMessageTableViewHeaderDelegate> delegate;
@end
