//
//  SWSignInWithFacebookViewController.h
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWSignInWithFacebookViewController;

@protocol SWSignInWithFacebookViewControllerDelegate <NSObject>
- (void)didDismissWithSuccessfulLogin:(SWSignInWithFacebookViewController *)vc;
- (void)didDismissWithoutLoggingIn:(SWSignInWithFacebookViewController *)vc;
@end

@interface SWSignInWithFacebookViewController : UIViewController
@property (weak, nonatomic) id <SWSignInWithFacebookViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *context;
@property (assign) BOOL showStatusBar;
@end
