//
//  SWActivityIndicatorView.m
//  Sickweather
//
//  Created by John Erck on 10/6/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWActivityIndicatorView.h"

@interface SWActivityIndicatorView ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UILabel *messageLabel;
@property (assign) BOOL styleForLight;
@property (strong, nonatomic) NSString *myMessageText;
@end

@implementation SWActivityIndicatorView

#pragma mark - Custom Getters/Setters

#pragma mark - Public Methods

- (void)startAnimating
{
    [self.activityIndicator startAnimating];
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL finished) {
        //[self.activityIndicator startAnimating];
    }];
}

- (void)stopAnimating
{
    [self.activityIndicator stopAnimating];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        //[self.activityIndicator stopAnimating];
        self.hidden = YES;
    }];
}

- (void)setMessageText:(NSString *)messageText
{
    self.myMessageText = messageText;
    [self helperUpdateProps];
}

- (void)addSuccessMessage:(NSString *)successMessage
{
    if (self.styleForLight)
    {
        self.messageLabel.attributedText = [[NSAttributedString alloc] initWithString:successMessage attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
    }
    else
    {
        self.messageLabel.attributedText = [[NSAttributedString alloc] initWithString:successMessage attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
    }
}

- (void)styleForInlineCenterScreenUse
{
    self.layer.cornerRadius = 20;
}

#pragma mark - Helpers

- (void)helperUpdateProps
{
    if (self.myMessageText)
    {
        if (self.styleForLight)
        {
            self.messageLabel.attributedText = [[NSAttributedString alloc] initWithString:self.myMessageText attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
        }
        else
        {
            self.messageLabel.attributedText = [[NSAttributedString alloc] initWithString:self.myMessageText attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
        }
    }
}

#pragma mark - Life Cycle Methods

- (void)sharedInit
{
    // INIT ALL REQUIRED UI ELEMENTS
    self.contentView = [[UIView alloc] init];
    //self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]; // Handled by different init methods
    self.messageLabel = [[UILabel alloc] init];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.activityIndicator];
    [self.contentView addSubview:self.messageLabel];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.activityIndicator forKey:@"activityIndicator"];
    [self.viewsDictionary setObject:self.messageLabel forKey:@"messageLabel"];
    
    // LAYOUT
    
    // Layout CONTENT VIEW
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView(>=100)]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView(>=100)]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout ACTIVITY INDICATOR VIEW
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // Layout MESSAGE LABEL
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[messageLabel]-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[activityIndicator]-[messageLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Config SELF
    self.hidden = YES;
    self.alpha = 0.0;
    
    // Config CONTENT VIEW
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
    
    // Config MESSAGE LABEL
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    
    // Config ACTIVITY INDICATOR VIEW
}

- (id)initUsingLight
{
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        self.styleForLight = YES;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self sharedInit];
    }
    return self;
}

- (id)initUsingDark
{
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        self.styleForLight = NO;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self sharedInit];
    }
    return self;
}

@end
