//
//  SWGroupProfileCollectionViewCellForSympathy.h
//  Sickweather
//
//  Created by John Erck on 5/6/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWGroupProfileCollectionViewCellForSympathy;

@protocol SWGroupProfileCollectionViewCellForSympathyDelegate
- (void)groupProfileCollectionViewCellForSympathyButtonTapped:(SWGroupProfileCollectionViewCellForSympathy *_Nonnull)sender;
@end

@interface SWGroupProfileCollectionViewCellForSympathy : UICollectionViewCell
@property (nonatomic, weak) _Nullable id<SWGroupProfileCollectionViewCellForSympathyDelegate> delegate;
@property (strong, nonnull) NSDictionary *messageDict;
- (void)groupProfileCollectionViewCellForSympathyBumpCount;
- (void)groupProfileCollectionViewCellForSympathySetIconImage:(UIImage *_Nonnull)image;
- (void)groupProfileCollectionViewCellForSympathySetSympathyDict:(NSDictionary *_Nullable)dict;
- (void)groupProfileCollectionViewCellForSympathyUpdateDefaultInsetsLeft:(NSNumber *_Nonnull)left right:(NSNumber *_Nonnull)right top:(NSNumber *_Nonnull)top bottom:(NSNumber *_Nonnull)bottom;
@end
