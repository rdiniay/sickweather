//
//  SWUserCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWUserCDM.h"

@interface SWUserCDM (SWAdditions)

// This method will log the given user in (and create or update the related user record in our local database)
+ (SWUserCDM *)createOrGetAsUpdatedLoggedInUserUsingUserId:(NSString *)sickweatherUserId
                                                 emailHash:(NSString *)emailHash
                                              passwordHash:(NSString *)passwordHash
                                                 firstName:(NSString *)firstName;

// If this method returns nil than you know you don't have a logged in user
+ (SWUserCDM *)currentlyLoggedInUser;

// The following method will ALWAYS return to you the current user
+ (SWUserCDM *)currentUser;

// This method will log the currently logged in user out, and return to you the current user object (keeps total user count at 1)
+ (SWUserCDM *)logUserOut;

- (NSDate *)birthDateAsDate;
- (NSString *)fullName;
- (NSString *)genderForUI;
- (NSMutableDictionary*)userProfileDataForSnow;
- (NSInteger) ageForUI;
- (NSString *)raceEthnicityForUI;
- (NSString *)birthDateForUI;
- (NSString *)healthCarePrefForUI;
- (NSString *)userLocationForUI;
- (NSString *)birthDateDayForServer;
- (NSString *)birthDateMonthForServer;
- (NSString *)birthDateYearForServer;
- (NSString *)formattedAddress;
- (CLPlacemark *)userLastKnownCurrentLocationPlacemarkObject;
- (void)setProfilePicThumbnailImageDataUsingImage:(UIImage *)image;

@end
