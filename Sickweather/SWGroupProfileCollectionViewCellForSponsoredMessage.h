//
//  SWGroupProfileCollectionViewCellForSponsoredMessage.h
//  Sickweather
//
//  Created by John Erck on 10/20/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SWGroupProfileCollectionViewCellForSponsoredMessage;

@protocol SWGroupProfileCollectionViewCellForSponsoredMessageDelegate
- (void)groupProfileCollectionViewCellForSponsoredMessageTapped:(SWGroupProfileCollectionViewCellForSponsoredMessage *)sender;
@end

@interface SWGroupProfileCollectionViewCellForSponsoredMessage : UICollectionViewCell
@property (nonatomic, weak) id<SWGroupProfileCollectionViewCellForSponsoredMessageDelegate> delegate;
+ (CGSize)sizeForSponsoredMessage:(NSString *)messageString givenWidth:(CGFloat)width;
- (void)updateLeftInset:(NSNumber *)leftInset rightInset:(NSNumber *)rightInset;
- (void)updateTopInset:(NSNumber *)topInset bottomInset:(NSNumber *)bottomInset;
- (void)updateForServerSponsorshipDict:(NSDictionary *)sponsorshipDict;
- (void)updateSponsorProfilePicUsingImage:(UIImage *)image;
@end
