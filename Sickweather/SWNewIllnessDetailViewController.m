//
//  SWNewIllnessDetailViewController.m
//  Sickweather
//
//  Created by John Erck on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

// SDK IMPORTS
#import <MapKit/MapKit.h>

// MY OWN HEADER FILE
#import "SWNewIllnessDetailViewController.h"

// CUSTOM ANNOTATIONS
#import "SWCustomMapKitAnnotation.h"
#import "SWPrivateReportMapKitAnnotation.h"
#import "SWSponsoredMarkerMapKitAnnotation.h"

// CUSTOM ANNOTATION VIEWS
#import "SWCustomMapKitAnnotationView.h"
#import "SWPrivateReportMapKitAnnotationView.h"
#import "SWSponsoredMarkerMapKitAnnotationView.h"

// SHOW FULL MAP EXPERIENCE WHEN THE USER TAPS THE MAP HEADER
#import "SWNewMapViewController.h"

@interface SWNewIllnessDetailViewController () <MKMapViewDelegate>

// VIEWS
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIScrollView *containerScrollView;
@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) UIView *mapViewOverlayView;
@property (strong, nonatomic) UIScrollView *horizontalScrollView;
@property (strong, nonatomic) UILabel *horizontalLabel1;
@property (strong, nonatomic) UILabel *horizontalLabel2;
@property (strong, nonatomic) UILabel *horizontalLabel3;
@property (strong, nonatomic) UILabel *horizontalLabel4;
@property (strong, nonatomic) UILabel *horizontalLabel5;
@property (strong, nonatomic) UILabel *horizontalLabel6;
@property (strong, nonatomic) UILabel *horizontalLabel7;
@property (strong, nonatomic) UILabel *horizontalLabel8;
@property (strong, nonatomic) UIView *horizontalLineView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UITextView *descriptionLabel;

// DATA
@property (strong, nonatomic) NSDictionary *networkResponse;
@property (strong, nonatomic) NSString *illnessIDs;
@property (strong, nonatomic) CLLocation *location;

@end

@implementation SWNewIllnessDetailViewController

#pragma mark - PUBLIC METHODS

- (void)updateForIllnessId:(NSString *)illnessId andLocation:(CLLocation *)location
{
    NSString *objectFromDocsUsingName = self.isCovidScore ? @"trending_keywords" : @"illnesses";
    NSString *name = [SWHelper helperIllnessNameForId:illnessId  objectFromDocsUsingName: objectFromDocsUsingName];
    self.title = [NSString stringWithFormat:@"%@ Reports", name];
    self.illnessIDs = illnessId;
    self.location = location;
    
    // HIT THE NETWORK LOOKING FOR REPORTS NEAR THE MAP'S CENTER LOCATION
    [[SWHelper helperAppBackend] appBackendGetMarkersInRadiusForIllnessTypeID:self.illnessIDs lat:location.coordinate.latitude lon:location.coordinate.longitude limit:@(10) usingCompletionHandler:^(NSArray *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        
        // RIGHT AWAY SWITCH BACK TO MAIN THREAD FOR PROCESSING
        dispatch_async(dispatch_get_main_queue(), ^{
            
		// DUMP FOR DEBUG
		//  NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
		//NSLog(@"responseBodyAsString = %@", responseBodyAsString);
            
            // NOW, ITERATE THROUGH EACH OF THE REPORTS RETURNED TO US BY THE SERVER
			if (jsonResponseBody && [jsonResponseBody isKindOfClass:[NSArray class]]) {
				for (NSDictionary *report in jsonResponseBody){
						@try{
							if ([report isKindOfClass:[NSDictionary class]]) {
									// DROP USER LOCATION PREDICATE
								NSPredicate *dropUserLocationPredicate = [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
									return ![object isKindOfClass:[MKUserLocation class]];
								}];
								NSArray *cleanAnnotations = [self.mapView.annotations filteredArrayUsingPredicate:dropUserLocationPredicate];
								if ([report objectForKey:@"id"] != nil) {
										// CHECK TO SEE IF THIS REPORT IS ALREADY ON THE MAP
									NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier==%@",[report objectForKey:@"id"]];
									NSArray *results = [cleanAnnotations filteredArrayUsingPredicate:predicate];
									if ([results count] == 0)
										{
											// REPORT NOT YET ON MAP YET, TIME TO ADD
											//NSLog(@"results = %@", results);
										
											// ADD TO MAP
                                            NSObject <MKAnnotation> *markerAnnotation = [SWNewMapViewController classHelperGetAnnotationForReportDict:report objectFromDocsUsingName:objectFromDocsUsingName];
										[[self mapView] addAnnotation:markerAnnotation];
										
											// CENTER MAP ON PIN IF IS THE FIRST RESULT (NEAREST TO GIVEN LOCATION)
										if ([jsonResponseBody indexOfObject:report] == 0)
											{
												// CENTER MAP ON IT
											[self.mapView setCenterCoordinate:markerAnnotation.coordinate animated:YES];
											
												// AUTO-SELECT IT FOR THE UI
											[self.mapView selectAnnotation:markerAnnotation animated:YES];
										}
										
									}
								}
							}
						}@catch (NSException * e) {
							[SWHelper addNonFatalExceptionWith:e];
					}
				}
			}
			
        });
    }];
}

#pragma mark - MAP VIEW DELEGATE

// http://stackoverflow.com/questions/1857160/how-can-i-create-a-custom-pin-drop-animation-using-mkannotationview/7045916#7045916
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    // INIT STARTING VAR
    MKAnnotationView *aV;
    for (aV in views)
    {
        // SKIP IF WE ARE LOOKING AT THE USER'S CURRENT LOCATION ANNOTATION
        if ([aV.annotation isKindOfClass:[MKUserLocation class]])
        {
            continue;
        }
        
        // SKIP IF ALREADY VISIBLE
        MKMapPoint point =  MKMapPointForCoordinate(aV.annotation.coordinate);
        if (!MKMapRectContainsPoint(self.mapView.visibleMapRect, point))
        {
            continue;
        }
        
        // DEFINE THE END GAME
        CGRect endFrame = aV.frame;
        
        // MOVE ANNOTATION OUT OF VIEW...
        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - self.view.frame.size.height, aV.frame.size.width, aV.frame.size.height);
        
        // ANIMATE THE DROP
        [UIView animateWithDuration:0.5 delay:0.04*[views indexOfObject:aV] options:UIViewAnimationOptionCurveLinear animations:^{
            
            // SET THE END FRAME
            aV.frame = endFrame;
            
        }completion:^(BOOL finished){
            
            // ANIMATE SQUASH
            if (finished)
            {
                [UIView animateWithDuration:0.05 animations:^{
                    aV.transform = CGAffineTransformMakeScale(1.0, 0.8);
                }completion:^(BOOL finished){
                    [UIView animateWithDuration:0.1 animations:^{
                        aV.transform = CGAffineTransformIdentity;
                    }];
                }];
            }
        }];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // The annotation view to display for the specified annotation or nil if you want to display a standard annotation view.
    
    // Check and see if we're looking at the user's current location dot
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        // Then don't trample the user location annotation (pulsing blue dot).
        return nil;
    }
    
    // The annotation view objects act like cells in a tableview. When off screen,
    // they are added to a queue waiting to be reused. This code mirrors that for
    // getting a table cell. First, check if the queue has available annotation views
    // of the right type, identified by the identifier string. If nil is returned,
    // then allocate a new annotation view.
    
    // Define return var
    MKAnnotationView *annotationViewToReturn;
    
    // PUBLIC MARKER
    if ([annotation isKindOfClass:[SWCustomMapKitAnnotation class]])
    {
        // Create reuse id (static for performance)
        static NSString *sickweatherCloudAnnotationReuseId = @"sickweatherCloudAnnotationReuseId";
        
        // The result of the call is being cast (SWCustomMapKitAnnotationView *) to the correct
        // view class or else the compiler complains
        SWCustomMapKitAnnotationView *annotationView = (SWCustomMapKitAnnotationView *)[self.mapView
                                                                                        dequeueReusableAnnotationViewWithIdentifier:sickweatherCloudAnnotationReuseId];
        
        // Check for nil
        if(annotationView == nil)
        {
            // Create for the first time since we haven't yet
            if(self.isCovidScore) {
                annotationView = [[SWCustomMapKitAnnotationView alloc] initWithCovidAnnotation:annotation reuseIdentifier:sickweatherCloudAnnotationReuseId];
            }
            else {
                annotationView = [[SWCustomMapKitAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:sickweatherCloudAnnotationReuseId];
            }
        }
        
        // Tapping the pin produces callout
        annotationView.canShowCallout = YES;
        
        // And then finally we return our annotationView to the caller!
        annotationViewToReturn = annotationView;
    }
    
    // PRIVATE MARKER
    else if ([annotation isKindOfClass:[SWPrivateReportMapKitAnnotation class]])
    {
        // Create reuse id (static for performance)
        static NSString *privateReportMapKitAnnotationReuseId = @"privateReportMapKitAnnotationReuseId";
        
        // Dequeue marker
        SWPrivateReportMapKitAnnotationView *annotationView = (SWPrivateReportMapKitAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:privateReportMapKitAnnotationReuseId];
        
        // Check for nil
        if(annotationView == nil)
        {
            // Create for the first time since we haven't yet
            if (self.isCovidScore) {
                annotationView = [[SWPrivateReportMapKitAnnotationView alloc] initWithCovidAnnotation:annotation reuseIdentifier:privateReportMapKitAnnotationReuseId];
            }
            else {
                annotationView = [[SWPrivateReportMapKitAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:privateReportMapKitAnnotationReuseId];
            }
        }
        
        // Tapping the pin produces callout
        annotationView.canShowCallout = YES;
        
        // And then finally we return our annotationView to the caller!
        annotationViewToReturn = annotationView;
    }
    
    // SPONSORED MARKER
    else if ([annotation isKindOfClass:[SWSponsoredMarkerMapKitAnnotation class]])
    {
        // Cast so we can call methods on it
        SWSponsoredMarkerMapKitAnnotation *sponsoredMarkerAnnotation = (SWSponsoredMarkerMapKitAnnotation *)annotation;
        
        // Create reuse id (static for performance)
        static NSString *sponsoredMarkerMapKitAnnotationViewReuseId = @"SWSponsoredMarkerMapKitAnnotationViewReuseId";
        
        // Cast
        SWSponsoredMarkerMapKitAnnotationView *annotationView = (SWSponsoredMarkerMapKitAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:sponsoredMarkerMapKitAnnotationViewReuseId];
        
        // Check
        if(annotationView == nil)
        {
            // Create (for the first time since we haven't yet)
            if (self.isCovidScore) {
                annotationView = [[SWSponsoredMarkerMapKitAnnotationView alloc] initWithCovidAnnotation:annotation reuseIdentifier:sponsoredMarkerMapKitAnnotationViewReuseId];
            }
            else {
                annotationView = [[SWSponsoredMarkerMapKitAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:sponsoredMarkerMapKitAnnotationViewReuseId];
            }
        }
        
        // Make sure we don't have any outstanding custom callout views
        UIView *sponsoredMapMarkerAnnotationViewCustomCalloutView = [annotationView viewWithTag:[SWConstants sponsoredMapMarkerAnnotationViewCustomCalloutViewTag]];
        //NSLog(@"sponsoredMapMarkerAnnotationViewCustomCalloutView = %@", sponsoredMapMarkerAnnotationViewCustomCalloutView);
        [sponsoredMapMarkerAnnotationViewCustomCalloutView removeFromSuperview];
        
        // Set image each time
        UIImage *image = [[SWHelper helperAppBackend] fetchImage:sponsoredMarkerAnnotation.markerImgURL usingCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *newImage = [UIImage imageWithData:data scale:[UIScreen mainScreen].scale];
                annotationView.image = newImage;
                [annotationView sizeToFit];
                annotationView.centerOffset = CGPointMake(0, -newImage.size.height/2);
            });
        }];
        if (image)
        {
            annotationView.image = image;
            [annotationView sizeToFit];
        }
        
        // Tapping the pin produces callout
        annotationView.canShowCallout = NO; // We have a custom one
        
        //annotationView.draggable = YES; // Uncommment to debug using new_sponsor-marker_walgreens-dev-crosshairs-with-outline and NO offset to gather the perfect lat lon coors for development and perfect placement purposes. Search for addSponsoredMarkersForCoordinate to see where you'd set the values based on incoming marker ID to override the web service's inaccurate lat/lon vals.
        
        // And then finally we return our annotationView to the caller!
        annotationViewToReturn = annotationView;
    }
    
    // Serve it up!
    return annotationViewToReturn;
}


#pragma mark - HELPERS

- (NSString *)getHTMLPageForHTMLContent:(NSString *)htmlContent
{
    NSMutableString *html = [NSMutableString stringWithString: @"<html><head><title></title></head><body style=\"background:transparent;\">"];
    [html appendString:htmlContent];
    [html appendString:@"</body></html>"];
    return [htmlContent description];
}

- (NSString *)stringByStrippingHTML:(NSString *)s
{
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (NSAttributedString *)attributedStringForTitle:(NSString *)title
{
    return [[NSAttributedString alloc] initWithString:[title uppercaseString] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:18],NSForegroundColorAttributeName:[SWColor colorSickweatherBlack0x0x0]}];
}

- (NSAttributedString *)attributedStringForContent:(NSString *)content
{
	NSString *html = [NSString stringWithFormat:@"%@ <style>body{font-family: 'Raleway'; font-size:15px;}</style>",content];
	return [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
}

-(void) highlightLabel:(int)Index
{
    UIColor* highlightColor = [SWColor colorSickweatherBlueSelected2x146x207];
    switch (Index) {
        case 1:
            self.horizontalLabel1.textColor = highlightColor;
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 2:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = highlightColor;
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 3:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = highlightColor;
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 4:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = highlightColor;
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 5:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = highlightColor;
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 6:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = highlightColor;
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 7:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = highlightColor;
            self.horizontalLabel8.textColor = [SWColor colorSickweatherGrayText135x135x135];
            break;
        case 8:
            self.horizontalLabel1.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel2.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel3.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel4.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel5.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel6.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel7.textColor = [SWColor colorSickweatherGrayText135x135x135];
            self.horizontalLabel8.textColor = highlightColor;
            break;

        default:
            break;
    }
}

#pragma mark - TARGET ACTION

- (void)mapViewTapped:(id)sender
{
    SWNewMapViewController *newMapVC = [[SWNewMapViewController alloc] init];
    newMapVC.isCovidScore = self.isCovidScore;
    [newMapVC updateForIllnessIDs:[self.illnessIDs componentsSeparatedByString:@","] andLocation:self.location];
    [self.navigationController pushViewController:newMapVC animated:YES];
}

- (void)labelTapped:(id)sender
{
    NSLog(@"HERE");
    if ([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        UITapGestureRecognizer *tap = sender;
        double selfNumber = 0;
        NSString * sectionSlug = @"what-is-disease";
        if ([tap.view isEqual:self.horizontalLabel1])
        {
            selfNumber = 1;
            sectionSlug = @"what-is-disease";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel2])
        {
            selfNumber = 2;
            sectionSlug = @"causes";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel3])
        {
            selfNumber = 3;
            sectionSlug = @"symptoms";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel4])
        {
            selfNumber = 4;
            sectionSlug = @"how-diagnosed";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel5])
        {
            selfNumber = 5;
            sectionSlug = @"treatment";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel6])
        {
            selfNumber = 6;
            sectionSlug = @"when-to-call-doctor";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel7])
        {
            selfNumber = 7;
            sectionSlug = @"key-points";
            [self highlightLabel:selfNumber];
        }
        if ([tap.view isEqual:self.horizontalLabel8])
        {
            selfNumber = 8;
            sectionSlug = @"action-alert";
            [self highlightLabel:selfNumber];
        }

        NSLog(@"LABEL %@", sectionSlug);
        
        if ([[self.networkResponse objectForKey:@"sections"] isKindOfClass:[NSArray class]])
        {
            NSArray *sections = [self.networkResponse objectForKey:@"sections"];
            for (int index = 0; index < [sections count]; index++) {
                
                if ([[sections objectAtIndex:index] isKindOfClass:[NSDictionary class]]) {
                   NSDictionary *section = [sections objectAtIndex:index];
                    
                    if ([[section objectForKey:@"section_slug"] isEqual:sectionSlug]) {
                        if ([[section objectForKey:@"section_title"] isKindOfClass:[NSString class]])
                        {
                            self.titleLabel.attributedText = [self attributedStringForTitle:[section objectForKey:@"section_title"]];
                        }
                        if ([[section objectForKey:@"content"] isKindOfClass:[NSString class]])
                        {
                            self.descriptionLabel.attributedText = [self attributedStringForContent:[section objectForKey:@"content"]];
                        }
                        break;
                    }
               }
            }
            
        }
    }
}

#pragma mark - LIFE CYCLE METHODS

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.containerScrollView = [[UIScrollView alloc] init];
    self.mapView = [[MKMapView alloc] init];
    self.mapViewOverlayView = [[UIView alloc] init];
    self.horizontalScrollView = [[UIScrollView alloc] init];
    self.horizontalLabel1 = [[UILabel alloc] init];
    self.horizontalLabel2 = [[UILabel alloc] init];
    self.horizontalLabel3 = [[UILabel alloc] init];
    self.horizontalLabel4 = [[UILabel alloc] init];
    self.horizontalLabel5 = [[UILabel alloc] init];
    self.horizontalLabel6 = [[UILabel alloc] init];
    self.horizontalLabel7 = [[UILabel alloc] init];
    self.horizontalLabel8 = [[UILabel alloc] init];
    self.horizontalLineView = [[UIView alloc] init];
    self.titleLabel = [[UILabel alloc] init];
    self.descriptionLabel = [[UITextView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.containerScrollView forKey:@"containerScrollView"];
    [self.viewsDictionary setObject:self.mapView forKey:@"mapView"];
    [self.viewsDictionary setObject:self.mapViewOverlayView forKey:@"mapViewOverlayView"];
    [self.viewsDictionary setObject:self.horizontalScrollView forKey:@"horizontalScrollView"];
    [self.viewsDictionary setObject:self.horizontalLabel1 forKey:@"horizontalLabel1"];
    [self.viewsDictionary setObject:self.horizontalLabel2 forKey:@"horizontalLabel2"];
    [self.viewsDictionary setObject:self.horizontalLabel3 forKey:@"horizontalLabel3"];
    [self.viewsDictionary setObject:self.horizontalLabel4 forKey:@"horizontalLabel4"];
    [self.viewsDictionary setObject:self.horizontalLabel5 forKey:@"horizontalLabel5"];
    [self.viewsDictionary setObject:self.horizontalLabel6 forKey:@"horizontalLabel6"];
    [self.viewsDictionary setObject:self.horizontalLabel7 forKey:@"horizontalLabel7"];
    [self.viewsDictionary setObject:self.horizontalLabel8 forKey:@"horizontalLabel8"];
    [self.viewsDictionary setObject:self.horizontalLineView forKey:@"horizontalLineView"];
    [self.viewsDictionary setObject:self.titleLabel forKey:@"titleLabel"];
    [self.viewsDictionary setObject:self.descriptionLabel forKey:@"descriptionLabel"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.containerScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.mapView.translatesAutoresizingMaskIntoConstraints = NO;
    self.mapViewOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel1.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel2.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel3.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel4.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel5.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel6.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel7.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLabel8.translatesAutoresizingMaskIntoConstraints = NO;
    self.horizontalLineView.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.containerScrollView];
    [self.containerScrollView addSubview:self.mapView];
    [self.containerScrollView addSubview:self.mapViewOverlayView];
    [self.containerScrollView addSubview:self.horizontalScrollView];
    [self.horizontalScrollView addSubview:self.horizontalLabel1];
    [self.horizontalScrollView addSubview:self.horizontalLabel2];
    [self.horizontalScrollView addSubview:self.horizontalLabel3];
    [self.horizontalScrollView addSubview:self.horizontalLabel4];
    [self.horizontalScrollView addSubview:self.horizontalLabel5];
    [self.horizontalScrollView addSubview:self.horizontalLabel6];
    [self.horizontalScrollView addSubview:self.horizontalLabel7];
    [self.horizontalScrollView addSubview:self.horizontalLabel8];
    [self.containerScrollView addSubview:self.horizontalLineView];
    [self.containerScrollView addSubview:self.titleLabel];
    [self.containerScrollView addSubview:self.descriptionLabel];
    
    // LAYOUT
    
    // containerScrollView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[containerScrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[containerScrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // mapView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[mapView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mapView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]; // PUSH OUT, SET WIDTH
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[mapView(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // mapViewOverlayView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[mapViewOverlayView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.mapViewOverlayView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]]; // PUSH OUT, SET WIDTH
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[mapViewOverlayView(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // horizontalScrollView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[horizontalScrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[mapView]-(0)-[horizontalScrollView(50)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // horizontalLabel1
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[horizontalLabel1]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel1 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel2
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel1]-(10)-[horizontalLabel2]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel2 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel3
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel2]-(10)-[horizontalLabel3]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel3 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel4
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel3]-(10)-[horizontalLabel4]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel4 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel5
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel4]-(10)-[horizontalLabel5]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel5 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel6
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel5]-(10)-[horizontalLabel6]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel6 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel7
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel6]-(10)-[horizontalLabel7]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel7 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLabel8
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[horizontalLabel7]-(10)-[horizontalLabel8]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.horizontalLabel8 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.horizontalScrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    // horizontalLineView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[horizontalLineView]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[horizontalScrollView]-(0)-[horizontalLineView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // titleLabel
    if (@available(iOS 11.0, *))
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10]];
    }
    else
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[titleLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    }
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[horizontalLineView]-(20)-[titleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // descriptionLabel
    if (@available(iOS 11.0, *))
    {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.descriptionLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.descriptionLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10]];
    }
    else
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(10)-[descriptionLabel]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    }
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[titleLabel]-(15)-[descriptionLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // self
    self.view.backgroundColor = [UIColor whiteColor];
    
    // containerScrollView
    //self.containerScrollView.delegate = self;
    self.containerScrollView.alwaysBounceVertical = YES;
    
    // mapView
    self.mapView.delegate = self;
    self.mapView.userInteractionEnabled = NO;
    
    // mapViewOverlayView
    UITapGestureRecognizer *mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapViewTapped:)];
    [self.mapViewOverlayView addGestureRecognizer:mapTap];
    self.mapViewOverlayView.userInteractionEnabled = YES;
    
    // horizontalScrollView
    //self.horizontalScrollView.backgroundColor = [UIColor grayColor];
    self.horizontalScrollView.alwaysBounceHorizontal = YES;
    self.horizontalScrollView.showsHorizontalScrollIndicator = NO;
    
    // horizontalLabel1
    UITapGestureRecognizer *horizontalLabel1Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel1 addGestureRecognizer:horizontalLabel1Tap];
    self.horizontalLabel1.userInteractionEnabled = YES;
    
    // horizontalLabel2
    UITapGestureRecognizer *horizontalLabel2Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel2 addGestureRecognizer:horizontalLabel2Tap];
    self.horizontalLabel2.userInteractionEnabled = YES;
    
    // horizontalLabel3
    UITapGestureRecognizer *horizontalLabel3Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel3 addGestureRecognizer:horizontalLabel3Tap];
    self.horizontalLabel3.userInteractionEnabled = YES;
    
    // horizontalLabel4
    UITapGestureRecognizer *horizontalLabel4Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel4 addGestureRecognizer:horizontalLabel4Tap];
    self.horizontalLabel4.userInteractionEnabled = YES;
    
    // horizontalLabel5
    UITapGestureRecognizer *horizontalLabel5Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel5 addGestureRecognizer:horizontalLabel5Tap];
    self.horizontalLabel5.userInteractionEnabled = YES;
    
    // horizontalLabel6
    UITapGestureRecognizer *horizontalLabel6Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel6 addGestureRecognizer:horizontalLabel6Tap];
    self.horizontalLabel6.userInteractionEnabled = YES;
    
    // horizontalLabel7
    UITapGestureRecognizer *horizontalLabel7Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel7 addGestureRecognizer:horizontalLabel7Tap];
    self.horizontalLabel7.userInteractionEnabled = YES;
    
    // horizontalLabel8
    UITapGestureRecognizer *horizontalLabel8Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    [self.horizontalLabel8 addGestureRecognizer:horizontalLabel8Tap];
    self.horizontalLabel8.userInteractionEnabled = YES;
    
    // IF WE HAVE A STARTING LOCAITON AT THIS STAGE, INIT THE MAP WITH OUR BEST GUESS ZOOM
    // WITHOUT ANIMATION, AS A STARITNG POINT (ZOOM WILL UPDATE TO SMART-LEVEL ZOOM AFTER
    // OUR FIRST REQUEST TO FETCH MARKERS).
    if (self.location)
    {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.location.coordinate, 10000, 10000);
        [self.mapView setRegion:region animated:NO];
    }
    
    // horizontalScrollView
    
    // horizontalLineView
    self.horizontalLineView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
    
    // titleLabel
    self.titleLabel.backgroundColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 0;
    
    // descriptionLabel
    self.descriptionLabel.backgroundColor = [UIColor whiteColor];
   // self.descriptionLabel.numberOfLines = 0;
	[self.descriptionLabel setEditable:NO];
	self.descriptionLabel.scrollEnabled = NO;
	
    // LOAD DATA
    if (self.isCovidScore) {
        [[SWHelper helperAppBackend] appBackendGetCovidContentByKeyword:self.illnessIDs usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"MAIN");
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                
                
                [self parseContentByKeywordWithJsonResponseBody: jsonResponseBody];
                 
                 
            });
        }];
    }
    else {
        [[SWHelper helperAppBackend] appBackendGetContentByKeyword:self.illnessIDs usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"MAIN");
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                
                
                [self parseContentByKeywordWithJsonResponseBody: jsonResponseBody];
                 
                 
            });
        }];
    }
    
}

- (void)parseContentByKeywordWithJsonResponseBody:(NSDictionary *) jsonResponseBody {
    self.networkResponse = jsonResponseBody;
    for (NSDictionary *section in [jsonResponseBody objectForKey:@"sections"])
    {
        NSLog(@"section = %@", section);
        UIFont *font = [SWFont fontStandardFontOfSize:16];
        
        // LABEL 1
        // what-is-disease
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"what-is-disease"])
        {
            // BUTTON TITLE
            self.horizontalLabel1.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherBlueSelected2x146x207]}];
            
            
            // ARTICLE TITLE
            self.titleLabel.attributedText = [self attributedStringForTitle:[section objectForKey:@"section_title"]];
            
            // ARTICLE CONTENT
            self.descriptionLabel.attributedText = [self attributedStringForContent:[section objectForKey:@"content"]];
        
        }
        
        // LABEL 2
        // causes
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"causes"])
        {
            self.horizontalLabel2.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
        
        // LABEL 3
        // symptoms
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"symptoms"])
        {
            self.horizontalLabel3.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
        
        // LABEL 4
        // how-diagnosed
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"how-diagnosed"])
        {
            self.horizontalLabel4.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
        
        // LABEL 5
        // treatment
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"treatment"])
        {
            self.horizontalLabel5.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
        
        // LABEL 6
        // when-to-call-doctor
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"when-to-call-doctor"])
        {
            self.horizontalLabel6.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
        
        // LABEL 7
        // key-points
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"key-points"])
        {
            self.horizontalLabel7.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
        
        // LABEL 8
        // action-alert
        if ([[section objectForKey:@"section_slug"] isEqualToString:@"action-alert"])
        {
            self.horizontalLabel8.attributedText = [[NSAttributedString alloc] initWithString:[section objectForKey:@"section_name"] attributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135]}];
        }
    }
}
/*
{
"status": "success",
"sections": [
{
"section_name": "Overview",
"section_title": "What is a fever?",
"section_slug": "what-is-disease",
"content": "<p>A fever is a body temperature that is higher than normal. It usually means there is an abnormal process occurring in the body. Exercise, hot weather, and common childhood immunizations can also make body temperature rise.</p>"
},
{
"section_name": "Causes",
"section_title": "What causes a fever?",
"section_slug": "causes",
"content": "<p>A fever is not an illness by itself. Rather it is a symptom that something is not right within the body. A fever does not tell you what is causing it, or even that a disease is present. It may be a bacterial or viral infection. Or, it could be a reaction from an allergy to food or medicine. Becoming overheated at play or in the sun can also result in fever.</p>"
},
{
"section_name": "Symptoms",
"section_title": "What are the symptoms of a fever?",
"section_slug": "symptoms",
"content": "<p>Normal body temperature ranges from 97.5\u00b0F to 98.9\u00b0F (36.4\u00b0C to 37.2\u00b0C). It tends to be lower in the morning and higher in the evening. Most healthcare providers consider a fever to be 100.4\u00b0F (38\u00b0C) or higher. High fevers may bring on seizures or confusion in children. It's not how high the temperature is, but how fast the temperature goes up that causes a seizure.</p><p>A fever has other symptoms besides a higher-than-normal temperature. These are especially important when caring for babies, young children, and disabled people. These groups may not be able to express how they feel. Signs that mean fever include:</p><ul><li>Flushed face</li><li>Hot, dry skin</li><li>Low output of urine, or dark urine</li><li>Not interested in eating</li><li>Constipation or diarrhea</li><li>Vomiting</li><li>Headache</li><li>Aching all over</li><li>Nausea</li></ul>"
},
{
"section_name": "Diagnosis",
"section_title": "How is a fever diagnosed?",
"section_slug": "how-diagnosed",
"content": "<p>The best way to diagnose a fever is to take a temperature with a thermometer. There are several types of thermometers, including the following:</p><ul><li>Digital thermometer (oral, rectal, or under the armpit)</li><li>Tympanic (ear) thermometer (not recommended in babies younger than 6 months of age)</li><li>Temporal artery (temperature taken across the forehead area)</li></ul><p>Taking a temperature rectally is the most accurate method in children under 3 years of age. In older children and adults, take the temperature under the armpit or in the mouth. Talk with your healthcare provider about the best way to take your temperature.</p><p>Most thermometers today are digital, but there are some glass thermometers containing mercury still in use. Mercury is toxic substance and is dangerous to humans and the environment. Because glass thermometers can break, they should be disposed of properly in accordance with local, state, and federal laws. For information on how to safely dispose of a mercury thermometer, contact your local health department, waste disposal authority, or fire department.</p>"
},
{
"section_name": "Treatment",
"section_title": "How is a fever treated?",
"section_slug": "treatment",
"content": "<p>You can treat a fever with acetaminophen or ibuprofen in dosages advised by your healthcare provider. Switching between giving acetaminophen and ibuprofen can cause medicine errors and may lead to side effects. Never give aspirin to a child or young adult who has a fever.</p><p>A lukewarm bath may reduce the fever. Alcohol rubdowns are no longer recommended.</p><p>Call your healthcare provider for guidance anytime you are uncomfortable with the conditions of the fever, and remember to contact your healthcare provider any time a temperature spikes quickly or persists despite treatment.</p>"
},
{
"section_name": "When to Call a Healthcare Provider",
"section_title": "When should I call my healthcare provider?",
"section_slug": "when-to-call-doctor",
"content": "<p>Call your healthcare provider right away for a fever in a baby younger than 3 months old.</p><p>Call right away or seek immediate medical attention if any of the following occur with a fever:</p><ul><li>Seizure</li><li>Feeling dull or sleepy</li><li>Irregular breathing</li><li>Stiff neck</li><li>Confusion</li><li>Purple spotted rash</li><li>Ear pain (a child tugging on his or her ear)</li><li>Sore throat that persists</li><li>Vomiting</li><li>Diarrhea</li><li>Painful, burning, or frequent urination</li></ul>"
},
{
"section_name": "Key Points",
"section_title": "Key points about fevers",
"section_slug": "key-points",
"content": "<ul><li>A fever is not an illness by itself, but, rather, a sign that something is not right within the body.</li><li>Illness, exercise, hot weather, and common childhood immunizations can make body temperature rise.</li><li>In addition to an elevated temperature, look for other signs, such as: flushed face, hot skin, low urine output, loss of appetite, headache, or other symptoms of an infection or illness.</li><li>Once you have determined that the person has a fever, you may treat it by giving acetaminophen or ibuprofen in dosages advised by your healthcare provider.</li><li>Call your healthcare provider if a baby under 3 months has a fever, or seek immediate medical attention if a fever is accompanied by a seizure, lethargy, irregular breathing, stiff neck, confusion, or other signs of a serious illness.</li></ul>"
},
{
"section_name": "Next Steps",
"section_title": "Next steps",
"section_slug": "action-alert",
"content": "<p>Tips to help you get the most from a visit to your healthcare provider:</p><ul><li>Know the reason for your visit and what you want to happen.</li><li>Before your visit, write down questions you want answered.</li><li>Bring someone with you to help you ask questions and remember what your provider tells you.</li><li>At the visit, write down the name of a new diagnosis, and any new medicines, treatments, or tests. Also write down any new instructions your provider gives you.</li><li>Know why a new medicine or treatment is prescribed, and how it will help you. Also know what the side effects are.</li><li>Ask if your condition can be treated in other ways.</li><li>Know why a test or procedure is recommended and what the results could mean.</li><li>Know what to expect if you do not take the medicine or have the test or procedure.</li><li>If you have a follow-up appointment, write down the date, time, and purpose for that visit.</li><li>Know how you can contact your provider if you have questions.</li></ul>"
}
]
}
 */
@end
