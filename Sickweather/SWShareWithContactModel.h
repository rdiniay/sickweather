//
//  SWShareWithContactModel.h
//  Sickweather
//
//  Created by John Erck on 2/12/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import "SWShareWithPersonModel.h"

@interface SWShareWithContactModel : SWShareWithPersonModel
@property (readwrite, nonatomic) ABRecordRef addressBookRecordRef;
@property (strong, nonatomic) NSString *addressBookRecordId;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) UIImage *image;
@end
