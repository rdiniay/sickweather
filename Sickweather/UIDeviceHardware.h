//
//  UIDeviceHardware.h
//
//  Used to determine EXACT version of device software is running on.

// http://stackoverflow.com/a/3950748/394969

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject 

- (NSString *) platform;
- (NSString *) platformString;

@end