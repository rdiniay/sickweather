//
//  SWTrackerTypeModal.h
//  Sickweather
//
//  Created by Shan Shafiq on 2/12/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWTrackerTypeModal : NSObject
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *name;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
