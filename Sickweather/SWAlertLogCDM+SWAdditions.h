//
//  SWAlertLogCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 4/11/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "SWAlertLogCDM.h"

@interface SWAlertLogCDM (SWAdditions)

+ (SWAlertLogCDM *)getAlertLogObjectForReportId:(NSString *)reportId;
+ (SWAlertLogCDM *)createAlertLogObjectThatWeHaveStartedRegionMonitoringForForReport:(NSDictionary *)report region:(CLRegion *)region;
+ (NSDictionary *)reportForRegionId:(NSString *)regionId;

+ (NSMutableArray *)regionsAlreadyFetchedFromWebServiceThatWeHaveStartedMonitoringFor;
+ (NSMutableArray *)reportsAlreadyFetchedFromWebServiceThatWeHaveStartedMonitoringFor;

- (CLRegion *)regionObject;
- (NSDictionary *)reportDictionary;

@end
