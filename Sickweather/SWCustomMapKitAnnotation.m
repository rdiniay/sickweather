//
//  SWMapKitAnnotation.m
//  Sickweather
//
//  Created by John Erck on 10/4/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWCustomMapKitAnnotation.h"

@implementation SWCustomMapKitAnnotation

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    _coordinate = coordinate;
}

- (void)setIdentifier:(NSString *)identifier
{
    _identifier = identifier;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    _subtitle = subtitle;
}

- (void)setTypeId:(NSString *)typeId
{
    _typeId = typeId;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate identifier:(NSString *)identifier title:(NSString *)title subtitle:(NSString *)subtitle typeId:(NSString *)typeId
{
    self = [super init];
    if (self) {
        self.coordinate = coordinate;
        self.identifier = identifier;
        self.title = title;
        self.subtitle = subtitle;
        self.typeId = typeId;
    }
    return self;
}

@end
