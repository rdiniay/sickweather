//
//  SWFamilyProfileViewController.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWFamilyMemberModal.h"

@interface SWFamilyProfileViewController : UIViewController
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
@end
