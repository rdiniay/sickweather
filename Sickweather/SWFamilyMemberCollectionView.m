//
//  SWFamilyMemberCollectionView.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 01/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyMemberCollectionView.h"
#import "SWFamilyMemberCollectionViewCell.h"

#define ITEM_WIDTH 140
#define ITEM_SPACING 0

@interface SWFamilyMemberCollectionView()  <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property NSMutableArray* familyMemberArray;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation SWFamilyMemberCollectionView

-(void) initializeCollectionViewWith:(NSMutableArray*)familyMembers
{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SWFamilyMemberCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"familyMemberCell"];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    if (familyMembers != nil) {
        self.familyMemberArray = [[NSMutableArray alloc] initWithArray:familyMembers];
    } else {
        self.familyMemberArray = [[NSMutableArray alloc] init];
    }
    
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    SWFamilyMemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"familyMemberCell" forIndexPath:indexPath];
    if (indexPath.row == 0) { // Current user should be first
        [cell initWithData:nil isCurrentUser:YES];
    } else {
        [cell initWithData:self.familyMemberArray[indexPath.row-1] isCurrentUser:NO];
    }
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.familyMemberArray.count + 1; // one additional user is currentuser
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0){
        [self.delegate youItemSelected];
    } else {
        SWFamilyMemberModal* member = self.familyMemberArray[indexPath.row - 1];
        [self.delegate familyMemberItemSelected:member];
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat totalCellWidth = ITEM_WIDTH * (self.familyMemberArray.count + 1);
    CGFloat totalSpacingWidth = ITEM_SPACING * (((float)self.familyMemberArray.count + 1 - 1) < 0 ? 0 : self.familyMemberArray.count + 1 - 1);
    CGFloat leftInset = (self.bounds.size.width - (totalCellWidth + totalSpacingWidth)) / 2;
    leftInset = leftInset < 0 ? 0 : leftInset;
    CGFloat rightInset = leftInset;
    UIEdgeInsets sectionInset = UIEdgeInsetsMake(0, leftInset, 0, rightInset);
    return sectionInset;
}

@end
