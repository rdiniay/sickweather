//
//  BTUtility.m
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/13/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#import "BTUtility.h"

@implementation BTUtility


+ (NSString *)charToASCII:(char)charValue {
    NSString *stringData = [NSString stringWithFormat:@"%c", charValue];
    
    return stringData;
}

+ (NSString *)hexToASCII:(NSData *)hexValue {
    NSString *stringData = [[NSString alloc]initWithData:hexValue encoding:NSASCIIStringEncoding];
    
    return stringData;
}

+ (NSString *)hexToString:(NSData *)hexValue {
    NSString *stringData = [[NSString alloc]initWithData:hexValue encoding:NSUTF8StringEncoding];
    
    return stringData;
}

+ (int)hexToDecimal:(NSData *)hexValue {
    NSString *stringData = [hexValue description];
    stringData = [stringData substringWithRange:NSMakeRange(1, [stringData length]-2)];
    
    NSScanner *scanner = [NSScanner scannerWithString:stringData];
    unsigned int decValue = 0;
    [scanner scanHexInt:&decValue];
    
    return decValue;
}

+ (NSMutableData *)intToData:(int)value {
    NSMutableData *dataValue = [NSMutableData data];
    
    NSScanner *scanner = [NSScanner scannerWithString:[NSString stringWithFormat:@"%i", value]];
    int intValue;
    [scanner scanInt:&intValue];
    [dataValue appendBytes:&intValue length:1];
    
    return dataValue;
}

+ (void)writeCharacteristic:(CBPeripheral *)peripheral service:(CBUUID *)currentService characteristic:(CBUUID *)currentCharacteristic data:(NSData *)data {
    for (CBService *service in peripheral.services) {
        if ([service.UUID isEqual:currentService]) {
            for (CBCharacteristic *characteristic in service.characteristics) {
                if ([characteristic.UUID isEqual:currentCharacteristic]) {
                    [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                }
            }
        }
    }
}


@end
