//
//  SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 12/16/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM.h"
#import "SWSponsoredMarkerMapKitAnnotation.h"

@interface SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM (SWAdditions)
+ (SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM *)sponsoredMapMarkerDidComeOnScreenLogEntryInsertNewUsingSponsoredMarkerAnnotation:(SWSponsoredMarkerMapKitAnnotation *)sponsoredMarkerAnnotation;
@end
