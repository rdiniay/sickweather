//
//  SWAddMessageToReportViewController.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWAddMessageToReportViewControllerDelegate <NSObject>
- (void)addMessageToReportViewControllerWantsToDismissAfterTaggingToGroupOrAddingMessage;
- (void)addMessageToReportViewControllerWantsToDismissWithoutAddingMessageOrTaggingToAnyGroups;
@end

@interface SWAddMessageToReportViewController : UIViewController
@property (nonatomic, assign) id <SWAddMessageToReportViewControllerDelegate> delegate;
@property (strong, nonatomic) NSArray *reports;
@property (strong, nonatomic) NSDictionary *latestMenuServerResponse;
@property (strong, nonatomic) NSDictionary *serverGroupDict;
@end
