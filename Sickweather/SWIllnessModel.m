//
//  SWIllnessModel.m
//  Sickweather
//
//  Created by John Erck on 10/14/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWIllnessModel.h"
#import "SWSettings.h"
#import "SWIllnessCDM+SWAdditions.h"

@interface SWIllnessModel () <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

// http://stackoverflow.com/questions/3935574/can-i-use-objective-c-blocks-as-properties
@property (readwrite, copy) SWIllnessesRequestCompletionHandler completionHandler;
@property (strong, nonatomic) NSURLSession *illnessRequestSession;
@property (strong, nonatomic) NSMutableData *illnessRequestResponseData;

@end

@implementation SWIllnessModel

#pragma mark - Custom Getters/Setters

- (NSMutableData *)illnessRequestResponseData
{
    if (!_illnessRequestResponseData) {
        _illnessRequestResponseData = [[NSMutableData alloc] init];
    }
    return _illnessRequestResponseData;
}

#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    
    // After invalidating the session, when all outstanding tasks have been canceled or have finished, the session sends the delegate a URLSession:didBecomeInvalidWithError: message. When that delegate method returns, the session disposes of its strong reference to the delegate.

    // Important: The session object keeps a strong reference to the delegate until your app explicitly invalidates the session. If you do not invalidate the session, your app leaks memory.
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    // Ok great, we can now make use of prop that hold's response data...
    
    // Convert to JSON
    NSError *jsonError = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:self.illnessRequestResponseData options:kNilOptions error:&jsonError];
    
    // Future TODO: Take each NSDict and convert to SWReportModel
    // before returning the collection. This is the proper spot
    // to go from "string land" back to "typed land".
    
    // Execute user provided callback on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        self.completionHandler(jsonArray, jsonError);
    });
    
    // Invalidate the session, we're done!
//    [session invalidateAndCancel];
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    // String it all together...
    [self.illnessRequestResponseData appendData:data];
}

#pragma mark - Public Methods

+ (NSString *)nameForId:(NSString *)identifier
{
    NSString *name;
    for (SWIllnessCDM *illness in [SWIllnessCDM illnessGetAll])
    {
        if ([illness.unique isEqualToString:identifier])
        {
            name = illness.name;
            break;
        }
    }
    return name;
}

- (id)initUsingDict:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        self.id = dict[@"id"];
        self.name = dict[@"name"];
        self.illnessDescription = dict[@"description"];
    }
    return self;
}

@end
