//
//  SWSettingsTableViewCell.m
//  Sickweather
//
//  Created by John Erck on 10/9/13.
//  Copyright (c) 2013 Sickweather, LLC. All rights reserved.
//

#import "SWSettingsTableViewCell.h"

@implementation SWSettingsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
