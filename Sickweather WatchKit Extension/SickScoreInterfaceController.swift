//
//  SickScoreInterfaceController.swift
//  Sickweather WatchKit Extension
//
//  Created by John Erck on 2/18/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class SickScoreInterfaceController: WKInterfaceController
{
    var sickScore = 0 // 0-100
    var locationName = "Scanning..."
    var topIllnessIds = [String]()
    var topIllnessNames = [String]()
    var topIllnessIconLetters = [String]()
    let π = Double(Double.pi)
    let degree360InRadians = Double(Double.pi)*2
    let startingBlueFillColor = UIColor(red: 36/255.0, green: 169/255.0, blue: 225/255.0, alpha: 1)
    let fullRowCount = 5
    
    @IBOutlet weak var table: WKInterfaceTable!
    
    // MARK: Helper Methods
    
    func updateUIForCurrentSickScoreValue() {
        // Configure header row
        if let headerRowController = table.rowController(at: 0) as? SickScoreInterfaceTableRowControllerForTypeHeader {
            
            // Set self as delegate
            headerRowController.presentingController = self
            
            // Get color risk
            let currentColorRisk = SWHelper.helperSickScoreGetColorRiskGlanceBackgroundImageNameForScore(sickScore)
            
            // We needed zero padded single digit file names to get things working correctly...
            let meterCount = sickScore/5
            var meterCountString = ""
            if meterCount == 0 {
                meterCountString = "zero"
            } else if meterCount > 0 && meterCount < 10 {
                meterCountString = "\(meterCount)" // We used to zero pad, but not needed anymore...
            } else {
                meterCountString = "\(meterCount)"
            }
            let imageName = "sickscore-meter-\(meterCountString)"
            
            // Configure header
            headerRowController.washHandsButton.setBackgroundColor(currentColorRisk.color)
            headerRowController.sickScoreBackgroundGroup.setBackgroundColor(currentColorRisk.color)
            headerRowController.sickScoreMeterInterfaceImage.setImageNamed(imageName)
            headerRowController.sickScoreMeterInterfaceGroup.setBackgroundColor(currentColorRisk.color)
            headerRowController.riskLevelLabel.setText(currentColorRisk.risk)
            headerRowController.riskLevelLabel.setTextColor(currentColorRisk.color)
            headerRowController.locationNameLabel.setText(locationName)
            if sickScore == 0 {
                headerRowController.sickScoreIntegerValueLabel.setText("--")
            } else {
                headerRowController.sickScoreIntegerValueLabel.setText("\(sickScore)")
            }
            
            // Setup rows once we know we have sickscore data
            if table.numberOfRows < fullRowCount && locationName != "No Location" && locationName != "Scanning..." && sickScore > 0 {
                table.insertRows(at: IndexSet(integer: 1), withRowType: "topIllnessRowType")
                table.insertRows(at: IndexSet(integer: 2), withRowType: "topIllnessRowType")
                table.insertRows(at: IndexSet(integer: 3), withRowType: "topIllnessRowType")
                table.insertRows(at: IndexSet(integer: 4), withRowType: "spacingRowType")
                headerRowController.noLocationGroup.setHidden(true)
                headerRowController.locationRiskGroup.setHidden(false)
                headerRowController.riskLevelLabel.setAlpha(1.0)
                headerRowController.topIllnessesGroup.setHidden(false)
            }
            
            // Handle no location
            if (locationName == "No Location") {
                // Issue dismissable system location prompt one time on watch
                let myLocationManager = CLLocationManager();
                myLocationManager.requestAlwaysAuthorization();
                headerRowController.locationRiskGroup.setHidden(true)
                headerRowController.noLocationGroup.setHidden(false)
            }
            
            // Check for each row and if found, set 'em up
            if table.numberOfRows > 1 {
                if let topIllnessRow1 = table.rowController(at: 1) as? SickScoreInterfaceTableRowControllerForTypeTopIllness {
                    topIllnessRow1.containerGroup.setBackgroundColor(SWColor.clearColor())
                    if topIllnessIconLetters.count > 0 {
                        topIllnessRow1.shortNameInterfaceLabel.setText(topIllnessIconLetters[0])
                        topIllnessRow1.longNameInterfaceLabel.setText(topIllnessNames[0])
                    }
                }
            }
            
            if table.numberOfRows > 2 {
                if let topIllnessRow2 = table.rowController(at: 2) as? SickScoreInterfaceTableRowControllerForTypeTopIllness {
                    topIllnessRow2.containerGroup.setBackgroundColor(SWColor.clearColor())
                    if topIllnessIconLetters.count > 1 {
                        topIllnessRow2.shortNameInterfaceLabel.setText(topIllnessIconLetters[1])
                        topIllnessRow2.longNameInterfaceLabel.setText(topIllnessNames[1])
                    }
                }
            }
            
            if table.numberOfRows > 3 {
                if let topIllnessRow3 = table.rowController(at: 3) as? SickScoreInterfaceTableRowControllerForTypeTopIllness {
                    topIllnessRow3.containerGroup.setBackgroundColor(SWColor.clearColor())
                    if topIllnessIconLetters.count > 2 {
                        topIllnessRow3.shortNameInterfaceLabel.setText(topIllnessIconLetters[2])
                        topIllnessRow3.longNameInterfaceLabel.setText(topIllnessNames[2])
                    }
                    topIllnessRow3.bottomOfRowInterfaceSeparator.setHidden(true)
                }
            }
        }
    }
    
    func getDataFromPhone()
    {
        // Get watch app's delegate object
        let appDelegate: SWWatchKitAppExtensionDelegate = WKExtension.shared().delegate as! SWWatchKitAppExtensionDelegate;
        
        // Clear all outstanding hand wash complete timer notifications (if any)
        appDelegate.session?.sendMessage(["handleWatchKitExtensionRequestMethod": "clearAllHandWashTimerCompleteNotifications"], replyHandler: { (response) -> Void in
            print(response)
            }, errorHandler: { (error) -> Void in
                print(error)
        })
        
        // Get updated sick score
        self.updateUIForCurrentSickScoreValue();
        appDelegate.session?.sendMessage(["handleWatchKitExtensionRequestMethod": "getSickScoreForCurrentLocation"], replyHandler: { (response) -> Void in
            // Handle response from iphone app
            /*
             "sickscore": "88",
             "location_name": "Huntington Beach",
             "top_illness_ids": ["1","2","3"],
             "top_illness_names": ["Flu","Cold","Fever"],
             "top_illness_icon_letters": ["Fl","Co","Fe"]
             */
            print("Background thread...");
            DispatchQueue.main.async
                {
                    print("Main thread...");
                    if (response.count > 0)
                    {
                        if let sickScore = response["sickscore"] as? String {
                            self.sickScore = Int(sickScore)!
                        }
                        if let locationName = response["location_name"] as? String {
                            self.locationName = locationName
                        }
                        if let topIllnessIds = response["top_illness_ids"] as? NSArray {
                            self.topIllnessIds = [String]()
                            for illnessId in topIllnessIds as! [String] {
                                self.topIllnessIds.append(illnessId)
                            }
                        }
                        if let topIllnessNames = response["top_illness_names"] as? NSArray {
                            self.topIllnessNames = [String]()
                            for illnessName in topIllnessNames as! [String] {
                                self.topIllnessNames.append(illnessName)
                            }
                        }
                        if let topIllnessIconLetters = response["top_illness_icon_letters"] as? NSArray {
                            self.topIllnessIconLetters = [String]()
                            for illnessIconLetters in topIllnessIconLetters as! [String] {
                                self.topIllnessIconLetters.append(illnessIconLetters)
                            }
                        }
                    }
                    self.updateUIForCurrentSickScoreValue()
            }
            }, errorHandler: { (error) -> Void in
                print(error)
        })
    }
    
    // MARK: Table Delegate Methods
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let selectedRowController: AnyObject = table.rowController(at: rowIndex)! as AnyObject
        if selectedRowController is SickScoreInterfaceTableRowControllerForTypeHandWash {
            // Present hand wash controller
            presentController(withName: "washHandsInterfaceController", context: nil)
        }
        if selectedRowController is SickScoreInterfaceTableRowControllerForTypeTopIllness {
            // TBD...
        }
    }
    
    // MARK: Life Cycle Methods
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        table.insertRows(at: IndexSet(integer: 0), withRowType: "headerRowType")
        table.insertRows(at: IndexSet(integer: 1), withRowType: "washHandsRowType")
        
        if let headerRowController = table.rowController(at: 0) as? SickScoreInterfaceTableRowControllerForTypeHeader {
            headerRowController.sickScoreTableViewHeaderRowGroupContainer.setBackgroundColor(SWColor.clearColor())
            headerRowController.sickScoreBackgroundGroup.setBackgroundImageNamed("sickscore-background")
            headerRowController.noLocationGroup.setHidden(true)
            headerRowController.locationRiskGroup.setHidden(false)
            headerRowController.riskLevelLabel.setAlpha(0.0)
            headerRowController.topIllnessesGroup.setHidden(true)
        }
    }
    
    override func willActivate()
    {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate();
        
        // Log this event to flurry
      // FlurryWatch.logWatchEvent("SickScoreInterfaceContrller WillActivate Called");
		
        // Setup handoff
        updateUserActivity("com.sickweather.Sickweather.watchkitextension.sickScoreInterfaceControllerWillActivate", userInfo: ["myDictKey":"myDictVal"], webpageURL: nil);
        
        // Get data from phone
        self.getDataFromPhone();
        var deadlineTime = DispatchTime.now() + .seconds(2);
        DispatchQueue.main.asyncAfter(deadline: deadlineTime)
        {
            self.getDataFromPhone();
        }
        deadlineTime = DispatchTime.now() + .seconds(5);
        DispatchQueue.main.asyncAfter(deadline: deadlineTime)
        {
            self.getDataFromPhone();
        }
    }
    
    override func didDeactivate()
    {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate();
    }
}
