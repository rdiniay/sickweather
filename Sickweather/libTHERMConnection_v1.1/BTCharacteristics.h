//
//  BTCharacteristics.h
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/13/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#ifndef BTCharacteristics_h
#define BTCharacteristics_h


// Device Information Service 0x180A
#define SystemID_charID                         @"2A23"
#define ModelNumberString_charID                @"2A24"
#define SerialNumberString_charID               @"2A25"
#define FirmwareRevisionString_charID           @"2A26"
#define HardwareRevisionString_charID           @"2A27"
#define SoftwareRevisionString_charID           @"2A28"
#define ManufacturerNameString_charID           @"2A29"
#define RegulatoryCertificationDataList_charID  @"2A2A"
#define PnPID_charID                            @"2A50"
#define DigitalInput_charID                     @"2A45"

// Battery Service 0x180F
#define BatteryLevel_charID                     @"2A19"

// Health Thermometer Service 0x1809
#define TemperatureMeasurement_charID           @"2A1C"
#define TemperatureType_charID                  @"2A1D"
#define TemperatureFahrenheit_charID            @"2A20"
#define AlertStatus_charID                      @"2A3F"
#define NewAlert_charID                         @"2A46"

// Current Time Service 0x1805
#define DateTime_charID                         @"2A08"
#define ReferenceTimeInformation_charID         @"2A14"
#define CurrentTime_charID                      @"2A2B"

// Custom Service (Termperature) 0x2893B28B-C868-423A-9DC2-E9C2FCB4EBB5
#define RequestOpcode_charID                    @"28930000-C868-423A-9DC2-E9C2FCB4EBB5"
#define RequestResponse_charID                  @"28930001-C868-423A-9DC2-E9C2FCB4EBB5"
#define DeviceEvent_charID                      @"28930002-C868-423A-9DC2-E9C2FCB4EBB5"
#define PairStatus_charID                       @"28930003-C868-423A-9DC2-E9C2FCB4EBB5"


#endif /* BTCharacteristics_h */
