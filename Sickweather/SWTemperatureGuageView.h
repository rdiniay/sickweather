//
//  SWTemperatureGuageView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

struct ArcFragment
{
    CGFloat startAngle;
    CGFloat endAngle;
};

@interface SWTemperatureGuageView : UIView
@property (strong, nonatomic) NSMutableArray *arcFragments;
@property float temperature;
@property BOOL isCelsiusTemperatureScaleType;
@end
