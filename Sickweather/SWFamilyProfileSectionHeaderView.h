//
//  SWFamilyProfileSectionHeaderView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWFamilyProfileSectionHeaderView;

@protocol SWFamilyProfileSectionHeaderViewDelegate
- (NSString *)familyProfileSectionHeaderViewTitleString:(SWFamilyProfileSectionHeaderView *)sender;
@end

@interface SWFamilyProfileSectionHeaderView : UIView
@property (nonatomic, weak) id<SWFamilyProfileSectionHeaderViewDelegate> delegate;

@end
