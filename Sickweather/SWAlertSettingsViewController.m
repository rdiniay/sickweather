//
//  SWAlertSettingsViewController.m
//  Sickweather
//
//  Created by John Erck on 12/19/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>
#import "SWAlertSettingsViewController.h"
#import "SWAlertSettingsSwitchTableViewCell.h"

@interface SWAlertSettingsViewController () <UITableViewDelegate, UITableViewDataSource, SWAlertSettingsSwitchTableViewCellDelegate>

// NETWORK DATA
@property (strong, nonatomic) NSArray *networkIllnesses;
@property (strong, nonatomic) NSArray *individualIllnesses; // derived from network data

// UI VIEWS
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UITableView *tableView;

@end

@implementation SWAlertSettingsViewController

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *myLabel = [[UILabel alloc] init];
    //myLabel.backgroundColor = [UIColor redColor];
    NSDictionary *attributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:14]};
    if (section == 0)
    {
        myLabel.attributedText = [[NSAttributedString alloc] initWithString:@"SickZone Alerts" attributes:attributes];
    }
    else
    {
        myLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Alert Me About Nearby" attributes:attributes];
    }
    myLabel.translatesAutoresizingMaskIntoConstraints = NO;
    UITableViewCell *myHeaderView = [[UITableViewCell alloc] init];
    //myHeaderView.contentView.backgroundColor = [UIColor greenColor];
    //myHeaderView.backgroundColor = [UIColor yellowColor];
    [myHeaderView.contentView addSubview:myLabel];
    NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
    [viewsDictionary setObject:myLabel forKey:@"myLabel"];
    [myHeaderView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(20)-[myLabel]" options:0 metrics:0 views:viewsDictionary]];
    //[myHeaderView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[myLabel]-(0)-|" options:0 metrics:0 views:viewsDictionary]];
    [myHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:myLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:myHeaderView.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    return myHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // remove bottom extra 20px space for UITableViewStyleGrouped table type
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // remove bottom extra 20px space for UITableViewStyleGrouped table type
    return CGFLOAT_MIN;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1; // MASTER
    }
    return [self.individualIllnesses count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellToReturn = nil;
    SWAlertSettingsSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWAlertSettingsSwitchTableViewCell"];
    if (cell == nil)
    {
        cell = [[SWAlertSettingsSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SWAlertSettingsSwitchTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (indexPath.section == 0)
    {
        // MASTER SWITCH
        if ([[defaults stringForKey:@"proximity_alerts_are_on_yes_no"] isEqualToString:@"yes"])
        {
            [cell updateForDict:@{@"name":@"Alerts Are On"}];
            [cell styleAsOn];
            [cell styleAsActive];
        }
        else
        {
            [cell updateForDict:@{@"name":@"Alerts Are Off"}];
            [cell styleAsOff];
            [cell styleAsActive];
        }
        cellToReturn = cell;
    }
    else
    {
        // INDIVIDUAL ILLNESS
        NSDictionary *illness = [self.individualIllnesses objectAtIndex:indexPath.row];
        [cell updateForDict:illness];
        NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_for_id_%@_yes_no", [illness objectForKey:@"id"]];
        if ([[defaults stringForKey:defaultsKey] isEqualToString:@"yes"])
        {
            [cell styleAsOn];
        }
        else
        {
            [cell styleAsOff];
        }
        defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_yes_no"];
        if ([[defaults stringForKey:defaultsKey] isEqualToString:@"yes"])
        {
            [cell styleAsActive];
        }
        else
        {
            [cell styleAsInactive];
        }
        cellToReturn = cell;
    }
    cellToReturn.selectionStyle = UITableViewCellSelectionStyleNone;
    return cellToReturn;
}

#pragma mark - SWAlertSettingsSwitchTableViewCellDelegate

/*
 WHAT'S THE DATA MODEL LOOK LIKE?
 IT'S ALL NS USER DEFAULTS, IT'S ALL NEW FOR
 THIE VIEW CONTROLLER/V4.0 RELEASE. EVERYTHING
 IS PREFIXED WITH PROXIMITY.
 */

- (void)mySwitchFlippedOn:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
				[self logEventwithTag:@"Alerts > Settings button tapped"];
                // MASTER SWITCH
                NSString *answer = @"yes";
                NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_yes_no"];
                NSLog(@"defaultsKey = %@ => %@", defaultsKey, answer);
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:answer forKey:defaultsKey];
                [defaults synchronize];
                [self.tableView reloadData];
                
                // VALIDATE SWITCH
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self validateSwitch];
                });
            }
        }
        else if (indexPath.section == 1)
        {
            // INDIVIDUAL ILLNESSES
            NSDictionary *illness = [self.individualIllnesses objectAtIndex:indexPath.row];
            NSString *answer = @"yes";
			[self logEventwithTag:[NSString stringWithFormat:@"%@ switch tapped",[illness objectForKey:@"name"]]];
            NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_for_id_%@_yes_no", [illness objectForKey:@"id"]];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:answer forKey:defaultsKey];
            [defaults synchronize];
        }
    }
}

- (void)mySwitchFlippedOff:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
				[self logEventwithTag:@"Alerts > Settings button tapped"];
                // MASTER SWITCH
                NSString *answer = @"no";
                NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_yes_no"];
                NSLog(@"defaultsKey = %@ => %@", defaultsKey, answer);
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:answer forKey:defaultsKey];
                [defaults synchronize];
                [self.tableView reloadData];
            }
        }
        else if (indexPath.section == 1)
			{
            // INDIVIDUAL ILLNESSES
            NSDictionary *illness = [self.individualIllnesses objectAtIndex:indexPath.row];
            NSString *answer = @"no";
			[self logEventwithTag:[NSString stringWithFormat:@"%@ switch tapped",[illness objectForKey:@"name"]]];
            NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_for_id_%@_yes_no", [illness objectForKey:@"id"]];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:answer forKey:defaultsKey];
            [defaults synchronize];
        }
    }
}

#pragma mark - HELPERS

-(void)logEventwithTag:(NSString*)tag{
	[SWHelper logFlurryEventsWithTag:tag];
}

- (void)appDidBecomeActiveAgain:(id)sender
{
    // VALIDATE SWITCH
    [self validateSwitch];
}

- (void)validateSwitch
{
    // CHECK/WARN ABOUT CURRENT DEVICE SETTINGS (IF NOTIFICATIONS ARE OFF)
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus != UNAuthorizationStatusAuthorized)
        {
            // LINK THEM TO SETTINGS
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notifications Are Off" message:@"Tap \"Go to Settings\" below to turn notifications on." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                // MAKE SURE MASTER SWITCH IS OFF...
                NSString *answer = @"no";
                NSString *defaultsKey = [NSString stringWithFormat:@"proximity_alerts_are_on_yes_no"];
                NSLog(@"defaultsKey = %@ => %@", defaultsKey, answer);
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:answer forKey:defaultsKey];
                [defaults synchronize];
                [self.tableView reloadData];
            }];
            [alert addAction:cancel];
            UIAlertAction *goToSettings = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            [alert addAction:goToSettings];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}

-(void)dismissSelf:(id)sender
{
    [self.delegate dismissAlertSettings:self];
}

#pragma mark - LIFE CYCLE METHODS

- (void)dealloc
{
    // REMOVE OBSERVERS
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    // INVOKE SUPER
    [super viewDidAppear:animated];
    
    // VALIDATE SWITCH
    [self validateSwitch];
}

- (void)viewDidLoad
{
    // INIT SUPER
    [super viewDidLoad];
    
    // MAKE SURE WE ARE TOLD ABOUT WHEN WE COME BACK ON SCREEN (USER PLAYING BACK AND FORTH WITH SETTINGS APP)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActiveAgain:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // FETCH NETWORK DATA TO PROP
    self.networkIllnesses = [SWHelper helperLoadJSONObjectFromDocsUsingName:@"my-network-illnesses"];
    self.individualIllnesses = @[];
    for (NSDictionary *illness in self.networkIllnesses)
    {
        if (![[illness objectForKey:@"id"] containsString:@","])
        {
            // IS NOT GROUP, IS INDIVIDUAL, ADD
            self.individualIllnesses = [self.individualIllnesses arrayByAddingObject:illness];
        }
    }
	
	//SORT ALPHABETICALLY
	if (self.individualIllnesses){
		NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name"
																	 ascending:YES];
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
		self.individualIllnesses = [self.individualIllnesses sortedArrayUsingDescriptors:sortDescriptors];
	}
	
    // INIT ALL REQUIRED UI ELEMENTS
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped]; // make so headers aren't sticky
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.tableView forKey:@"tableView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.tableView];
    
    // LAYOUT
    
    // tableView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[tableView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[tableView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // CONFIG
    
    // Back Button
    if(self.delegate != nil){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissSelf:)];
    }
    // navBar
    self.navigationController.navigationBar.translucent = NO;
    
    // self
    self.title = @"Alert Settings";

    // view
    self.view.backgroundColor = [UIColor whiteColor];
    
    // tableView
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[SWAlertSettingsSwitchTableViewCell class] forCellReuseIdentifier:@"SWAlertSettingsSwitchTableViewCell"];
}

@end
