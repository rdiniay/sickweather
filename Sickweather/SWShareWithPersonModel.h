//
//  SWShareWithPersonModel.h
//  Sickweather
//
//  Created by John Erck on 2/12/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWShareWithPersonModel : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@end
