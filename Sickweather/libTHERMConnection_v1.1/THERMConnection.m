//
//  THERMConnection.m
//  THERMConnection
//
//  Created by GURPAL BHOOT on 3/13/17.
//  Copyright © 2017 Swaive Corporation. All rights reserved.
//

#import "THERMConnection .h"
#import "BTCharacteristics.h"
#import "BTServices.h"
#import "BTUtility.h"

@interface THERMConnection()

- (void)readTemperature:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic;
- (NSString *)readTime:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic;
- (NSDate *)getTime:(NSString *)string;
- (NSDate *)getCurrentTime;

@property (readonly, nonatomic) CBPeripheral *activePeripheral;
@property (readonly, nonatomic) NSArray *allServices;

@end

@implementation THERMConnection

static THERMConnection *connector;
@synthesize bluetoothManager, peripherals, activePeripheral;

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    switch ([central state]) {
        case CBManagerStatePoweredOn:
            NSLog(@"CoreBluetooth BLE hardware is powered on");
            [bluetoothManager scanForPeripheralsWithServices:nil options:nil];
            
            break;
            
        case CBManagerStatePoweredOff:
            NSLog(@"CoreBluetooth BLE hardware is powered off");
            
            break;
            
        case CBManagerStateUnauthorized:
            NSLog(@"CoreBluetooth BLE state is unauthorized");
            
            break;
            
        case CBManagerStateUnknown:
            NSLog(@"CoreBluetooth BLE state is unknown");
            
            break;
            
        case CBManagerStateUnsupported:
            NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
            
            break;
            
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    if ([peripheral.name isEqualToString:@"SWT1A"] || [peripheral.name isEqualToString:@"Philips ear thermometer"] || [peripheral.name containsString:@"SCH740"] || [peripheral.name containsString:@"KS_"]) {
        if ([self.delegate respondsToSelector:@selector(deviceDiscovered:)]) {
            [self.delegate deviceDiscovered:peripheral];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    if ([self.delegate respondsToSelector:@selector(deviceConnection:)]) {
        [self.delegate deviceConnection:peripheral];
    }

    [bluetoothManager stopScan];
    peripheral.delegate = self;
    [peripherals addObject:peripheral];
    activePeripheral = peripheral;
    [peripheral discoverServices:_allServices];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Failed to connect to %@", peripheral.name);
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(deviceDisconnection:)]) {
        [self.delegate deviceDisconnection:peripheral];
    }
    
    [peripherals removeObject:peripheral];
    activePeripheral = nil;
    [bluetoothManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    for (CBService *aService in peripheral.services) {
        [peripheral discoverCharacteristics:nil forService:aService];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    for (CBCharacteristic *characteristic in service.characteristics) {
        [peripheral readValueForCharacteristic:characteristic];
        
        if (/*[characteristic.UUID isEqual:[CBUUID UUIDWithString:BatteryLevel_charID]] || */[characteristic.UUID isEqual:[CBUUID UUIDWithString:DeviceEvent_charID]] || [characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureMeasurement_charID]] || [characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureType_charID]] || [characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureFahrenheit_charID]]/* || [characteristic.UUID isEqual:[CBUUID UUIDWithString:DateTime_charID]] || [characteristic.UUID isEqual:[CBUUID UUIDWithString:CurrentTime_charID]]*/) {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if /*([characteristic.UUID isEqual:[CBUUID UUIDWithString:SystemID_charID]]) {
        if ([self.delegate respondsToSelector:@selector(getSystemID:)]) {
            [self.delegate getSystemID:characteristic.value];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:ModelNumberString_charID]]) {
        NSString *stringData = [BTUtility hexToASCII:characteristic.value];
        
        if ([self.delegate respondsToSelector:@selector(getModelNumber:)]) {
            [self.delegate getModelNumber:stringData];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:SerialNumberString_charID]]) {
        NSString *stringData = [BTUtility hexToASCII:characteristic.value];
        
        if ([self.delegate respondsToSelector:@selector(getSerialNumber:)]) {
            [self.delegate getSerialNumber:stringData];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:FirmwareRevisionString_charID]]) {
        NSString *stringData = [BTUtility hexToASCII:characteristic.value];
        
        if ([self.delegate respondsToSelector:@selector(getFirmwareVersion:)]) {
            [self.delegate getFirmwareVersion:stringData];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HardwareRevisionString_charID]]) {
        NSString *stringData = [BTUtility hexToASCII:characteristic.value];
        if ([self.delegate respondsToSelector:@selector(getHardwareVersion:)]) {
            [self.delegate getHardwareVersion:stringData];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:SoftwareRevisionString_charID]]) {
        NSString *stringData = [BTUtility hexToASCII:characteristic.value];
        
        if ([self.delegate respondsToSelector:@selector(getSoftwareVersion:)]) {
            [self.delegate getSoftwareVersion:stringData];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:ManufacturerNameString_charID]]) {
        NSString *stringData = [BTUtility hexToASCII:characteristic.value];
        
        if ([self.delegate respondsToSelector:@selector(getManufacturerName:)]) {
            [self.delegate getManufacturerName:stringData];
        }
    } else if*/ ([characteristic.UUID isEqual:[CBUUID UUIDWithString:BatteryLevel_charID]]) {
        int decValue = [BTUtility hexToDecimal:characteristic.value];
        
        if (decValue != 0) {
            if ([self.delegate respondsToSelector:@selector(getBatteryLevel:)]) {
                [self.delegate getBatteryLevel:decValue];
            }
        }
    }/* else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:AlertStatus_charID]]) {
        int decValue = [BTUtility hexToDecimal:characteristic.value];
        
        if ([self.delegate respondsToSelector:@selector(getAlertStatus:)]) {
            [self.delegate getAlertStatus:decValue];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DigitalInput_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            int decValue = [BTUtility hexToDecimal:characteristic.value];
            
            if ([self.delegate respondsToSelector:@selector(getLEDBrightness:)]) {
                [self.delegate getLEDBrightness:decValue];
            }
        }
    }*/ else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureMeasurement_charID]]) {
        [self readTemperature:peripheral characteristic:characteristic];
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureType_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            NSString *stringData;
            
            NSUInteger bytesLength = characteristic.value.length;
            unsigned char dataBytes[bytesLength];
            [characteristic.value getBytes:dataBytes length:bytesLength];
            
            if (dataBytes[0] == 0x00) {
                stringData = @"F";
            } else {
                stringData = @"C";
            }
            
            if ([self.delegate respondsToSelector:@selector(getDeviceTemperatureScale:)]) {
                [self.delegate getDeviceTemperatureScale:stringData];
            }
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureFahrenheit_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            [self readTemperature:peripheral characteristic:characteristic] ;
        }
    }/* else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DateTime_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            [self readTime:peripheral characteristic:characteristic];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:CurrentTime_charID]]) {
        [self readTime:peripheral characteristic:characteristic];
    }*/ else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DeviceEvent_charID]]) {
        if ([peripheral.name containsString:@"KS_"]) {
            [self readTemperature:peripheral characteristic:characteristic];
        }
    }
}

- (void)readTemperature:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic {
    NSUInteger bytesLength = characteristic.value.length;
    unsigned char dataBytes[bytesLength];
    [characteristic.value getBytes:dataBytes length:bytesLength];

    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureMeasurement_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            //NSLog(@"Need to write code to read stored reading from SWT1A");
            /*
            NSLog(@"%c %c", dataBytes[0], dataBytes[1]);
            
            if (dataBytes[2] != 0x00) {
                float temperatureValue = dataBytes[2] + (dataBytes[3] * 0.1);
                NSString *temperatureScale = @"F";
                NSString *dateString = [self readTime:peripheral characteristic:characteristic];
                NSDate *temperatureDate = [self getTime:dateString];
                
                if ([self.delegate respondsToSelector:@selector(getTemperature:scale:date:)]) {
                    [self.delegate getTemperature:temperatureValue scale:temperatureScale date:temperatureDate];
                }
            }

            const uint8_t writeBytes[] = {dataBytes[0], dataBytes[1]};
            NSData *writeData = [NSData dataWithBytes:writeBytes length:sizeof(writeBytes)];
            
            [BTUtility writeCharacteristic:peripheral service:[CBUUID UUIDWithString:HealthThermometerServiceID] characteristic:[CBUUID UUIDWithString:TemperatureMeasurement_charID] data:writeData];*/
        } else if ([peripheral.name isEqualToString:@"Philips ear thermometer"] || [peripheral.name containsString:@"SCH740"]) {
            //NSLog(@"Need to write code to read temperature measurement from Philips");
            const uint16_t tempBytes = ((uint8_t)dataBytes[2] << 8) | dataBytes[1];
			NSString *temperatureScale = @"C";
            float temperatureValue = ((float)tempBytes) / 10;
            NSString *dateString = [self readTime:peripheral characteristic:characteristic];
            NSDate *temperatureDate = [self getTime:dateString];
            
			if ([self.delegate respondsToSelector:@selector(getTemperature:scale:date:deviceDiscovered:)]) {
                [self.delegate getTemperature:temperatureValue scale:temperatureScale date:temperatureDate deviceDiscovered:peripheral];
            }
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureFahrenheit_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            //NSLog(@"Need to write code to read temperature measurement from SWT1A");
            
            float temperatureValue = dataBytes[0] + (dataBytes[1] * 0.1);
            
            NSString *temperatureScale = @"F";
            NSDate *temperatureDate = [self getCurrentTime];
            
            if ([self.delegate respondsToSelector:@selector(getTemperature:scale:date:deviceDiscovered:)]) {
                [self.delegate getTemperature:temperatureValue scale:temperatureScale date:temperatureDate deviceDiscovered:peripheral];
            }
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DeviceEvent_charID]]) {

        unsigned char finalizedBytes[20];
        for (int i=0; i < characteristic.value.length; i++) {
            finalizedBytes[i] = dataBytes[i] - 0x30;
        }
        
        float temperatureValue = (finalizedBytes[2] * 100) + (finalizedBytes[3] * 10) + finalizedBytes[4] + (finalizedBytes[6] * 0.1);
        NSString *temperatureScale = [BTUtility charToASCII:dataBytes[7]];
        
        NSString *dateString = [self readTime:peripheral characteristic:characteristic];
        NSDate *temperatureDate = [self getTime:dateString];
        
        if ([self.delegate respondsToSelector:@selector(getTemperature:scale:date:deviceDiscovered:)]) {
            [self.delegate getTemperature:temperatureValue scale:temperatureScale date:temperatureDate deviceDiscovered:peripheral];
        }
    }
}

- (NSString *)readTime:(CBPeripheral *)peripheral characteristic:(CBCharacteristic *)characteristic {
    NSString *dateString = nil;
    NSUInteger bytesLength = characteristic.value.length;
    unsigned char dataBytes[bytesLength];
    [characteristic.value getBytes:dataBytes length:bytesLength];

    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TemperatureMeasurement_charID]]) {
        if ([peripheral.name isEqualToString:@"Philips ear thermometer"] || [peripheral.name containsString:@"SCH740"]) {
            //NSLog(@"Need to write code to read timestamp from temperature measurement from Philips");
            const uint16_t yearBytes = ((uint8_t)dataBytes[6] << 8) | dataBytes[5];
            
            dateString = [NSString stringWithFormat:@"%d"/*-%2c-%2c %2c:%2c:%2c"*/, yearBytes/*, dataBytes[7], dataBytes[8], dataBytes[9], dataBytes[10], dataBytes[11]*/];
        } else if ([peripheral.name isEqualToString:@"SWT1A"]) {
            dateString = [NSString stringWithFormat:@"20%2c-%2c-%2c %2c:%2c:%2c", dataBytes[10], dataBytes[9], dataBytes[7], dataBytes[6], dataBytes[5], dataBytes[4]];
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DeviceEvent_charID]]) {
        dateString = [NSString stringWithFormat:@"20%c%c-%c%c-%c%c %c%c:%c%c:%c%c", dataBytes[8], dataBytes[9], dataBytes[10], dataBytes[11], dataBytes[12], dataBytes[13], dataBytes[14], dataBytes[15], dataBytes[16], dataBytes[17], dataBytes[18], dataBytes[19]];
    } /*else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DateTime_charID]]) {
        if ([peripheral.name isEqualToString:@"SWT1A"]) {
            NSLog(@"Need to write code to read current time from SWT1A");
        }
    } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:CurrentTime_charID]]) {
        if ([peripheral.name isEqualToString:@"Philips ear thermometer"]) {
            NSLog(@"Need to write code to read current time from Philips");
        }
    }*/
    
    return dateString;
}

- (NSDate *)getTime:(NSString *)string {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateStyle:NSDateFormatterFullStyle];
    [dateFormat setTimeStyle:NSDateFormatterFullStyle];
    [dateFormat setLocale:[NSLocale systemLocale]];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *temperatureDate = [dateFormat dateFromString:string];
    
    return temperatureDate;
}

- (NSDate *)getCurrentTime {
    NSLocale* currentLocale = [NSLocale currentLocale];
    [[NSDate date] descriptionWithLocale:currentLocale];
    NSDate *currentDate = [NSDate date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    dateFormat.dateStyle = NSDateFormatterFullStyle;
    dateFormat.timeStyle = NSDateFormatterFullStyle;
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    //NSString *dateString = [dateFormat stringFromDate:currentDate];
    
    //return dateString;
    return currentDate;
}

+ (THERMConnection *)sharedManager {
    if (connector != nil) {
        return connector;
    }
    
    connector = [[THERMConnection alloc]init];
    return connector;
}

- (void)initialize {
    peripherals = [[NSMutableArray alloc]init];
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"Scale"];
    bluetoothManager = [[CBCentralManager alloc]initWithDelegate:self queue:nil options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:CBCentralManagerOptionShowPowerAlertKey]];
    _allServices = [NSArray arrayWithObjects:[CBUUID UUIDWithString:DeviceInformationServiceID], [CBUUID UUIDWithString:BatteryServiceID], [CBUUID UUIDWithString:HealthThermometerServiceID], [CBUUID UUIDWithString:CurrentTimeServiceID], [CBUUID UUIDWithString:CustomServiceID], nil];
    
    NSLog(@"Welcome to THERMConnection v1.1");
}

- (void)connectTHERM:(CBPeripheral *)peripheral {
    [bluetoothManager stopScan];
    peripheral.delegate = self;
    [bluetoothManager connectPeripheral:peripheral options:nil];
}

- (void)disconnectTHERM:(CBPeripheral *)peripheral {
    [bluetoothManager cancelPeripheralConnection:peripheral];
}

/*- (NSString *)setLEDBrightness:(int)value {
    if ([activePeripheral.name isEqualToString:@"SWT1A"]) {
        NSLog(@"Need to write code to set LED brightness of SWT1A");
        
        NSData *dataValue = [BTUtility intToData:value];
        
        [BTUtility writeCharacteristic:activePeripheral service:[CBUUID UUIDWithString:DeviceInformationServiceID] characteristic:[CBUUID UUIDWithString:DigitalInput_charID] data:dataValue];
        
        return [NSString stringWithFormat:@"LED Brightness set to %d", value];
    }
    
    return [NSString stringWithFormat:@"Not connected to SWT1A"];
}*/

- (NSString *)setTemperatureScale:(int)scale {
    if ([activePeripheral.name isEqualToString:@"SWT1A"]) {

        int intValue;
        NSString *stringData;
        if (scale < 1) {
            intValue = 0x00;
            stringData = @"F";
        } else {
            intValue = 0xFF;
            stringData = @"C";
        }
        
        NSMutableData *dataValue = [BTUtility intToData:intValue];
        
        [BTUtility writeCharacteristic:activePeripheral service:[CBUUID UUIDWithString:HealthThermometerServiceID] characteristic:[CBUUID UUIDWithString:TemperatureType_charID] data:dataValue];
        
        return [NSString stringWithFormat:@"Temperature scale set to %@ successfully", stringData];
	}
    return [NSString stringWithFormat:@"Not connected to Device"];
}

@end
