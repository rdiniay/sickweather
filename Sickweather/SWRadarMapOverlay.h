//
//  SWRadarMapOverlay.h
//  Sickweather
//
//  Created by John Erck on 5/1/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SWRadarMapOverlay : NSObject <MKOverlay>

@property (nonatomic, readonly) MKMapRect boundingMapRect;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly) NSString *overlayImageStringURL;

- (id)initUsingBoundingMapRect:(MKMapRect)boundingMapRect andCoordinate:(CLLocationCoordinate2D)coordinate withOverlayImageStringURL:(NSString *)overlayImageStringURL;

@end
