//
//  SWEmptyTableViewCell.h
//  Sickweather
//
//  Created by Shan Shafiq on 1/17/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWEmptyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@end
