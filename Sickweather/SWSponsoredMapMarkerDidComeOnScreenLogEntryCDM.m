//
//  SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM.m
//  Sickweather
//
//  Created by John Erck on 12/27/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM.h"


@implementation SWSponsoredMapMarkerDidComeOnScreenLogEntryCDM

@dynamic dateSeen;
@dynamic sponsoredMapMarkerId;

@end
