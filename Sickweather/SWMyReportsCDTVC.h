//
//  SWMyReportsCDTVC.h
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWCoreDataTableViewController.h"

@interface SWMyReportsCDTVC : SWCoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
