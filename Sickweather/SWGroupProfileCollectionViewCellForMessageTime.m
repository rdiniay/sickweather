//
//  SWDateFlowCollectionViewCell.m
//  Sickweather
//
//  Created by John Erck on 1/20/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForMessageTime.h"

@interface SWGroupProfileCollectionViewCellForMessageTime ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UILabel *dateLabel;
@end

@implementation SWGroupProfileCollectionViewCellForMessageTime

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForMessageTimeSetMessageTimeAttributedString:(NSAttributedString *)date
{
    self.dateLabel.attributedText = date;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.dateLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.dateLabel forKey:@"dateLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.dateLabel];
        
        // LAYOUT
        
        // Layout TAG LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[dateLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[dateLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config TAG LABEL
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.dateLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end

