//
//  SWAboutViewController.m
//  Sickweather
//
//  Created by John Erck on 11/16/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWAboutViewController.h"
#import "SWBuyDeviceViewController.h"

@interface SWAboutViewController () <UIScrollViewAccessibilityDelegate>
@property (strong, nonatomic) NSDictionary *aboutData; // aboutData.json
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIImageView *logoImageView;
@property (strong, nonatomic) UILabel *aboutLabel;
@property (strong, nonatomic) UILabel *questionsContactLabel;
@property (strong, nonatomic) UIButton *emailButton;
@property (strong, nonatomic) UIButton *privacyButton;
@property (strong, nonatomic) UIButton *phoneButton;
@property (strong, nonatomic) UILabel *phoneLabel;
@property (strong, nonatomic) UILabel *versionLabel;
@property (strong, nonatomic) UILabel *copyrightLabel;
@end

@implementation SWAboutViewController

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.delegate aboutViewControllerWantsToDismiss];
}

- (void)emailButtonTouchUpInside:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:info@sickweather.com"]]];
}

- (void)phoneButtonTouchUpInside:(id)sender
{
    NSString *urlString = [@"telprompt://" stringByAppendingString:@"14439187425"];
    NSURL *url = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)privacyButtonTouchUpInside:(id)sender
{
	SWBuyDeviceViewController *vc =[[UIStoryboard storyboardWithName:@"BluetoothDevice" bundle:nil] instantiateViewControllerWithIdentifier:@"SWBuyDeviceViewController"];
	vc.urlString = @"https://www.sickweather.com/privacy";
	vc.viewTitle = @"Privacy Policy";
	[self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Helpers

#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // SET HELPERS
    NSUInteger linkFontSize = 18;
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.scrollView = [[UIScrollView alloc] init];
    self.contentView = [[UIView alloc] init];
    self.logoImageView = [[UIImageView alloc] init];
    self.aboutLabel = [[UILabel alloc] init];
    self.questionsContactLabel = [[UILabel alloc] init];
    self.emailButton = [[UIButton alloc] init];
    self.phoneButton = [[UIButton alloc] init];
    self.phoneLabel = [[UILabel alloc] init];
    self.versionLabel = [[UILabel alloc] init];
	self.privacyButton = [[UIButton alloc] init];
    self.copyrightLabel = [[UILabel alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.scrollView forKey:@"scrollView"];
    [self.viewsDictionary setObject:self.contentView forKey:@"contentView"];
    [self.viewsDictionary setObject:self.logoImageView forKey:@"logoImageView"];
    [self.viewsDictionary setObject:self.aboutLabel forKey:@"aboutLabel"];
    [self.viewsDictionary setObject:self.questionsContactLabel forKey:@"questionsContactLabel"];
    [self.viewsDictionary setObject:self.emailButton forKey:@"emailButton"];
    [self.viewsDictionary setObject:self.phoneButton forKey:@"phoneButton"];
    [self.viewsDictionary setObject:self.phoneLabel forKey:@"phoneLabel"];
    [self.viewsDictionary setObject:self.versionLabel forKey:@"versionLabel"];
	[self.viewsDictionary setObject:self.privacyButton forKey:@"privacyButton"];
    [self.viewsDictionary setObject:self.copyrightLabel forKey:@"copyrightLabel"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.aboutLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.questionsContactLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.emailButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.phoneButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.phoneLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.versionLabel.translatesAutoresizingMaskIntoConstraints = NO;
	self.privacyButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.copyrightLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    [self.contentView addSubview:self.logoImageView];
    [self.contentView addSubview:self.aboutLabel];
    [self.contentView addSubview:self.questionsContactLabel];
    [self.contentView addSubview:self.emailButton];
    [self.contentView addSubview:self.phoneButton];
    [self.contentView addSubview:self.phoneLabel];
    [self.contentView addSubview:self.versionLabel];
	[self.contentView addSubview:self.privacyButton];
    [self.contentView addSubview:self.copyrightLabel];
    
    // LAYOUT
    
    // Layout SCROLL VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[scrollView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CONTENT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[contentView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout LOGO IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[logoImageView]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[logoImageView(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout ABOUT LABEL
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[aboutLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[logoImageView]-(10)-[aboutLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout QUESTIONS CONTACT LABEL
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[questionsContactLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[aboutLabel]-(30)-[questionsContactLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout EMAIL BUTTON
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[emailButton]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[questionsContactLabel]-(10)-[emailButton]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout PHONE BUTTON
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[phoneButton]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[emailButton]-(10)-[phoneButton]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout PHONE LABEL
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[phoneLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[phoneButton]-(5)-[phoneLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout VERSION LABEL
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[versionLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[phoneLabel]-(10)-[versionLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
	
	// Layout PRIVACY BUTTON
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[privacyButton]|"] options:0 metrics:0 views:self.viewsDictionary]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[versionLabel]-(5)-[privacyButton]"] options:0 metrics:0 views:self.viewsDictionary]];
	
    // Layout copyrightLabel
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[copyrightLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[privacyButton]-(5)-[copyrightLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR HEIGHT !!!
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.copyrightLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]]; // !!! THIS IS OUR SCROLL VIEW'S "PUSH OUT" VIEW FOR WIDTH !!!
    
    // CONFIG
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.navigationBar.translucent = NO;
    
    // Config NAVIGATION ITEM
    self.navigationItem.titleView = nil;
    self.navigationItem.title = @"About";
    
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    
    // Config SCROLL VIEW
    self.scrollView.delegate = self;
    self.scrollView.alwaysBounceVertical = YES;
    
    // Config CONTENT VIEW
    
    // Config LOGO IMAGE VIEW
    self.logoImageView.image = [UIImage imageNamed:@"cloud-sickweather-going-around"];
    self.logoImageView.contentMode = UIViewContentModeCenter;
    
    // Config ABOUT LABEL
    self.aboutLabel.numberOfLines = 0;
    self.aboutLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Sickweather is the largest illnesses crowdsourcing community of its kind - processing over 6 million reports of illness every month.\n\nOur patent-pending process is continually updated by our team of public health experts and data scientists to power the most sophisticated real-time disease surveillance system in the world." attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:14],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    self.aboutLabel.textAlignment = NSTextAlignmentCenter;
    
    // Config QUESTIONS CONTACT LABEL
    self.questionsContactLabel.numberOfLines = 0;
    self.questionsContactLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Questions? Contact us:" attributes:@{NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:linkFontSize],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    self.questionsContactLabel.textAlignment = NSTextAlignmentCenter;
    
    // Config EMAIL BUTTON
    [self.emailButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"info@sickweather.com" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:linkFontSize],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224]}] forState:UIControlStateNormal];
    [self.emailButton addTarget:self action:@selector(emailButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config PHONE BUTTON
    [self.phoneButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"+1 (443) 918-SICK" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:linkFontSize],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224]}] forState:UIControlStateNormal];
    [self.phoneButton addTarget:self action:@selector(phoneButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config PHONE LABEL
    self.phoneLabel.attributedText = [[NSAttributedString alloc] initWithString:@"(1-443-918-7425)" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    self.phoneLabel.textAlignment = NSTextAlignmentCenter;
    
    // Config VERSION LABEL
    self.versionLabel.attributedText = [[NSAttributedString alloc] initWithString:[SWHelper helperAppVersionString] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    self.versionLabel.textAlignment = NSTextAlignmentCenter;
	
	// Config PRIVACY BUTTON
	[self.privacyButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Privacy Policy" attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224]}] forState:UIControlStateNormal];
	[self.privacyButton addTarget:self action:@selector(privacyButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
	
    // Config copyrightLabel
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    self.copyrightLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Copyright © %@ Sickweather, Inc.", [dateFormatter stringFromDate:[NSDate new]]] attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:12],NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32]}];
    self.copyrightLabel.textAlignment = NSTextAlignmentCenter;
    
    // COLOR FOR DEVELOPMENT PURPOSES
    //self.scrollView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    //self.contentView.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
    //self.versionLabel.backgroundColor = [SWColor color:[UIColor purpleColor] usingOpacity:0.5];
}

@end
