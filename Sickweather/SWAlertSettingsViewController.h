//
//  SWAlertSettingsViewController.h
//  Sickweather
//
//  Created by John Erck on 12/19/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWAlertSettingsViewController;

@protocol SWAlertSettingsViewControllerDelegate
-(void)dismissAlertSettings:(SWAlertSettingsViewController *)vc;
@end

@interface SWAlertSettingsViewController : UIViewController
@property id<SWAlertSettingsViewControllerDelegate> delegate;
@end

