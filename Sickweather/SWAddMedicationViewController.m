//
//  SWAddMedicationViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 1/31/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWAddMedicationViewController.h"
#import "SWEventsTableViewController.h"

@interface SWAddMedicationViewController ()
@property (nonatomic , strong) NSArray *units;
@end

@implementation SWAddMedicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = @"Add Medication";
	self.units = @[@"mL",@"mg",@"tsp",@"tablets",@"capsules",@"caplets",@"packets",@"drops",@"other"];
	// Config Navigation Bar Items
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemTapped:)];
	[self.nameButton addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventTouchUpInside];
	[self.unitButton addTarget:self action:@selector(addUnits) forControlEvents:UIControlEventTouchUpInside];
	self.notesTextView.text = @"Notes (Optional)";
	self.doseTextField.attributedPlaceholder =
	[[NSAttributedString alloc]
	 initWithString:@"Dose"
	 attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
	self.notesTextView.textColor = [UIColor lightGrayColor];
	//Add Gesture
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
	[self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Target/Action

-(void)addUnits{
	UIAlertController * alertController =   [UIAlertController
								 alertControllerWithTitle:@"Choose Unit Type"
								 message:nil
								 preferredStyle:UIAlertControllerStyleActionSheet];
	for (NSString *unit in self.units) {
		UIAlertAction* unitAction = [UIAlertAction actionWithTitle:unit style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
			[self.unitButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
			[self.unitButton setTitle:action.title forState:UIControlStateNormal];
		}];
		[alertController addAction:unitAction];
	}
	
	UIAlertAction* cancel = [UIAlertAction
							 actionWithTitle:@"Cancel"
							 style:UIAlertActionStyleCancel
							 handler:nil];
	[alertController addAction:cancel];
	if ([SWHelper isIpad]){
		alertController.modalPresentationStyle = UIModalPresentationPopover;
		alertController.popoverPresentationController.sourceView = self.view;
		[self presentViewController:alertController animated:YES completion:nil];
	}else{
		[self presentViewController:alertController animated:YES completion:nil];
	}
}
	


-(void)textFieldDidChange{
	//Add Flurry Event
	[SWHelper logFlurryEventsWithTag:@"Family > Medication > Search Medication button tapped"];
	SWAddMedicationNameViewController *addMedicationNameViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddMedicationNameViewController"];
	addMedicationNameViewController.delegate = self;
	[self.navigationController pushViewController:addMedicationNameViewController animated:YES];
}

-(void)hideKeyboard{
	[self.view endEditing:YES];
}

- (void)leftBarButtonItemTapped:(id)sender
{
	[self hideKeyboard];
	[self.navigationController popViewControllerAnimated:true];
}

- (void)rightBarButtonItemTapped:(id)sender
{
	//Add Flurry Event
	[SWHelper logFlurryEventsWithTag:@"Family > Medication > Save Medication button tapped"];
	
	if ([self.nameButton.currentTitle isEqualToString:@"Medication Name"]) {
		[self showAlert:@"Add a name to save medication"];
		return;
	}
	
	if ([self.doseTextField.text isEqualToString:@""]) {
		[self showAlert:@"Add a dose to save medication"];
		return;
	}
	
	double dose = [self.doseTextField.text doubleValue];
	if (dose <= 0) {
		[self showAlert:@"Dose can not be zero"];
		return;
	}
	
	if ([self.unitButton.currentTitle isEqualToString:@"Unit"]) {
		[self showAlert:@"Select a unit to save medication"];
		return;
	}
	[self.view endEditing:true];
	NSString *name = self.nameButton.currentTitle;
	NSString *unit =self.unitButton.currentTitle;
	NSString *medicationNotes = [self.notesTextView.text isEqualToString:@"Notes (Optional)"] ? @"" : self.notesTextView.text;
	[self addTrackerWith:@{@"value":name, @"medication_unit":unit, @"medication_notes":medicationNotes, @"medication_dose":self.doseTextField.text, @"medication_name":name, @"type":[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeMdeication], @"member_id":self.familyMember != nil ? self.familyMember.member_id : @""}];
}

#pragma mark - Add Tracker

-(void)addTrackerWith:(NSDictionary*)data{
	[self.activityIndicator startAnimating];
	[[SWHelper helperAppBackend]appBackendAddTrackerUsingArgs:data completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.activityIndicator stopAnimating];
			if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) {
				if (self.isFromFamilyScreen) {
					[self navigateToEventsWith:@"Medication"];
				}else {
					[[NSNotificationCenter defaultCenter] postNotificationName:kSWRefreshEventsNotification
																		object:self
																	  userInfo:@{@"message":@"Medication"}];
					[self popBackWith:3];
				}
			}else {
				UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"An Error Occurred" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
				UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
				[alert addAction:ok];
				[self presentViewController:alert animated:YES completion:nil];
			}
		});
	}];
}

#pragma - mark Alert

-(void)showAlert:(NSString*)message {
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
	UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
	[alert addAction:ok];
	[self presentViewController:alert animated:YES completion:nil];
	return;
}

#pragma - mark User Navigation

-(void)popBackWith:(int)nb{
	NSArray *viewControllers = [self.navigationController viewControllers];
	if ([viewControllers count] > nb) {
		[self.navigationController popToViewController:[viewControllers objectAtIndex:[viewControllers count] - nb] animated:true];
	}
}


-(void)navigateToEventsWith:(NSString*)message{
	NSMutableArray *viewController = [NSMutableArray arrayWithArray: [self.navigationController viewControllers]];
	if (viewController.count >=1){
		[viewController removeObjectsInRange:NSMakeRange(1, viewController.count-1)];
		SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
		if (self.familyMember) {
			eventsTableViewController.title = self.familyMember.name_first;
		} else {
			eventsTableViewController.title = [SWUserCDM currentUser].firstName;
		}
		eventsTableViewController.showSuccessHeader = true;
		eventsTableViewController.successMessage = message;
		eventsTableViewController.isFamilyMember = self.familyMember ? YES : NO;
		eventsTableViewController.familyMember = self.familyMember;
		[viewController addObject:eventsTableViewController];
		[self.navigationController setViewControllers:viewController];
	}
	
}

#pragma -mark UITextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
	if([text isEqualToString:@"\n"]){
		textView.text = [NSString stringWithFormat:@"%@%@",textView.text , @"\n"];
		return NO;
	}
	return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
	if ([textView.text isEqualToString:@""]) {
		textView.text = @"Notes (Optional)";
		textView.textColor = [UIColor lightGrayColor];
	}
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
	if (textView.textColor == [UIColor lightGrayColor]){
		textView.text = nil;
		textView.textColor = [UIColor blackColor];
	}
}

#pragma -mark SWAddMedicationNameViewControllerDelegate
-(void)didDismissWithSuccessfulMedication:(NSString *)name{
	[self.nameButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self.nameButton setTitle:name forState:UIControlStateNormal];
}

@end


