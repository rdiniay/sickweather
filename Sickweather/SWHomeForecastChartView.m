//
//  SWHomeForecastChartView.m
//  Sickweather
//
//  Created by John Erck on 7/13/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWHomeForecastChartView.h"

@interface SWHomeForecastChartView()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@end

@implementation SWHomeForecastChartView

#pragma mark - Public Methods

- (void)updateBackgroundImage:(UIImage *)image
{
    self.backgroundImageView.image = image;
}

#pragma mark - Life Cycle Methods

- (id)init
{
    self = [super init];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.backgroundImageView = [[UIImageView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.backgroundImageView forKey:@"backgroundImageView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.backgroundImageView];
        
        // LAYOUT
        
        // backgroundImageView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[backgroundImageView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[backgroundImageView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // self
        
        // backgroundImageView
        self.backgroundImageView.image = [UIImage imageNamed:@"allergies-forecast-mock-image"];
        self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

@end
