//
//  SWConnectDeviceViewController.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 07/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THERMConnection .h"
#import "SWTemperatureView.h"
#import "SWFamilyMemberModal.h"
#import "Sickweather-Swift.h"

#define tickMark @"✓"

@interface SWConnectDeviceViewController : UIViewController <THERMConnectionDelegate, SWTemperatureViewDelegate>
typedef enum DeviceType: NSUInteger {
    philips,
    swaive,
    kinsa
} DeviceType;
typedef enum DeviceTemperatureScale: NSUInteger {
	farenheit,
	celsius,
} DeviceTemperatureSclae;
//Property
@property (nonatomic) DeviceType deviceType;
@property (nonatomic) BOOL isFamilyMember;
@property (nonatomic,strong) SWFamilyMemberModal* familyMember;
//Outlets
@property (weak, nonatomic) IBOutlet Switch *switchTempScale;
//Action
- (IBAction)temperatureScaleValueChanged:(Switch *)sender;
@end
