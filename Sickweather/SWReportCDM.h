//
//  SWReportCDM.h
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SWIllnessCDM, SWUserCDM;

NS_ASSUME_NONNULL_BEGIN

@interface SWReportCDM : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SWReportCDM+CoreDataProperties.h"
