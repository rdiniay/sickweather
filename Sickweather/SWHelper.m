//
//  SWHelpers.m
//  Sickweather
//
//  Created by John Erck on 3/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SWHelper.h"
#import "Flurry.h"
#import "Reachability.h"
#import "SWIllnessCDM+SWAdditions.h"
#import "SWActivityIndicatorView.h"
#import "UIView+SWAdditions.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation SWHelper

+ (NSManagedObjectContext *)helperManagedObjectContext
{
    SWAppDelegate *appDelegate = (SWAppDelegate *)[UIApplication sharedApplication].delegate;
    return appDelegate.managedDocument.managedObjectContext;
}

+ (NSUserDefaults *)helperDefaults
{
    return [NSUserDefaults standardUserDefaults];
}

+ (SWAppBackend *)helperAppBackend
{
    SWAppDelegate *appDelegate = (SWAppDelegate *)[UIApplication sharedApplication].delegate;
    return appDelegate.sickweatherAppBackend;
}

+ (SWAppDelegate *)helperAppDelegate
{
    return (SWAppDelegate *)[UIApplication sharedApplication].delegate;
}

+ (SWUserCDM *)helperGetCurrentUser // Legacy... this IS meant to map to currentlyLoggedInUser
{
    return [SWUserCDM currentlyLoggedInUser];
}

+ (SWUserCDM *)helperCurrentUser
{
    return [SWUserCDM currentUser];
}

+ (NSString *)helperCurrentUserSickweatherId
{
    NSString *currentUserSWId = [SWHelper helperGetCurrentUserSWIdFromDefaults];
    NSString *answer = nil;
    if ([currentUserSWId length] > 0)
    {
        answer = currentUserSWId;
    }
    return answer;
}

+ (BOOL)helperUserIsLoggedIn
{
    return [[SWHelper helperCurrentUserSickweatherId] length] > 0;
}

+ (NSString *)helperLongStateNameGivenTwoCharCode:(NSString *)twoCharCode
{
    NSDictionary *nameAbbreviations = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"Alabama",@"AL",
                                       @"Alaska",@"AK",
                                       @"Arizona",@"AZ",
                                       @"Arkansas",@"AR",
                                       @"California",@"CA",
                                       @"Colorado",@"CO",
                                       @"Connecticut",@"CT",
                                       @"Delaware",@"DE",
                                       @"District of columbia",@"DC",
                                       @"Florida",@"FL",
                                       @"Georgia",@"GA",
                                       @"Hawaii",@"HI",
                                       @"Idaho",@"ID",
                                       @"Illinois",@"IL",
                                       @"Indiana",@"IN",
                                       @"Iowa",@"IA",
                                       @"Kansas",@"KS",
                                       @"Kentucky",@"KY",
                                       @"Louisiana",@"LA",
                                       @"Maine",@"ME",
                                       @"Maryland",@"MD",
                                       @"Massachusetts",@"MA",
                                       @"Michigan",@"MI",
                                       @"Minnesota",@"MN",
                                       @"Mississippi",@"MS",
                                       @"Missouri",@"MO",
                                       @"Montana",@"MT",
                                       @"Nebraska",@"NE",
                                       @"Nevada",@"NV",
                                       @"New hampshire",@"NH",
                                       @"New jersey",@"NJ",
                                       @"New mexico",@"NM",
                                       @"New york",@"NY",
                                       @"North carolina",@"NC",
                                       @"North dakota",@"ND",
                                       @"Ohio",@"OH",
                                       @"Oklahoma",@"OK",
                                       @"Oregon",@"OR",
                                       @"Pennsylvania",@"PA",
                                       @"Rhode island",@"RI",
                                       @"South carolina",@"SC",
                                       @"South dakota",@"SD",
                                       @"Tennessee",@"TN",
                                       @"Texas",@"TX",
                                       @"Utah",@"UT",
                                       @"Vermont",@"VT",
                                       @"Virginia",@"VA",
                                       @"Washington",@"WA",
                                       @"West virginia",@"WV",
                                       @"Wisconsin",@"WI",
                                       @"Wyoming",@"WY",
                                       nil];
    NSString *translation = [nameAbbreviations objectForKey:twoCharCode];
    if (!translation) translation = twoCharCode;
    return translation;
}

+ (SWGeocodeManager *)helperGeocodeManager
{
    return [SWHelper helperAppDelegate].geocodeManager;
}

+ (CLLocationManager *)helperLocationManager
{
    return [SWHelper helperAppDelegate].locationManager;
}

+ (BOOL)helperIsCurrentlyConnectedToTheInternet
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

+ (CGFloat)getScreenWidth
{
	return [[UIScreen mainScreen] bounds].size.width;
}

+ (CGFloat)getScreenHeight
{
	return [[UIScreen mainScreen] bounds].size.height;
}

+ (BOOL)isIpad
{
	return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

+ (BOOL)helperIsProduction
{
    NSString *isProductionYesNoString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"SWIsProductionYesNoString"];
    if ([isProductionYesNoString isEqualToString:@"Yes"])
    {
        return YES;
    }
    return NO;
}

+ (NSString *)helperAppVersionString
{
    // Show version and build when in dev, in production just show version
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [NSString stringWithFormat:@" #%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    if ([SWHelper helperIsProduction])
    {
        build = @"";
    }
    return [NSString stringWithFormat:@"v%@%@", version, build];
}

+ (NSString *)helperAppName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}

+ (BOOL)helperIsIOS8OrHigher
{
    BOOL helperIsIOS8OrHigher = NO;
    if ([NSProcessInfo instancesRespondToSelector:@selector(isOperatingSystemAtLeastVersion:)])
    {
        // Conditionally check for any version >= iOS 8 using 'isOperatingSystemAtLeastVersion'
        NSOperatingSystemVersion ios8_0_0 = (NSOperatingSystemVersion){8, 0, 0};
        if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios8_0_0])
        {
            // iOS 8.0.0 and above logic
            helperIsIOS8OrHigher = YES;
        }
        else
        {
            helperIsIOS8OrHigher = NO;
        }
    }
    else
    {
        // We're on iOS 7 or below
        helperIsIOS8OrHigher = NO;
    }
    return helperIsIOS8OrHigher;
}

+ (NSString *)helperURLEncode:(NSString *)toBeURLEncodedString
{
    NSString *str = [toBeURLEncodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    str = [str stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    str = [str stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    return str;
}

+ (void)helperLogErrorToAppBackendUsingMessage:(NSString *)message file:(const char *)file prettyFunction:(const char *)prettyFunction line:(int)line
{
    NSString *appVersion = [SWHelper helperAppVersionString];
    NSString *fileString = [NSString stringWithUTF8String:file];
    NSString *prettyFunctionString = [NSString stringWithUTF8String:prettyFunction];
    NSString *lineString = [NSString stringWithFormat:@"%d", line];
    NSString *name = [NSString stringWithFormat:@"v%@-%@-%@", appVersion, [fileString lastPathComponent], lineString];
    NSString *timeNameWithExt = [NSString stringWithFormat:@"%f%@.txt", [[NSDate new] timeIntervalSince1970], name];
    NSString *content = [NSString stringWithFormat:@"NSString *name = %@\
                         \n\nNSString *message = %@\
                         \n\nNSString *prettyFunction = %@\
                         \n\nNSString *line = %@\
                         \n\nNSString *appVersion = %@\
                         ", name, message, prettyFunctionString, lineString, appVersion];
    [[SWHelper helperAppBackend] appBackendWriteFileNamed:timeNameWithExt fileContent:content usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        // Completion handler block...
    }];
}

+ (void)helperLogErrorToAppBackendForJSONResponseBody:(NSDictionary *)jsonResponseBody
                                 responseBodyAsString:(NSString *)responseBodyAsString
                                      responseHeaders:(NSHTTPURLResponse *)responseHeaders
                                         requestError:(NSError *)requestError
                                       jsonParseError:(NSError *)jsonParseError
                             requestURLAbsoluteString:(NSString *)requestURLAbsoluteString
                                                 file:(const char *)file
                                       prettyFunction:(const char *)prettyFunction
                                                 line:(int)line
{
    NSString *appVersion = [SWHelper helperAppVersionString];
    NSString *fileString = [NSString stringWithUTF8String:file];
    NSString *prettyFunctionString = [NSString stringWithUTF8String:prettyFunction];
    NSString *lineString = [NSString stringWithFormat:@"%d", line];
    NSString *name = [NSString stringWithFormat:@"v%@-%@-%@", appVersion, [fileString lastPathComponent], lineString];
    NSString *timeNameWithExt = [NSString stringWithFormat:@"%f%@.txt", [[NSDate new] timeIntervalSince1970], name];
    NSString *content = [NSString stringWithFormat:@"NSString *name = %@\
\n\nNSDictionary *jsonResponseBody = %@\
\n\nNSString *responseBodyAsString = %@\
\n\nNSHTTPURLResponse *responseHeaders = %@\
\n\nNSError *requestError = %@\
\n\nNSError *jsonParseError = %@\
\n\nNSString *requestURLAbsoluteString = %@\
\n\nNSString *prettyFunction = %@\
\n\nNSString *line = %@\
\n\nNSString *appVersion = %@\
", name, jsonResponseBody, responseBodyAsString, responseHeaders, requestError, jsonParseError, requestURLAbsoluteString, prettyFunctionString, lineString, appVersion];
    [[SWHelper helperAppBackend] appBackendWriteFileNamed:timeNameWithExt fileContent:content usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
        // Completion handler block...
    }];
}

+ (SWSlidingViewController *)helperGetTab1SlidingViewController
{
    UITabBarController *tabBar = (UITabBarController *)[SWHelper helperAppDelegate].window.rootViewController;
    SWSlidingViewController *sliderVC = (SWSlidingViewController *)[tabBar.viewControllers objectAtIndex:0];
    return sliderVC;
}

+ (UITabBarController *)helperGetTabBarSlidingViewController
{
    return (UITabBarController *)[SWHelper helperAppDelegate].window.rootViewController;
}

+ (UIViewController *)helperGetTopVCFor:(UIViewController *)vc
{
    UIViewController *topVC;
    if ([vc isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navVC = (UINavigationController *)vc;
        topVC = [SWHelper helperGetTopVCFor:navVC.topViewController];
    }
    else if ([vc isKindOfClass:[SWSlidingViewController class]])
    {
        SWSlidingViewController *slidingVC = (SWSlidingViewController *)vc;
        topVC = [SWHelper helperGetTopVCFor:slidingVC.topViewController];
    }
    else
    {
        UIViewController *nextVC = vc.presentedViewController;
        UIViewController *lastFound = vc;
        while (nextVC)
        {
            lastFound = nextVC;
            nextVC = nextVC.presentedViewController;
        }
        if ([lastFound isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navVC = (UINavigationController *)lastFound;
            topVC = [SWHelper helperGetTopVCFor:navVC.topViewController];
        }
        else
        {
            topVC = lastFound;
        }
    }
    return topVC;
}

+ (UIImage *)helperGetScreenshot
{
    UIApplication *app = [UIApplication sharedApplication];
    UIWindow *appWindow = [app.windows objectAtIndex:0];
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {
        UIGraphicsBeginImageContextWithOptions(appWindow.bounds.size, NO, [UIScreen mainScreen].scale);
    }
    else
    {
        UIGraphicsBeginImageContext(appWindow.bounds.size);
    }
    [appWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)helperOnePixelImageUsingColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSDictionary*)helperParseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

+ (void)helperShowAlertWithTitle:(NSString *)title
{
    [SWHelper helperShowAlertWithTitle:title message:@""];
}

+ (void)helperShowAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

// Figure out which switches the user has flipped on...
+ (NSArray *)helperArrayOfIllnessIDsToGeofenceFor
{
    // Figure out which switches the user has flipped on...
    NSMutableArray *runningListOfOns = [[NSMutableArray alloc] init];
    for (SWIllnessCDM *illness in [SWIllnessCDM illnessGetAllIndividuals])
    {
        // Define key
        NSString *keyword = illness.unique;
        NSString *keyPrefix = @"geofence_is_on_for_";
        NSString *dynamicKey = [keyPrefix stringByAppendingString:keyword];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:dynamicKey])
        {
            // Is on...
            [runningListOfOns addObject:illness.unique];
        }
    }
    
    // Return as NSArray
    return [runningListOfOns copy];
}

+ (NSString *)helperGetUserSelectedIllnessIDsAsCommaSeparatedString
{
    return [[SWHelper helperArrayOfIllnessIDsToGeofenceFor] componentsJoinedByString:@","];
}

+ (NSNumber *)helperHandWashTimerTotalFrameCount
{
    return [NSNumber numberWithInteger:[SWHelper helperHandWashTimerDuration].integerValue * [SWHelper helperHandWashTimerFramesPerSecond].integerValue];
}

+ (NSNumber *)helperHandWashTimerDuration
{
    return [NSNumber numberWithInteger:20];
}

+ (NSNumber *)helperHandWashTimerFramesPerSecond
{
    return [NSNumber numberWithInteger:4];
}

+ (NSString *)helperSickweatherAPIKey
{
    return [SWHelper helperAppDelegate].appBackendSickweatherAPIKey;
}

+ (CGFloat)helperECSlidingViewControllerAnchorRightPeekAmount
{
    return 64.0f;
}

+ (NSData *)helperLoadJSONDataFromBundleDocsUsingName:(NSString *)name forceFeedFromBundle:(BOOL)forceFeedFromBundle
{
    // GET BUNDLE PATH
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:name ofType:@"json"];
    NSString *bundleJSONString = [NSString stringWithContentsOfFile:bundlePath encoding:NSUTF8StringEncoding error:nil];
    //NSLog(@"bundleJSONString = %@", bundleJSONString);
    
    // GET DOCUMENTS PATH
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", name]];
    //NSLog(@"documentsPath %@", documentsPath);
    
    // CHECK FOR FORCE FEED ARG
    if (forceFeedFromBundle == YES)
    {
        NSError *errorBundleJSONString;
        [bundleJSONString writeToFile:documentsPath atomically:YES encoding:NSUTF8StringEncoding error:&errorBundleJSONString];
    }
    else
    {
        // IF THE DOCUMENTS PATH FILE DOES NOT EXIST, CREATE IT BASED ON THE BUNDLE PATH FILE
        if (![[NSFileManager defaultManager] fileExistsAtPath:documentsPath])
        {
            NSError *errorBundleJSONString;
            [bundleJSONString writeToFile:documentsPath atomically:YES encoding:NSUTF8StringEncoding error:&errorBundleJSONString];
        }
    }
    
    // NOW THAT WE'VE NORMALIZED FROM BUNDLE TO DOCUMENTS DIR, LET'S UNCONDITIONALLY READ IN FROM DOCUMENTS DIR
    
    // NOTE: THE DOCS DIR HOLDS THE DYNAMIC FILE CONTENT, THE BUNDLE DIR SERVES AS THE DEFAULT
    
    // RETURN BASED ON DOCUMENTS PATH
    NSData *documentsData = [NSData dataWithContentsOfFile:documentsPath];
    return documentsData;
}

+ (NSString *)helperLoadJSONStringFromBundleDocsUsingName:(NSString *)name forceFeedFromBundle:(BOOL)forceFeedFromBundle
{
    NSData *data = [SWHelper helperLoadJSONDataFromBundleDocsUsingName:name forceFeedFromBundle:forceFeedFromBundle];
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return string;
}

+ (NSObject *)helperLoadJSONObjectFromBundleDocsUsingName:(NSString *)name forceFeedFromBundle:(BOOL)forceFeedFromBundle
{
    NSString *string = [SWHelper helperLoadJSONStringFromBundleDocsUsingName:name forceFeedFromBundle:forceFeedFromBundle];
    NSError *jsonParseError =  nil;
    return [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonParseError];
}

+ (NSData *)helperLoadJSONDataFromDocsUsingName:(NSString *)name
{
    // GET DOCUMENTS PATH
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", name]];
    //NSLog(@"documentsPath %@", documentsPath);
    
    // IF THE DOCUMENTS PATH FILE DOES NOT EXIST, RETURN NIL
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsPath])
    {
        return nil;
    }
    
    // GET DATA, RETURN DATA
    NSData *documentsData = [NSData dataWithContentsOfFile:documentsPath];
    return documentsData;
}

+ (NSString *)helperLoadJSONStringFromDocsUsingName:(NSString *)name
{
    NSData *data = [SWHelper helperLoadJSONDataFromDocsUsingName:name];
    NSString *string = nil;
    if (data)
    {
        string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    return string;
}

+ (id)helperLoadJSONObjectFromDocsUsingName:(NSString *)name
{
    NSString *string = [SWHelper helperLoadJSONStringFromDocsUsingName:name];
    if (string)
    {
        NSError *jsonParseError =  nil;
        return [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonParseError];
    }
    return nil;
}

+ (void)helperSaveJSONData:(NSData *)data toDocsUsingName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", name]];
    //NSLog(@"documentsPath %@", documentsPath);
    [data writeToFile:documentsPath atomically:YES];
}

+ (void)helperSaveJSONString:(NSString *)string toDocsUsingName:(NSString *)name
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    [SWHelper helperSaveJSONData:data toDocsUsingName:name];
}

+ (void)helperSaveJSONObject:(NSObject *)object toDocsUsingName:(NSString *)name
{
    NSError *jsonSerializationError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&jsonSerializationError];
    if (jsonData)
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", name]];
        //NSLog(@"documentsPath %@", documentsPath);
        NSError *errorDocumentsPathJSONString;
        [jsonString writeToFile:documentsPath atomically:YES encoding:NSUTF8StringEncoding error:&errorDocumentsPathJSONString];
    }
}

+ (NSString *)helperIllnessNameForId:(NSString *)illnessId objectFromDocsUsingName: (NSString *)objectFromDocsUsingName
{
    // GET AS ARRAY
    NSArray *illnessIds = [illnessId componentsSeparatedByString:@","];
    
    // ORDER THEM ASCENDING
    NSArray *sorted = [illnessIds sortedArrayUsingComparator:^(id obj1, id obj2) {
        if ([obj1 isKindOfClass:[NSString class]] && [obj2 isKindOfClass:[NSString class]])
        {
            //NSLog(@"obj1 = %@", obj1);
            //NSLog(@"obj2 = %@", obj2);
            
            NSString *str1 = obj1;
            NSString *str2 = obj2;
            if ([str1 integerValue] > [str2 integerValue])
            {
                return NSOrderedDescending;
            }
            else if ([str1 integerValue] < [str2 integerValue])
            {
                return NSOrderedAscending;
            }
            else
            {
                return NSOrderedSame;
            }
        }
        return NSOrderedSame;
    }];
    
    // JOIN AS ORDERED STRING
    NSString *idString = [sorted componentsJoinedByString:@","];
    //NSLog(@"idString = %@", idString);
    
    // FINE NAME(S) BASED ON IDS...
    NSArray *names = @[];
    NSString * docsUsingName = [NSString stringWithFormat:@"my-network-%@", objectFromDocsUsingName];
    NSArray *illnessLookUpList = [SWHelper helperLoadJSONObjectFromDocsUsingName:docsUsingName];
    for (NSDictionary *serverIllnessDict in illnessLookUpList)
    {
        //NSLog(@"serverIllnessDict = %@", serverIllnessDict);
        if ([[serverIllnessDict objectForKey:@"id"] isKindOfClass:[NSString class]])
        {
            //NSLog(@"SERVER ID IS STRING");
            NSString *serverId = [serverIllnessDict objectForKey:@"id"];
            if ([serverId isEqualToString:idString])
            {
                // FOUND A MATCH, WE WANT TO USE THIS NAME
                names = [names arrayByAddingObject:[serverIllnessDict objectForKey:@"name"]];
            }
        }
    }
    if ([names count] == 0)
    {
        // THEN THAT MEANS WE DIDN'T FIND AN EXACT MATCH (E.G. ONLY FLU, OR THE RESPIRATORY GROUPS IDS)
        // WE MUST THEN BE WORKING WITH A "CUSTOM LIST" OF ILLNESS IDS... E.I. OUR TRENDING SICKSCORE ILLNESS IDS...
        for (id myIllnessId in illnessIds)
        {
            NSString *myIllnessIdAsString = myIllnessId;
            if ([myIllnessId isKindOfClass:[NSNumber class]])
            {
                NSNumber *tempNum = myIllnessId;
                myIllnessIdAsString = [tempNum stringValue];
            }
            //NSLog(@"myIllnessIdAsString = %@", myIllnessIdAsString);
            for (NSDictionary *serverIllnessDict in illnessLookUpList)
            {
                //NSLog(@"serverIllnessDict = %@", serverIllnessDict);
                if ([[serverIllnessDict objectForKey:@"id"] isKindOfClass:[NSString class]])
                {
                    //NSLog(@"SERVER ID IS STRING");
                    NSString *serverId = [serverIllnessDict objectForKey:@"id"];
                    //NSLog(@"serverId = %@", serverId);
                    //NSLog(@"myIllnessIdAsString = %@", myIllnessIdAsString);
                    if ([serverId isEqualToString:myIllnessIdAsString])
                    {
                        // FOUND A MATCH, WE WANT TO USE THIS NAME
                        names = [names arrayByAddingObject:[serverIllnessDict objectForKey:@"name"]];
                    }
                }
            }
        }
    }
    return [names componentsJoinedByString:@", "];
}

+ (NSString *)helperUserLocationCityStateCountryFormattedStringForUICity:(NSString *)city state:(NSString *)state country:(NSString *)country;
{
    NSString *answer = @"";
    city = [city stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    state = [state stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    country = [country stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([city isEqualToString:@""]) {city = nil;}
    if ([state isEqualToString:@""]) {state = nil;}
    if ([country isEqualToString:@""]) {country = nil;}
    if (city && state && country)
    {
        answer = [[NSString alloc] initWithFormat:@"%@, %@, %@", city, state, country];
    }
    else if (city && state)
    {
        answer = [[NSString alloc] initWithFormat:@"%@, %@", city, state];
    }
    else if (city)
    {
        answer = [[NSString alloc] initWithFormat:@"%@", city];
    }
    else if (state)
    {
        answer = [[NSString alloc] initWithFormat:@"%@", state];
    }
    else if (country)
    {
        answer = [[NSString alloc] initWithFormat:@"%@", country];
    }
    return answer;
}

+ (void)helperShowFullViewLabel:(UILabel *)label usingView:(UIView *)view;
{
    label.textAlignment = NSTextAlignmentCenter;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
    [viewsDictionary setObject:label forKey:@"label"];
    [view addSubview:label];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[label]|"] options:0 metrics:0 views:viewsDictionary]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[label]|"] options:0 metrics:0 views:viewsDictionary]];
}

+ (void)helperShowFullScreenActivityIndicatorUsingMessage:(NSString *)message
{
    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:message backgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.75] backgroundColorIsDark:YES];
}

+ (void)helperShowFullScreenActivityIndicatorUsingMessage:(NSString *)message backgroundColor:(UIColor *)backgroundColor backgroundColorIsDark:(BOOL)backgroundColorIsDark
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = [[keyWindow subviews] objectAtIndex:[[keyWindow subviews] count] - 1];
    //NSLog(@"topView = %@", topView);
    if ([topView isKindOfClass:[SWActivityIndicatorView class]])
    {
        // Use it
        SWActivityIndicatorView *activityIndicator = (SWActivityIndicatorView *)topView;
        [activityIndicator startAnimating];
    }
    else
    {
        // Add it, then use it
        SWActivityIndicatorView *activityIndicator;
        if (backgroundColorIsDark)
        {
            activityIndicator = [[SWActivityIndicatorView alloc] initUsingLight];
        }
        else
        {
            activityIndicator = [[SWActivityIndicatorView alloc] initUsingDark];
        }
        activityIndicator.backgroundColor = backgroundColor;
        activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
        [viewsDictionary setObject:activityIndicator forKey:@"activityIndicator"];
        [keyWindow addSubview:activityIndicator];
        [keyWindow bringSubviewToFront:activityIndicator];
        [keyWindow addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[activityIndicator]|"] options:0 metrics:0 views:viewsDictionary]];
        [keyWindow addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[activityIndicator]|"] options:0 metrics:0 views:viewsDictionary]];
        [activityIndicator setMessageText:message];
        [activityIndicator startAnimating];
    }
}

+ (void)helperDismissFullViewLabel:(UILabel *)label
{
    [label removeFromSuperview];
}

+ (void)helperDismissFullScreenActivityIndicator
{
    SWActivityIndicatorView *activityIndicator;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    if ([keyWindow isKindOfClass:[SWActivityIndicatorView class]])
    {
        activityIndicator = (SWActivityIndicatorView *)keyWindow;
    }
    else
    {
        for (UIView *view in [keyWindow allSubviews])
        {
            if ([view isKindOfClass:[SWActivityIndicatorView class]])
            {
                activityIndicator = (SWActivityIndicatorView *)view;
                break;
            }
        }
    }
    if (activityIndicator)
    {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    }
    else
    {
        //NSLog(@"Couldn't find activityIndicator...");
    }
}

+ (void)helperDismissFullScreenActivityIndicatorUsingSuccessMessage:(NSString *)successMessage
{
    SWActivityIndicatorView *activityIndicator;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    if ([keyWindow isKindOfClass:[SWActivityIndicatorView class]])
    {
        activityIndicator = (SWActivityIndicatorView *)keyWindow;
    }
    else
    {
        for (UIView *view in [keyWindow allSubviews])
        {
            if ([view isKindOfClass:[SWActivityIndicatorView class]])
            {
                activityIndicator = (SWActivityIndicatorView *)view;
                break;
            }
        }
    }
    if (activityIndicator)
    {
        [activityIndicator addSuccessMessage:successMessage];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        });
    }
    else
    {
        //NSLog(@"Couldn't find activityIndicator...");
    }
}

+ (NSString *)helperGetIllnessesPath
{
    // OLD: [[NSBundle mainBundle] pathForResource:@"illnesses-data" ofType:@"json"]
    /*
     Library/
     
     This is the top-level directory for any files that are not user data files. You typically put files in one of several standard subdirectories. iOS apps commonly use the Application Support and Caches subdirectories; however, you can create custom subdirectories.
     Use the Library subdirectories for any files you don’t want exposed to the user. Your app should not use these directories for user data files.
     The contents of the Library directory (with the exception of the Caches subdirectory) are backed up by iTunes and iCloud.
     For additional information about the Library directory and its commonly used subdirectories, see The Library Directory Stores App-Specific Files.
     */
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [NSString stringWithFormat:@"%@.json", @"getIllnesses.php"]];
    //NSLog(@"libraryPath = %@", libraryPath);
    return libraryPath;
}

+ (void)helperWriteString:(NSString *)string toFilePath:(NSString *)filePath
{
    NSError *error = nil;
    BOOL ok = [string writeToFile:filePath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    if (!ok)
    {
        NSLog(@"Failed to write in helperWriteString");
    }
    if (error)
    {
        NSLog(@"Fail: %@", [error localizedDescription]);
    }
}

+ (NSURL *)helperGetDocumentDirectoryURLForFileName:(NSString *)fileName
{
    NSArray *urls = [[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    urls = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *url = [urls objectAtIndex:0];
    url = [url URLByAppendingPathComponent:fileName];
    return url;
}

+ (NSDictionary *)helperGetCurrentUserFromDefaults:(NSDictionary *)user
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = @"currentUser";
    if ([user isKindOfClass:[NSDictionary class]])
    {
        [defaults setObject:user forKey:key];
        [defaults synchronize];
    }
    return [defaults objectForKey:key];
}

+ (NSString *)helperGetCurrentUserSWIdFromDefaults
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:[SWConstants currentlyLoggedInUserSickweatherIdDefaultsKey]];
}

+ (NSDictionary *)helperGetCurrentUserFromDefaults
{
    return [SWHelper helperGetCurrentUserFromDefaults:nil];
}

+ (BOOL)helperHasPassedFilterTimeSinceLastDisplayedUserFeedbackPrompt
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *date = [defaults objectForKey:@"dateObjLastDisplayedUserFeedbackPrompt"];
    if ([date isKindOfClass:[NSDate class]])
    {
        //NSLog(@"%@", date);
        NSDate *now = [NSDate new];
        double nowSeconds = [now timeIntervalSince1970];
        double thenSeconds = [date timeIntervalSince1970];
        double thirtyDaysInSeconds = 60*60*24*30;
        if (nowSeconds - thenSeconds > thirtyDaysInSeconds)
        {
            // It's time, yes, display!
            return YES;
        }
        // Nope, too soon bro
        return NO;
    }
    else
    {
        // No date even on file, yes, display!
        return YES;
    }
}

+(NSString *)getFamilId{
	return [[NSUserDefaults standardUserDefaults] valueForKey:[SWConstants familyIdDefaultKey]];
}

+ (void)addNonFatalExceptionWith:(NSException *)exception{
	NSArray *stack = [exception callStackReturnAddresses];
	[[Crashlytics sharedInstance] recordCustomExceptionName: exception.name
													 reason: exception.reason
												 frameArray: stack];
}

+(void)setFamilyId:(NSString *)familyId{
	if (familyId != nil && ![familyId isEqual:[NSNull null]]){
		[[NSUserDefaults standardUserDefaults] setValue:familyId forKey:[SWConstants familyIdDefaultKey]];
	}
}

+ (void)addNonFatalError:(NSError *)error{
	[CrashlyticsKit recordError:error];
}

+(void)logFlurryEventsWithTag:(NSString*)tag{
	if ([SWHelper helperUserIsLoggedIn]){
		NSString *userId = [SWUserCDM currentUser].unique;
		if (userId){
			[SWHelper logFlurryEventsWithTag:tag parameters:@{@"userId":userId}];
		}
		return;
	}
	[Flurry logEvent:tag];
}

+(void)logFlurryEventsWithTag:(NSString*)tag parameters:(NSDictionary*)params{
	[Flurry logEvent:tag withParameters:params];
}


+ (void)addNonFatalErrorWithDictionary:(NSDictionary *)dictionary{
	if (dictionary && [dictionary isKindOfClass:[NSDictionary class]]){
		NSError *err = [NSError errorWithDomain:@"Grouped Illness Save Crash"
										   code:500
									   userInfo:@{
												  NSLocalizedDescriptionKey:[dictionary description]
												  }];
		[CrashlyticsKit recordError:err];
	}
}

+(void)addNonFatalEventsWithName:(NSString*)name  data:(NSDictionary*)dictionary{
	if (dictionary && [dictionary isKindOfClass:[NSDictionary class]]){
		[Answers logCustomEventWithName:name customAttributes:dictionary];
	}
}

+ (UIImage *)imageFromString:(NSString *)string attributes:(NSDictionary *)attributes size:(CGSize)size
{
	UIGraphicsBeginImageContextWithOptions(size, NO, 0);
	[string drawInRect:CGRectMake(0, 0, size.width, size.height) withAttributes:attributes];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

+(float)convertCelsiusToFarenheit:(float)temp{
	return (1.8*temp) + 32;
}

+(float)convertFarenhietToCelsius:(float)temp{
	return (temp - 32)*5/9;
}

+(NSString*)updateFeverSeverityWithCelcius:(float) temp{
	if (temp >= 38.9) {
		return @"Severe Fever";
	} else if (temp >= 37.8) {
		return @"Mild Fever";
	} else if (temp >= 37.4) {
		return @"Low Fever";
	} else {
		return @"No Fever";
	}
}

+(NSString*)updateFeverSeverityWithFarenheit:(float) temp{
	if (temp >= 102) {
		return @"Severe Fever";
	} else if (temp >= 100) {
		return @"Mild Fever";
	} else if (temp >= 99.4) {
		return @"Low Fever";
	} else {
		return @"No Fever";
	}
}
@end
