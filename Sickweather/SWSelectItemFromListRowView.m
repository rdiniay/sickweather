//
//  SWSelectItemFromListRowView.m
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import "SWSelectItemFromListRowView.h"

@interface SWSelectItemFromListRowView ()
@property (assign) BOOL shouldShowTopBorder;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *rowIconImageView;
@property (strong, nonatomic) UITextField *rowTextField;
@property (strong, nonatomic) UIImageView *rowCheckmarkImageView;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UIView *bottomBorderView;
@end

@implementation SWSelectItemFromListRowView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWSelectItemFromListRowViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    self.rowTextField.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate selectItemFromListRowViewTitleString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
}

#pragma mark - Public Methods

- (void)showBottomBorder
{
    self.bottomBorderView.hidden = NO;
}

- (void)showCheckmark
{
    self.rowCheckmarkImageView.hidden = NO;
}

#pragma mark - Target Action

- (void)viewWasTapped:(id)sender
{
    [self.delegate selectItemFromListRowWasTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.rowIconImageView = [[UIImageView alloc] init];
        self.rowTextField = [[UITextField alloc] init];
        self.rowCheckmarkImageView = [[UIImageView alloc] init];
        self.topBorderView = [[UIView alloc] init];
        self.bottomBorderView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.rowIconImageView forKey:@"rowIconImageView"];
        [self.viewsDictionary setObject:self.rowTextField forKey:@"rowTextField"];
        [self.viewsDictionary setObject:self.rowCheckmarkImageView forKey:@"rowCheckmarkImageView"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        [self.viewsDictionary setObject:self.bottomBorderView forKey:@"bottomBorderView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.rowIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowCheckmarkImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.rowIconImageView];
        [self addSubview:self.rowTextField];
        [self addSubview:self.rowCheckmarkImageView];
        [self addSubview:self.topBorderView];
        [self addSubview:self.bottomBorderView];
        
        // LAYOUT
        
        // Layout ROW ICON IMAGE VIEW (MAKING IMAGE VIEW 0 LEFT MARGIN, 0 WIDTH TO HIDE BUT STILL KEEP IN HERE FOR LATER USE IF NEEDED)
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[rowIconImageView(0)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rowIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout SECTION LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowIconImageView]-(15)-[rowTextField]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[rowTextField]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ROW CHECKMARK IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowCheckmarkImageView(20)]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rowCheckmarkImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout TOP BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topBorderView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout BOTTOM BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[bottomBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[bottomBorderView(1)]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config ROW
        self.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
        
        // Config ROW ICON IMAGE VIEW
        self.rowIconImageView.contentMode = UIViewContentModeCenter;
        
        // Config ROW TEXT FIELD
        self.rowTextField.userInteractionEnabled = NO; // Make like label
        
        // Config ROW CHECKMARK IMAGE VIEW
        self.rowCheckmarkImageView.contentMode = UIViewContentModeCenter;
        self.rowCheckmarkImageView.image = [UIImage imageNamed:@"checkmark"];
        self.rowCheckmarkImageView.hidden = YES;
        
        // Config TOP BORDER ROW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config BOTTOM BORDER ROW
        self.bottomBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        self.bottomBorderView.hidden = YES;
        
        // COLOR FOR DEVELOPMENT PURPOSES
//        self.rowIconImageView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
//        self.rowTextField.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
//        self.rowCheckmarkImageView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
    }
    return self;
}

@end
