//
//  SWGroupProfileCollectionViewCellForSponsoredMessage.m
//  Sickweather
//
//  Created by John Erck on 10/20/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForSponsoredMessage.h"

@interface SWGroupProfileCollectionViewCellForSponsoredMessage ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) NSArray *horizontalConstraints;
@property (strong, nonatomic) NSArray *verticalConstraints;
@property (strong, nonatomic) UIView *myBaseView;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UIView *bottomBorderView;
@property (strong, nonatomic) UIImageView *profilePicImageView;
@property (strong, nonatomic) UILabel *usernameLabel;
@property (strong, nonatomic) UILabel *sponsorshipLabel;
@property (strong, nonatomic) UITextView *sponsoredMessageTextView;
@property (strong, nonatomic) UILabel *learnMoreLabel;
@property (strong, nonatomic) UILabel *linkLabel;
@end

@implementation SWGroupProfileCollectionViewCellForSponsoredMessage

#pragma mark - Public Methods

+ (CGSize)sizeForSponsoredMessage:(NSString *)messageString givenWidth:(CGFloat)width
{
    NSAttributedString *myString = [SWGroupProfileCollectionViewCellForSponsoredMessage attributedStringForMessage:messageString];
    CGRect rect = [myString boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return rect.size;
}

- (void)updateLeftInset:(NSNumber *)leftInset rightInset:(NSNumber *)rightInset
{
    [self.contentView removeConstraints:self.horizontalConstraints];
    self.horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[myBaseView]-(right)-|" options:0 metrics:@{@"left":leftInset,@"right":rightInset} views:self.viewsDictionary];
    [self.contentView addConstraints:self.horizontalConstraints];
}

- (void)updateTopInset:(NSNumber *)topInset bottomInset:(NSNumber *)bottomInset
{
    [self.contentView removeConstraints:self.verticalConstraints];
    self.verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[myBaseView]-(bottom)-|" options:0 metrics:@{@"top":topInset,@"bottom":bottomInset} views:self.viewsDictionary];
    [self.contentView addConstraints:self.verticalConstraints];
}

- (void)updateForServerSponsorshipDict:(NSDictionary *)sponsorshipDict
{
    /*
     {
     "advertiser_id" = 3;
     "bg_color_hex" = E6F5FC;
     "bg_color_rgb" = "230,245,252";
     brand = Clorox;
     "more_url" = "https://www.clorox.com/";
     keywords =             (
     flu,
     "common cold"
     );
     logo = "http://megan.sickweather.com/images/clorox.png";
     message = "Did you know that Clorox wipes kill over 99% of germs? Including Staph, E. Coli, and Salmonella!\\nLearn more: ";
     "more_url" = "http://sickweather.com";
     }
     */
    UIColor *colorToUse = [UIColor whiteColor];
    if ([sponsorshipDict objectForKey:@"bg_color_rgb"])
    {
        //NSLog(@"RGB: %@", [sponsorshipInfoDict objectForKey:@"bg_color_rgb"]);
        NSString *rgbString = [sponsorshipDict objectForKey:@"bg_color_rgb"];
        NSArray *items = [rgbString componentsSeparatedByString:@","];
        NSString *r;
        NSString *g;
        NSString *b;
        for (NSString *stringDigit in items)
        {
            //NSLog(@"stringDigit = %@", stringDigit);
            NSUInteger myIndex = [items indexOfObject:stringDigit];
            if (myIndex == 0) {r = stringDigit;}
            if (myIndex == 1) {g = stringDigit;}
            if (myIndex == 2) {b = stringDigit;}
        }
        //NSLog(@"RGB: %@,%@,%@", r, g, b);
        colorToUse = [UIColor colorWithRed:r.floatValue/255.0 green:g.floatValue/255.0 blue:b.floatValue/255.0 alpha:1.0];
    }
    self.myBaseView.backgroundColor = colorToUse;
    
    if ([sponsorshipDict objectForKey:@"message"])
    {
        // Message...
        //NSLog(@"%@", [sponsorshipDict objectForKey:@"message"]);
        NSAttributedString *str = [SWGroupProfileCollectionViewCellForSponsoredMessage attributedStringForMessage:[sponsorshipDict objectForKey:@"message"]];
        NSMutableAttributedString *attStr= [[NSMutableAttributedString alloc] initWithAttributedString:str];
        self.sponsoredMessageTextView.attributedText = attStr;
    }
    
    if ([sponsorshipDict objectForKey:@"brand"])
    {
        // Colorox
        //NSLog(@"%@", [sponsorshipDict objectForKey:@"brand"]);
        NSDictionary *usernameAttributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226],NSFontAttributeName:[SWFont fontStandardFontOfSize:12]};
        self.usernameLabel.attributedText = [[NSAttributedString alloc] initWithString:[sponsorshipDict objectForKey:@"brand"] attributes:usernameAttributes];
    }
    
    if ([sponsorshipDict objectForKey:@"more_url"])
    {
        // https://www.clorox.com/
        //NSLog(@"%@", [sponsorshipDict objectForKey:@"more_url"]);
    }
    
    if ([sponsorshipDict objectForKey:@"keywords"])
    {
        // array(flu, common cold)
        //NSLog(@"%@", [sponsorshipDict objectForKey:@"keywords"]);
    }
    
    if ([sponsorshipDict objectForKey:@"logo"])
    {
        // http://megan.sickweather.com/images/clorox.png
        //NSLog(@"%@", [sponsorshipDict objectForKey:@"logo"]);
    }
    
    if ([sponsorshipDict objectForKey:@"more_url"])
    {
        // http://sickweather.com
        //NSLog(@"%@", [sponsorshipDict objectForKey:@"more_url"]);
    }
}

- (void)updateSponsorProfilePicUsingImage:(UIImage *)image
{
    //NSLog(@"image = %@", image);
    self.profilePicImageView.image = image;
}

#pragma mark - Target/Action

- (void)tapped:(id)sender
{
    [self.delegate groupProfileCollectionViewCellForSponsoredMessageTapped:self];
}

#pragma mark - Helpers

+ (NSAttributedString *)attributedStringForMessage:(NSString *)message
{
    return [[NSAttributedString alloc] initWithString:message attributes:@{NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INVOKE SUPER
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.myBaseView = [[UIView alloc] init];
        self.topBorderView = [[UIView alloc] init];
        self.bottomBorderView = [[UIView alloc] init];
        self.profilePicImageView = [[UIImageView alloc] init];
        self.usernameLabel = [[UILabel alloc] init];
        self.sponsorshipLabel = [[UILabel alloc] init];
        self.sponsoredMessageTextView = [[UITextView alloc] init];
        self.learnMoreLabel = [[UILabel alloc] init];
        self.linkLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.myBaseView forKey:@"myBaseView"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        [self.viewsDictionary setObject:self.bottomBorderView forKey:@"bottomBorderView"];
        [self.viewsDictionary setObject:self.profilePicImageView forKey:@"profilePicImageView"];
        [self.viewsDictionary setObject:self.usernameLabel forKey:@"usernameLabel"];
        [self.viewsDictionary setObject:self.sponsorshipLabel forKey:@"sponsorshipLabel"];
        [self.viewsDictionary setObject:self.sponsoredMessageTextView forKey:@"sponsoredMessageTextView"];
        [self.viewsDictionary setObject:self.learnMoreLabel forKey:@"learnMoreLabel"];
        [self.viewsDictionary setObject:self.linkLabel forKey:@"linkLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myBaseView.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.profilePicImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.usernameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sponsorshipLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sponsoredMessageTextView.translatesAutoresizingMaskIntoConstraints = NO;
        self.learnMoreLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.linkLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
        [self.contentView addSubview:self.myBaseView];
        [self.myBaseView addSubview:self.topBorderView];
        [self.myBaseView addSubview:self.bottomBorderView];
        [self.myBaseView addSubview:self.profilePicImageView];
        [self.myBaseView addSubview:self.usernameLabel];
        [self.myBaseView addSubview:self.sponsorshipLabel];
        [self.myBaseView addSubview:self.sponsoredMessageTextView];
        [self.myBaseView addSubview:self.learnMoreLabel];
        [self.myBaseView addSubview:self.linkLabel];
        
        // LAYOUT
        
        // MY BASE VIEW
        self.horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[myBaseView]-(right)-|" options:0 metrics:@{@"left":@(0),@"right":@(0)} views:self.viewsDictionary];
        [self.contentView addConstraints:self.horizontalConstraints];
        self.verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[myBaseView]-(bottom)-|" options:0 metrics:@{@"top":@(0),@"bottom":@(0)} views:self.viewsDictionary];
        [self.contentView addConstraints:self.verticalConstraints];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[topBorderView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[topBorderView(1)]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[bottomBorderView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottomBorderView(1)]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[profilePicImageView(40)]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[profilePicImageView(40)]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[profilePicImageView]-(10)-[usernameLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(24)-[usernameLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[usernameLabel]-(10)-[sponsorshipLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(24)-[sponsorshipLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[sponsoredMessageTextView]-(10)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[profilePicImageView]-(10)-[sponsoredMessageTextView]-(10)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[learnMoreLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sponsoredMessageTextView]-(10)-[learnMoreLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[learnMoreLabel]-(10)-[linkLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sponsoredMessageTextView]-(10)-[linkLabel]" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // SELF
        self.clipsToBounds = NO;
        self.contentView.clipsToBounds = NO;
        
        // TOP BORDER VIEW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // BOTTOM BORDER VIEW
        self.bottomBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // PROFILE PIC IMAGE VIEW
        self.profilePicImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.profilePicImageView.layer.cornerRadius = 20;
        self.profilePicImageView.clipsToBounds = YES;
        self.profilePicImageView.backgroundColor = [UIColor whiteColor];
        
        //self.usernameLabel.text = @"DEMO";
        
        NSDictionary *createdAttributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:12]};
        self.sponsorshipLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Sponsored Message" attributes:createdAttributes];
        
        self.sponsoredMessageTextView.backgroundColor = [UIColor clearColor];
        self.sponsoredMessageTextView.editable = NO;
        [self.sponsoredMessageTextView setDataDetectorTypes:UIDataDetectorTypeNone];
        
        //self.learnMoreLabel.text = @"Learn more:";
        
        //self.linkLabel.text = @"http://clorox.com";
		[self.sponsoredMessageTextView setUserInteractionEnabled:false];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}


@end
