//
//  SWGroupProfileViewController.m
//  Sickweather
//
//  Created by John Erck on 11/16/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <WebKit/WebKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "SWGroupProfileViewController.h"
#import "SWGroupProfileHeaderView.h"
#import "NSAttributedString+Height.h"
#import "SWGroupProfileCollectionViewCellForIllnessTag.h"
#import "SWGroupProfileCollectionViewCellForProfilePic.h"
#import "SWGroupProfileCollectionViewCellForUsername.h"
#import "SWGroupProfileCollectionViewCellForMessageTime.h"
#import "SWGroupProfileCollectionViewCellForMessageText.h"
#import "SWGroupProfileCollectionViewCellForSympathy.h"
#import "SWGroupProfileCollectionViewCellForSponsoredMessage.h"
#import "SWGroupProfileCollectionViewCellForMessageSpacer.h"
#import "SWGroupProfileCollectionViewCellForEmptyMessageArea.h"
#import "SWGroupProfileCollectionView.h"
#import "SWGroupProfileCollectionViewCellForCustomView.h"
#import "SWCollectionViewFlowLayout.h"
#import "SWReportIllnessViewController.h"
#import "Flurry.h"
#import "SWReportCDM+SWAdditions.h"
#import "NSString+FontAwesome.h"
#import "SWMessageBannerView.h"
#import "SWBannerView.h"

#define staticHeaderImageViewDefaultHeight 200
#define DEFAULT_LEFT_RIGHT_PADDING_INSET 15
#define DEFAULT_TOP_BOTTOM_PADDING_INSET 10
#define DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT 36
#define DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT_PLUS_PADDING 36+DEFAULT_TOP_BOTTOM_PADDING_INSET+DEFAULT_TOP_BOTTOM_PADDING_INSET // +10+10 is for V spacing between container and textview in layout
#define CELL_TYPE_KEY @"cell_type"
#define IS_HEADER_CELL_TYPE @"header"
#define IS_PROFILE_PIC_CELL_TYPE @"profile_pic"
#define IS_USERNAME_CELL_TYPE @"username"
#define IS_MESSAGE_TIME_CELL_TYPE @"message_time"
#define IS_KEYWORD_CELL_TYPE @"keyword"
#define IS_MESSAGE_TEXT_CELL_TYPE @"messsage_text"
#define IS_SYMPATHY_CELL_TYPE @"sympathy"
#define IS_SPONSORED_MESSAGE_CELL_TYPE @"SWGroupProfileCollectionViewCellForSponsoredMessage"
#define IS_MESSAGE_SPACER_CELL_TYPE @"messsage_spacer"
#define IS_EMPTY_MESSAGE_AREA_CELL_TYPE @"empty_message_area"

@interface SWGroupProfileViewController () <SWGroupProfileMessageTableViewHeaderDelegate, UIActionSheetDelegate, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SWGroupProfileCollectionViewCellForMessageSpacerDelegate, SWGroupProfileCollectionViewCellForSympathyDelegate, SWReportIllnessViewControllerDelegate, SWMessageBannerViewDelegate, UIAlertViewDelegate, SWGroupProfileCollectionViewCellForSponsoredMessageDelegate, SWBannerViewDelegate, SWGroupProfileCollectionViewCellForEmptyMessageAreaDelegate, FBSDKSharingDelegate, MFMessageComposeViewControllerDelegate>
@property (strong, nonatomic) UIImage *sympathyAnimationImage;
@property (strong, nonatomic) UIImage *sympathyIconColorImage;
@property (strong, nonatomic) UIImage *sympathyIconGrayImage;
@property (strong, nonatomic) UIImage *sponsoredMessageLogoImage;
@property (strong, nonatomic) UIAlertView *areYouSureYouWantToDeleteThisGroupAlert;
@property (strong, nonatomic) UIAlertView *areYouSureYouWantToUnfollowThisGroupAndLoseAdminRightsAlert;
@property (strong, nonatomic) NSMutableDictionary *imageForStringURL;
@property (strong, nonatomic) NSMutableArray *stringURLCurrentlyLoading;
@property (strong, nonatomic) UIImageView *staticHeaderImageView;
@property (strong, nonatomic) UIView *staticHeaderImageOverlayView;
@property (strong, nonatomic) SWCollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) NSMutableArray *cvDataObjects;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) SWGroupProfileCollectionView *collectionView;
@property (strong, nonatomic) UIView *chatBarContainerView;
@property (strong, nonatomic) UIView *chatBarContainerViewInnerBackgroundView;
@property (strong, nonatomic) UITextView *chatTextView;
@property (strong, nonatomic) UIButton *chatButtonSend;
@property (strong, nonatomic) WKWebView *webView;
@property (strong, nonatomic) SWBannerView *bannerView;
@property (strong, nonatomic) NSString *reportCount;
@property (strong, nonatomic) NSString *followerCount;
@property (strong, nonatomic) NSString *adminCount;
@property (strong, nonatomic) SWGroupProfileHeaderView *header;
@property (strong, nonatomic) NSMutableDictionary *getGroupProfileJSONObject;
@property (strong, nonatomic) UIActionSheet *groupLevelAdminActionSheet;
@property (strong, nonatomic) UIActionSheet *groupLevelStandardActionSheet;
@property (strong, nonatomic) UIActionSheet *messageLevelActionSheet;
@property (strong, nonatomic) UIActionSheet *shareGroupActionSheet;
@property (strong, nonatomic) NSLayoutConstraint *chatTextViewHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *spaceFromBottomChatBarContainerViewConstraint;
@property (strong, nonatomic) NSIndexPath *currentTagFlowCollectionViewIndexPath;
@property (strong, nonatomic) NSString *currentMessageId;
@property (strong, nonatomic) NSDictionary *currentMessageDict;
@property (strong, nonatomic) NSLayoutConstraint *staticHeaderImageViewHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *staticHeaderImageOverlayViewHeightConstraint;
@property (strong, nonatomic) NSString *startIndex;
@property (strong, nonatomic) NSString *perPage;
@property (strong, nonatomic) NSLayoutConstraint *webViewApplyToHideConstraint;
@property (strong, nonatomic) NSLayoutConstraint *webViewApplyToShowConstraint;
@property (strong, nonatomic) NSLayoutConstraint *bannerViewApplyToHideConstraint;
@property (strong, nonatomic) NSLayoutConstraint *bannerViewApplyToShowConstraint;
@property (assign) BOOL isCurrentlyLoadingNextPage;
@property (assign) BOOL haveLoadedAllMessages;
@property (assign) BOOL keyboardIsComingUp;
@property (assign) BOOL haveLoggedSponsoredMessageServedForThisViewDidLoad;
@end

@implementation SWGroupProfileViewController

#pragma mark - Public Methods

- (void)loadForFoursquareId:(NSString *)foursquareId
{
    self.serverGroupDict = @{@"foursquare_id": foursquareId};
    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkDataUsingCompletionBlock:^{}];
}

- (void)setThenLoadGetGroupProfileJSONObject:(NSMutableDictionary *)dict
{
    if (!dict)
    {
        [self.delegate groupProfileViewControllerWantsToDismiss];
        [SWHelper helperShowAlertWithTitle:@"Group Not Found"];
    }
    else
    {
        self.getGroupProfileJSONObject = dict;
        self.serverGroupDict = @{@"foursquare_id": [self helperGetGroupFoursquareId]};
        [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkDataUsingCompletionBlock:^{}];
    }
}

#pragma mark - FBSDKSharingDelegate

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    //NSLog(@"sharer = %@", sharer);
    [Flurry logEvent:@"Share Group Cancelled" withParameters:@{@"Network": @"Facebook"}];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    //NSLog(@"error = %@", error);
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    //NSLog(@"results = %@", results);
    [Flurry logEvent:@"Share Group" withParameters:@{@"Network": @"Facebook"}];
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    if (result == MessageComposeResultCancelled)
    {
        [Flurry logEvent:@"Share Group Cancelled" withParameters:@{@"Network": @"Text Message"}];
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
    else if (result == MessageComposeResultFailed)
    {
        [self dismissViewControllerAnimated:YES completion:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occurred. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }];
    }
    else if (result == MessageComposeResultSent)
    {
        [Flurry logEvent:@"Share Group" withParameters:@{@"Network": @"Text Message"}];
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
}

#pragma mark - SWBannerViewDelegate

- (void)bannerViewWantsToDismiss:(SWBannerView *)sender
{
    if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
    {
        if ([[[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"] isKindOfClass:[NSString class]])
        {
            NSString *brand = [[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"];
            [Flurry logEvent:@"Sponsored Sympathy Message OK" withParameters:@{@"Brand": brand}];
        }
    }
    [self helperBannerViewHide];
}

#pragma mark - SWGroupProfileCollectionViewCellForSponsoredMessageDelegate

- (void)groupProfileCollectionViewCellForSponsoredMessageTapped:(SWGroupProfileCollectionViewCellForSponsoredMessage *)sender
{
    //NSLog(@"Now we here....");
    [self.view endEditing:YES];
    if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
    {
        if ([[[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"more_url"] isKindOfClass:[NSString class]])
        {
            NSString *moreURL = [[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"more_url"];
            NSURL *url = [NSURL URLWithString:moreURL];
            [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:url]];
            if ([[[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"] isKindOfClass:[NSString class]])
            {
                NSString *brand = [[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"];
                [Flurry logEvent:@"Sponsored Message Tapped" withParameters:@{@"Brand": brand}];
            }
            self.webView.hidden = NO;
            [UIView animateWithDuration:0.5 animations:^{
                [self.view removeConstraint:self.webViewApplyToHideConstraint];
                [self.view addConstraint:self.webViewApplyToShowConstraint];
                [self.view layoutIfNeeded];
            }];
            /*
             if (![[UIApplication sharedApplication] openURL:url])
             {
             //NSLog(@"%@%@",@"Failed to open url:",[url description]);
             }
             */
        }
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:self.areYouSureYouWantToDeleteThisGroupAlert])
    {
        if (buttonIndex == 0)
        {
            //NSLog(@"Cancel delete group...");
            
            // Do nothing
        }
        else if (buttonIndex == 1)
        {
            //NSLog(@"Delete group...");
            NSMutableDictionary *serverArgs = [[NSMutableDictionary alloc] init];
            [serverArgs setObject:[self helperGetGroupId] forKey:@"group_id"];
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendDeleteGroupUsingArgs:serverArgs completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    NSString *message = [jsonResponseBody objectForKey:@"message"];
                    if ([message isKindOfClass:[NSString class]] && message.length > 0)
                    {
                        if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                        {
                            [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                        }
                    }
                    [self.delegate groupProfileViewControllerWantsToDismiss];
                });
            }];
        }
    }
    if ([alertView isEqual:self.areYouSureYouWantToUnfollowThisGroupAndLoseAdminRightsAlert])
    {
        if (buttonIndex == 0)
        {
            // Do nothing
        }
        else if (buttonIndex == 1)
        {
            // Log to Flurry
            [Flurry logEvent:@"Following button tapped in a group listing"];
            
            [[self.getGroupProfileJSONObject objectForKey:@"user"] setObject:@(0) forKey:@"following"];
            self.followerCount = [[[NSNumber alloc] initWithInt:self.followerCount.intValue - 1] stringValue];
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendUnfollowGroupUsingArgs:@{@"group_id":[self helperGetGroupId]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                });
            }];
        }
    }
}

#pragma mark - Phone Rotated Code

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.flowLayout invalidateLayout];
    [self.header setNeedsLayout];
}

#pragma mark - Keyboard Management Methods

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    self.keyboardIsComingUp = YES;
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    self.spaceFromBottomChatBarContainerViewConstraint.constant = -kbRect.size.height;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    /*
    // Calculate the frame of the keyboard, in self.view's coordinate system
    NSDictionary* info          = [aNotification userInfo];
    CGRect kbRect               = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect                      = [self.view convertRect:kbRect fromView:nil];
    self.spaceFromBottomChatBarContainerViewConstraint.constant = -kbRect.size.height;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
     */
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    self.collectionView.scrollIndicatorInsets = [self helperGetDefaultCollectionViewScrollIndicatorInsets];
	if (@available(iOS 11.0, *)) {
		self.spaceFromBottomChatBarContainerViewConstraint.constant = 0;
	}else {
		self.spaceFromBottomChatBarContainerViewConstraint.constant = -49;
	}
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

// Register for notifications helper
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWasTapped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
}

#pragma mark - SWMessageBannerViewDelegate

- (void)messageBannerViewWantsToDismiss:(id)sender
{
    if ([sender isKindOfClass:[SWMessageBannerView class]])
    {
        SWMessageBannerView *messageBannerView = (SWMessageBannerView *)sender;
        [UIView animateWithDuration:1.0 animations:^{
            messageBannerView.alpha = 0;
        } completion:^(BOOL finished) {
            [messageBannerView removeFromSuperview];
        }];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.collectionView])
    {
        if (scrollView.contentSize.height > 0)
        {
            CGFloat y = scrollView.contentOffset.y;
            //NSLog(@"y = %f", y);
            //NSLog(@"y + scrollView.frame.size.height = %f", y + scrollView.frame.size.height);
            //NSLog(@"scrollView.contentSize.height = %f", scrollView.contentSize.height);
            //NSLog(@"scrollView.contentSize.height + scrollView.frame.size.height = %f", scrollView.contentSize.height + scrollView.frame.size.height);
            
            // Handle top view size/zoom animation...
            if (y < 0)
            {
                self.staticHeaderImageViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight - y - 1;
                self.staticHeaderImageOverlayViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight - y - 1;
            }
            else if (y < staticHeaderImageViewDefaultHeight)
            {
                self.staticHeaderImageViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight - y;
                self.staticHeaderImageOverlayViewHeightConstraint.constant = staticHeaderImageViewDefaultHeight - y;
            }
            else
            {
                self.staticHeaderImageViewHeightConstraint.constant = 0;
                self.staticHeaderImageOverlayViewHeightConstraint.constant = 0;
            }
            
            // Handle infini scroll...
            if (y + scrollView.frame.size.height > scrollView.contentSize.height)
            {
                //[SWHelper helperShowAlertWithTitle:@"At Bottom, Load More..."];
                NSString *foursquareId = [self.serverGroupDict objectForKey:@"foursquare_id"];
                if (foursquareId)
                {
                    //start_index - pagination start index for groups messages (defaults to 0)
                    //per_page - number of groups messages per page (defaults to 10)
                    if (self.isCurrentlyLoadingNextPage)
                    {
                        // Skip...
                        //NSLog(@"Skip..., isCurrentlyLoadingNextPage");
                    }
                    else if (self.haveLoadedAllMessages)
                    {
                        // Skip...
                        //NSLog(@"Skip..., haveLoadedAllMessages");
                    }
                    else
                    {
                        self.isCurrentlyLoadingNextPage = YES;
                        [[SWHelper helperAppBackend] appBackendGetGroupProfileUsingArgs:@{@"foursquare_id":foursquareId,@"start_index":self.startIndex,@"per_page":self.perPage} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.isCurrentlyLoadingNextPage = NO;
                                //self.getGroupProfileJSONObject = (NSMutableDictionary *)jsonResponseBody;
                                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                                if ([[self.getGroupProfileJSONObject objectForKey:@"messages"] isKindOfClass:[NSArray class]] && [[jsonResponseBody objectForKey:@"messages"] isKindOfClass:[NSArray class]])
                                {
                                    NSArray *newMessages = [jsonResponseBody objectForKey:@"messages"];
                                    if (newMessages.count < [self.perPage integerValue])
                                    {
                                        self.haveLoadedAllMessages = YES;
                                    }
                                    //NSLog(@"self.startIndex BEFORE = %@", self.startIndex);
                                    self.startIndex = [@([self.startIndex integerValue] + newMessages.count) stringValue];
                                    //NSLog(@"self.startIndex AFTER = %@", self.startIndex);
                                    NSArray *compositeMessages = [[self.getGroupProfileJSONObject objectForKey:@"messages"] arrayByAddingObjectsFromArray:newMessages];
                                    [self.getGroupProfileJSONObject setObject:compositeMessages forKey:@"messages"];
                                    [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse]; // This is the one place we run this code, after that it's just a static structure we work with
                                    [self helperUpdateUIBasedOnLocalGetGroupProfileJSONObject];
                                }
                            });
                        }];
                    }
                }
            }
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.collectionView])
    {
        [self.view endEditing:YES];
        [self helperBannerViewHide];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return nil;
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (void)textViewDidChange:(UITextView *)textView
{
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherDarkGrayText104x104x104],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
    //NSLog(@"textView.frame.size.width = %@", @(textView.frame.size.width));
    CGFloat attrStringHeight = [textView.attributedText heightForWidth:textView.frame.size.width - 12]; // The -12 just makes us grow the textView's height a little early rather than a lil late
    self.chatTextViewHeightConstraint.constant = attrStringHeight + 10;
    if (self.chatTextViewHeightConstraint.constant < DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT)
    {
        self.chatTextViewHeightConstraint.constant = DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT;
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //NSLog(@"buttonIndex = %ld", (long)buttonIndex);
    if ([actionSheet isEqual:self.groupLevelAdminActionSheet])
    {
        if (buttonIndex == 0)
        {
            // Delete Group
            self.areYouSureYouWantToDeleteThisGroupAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, Delete Group", nil];
            [self.areYouSureYouWantToDeleteThisGroupAlert show];
        }
        if (buttonIndex == 1)
        {
            // Share Group
            [self helperShareGroup];
        }
        if (buttonIndex == 2)
        {
            // Release Claim (Remove Admin)
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendRemoveGroupAdminUsingArgs:@{@"group_id":[self helperGetGroupId]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    NSString *message = [jsonResponseBody objectForKey:@"message"];
                    if ([message isKindOfClass:[NSString class]] && message.length > 0)
                    {
                        if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                        {
                            [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                        }
                    }
                });
            }];
        }
        if (buttonIndex == 3)
        {
            // Turn group notifications on/off (notifications - 0 for off, 1 for on)
            NSString *notificationsArgValue = @"1";
            if ([self helperUserHasGroupNotificationsOn])
            {
                // They are currently on, so let's turn off...
                
                // Log to Flurry
                [Flurry logEvent:@"Turn off group notifications button tapped"];
                
                notificationsArgValue = @"0";
            }
            else
            {
                // They are currently off, so let's turn on...
                
                // Log to Flurry
                [Flurry logEvent:@"Turn on group notifications button tapped"];
                
                notificationsArgValue = @"1";
            }
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendTurnGroupNotificationsOnOffUsingArgs:@{@"group_id":[self helperGetGroupId],@"notifications":notificationsArgValue} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    NSString *message = [jsonResponseBody objectForKey:@"message"];
                    if ([message isKindOfClass:[NSString class]] && message.length > 0)
                    {
                        if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                        {
                            [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                        }
                    }
                });
            }];
        }
        if (buttonIndex == 4)
        {
            // Cancel
        }
    }
    if ([actionSheet isEqual:self.groupLevelStandardActionSheet])
    {
        if (buttonIndex == 0)
        {
            [self helperShareGroup];
        }
        if (buttonIndex == 1)
        {
            // Report Inappropriate
            
            // Log to Flurry
            [Flurry logEvent:@"Report Inappropriate button tapped"];
            
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendReportGroupUsingArgs:@{@"group_id":[self helperGetGroupId]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    NSString *message = [jsonResponseBody objectForKey:@"message"];
                    if ([message isKindOfClass:[NSString class]] && message.length > 0)
                    {
                        if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                        {
                            [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                        }
                    }
                });
            }];
        }
        if (buttonIndex == 2)
        {
            // Turn group notifications on/off (notifications - 0 for off, 1 for on)
            NSString *notificationsArgValue = @"1";
            if ([self helperUserHasGroupNotificationsOn])
            {
                notificationsArgValue = @"0";
            }
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendTurnGroupNotificationsOnOffUsingArgs:@{@"group_id":[self helperGetGroupId],@"notifications":notificationsArgValue} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    NSString *message = [jsonResponseBody objectForKey:@"message"];
                    if ([message isKindOfClass:[NSString class]] && message.length > 0)
                    {
                        if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                        {
                            [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                        }
                    }
                });
            }];
        }
        if (buttonIndex == 3)
        {
            // Cancel
        }
    }
    if ([actionSheet isEqual:self.messageLevelActionSheet])
    {
        // Is the user's own message
        if ([[self.currentMessageDict objectForKey:@"user_id"] isEqualToString:[SWUserCDM currentlyLoggedInUser].unique])
        {
            //self.messageLevelActionSheet = [[UIActionSheet alloc] initWithTitle:@"Message Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete Message", nil];
            if (buttonIndex == 0)
            {
                // Delete message
                
                // Link to delete message endpoint
                [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                [[SWHelper helperAppBackend] appBackendDeleteGroupMessageUsingArgs:@{@"group_id":[self helperGetGroupId],@"message_id":self.currentMessageId} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                        [SWHelper helperDismissFullScreenActivityIndicator];
                        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                        NSString *message = [jsonResponseBody objectForKey:@"message"];
                        if ([message isKindOfClass:[NSString class]] && message.length > 0)
                        {
                            if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                            {
                                [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                            }
                        }
                    });
                }];
            }
            if (buttonIndex == 1)
            {
                // Cancel
            }
        }
        else
        {
            // Is another user's message
            if ([self helperUserIsAdmin])
            {
                // User is admin, they can delete or block
                //self.messageLevelActionSheet = [[UIActionSheet alloc] initWithTitle:@"Message Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete Message", @"Block Follower", nil];
                if (buttonIndex == 0)
                {
                    // Delete message
                    
                    // Link to delete message endpoint
                    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                    [[SWHelper helperAppBackend] appBackendDeleteGroupMessageUsingArgs:@{@"group_id":[self helperGetGroupId],@"message_id":self.currentMessageId} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                            [SWHelper helperDismissFullScreenActivityIndicator];
                            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                            NSString *message = [jsonResponseBody objectForKey:@"message"];
                            if ([message isKindOfClass:[NSString class]] && message.length > 0)
                            {
                                if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                                {
                                    [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                                }
                            }
                        });
                    }];
                }
                if (buttonIndex == 1)
                {
                    // Block follower
                    
                    // Link to block follower endpoint
                    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                    [[SWHelper helperAppBackend] appBackendBlockGroupFollowerUsingArgs:@{@"group_id":[self helperGetGroupId],@"block_id":[self.currentMessageDict objectForKey:@"user_id"]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                            [SWHelper helperDismissFullScreenActivityIndicator];
                            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                            NSString *message = [jsonResponseBody objectForKey:@"message"];
                            if ([message isKindOfClass:[NSString class]] && message.length > 0)
                            {
                                if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                                {
                                    [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                                }
                            }
                        });
                    }];
                }
                if (buttonIndex == 2)
                {
                    // Add admin
                    
                    // Link to add admin endpoint
                    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                    [[SWHelper helperAppBackend] appBackendAddGroupAdminUsingArgs:@{@"group_id":[self helperGetGroupId],@"admin_id":[self.currentMessageDict objectForKey:@"user_id"]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                            [SWHelper helperDismissFullScreenActivityIndicator];
                            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                            NSString *message = [jsonResponseBody objectForKey:@"message"];
                            if ([message isKindOfClass:[NSString class]] && message.length > 0)
                            {
                                if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                                {
                                    [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                                }
                            }
                        });
                    }];
                }
                if (buttonIndex == 3)
                {
                    // Cancel
                }
            }
            else
            {
                // Just report
                //self.messageLevelActionSheet = [[UIActionSheet alloc] initWithTitle:@"Message Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report User", nil];
                if (buttonIndex == 0)
                {
                    // Report User
                    
                    // Log to Flurry
                    [Flurry logEvent:@"Report user button tapped"];
                    
                    // Link to report user endpoint
                    [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                    [[SWHelper helperAppBackend] appBackendReportUserOfGroupUsingArgs:@{@"group_id":[self helperGetGroupId],@"reported_id":[self.currentMessageDict objectForKey:@"user_id"]} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                            [SWHelper helperDismissFullScreenActivityIndicator];
                            //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                            NSString *message = [jsonResponseBody objectForKey:@"message"];
                            if ([message isKindOfClass:[NSString class]] && message.length > 0)
                            {
                                if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                                {
                                    [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                                }
                            }
                        });
                    }];
                }
                if (buttonIndex == 1)
                {
                    // Cancel
                }
            }
        }
    }
    if ([actionSheet isEqual:self.shareGroupActionSheet])
    {
        NSString *urlString = [NSString stringWithFormat:@"https://www.sickweather.com/groups/profile/%@", [self helperGetGroupFoursquareId]];
        if (buttonIndex == 0)
        {
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            content.contentURL = [NSURL URLWithString:urlString];
			//content.contentDescription = urlString;
			//content.contentTitle = @"Share Group";
            [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
        }
        else if (buttonIndex == 1)
        {
            //NSLog(@"buttonIndex == 1, Twitter");
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                composeViewController.completionHandler = ^(SLComposeViewControllerResult result) {
                    switch(result) {
                            //  This means the user cancelled without sending the Tweet
                        case SLComposeViewControllerResultCancelled:
                            [Flurry logEvent:@"Share Group Cancelled" withParameters:@{@"Network": @"Twitter"}];
                            break;
                            //  This means the user hit 'Send'
                        case SLComposeViewControllerResultDone:
                            [Flurry logEvent:@"Share Group" withParameters:@{@"Network": @"Twitter"}];
                            break;
                    }
                };
                [composeViewController setInitialText:urlString];
                [self presentViewController:composeViewController animated:YES completion:^{}];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign into Twitter" message:@"You can do so via the Settings app under Twitter." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else if (buttonIndex == 2)
        {
            //NSLog(@"buttonIndex == 2, iMessage");
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            messageController.messageComposeDelegate = self;
            messageController.body = urlString;
            [self presentViewController:messageController animated:YES completion:^{}];
        }
        else if (buttonIndex == 3)
        {
            //NSLog(@"buttonIndex == 3, Copy Link");
            [SWHelper helperShowAlertWithTitle:@"Share Link Copied" message:@"Paste to iMessage, Facebook, Twitter, or anywhere else to share this group."];
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.URL = [NSURL URLWithString:urlString];
            [Flurry logEvent:@"Share Group" withParameters:@{@"Network": @"Copy Link"}];
        }
        else if (buttonIndex == 4)
        {
            //NSLog(@"buttonIndex == 4, Cancel");
        }
    }
}

#pragma mark - SWGroupProfileCollectionViewCellForEmptyMessageAreaDelegate

- (void)groupProfileCollectionViewCellForEmptyMessageAreaShareGroupButtonTapped:(id)sender
{
    [self helperShareGroup];
}

#pragma mark - SWGroupProfileMessageTableViewHeaderDelegate

- (NSString *)groupProfileHeaderViewTitleString:(SWGroupProfileHeaderView *)sender
{
    NSString *answer = @"";
    if (self.serverGroupDict)
    {
        NSString *title = [self.serverGroupDict objectForKey:@"title"];
        if (title)
        {
            answer = title;
        }
    }
    if ([[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"group_name"])
    {
        answer = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"group_name"];
    }
    return answer;
}

- (NSString *)groupProfileHeaderViewAddressString:(SWGroupProfileHeaderView *)sender
{
    NSString *answer = @"";
    if (self.serverGroupDict)
    {
        NSString *address = [self.serverGroupDict objectForKey:@"address"];
        if (address && [address isKindOfClass:[NSString class]])
        {
            answer = address;
            NSString *city = [self.serverGroupDict objectForKey:@"city"];
            if (city && [city isKindOfClass:[NSString class]])
            {
                answer = [answer stringByAppendingString:[NSString stringWithFormat:@", %@", city]];
                NSString *state = [self.serverGroupDict objectForKey:@"state"];
                if (state && [state isKindOfClass:[NSString class]])
                {
                    answer = [answer stringByAppendingString:[NSString stringWithFormat:@", %@", state]];
                }
            }
        }
    }
    return answer;
}

- (NSString *)groupProfileHeaderViewReportCountString:(SWGroupProfileHeaderView *)sender
{
    return self.reportCount;
}

- (NSString *)groupProfileHeaderViewFollowerCountString:(SWGroupProfileHeaderView *)sender
{
    return self.followerCount;
}

- (NSString *)groupProfileHeaderViewAdminCountString:(SWGroupProfileHeaderView *)sender
{
    return self.adminCount;
}

- (BOOL)groupProfileHeaderViewUserIsFollowingGroup:(SWGroupProfileHeaderView *)sender
{
    return [self helperUserIsFollowingGroup];
}

- (BOOL)groupProfileHeaderViewUserIsGroupAdmin:(SWGroupProfileHeaderView *)sender
{
    return [self helperUserIsAdmin];
}

- (void)groupProfileHeaderViewFollowButtonTapped:(SWGroupProfileHeaderView *)sender;
{
    /*
     po self.getGroupProfileJSONObject
     {
     messages =     (
     );
     profile =     {
     address = "501 Main St Ste E";
     "admin_count" = 0;
     "admin_owner" = "";
     categories = "Vegetarian / Vegan Restaurant";
     city = "Huntington Beach";
     closed = "";
     country = "United States";
     "date_created" = "2016-01-08 21:06:09";
     "date_lastmod" = "2016-01-08 21:06:09";
     "foursquare_id" = 4bb95641935e9521c6142790;
     "group_name" = "Bodhi Tree Vegetarian Cafe";
     "group_type" = "";
     id = 62;
     lat = "33.66120147705";
     lon = "-117.99891662598";
     "members_count" = 0;
     phone = "(714) 969-9500";
     "reports_count" = 0;
     state = CA;
     zip = 92648;
     };
     user =     {
     following = 0;
     };
     }
     */
    NSString *sickweatherGroupId = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"id"];
    if (sickweatherGroupId && [sickweatherGroupId isKindOfClass:[NSString class]] && sickweatherGroupId.length > 0)
    {
        // Then yes, we have a sickweatherId to work with
        //NSLog(@"sickweatherGroupId = %@", sickweatherGroupId);
        // Build according to follwer status
        if ([self helperUserIsFollowingGroup] == YES)
        {
            // Link into unfollow endpoint
            
            if ([self helperUserIsAdmin])
            {
                self.areYouSureYouWantToUnfollowThisGroupAndLoseAdminRightsAlert = [[UIAlertView alloc] initWithTitle:@"Unfollow & Lose Admin Rights?" message:@"Since you are an admin of this group unfollowing will remove your admin status. Is this what you want?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, Unfollow & Lose Admin Rights", nil];
                [self.areYouSureYouWantToUnfollowThisGroupAndLoseAdminRightsAlert show];
            }
            else
            {
                // Log to Flurry
                [Flurry logEvent:@"Following button tapped in a group listing"];
                
                [[self.getGroupProfileJSONObject objectForKey:@"user"] setObject:@(0) forKey:@"following"];
                self.followerCount = [[[NSNumber alloc] initWithInt:self.followerCount.intValue - 1] stringValue];
                [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
                [[SWHelper helperAppBackend] appBackendUnfollowGroupUsingArgs:@{@"group_id":sickweatherGroupId} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                        [SWHelper helperDismissFullScreenActivityIndicator];
                        //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                        //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    });
                }];
            }
        }
        else
        {
            // Link into follow endpoint
            
            // Log to Flurry
            [Flurry logEvent:@"Follow button tapped in a group listing"];
            
            [[self.getGroupProfileJSONObject objectForKey:@"user"] setObject:@(1) forKey:@"following"];
            self.followerCount = [[[NSNumber alloc] initWithInt:self.followerCount.intValue + 1] stringValue];
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendFollowGroupUsingArgs:@{@"group_id":sickweatherGroupId} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                });
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
            }];
        }
        [self.header reloadDelegateInfo];
    }
    else
    {
        //NSLog(@"Missing sickweatherGroupId, nothing to do...");
    }
    //[SWHelper helperShowAlertWithTitle:@"FollowButtonTapped"];
}

- (void)groupProfileHeaderViewClaimButtonTapped:(SWGroupProfileHeaderView *)sender
{
    NSString *sickweatherGroupId = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"id"];
    if (sickweatherGroupId && [sickweatherGroupId isKindOfClass:[NSString class]] && sickweatherGroupId.length > 0)
    {
        // Then yes, we have a sickweatherId to work with
        
        // Check current admin status
        if ([self helperUserIsAdmin])
        {
            // Nothing to do...
        }
        else
        {
            // Check follow status
            if ([self helperUserIsFollowingGroup] == NO)
            {
                // Execute follow code
                [self groupProfileHeaderViewFollowButtonTapped:sender];
            }
            
            // Link into claim endpoint
            [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
            [[SWHelper helperAppBackend] appBackendClaimGroupUsingArgs:@{@"group_id":sickweatherGroupId} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                    [SWHelper helperDismissFullScreenActivityIndicator];
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    NSString *message = [jsonResponseBody objectForKey:@"message"];
                    if ([message isKindOfClass:[NSString class]] && message.length > 0)
                    {
                        [SWHelper helperShowAlertWithTitle:@"Success" message:message];
                    }
                });
            }];
        }
    }
    else
    {
        //NSLog(@"Missing sickweatherGroupId, nothing to do...");
    }
}

- (void)groupProfileHeaderViewEllipsesButtonTapped:(SWGroupProfileHeaderView *)sender
{
    if ([self helperUserIsAdmin])
    {
        NSString *title = @"Group Actions (Admin)";
        if ([self helperUserHasGroupNotificationsOn])
        {
            self.groupLevelAdminActionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Group" otherButtonTitles:@"Share Group", @"Release Claim", @"Turn off Group Notifications", nil];
        }
        else
        {
            self.groupLevelAdminActionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Group" otherButtonTitles:@"Share Group", @"Release Claim", @"Turn on Group Notifications", nil];
        }
        [self.groupLevelAdminActionSheet showInView:self.view];
    }
    else
    {
        NSString *title = @"Group Actions";
        if ([self helperUserHasGroupNotificationsOn])
        {
            self.groupLevelStandardActionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Share Group", @"Report Inappropriate", @"Turn off Group Notifications", nil];
        }
        else
        {
            self.groupLevelStandardActionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Share Group", @"Report Inappropriate", @"Turn on Group Notifications", nil];
        }
        [self.groupLevelStandardActionSheet showInView:self.view];
    }
}

#pragma mark - Custom Getters/Setters

- (void)setServerGroupDict:(NSDictionary *)serverGroupDict
{
    _serverGroupDict = serverGroupDict;
}

#pragma mark - Target/Action

- (void)chatButtonSendTapped:(id)sender
{
    [self.view endEditing:YES];
    NSString *sickweatherGroupId = [self.serverGroupDict objectForKey:@"id"];
    if (sickweatherGroupId && [sickweatherGroupId isKindOfClass:[NSString class]] && sickweatherGroupId.length > 0 && self.chatTextView.text.length > 0)
    {
        // Then we for sure have a sickweatherGroupId already and a message to send...
        
        // Log to Flurry
        [Flurry logEvent:@"Send/Post button tapped to add a message to a group"];
        
        [SWHelper helperShowFullScreenActivityIndicatorUsingMessage:@"" backgroundColor:[UIColor clearColor] backgroundColorIsDark:NO];
        [[SWHelper helperAppBackend] appBackendSubmitMessageToGroupUsingArgs:@{@"group_id":sickweatherGroupId,@"message":self.chatTextView.text} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                self.chatTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
                [self textViewDidChange:self.chatTextView];
                [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
                [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
                [SWHelper helperDismissFullScreenActivityIndicator];
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                NSString *message = [jsonResponseBody objectForKey:@"message"];
                if ([message isKindOfClass:[NSString class]] && message.length > 0)
                {
                    if ([[jsonResponseBody objectForKey:@"status"] isKindOfClass:[NSString class]])
                    {
                        if (![[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"])
                        {
                            [SWHelper helperShowAlertWithTitle:[[jsonResponseBody objectForKey:@"status"] capitalizedString] message:message];
                        }
                    }
                }
            });
        }];
    }
}

- (void)leftBarButtonItemTapped:(id)sender
{
    if (self.webView.hidden == NO)
    {
        [self.view endEditing:YES];
        [UIView animateWithDuration:0.5 animations:^{
            [self.view removeConstraint:self.webViewApplyToShowConstraint];
            [self.view addConstraint:self.webViewApplyToHideConstraint];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.webView.hidden = YES;
        }];
    }
    else
    {
        [self.delegate groupProfileViewControllerWantsToDismiss];
    }
}

- (void)rightBarButtonItemTapped:(id)sender
{
    // Check if the user is following...
    if ([self helperUserIsFollowingGroup] == NO)
    {
        [self groupProfileHeaderViewFollowButtonTapped:nil];
    }
    
    // Log to Flurry
    [Flurry logEvent:@"Report illness from group screen tapped"];
    
    // Present self report screen
    SWReportIllnessViewController *vc = [[SWReportIllnessViewController alloc] init];
    vc.delegate = self; // Set self as destination's delegate
    vc.groupId = [self helperGetGroupId];
    vc.latestMenuServerResponse = self.latestMenuServerResponse;
    vc.serverGroupDict = self.serverGroupDict;
    [self.navigationController pushViewController:vc animated:YES];
    /*
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:^{}];
     */
}

- (void)emailButtonTouchUpInside:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:info@sickweather.com"]]];
}

- (void)phoneButtonTouchUpInside:(id)sender
{
    NSString *urlString = [@"telprompt://" stringByAppendingString:@"14439187425"];
    NSURL *url = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Helpers

- (void)helperShareGroup
{
    self.shareGroupActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share Group" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"Twitter", @"iMessage", @"Copy Link", nil];
    [self.shareGroupActionSheet showInView:self.view];
}

- (void)helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData
{
    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkDataUsingCompletionBlock:^{}];
}

- (void)helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkDataUsingCompletionBlock:(void (^)(void))completionBlock
{
    NSString *foursquareId = [self.serverGroupDict objectForKey:@"foursquare_id"];
    if (foursquareId)
    {
        //start_index - pagination start index for groups messages (defaults to 0)
        //per_page - number of groups messages per page (defaults to 10)
        self.startIndex = @"0";
        self.perPage = @"10";
        self.isCurrentlyLoadingNextPage = NO;
        self.haveLoadedAllMessages = NO;
        [[SWHelper helperAppBackend] appBackendGetGroupProfileUsingArgs:@{@"foursquare_id":foursquareId,@"start_index":self.startIndex,@"per_page":self.perPage} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                
                self.getGroupProfileJSONObject = (NSMutableDictionary *)jsonResponseBody;
                if ([self.getGroupProfileJSONObject objectForKey:@"sympathy_animation_image_url"])
                {
                    [[SWHelper helperAppBackend] appBackendGetLocallyCachedImageForStringURL:[self.getGroupProfileJSONObject objectForKey:@"sympathy_animation_image_url"] alwaysUsingCompletionHandler:^(UIImage *image, NSHTTPURLResponse *response, NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.sympathyAnimationImage = image;
                        });
                    }];
                }
                if ([self.getGroupProfileJSONObject objectForKey:@"sympathy_icon_color_image_url"])
                {
                    [[SWHelper helperAppBackend] appBackendGetLocallyCachedImageForStringURL:[self.getGroupProfileJSONObject objectForKey:@"sympathy_icon_color_image_url"] alwaysUsingCompletionHandler:^(UIImage *image, NSHTTPURLResponse *response, NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.sympathyIconColorImage = image;
                        });
                    }];
                }
                if ([self.getGroupProfileJSONObject objectForKey:@"sympathy_icon_gray_image_url"])
                {
                    [[SWHelper helperAppBackend] appBackendGetLocallyCachedImageForStringURL:[self.getGroupProfileJSONObject objectForKey:@"sympathy_icon_gray_image_url"] alwaysUsingCompletionHandler:^(UIImage *image, NSHTTPURLResponse *response, NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.sympathyIconGrayImage = image;
                        });
                    }];
                }
                /*
                 "sympathy_sponsored_message" =     (
                 {
                 "advertiser_id" = 3;
                 "bg_color_hex" = E6F5FC;
                 "bg_color_rgb" = "230,245,252";
                 brand = Clorox;
                 "more_url" = "https://www.clorox.com/";
                 keywords =             (
                 flu,
                 "common cold"
                 );
                 logo = "http://megan.sickweather.com/images/clorox.png";
                 message = "Did you know that Clorox wipes kill over 99% of germs? Including Staph, E. Coli, and Salmonella!\\nLearn more: ";
                 "more_url" = "http://sickweather.com";
                 }
                 );
                 */
                if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
                {
                    NSArray *array = [self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"];
                    if ([array isKindOfClass:[NSArray class]] && [array count] > 0)
                    {
                        NSDictionary *dict = [array objectAtIndex:0];
                        if ([dict isKindOfClass:[NSDictionary class]])
                        {
                            if ([[dict objectForKey:@"logo"] length] > 0)
                            {
                                NSString *logoURL = [dict objectForKey:@"logo"];
                                [[SWHelper helperAppBackend] appBackendGetLocallyCachedImageForStringURL:logoURL alwaysUsingCompletionHandler:^(UIImage *image, NSHTTPURLResponse *response, NSError *error) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        self.sponsoredMessageLogoImage = image;
                                        [self.collectionView reloadItemsAtIndexPaths:self.collectionView.indexPathsForVisibleItems];
                                    });
                                }];
                            }
                        }
                    }
                }
                
                //NSLog(@"requestURLAbsoluteString = %@", requestURLAbsoluteString);
                //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                /*
                 {
                 "profile": {
                 "id": "4624",
                 "foursquare_id": "sw_c81a892170fef03deec43f8f95d9bd341463860380",
                 "group_name": "Erck Family",
                 "address": null,
                 "city": null,
                 "state": null,
                 "zip": null,
                 "country": "",
                 "phone": "6126369173",
                 "lat": null,
                 "lon": null,
                 "closed": true,
                 "user_group": false,
                 "admin_owner": "138128",
                 "group_type": "",
                 "categories": null,
                 "date_created": "2016-05-21 15:53:01",
                 "date_lastmod": "2016-05-21 15:53:01",
                 "admin_count": "1",
                 "members_count": "1",
                 "reports_count": 0
                 },
                 "user": {
                 "following": true,
                 "last_seen": "1463860476",
                 "notifications": true,
                 "blocked": false,
                 "admin": true
                 },
                 "messages": [
                 
                 ]
                 }
                 */
                
                // Always make sure we have id mapped back in here
                NSMutableDictionary *mutableServerGroupDict = [self.serverGroupDict mutableCopy];
                //NSLog(@"mutableServerGroupDict = %@", mutableServerGroupDict);
                NSDictionary *addOns = [self.getGroupProfileJSONObject objectForKey:@"profile"];
                [mutableServerGroupDict addEntriesFromDictionary:addOns];
                //NSLog(@"mutableServerGroupDict = %@", mutableServerGroupDict);
                self.serverGroupDict = [mutableServerGroupDict copy];
                
                if ([[jsonResponseBody objectForKey:@"messages"] isKindOfClass:[NSArray class]])
                {
                    NSArray *messages = [jsonResponseBody objectForKey:@"messages"];
                    self.startIndex = [@([self.startIndex integerValue] + messages.count) stringValue];
                }
                [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse]; // This is the one place we run this code, after that it's just a static structure we work with
                self.setAsFollowing = NO; // We let network control now...
                self.setAsAdmin = NO; // We let network control now...
                [self helperUpdateUIBasedOnLocalGetGroupProfileJSONObject];
                completionBlock();
            });
        }];
    }
}

- (void)helperRunSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)helperSplashFlowers
{
    // INIT ALL REQUIRED UI ELEMENTS
    UIImageView *sympathySplashFlowersImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sw-sympathy_achievement-large"]];
    if (self.sympathyAnimationImage) // Use sponsored animation image if one is loaded...
    {
        sympathySplashFlowersImageView = [[UIImageView alloc] initWithImage:self.sympathyAnimationImage];
    }
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    sympathySplashFlowersImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:sympathySplashFlowersImageView];
    
    // CONFIG
    sympathySplashFlowersImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // INIT AUTO LAYOUT VIEWS DICT
    NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
    [viewsDictionary setObject:sympathySplashFlowersImageView forKey:@"sympathySplashFlowersImageView"];
    
    // LAYOUT
    
    // Layout VIEWS
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:sympathySplashFlowersImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:200];
    NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint constraintWithItem:sympathySplashFlowersImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:sympathySplashFlowersImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:200];
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:sympathySplashFlowersImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-100];
    NSArray *constraints = @[widthConstraint, centerXConstraint, heightConstraint, topConstraint];
    [self.view addConstraints:constraints];
    [self.view layoutIfNeeded];
    [self helperRunSpinAnimationOnView:sympathySplashFlowersImageView duration:1.0 rotations:1 repeat:1];
    topConstraint.constant = -self.view.bounds.size.height;
    //widthConstraint.constant = 400;
    [UIView animateWithDuration:2.0 delay:0.0 options:0 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        widthConstraint.constant = 0;
        [UIView animateWithDuration:0.5 animations:^{
            sympathySplashFlowersImageView.alpha = 0;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [sympathySplashFlowersImageView removeFromSuperview];
            [self.view removeConstraints:constraints];
        }];
    }];
}

- (void)addBannerWithMessage:(NSString *)message
{
    // INIT ALL REQUIRED UI ELEMENTS
    SWMessageBannerView *messageBannerView = [[SWMessageBannerView alloc] init];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    messageBannerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY
    [self.view addSubview:messageBannerView];
    
    // CONFIG
    
    // Config MESSAGE BANNER VIEW
    messageBannerView.delegate = self;
    [messageBannerView setMessage:message];
    
    // INIT AUTO LAYOUT VIEWS DICT
    NSMutableDictionary *viewsDictionary = [NSMutableDictionary new];
    [viewsDictionary setObject:messageBannerView forKey:@"messageBannerView"];
    
    // LAYOUT
    
    // Layout MESSAGE BANNER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[messageBannerView]|"] options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[messageBannerView(44)]"] options:0 metrics:0 views:viewsDictionary]];
}

- (BOOL)helperUserHasGroupNotificationsOn
{
    BOOL answer = NO;
    NSNumber *notificationsAreOn = [[self.getGroupProfileJSONObject objectForKey:@"user"] objectForKey:@"notifications"];
    //NSLog(@"notificationsAreOn = %@", notificationsAreOn);
    if ([notificationsAreOn boolValue] == YES)
    {
        answer = YES;
    }
    return answer;
}

- (BOOL)helperUserIsAdmin
{
    if (self.setAsAdmin == YES)
    {
        return YES;
    }
    else
    {
        BOOL answer = NO;
        NSNumber *isAdminTrueFalse = [[self.getGroupProfileJSONObject objectForKey:@"user"] objectForKey:@"admin"];
        //NSLog(@"isAdminTrueFalse = %@", isAdminTrueFalse);
        if ([isAdminTrueFalse boolValue] == YES)
        {
            answer = YES;
        }
        return answer;
    }
}

- (BOOL)helperUserIsFollowingGroup
{
    if (self.setAsFollowing == YES)
    {
        return YES;
    }
    else
    {
        BOOL answer = NO;
        NSNumber *followingTrueFalse = [[self.getGroupProfileJSONObject objectForKey:@"user"] objectForKey:@"following"];
        //NSLog(@"followingTrueFalse = %@", followingTrueFalse);
        if ([followingTrueFalse boolValue] == YES)
        {
            answer = YES;
        }
        return answer;
    }
}

- (NSString *)helperGetGroupId
{
	NSDictionary *profile = [self.getGroupProfileJSONObject objectForKey:@"profile"];
	if (profile != nil && ![profile isEqual:[NSNull null]] && ![[profile objectForKey:@"id"] isEqual:[NSNull null]]){
		 return [profile objectForKey:@"id"];
	}
	return @"";
}

- (NSString *)helperGetGroupFoursquareId
{
    //NSLog(@"self.getGroupProfileJSONObject = %@", self.getGroupProfileJSONObject);
    return [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"foursquare_id"];
}

- (UIEdgeInsets)helperGetDefaultCollectionViewContentInset
{
    // top, left, bottom, right
    return UIEdgeInsetsMake(0, DEFAULT_LEFT_RIGHT_PADDING_INSET, DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT_PLUS_PADDING, DEFAULT_LEFT_RIGHT_PADDING_INSET);
}

- (UIEdgeInsets)helperGetDefaultCollectionViewScrollIndicatorInsets
{
    return UIEdgeInsetsMake(0, 0, DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT_PLUS_PADDING, 0);
}

- (NSAttributedString *)helperGetMessageAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *messages = [self.getGroupProfileJSONObject objectForKey:@"messages"];
    NSAttributedString *attrStrToReturn = [[NSAttributedString alloc] initWithString:@"" attributes:@{}];
    if ([messages isKindOfClass:[NSArray class]])
    {
        if (messages.count >= indexPath.row + 1)
        {
            NSDictionary *messageDict = [messages objectAtIndex:indexPath.row];
            //NSLog(@"messageDict = %@", messageDict);
            if ([messageDict isKindOfClass:[NSDictionary class]])
            {
                NSString *messageString = [messageDict objectForKey:@"message"];
                if ([messageString isKindOfClass:[NSString class]])
                {
                    attrStrToReturn = [[NSAttributedString alloc] initWithString:messageString attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:10]}];
                }
            }
        }
    }
    return attrStrToReturn;
}

- (NSAttributedString *)helperGetUsernameAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *messages = [self.getGroupProfileJSONObject objectForKey:@"messages"];
    NSAttributedString *attrStrToReturn = [[NSAttributedString alloc] initWithString:@"" attributes:@{}];
    if ([messages isKindOfClass:[NSArray class]])
    {
        if (messages.count >= indexPath.row + 1)
        {
            NSDictionary *messageDict = [messages objectAtIndex:indexPath.row];
            //NSLog(@"messageDict = %@", messageDict);
            if ([messageDict isKindOfClass:[NSDictionary class]])
            {
                NSString *usernameString = [messageDict objectForKey:@"username"];
                if ([usernameString isKindOfClass:[NSString class]])
                {
                    attrStrToReturn = [[NSAttributedString alloc] initWithString:usernameString attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
                }
            }
        }
    }
    return attrStrToReturn;
}

- (NSAttributedString *)helperGetMinsAgoAttributedStringForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *messages = [self.getGroupProfileJSONObject objectForKey:@"messages"];
    NSAttributedString *attrStrToReturn = [[NSAttributedString alloc] initWithString:@"" attributes:@{}];
    if ([messages isKindOfClass:[NSArray class]])
    {
        if (messages.count >= indexPath.row + 1)
        {
            NSDictionary *messageDict = [messages objectAtIndex:indexPath.row];
            //NSLog(@"messageDict = %@", messageDict);
            if ([messageDict isKindOfClass:[NSDictionary class]])
            {
                NSString *usernameString = [messageDict objectForKey:@"username"];
                if ([usernameString isKindOfClass:[NSString class]])
                {
                    attrStrToReturn = [[NSAttributedString alloc] initWithString:usernameString attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
                }
            }
        }
    }
    return attrStrToReturn;
}

- (NSString *)helperGetMinsAgoStringForTimestampString:(NSString *)timestampString
{
    NSDate *now = [[NSDate alloc] init];
    NSTimeInterval nowTime = [now timeIntervalSince1970];
    CGFloat mathFactor = -1;
    NSString *wordFactor = @"ago";
    if (timestampString.doubleValue > nowTime)
    {
        //NSLog(@"Future time given");
        return @"just now";
    }
    NSString *finalAnswer = @"";
    NSDate *creationTimeAsDate = [NSDate dateWithTimeIntervalSince1970:timestampString.doubleValue];
    NSTimeInterval secondsAgoDiff = [creationTimeAsDate timeIntervalSinceNow]*mathFactor;
    NSTimeInterval minutesAgoDiff = secondsAgoDiff/60.0;
    NSTimeInterval hoursAgoDiff = minutesAgoDiff/60.0;
    NSTimeInterval daysAgoDiff = hoursAgoDiff/24.0;
    NSTimeInterval yearsAgoDiff = daysAgoDiff/365.0;
    if (secondsAgoDiff < 60)
    {
        NSString *seconds = @"seconds";
        if (@(secondsAgoDiff).integerValue == 1)
        {
            seconds = @"second";
        }
        finalAnswer = [[NSString alloc] initWithFormat:@"%ld %@ %@", (long)@(secondsAgoDiff).integerValue, seconds, wordFactor];
    }
    else if (minutesAgoDiff < 60)
    {
        NSString *minutes = @"minutes";
        if (@(minutesAgoDiff).integerValue == 1)
        {
            minutes = @"minute";
        }
        finalAnswer = [[NSString alloc] initWithFormat:@"%ld %@ %@", (long)@(minutesAgoDiff).integerValue, minutes, wordFactor];
    }
    else if (hoursAgoDiff < 24)
    {
        NSString *hours = @"hours";
        if (@(hoursAgoDiff).integerValue == 1)
        {
            hours = @"hour";
        }
        finalAnswer = [[NSString alloc] initWithFormat:@"%ld %@ %@", (long)@(hoursAgoDiff).integerValue, hours, wordFactor];
    }
    else if (daysAgoDiff < 365)
    {
        NSString *days = @"days";
        if (@(daysAgoDiff).integerValue == 1)
        {
            days = @"day";
        }
        finalAnswer = [[NSString alloc] initWithFormat:@"%ld %@ %@", (long)@(daysAgoDiff).integerValue, days, wordFactor];
    }
    else
    {
        NSString *years = @"years";
        if (@(yearsAgoDiff).integerValue == 1)
        {
            years = @"year";
        }
        finalAnswer = [[NSString alloc] initWithFormat:@"%ld %@ %@", (long)@(yearsAgoDiff).integerValue, years, wordFactor];
    }
    return finalAnswer;
}

- (NSAttributedString *)helperGetMinsAgoAttributedStringForTimestampString:(NSString *)finalAnswer
{
    return [[NSAttributedString alloc] initWithString:finalAnswer attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:10]}];
}

- (void)helperUpdateUIBasedOnLocalGetGroupProfileJSONObject
{
    NSString *adminCount = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"admin_count"];
    self.adminCount = @"0";
    if ([adminCount isKindOfClass:[NSString class]] && adminCount.length > 0)
    {
        self.adminCount = adminCount;
    }
    NSString *followersCount = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"members_count"];
    self.followerCount = @"0";
    if ([followersCount isKindOfClass:[NSString class]] && followersCount.length > 0)
    {
        self.followerCount = followersCount;
    }
    NSString *reportCount = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"reports_count"];
    self.reportCount = @"0";
    if ([reportCount isKindOfClass:[NSString class]] && reportCount.length > 0)
    {
        self.reportCount = reportCount;
    }
    [self.header reloadDelegateInfo];
    [self.collectionView reloadData];
}

- (CGFloat)helperCollectionViewWidth
{
    return self.view.bounds.size.width-(DEFAULT_LEFT_RIGHT_PADDING_INSET*2);
}

- (UIImage *)helperGetProfilePicImageForIndexPath:(NSIndexPath *)indexPath
{
    UIImage *imageToReturn = [UIImage imageNamed:@"avatar-empty"];
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_PROFILE_PIC_CELL_TYPE])
        {
            NSDictionary *messageDict = [cellData objectForKey:@"message_dict"];
            NSString *stringURL = [cellData objectForKey:@"data"];
            if ([stringURL length] > 0)
            {
                // We have an URL to work with...
                UIImage *networkImage = [self.imageForStringURL objectForKey:stringURL];
                if (networkImage)
                {
                    imageToReturn = networkImage;
                }
                else if ([[messageDict objectForKey:@"user_id"] isEqualToString:[SWHelper helperCurrentUser].unique] && [SWHelper helperCurrentUser].profilePicThumbnailImageData)
                {
                    imageToReturn = [UIImage imageWithData:[SWHelper helperCurrentUser].profilePicThumbnailImageData];
                }
                else
                {
                    if ([self.stringURLCurrentlyLoading containsObject:stringURL])
                    {
                        //NSLog(@"Skip, work in progress for stringURL %@", stringURL);
                    }
                    else
                    {
                        //NSLog(@"Start, fetch for stringURL %@", stringURL);
                        [self.stringURLCurrentlyLoading addObject:stringURL]; // Log as currently loading
                        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
                        [[session dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            //NSLog(@"Background Thread...");
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //NSLog(@"Main Thread...");
                                [self.stringURLCurrentlyLoading removeObject:stringURL]; // Remove from currently loading
                                UIImage *image = [UIImage imageWithData:data];
                                if (image)
                                {
                                    [self.imageForStringURL setObject:image forKey:stringURL]; // Load to cache
                                    //NSLog(@"Loaded image to cache for stringURL %@", stringURL);
                                }
                                [self.collectionView reloadItemsAtIndexPaths:self.collectionView.indexPathsForVisibleItems];
                            });
                        }] resume];
                    }
                }
            }
        }
    }
    return imageToReturn;
}

- (UIImage *)helperGetSympathyImageForIndexPath:(NSIndexPath *)indexPath
{
    UIImage *imageToReturn = [UIImage imageNamed:@"sw-sympathy_achievement-gray"];
    if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_animation_image_url"] length] > 0)
    {
        imageToReturn = [UIImage new];
    }
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_SYMPATHY_CELL_TYPE])
        {
            NSString *stringURL = [[cellData objectForKey:@"data"] objectForKey:@"icon"];
            if ([stringURL length] > 0)
            {
                // We have an URL to work with...
                UIImage *networkImage = [self.imageForStringURL objectForKey:stringURL];
                if (networkImage)
                {
                    imageToReturn = networkImage;
                }
                else
                {
                    if ([self.stringURLCurrentlyLoading containsObject:stringURL])
                    {
                        //NSLog(@"Skip, work in progress for stringURL %@", stringURL);
                    }
                    else
                    {
                        //NSLog(@"Start, fetch for stringURL %@", stringURL);
                        [self.stringURLCurrentlyLoading addObject:stringURL]; // Log as currently loading
                        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
                        [[session dataTaskWithURL:[NSURL URLWithString:stringURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                            //NSLog(@"Background Thread...");
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //NSLog(@"Main Thread...");
                                [self.stringURLCurrentlyLoading removeObject:stringURL]; // Remove from currently loading
                                UIImage *image = [UIImage imageWithData:data];
                                if (image)
                                {
                                    [self.imageForStringURL setObject:image forKey:stringURL]; // Load to cache
                                    //NSLog(@"Loaded image to cache for stringURL %@", stringURL);
                                }
                                [self.collectionView reloadItemsAtIndexPaths:self.collectionView.indexPathsForVisibleItems];
                            });
                        }] resume];
                    }
                }
            }
        }
    }
    return imageToReturn;
}

- (void)helperBannerViewShowUsingMessage:(NSString *)message
{
    [self.bannerView updateMessage:message];
    if ([self.view.constraints containsObject:self.bannerViewApplyToHideConstraint])
    {
        // Then we need to show...
        [UIView animateWithDuration:0.5 animations:^{
            [self.view removeConstraint:self.bannerViewApplyToHideConstraint];
            [self.view addConstraint:self.bannerViewApplyToShowConstraint];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            // DONE
        }];
    }
}

- (void)helperBannerViewHide
{
    if ([self.view.constraints containsObject:self.bannerViewApplyToShowConstraint])
    {
        // Then we need to hide...
        [UIView animateWithDuration:0.5 animations:^{
            [self.view removeConstraint:self.bannerViewApplyToShowConstraint];
            [self.view addConstraint:self.bannerViewApplyToHideConstraint];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            // DONE
        }];
    }
}

- (void)updateCVDataItemAtIndexPathForNewUserSympathy:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [self.cvDataObjects objectAtIndex:indexPath.item];
    if (dict)
    {
        //NSLog(@"dict = %@", dict);
        NSMutableDictionary *sympathy = [dict objectForKey:@"data"];
        if ([sympathy isKindOfClass:[NSMutableDictionary class]])
        {
            // Bump the count by one
            if ([[sympathy objectForKey:@"count"] isKindOfClass:[NSNumber class]])
            {
                NSNumber *currentCount = [sympathy objectForKey:@"count"];
                NSNumber *newCount = @(currentCount.integerValue + 1);
                [sympathy setObject:newCount forKey:@"count"];
            }
            
            // Set the icon image to the colored version
            if ([[sympathy objectForKey:@"icon"] isKindOfClass:[NSString class]])
            {
                if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_icon_color_image_url"] isKindOfClass:[NSString class]])
                {
                    [sympathy setObject:[self.getGroupProfileJSONObject objectForKey:@"sympathy_icon_color_image_url"] forKey:@"icon"];
                }
            }
            
            // Add user to the list of users that have added sympathy
            SWUserCDM *currentUser = [SWUserCDM currentlyLoggedInUser];
            if ([[sympathy objectForKey:@"users"] isKindOfClass:[NSMutableDictionary class]])
            {
                NSMutableDictionary *users = [sympathy objectForKey:@"users"];
                [users setObject:currentUser.username forKey:currentUser.unique];
            }
            else
            {
                NSMutableDictionary *users = [NSMutableDictionary new];
                [users setObject:currentUser.username forKey:currentUser.unique];
                [sympathy setObject:users forKey:@"users"];
            }
        }
    }
}

#pragma mark - SWReportIllnessViewControllerDelegate

- (void)didDismissViewControllerWithNewReports:(NSArray *)reports sender:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        // Shawty said
        if ([reports count] > 1)
        {
            // Plural
            [self addBannerWithMessage:@"Thanks for sharing your reports!"];
        }
        else
        {
            // Singular
            [self addBannerWithMessage:@"Thanks for sharing your report!"];
        }
        [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
    }];
}

- (void)didDismissViewControllerWithoutAnyReports
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
    }];
}

#pragma mark - SWGroupProfileCollectionViewCellForMessageSpacerDelegate

- (void)groupProfileCollectionViewCellForMessageSpacerMessageActionButtonTapped:(SWGroupProfileCollectionViewCellForMessageSpacer *)sender
{
    self.currentMessageId = @"";
    self.currentMessageDict = @{};
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    NSDictionary *data = [self.cvDataObjects objectAtIndex:indexPath.item];
    NSDictionary *message = [data objectForKey:@"data"];
    if (message && [message isKindOfClass:[NSDictionary class]])
    {
        //NSLog(@"[SWUserCDM currentlyLoggedInUser].unique = %@", [SWUserCDM currentlyLoggedInUser].unique);
        //NSLog(@"message = %@", message);
        NSString *messageId = [message objectForKey:@"message_id"];
        if (messageId && [messageId isKindOfClass:[NSString class]])
        {
            self.currentMessageId = messageId;
            self.currentMessageDict = message;
        }
        NSString *userId = [message objectForKey:@"user_id"];
        if (userId && [userId isKindOfClass:[NSString class]])
        {
            if ([userId isEqualToString:[SWUserCDM currentlyLoggedInUser].unique])
            {
                // Is the user's own message
                self.messageLevelActionSheet = [[UIActionSheet alloc] initWithTitle:@"Message Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete Message", nil];
            }
            else
            {
                // Is another user's message
                if ([self helperUserIsAdmin])
                {
                    // User is admin, they can delete or block
                    self.messageLevelActionSheet = [[UIActionSheet alloc] initWithTitle:@"Admin Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete Message", @"Block Follower", @"Add Administrator", nil];
                }
                else
                {
                    // Just report
                    self.messageLevelActionSheet = [[UIActionSheet alloc] initWithTitle:@"Message Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report User", nil];
                }
            }
            [self.messageLevelActionSheet showInView:self.view];
        }
    }
}

#pragma mark - SWGroupProfileCollectionViewCellForSympathyDelegate

- (void)groupProfileCollectionViewCellForSympathyButtonTapped:(SWGroupProfileCollectionViewCellForSympathy *)sender
{
    // Log to Flurry
    [Flurry logEvent:@"Sympathy Button Tapped"];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    NSMutableDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    NSString *swCurrentUserId = [SWHelper helperCurrentUser].unique;
    NSArray *sympathyUsers = [[cellData objectForKey:@"data"] objectForKey:@"users"];
    BOOL matchFound = NO;
    for (id sympathyUserSWId in sympathyUsers)
    {
        //NSString *swUserUsername = [sympathyUsers valueForKey:sympathyUserSWId];
        NSString *stringSWId = @"";
        if ([sympathyUserSWId isKindOfClass:[NSString class]])
        {
            // String
            stringSWId = sympathyUserSWId;
        }
        if ([sympathyUserSWId isKindOfClass:[NSNumber class]])
        {
            // Number
            stringSWId = [sympathyUserSWId stringValue];
        }
        //NSLog(@"myValue = %@", swUserUsername);
        //NSLog(@"sympathyUser = %@", sympathyUserSWId);
        if ([swCurrentUserId isEqualToString:stringSWId])
        {
            // Match found, skip web service call, do show flowers though...
            matchFound = YES;
        }
    }
    if (matchFound)
    {
        // They have already added sympathy...
        [self helperSplashFlowers];
        if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
        {
            if ([[[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"] isKindOfClass:[NSString class]])
            {
                NSString *brand = [[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"];
                [Flurry logEvent:@"Sponsored Sympathy Tapped Subsequent" withParameters:@{@"Brand": brand}];
            }
        }
        
        // Only for dev...
        //[self helperBannerViewShowUsingMessage:@"You sent sympathy and an offer for Clorox disinfecting products to help the recipient reduce the spread of germs – A virtual get well soon!"];
    }
    else
    {
        NSString *swGroupId = [[self.getGroupProfileJSONObject objectForKey:@"profile"] objectForKey:@"id"];
        NSString *swMessageId = [sender.messageDict objectForKey:@"message_id"];
        //NSLog(@"swGroupId = %@", swGroupId);
        //NSLog(@"swMessageId = %@", swMessageId);
        if (swGroupId && swMessageId)
        {
            [self helperSplashFlowers];
            [sender groupProfileCollectionViewCellForSympathyBumpCount];
            [sender groupProfileCollectionViewCellForSympathySetIconImage:self.sympathyIconColorImage];
            NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
            [self updateCVDataItemAtIndexPathForNewUserSympathy:indexPath]; // Prevents us from having to hit the network, we just mimic locally...
            [self.collectionView reloadItemsAtIndexPaths:@[indexPath]]; // Update UI based on locally edited data^ that backs how UI is built...
            if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
            {
                if ([[[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"] isKindOfClass:[NSString class]])
                {
                    NSString *brand = [[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"];
                    [Flurry logEvent:@"Sponsored Sympathy Tapped Initial" withParameters:@{@"Brand": brand}];
                }
            }
            [[SWHelper helperAppBackend] appBackendAddSympathyToGroupMessageUsingArgs:@{@"group_id":swGroupId, @"message_id":swMessageId} completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //NSLog(@"responseBodyAsString = %@", responseBodyAsString);
                    if ([[jsonResponseBody objectForKey:@"message"] isKindOfClass:[NSString class]])
                    {
                        [self helperBannerViewShowUsingMessage:[jsonResponseBody objectForKey:@"message"]];
                    }
                });
            }];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_HEADER_CELL_TYPE])
        {
            size = CGSizeMake([self helperCollectionViewWidth], staticHeaderImageViewDefaultHeight);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_PROFILE_PIC_CELL_TYPE])
        {
            size = CGSizeMake(40, 40);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_USERNAME_CELL_TYPE])
        {
            NSAttributedString *username = [cellData objectForKey:@"data"];
            size = CGSizeMake([username size].width + 04, 20);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_MESSAGE_TIME_CELL_TYPE])
        {
            NSAttributedString *created = [cellData objectForKey:@"data"];
            size = CGSizeMake([created size].width + 04, 20);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_KEYWORD_CELL_TYPE])
        {
            NSAttributedString *keyword = [cellData objectForKey:@"data"];
            size = CGSizeMake([keyword size].width + 20, 20);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_MESSAGE_TEXT_CELL_TYPE])
        {
            NSAttributedString *message = [cellData objectForKey:@"data"];
            if ([message length] == 0)
            {
                size = CGSizeMake([self helperCollectionViewWidth], 0);
            }
            else
            {
                // The minus 10 makes sure we get the height and line wrapping we want, the plus 20 is standard margin for posts with text
                size = CGSizeMake([self helperCollectionViewWidth], [message heightForWidth:[self helperCollectionViewWidth]-10] + 20);
            }
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_SYMPATHY_CELL_TYPE])
        {
            //NSDictionary *sympathy = [cellData objectForKey:@"data"];
            size = CGSizeMake([self helperCollectionViewWidth], SYMPATHY_CELL_HEIGHT);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_SPONSORED_MESSAGE_CELL_TYPE])
        {
            CGSize messageSize = CGSizeZero;
            NSArray *sponsoredMessage = [cellData objectForKey:@"data"];
            //NSLog(@"sponsoredMessage = %@", sponsoredMessage);
            if ([sponsoredMessage isKindOfClass:[NSArray class]])
            {
                if ([[sponsoredMessage lastObject] isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *sponsoredMessageDict = [sponsoredMessage lastObject];
                    //NSLog(@"sponsoredMessageDict = %@", sponsoredMessageDict);
                    if ([sponsoredMessageDict objectForKey:@"message"])
                    {
                        messageSize = [SWGroupProfileCollectionViewCellForSponsoredMessage sizeForSponsoredMessage:[sponsoredMessageDict objectForKey:@"message"] givenWidth:[self helperCollectionViewWidth]];
                        //NSLog(@"messageSize = %@", NSStringFromCGSize(messageSize));
                    }
                }
            }
            //NSLog(@"messageSize = %@", NSStringFromCGSize(messageSize));
            size = CGSizeMake([self helperCollectionViewWidth], messageSize.height + 80);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_MESSAGE_SPACER_CELL_TYPE])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 1);
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_EMPTY_MESSAGE_AREA_CELL_TYPE])
        {
            size = CGSizeMake([self helperCollectionViewWidth], 120);
        }
    }
    
    return size;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    
    NSDictionary *cellData = [self.cvDataObjects objectAtIndex:indexPath.item];
    if ([cellData isKindOfClass:[NSDictionary class]])
    {
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_HEADER_CELL_TYPE])
        {
            if (!self.header)
            {
                self.header = [[SWGroupProfileHeaderView alloc] init];
                self.header.delegate = self;
            }
            SWGroupProfileCollectionViewCellForCustomView *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForCustomView" forIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForCustomViewSetView:self.header left:@(-DEFAULT_LEFT_RIGHT_PADDING_INSET) right:@(-DEFAULT_LEFT_RIGHT_PADDING_INSET) top:@0 bottom:@0];
            //myCell.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_PROFILE_PIC_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForProfilePic *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForProfilePic" forIndexPath:indexPath];
            UIImage *profilePicImage = [self helperGetProfilePicImageForIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForProfilePicSetImage:profilePicImage];
            //myCell.backgroundColor = [SWColor color:[UIColor orangeColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_USERNAME_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForUsername *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForUsername" forIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForUsernameSetUsernameAttributedString:[cellData objectForKey:@"data"]];
            //myCell.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_MESSAGE_TIME_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForMessageTime *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForMessageTime" forIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForMessageTimeSetMessageTimeAttributedString:[cellData objectForKey:@"data"]];
            //myCell.backgroundColor = [SWColor color:[UIColor greenColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_KEYWORD_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForIllnessTag *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForIllnessTag" forIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForIllnessTagSetNameAttributedString:[cellData objectForKey:@"data"]];
            //myCell.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_MESSAGE_TEXT_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForMessageText *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForMessageText" forIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForMessageTextSetMessageTextAttributedString:[cellData objectForKey:@"data"]];
            //myCell.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_SYMPATHY_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForSympathy *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForSympathy" forIndexPath:indexPath];
            UIImage *iconImage = [self helperGetSympathyImageForIndexPath:indexPath];
            myCell.delegate = self;
            myCell.messageDict = [cellData objectForKey:@"messageDict"];
            [myCell groupProfileCollectionViewCellForSympathySetIconImage:iconImage];
            [myCell groupProfileCollectionViewCellForSympathySetSympathyDict:[cellData objectForKey:@"data"]];
            [myCell groupProfileCollectionViewCellForSympathyUpdateDefaultInsetsLeft:@(0) right:@(0) top:@(0) bottom:@(0)];
            //myCell.backgroundColor = [SWColor color:[UIColor brownColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_SPONSORED_MESSAGE_CELL_TYPE])
        {
            NSDictionary *sponsorshipDict;
            if ([[cellData objectForKey:@"data"] isKindOfClass:[NSArray class]])
            {
                NSArray *sponsorshipArray = [cellData objectForKey:@"data"];
                if ([[sponsorshipArray lastObject] isKindOfClass:[NSDictionary class]])
                {
                    sponsorshipDict = [sponsorshipArray lastObject];
                }
            }
            SWGroupProfileCollectionViewCellForSponsoredMessage *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForSponsoredMessage" forIndexPath:indexPath];
            [myCell updateLeftInset:@(-DEFAULT_LEFT_RIGHT_PADDING_INSET) rightInset:@(-DEFAULT_LEFT_RIGHT_PADDING_INSET)];
            [myCell updateTopInset:@(0) bottomInset:@(-DEFAULT_TOP_BOTTOM_PADDING_INSET)];
            if ([sponsorshipDict isKindOfClass:[NSDictionary class]])
            {
                [myCell updateForServerSponsorshipDict:sponsorshipDict];
                [myCell updateSponsorProfilePicUsingImage:self.sponsoredMessageLogoImage];
            }
            myCell.delegate = self;
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_MESSAGE_SPACER_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForMessageSpacer *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForMessageSpacer" forIndexPath:indexPath];
            myCell.delegate = self;
            [myCell groupProfileCollectionViewCellForMessageSpacerSetMarginLeft:@(-DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT) right:@(-DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT)];
            //myCell.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
            cell = myCell;
        }
        if ([[cellData objectForKey:CELL_TYPE_KEY] isEqualToString:IS_EMPTY_MESSAGE_AREA_CELL_TYPE])
        {
            SWGroupProfileCollectionViewCellForEmptyMessageArea *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForEmptyMessageArea" forIndexPath:indexPath];
            [myCell groupProfileCollectionViewCellForEmptyMessageAreaUpdateDefaultInsetsLeft:@(-DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT) right:@(-DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT) top:@0 bottom:@0];
            myCell.delegate = self;
            //myCell.backgroundColor = [SWColor color:[UIColor blueColor] usingOpacity:0.5];
            cell = myCell;
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // If you need to use the touched cell, you can retrieve it like so
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    //NSLog(@"touched cell %@ at indexPath %@", cell, indexPath);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cvDataObjects.count;
}

#pragma mark - Life Cycle Methods

- (void)helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse
{
    // Get messages (may be nil mind you)
    NSArray *messages = [self.getGroupProfileJSONObject objectForKey:@"messages"];
    
    // This is where we order things for the collection view's UI!!!!
    self.cvDataObjects = [NSMutableArray new];
    
    // Always start with header
    [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_HEADER_CELL_TYPE,@"data":@""}]; // We don't neead to pass any data for header, it can get data itself
    
    // Add sponsored message if needed...
    /*
     "sympathy_sponsored_message" =     (
         {
             "advertiser_id" = 3;
             "bg_color_hex" = E6F5FC;
             "bg_color_rgb" = "230,245,252";
             brand = Clorox;
             "more_url" = "https://www.clorox.com/";
             keywords =             (
                flu,
                "common cold"
             );
             logo = "http://megan.sickweather.com/images/clorox.png";
             message = "Did you know that Clorox wipes kill over 99% of germs? Including Staph, E. Coli, and Salmonella!\\nLearn more: ";
             "more_url" = "http://sickweather.com";
         }
     );
     */
    if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
    {
        [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_SPONSORED_MESSAGE_CELL_TYPE,@"data":[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"]}];
        if (!self.haveLoggedSponsoredMessageServedForThisViewDidLoad)
        {
            if ([[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] isKindOfClass:[NSArray class]] && [[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] count] > 0)
            {
                if ([[[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"] isKindOfClass:[NSString class]])
                {
                    self.haveLoggedSponsoredMessageServedForThisViewDidLoad = YES;
                    NSString *brand = [[[self.getGroupProfileJSONObject objectForKey:@"sympathy_sponsored_message"] objectAtIndex:0] objectForKey:@"brand"];
                    [Flurry logEvent:@"Sponsored Message Served" withParameters:@{@"Brand": brand}];
                }
            }
        }
    }
    
    // Then add in message info...
    for (NSDictionary *messageDict in messages)
    {
        //NSLog(@"messageDict = %@", messageDict);
        /*
         messageDict = {
         admin = true;
         keywords =     (
         "man flu"
         );
         message = "";
         "message_id" = 274;
         sympathy = {
            count = 0;
            icon = "https://mobilesvc.sickweather.com/images/sw-sympathy_achievement-gray.png";
         };
         "time_created" = 1454541578;
         "user_id" = 138128;
         "user_photo" = "https://s3.amazonaws.com/sw-profile-pics/138128_thumb.jpg";
         username = tritonaaron7;
         "utc_created" = "2016-02-03 18:19:38";
         }
         */
        // Add spacer cell before every message
        [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_MESSAGE_SPACER_CELL_TYPE,@"data":messageDict}];
        
        // Profile pic setup (data = NSString)
        NSString *userPhotoURLString = [messageDict objectForKey:@"user_photo"];
        if ([userPhotoURLString isKindOfClass:[NSString class]])
        {
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_PROFILE_PIC_CELL_TYPE,@"data":userPhotoURLString,@"message_dict":messageDict}];
        }
        else
        {
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_PROFILE_PIC_CELL_TYPE,@"data":@""}]; // Load default
        }
        
        // Username setup (data = NSAttributedString)
        NSString *username = [messageDict objectForKey:@"username"];
        NSNumber *adminBool = [messageDict objectForKey:@"admin"];
        NSDictionary *usernameAttributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226],NSFontAttributeName:[SWFont fontStandardFontOfSize:12]};
        if ([username isKindOfClass:[NSString class]])
        {
            NSMutableAttributedString *data = [[NSMutableAttributedString alloc] initWithString:username attributes:usernameAttributes];
            if ([adminBool boolValue] == YES)
            {
                [data appendAttributedString:[[NSAttributedString alloc] initWithString:@"  (" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226], NSFontAttributeName:[SWFont fontStandardFontOfSize:12]}]];
                [data appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString awesomeIcon:FaUser] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226], NSFontAttributeName:[SWFont fontAwesomeOfSize:12]}]];
                [data appendAttributedString:[[NSAttributedString alloc] initWithString:@" admin)" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226], NSFontAttributeName:[SWFont fontStandardFontOfSize:12]}]];
            }
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_USERNAME_CELL_TYPE,@"data":data}];
        }
        else
        {
            NSAttributedString *data = [[NSAttributedString alloc] initWithString:@"" attributes:usernameAttributes];
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_USERNAME_CELL_TYPE,@"data":data}];
        }
        
        // Created setup (data = NSAttributedString)
        NSDictionary *createdAttributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:12]};
        NSString *created = [messageDict objectForKey:@"time_created"];
        if ([created isKindOfClass:[NSNumber class]])
        {
            //NSLog(@"Now according to server: %@", created);
            NSNumber *number = (NSNumber *)created;
            created = number.stringValue;
            //NSLog(@"Now according to server: %@", created);
        }
        if ([created isKindOfClass:[NSString class]])
        {
            //NSLog(@"Now according to server: %@", created);
            NSDate *now = [[NSDate alloc] init];
            NSTimeInterval nowTime = [now timeIntervalSince1970];
            if (created.doubleValue > nowTime)
            {
                //NSLog(@"Future time given");
                //created = @"1453517342";
                //created = [NSString stringWithFormat:@"Future time given: %@", created];
                created = [self helperGetMinsAgoStringForTimestampString:created];
                //NSLog(@"created = %@", created);
            }
            else
            {
                created = [self helperGetMinsAgoStringForTimestampString:created];
            }
            NSAttributedString *data = [[NSAttributedString alloc] initWithString:created attributes:createdAttributes];
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_MESSAGE_TIME_CELL_TYPE,@"data":data}];
        }
        else
        {
            NSAttributedString *data = [[NSAttributedString alloc] initWithString:@"" attributes:createdAttributes];
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_MESSAGE_TIME_CELL_TYPE,@"data":data}];
        }
        
        // Keyword setup (data = NSAttributedString)
        NSDictionary *keywordAttributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherBlue41x171x226],NSFontAttributeName:[SWFont fontStandardFontOfSize:12]};
        NSArray *keywords = [messageDict objectForKey:@"keywords"];
        if ([keywords isKindOfClass:[NSArray class]])
        {
            //keywords = @[@"flu",@"cold",@"sore throat",@"norovirus"];
            for (NSString *keyword in keywords)
            {
                if ([keyword isKindOfClass:[NSString class]])
                {
                    NSAttributedString *data = [[NSAttributedString alloc] initWithString:keyword attributes:keywordAttributes];
                    [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_KEYWORD_CELL_TYPE,@"data":data}];
                }
            }
        }
        
        // Message setup (data = NSAttributedString)
        NSDictionary *messageAttributes = @{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGrayDark35x31x32],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]};
        NSString *message = [messageDict objectForKey:@"message"];
        if ([message isKindOfClass:[NSString class]])
        {
            NSAttributedString *data = [[NSAttributedString alloc] initWithString:message attributes:messageAttributes];
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_MESSAGE_TEXT_CELL_TYPE,@"data":data}];
        }
        else
        {
            NSAttributedString *data = [[NSAttributedString alloc] initWithString:@"" attributes:messageAttributes];
            [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_MESSAGE_TEXT_CELL_TYPE,@"data":data}]; // Load default
        }
        
        // Sympathy setup (data = NSDictionary)
        NSMutableDictionary *sympathy = [messageDict objectForKey:@"sympathy"];
        [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_SYMPATHY_CELL_TYPE,@"data":sympathy,@"messageDict":messageDict}];
    }
    
    // Check and see if we actually worked with any messages above...
    if (messages.count == 0)
    {
        // Then throw a cell on to the list to represent this state...
        [self.cvDataObjects addObject:@{CELL_TYPE_KEY:IS_EMPTY_MESSAGE_AREA_CELL_TYPE,@"data":@""}];
    }
}

- (void)viewDidLoad
{
    // INVOKE SUPER
    [super viewDidLoad];
    
    // INIT DEFAULTS
    self.flowLayout = [[SWCollectionViewFlowLayout alloc] init];
    self.flowLayout.minimumLineSpacing = 9;
    self.startIndex = @"0";
    self.perPage = @"10";
    self.reportCount = @"0";
    self.followerCount = @"0";
    self.adminCount = @"0";
    self.imageForStringURL = [[NSMutableDictionary alloc] init];
    self.stringURLCurrentlyLoading = [[NSMutableArray alloc] init];
    
    // Add tap (for dismissing keyboard)
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewWasTapped:)]];
    [self registerForKeyboardNotifications];
    
    // INIT ALL REQUIRED UI ELEMENTS
    self.staticHeaderImageView = [[UIImageView alloc] init];
    self.staticHeaderImageOverlayView = [[UIView alloc] init];
    self.collectionView = [[SWGroupProfileCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    self.chatBarContainerView = [[UIView alloc] init];
    self.chatBarContainerViewInnerBackgroundView = [[UIView alloc] init];
    self.chatTextView = [[UITextView alloc] init];
    self.chatButtonSend = [[UIButton alloc] init];
    self.webView = [[WKWebView alloc] init];
    self.bannerView = [[SWBannerView alloc] init];
    
    // INIT AUTO LAYOUT VIEWS DICT
    self.viewsDictionary = [NSMutableDictionary new];
    [self.viewsDictionary setObject:self.staticHeaderImageView forKey:@"staticHeaderImageView"];
    [self.viewsDictionary setObject:self.staticHeaderImageOverlayView forKey:@"staticHeaderImageOverlayView"];
    [self.viewsDictionary setObject:self.collectionView forKey:@"collectionView"];
    [self.viewsDictionary setObject:self.chatBarContainerView forKey:@"chatBarContainerView"];
    [self.viewsDictionary setObject:self.chatBarContainerViewInnerBackgroundView forKey:@"chatBarContainerViewInnerBackgroundView"];
    [self.viewsDictionary setObject:self.chatTextView forKey:@"chatTextView"];
    [self.viewsDictionary setObject:self.chatButtonSend forKey:@"chatButtonSend"];
    [self.viewsDictionary setObject:self.webView forKey:@"webView"];
    [self.viewsDictionary setObject:self.bannerView forKey:@"bannerView"];
    
    // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
    self.staticHeaderImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.staticHeaderImageOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatBarContainerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatBarContainerViewInnerBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatTextView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chatButtonSend.translatesAutoresizingMaskIntoConstraints = NO;
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    self.bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // ESTABLISH VIEW HIERARCHY (ORDER MATTER)
    [self.view addSubview:self.staticHeaderImageView];
    [self.view addSubview:self.staticHeaderImageOverlayView];
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.chatBarContainerView];
    [self.chatBarContainerView addSubview:self.chatBarContainerViewInnerBackgroundView];
    [self.chatBarContainerView addSubview:self.chatTextView];
    [self.chatBarContainerView addSubview:self.chatButtonSend];
    [self.view addSubview:self.webView];
    [self.view addSubview:self.bannerView];
    
    // LAYOUT
    
    // Layout STATIC HEADER IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[staticHeaderImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[staticHeaderImageView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.staticHeaderImageViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.staticHeaderImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:staticHeaderImageViewDefaultHeight];
    [self.view addConstraint:self.staticHeaderImageViewHeightConstraint];
    
    // Layout STATIC HEADER OVERLAY IMAGE VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[staticHeaderImageOverlayView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[staticHeaderImageOverlayView]"] options:0 metrics:0 views:self.viewsDictionary]];
    self.staticHeaderImageOverlayViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.staticHeaderImageOverlayView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:staticHeaderImageViewDefaultHeight];
    [self.view addConstraint:self.staticHeaderImageOverlayViewHeightConstraint];
    
    // Layout COLLECTION VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[collectionView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CHAT BAR CONTAINER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[chatBarContainerView]|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[chatBarContainerView]"] options:0 metrics:0 views:self.viewsDictionary]];
	if (@available(iOS 11.0, *)) {
		UILayoutGuide * guide = self.view.safeAreaLayoutGuide;
		self.spaceFromBottomChatBarContainerViewConstraint = [NSLayoutConstraint constraintWithItem:self.chatBarContainerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
		[self.chatBarContainerViewInnerBackgroundView.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
	} else {
		self.spaceFromBottomChatBarContainerViewConstraint = [NSLayoutConstraint constraintWithItem:self.chatBarContainerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-49];
	}
	[self.view addConstraint:self.spaceFromBottomChatBarContainerViewConstraint];
    
    // Layout CHAT BAR CONTAINER VIEW INNER BACKGROUND VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[chatBarContainerViewInnerBackgroundView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(1)-[chatBarContainerViewInnerBackgroundView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout CHAT TEXT VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[chatTextView]-[chatButtonSend]"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(10)-[chatTextView]-(10)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    self.chatTextViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.chatTextView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:DEFAULT_TEXT_VIEW_HEIGHT_FOR_CHAT];
    [self.view addConstraint:self.chatTextViewHeightConstraint];
    
    // Layout CHAT BUTTON SEND
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[chatButtonSend]-|"] options:0 metrics:0 views:self.viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[chatButtonSend]-|"] options:0 metrics:0 views:self.viewsDictionary]];
    
    // Layout WEB VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[webView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
    [self.view addConstraint:heightConstraint];
    self.webViewApplyToHideConstraint = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    [self.view addConstraint:self.webViewApplyToHideConstraint];
    self.webViewApplyToShowConstraint = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    
    // Layout BANNER VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[bannerView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
    self.bannerViewApplyToHideConstraint = [NSLayoutConstraint constraintWithItem:self.bannerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    self.bannerViewApplyToShowConstraint = [NSLayoutConstraint constraintWithItem:self.bannerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    [self.view addConstraint:self.bannerViewApplyToHideConstraint];
    
    // CONFIG
    
    // Config VIEW
    self.view.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
    
    // Config NAVIGATION CONTROLLER
    self.navigationController.navigationBar.translucent = NO;
    
    // CREATE AND SET INTO NAVIGATION ITEM OUR CUSTOM SICKWEATHER FONT TITLE VIEW
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [SWFont fontDuepuntozeroRegularFontOfSize:26];
    titleLabel.textColor = [SWColor colorSickweatherBlue41x171x226];
    titleLabel.text = @"sickweather";
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    /*
    // Config LEFT BAR BUTTON ITEM
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close-icon"] landscapeImagePhone:[UIImage imageNamed:@"close-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
    */
    // Config RIGHT BAR BUTTON ITEM
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"report-icon"] landscapeImagePhone:[UIImage imageNamed:@"report-icon-landscape"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemTapped:)];
    
    // Config STATIC HEADER IMAGE VIEW
    self.staticHeaderImageView.image = [UIImage imageNamed:@"profile-map-background"];
    self.staticHeaderImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.staticHeaderImageView.clipsToBounds = YES;
    //self.staticHeaderImageView.alpha = staticHeaderImageViewDefaultAlpha;
    
    // Config STATIC HEADER IMAGE OVERLAY VIEW
    self.staticHeaderImageOverlayView.backgroundColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
    self.staticHeaderImageOverlayView.alpha = 0.9;
    //self.staticHeaderImageOverlayView.alpha = staticHeaderImageOverlayViewDefaultAlpha;
    
    // Config COLLECTION VIEW
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.allowsSelection = NO;
    self.collectionView.contentInset = [self helperGetDefaultCollectionViewContentInset];
    self.collectionView.scrollIndicatorInsets = [self helperGetDefaultCollectionViewScrollIndicatorInsets];
    self.collectionView.scrollsToTop = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForCustomView class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForCustomView"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForIllnessTag class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForIllnessTag"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForProfilePic class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForProfilePic"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForUsername class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForUsername"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForMessageTime class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForMessageTime"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForMessageText class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForMessageText"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForSympathy class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForSympathy"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForSponsoredMessage class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForSponsoredMessage"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForMessageSpacer class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForMessageSpacer"];
    [self.collectionView registerClass:[SWGroupProfileCollectionViewCellForEmptyMessageArea class] forCellWithReuseIdentifier:@"SWGroupProfileCollectionViewCellForEmptyMessageArea"];
    
    // Config CHAT BAR CONTAINER VIEW
    self.chatBarContainerView.backgroundColor = [SWColor colorChatBoxGrayAccent];
    
    // Config CHAT BAR CONTAINER VIEW INNER BACKGROUND VIEW
    self.chatBarContainerViewInnerBackgroundView.backgroundColor = [SWColor colorChatBoxGray];
    
    // Config CHAT TEXT VIEW
    self.chatTextView.delegate = self;
    self.chatTextView.layer.cornerRadius = 5;
    self.chatTextView.layer.borderColor = [SWColor colorChatBoxGrayAccent].CGColor;
    self.chatTextView.layer.borderWidth = 1;
    self.chatTextView.scrollsToTop = NO;
    
    // Config CHAT BUTTON SEND
    [self.chatButtonSend setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Send" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:16]}] forState:UIControlStateNormal];
    [self.chatButtonSend addTarget:self action:@selector(chatButtonSendTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Config WEB VIEW
    self.webView.scrollView.delegate = self;
    self.webView.hidden = YES;
    
    // Config BANNER VIEW
    self.bannerView.delegate = self;
    
    // Config data...
    [self helperUpdateUIUsingServerGroupDictAsBasisUsingFreshNetworkData];
    [self helperUpdateUIBasedOnLocalGetGroupProfileJSONObject];
    [self helperRebuildCVDataObjectsBasedOnLatestLocalServerResponse];
    
    // COLOR FOR DEVELOPMENT PURPOSES
    //self.collectionView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
}

@end
