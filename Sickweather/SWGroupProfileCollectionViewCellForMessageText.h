//
//  SWGroupProfileCollectionViewCellForMessageText.h
//  Sickweather
//
//  Created by John Erck on 1/22/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWGroupProfileCollectionViewCellForMessageText : UICollectionViewCell
- (void)groupProfileCollectionViewCellForMessageTextSetMessageTextAttributedString:(NSAttributedString *)text;
@end
