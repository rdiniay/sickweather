//
//  SWTabBarController.m
//  Sickweather
//
//  Created by Shan Shafiq on 1/24/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWTabBarController.h"

@interface SWTabBarController ()

@end

@implementation SWTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
	NSUInteger indexOfTab = [[tabBar items] indexOfObject:item];
	NSString *tag = @"";
	switch (indexOfTab) {
		case 0:
			tag = @"Home Tab tapped";
			break;
		case 1:
			tag = @"Family Tab tapped";
			break;
		case 2:
			tag = @"Report Tab tapped";
			break;
		case 3:
			tag = @"Alerts Tab tapped";
			break;
		case 4:
			tag = @"Groups Tab tapped";
			break;
	}
	[SWHelper logFlurryEventsWithTag:tag];
}

@end
