//
//  SWSavedLocationCDM.h
//  Sickweather
//
//  Created by John Erck on 5/15/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SWSavedLocationCDM : NSManagedObject

@property (nonatomic, retain) NSData * placemarkObjectData;
@property (nonatomic, retain) NSNumber * savedLocationId;

@end
