//
//  SWAlertLogCDM.m
//  Sickweather
//
//  Created by John Erck on 4/14/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import "SWAlertLogCDM.h"


@implementation SWAlertLogCDM

@dynamic regionObjectData;
@dynamic reportDictionaryData;
@dynamic reportId;
@dynamic hasTriggeredLocalNotification;

@end
