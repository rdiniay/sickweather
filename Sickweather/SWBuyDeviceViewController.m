//
//  SWBuyDeviceViewController.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 12/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWBuyDeviceViewController.h"

@interface SWBuyDeviceViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property BOOL reload;
@end

@implementation SWBuyDeviceViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.reload = NO;
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.activityIndicator.center = CGPointMake(self.view.center.x, self.view.center.y - 64);
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.view addSubview:self.activityIndicator];
    
    NSURL *url = [[NSURL alloc] initWithString:self.urlString];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    [self.webView loadRequest:urlRequest];
    self.webView.delegate = self;
    self.navigationItem.title = self.viewTitle;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.reload){
        [self.webView reload];
    }
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)leftBarButtonItemTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicator startAnimating];
    self.webView.hidden = YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
    self.webView.hidden = NO;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    NSLog(@"%@",[error localizedDescription]);
}

@end
