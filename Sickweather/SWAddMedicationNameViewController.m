//
//  SWAddMedicationNameViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 1/31/18.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWAddMedicationNameViewController.h"

@interface SWAddMedicationNameViewController ()
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *medications;
@property (nonatomic, strong) NSString *typeText;
@end

@implementation SWAddMedicationNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = @"Medication";
	//INIT
	self.medications = [[NSMutableArray alloc] init];
	//INIT Config
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
	self.typeText = @"Search for medications, or add your own";
	self.searchController.searchResultsUpdater = self;
	self.searchController.searchBar.delegate = self;
	self.tableView.tableHeaderView = self.searchController.searchBar;
	self.definesPresentationContext = true;
	self.searchController.dimsBackgroundDuringPresentation = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark Search Medication
-(void)searchMedicationWith:(NSString*)name{
	[[SWHelper helperAppBackend] appBackendSearchForMedicationUsingArgs:name usingCompletionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		  dispatch_async(dispatch_get_main_queue(), ^{
			  if ([jsonResponseBody isKindOfClass:[NSDictionary class]] && [[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) {
				  NSArray *candidates = [jsonResponseBody objectForKey:@"candidates"];
				  if (candidates && [candidates isKindOfClass:[NSArray class]]) {
					  self.medications = [NSMutableArray arrayWithArray:candidates];
				  }
			  }else {
				  [self.medications removeAllObjects];
			  }
			   [self.tableView reloadData];
		  });
	}];
}


#pragma mark - Target/Action

-(void)hideKeyboard{
	[self.view endEditing:YES];
}

- (void)leftBarButtonItemTapped:(id)sender
{
	[self hideKeyboard];
	[self.navigationController popViewControllerAnimated:true];
}

#pragma -mark UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.medications.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil){
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}
	if (indexPath.row == 0 && [self.typeText isEqualToString:@"Search for medications, or add your own"]) {
		cell.textLabel.textColor = [UIColor lightGrayColor];
	}else {
		cell.textLabel.textColor = [UIColor blackColor];
	}
	cell.textLabel.text = (indexPath.row == 0) ? self.typeText : [self.medications objectAtIndex:indexPath.row - 1] ;
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	NSString *name = (indexPath.row == 0) ?  self.typeText : [self.medications objectAtIndex:indexPath.row - 1];
	if (![name isEqualToString:@"Search for medications, or add your own"]){
		[self.delegate didDismissWithSuccessfulMedication:name];
		[self.navigationController popViewControllerAnimated:true];
	}
}

#pragma mark - Search Controller Delegate
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
	NSString * name = searchController.searchBar.text;
	[self searchMedicationWith:name];
	if (![name isEqualToString:@""]){
		self.typeText = name;
	}
	[self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if ([searchText isEqualToString:@""]) {
		self.typeText = @"Search for medications, or add your own";
		[self.medications removeAllObjects];
		[self.tableView reloadData];
	}
}
@end
