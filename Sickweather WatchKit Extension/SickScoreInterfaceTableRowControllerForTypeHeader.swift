//
//  SickScoreInterfaceTableRowControllerForTypeHeader.swift
//  Sickweather
//
//  Created by John Erck on 2/20/15.
//  Copyright (c) 2015 Sickweather, LLC. All rights reserved.
//

import UIKit
import WatchKit

class SickScoreInterfaceTableRowControllerForTypeHeader: NSObject {
    weak var presentingController: WKInterfaceController? = nil
    @IBOutlet weak var sickScoreTableViewHeaderRowGroupContainer: WKInterfaceGroup!
    @IBOutlet weak var sickScoreBackgroundGroup: WKInterfaceGroup!
    @IBOutlet weak var sickScoreIntegerValueLabel: WKInterfaceLabel!
    @IBOutlet weak var sickScoreBrandTMLabel: WKInterfaceLabel!
    @IBOutlet weak var bySickweatherLabel: WKInterfaceLabel!
    @IBOutlet weak var locationNameLabel: WKInterfaceLabel!
    @IBOutlet weak var riskLevelLabel: WKInterfaceLabel!
    @IBOutlet weak var sickScoreMeterInterfaceGroup: WKInterfaceGroup!
    @IBOutlet weak var sickScoreMeterInterfaceImage: WKInterfaceImage!
    @IBOutlet weak var noLocationGroup: WKInterfaceGroup!
    @IBOutlet weak var locationRiskGroup: WKInterfaceGroup!
    @IBOutlet weak var topIllnessesGroup: WKInterfaceGroup!
    @IBOutlet weak var washHandsButton: WKInterfaceButton!
    @IBAction func washHandsButtonWasTapped() {
        presentingController?.presentController(withName: "washHandsInterfaceController", context: nil)
    }
}
