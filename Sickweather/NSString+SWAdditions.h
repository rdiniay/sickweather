//
//  NSString+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 12/7/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SWAdditions)

- (NSString *)urlencode;

@end
