//
//  SWHomePageView.m
//  Sickweather
//
//  Created by John Erck on 7/25/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SWHomePageView.h"
#import "SWHomeTrendingIllnessRecordView.h"
#import "SWHomeForecastChartView.h"
#import "NSString+FontAwesome.h"

@interface SWHomePageView()

// NETWORK DATA
@property (strong, nonatomic) NSDictionary *latestSickScoreResponse;
@property (strong, nonatomic) NSDictionary *latestCovidScoreResponse;
@property (strong, nonatomic) NSDictionary *lastSelectedRowDict;

// VIEW DATA
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) MKMapView *myMapView;
@property (strong, nonatomic) UILabel *sickScoreIntBackgroundCircleLabel;
@property (strong, nonatomic) UILabel *sickScoreIntForegroundLabel;
@property (strong, nonatomic) UILabel *sickScoreRiskLabel;
@property (strong, nonatomic) UILabel *sickScoreBrandLabel;
@property (strong, nonatomic) UILabel *sickScoreCityLabel;
@property (strong, nonatomic) UILabel *sickScoreTapForMoreInfoLabel;
@property (strong, nonatomic) UIPageControl *sickScorePageControl;
@property (strong, nonatomic) UIActivityIndicatorView *sickScoreActivityIndicatorView;
@property (strong, nonatomic) UIView *noLocationView;
@property (strong, nonatomic) UILabel *noLocationTitleLabel;
@property (strong, nonatomic) UILabel *noLocationDescriptionLabel;
@property (strong, nonatomic) UIButton *noLocationTurnLocationOnButton;
@property (strong, nonatomic) UIButton *noLocationSearchManuallyButton;
@property (strong, nonatomic) UIButton *toggleLogoButton;
@property (strong, nonatomic) UILabel *trendingTitleLabel;
@property (strong, nonatomic) SWHomeTrendingIllnessRecordView *illnessRow0;
@property (strong, nonatomic) SWHomeTrendingIllnessRecordView *illnessRow1;
@property (strong, nonatomic) SWHomeTrendingIllnessRecordView *illnessRow2;
@property (strong, nonatomic) SWHomeForecastChartView *forecastChart0;
@property (strong, nonatomic) SWHomeForecastChartView *forecastChart1;
@property (strong, nonatomic) NSLayoutConstraint *forecastChart1PushDownConstraint;
@property (strong, nonatomic) NSLayoutConstraint *sickScoreIntForegroundHeightConstraint;
@end

@implementation SWHomePageView

#pragma mark - Public Methods

- (void)updateForSickScoreDict:(NSDictionary *)dict
{
    //NSLog(@"CURRENT STATE API dict = %@", dict);
    if (![dict isKindOfClass:[NSDictionary class]] || [self.currentObjectForKey isEqual:@"sickscore"])
    {
        return;
    }
    /*
    // NEW DICT (TO PASS OFF TO MEGAN)
    NSMutableDictionary *newDict = [dict mutableCopy];
    NSMutableArray *myArray = [NSMutableArray new];
    for (NSNumber *illnessId in [dict objectForKey:@"top_illnesses"])
    {
        //NSLog(@"illnessId = %@", illnessId);
        [myArray addObject:@{@"illness_id":[NSString stringWithFormat:@"%@", illnessId],@"illness_name":[NSString stringWithFormat:@"Flu %@", illnessId],@"illness_short_code":[NSString stringWithFormat:@"%@", illnessId],@"number_of_reports":@"23",@"trending":@"up",@"trending_percent":@"43"}];
    }
    [newDict setObject:myArray forKey:@"illnesses"];
    dict = [newDict copy];
    */
    //NSLog(@"FUTURE STATE API DICT: dict = %@", dict);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (jsonData)
    {
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSLog(@"jsonString = %@", jsonString);
    }
    
    // SET COLORS
    UIColor *cityTextColor = [UIColor whiteColor];
    
    // LOG FOR LATER REFERENCE IF NEEDED
    self.latestSickScoreResponse = dict;
    
    // CALCULATE THREAT COLOR AND STATEMENT
    NSString *objectForKey = @"sickscore";
    self.currentObjectForKey = objectForKey;
    UIColor *currentThreatColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
    NSString *currentThreatStatement = @"VERY LOW RISK";
    if ([[self.latestSickScoreResponse objectForKey:objectForKey] integerValue] < 25)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
        currentThreatStatement = @"VERY LOW RISK";
    }
    else if ([[self.latestSickScoreResponse objectForKey:objectForKey] integerValue] < 50)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideYellow249x237x11];
        currentThreatStatement = @"LOW RISK";
        cityTextColor = [UIColor blackColor];
    }
    else if ([[self.latestSickScoreResponse objectForKey:objectForKey] integerValue] < 75)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideOrange252x111x6];
        currentThreatStatement = @"MEDIUM RISK";
    }
    else if ([[self.latestSickScoreResponse objectForKey:objectForKey] integerValue] >= 75)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideRed255x1x0];
        currentThreatStatement = @"HIGH RISK";
    }
    
    [self updateForCurrentThreatStatement:currentThreatStatement
                       currentThreatColor:currentThreatColor
                        currentDictionary:dict
                         objectForKey:objectForKey
                          cityTextColor: cityTextColor];
}

- (void)updateForCovidScoreDict:(NSDictionary *)dict
{
    //NSLog(@"CURRENT STATE API dict = %@", dict);
    if (![dict isKindOfClass:[NSDictionary class]] || [self.currentObjectForKey isEqual:@"cvscore"])
    {
        return;
    }
    /*
    // NEW DICT (TO PASS OFF TO MEGAN)
    NSMutableDictionary *newDict = [dict mutableCopy];
    NSMutableArray *myArray = [NSMutableArray new];
    for (NSNumber *illnessId in [dict objectForKey:@"top_illnesses"])
    {
        //NSLog(@"illnessId = %@", illnessId);
        [myArray addObject:@{@"illness_id":[NSString stringWithFormat:@"%@", illnessId],@"illness_name":[NSString stringWithFormat:@"Flu %@", illnessId],@"illness_short_code":[NSString stringWithFormat:@"%@", illnessId],@"number_of_reports":@"23",@"trending":@"up",@"trending_percent":@"43"}];
    }
    [newDict setObject:myArray forKey:@"illnesses"];
    dict = [newDict copy];
    */
    //NSLog(@"FUTURE STATE API DICT: dict = %@", dict);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (jsonData)
    {
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSLog(@"jsonString = %@", jsonString);
    }
    
    // SET COLORS
    UIColor *cityTextColor = [UIColor whiteColor];
    
    // LOG FOR LATER REFERENCE IF NEEDED
    self.latestCovidScoreResponse = dict;
    
    // CALCULATE THREAT COLOR AND STATEMENT
    NSString *objectForKey = @"cvscore";
    self.currentObjectForKey = objectForKey;
    UIColor *currentThreatColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
    NSString *currentThreatStatement = @"VERY LOW RISK";
    if ([[self.latestCovidScoreResponse objectForKey:objectForKey] integerValue] < 25)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
        currentThreatStatement = @"VERY LOW RISK";
    }
    else if ([[self.latestCovidScoreResponse objectForKey:objectForKey] integerValue] < 50)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideYellow249x237x11];
        currentThreatStatement = @"LOW RISK";
        cityTextColor = [UIColor blackColor];
    }
    else if ([[self.latestCovidScoreResponse objectForKey:objectForKey] integerValue] < 75)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideOrange252x111x6];
        currentThreatStatement = @"MEDIUM RISK";
    }
    else if ([[self.latestCovidScoreResponse objectForKey:objectForKey] integerValue] >= 75)
    {
        currentThreatColor = [SWColor colorSickweatherStyleGuideRed255x1x0];
        currentThreatStatement = @"HIGH RISK";
    }
    
    [self updateForCurrentThreatStatement:currentThreatStatement
                       currentThreatColor:currentThreatColor
                        currentDictionary:dict
                         objectForKey:objectForKey
                          cityTextColor: cityTextColor];
}

-(void)updateForCurrentThreatStatement: (NSString *) currentThreatStatement
                currentThreatColor: (UIColor *) currentThreatColor
                 currentDictionary: (NSDictionary *) dict
                  objectForKey: (NSString *) objectForKey
                   cityTextColor: (UIColor *) cityTextColor {
        BOOL isCovidScore = [objectForKey  isEqual: @"cvscore"];
        //UPDATE TOGGLE BRAND LOGO BUTTON

        NSString * logoName = !isCovidScore ? @"-covid" : @"";
        NSString * toggleName = [NSString stringWithFormat:@"map-marker%@-public", logoName];
        [self.toggleLogoButton setImage:[UIImage imageNamed: toggleName] forState: UIControlStateNormal];
    
        // UPDATE CIRCLE USING UPDATED THREAT COLOR
        self.sickScoreIntBackgroundCircleLabel.layer.borderColor = currentThreatColor.CGColor;
        
        // UPDATE MAP OVERLAY VIEW FOR RISK COLOR
        self.mapOverlayView.backgroundColor = currentThreatColor;
        self.mapOverlayView.alpha = 0.4;
        self.myMapView.alpha = 1.0;
        
        // CENTER MAP ON THIS LOCATION
        if ([self latestSickScoreResponseLocation])
        {
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([self latestSickScoreResponseLocation].coordinate, 10000, 10000);
            [self.myMapView setRegion:region animated:YES];
        }
        
        // SET THE BIG INT VALUE
        if ([dict objectForKey:objectForKey])
        {
            //CGFloat size = [objectForKey  isEqual: @"cvscore"] ? 10.0 : 80.0;
            CGFloat height = isCovidScore ? 150 : 200;
            UIColor * color = isCovidScore ? [UIColor clearColor] : [UIColor blackColor];
            self.sickScoreIntForegroundLabel.attributedText = [[NSAttributedString alloc] initWithString:[[dict objectForKey:objectForKey] stringValue] attributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:80]}];
            [UIView animateWithDuration:0.3 animations:^{
                self.sickScoreIntForegroundHeightConstraint.constant = height;
                [self layoutIfNeeded];
            }];
            [self.sickScoreActivityIndicatorView stopAnimating];
        }
        
        // SET THE RISK STATEMENT
        self.sickScoreRiskLabel.attributedText = [[NSAttributedString alloc] initWithString:currentThreatStatement attributes:@{NSForegroundColorAttributeName:currentThreatColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
        
        // UPDATE THE PAGE CONTROL TINT COLORS TO MATCH THREAT LEVEL
        self.sickScorePageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        self.sickScorePageControl.currentPageIndicatorTintColor = currentThreatColor;
        
        // SET THE BRAND LABEL
        NSString * title = isCovidScore ? @"COVIDScore" : @"SickScore®";
        UIColor * sickScoreBrandFontColor = isCovidScore ? currentThreatColor : [UIColor blackColor];
        self.sickScoreBrandLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:sickScoreBrandFontColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:12]}];
        
        // SET THE CITY LABEL
        if ([[dict objectForKey:@"placemark"] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *myMutable = [dict mutableCopy];
            // IF administrativeArea NOT SET, LOOK TO administrative_area IF SET
            if (![[myMutable objectForKey:@"placemark"] objectForKey:@"administrativeArea"])
            {
                if ([[myMutable objectForKey:@"placemark"] objectForKey:@"administrative_area"])
                {
                    [[myMutable objectForKey:@"placemark"] setObject:[[myMutable objectForKey:@"placemark"] objectForKey:@"administrative_area"] forKey:@"administrativeArea"];
                }
            }
            dict = myMutable;
        }
        
        if ([[[dict objectForKey:@"placemark"] objectForKey:@"locality"] isKindOfClass:[NSString class]])
        {
            // FORMAT CITY NAME
            NSString *cityName = [[dict objectForKey:@"placemark"] objectForKey:@"locality"];
            NSString *administrativeArea = [[dict objectForKey:@"placemark"] objectForKey:@"administrativeArea"];
            if ([[[dict objectForKey:@"placemark"] objectForKey:@"administrativeArea"] isKindOfClass:[NSString class]]) {
                if (![cityName isEqualToString:administrativeArea]) {
                    cityName = [NSString stringWithFormat:@"%@, %@",cityName,[[dict objectForKey:@"placemark"] objectForKey:@"administrativeArea"]];
                }
            }
    //        NSLog(@"cityName = %@", cityName);
            if([[dict objectForKey:@"placemark_is_current_location"]  isEqual: @"yes"]) {
                [[NSUserDefaults standardUserDefaults]setObject:cityName forKey:[SWConstants cityDefaultKey]];
            }
            cityName = [cityName uppercaseString];
            
            // CHECK FOR THE CURRENT LOCATION FLAG
            
            self.sickScoreCityLabel.attributedText = [[NSAttributedString alloc] initWithString:cityName attributes:@{NSForegroundColorAttributeName:cityTextColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:22]}];
            if ([[dict objectForKey:@"placemark_is_current_location"] isEqualToString:@"yes"])
            {
                NSMutableAttributedString *mixedFontTitle = [[NSMutableAttributedString alloc] initWithString:@"" attributes:nil];
                [mixedFontTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", cityName] attributes:@{NSForegroundColorAttributeName:cityTextColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:22]}]];
                [mixedFontTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString awesomeIcon:FaLocationArrow] attributes:@{NSForegroundColorAttributeName:cityTextColor,NSFontAttributeName:[SWFont fontAwesomeOfSize:22]}]];
                self.sickScoreCityLabel.attributedText = mixedFontTitle;
            }
        }
        
        // SET THE INFO LABEL
        self.sickScoreTapForMoreInfoLabel.attributedText = [[NSAttributedString alloc] initWithString:@"(TAP TO VIEW MAP)" attributes:@{NSForegroundColorAttributeName:cityTextColor,NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:10]}];
        
        // UPDATE TOP ILLNESSES
        if ([[dict objectForKey:@"top_illnesses"] isKindOfClass:[NSArray class]])
        {
            NSString * illnessObject = isCovidScore ? @"trending_keywords" : @"illnesses";
            NSArray *illnesses = [dict objectForKey:illnessObject];
            if ([illnesses count] > 0)
            {
                // CONFIG 0 RECORD
                [self.illnessRow0 updateForIllnessInfoDict:[illnesses objectAtIndex:0]];
            }
            if ([illnesses count] > 1)
            {
                // CONFIG 1 RECORD
                [self.illnessRow1 updateForIllnessInfoDict:[illnesses objectAtIndex:1]];
            }
            if ([illnesses count] > 2)
            {
                // CONFIG 2 RECORD
                [self.illnessRow2 updateForIllnessInfoDict:[illnesses objectAtIndex:2]];
            }
        }
}

- (NSDictionary *)getLastSelectedRowDataDict
{
    return self.lastSelectedRowDict;
}

- (NSDictionary *)getDataDict
{
    BOOL isCovidScore = [self.currentObjectForKey isEqual:@"cvscore"] && self.latestCovidScoreResponse;
    NSDictionary * dict = isCovidScore ? self.latestCovidScoreResponse : self.latestSickScoreResponse;
    return dict;
}

- (void)setupAsNoLocationUI
{
    self.noLocationView.hidden = NO;
    self.toggleLogoButton.hidden = YES;
}

- (void)setupAsHasLocationUI
{
    self.noLocationView.hidden = YES;
    self.toggleLogoButton.hidden = NO;
    self.sickScoreTapForMoreInfoLabel.hidden = NO;
}

- (BOOL)isCovidScore {
    return [self.currentObjectForKey isEqual:@"cvscore"] && self.latestCovidScoreResponse;
}

- (CLLocation *)getLocation
{
    CLLocation *location = nil;
    NSDictionary * dict = self.isCovidScore ? self.latestCovidScoreResponse : self.latestSickScoreResponse;
    if ([[dict objectForKey:@"placemark"] isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *placemark = [dict objectForKey:@"placemark"];
        if ([[placemark objectForKey:@"latitude"] isKindOfClass:[NSNumber class]] && [[placemark objectForKey:@"longitude"] isKindOfClass:[NSNumber class]])
        {
            NSNumber *lat = [placemark objectForKey:@"latitude"];
            NSNumber *lon = [placemark objectForKey:@"longitude"];
            location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
        }
        else if ([[placemark objectForKey:@"latitude"] isKindOfClass:[NSString class]] && [[placemark objectForKey:@"longitude"] isKindOfClass:[NSString class]])
        {
            NSString *lat = [placemark objectForKey:@"latitude"];
            NSString *lon = [placemark objectForKey:@"longitude"];
            location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
        }
    }
    return location;
}

#pragma mark - Target/Action Methods

- (void)mapOverlayViewTapped:(id)sender
{
	[SWHelper logFlurryEventsWithTag:@"Tap to view map tapped"];
    [self.delegate homePageViewWantsToLoadTrendingMap:self];
}

- (void)rowTapped:(id)sender
{
    NSLog(@"rowTapped = %@", sender);
    NSDictionary * dict = self.isCovidScore ? self.latestCovidScoreResponse : self.latestSickScoreResponse;
    if ([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        UITapGestureRecognizer *tap = sender;
        if ([[dict objectForKey:@"top_illnesses"] isKindOfClass:[NSArray class]])
        {
            NSString * illnessesObjectName = self.isCovidScore ? @"trending_keywords" : @"illnesses";
            NSArray *illnesses = [dict objectForKey:illnessesObjectName];
            if ([illnesses count] > 0 && [tap.view isEqual:self.illnessRow0])
            {
                self.lastSelectedRowDict = [illnesses objectAtIndex:0];
                [self.delegate homePageViewTrendingIllnessRecordTapped:self];
            }
            if ([illnesses count] > 1 && [tap.view isEqual:self.illnessRow1])
            {
                self.lastSelectedRowDict = [illnesses objectAtIndex:1];
                [self.delegate homePageViewTrendingIllnessRecordTapped:self];
            }
            if ([illnesses count] > 2 && [tap.view isEqual:self.illnessRow2])
            {
                self.lastSelectedRowDict = [illnesses objectAtIndex:2];
                [self.delegate homePageViewTrendingIllnessRecordTapped:self];
            }
        }
    }
}

- (void)noLocationTurnLocationOnButtonTapped:(id)sender
{
    [self.delegate homePageViewTurnLocationOnButtonTapped:self];
}

- (void)noLocationSearchManuallyButtonTapped:(id)sender
{
    [self.delegate homePageViewManuallySearchForLocationButtonTapped:self];
}

#pragma mark - Helpers

- (CLLocation *)latestSickScoreResponseLocation
{
    CLLocation *location;
    NSDictionary * dict = self.isCovidScore ? self.latestCovidScoreResponse : self.latestSickScoreResponse;
    
    if ([dict objectForKey:@"placemark"])
    {
        if ([[dict objectForKey:@"placemark"] objectForKey:@"latitude"])
        {
            NSString *lat = [[dict objectForKey:@"placemark"] objectForKey:@"latitude"];
            if ([[dict objectForKey:@"placemark"] objectForKey:@"longitude"])
            {
                NSString *lon = [[dict objectForKey:@"placemark"] objectForKey:@"longitude"];
                location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
            }
        }
    }
    return location;
}

#pragma mark - Life Cycle Methods

- (id)init
{
    self = [super init];
    if (self)
    {
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.myMapView = [[MKMapView alloc] init];
        self.mapOverlayView = [[UIView alloc] init];
        self.sickScoreIntBackgroundCircleLabel = [[UILabel alloc] init];
        self.sickScoreIntForegroundLabel = [[UILabel alloc] init];
        self.sickScoreRiskLabel = [[UILabel alloc] init];
        self.sickScoreBrandLabel = [[UILabel alloc] init];
        self.sickScoreCityLabel = [[UILabel alloc] init];
        self.sickScoreTapForMoreInfoLabel = [[UILabel alloc] init];
        self.sickScoreActivityIndicatorView = [[UIActivityIndicatorView alloc] init];
        self.noLocationView = [[UIView alloc] init];
        self.noLocationTitleLabel = [[UILabel alloc] init];
        self.noLocationDescriptionLabel = [[UILabel alloc] init];
        self.noLocationTurnLocationOnButton = [[UIButton alloc] init];
        self.noLocationSearchManuallyButton = [[UIButton alloc] init];
        self.trendingTitleLabel = [[UILabel alloc] init];
        self.illnessRow0 = [[SWHomeTrendingIllnessRecordView alloc] init];
        self.illnessRow1 = [[SWHomeTrendingIllnessRecordView alloc] init];
        self.illnessRow2 = [[SWHomeTrendingIllnessRecordView alloc] init];
        self.forecastChart0 = [[SWHomeForecastChartView alloc] init];
        self.forecastChart1 = [[SWHomeForecastChartView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.myMapView forKey:@"myMapView"];
        [self.viewsDictionary setObject:self.mapOverlayView forKey:@"mapOverlayView"];
        [self.viewsDictionary setObject:self.sickScoreIntBackgroundCircleLabel forKey:@"sickScoreIntBackgroundCircleLabel"];
        [self.viewsDictionary setObject:self.sickScoreIntForegroundLabel forKey:@"sickScoreIntForegroundLabel"];
        [self.viewsDictionary setObject:self.sickScoreRiskLabel forKey:@"sickScoreRiskLabel"];
        [self.viewsDictionary setObject:self.sickScoreBrandLabel forKey:@"sickScoreBrandLabel"];
        [self.viewsDictionary setObject:self.sickScoreCityLabel forKey:@"sickScoreCityLabel"];
        [self.viewsDictionary setObject:self.sickScoreTapForMoreInfoLabel forKey:@"sickScoreTapForMoreInfoLabel"];
        [self.viewsDictionary setObject:self.sickScoreActivityIndicatorView forKey:@"sickScoreActivityIndicatorView"];
        [self.viewsDictionary setObject:self.noLocationView forKey:@"noLocationView"];
        [self.viewsDictionary setObject:self.noLocationTitleLabel forKey:@"noLocationTitleLabel"];
        [self.viewsDictionary setObject:self.noLocationDescriptionLabel forKey:@"noLocationDescriptionLabel"];
        [self.viewsDictionary setObject:self.noLocationTurnLocationOnButton forKey:@"noLocationTurnLocationOnButton"];
        [self.viewsDictionary setObject:self.noLocationSearchManuallyButton forKey:@"noLocationSearchManuallyButton"];
        [self.viewsDictionary setObject:self.trendingTitleLabel forKey:@"trendingTitleLabel"];
        [self.viewsDictionary setObject:self.illnessRow0 forKey:@"illnessRow0"];
        [self.viewsDictionary setObject:self.illnessRow1 forKey:@"illnessRow1"];
        [self.viewsDictionary setObject:self.illnessRow2 forKey:@"illnessRow2"];
        [self.viewsDictionary setObject:self.forecastChart0 forKey:@"forecastChart0"];
        [self.viewsDictionary setObject:self.forecastChart1 forKey:@"forecastChart1"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.myMapView.translatesAutoresizingMaskIntoConstraints = NO;
        self.mapOverlayView.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreIntBackgroundCircleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreIntForegroundLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreRiskLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreBrandLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreCityLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreTapForMoreInfoLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.sickScoreActivityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        self.noLocationView.translatesAutoresizingMaskIntoConstraints = NO;
        self.noLocationTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.noLocationDescriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.noLocationTurnLocationOnButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.noLocationSearchManuallyButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.trendingTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.illnessRow0.translatesAutoresizingMaskIntoConstraints = NO;
        self.illnessRow1.translatesAutoresizingMaskIntoConstraints = NO;
        self.illnessRow2.translatesAutoresizingMaskIntoConstraints = NO;
        self.forecastChart0.translatesAutoresizingMaskIntoConstraints = NO;
        self.forecastChart1.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.myMapView];
        [self addSubview:self.mapOverlayView];
        [self addSubview:self.sickScoreIntBackgroundCircleLabel];
        [self addSubview:self.sickScoreIntForegroundLabel];
        [self addSubview:self.sickScoreRiskLabel];
        [self addSubview:self.sickScoreBrandLabel];
        [self addSubview:self.sickScoreCityLabel];
        [self addSubview:self.sickScoreTapForMoreInfoLabel];
        [self addSubview:self.sickScoreActivityIndicatorView];
        [self addSubview:self.noLocationView];
        [self.noLocationView addSubview:self.noLocationTitleLabel];
        [self.noLocationView addSubview:self.noLocationDescriptionLabel];
        [self.noLocationView addSubview:self.noLocationTurnLocationOnButton];
        [self.noLocationView addSubview:self.noLocationSearchManuallyButton];
        [self addSubview:self.trendingTitleLabel];
        [self addSubview:self.illnessRow0];
        [self addSubview:self.illnessRow1];
        [self addSubview:self.illnessRow2];
        [self addSubview:self.forecastChart0];
        [self addSubview:self.forecastChart1];
        
        // LAYOUT
        
        // myMapView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[myMapView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[myMapView(290)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // mapOverlayView
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(0)-[mapOverlayView]-(0)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(0)-[mapOverlayView(290)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // sickScoreIntBackgroundCircleLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[sickScoreIntBackgroundCircleLabel(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[sickScoreIntBackgroundCircleLabel(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:15]];
        
        // sickScoreIntForegroundLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[sickScoreIntForegroundLabel(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
        self.sickScoreIntForegroundHeightConstraint = [NSLayoutConstraint constraintWithItem:self.sickScoreIntForegroundLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:200];
        [self addConstraint:self.sickScoreIntForegroundHeightConstraint];
        //[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[sickScoreIntForegroundLabel(200)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreIntForegroundLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreIntForegroundLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-18]];
        
        // sickScoreRiskLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[sickScoreIntForegroundLabel]-(-60)-[sickScoreRiskLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreRiskLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreRiskLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
        
        // sickScoreBrandLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[sickScoreRiskLabel]-(5)-[sickScoreBrandLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreBrandLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreBrandLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
        
        // sickScoreCityLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[sickScoreIntBackgroundCircleLabel]-(20)-[sickScoreCityLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreCityLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreCityLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
        
        // sickScoreTapForMoreInfoLabel
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreTapForMoreInfoLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.mapOverlayView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-10]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreTapForMoreInfoLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.mapOverlayView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreTapForMoreInfoLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.mapOverlayView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
        
        // sickScoreActivityIndicatorView
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreActivityIndicatorView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.sickScoreActivityIndicatorView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        
        // noLocationView
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.mapOverlayView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.noLocationView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
        
        // noLocationTitleLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[noLocationTitleLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(20)-[noLocationTitleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // noLocationDescriptionLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[noLocationDescriptionLabel]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[noLocationTitleLabel]-(20)-[noLocationDescriptionLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // noLocationTurnLocationOnButton
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[noLocationTurnLocationOnButton]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[noLocationDescriptionLabel]-(20)-[noLocationTurnLocationOnButton(60)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // noLocationSearchManuallyButton
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[noLocationSearchManuallyButton]-(20)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[noLocationTurnLocationOnButton]-(20)-[noLocationSearchManuallyButton(60)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // trendingTitleLabel
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(20)-[trendingTitleLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[mapOverlayView]-(10)-[trendingTitleLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // illnessRow0
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[illnessRow0]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.illnessRow0 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[trendingTitleLabel]-(0)-[illnessRow0(80)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // illnessRow1
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[illnessRow1]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.illnessRow1 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[illnessRow0]-(0)-[illnessRow1(80)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // illnessRow2
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[illnessRow2]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.illnessRow2 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[illnessRow1]-(0)-[illnessRow2(80)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // forecastChart0
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[forecastChart0]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.forecastChart0 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[illnessRow2]-(0)-[forecastChart0(0)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // forecastChart1
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[forecastChart1]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.forecastChart1 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[forecastChart0]-(0)-[forecastChart1(0)]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        
        self.toggleLogoButton = [[UIButton alloc] init];
        self.toggleLogoButton.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.toggleLogoButton setAttributedTitle:@"" forState:UIControlStateNormal];
        [self.toggleLogoButton setImage:[UIImage imageNamed:@"map-marker-covid-public"] forState: UIControlStateNormal];
        [self.toggleLogoButton addTarget:self action:@selector(toggleLogoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        NSLayoutConstraint *leftButtonXConstraint =
        [NSLayoutConstraint
         constraintWithItem:self.toggleLogoButton attribute:NSLayoutAttributeLeft
         relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:
         NSLayoutAttributeLeft multiplier:1.0 constant:-65];
        /* Top space to superview Y*/
        NSLayoutConstraint *topButtonYConstraint =
        [NSLayoutConstraint
         constraintWithItem:self.toggleLogoButton attribute:NSLayoutAttributeTop
         relatedBy:NSLayoutRelationEqual toItem:self.sickScoreIntBackgroundCircleLabel attribute:
         NSLayoutAttributeTop multiplier:1.0f constant:80];
        
        [self addConstraints:@[leftButtonXConstraint, topButtonYConstraint]];
        
        [self addSubview:self.toggleLogoButton];
        
        //NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[forecastChart1]-(15)-|"] options:0 metrics:0 views:self.viewsDictionary];
        //NSLog(@"constraints = %@", constraints);
        //self.forecastChart1PushDownConstraint = [constraints objectAtIndex:0];
        //self.forecastChart1PushDownConstraint.priority = 800;
        //[self addConstraint:self.forecastChart1PushDownConstraint];
        
        // CONFIG
        
        // myMapView
        
        // mapOverlayView
        UITapGestureRecognizer *headerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapOverlayViewTapped:)];
        [self.mapOverlayView addGestureRecognizer:headerTap];
        //self.mapOverlayView.backgroundColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
        //self.mapOverlayView.alpha = 1.0;
        //self.myMapView.alpha = 1.0;
        
        // sickScoreIntBackgroundCircleLabel
        self.sickScoreIntBackgroundCircleLabel.textAlignment = NSTextAlignmentCenter;
        self.sickScoreIntBackgroundCircleLabel.backgroundColor = [UIColor whiteColor];
        self.sickScoreIntBackgroundCircleLabel.layer.borderWidth = 12;
        self.sickScoreIntBackgroundCircleLabel.clipsToBounds = YES;
        self.sickScoreIntBackgroundCircleLabel.layer.cornerRadius = 100;
        self.sickScoreIntBackgroundCircleLabel.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
        
        // sickScoreIntForegroundLabel
        self.sickScoreIntForegroundLabel.textAlignment = NSTextAlignmentCenter;
        
        // sickScoreRiskLabel
        self.sickScoreRiskLabel.textAlignment = NSTextAlignmentCenter;
        
        // sickScoreBrandLabel
        self.sickScoreBrandLabel.textAlignment = NSTextAlignmentCenter;
        
        // sickScoreCityLabel
        self.sickScoreCityLabel.textAlignment = NSTextAlignmentCenter;
        
        // sickScoreTapForMoreInfoLabel
        self.sickScoreTapForMoreInfoLabel.textAlignment = NSTextAlignmentCenter;
        
        // sickScoreActivityIndicatorView
        [self.sickScoreActivityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [self.sickScoreActivityIndicatorView startAnimating];
        
        // noLocationView
        [self.noLocationView setBackgroundColor:[UIColor whiteColor]];
        self.noLocationView.hidden = YES;
        [self bringSubviewToFront:self.noLocationView];
        
        // noLocationTitleLabel
        self.noLocationTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Real-Time SickScores Anywhere" attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
        self.noLocationTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        // noLocationDescriptionLabel
        self.noLocationDescriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Would you like to grant location access to enable this feature?" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherGrayText135x135x135],NSFontAttributeName:[SWFont fontStandardFontOfSize:14]}];
        self.noLocationDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.noLocationDescriptionLabel.numberOfLines = 0;
        
        // noLocationTurnLocationOnButton
        [self.noLocationTurnLocationOnButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Enable Real-Time SickScores" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherWhite255x255x255],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
        self.noLocationTurnLocationOnButton.backgroundColor = [SWColor colorSickweatherStyleGuideBlue37x169x224];
        self.noLocationTurnLocationOnButton.layer.cornerRadius = 6;
        [self.noLocationTurnLocationOnButton addTarget:self action:@selector(noLocationTurnLocationOnButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // noLocationSearchManuallyButton
        [self.noLocationSearchManuallyButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Manually Search Locations" attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideBlue37x169x224],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}] forState:UIControlStateNormal];
        self.noLocationSearchManuallyButton.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        self.noLocationSearchManuallyButton.layer.cornerRadius = 6;
        self.noLocationSearchManuallyButton.layer.borderColor = [SWColor colorSickweatherStyleGuideBlue37x169x224].CGColor;
        self.noLocationSearchManuallyButton.layer.borderWidth = 2.0;
        [self.noLocationSearchManuallyButton addTarget:self action:@selector(noLocationSearchManuallyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        // trendingTitleLabel
        self.trendingTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"TRENDING" attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[SWFont fontStandardBoldFontOfSize:18]}];
        
        // illnessRow0
        //self.illnessRow0.backgroundColor = [UIColor blueColor];
        UITapGestureRecognizer *row0Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rowTapped:)];
        [self.illnessRow0 addGestureRecognizer:row0Tap];
        
        // illnessRow1
        //self.illnessRow1.backgroundColor = [UIColor redColor];
        UITapGestureRecognizer *row1Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rowTapped:)];
        [self.illnessRow1 addGestureRecognizer:row1Tap];
        
        // illnessRow2
        //self.illnessRow2.backgroundColor = [UIColor brownColor];
        UITapGestureRecognizer *row2Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rowTapped:)];
        [self.illnessRow2 addGestureRecognizer:row2Tap];
        
        // forecastChart0
        //self.forecastChart0.backgroundColor = [UIColor lightGrayColor];
        [self.forecastChart0 updateBackgroundImage:[UIImage imageNamed:@"allergies-forecast-mock-image"]];
        
        // forecastChart1
        //self.forecastChart1.backgroundColor = [UIColor grayColor];
        [self.forecastChart1 updateBackgroundImage:[UIImage imageNamed:@"flu-forecast-mock-image"]];
    }
    return self;
}

-(void)toggleLogoButtonTapped:(id)sender {
    if (self.currentObjectForKey) {
        BOOL shouldToggle = [self.currentObjectForKey isEqual: @"cvscore"];
        NSString * toggleObjectForKey = shouldToggle ? @"sickscore" : @"cvscore";
        [[NSNotificationCenter defaultCenter] postNotificationName:kSWDidUpdateCurrentLocationPlacemarkNotification
                 object:self userInfo:@{@"toggle": toggleObjectForKey}];
    }
}

@end
