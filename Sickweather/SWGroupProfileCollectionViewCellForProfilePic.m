//
//  SWProfilePicFlowCollectionViewCell.m
//  Sickweather
//
//  Created by John Erck on 1/20/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWGroupProfileCollectionViewCellForProfilePic.h"

@interface SWGroupProfileCollectionViewCellForProfilePic ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *profilePicImageView;
@end

@implementation SWGroupProfileCollectionViewCellForProfilePic

#pragma mark - Public Methods

- (void)groupProfileCollectionViewCellForProfilePicSetImage:(UIImage *)image;
{
    self.profilePicImageView.image = image;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.profilePicImageView = [[UIImageView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.profilePicImageView forKey:@"profilePicImageView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.profilePicImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.profilePicImageView];
        
        // LAYOUT
        
        // Layout TAG LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[profilePicImageView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[profilePicImageView]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config TAG LABEL
        self.profilePicImageView.clipsToBounds = YES;
        self.profilePicImageView.layer.cornerRadius = 20;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.profilePicImageView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
