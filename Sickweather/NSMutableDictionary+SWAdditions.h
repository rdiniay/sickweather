//
//  NSMutableDictionary+SWAdditions.h
//  Sickweather
//
//  Created by John Erck on 1/31/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (SWAdditions)

+(NSMutableDictionary *)createDictionaryForSectionIndex:(NSArray *)array;
/*
 
 **Input:**
 {"Aardvark", "Cabbage", "Boot", "Eggs", "Ape", "Aquaman", "Elephant", "Cat", "Bat", "Bubbles"}
 
 **Output:**
 NSMutableDictionary
 key => 'A'
 object => {"Aardvark", "Ape", "Aquaman"}
 
 key => 'B'
 object => {"Bat", "Boot", "Bubbles"}
 
 key => 'C'
 object => {"Cat", "Cabbage"}
 
 key => 'E'
 object => {"Elephant", "Eggs"}
 
 */

@end
