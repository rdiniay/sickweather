//
//  SWLabel3FlowCollectionViewCell.m
//  Sickweather
//
//  Created by John Erck on 2/2/16.
//  Copyright © 2016 Sickweather, LLC. All rights reserved.
//

#import "SWAddMessageToReportCollectionViewCellForLabel3.h"
#import "SWColor.h"
#import "SWFont.h"

@interface SWAddMessageToReportCollectionViewCellForLabel3 ()
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UILabel *usernameLabel;
@end

@implementation SWAddMessageToReportCollectionViewCellForLabel3

#pragma mark - Public Methods

- (void)addMessageToReportCollectionViewCellForLabel3SetLabel3AttributedString:(NSAttributedString *)username
{
    self.usernameLabel.attributedText = username;
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT BASICS
        
        // INIT ALL REQUIRED UI ELEMENTS
        self.usernameLabel = [[UILabel alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.usernameLabel forKey:@"usernameLabel"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.usernameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self.contentView addSubview:self.usernameLabel];
        
        // LAYOUT
        
        // Layout TAG LABEL
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[usernameLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[usernameLabel]-(0)-|" options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config TAG LABEL
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //self.contentView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.75];
        //self.usernameLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end

