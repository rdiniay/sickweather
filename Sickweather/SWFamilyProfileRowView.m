//
//  SWFamilyProfileRowView.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 09/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "SWFamilyProfileRowView.h"
#import "NSString+FontAwesome.h"

@interface SWFamilyProfileRowView ()
@property (assign) BOOL shouldShowTopBorder;
@property (strong, nonatomic) NSMutableDictionary *viewsDictionary;
@property (strong, nonatomic) UIImageView *rowIconImageView;
@property (strong, nonatomic) UILabel *rowLabel;
@property (strong, nonatomic) UIImageView *rowDisclosureIconImageView;
@property (strong, nonatomic) UIView *topBorderView;
@property (strong, nonatomic) UIView *bottomBorderView;
@end

@implementation SWFamilyProfileRowView

#pragma mark - Custom Getters/Setters

- (void)setDelegate:(id<SWFamilyProfileRowViewDelegate>)delegate
{
    // SET BACKING VAR
    _delegate = delegate;
    
    // UPDATE PROPS
    if ([self.delegate familyProfileRowViewIconImage:self])
    {
        self.rowIconImageView.image = [self.delegate familyProfileRowViewIconImage:self];
    }
    self.rowLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.delegate familyProfileRowViewTitleString:self] attributes:@{NSForegroundColorAttributeName:[SWColor colorSickweatherStyleGuideGray79x76x77],NSFontAttributeName:[SWFont fontStandardFontOfSize:16]}];
    self.rowDisclosureIconImageView.image = [UIImage imageNamed:@"menu-icon-disclosure-arrow"];
}

#pragma mark - Public Methods

- (void)showBottomBorder
{
    self.bottomBorderView.hidden = NO;
}

- (void)setShowDisclosureIndicatorIcon
{
    self.rowDisclosureIconImageView.hidden = NO;
}

#pragma mark - Target/Action

- (void)viewTapped:(id)sender
{
    [self.delegate familyProfileRowViewWasTapped:self];
}

#pragma mark - Life Cycle Methods

- (id)initWithFrame:(CGRect)frame
{
    // INIT SELF
    self = [super initWithFrame:frame];
    if (self)
    {
        // INIT ALL REQUIRED UI ELEMENTS
        self.rowIconImageView = [[UIImageView alloc] init];
        self.rowLabel = [[UILabel alloc] init];
        self.rowDisclosureIconImageView = [[UIImageView alloc] init];
        self.topBorderView = [[UIView alloc] init];
        self.bottomBorderView = [[UIView alloc] init];
        
        // INIT AUTO LAYOUT VIEWS DICT
        self.viewsDictionary = [NSMutableDictionary new];
        [self.viewsDictionary setObject:self.rowIconImageView forKey:@"rowIconImageView"];
        [self.viewsDictionary setObject:self.rowLabel forKey:@"rowLabel"];
        [self.viewsDictionary setObject:self.rowDisclosureIconImageView forKey:@"rowDisclosureIconImageView"];
        [self.viewsDictionary setObject:self.topBorderView forKey:@"topBorderView"];
        [self.viewsDictionary setObject:self.bottomBorderView forKey:@"bottomBorderView"];
        
        // TURN AUTO LAYOUT ON FOR EACH ONE OF THEM
        self.rowIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.rowDisclosureIconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomBorderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // ESTABLISH VIEW HIERARCHY
        [self addSubview:self.rowIconImageView];
        [self addSubview:self.rowLabel];
        [self addSubview:self.rowDisclosureIconImageView];
        [self addSubview:self.topBorderView];
        [self addSubview:self.bottomBorderView];
        
        // LAYOUT
        
        // Layout ROW ICON IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(15)-[rowIconImageView(20)]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rowIconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // Layout ROW LABEL
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowIconImageView]-(15)-[rowLabel]"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[rowLabel]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout ROW DISCLOSURE ICON IMAGE VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[rowDisclosureIconImageView]-|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[rowDisclosureIconImageView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout TOP BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[topBorderView(1)]"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // Layout BOTTOM BORDER VIEW
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[bottomBorderView]|"] options:0 metrics:0 views:self.viewsDictionary]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[bottomBorderView(1)]|"] options:0 metrics:0 views:self.viewsDictionary]];
        
        // CONFIG
        
        // Config SELF
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        [self addGestureRecognizer:tap];
        
        // Config ROW
        self.backgroundColor = [SWColor colorSickweatherWhite255x255x255];
        
        // Config ROW ICON IMAGE VIEW
        self.rowIconImageView.contentMode = UIViewContentModeCenter;
        self.rowDisclosureIconImageView.hidden = YES;
        
        // Config ROW LABEL
        
        // Config ROW DISCLOSURE ICON IMAGE VIEW
        self.rowDisclosureIconImageView.contentMode = UIViewContentModeCenter;
        
        // Config TOP BORDER ROW
        self.topBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        
        // Config BOTTOM BORDER ROW
        self.bottomBorderView.backgroundColor = [SWColor colorSickweatherLightGray204x204x204];
        self.bottomBorderView.hidden = YES;
        
        // COLOR FOR DEVELOPMENT PURPOSES
        //        self.rowIconImageView.backgroundColor = [SWColor color:[UIColor redColor] usingOpacity:0.5];
        //        self.rowLabel.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
        //        self.rowDisclosureIconImageView.backgroundColor = [SWColor color:[UIColor yellowColor] usingOpacity:0.5];
    }
    return self;
}

@end
