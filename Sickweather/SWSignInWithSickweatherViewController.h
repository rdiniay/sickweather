//
//  SWSignInWithSickweatherViewController.h
//  Sickweather
//
//  Created by John Erck on 2/21/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWSignInWithSickweatherViewController;

@protocol SWSignInWithSickweatherViewControllerDelegate <NSObject>
- (void)signInWithSickweatherViewControllerDidDismissWithSuccessfulLogin:(SWSignInWithSickweatherViewController *)vc;
- (void)signInWithSickweatherViewControllerDidDismissWithoutLoggingIn:(SWSignInWithSickweatherViewController *)vc;
@end

@interface SWSignInWithSickweatherViewController : UIViewController
@property (weak, nonatomic) id <SWSignInWithSickweatherViewControllerDelegate> delegate;
@end
