//
//  SWAddNewEventTableViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/8/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import "SWAddNewEventTableViewController.h"
#import "SWAddSymptomsViewController.h"
#import "SWReportIllnessViewController.h"
#import "SWAddTemperatureViewController.h"
#import "SWEventsTableViewController.h"
#import "SWAddNoteViewController.h"
#import "SWAddMedicationViewController.h"
#import "SWTrackerTypeModal.h"

//Constants

NSString *const kTitleConstant = @"New Event";
@interface SWAddNewEventTableViewController ()
@property (nonatomic, strong) NSDictionary *imageNames;
@property (nonatomic, strong) NSMutableArray *trackerTypes;
@end

@implementation SWAddNewEventTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.trackerTypes = [[NSMutableArray alloc] init];
	self.imageNames = @{@"12":@"event-icon-illness-report", @"2":@"event-icon-medication",@"8":@"event-icon-symptoms", @"6":@"event-icon-temperature", @"10":@"event-icon-note"};
	// Config LEFT BAR BUTTON ITEM
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	self.title = kTitleConstant;
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[self populateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Populate Tracker Types

-(void)populateData {
	NSDictionary *typesdic = [[NSUserDefaults standardUserDefaults] valueForKey:[SWConstants trackerTypesDefaultKey]];
	if ([typesdic isKindOfClass:[NSDictionary class]]){
		NSArray *types = [typesdic objectForKey:@"types"];
		if (types != nil && [types isKindOfClass:[NSArray class]]){
			for (NSDictionary *type in types){
				SWTrackerTypeModal * trackerTypeObj = [[SWTrackerTypeModal alloc] initWithDictionary:type];
				if ([trackerTypeObj.id isKindOfClass:[NSString class]] && ![trackerTypeObj.id isEqual:[NSNull class]] && ![trackerTypeObj.id isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeBluetoothTemperature]]){
					if ([trackerTypeObj.name isEqualToString:@"Manual Temperature"])
						trackerTypeObj.name = @"Temperature";
					[self.trackerTypes addObject:trackerTypeObj];
				}
			}
			[self.tableView reloadData];
		}
	}
}


#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	if(self.isFromFamilyScreen) {
		[self navigateToEvents];
	}else {
		[self.navigationController popViewControllerAnimated:true];
	}
}

#pragma mark - User Navigation

-(void)navigateToEvents{
	NSMutableArray *viewController = [NSMutableArray arrayWithArray: [self.navigationController viewControllers]];
	if(viewController.count >= 1){
		[viewController removeObjectsInRange:NSMakeRange(1, viewController.count-1)];
		SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
		eventsTableViewController.title = [SWUserCDM currentUser].firstName;
		[viewController addObject:eventsTableViewController];
		[self.navigationController setViewControllers:viewController];
	}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.trackerTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifier = @"SWAddNewEventCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	UIImageView *imgView = (UIImageView *)[cell viewWithTag:101];
	UILabel *lblName = (UILabel *)[cell viewWithTag:102];
	SWTrackerTypeModal * trackerTypeObj = [self.trackerTypes objectAtIndex:indexPath.row];
	lblName.text = trackerTypeObj.name;
	imgView.image = [UIImage imageNamed:[self.imageNames objectForKey:trackerTypeObj.id]];
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	SWTrackerTypeModal *trackerTypeObj = [self.trackerTypes objectAtIndex:indexPath.row];
	if ([trackerTypeObj.id isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeIllness]]) {
		//Add Flurry Event
		[SWHelper logFlurryEventsWithTag:@"Family > Illness Report button tapped"];
		UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		SWReportIllnessViewController *addNewEventsTableViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"SWReportIllnessViewController"];
		//Decision for Pop
		addNewEventsTableViewController.isFromEventsScreen = self.isFromEventsScreen;
		addNewEventsTableViewController.isFromFamilyScreen = self.isFromFamilyScreen;
        if(self.isFamilyMember){
            addNewEventsTableViewController.isFamilyMember = self.isFamilyMember;
            addNewEventsTableViewController.familyMember = self.familyMember;
        }
		[self.navigationController pushViewController:addNewEventsTableViewController animated:YES];
	}else if ([trackerTypeObj.id isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeMdeication]]) {
		[SWHelper logFlurryEventsWithTag:@"Family > Medication button tapped"];
		SWAddMedicationViewController *addSMedicationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddMedicationViewController"];
		//Decision for Pop
		addSMedicationViewController.familyMember = self.familyMember;
		addSMedicationViewController.isFromFamilyScreen = self.isFromFamilyScreen;
		[self.navigationController pushViewController:addSMedicationViewController animated:YES];
	}else if ([trackerTypeObj.id isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeSymptoms]]){
		//Add Flurry Event
		[SWHelper logFlurryEventsWithTag:@"Family > Symptoms button tapped"];
		SWAddSymptomsViewController *addSymptomsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddSymptomsViewController"];
		//Decision for Pop
		addSymptomsViewController.isFromFamilyScreen = self.isFromFamilyScreen;
		if(self.isFamilyMember){
			addSymptomsViewController.isFamilyMember = self.isFamilyMember;
			addSymptomsViewController.familyMember = self.familyMember;
		}
		addSymptomsViewController.title = @"Symptoms";
		[self.navigationController pushViewController:addSymptomsViewController animated:YES];
	}else if ([trackerTypeObj.id isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeManualTemperature]]){
		//Add Flurry Event
		[SWHelper logFlurryEventsWithTag:@"Family > Add a Manual Temperature button tapped"];
		SWAddTemperatureViewController *addTemperatureViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddTemperatureViewController"];
		addTemperatureViewController.title = @"Add a Temperature";
		if(self.isFamilyMember){
			addTemperatureViewController.isFamilyMember = self.isFamilyMember;
			addTemperatureViewController.familyMember = self.familyMember;
		}
		[self.navigationController pushViewController:addTemperatureViewController animated:YES];
	}else if ([trackerTypeObj.id isEqualToString:[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeNotes]]){
		[SWHelper logFlurryEventsWithTag:@"Family > Notes button tapped"];
		SWAddNoteViewController *addSNotesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWAddNoteViewController"];
			//Decision for Pop
		addSNotesViewController.familyMember = self.familyMember;
		addSNotesViewController.isFromFamilyScreen = self.isFromFamilyScreen;
		[self.navigationController pushViewController:addSNotesViewController animated:YES];
	}
}
@end
