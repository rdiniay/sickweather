//
//  SWFamilyLoginView.h
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 11/12/2017.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SWFamilyLoginViewDelegate
-(void)loginButtonPressed;
-(void)createAnAccountButtonPressed;
@end

@interface SWFamilyLoginView : UIView
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) id<SWFamilyLoginViewDelegate> delegate;
@end
