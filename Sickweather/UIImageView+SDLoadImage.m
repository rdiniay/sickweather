//
//  UIImageView+SDLoadImage.m
//  Sickweather
//
//  Created by Taimoor Abdullah Khan on 03/01/2018.
//  Copyright © 2018 Sickweather, LLC. All rights reserved.
//

#import "UIImageView+SDLoadImage.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UIImageView (SDLoadImage)
-(void)sdLoadImageWith:(NSString*)url placeholder:(UIImage*)placeholder {
   NSURL* weburl = [[NSURL alloc] initWithString:url];
   [self sd_setImageWithURL:weburl placeholderImage:placeholder options:SDWebImageRefreshCached];
}
@end
