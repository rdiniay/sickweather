//
//  SWMyProfileHeaderView.h
//  Sickweather
//
//  Created by John Erck on 9/29/15.
//  Copyright © 2015 Sickweather, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWMyProfileHeaderView;

@protocol SWMyProfileHeaderViewDelegate
- (NSString *)myProfileHeaderViewNameString:(SWMyProfileHeaderView *)sender;
- (NSString *)myProfileHeaderViewUsernameString:(SWMyProfileHeaderView *)sender;
- (UIImage *)myProfileHeaderViewProfilePicImage:(SWMyProfileHeaderView *)sender;
@end

@interface SWMyProfileHeaderView : UIView
@property (nonatomic, weak) id<SWMyProfileHeaderViewDelegate> delegate;
- (void)setProfilePicImage:(UIImage *)image;
@end
