//
//  SWAddSymptomsViewController.m
//  Sickweather
//
//  Created by Shan Shafiq on 12/9/17.
//  Copyright © 2017 Sickweather, LLC. All rights reserved.
//
@import SWAnalytics;
#import "SWAddSymptomsViewController.h"
#import "SWSymptomsCollectionViewCell.h"
#import "SWConstants.h"
#import "SWSymptomsModal.h"
#import "SWSymptomHeaderView.h"
#import "SWSymptomsWithTempHeaderView.h"
#import "SWAppBackend.h"
#import "SWEventsTableViewController.h"

@interface SWAddSymptomsViewController ()
@property (strong, nonatomic) NSMutableArray *symptomsData;
@property (strong, nonatomic) NSMutableArray *selectedValues;
@end

@implementation SWAddSymptomsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.symptomsData = [[NSMutableArray alloc]init];
    // Config Navigation Bar Items
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-icon"] landscapeImagePhone:[UIImage imageNamed:@"back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemTapped:)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemTapped:)];
	// Content Spacing
	[self.collectionView setAllowsMultipleSelection:true];
	[self.collectionView setExclusiveTouch:true];
	self.collectionView.contentInset = UIEdgeInsetsMake(0,0, 0, 0);
	// Register Header Views Base On User Navigation
	if (self.isFromBlueToothTempScreen){
		[self.collectionView registerNib:[UINib nibWithNibName:@"SWSymptomsWithTempHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SWSymptomsWithTempHeaderView"];
	}else{
		[self.collectionView registerNib:[UINib nibWithNibName:@"SWSymptomHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SWSymptomHeaderView"];
	}
	[self populateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Populate Symptoms

-(void)populateData {
	NSArray *symptomsList = [[NSUserDefaults standardUserDefaults] valueForKey:[SWConstants symptomsDefaultKey]];
	if (symptomsList) {
		for (NSDictionary *symptom in symptomsList){
			SWSymptomsModal * symptomObj = [[SWSymptomsModal alloc] initWithDictionary:symptom];
			[self.symptomsData addObject:symptomObj];
		}
	}
	[self.collectionView reloadData];
}

#pragma mark - Add Tracker

-(void)addTrackerWith:(NSDictionary*)data{
	[self.activityIndicator startAnimating];
	[[SWHelper helperAppBackend]appBackendAddTrackerUsingArgs:data completionHandler:^(NSDictionary *jsonResponseBody, NSString *responseBodyAsString, NSHTTPURLResponse *responseHeaders, NSError *requestError, NSError *jsonParseError, NSString *requestURLAbsoluteString) {
		 dispatch_async(dispatch_get_main_queue(), ^{
			 [self.activityIndicator stopAnimating];
			if ([[jsonResponseBody objectForKey:@"status"] isEqualToString:@"success"]) {
				for (NSString *symptom in self.selectedValues) {
					NSMutableDictionary * additionalData = [[SWUserCDM currentlyLoggedInUser] userProfileDataForSnow];
					if (additionalData == nil || additionalData == NULL){
						additionalData = [[NSMutableDictionary alloc] init];
					}
					NSMutableDictionary * params = [additionalData objectForKey:@"params"];
					[params setObject:[data objectForKey:@"type"] forKey:@"tracker_id"];
					[params setObject:symptom forKey:@"symptom_name"];
					[SWAnalytics updateLocation:[SWHelper helperLocationManager].location.coordinate.latitude longitude:[SWHelper helperLocationManager].location.coordinate.longitude params:@{@"params":params}];
				}
				if (self.isFromBlueToothTempScreen) {
					[self navigateToEventsWith:@"temperature and symptoms"];
				}else if (self.isFromFamilyScreen){
					[self navigateToEventsWith:@"symptoms"];
				}else {
					[[NSNotificationCenter defaultCenter] postNotificationName:kSWRefreshEventsNotification
																		object:self
																	  userInfo:@{@"message":@"symptoms"}];
					[self popBackWith:3];
				}
			}else {
				UIAlertController *alert = [UIAlertController alertControllerWithTitle:[jsonResponseBody objectForKey:@"message"] message:nil preferredStyle:UIAlertControllerStyleAlert];
				UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
				[alert addAction:ok];
				[self presentViewController:alert animated:YES completion:nil];
			}
		});
	}];
}

#pragma mark - User Navigation

-(void)navigateToEventsWith:(NSString*)message{
	NSMutableArray *viewController = [NSMutableArray arrayWithArray: [self.navigationController viewControllers]];
	if (viewController.count >=1){
		[viewController removeObjectsInRange:NSMakeRange(1, viewController.count-1)];
		SWEventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWEventsTableViewController"];
        if (self.isFamilyMember) {
            eventsTableViewController.title = self.familyMember.name_first;
        } else {
            eventsTableViewController.title = [SWUserCDM currentUser].firstName;
        }
		eventsTableViewController.showSuccessHeader = true;
        eventsTableViewController.successMessage = message;
        eventsTableViewController.isFamilyMember = self.isFamilyMember;
        eventsTableViewController.familyMember = self.familyMember;
        [viewController addObject:eventsTableViewController];
		[self.navigationController setViewControllers:viewController];
	}
	
}

-(void)popBackWith:(int)nb{
	NSArray *viewControllers = [self.navigationController viewControllers];
	if ([viewControllers count] > nb) {
        [self.navigationController popToViewController:[viewControllers objectAtIndex:[viewControllers count] - nb] animated:true];
	}
}

#pragma mark - Target/Action

- (void)leftBarButtonItemTapped:(id)sender
{
	if (self.isFromBlueToothTempScreen) {
		[self navigateToEventsWith:@"temperature"];
	}else {
		[self.navigationController popViewControllerAnimated:true];
	}
}

- (void)rightBarButtonItemTapped:(id)sender
{
		// IF WE HAVE NEVER PROMPTED FOR LOCATION, DO SO NOW
	if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Access Required" message:@"Symptoms are attributed to your current location. Is it okay for us to ask for access to your location?" preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			[[SWHelper helperLocationManager] requestWhenInUseAuthorization];
		}];
		[alert addAction:ok];
		UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
				// Do nothing, they cancelled out
		}];
		[alert addAction:cancel];
		[self presentViewController:alert animated:YES completion:^{}];
	}
	// CHECK FOR CURRENT ACCESS (WE MIGHT ALREADY HAVE LOCATION ACCESS)
	else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
			//Add Flurry Event
		[SWHelper logFlurryEventsWithTag:@"Family > Symptoms > Save button tapped"];
		self.selectedValues = [[NSMutableArray alloc] init];
		for (SWSymptomsModal *symptom in self.symptomsData){
			if (symptom.isSelected){
				[self.selectedValues addObject:symptom.name];
			}
		}
		if ([self.selectedValues count] > 0) {
			[self addTrackerWith:@{@"type":[NSString stringWithFormat:@"%lu",(unsigned long)SWTrackerTypeSymptoms], @"member_id":self.familyMember != nil ? self.familyMember.member_id : @"", @"value":[self.selectedValues componentsJoinedByString:@", "]}];
			
		}else {
			UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please choose at least one symptom" preferredStyle:UIAlertControllerStyleAlert];
			UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
			[alert addAction:ok];
			[self presentViewController:alert animated:YES completion:nil];
		}
	}
}

#pragma mark - Helper Method
-(NSAttributedString*)getFormattedTemperatureString:(NSString*)temperatureString
{
	NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString: temperatureString];
	[attributedString addAttribute:NSBaselineOffsetAttributeName
							 value:@(15.0)
							 range:NSMakeRange(attributedString.length-1, 1)];
	[attributedString addAttribute:NSForegroundColorAttributeName
							 value:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]
							 range:NSMakeRange(attributedString.length-2, 2)];
	[attributedString addAttribute:NSFontAttributeName
							 value:[UIFont fontWithName:@"raleway-semibold" size:20]
							 range:NSMakeRange(attributedString.length-1, 1)];
	return attributedString;
}

#pragma mark - UICollection View Data Source
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifier = @"SWSymptomsCollectionViewCell";
	SWSymptomsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
	SWSymptomsModal *symptomObj = (SWSymptomsModal*)[self.symptomsData objectAtIndex:indexPath.row];
	[cell populateCellWith:symptomObj];
	return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
	return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return self.symptomsData.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	UICollectionReusableView *reusableview = nil;
	if (kind == UICollectionElementKindSectionHeader) {
		if (self.isFromBlueToothTempScreen) {
			SWSymptomsWithTempHeaderView *headerView = (SWSymptomsWithTempHeaderView *)[self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SWSymptomsWithTempHeaderView" forIndexPath:indexPath];
			headerView.lblFever.attributedText = [self getFormattedTemperatureString:self.temp];
			headerView.lblType.text = self.severity;
			reusableview = headerView;
		}else {
			SWSymptomHeaderView *headerView = (SWSymptomHeaderView *)[self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SWSymptomHeaderView" forIndexPath:indexPath];
			headerView.backgroundColor = [SWColor colorSickweatherBlue41x171x226];
			reusableview = headerView;
		}
	}
	return reusableview;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
	return (self.isFromBlueToothTempScreen) ? CGSizeMake(self.collectionView.frame.size.width, 140.0f) : CGSizeMake(self.collectionView.frame.size.width, 50.0f);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
	return UIEdgeInsetsMake(10,16, 10, 16);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	if (indexPath.row == 0) {
		for (SWSymptomsModal *symptom in self.symptomsData){
			if ([symptom.name isEqualToString:@"No Symptoms"]){
				symptom.isSelected = true;
			}else {
				symptom.isSelected = false;
			}
		}
	}else {
		SWSymptomsModal * symptomObj = [self.symptomsData objectAtIndex:indexPath.row];
		symptomObj.isSelected = symptomObj.isSelected == true ? false : true;
		symptomObj = [self.symptomsData objectAtIndex:0];
		symptomObj.isSelected = false;
	}
	[self.collectionView reloadData];
}

@end
