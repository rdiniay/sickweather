//
//  SWHelpers.h
//  Sickweather
//
//  Created by John Erck on 3/20/14.
//  Copyright (c) 2014 Sickweather, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SWAppBackend.h"
#import "SWAppDelegate.h"
#import "SWGeocodeManager.h"
#import "SWSlidingViewController.h"
#import "SWUserCDM+SWAdditions.h"

typedef NS_OPTIONS(NSUInteger, SWTrackerType) {
	SWTrackerTypeMdeication = 2,
	SWTrackerTypeBluetoothTemperature = 4,
	SWTrackerTypeManualTemperature = 6,
	SWTrackerTypeSymptoms = 8,
	SWTrackerTypeNotes = 10,
	SWTrackerTypeIllness = 12
};

@interface SWHelper : NSObject
+ (NSManagedObjectContext *)helperManagedObjectContext;
+ (NSUserDefaults *)helperDefaults;
+ (SWAppBackend *)helperAppBackend;
+ (SWAppDelegate *)helperAppDelegate;
+ (SWUserCDM *)helperGetCurrentUser;
+ (SWUserCDM *)helperCurrentUser;
+ (NSString *)helperCurrentUserSickweatherId;
+ (BOOL)helperUserIsLoggedIn;
+ (NSString *)helperLongStateNameGivenTwoCharCode:(NSString *)twoCharCode;
+ (SWGeocodeManager *)helperGeocodeManager;
+ (CLLocationManager *)helperLocationManager;
+ (BOOL)helperIsCurrentlyConnectedToTheInternet;
+ (BOOL)helperIsProduction;
+ (NSString *)helperAppVersionString;
+ (NSString *)helperAppName;
+ (BOOL)helperIsIOS8OrHigher;
+ (NSString *)helperURLEncode:(NSString *)toBeURLEncodedString;
+ (void)helperLogErrorToAppBackendUsingMessage:(NSString *)message file:(const char *)file prettyFunction:(const char *)prettyFunction line:(int)line;
+ (void)helperLogErrorToAppBackendForJSONResponseBody:(NSDictionary *)jsonResponseBody
                                 responseBodyAsString:(NSString *)responseBodyAsString
                                      responseHeaders:(NSHTTPURLResponse *)responseHeaders
                                         requestError:(NSError *)requestError
                                       jsonParseError:(NSError *)jsonParseError
                             requestURLAbsoluteString:(NSString *)requestURLAbsoluteString
                                                 file:(const char *)file
                                       prettyFunction:(const char *)prettyFunction
                                                 line:(int)line;
+ (SWSlidingViewController *)helperGetTab1SlidingViewController;
+ (UITabBarController *)helperGetTabBarSlidingViewController;
+ (UIViewController *)helperGetTopVCFor:(UIViewController *)vc;
+ (UIImage *)helperGetScreenshot;
+ (UIImage *)helperOnePixelImageUsingColor:(UIColor *)color;
+ (NSDictionary*)helperParseURLParams:(NSString *)query;
+ (void)helperShowAlertWithTitle:(NSString *)title;
+ (void)helperShowAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (NSArray *)helperArrayOfIllnessIDsToGeofenceFor;
+ (NSString *)helperGetUserSelectedIllnessIDsAsCommaSeparatedString;
+ (NSNumber *)helperHandWashTimerTotalFrameCount;
+ (NSNumber *)helperHandWashTimerDuration;
+ (NSNumber *)helperHandWashTimerFramesPerSecond;
+ (NSString *)helperSickweatherAPIKey;
+ (CGFloat)helperECSlidingViewControllerAnchorRightPeekAmount;

//Screen
+ (CGFloat)getScreenWidth;
+ (CGFloat)getScreenHeight;
+ (BOOL)isIpad;

//Fabric
+ (void)addNonFatalExceptionWith:(NSException *)exception;
+ (void)addNonFatalError:(NSError *)error;
+ (void)addNonFatalErrorWithDictionary:(NSDictionary *)dictionary;
+(void)addNonFatalEventsWithName:(NSString*)name  data:(NSDictionary*)dictionary;

//Create Image
+ (UIImage *)imageFromString:(NSString *)string attributes:(NSDictionary *)attributes size:(CGSize)size;

//Conversion
+(float)convertCelsiusToFarenheit:(float)temp;
+(float)convertFarenhietToCelsius:(float)temp;

//Temp Severity
+(NSString*)updateFeverSeverityWithCelcius:(float) temp;
+(NSString*)updateFeverSeverityWithFarenheit:(float) temp;

/* FILE SYSTEM HELPER METHODS */

// FamilyId
+(NSString*)getFamilId;
+(void)setFamilyId:(NSString *)familyId;

// BUNDLE
+ (NSData *)helperLoadJSONDataFromBundleDocsUsingName:(NSString *)name forceFeedFromBundle:(BOOL)forceFeedFromBundle;
+ (NSString *)helperLoadJSONStringFromBundleDocsUsingName:(NSString *)name forceFeedFromBundle:(BOOL)forceFeedFromBundle;
+ (NSObject *)helperLoadJSONObjectFromBundleDocsUsingName:(NSString *)name forceFeedFromBundle:(BOOL)forceFeedFromBundle;

// DOCS
+ (NSData *)helperLoadJSONDataFromDocsUsingName:(NSString *)name;
+ (NSString *)helperLoadJSONStringFromDocsUsingName:(NSString *)name;
+ (id)helperLoadJSONObjectFromDocsUsingName:(NSString *)name;

//Flurry
+(void)logFlurryEventsWithTag:(NSString*)tag;
+(void)logFlurryEventsWithTag:(NSString*)tag parameters:(NSDictionary*)params;

+ (void)helperSaveJSONData:(NSData *)data toDocsUsingName:(NSString *)name;
+ (void)helperSaveJSONString:(NSString *)string toDocsUsingName:(NSString *)name;
+ (void)helperSaveJSONObject:(NSObject *)object toDocsUsingName:(NSString *)name;

/* ILLNESS FILE HELPER METHODS */

+ (NSString *)helperIllnessNameForId:(NSString *)illnessId objectFromDocsUsingName: (NSString *)objectFromDocsUsingName;

/* OTHER HELPER METHODS */
+ (NSString *)helperUserLocationCityStateCountryFormattedStringForUICity:(NSString *)city state:(NSString *)state country:(NSString *)country;
+ (void)helperShowFullViewLabel:(UILabel *)label usingView:(UIView *)view;
+ (void)helperShowFullScreenActivityIndicatorUsingMessage:(NSString *)message;
+ (void)helperShowFullScreenActivityIndicatorUsingMessage:(NSString *)message backgroundColor:(UIColor *)backgroundColor backgroundColorIsDark:(BOOL)backgroundColorIsDark;
+ (void)helperDismissFullViewLabel:(UILabel *)label;
+ (void)helperDismissFullScreenActivityIndicator;
+ (void)helperDismissFullScreenActivityIndicatorUsingSuccessMessage:(NSString *)successMessage;
+ (NSString *)helperGetIllnessesPath;
+ (void)helperWriteString:(NSString *)string toFilePath:(NSString *)filePath;
+ (NSURL *)helperGetDocumentDirectoryURLForFileName:(NSString *)fileName;

// USER DEFAULT BACKED VALUES
+ (NSDictionary *)helperGetCurrentUserFromDefaults:(NSDictionary *)user;
+ (NSDictionary *)helperGetCurrentUserFromDefaults;
+ (BOOL)helperHasPassedFilterTimeSinceLastDisplayedUserFeedbackPrompt;
+ (NSString *)helperGetCurrentUserSWIdFromDefaults;
@end
